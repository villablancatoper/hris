<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_complaint extends CI_Controller {
    public $user;


	public function __construct(){
      
      parent::__construct();;
     
	

		 $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
        'Customercomplaint_model' => 'complaint'
      ));
      if(!$this->session->userdata('user')){
        redirect('login');
      } 
     date_default_timezone_set('Asia/Manila');
    }
    public function get_categories_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());
    }

    public function get_productservices_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());
    }

    public function get_branches_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());
    }

	public function index(){
    $user = $this->session->userdata('user');
    $department = $user->user_department;
    $user_id = $user->user_id;
    $user_position = $user->user_position;
    $user_role = $user->user_role;

    $branches = $this->complaint->get_complaint_branches($user_id, $user_position, $user_role);

    

	 $data = array(
        'page_title' => 'Customer Complaint',
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $branches,
        'changelogs' => $this->dashboard->get_changelogs()
      );
		$this->render('customer_complaint/dashboard', $data);
	}


	public function render($page, $data){

      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
  }



}


