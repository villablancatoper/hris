<?php



    class Joborder extends CI_Controller{

        public function __construct(){

            parent::__construct();



            $this->load->model(array(

                'Joborder_model' => 'joborder',

                'Customer_model' => 'customer',

                'Dashboard_model' => 'dashboard',
                'Report_model' => 'report',
                'Z_index_model' => 'z_index',
                'superadmin/User_model' => 'user'
            ));



            if(!$this->session->userdata('user')){

                redirect('login');

            }

        }

        public function get_servicedate(){
            $service_id = $this->input->post('service_id');
            $date = $this->input->post('date_rendered');
            $date_rendered = new Datetime($date);
            $date_rendered = $date_rendered->format("m/d/y");
            $productservice_dates = $this->joborder->get_servicedate($service_id);
            if ($productservice_dates->productservice_startdate == NULL) {
                $service_start = NULL;
            } else {
                $service_start = new Datetime($productservice_dates->productservice_startdate);
                $service_start = $service_start->format('m/d/y');
            }

            if ($productservice_dates->productservice_enddate == NULL) {
                $service_end = NULL;
            } else {
                $service_end = new Datetime($productservice_dates->productservice_enddate);
                $service_end = $service_end->format('m/d/y');
            }
            

            $data = array('start' => $service_start, 'end' => $service_end, 'date_rendered' => $date_rendered);
            echo json_encode($data);
        }

        public function get_retaildate(){
            $retail_id = $this->input->post('retail_id');
            $date = $this->input->post('date_rendered');
            $date_rendered = new Datetime($date);
            $date_rendered = $date_rendered->format("m/d/y");
            $productretail_dates = $this->joborder->get_retaildate($retail_id);
            if ($productretail_dates->productretail_startdate == NULL) {
                $retail_start = NULL;
            } else {
                $retail_start = new Datetime($productretail_dates->productretail_startdate);
                $retail_start = $retail_start->format('m/d/y');
            }

            if ($productretail_dates->productretail_enddate == NULL) {
                $retail_end = NULL;
            } else {
                $retail_end = new Datetime($productretail_dates->productretail_enddate);
                $retail_end = $retail_end->format('m/d/y');
            }
            

            $data = array('start' => $retail_start, 'end' => $retail_end, 'date_rendered' => $date_rendered);
            echo json_encode($data);
        }


        public function index(){

            $user = $this->session->userdata('user');



            $data = array(

                'page_title' => __CLASS__,

                'user' => $user,

                'locations' => $this->customer->get_locations()->result(),

                'occupations' => $this->customer->get_occupations()->result(),

                'ages' => $this->customer->get_ages()->result(),

                'customers' => $this->get_customers()->result(),

                'changelogs' => $this->dashboard->get_changelogs()

            );

            $this->render('joborder/new_index', $data);

        }



        public function entry($customer_id, $joborder_id=null, $workaround=null){

            $user = $this->session->userdata('user');

            $brand_id = $this->session->userdata('user')->brand_id;

            $data = array(

                'page_title' => __CLASS__,

                'user' => $user,

                'joborder' => null,

                'customer' => $this->customer->get_customer($customer_id),

                'foottraffic' => $this->joborder->get_customer_foottraffic($customer_id),

                'serviceproviders' => $this->joborder->get_serviceproviders()->result(),

                'productservices' => $this->dashboard->get_productservices_by_brand_id($brand_id)->result(),

                'productretails' => $this->dashboard->get_productretails_by_brand_id($brand_id)->result(),

                'user_lateencode' => $this->get_user_late_encoding_status($user->user_id),

                'branch_allowencode' => $this->joborder->get_branch_allowencode($user->branch_id),

                'request_expiry' => $this->dashboard->get_lateencoding_by_id($user->user_id, $user->branch_id),

                'changelogs' => $this->dashboard->get_changelogs()

            );



            if($joborder_id && $this->uri->segment(5) == null){

                $data['joborder'] = $this->joborder->get_joborder($joborder_id)->row();

                $data['customer'] = $this->customer->get_customer($customer_id);

                $data['foottraffic'] = $this->joborder->get_joborder($joborder_id)->row()->transaction_customerstatus;

            }



            if($joborder_id && $this->uri->segment(5) != null){

                $data['joborder'] = $this->joborder->get_joborder($joborder_id)->row();

                $data['customer'] = $this->customer->get_customer_old($this->uri->segment(5))->row();

            }

	
					
			if($brand_id == 100001){		
                $this->render('joborder/vs_entry', $data);		
            }	
            else {
                $this->render('joborder/new_entry', $data);
            }

            // var_dump($this->uri->segment(5));

        }



        public function get_customer_old($workaround){

            echo $this->customer->get_customer_old($workaround)->row();

        }




        public function get_serviceprovider($id){

            echo json_encode($this->joborder->get_serviceprovider($id)->row());

        }



        public function get_customers(){

            return $this->customer->get_customers();

        }



        public function turnaway(){

            $user = $this->session->userdata('user');



            $data = array(

                'page_title' => __CLASS__,

                'user' => $user,

                'branch' => $this->joborder->get_branches()->result(),

                'changelogs' => $this->dashboard->get_changelogs()

            );



            $this->render('joborder/turnaway', $data);

        }



        public function test(){

            echo var_dump($this->session->userdata('user'));

        }



        public function get_turnaways(){

            $draw = intval($this->input->get("draw"));

            $start = intval($this->input->get("start"));

            $length = intval($this->input->get("length"));



            $turnaways = $this->joborder->get_turnaways();



            $data = array();





            foreach($turnaways->result() as $turnaway){



                $data[] = array(

                    $turnaway->turnaway_id,

                    date('m/d/Y', strtotime($turnaway->turnaway_date)),

                    $turnaway->turnaway_quantity,

                    $turnaway->turnaway_remarks

                );



            }



            $output = array(

                "draw" => $turnaways->num_rows(),

                "recordsTotal" => $turnaways->num_rows(),

                "recordsFiltered" => $turnaways->num_rows(),

                "data" => $data

            );



            echo json_encode($output);

        }



        public function add_turnaway(){

            $data = array(

                'branch_id' => $this->session->userdata('user')->branch_id,

                'turnaway_date' => date('Y-m-d', strtotime($this->input->post('add_turnaway_date'))),

                'turnaway_quantity' => $this->input->post('add_turnaway_turnaway'),

                'turnaway_type' => $this->input->post('add_turnaway_type'),

                'turnaway_remarks' => $this->input->post('add_turnaway_remarks'),

                'turnaway_status' => 1

            );



            echo $this->joborder->add_turnaway($data);

        }



        public function add_customer(){

            $data = array(

                'customer_firstname' => $this->input->post('add_customer_firstname'),

                'branch_id' => $this->session->userdata('user')->branch_id,

                'customer_lastname' => $this->input->post('add_customer_lastname'),

                'customer_gender' => $this->input->post('add_customer_gender'),

                'customer_phoneno' => $this->input->post('add_customer_phoneno'),

                'customer_email' => $this->input->post('add_customer_email'),

                'customer_mobileno' => $this->input->post('add_customer_mobileno'),

                'location_id' => $this->input->post('add_customer_location_id'),

                'occupation_id' => $this->input->post('add_customer_occupation_id'),

                'customer_birthday' => $this->input->post('add_customer_customer_birthday'),

                'age_id' => $this->input->post('add_customer_age_id')

            );



            echo $this->customer->add_customer($data);

        }



        public function get_turnaway($id){

            $data = $this->joborder->get_turnaway($id)->row();



            $turnaway = array(

                'turnaway_date' => date('m/d/Y', strtotime($data->turnaway_date)),

                'turnaway_id' => $data->turnaway_id,

                'turnaway_type' => $data->turnaway_type,

                'turnaway_quantity' => $data->turnaway_quantity,

                'turnaway_remarks' => $data->turnaway_remarks

            );

                

            echo json_encode($turnaway);

        }



        public function delete_turnaway($id){

            echo $this->joborder->delete_turnaway($id);

        }



        public function update_turnaway($id){

            $data = array(

                'turnaway_date' => date('Y-m-d', strtotime($this->input->post('turnaway_date'))),

                'turnaway_type' => $this->input->post('turnaway_type'),

                'turnaway_quantity' => $this->input->post('turnaway_turnaway'),

                'turnaway_remarks' => $this->input->post('turnaway_remarks')

            );



            echo $this->joborder->update_turnaway($data, $id);

        }



        public function get_turnaways_by_date(){

            $date_start = date('Y-m-d', strtotime($this->input->post('date_start')));

            $date_end = date('Y-m-d', strtotime($this->input->post('date_end')));

            $branch_id = $this->input->post('branch_id');



            $draw = intval($this->input->post("draw"));

            $start = intval($this->input->post("start"));

            $length = intval($this->input->post("length"));



            $turnaways = null;

            

            if(!$branch_id){

                $branch_id = $this->session->userdata('user')->branch_id;

            }

            

            $turnaways = $this->joborder->get_turnaways_by_date($branch_id, $date_start, $date_end);



            $data = array();





            foreach($turnaways->result() as $turnaway){



                $data[] = array(

                    $turnaway->turnaway_id,

                    date('m/d/Y', strtotime($turnaway->turnaway_date)),

                    $turnaway->turnaway_type,

                    $turnaway->turnaway_quantity,

                    $turnaway->turnaway_remarks,

                );



            }



            $output = array(

                "draw" => $turnaways->num_rows(),

                "recordsTotal" => $turnaways->num_rows(),

                "recordsFiltered" => $turnaways->num_rows(),

                "data" => $data

            );



            echo json_encode($output);

        }



        public function insert_transaction(){

            $foottraffic = $this->input->post('transaction_customerstatus');
            $customer_id = $this->input->post('customer_id');

            $this->customer->update_customer_foottraffic($customer_id, $foottraffic);

            $transaction_data = array(

                'customer_id' => $this->input->post('customer_id'),

                'brand_id' => $this->input->post('brand_id'),

                'branch_id' => $this->input->post('branch_id'),

                'transaction_date' => date('Y-m-d', strtotime($this->input->post('transaction_date'))),

                'transaction_docketno' => $this->input->post('transaction_docketno'),

                'transaction_orno' => $this->input->post('transaction_orno'),

                'transaction_totalsales' => $this->input->post('transaction_totalsales'),

                'transaction_paymentcash' => $this->input->post('transaction_paymentcash'),

                'transaction_paymentcard' => $this->input->post('transaction_paymentcard'),

                'transaction_paymentgc' => $this->input->post('transaction_paymentgc'),

                'transaction_status' => 1,

                'transaction_customerstatus' => $this->input->post('transaction_customerstatus'),

            );



            $transaction_details_data = array(

                'retail_services_data' => $this->input->post('retail_services_data'),

                'product_services_data' => $this->input->post('product_services_data'),

                'product_consumables_data' => $this->input->post('product_consumables_data'),

                'gcs_data' => $this->input->post('gcs_data')

            );



            echo $this->joborder->insert_transaction($transaction_data, $transaction_details_data);

            // var_dump($transaction_details_data['retail_services_data']);

        }



        public function update_transaction(){

            $transaction_id = $this->input->post('transaction_id');



            $transaction_data = array(

                'customer_id' => $this->input->post('customer_id'),

                'brand_id' => $this->input->post('brand_id'),

                'branch_id' => $this->input->post('branch_id'),

                'transaction_date' => date('Y-m-d', strtotime($this->input->post('transaction_date'))),

                'transaction_docketno' => $this->input->post('transaction_docketno'),

                'transaction_orno' => $this->input->post('transaction_orno'),

                'serviceprovider_id' => $this->input->post('serviceprovider_id'),

                'transaction_assistid' => $this->input->post('transaction_assistid'),

                'transaction_totalsales' => $this->input->post('transaction_totalsales'),

                'transaction_grosssales' => $this->input->post('transaction_grosssales'),

                'transaction_subjectcom' => $this->input->post('transaction_subjectcom'),

                'transaction_netcom' => $this->input->post('transaction_netcom'),

                'transaction_paymentcash' => $this->input->post('transaction_paymentcash'),

                'transaction_paymentcard' => $this->input->post('transaction_paymentcard'),

                'transaction_paymentgc' => $this->input->post('transaction_paymentgc'),

            );



            $transaction_details_data = array(

                'retail_services_data' => $this->input->post('retail_services_data'),

                'product_services_data' => $this->input->post('product_services_data'),

                'product_consumables_data' => $this->input->post('product_consumables_data')

            );



            echo $this->joborder->update_transaction($transaction_id, $transaction_data, $transaction_details_data);

        }



        public function delete_transaction($transaction_id){

            $this->joborder->delete_transaction($transaction_id);

        }



        public function get_product_service($productservice_code){
            $data = array(
                'product_service' => $this->joborder->get_product_service($productservice_code)->row(),
                'servicemix' => $this->joborder->get_servicemix_by_productservice_code($productservice_code)->result()
            );
            if(!$data['product_service']){
                $data['servicemix'] = null;
            }
            echo json_encode($data);
        }



        public function get_servicemix_by_productservice_code($productservice_code){

            echo json_encode($this->joborder->get_servicemix_by_productservice_code($productservice_code)->result());

        }



        public function get_retail_service($productretail_code){

            $data = array(
                'product_retail' => $this->joborder->get_retail_service($productretail_code)->row(),
                'servicemix' => $this->joborder->get_servicemix_by_productservice_code($productretail_code)->result()
            );
            if(!$data['product_retail']){
                $data['servicemix'] = null;
            }
            echo json_encode($data);

        }



        public function get_joborders_by_customer_id(){



            $search_item = $this->input->get('search_item');



            $joborders = $this->joborder->get_joborders_by_customer_id($search_item)->result();



            $data = array();



            foreach($joborders as $joborder){

                $services_performed = $this->joborder->get_joborder_details_by_transaction_id($joborder->transaction_id)->result();



                $services = '';



                foreach($services_performed as $key => $service_performed){

                    if($service_performed->productservice_description){

                        if($key == 0){

                            $services .= $service_performed->productservice_description;

                        }

                        else{

                            $services .= ", $service_performed->productservice_description";

                        }

                        

                    }

                }



                $data[] = array(

                    'transaction_id' => $joborder->transaction_id,

                    'transaction_date' => $joborder->transaction_date,

                    'brand_name' => $joborder->brand_name,
                    
                    'branch_id' => $joborder->branch_id,

                    'branch_name' => $joborder->branch_name,

                    'serviceprovider_name' => $joborder->serviceprovider_name,

                    'services_performed' => $services

                );    

            }



            echo json_encode($data);

        }



        public function get_joborder_details_by_transaction_id($transaction_id){

            echo json_encode($this->joborder->get_joborder_details_by_transaction_id($transaction_id)->result());

        }



        public function get_joborder_details_products($transaction_id){

            echo json_encode($this->joborder->get_joborder_details_products($transaction_id)->result());

        }



        public function get_joborder_details_retails($transaction_id){

            echo json_encode($this->joborder->get_joborder_details_retails($transaction_id)->result());

        }



        public function get_joborders_on_job_trans_e(){

            $C_IDNO = $this->input->get('customer_id');
            
            $C_BRANCH_ID = $this->session->userdata('user')->branch_id;



            $joborders = $this->joborder->get_joborders_on_job_trans_e($C_IDNO, $C_BRANCH_ID)->result();



            $data = array();



            // foreach($joborders as $joborder){

                // $services_performed = $this->joborder->get_joborder_details_by_transaction_id($joborder->transaction_id)->result();



                // $services = '';



                // foreach($services_performed as $key => $service_performed){

                //     if($service_performed->productservice_description){

                //         if($key == 0){

                //             $services .= $service_performed->productservice_description;

                //         }

                //         else{

                //             $services .= ", $service_performed->productservice_description";

                //         }

                        

                //     }

                // }



                // $data[] = array(

                //     'transaction_id' => $joborder->transaction_id,

                //     'transaction_date' => $joborder->transaction_date,

                //     'brand_name' => $joborder->brand_name,

                //     'branch_name' => $joborder->branch_name,

                //     'serviceprovider_name' => $joborder->serviceprovider_name,

                //     'services_performed' => $services

                // );    

            // }

            echo json_encode($joborders);

        }



        public function check_gc_availability(){

            $search_item = $this->input->get("search_item");



            echo json_encode($this->joborder->check_gc_availability($search_item));

        }



        public function validate_gc(){

            $pin = $this->input->get("pin");

            $barcode = $this->input->get("barcode");

            echo json_encode($this->joborder->validate_gc($pin, $barcode));

        }

        public function get_all_customers(){
            echo json_encode($this->customer->get_all_customers()->result());
        }

        public function fetch_branches(){
            if ($this->input->post('brand_id')) {
              echo $this->joborder->fetch_branches($this->input->post('brand_id'));
            }
        }
        
        
        public function deposit(){
            $user = $this->session->userdata('user');
            $data = array(
                'page_title' => __CLASS__,
                'user' => $user,
                'branch' => $this->joborder->get_branches()->result(),
                'brands' => $this->report->get_brands()->result()
                // 'changelogs' => $this->dashboard->get_changelogs()
            );
            $this->render('joborder/deposit', $data);
        }

        public function get_all_deposit(){
            $user = $this->session->userdata('user');
            $branch_id = $user->branch_id;
            if ($user->user_position == 'Super Admin') {
                $data = $this->z_index->get_all_deposit();
            }else{
                $data = $this->z_index->get_deposit_branch($branch_id);
            }

            echo json_encode($data);
        }

        public function add_deposit(){
            $user = $this->session->userdata('user');
            if ($user->user_position == 'Standard User') {
                $brand_id = $user->brand_id;
                $branch_id = $user->branch_id;
            }else{
                $brand_id = $this->input->post('brand_id');
                $branch_id = $this->input->post('branch_id');
            }

            $datepicker = $this->input->post('datepicker');
            $transaction_date = new DateTime($datepicker);
            $transaction_date = $transaction_date->format('Y-m-d');
            $amount = $this->input->post('amount');
            $remarks = $this->input->post('remarks');

            $now = new DateTime();
            $now = $now->format('Y-m-d');
            $data = array('brand_id' => $brand_id,
                          'branch_id' => $branch_id, 
                            'transaction_date' => $transaction_date,
                            'cash_deposit' => $amount,
                            'deposit_date' => $now,
                            'remarks' => $remarks,
                         );
            $this->z_index->add_deposit($data);
        }

        public function check_servicemixext(){
            $productservice_code = $this->input->get('productservice_code');
            
            echo json_encode($this->joborder->check_servicemixext($productservice_code)->result());
        }

        public function check_servicemixml(){
            $productservice_code = $this->input->get('productservice_code');
            
            echo json_encode($this->joborder->check_servicemixml($productservice_code)->result());
        }

        public function check_start_end(){
            $search_product_service = $this->input->post('search_product_service');

            echo json_encode($this->joborder->check_start_end($search_product_service));
        }

        public function get_user_late_encoding_status($user_id){
            return $this->user->get_user_late_encoding_status($user_id)->row()->user_lateencode;
        }

        public function render($page, $data){

            $this->load->view('templates/head', $data);

            $this->load->view('templates/header', $data);

            $this->load->view('templates/sidebar', $data);

            $this->load->view($page, $data);

            $this->load->view('templates/footer', $data);

        }

        public function get_available_services(){
            $user = $this->session->userdata('user');
            $brand_id = $user->brand_id;
            $user_lateencode = $user->user_lateencode;
            $date_rendered = $this->input->post('date_rendered');
            
            if ($date_rendered) {
              echo $this->joborder->get_available_services($date_rendered, $brand_id, $user_lateencode);
            }
        }

   
    }



    