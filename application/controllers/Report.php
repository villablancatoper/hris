<?php

  class Report extends CI_Controller{
    public $user;
    function __construct(){
      parent::__construct();
      $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
      ));

      if(!$this->session->userdata('user')){
        redirect('login');
      }
    }

    public function dsr(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'changelogs' => $this->dashboard->get_changelogs()
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        $data['dsrs'] = $this->report->get_dsrs_by_branch_id($this->user->branch_id)->result();
        $this->render('report/dsr_branch', $data);
      }
      else{
        $this->render('report/dsr_ho', $data);
      }
    }

    public function search_dsrs(){
      $data = array(
        'brand_id' => $this->input->get('brand'),
        'branch_id' => $this->input->get('branch'),
        'start' => date('Y-m-d', strtotime($this->input->get('start'))),
        'end' => date('Y-m-d', strtotime($this->input->get('end')))
      );

      echo json_encode($this->report->search_dsrs($data)->result());
    }

    public function dsr_detailed(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'changelogs' => $this->dashboard->get_changelogs()
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        // $data['dsrs_detailed'] = $this->report->get_dsrs_detailed_by_branch_id($this->user->branch_id)->result();
        // $data['total_sales'] = $this->report->get_total_sales_branch()->row();
        // $data['total_cash'] = $this->report->get_total_cash_branch()->row();
        // $data['total_card'] = $this->report->get_total_card_branch()->row();
        // $data['total_gc'] = $this->report->get_total_gc_branch()->row();
        // $data['total_sales_services'] = $this->report->get_total_sales_product_service_branch();
        // $data['total_sales_otc'] = $this->report->get_total_sales_product_retail_branch()->row();
        // $data['total_turnaway'] = $this->report->get_total_turnaway_branch()->row();
        // $data['total_walkin'] = $this->report->get_total_walkin_branch()->num_rows();
        // $data['total_regular'] = $this->report->get_total_regular_branch()->num_rows();
        // $data['total_transfer'] = $this->report->get_total_transfer_branch()->num_rows();
        // $data['total_head_count'] = $this->report->get_total_head_count_branch()->num_rows();

        // $data['todate_total_sales'] = $this->report->get_todate_total_sales_branch()->row();
        // $data['todate_tph'] = $this->report->get_todate_tph_branch();
        // $data['todate_otc'] = $this->report->get_todate_otc_branch()->row();
        // $data['todate_services'] = $this->report->get_todate_services_branch();
       
        $this->render('report/dsr_detailed_branch', $data);
      }
      else{
        $this->render('report/dsr_detailed_ho', $data);
      }
    }

    public function search_dsrs_detailed(){
      $data = array(
        'brand_id' => $this->input->get('brand'),
        'branch_id' => $this->input->get('branch'),
        'start' => date('Y-m-d', strtotime($this->input->get('start'))),
        'end' => date('Y-m-d', strtotime($this->input->get('end')))
      );

      echo json_encode($this->report->search_dsrs_detailed($data)->result());
    }

    public function overall_report(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'changelogs' => $this->dashboard->get_changelogs()
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        $data['overall_report'] = $this->report->get_overall_report_by_branch_id($this->user->branch_id)->result();
        $data['productservices_count'] = $this->report->get_productservices_count($this->user->branch_id)->num_rows();
        $data['productretails_count'] = $this->report->get_productretails_count($this->user->branch_id)->num_rows();
        $data['turnaway_count'] = $this->report->get_turnaways_by_branch_id_and_date($this->user->branch_id)->num_rows();
        $this->render('report/overall_report_branch', $data);
      }
      else{
        $this->render('report/overall_report_ho', $data);
      }
    }

    public function search_overall_report(){
      $data = array(
        'brand_id' => $this->input->get('brand'),
        'branch_id' => $this->input->get('branch'),
        'start' => date('Y-m-d', strtotime($this->input->get('start'))),
        'end' => date('Y-m-d', strtotime($this->input->get('end')))
      );

      $overall_report_data = $this->report->search_overall_report($data);
      echo json_encode($overall_report_data);
    }
    
    public function search_dsrs_detailed_branch(){
        $start = date('Y-m-d', strtotime($this->input->get('start')));
        $end = date('Y-m-d', strtotime($this->input->get('end')));
        $branch_id = $this->session->userdata('user')->branch_id;
        
        echo json_encode($this->report->search_dsrs_by_branch_id($branch_id, $start, $end)->result());
    }
    
    public function get_dsrs_detailed_branch(){
      
        $branch_id = $this->input->get('branch');
        $start = date('Y-m-d', strtotime($this->input->get('start')));
        $end = date('Y-m-d', strtotime($this->input->get('end')));
        
        $data = array(
            'total_sales' => $this->report->get_total_sales_branch($branch_id, $start, $end)->row(),
            'total_cash' => $this->report->get_total_cash_branch($branch_id, $start, $end)->row(),
            'total_card' => $this->report->get_total_card_branch($branch_id, $start, $end)->row(),
            'total_gc' => $this->report->get_total_gc_branch($branch_id, $start, $end)->row(),
            'total_sales_otc' => $this->report->get_total_sales_product_retail_branch($branch_id, $start, $end)->row(),
            'total_sales_services' => $this->report->get_total_sales_product_service_branch($branch_id, $start, $end),
            'total_turnaway' => $this->report->get_total_turnaway_branch($branch_id, $start, $end)->row(),
            'total_walkin' => $this->report->get_total_walkin_branch($branch_id, $start, $end)->num_rows(),
            'total_regular' => $this->report->get_total_regular_branch($branch_id, $start, $end)->num_rows(),
            'total_transfer' => $this->report->get_total_transfer_branch($branch_id, $start, $end)->num_rows(),
            'total_head_count' => $this->report->get_total_head_count_branch($branch_id, $start, $end)->num_rows()
        );

        $data['todate_total_sales'] = $this->report->get_todate_total_sales_branch($branch_id, $start, $end)->row();
        $data['todate_tph'] = $this->report->get_todate_tph_branch($branch_id, $start, $end);
        $data['todate_otc'] = $this->report->get_todate_otc_branch($branch_id, $start, $end)->row();
        $data['todate_services'] = $this->report->get_todate_services_branch($branch_id, $start, $end);
        $data['branch_id'] = $branch_id;
        
        echo json_encode($data);
    }

    public function custom_dsr(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'changelogs' => $this->dashboard->get_changelogs()
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        // $this->render('report/dsr_branch', $data);
      }
      else{
        $this->render('report/custom_dsr_ho', $data);
      }
    }

    public function custom_isr(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result()
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        $this->render('report/custom_isr_branch', $data);
      }
      else{
        $this->render('report/custom_isr_ho', $data);
      }
    }

    public function search_custom_dsr_ho(){
      $brand_id = $this->input->get('brand');
      $branch_id = $this->input->get('branch');
      $date = date('Y-m-d', strtotime($this->input->get('date')));

      if($branch_id == 'asm_branches'){
        echo json_encode($this->report->search_custom_dsr_asm_branches($date));
      }
      else{
        echo json_encode($this->report->search_custom_dsr_ho($brand_id, $branch_id, $date));
      }
    }

    public function search_custom_isr_ho(){
      $brand_id = $this->input->get('brand');
      $branch_id = $this->input->get('branch');
      $date = date('Y-m-d', strtotime($this->input->get('date')));

      echo json_encode($this->report->search_custom_isr_ho($brand_id, $branch_id, $date));
    }

    public function search_custom_isr_branch(){
      $date = date('Y-m-d', strtotime($this->input->get('date')));

      echo json_encode($this->report->search_custom_isr_branch($date));
    }

    public function custom_csr(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

      if($this->user->user_position == "Standard User" && $this->user->user_role == "Standard User"){
        $this->render('report/custom_csr_branch', $data);
      }
      else{
        $this->render('report/custom_csr_ho', $data);
      }
    }

    public function custom_csr_otc(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

        $this->render('report/custom_csr_otc', $data);
      
    }

    public function search_annual_csr_otc(){
      $year = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");
      $otc = $this->input->post("otc");


      $month_list = array('January',
                          'February',
                          'March',
                          'April',
                          'May',
                          'June',
                          'July',
                          'August',
                          'September',
                          'October',
                          'November',
                          'December'
      );

      for ($i=0; $i < 12; $i++) { 
        $month = new DateTime($month_list[$i]." ".$year);
        $month_start = $month->format('Y-m-d');
        $month_end = $month->format('Y-m-t');
        $start_end = array('start' => $month_start, 'end' => $month_end);
        $month_array[$month_list[$i]] = $start_end;
      }
      
      $data = array('brand_id' => $brand_id,
                    'branch_id' => $branch_id,
                    'otc'    => $otc,
                    'months' => $month_array
      );            
      function date_compare($a, $b){
          $t1 = strtotime($a['start']);
          $t2 = strtotime($b['start']);
          return $t1 - $t2;
      }    
      usort($data['months'], 'date_compare');
      echo json_encode($this->report->search_annual_csr_otc($data));

     

    }

    public function annual_csr_otc(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

        $this->render('report/annual_csr_otc', $data);
      
    }

    public function annual_csr(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

        $this->render('report/annual_csr', $data);
    }




    public function search_custom_csr_branch(){
      $category_code = $this->input->get('category_code');
      $productservice_code = $this->input->get('productservice_code');
      $start = date('Y-m-d', strtotime($this->input->get('start')));
      $end = date('Y-m-d', strtotime($this->input->get('end')));

      echo json_encode($this->report->search_custom_csr_branch($category_code, $productservice_code, $start, $end)->result());
    }

    public function search_custom_csr_ho(){
      $brand_id = $this->input->get('brand_id');
      $branch_id = $this->input->get('branch_id');
      $category_code = $this->input->get('category_code');
      $productservice_code = $this->input->get('productservice_code');
      $start = date('Y-m-d', strtotime($this->input->get('start')));
      $end = date('Y-m-d', strtotime($this->input->get('end')));

      echo json_encode($this->report->search_custom_csr_ho($brand_id, $branch_id, $category_code, $productservice_code, $start, $end));
    }

    public function get_categories_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());
    }

    public function get_productservices_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());
    }

    public function get_branches_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());
    }
    public function head_count(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

   
        $this->render('report/head_count', $data);
    }

    public function get_head_count(){
      $year = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");
    //   $month = new DateTime("November 2019");
    //   $month = $month->format('Y-m-d');
      // $year = "2019";
      // $brand_id = 100005;
      // $branch_id = 111;

      $month_list = array('January',
                          'February',
                          'March',
                          'April',
                          'May',
                          'June',
                          'July',
                          'August',
                          'September',
                          'October',
                          'November',
                          'December'
       );

      for ($i=0; $i < 12; $i++) { 
        $month = new DateTime($month_list[$i]." ".$year);
        $month_start = $month->format('Y-m-d');
        $month_end = $month->format('Y-m-t');
        $start_end = array('start' => $month_start, 'end' => $month_end);
        $month_array[$month_list[$i]] = $start_end;
      }
      
      $data = array('brand_id' => $brand_id,
                    'branch_id' => $branch_id,
                    'months' => $month_array
      );            
      function date_compare($a, $b){
          $t1 = strtotime($a['start']);
          $t2 = strtotime($b['start']);
          return $t1 - $t2;
      }    
      usort($data['months'], 'date_compare');
      $output = $this->report->get_head_count($data);
      for ($i=0; $i < 12 ; $i++) { 
          if ($output[$i]['walk_in'] == 0 && $output[$i]['regular'] == 0 && $output[$i]['transfer'] == 0) {
              unset($output[$i]);
          }
      }
      $new_output = array_values($output);
      // echo "<pre>";
      // print_r($output);
      // echo "</pre>";

      echo json_encode($new_output);
    }

    public function get_converted_walk_in(){
      $month = $this->input->post('month');
      $year = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");
      
      echo json_encode($this->report->get_converted_walk_in($brand_id, $branch_id, $month, $year));
    }

    public function get_converted_regular(){
      $month = $this->input->post('month');
      $year = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");
      
      echo json_encode($this->report->get_converted_regular($brand_id, $branch_id, $month, $year));
    }



    public function get_converted_walk_in_annual(){
      $year = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");


      $month_list = array('January',
                          'February',
                          'March',
                          'April',
                          'May',
                          'June',
                          'July',
                          'August',
                          'September',
                          'October',
                          'November',
                          'December'
       );

      for ($i=0; $i < 12; $i++) { 
        $month = new DateTime($month_list[$i]." ".$year);
        $month_start = $month->format('Y-m-d');
        $month_end = $month->format('Y-m-t');
        $start_end = array('start' => $month_start, 'end' => $month_end);
        $month_array[$month_list[$i]] = $start_end;
      }

      $data = array('brand_id' => $brand_id,
                    'branch_id' => $branch_id,
                    'months' => $month_array
      );   

      function date_compare($a, $b){
          $t1 = strtotime($a['start']);
          $t2 = strtotime($b['start']);
          return $t1 - $t2;
      }  

      usort($data['months'], 'date_compare');
      $output = $this->report->get_converted_walk_in_annual($data);
      echo json_encode($output);

      // echo json_encode($this->report->get_converted_walk_in($brand_id, $branch_id, $year));
      
    }
    




    
    public function get_converted_walk_in_sp(){
      $month = $this->input->post("year");
      $brand_id = $this->input->post("brand_id");
      $branch_id = $this->input->post("branch_id");

      $month = new DateTime($month);
      $month_start = $month->format('Y-m-01');
      $month_end = $month->format('Y-m-t');

      $three_months = strtotime("+3 months", strtotime($month_end));
      $three_months = strftime ( '%Y-%m-%d' , $three_months );

      $data = array('start' => $month_start, 'brand_id' => $brand_id, 'branch_id' => $branch_id, 'end' => $month_end, 'three_months' => $three_months);

      $output = $this->report->get_converted_walk_in_sp($data);

      foreach($output['data'] as $elementKey => $element) {
          foreach($element as $valueKey => $value) {
            if($valueKey == 'walk_in' && $value == 0){
                      //delete this particular object from the $array
                unset($output['data'][$elementKey]);
          } 
        }
      }
      $new_output = array_values($output['data']);

      echo json_encode($new_output);  
    }

    public function converted_walk_in_sp(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

   
        $this->render('report/converted_walk_in_sp', $data);
    }
    
    
    
    public function converted_walk_in(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

   
        $this->render('report/converted_walk_in', $data);
    }

    public function converted_regular(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result()
      );

        $this->render('report/converted_regular', $data);
    }
    


    public function fetch_branches(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_branches($this->input->post('brand_id'));
      }
    }

    public function fetch_branches_no_all(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_branches_no_all($this->input->post('brand_id'));
      }
    }

    public function fetch_asm_brands(){
      if ($this->input->post('user_id')) {
        echo $this->report->fetch_asm_brands($this->input->post('user_id'));
      }
    }
    public function test(){
      $result = $this->report->fetch_asm_brands(352);
      print_r($result);
    }


    public function fetch_otc(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_otc($this->input->post('brand_id'));
      }
    }

    public function fetch_items(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_items($this->input->post('brand_id'));
      }
    }


    public function fetch_category(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_category($this->input->post('brand_id'));
      }
    }

    public function fetch_category_with_all(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_category_with_all($this->input->post('brand_id'));
      }
    }


    public function fetch_services(){
      if ($this->input->post('category_id')) {
        echo $this->report->fetch_services($this->input->post('category_id'));
      }
    }

    public function fetch_services_with_all(){
      if ($this->input->post('category_id')) {
        echo $this->report->fetch_services_with_all($this->input->post('category_id'));
      }
    }

    public function custom_csr_item(){
      $brand_id = $this->session->userdata('user')->brand_id;

      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

        $this->render('report/custom_csr_item', $data);
    }

    

    public function search_custom_csr_items(){
       $brand_id = $this->input->post('brand_id');
       $branch_id = $this->input->post('branch_id');
       $item_id = $this->input->post('item_id');
       $start = $this->input->post('start');
       $end = $this->input->post('end');

        echo json_encode($this->report->search_custom_csr_items($brand_id, $branch_id, $item_id, $start, $end));

    }

    public function search_annual_csr(){
     $brand_id = $this->input->post('brand_id');
     $branch_id = $this->input->post('branch_id');
     $category_id = $this->input->post('category_id');
     $service_id = $this->input->post('service_id');
     $year = $this->input->post('year');
      
    $month_list = array('January',
                          'February',
                          'March',
                          'April',
                          'May',
                          'June',
                          'July',
                          'August',
                          'September',
                          'October',
                          'November',
                          'December'
      );

      for ($i=0; $i < 12; $i++) { 
        $month = new DateTime($month_list[$i]." ".$year);
        $month_start = $month->format('Y-m-d');
        $month_end = $month->format('Y-m-t');
        $start_end = array('start' => $month_start, 'end' => $month_end);
        $month_array[$month_list[$i]] = $start_end;
      }
      
      $data = array('brand_id' => $brand_id,
                    'branch_id' => $branch_id,
                    'category_id'    => $category_id,
                    'service_id' => $service_id,
                    'months' => $month_array
      );            
      function date_compare($a, $b){
          $t1 = strtotime($a['start']);
          $t2 = strtotime($b['start']);
          return $t1 - $t2;
      }    
      usort($data['months'], 'date_compare');
      echo json_encode($this->report->search_annual_csr($data));
      // print_r($output); // echo json_encode($this->report->search_annual_csr($brand_id, $branch_id, $otc, $start, $end));

    }
    

    public function search_custom_csr_otc(){
     $brand_id = $this->input->post('brand_id');
     $branch_id = $this->input->post('branch_id');
     $otc = $this->input->post('otc');
     $start = $this->input->post('start');
     $end = $this->input->post('end');

      echo json_encode($this->report->search_custom_csr_otc($brand_id, $branch_id, $otc, $start, $end));

    }

    public function variance_analysis(){
      $brand_id = $this->session->userdata('user')->brand_id;
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'categories' => $this->report->get_categories_by_brand_id($brand_id)->result(),
        'productservices' => $this->report->get_productservices_by_brand_id($brand_id)->result(),
      );

        $this->render('report/variance_analysis', $data);
    }

    public function get_variance(){
      $year = $this->input->post('year');
      $month = $this->input->post('month');
      $branch_id = $this->input->post('branch_id');

      echo json_encode($this->report->get_variance($branch_id, $month, $year));
    }

    public function per_brand_sales(){
      $data = array(
        'page_title' => 'Report',
        'user' => $this->user,
      );
  
      $this->render('report/per_brand_sales', $data);
    }

    public function get_per_brand_sales(){
      $start = $this->input->get('start');
      echo json_encode($this->report->get_per_brand_sales($start));
    }

    public function per_branch_sales(){
      $data = array(
        'page_title' => 'Report',
        'user' => $this->user,
        'branches' => $this->joborder->get_branches()->result(),
      );
  
      $this->render('report/per_branch_sales', $data);
    }

    public function get_per_branch_sales(){
      $start = $this->input->get('start');
      $branch_id = $this->input->get('branch_id');
      echo json_encode($this->report->get_per_branch_sales($start, $branch_id));
    }

    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }

    public function fetch_services_by_brand_id(){
      if ($this->input->post('brand_id')) {
        echo $this->report->fetch_services_by_brand_id($this->input->post('brand_id'));
      }
    }
  }