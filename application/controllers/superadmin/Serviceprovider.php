<?php

  class Serviceprovider extends CI_Controller{
    public $user;

    function __construct(){
      parent::__construct();

      $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'superadmin/Serviceprovider_model' => 'serviceprovider'
      ));

      if(!$this->session->userdata('user')){
        redirect('login');
      }

      if($this->user->branch_id != 1){
        redirect('dashboard');
      }
    }

    public function index(){
      $data = array(
        'page_title' => "SP",
        'user' => $this->user,
        // 'brands' => $this->user_model->get_brands()->result(),
        'branches' => $this->serviceprovider->get_branches()->result(),
        'positions' => $this->serviceprovider->get_serviceprovider_positions()->result()
      );

      $this->render('superadmin/serviceprovider/index', $data);
    }

    public function get_serviceproviders(){
      echo json_encode($this->serviceprovider->get_serviceproviders()->result());
    }

    public function get_serviceprovider($serviceprovider_id){
      echo json_encode($this->serviceprovider->get_serviceprovider($serviceprovider_id)->row());
    }

    public function add_serviceprovider(){

      $position_id = $this->input->post('add_serviceprovider_level');
      $serviceprovider_level = null;

      if($position_id == 1){
        $serviceprovider_level = 'SL';
      }
      else if($position_id == 2){
        $serviceprovider_level = 'SR STYLIST';
      }
      else if($position_id == 3){
        $serviceprovider_level = 'JR STYLIST';
      }
      else if($position_id == 5){
        $serviceprovider_level = 'BARBER';
      }


      $data = array(

        'serviceprovider_name' => $this->input->post('add_serviceprovider_name'),
        
        'branch_id' => $this->input->post('add_branches'),

        'position_id' => $position_id,

        'serviceprovider_level' => $serviceprovider_level,

        'serviceprovider_empid' => $this->input->post('add_serviceprovider_empid')
      );

      $this->serviceprovider->add_serviceprovider($data);
      
    }

    public function delete_serviceprovider($serviceprovider_id){
      $this->serviceprovider->delete_serviceprovider($serviceprovider_id);
    }

    public function update_serviceprovider($serviceprovider_id){

      $position_id = $this->input->post('serviceprovider_level');
      $serviceprovider_level = null;

      if($position_id == 1){
        $serviceprovider_level = 'SL';
      }
      else if($position_id == 2){
        $serviceprovider_level = 'SR STYLIST';
      }
      else if($position_id == 3){
        $serviceprovider_level = 'JR STYLIST';
      }
      else if($position_id == 5){
        $serviceprovider_level = 'BARBER';
      }

      $data = array(

        'serviceprovider_name' => $this->input->post('serviceprovider_name'),

        'branch_id' => $this->input->post('branches'),

        'position_id' => $this->input->post('serviceprovider_level'),

        'serviceprovider_level' => $serviceprovider_level,

        'serviceprovider_empid' => $this->input->post('serviceprovider_empid')

      );

      $this->serviceprovider->update_serviceprovider($serviceprovider_id, $data);
      
    }

    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }
  }