<?php

    class Inventory extends CI_Controller{
        protected $user;

        function __construct(){
            parent::__construct();

            $this->load->model(array(
                'superadmin/Inventory_model' => 'inventory'
            ));
        }

        public function index(){

            $user = $this->session->userdata('user');

            $data = array(

                'page_title' => __CLASS__,

                'user' => $user,

                'categories' => $this->inventory->get_categories()->result()

            );

            $this->render('superadmin/inventory/index', $data);

        }

        public function render($page, $data){

            $this->load->view('templates/head', $data);
      
            $this->load->view('templates/header', $data);
      
            $this->load->view('templates/sidebar', $data);
      
            $this->load->view($page, $data);
      
            $this->load->view('templates/footer', $data);
      
        }
    }