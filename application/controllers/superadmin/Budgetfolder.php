<?php

class Budgetfolder extends CI_Controller{
    public $user;

    function __construct(){
        parent::__construct();

        $this->user = $this->session->userdata('user');

        $this->load->model(array(
            'Budgetfolder_model' => 'budgetfolder'
        ));

        if(!$this->session->userdata('user')){
            redirect('login');
        }

        if($this->user->branch_id != 1){
            redirect('dashboard');
        }
    }

    public function index(){
        $data = array(
          'page_title' => __CLASS__,
          'user' => $this->user,
          // 'brands' => $this->user_model->get_brands()->result(),
          'branches' => $this->budgetfolder->get_branches()->result(),
        );
  
        $this->render('budgetfolder/index', $data);
    }

    public function generate_budgetfolder(){
        $branch_id = $this->input->get('branch_id');

        echo json_encode($this->budgetfolder->generate_budgetfolder($branch_id)->result());
    }

    public function render($page, $data){
        $this->load->view('templates/head', $data);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer', $data);
    }
}