<?php

  class Assorted extends CI_Controller{
    public $user;

    function __construct(){
      parent::__construct();

      $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'superadmin/Assorted_model' => 'assorted_model'
      ));

      if(!$this->session->userdata('user')){
        redirect('login');
      }

      if($this->user->branch_id != 1){
        redirect('dashboard');
      }
    }

    public function render($page, $data){
        $this->load->view('templates/head', $data);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function get_suppliers_by_folder(){
        $folder = $this->input->get('folder');

        echo json_encode($this->assorted_model->get_suppliers_by_folder($folder)->result());
    }

  }