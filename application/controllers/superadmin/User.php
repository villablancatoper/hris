<?php

  class User extends CI_Controller{
    public $user;

    function __construct(){
      parent::__construct();

      $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'superadmin/User_model' => 'user_model'
      ));

      if(!$this->session->userdata('user')){
        redirect('login');
      }

      if($this->user->branch_id != 1){
        redirect('dashboard');
      }
    }

    public function index(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        // 'brands' => $this->user_model->get_brands()->result(),
        'branches' => $this->user_model->get_branches()->result(),
      );

      $this->render('superadmin/user/index', $data);
    }

    public function get_users(){
      // echo json_encode($this->user_model->get_users()->result());
      echo json_encode('test');
    }

    public function get_user($user_id){
      echo json_encode($this->user_model->get_user($user_id)->row());
    }

    public function add_user(){

      $data = array(

        'user_name' => $this->input->post('add_user_name'),
        
        'user_username' => $this->input->post('add_user_username'),

        'branch_id' => $this->input->post('add_branches'),

        'user_password' => $this->input->post('add_user_password')
      
      );

      $this->user_model->add_user($data);
      
    }

    public function delete_user($user_id){
      $this->user_model->delete_user($user_id);
    }

    public function update_user($user_id){

      $data = array(

        'user_name' => $this->input->post('user_name'),
        
        'user_username' => $this->input->post('user_username'),

        'branch_id' => $this->input->post('branches')
      
      );

      $this->user_model->update_user($user_id, $data);
      
    }

    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }
    
    public function check_branch_no_transaction_yesterday(){
      
      $export_excel = $this->input->post('export_excel');

      if(isset($export_excel)){

        $yesterday = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));

        $branch_no_transaction_yesterday = $this->user_model->check_branch_no_transaction_yesterday($yesterday)->result();

        $excel = '
            <table class="table" bordered="1">
              <tr>
                <th style="text-align: center;">Store</th>
                <th style="text-align: center;">ASM</th>
                <th style="text-align: center;">Last date encoded</th>
              </tr>
        ';
  
        foreach($branch_no_transaction_yesterday as $value){
          $excel .= '
            <tr>
              <td>'.$value->branch_name.'</td>
              <td>'.$value->user_name.'</td>
              <td>'.$value->encoding_status.'</td>
            </tr>
          ';
        }
  
        $excel .= '</table>';
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=Not_Updated_CRM.xls");
        echo $excel;

      }


      // redirect('superadmin/user/index');
    }

    public function send_not_updated_crm(){
      $yesterday = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));

      $branch_no_transaction_yesterday = $this->user_model->check_branch_no_transaction_yesterday($yesterday)->result();

      $message = 'R2-CFS System Generated Message - Do Not Reply ';

      // foreach($branch_no_transaction_yesterday as $key => $value){

      //   if($key == 0){
      //     break;
      //   }

      //   $message .= $value->branch_name."\n";
      // }

      $result = $this->itexmo('09954246301', $message, 'TR-TIMOT246301_KNEL3');

      if ($result == ""){
        echo "iTexMo: No response from server!!! Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.	Please CONTACT US for help. ";	
      }
      else if($result == 0){
        echo "Message Sent!";
      }
      else{	
        echo "Error Num ". $result . " was encountered!";
      }
    }

    public function itexmo($number, $message, $apicode){
      $ch = curl_init();
			$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			 curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			return curl_exec ($ch);
			curl_close ($ch);
    }
  }