<?php

  class Userid extends CI_Controller{
    public $user;

    function __construct(){
      parent::__construct();

      $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'superadmin/Userid_model' => 'userid_model'
      ));

      if(!$this->session->userdata('user')){
        redirect('login');
      }

      if($this->user->branch_id != 1){
        redirect('dashboard');
      }
    }

    public function index(){
      $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        // 'brands' => $this->userid_model->get_brands()->result(),
        'branches' => $this->userid_model->get_branches()->result(),
      );

      $this->render('superadmin/userid/index', $data);
    }

    public function get_users(){
      echo json_encode($this->userid_model->get_users()->result());
    }

    public function get_user($emp_id){
      echo json_encode($this->userid_model->get_user($emp_id)->row());
    }
    public function get_bname($emp_id){
      echo json_encode($this->userid_model->get_bname($emp_id)->row());
    }
    public function add_user(){

      $data = array(
        'emp_number' => $this->input->post('emp_number'),
        'emp_lastname' => $this->input->post('emp_lastname'),
         'emp_firstname' => $this->input->post('emp_firstname'),
          'emp_middlename' => $this->input->post('emp_middlename'),
          'emp_company' => $this->input->post('emp_company'),
        'emp_branch' => $this->input->post('emp_branch'),
        'emp_status' => $this->input->post('emp_status')

      
      );

      $this->userid_model->add_user($data);
      
    }

    public function delete_user($user_id){
      $this->userid_model->delete_user($user_id);
    }

    public function update_user($user_id){

      $data = array(

        
        'emp_lastname' => $this->input->post('emp_lastname'),
         'emp_firstname' => $this->input->post('emp_firstname'),
          'emp_middlename' => $this->input->post('emp_middlename'),
          'emp_company' => $this->input->post('emp_company'),
        'emp_branch' => $this->input->post('emp_branch'),
        'emp_status' => $this->input->post('emp_status')
      
      );

      $this->userid_model->update_user($user_id, $data);
      
    }

    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }
    
    public function check_branch_no_transaction_yesterday(){
      
      $export_excel = $this->input->post('export_excel');

      if(isset($export_excel)){

        $yesterday = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));

        $branch_no_transaction_yesterday = $this->userid_model->check_branch_no_transaction_yesterday($yesterday)->result();

        $excel = '
            <table class="table" bordered="1">
              <tr>
                <th style="text-align: center;">Store</th>
                <th style="text-align: center;">ASM</th>
              </tr>
        ';
  
        foreach($branch_no_transaction_yesterday as $value){
          $excel .= '
            <tr>
              <td>'.$value->branch_name.'</td>
              <td>'.$value->user_name.'</td>
            </tr>
          ';
        }
  
        $excel .= '</table>';
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=Not_Updated_CRM.xls");
        echo $excel;

      }


      // redirect('superadmin/user/index');
    }

    public function send_not_updated_crm(){
      $yesterday = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d")-1,date("Y")));

      $branch_no_transaction_yesterday = $this->userid_model->check_branch_no_transaction_yesterday($yesterday)->result();

      $message = 'NOT UPDATED CRM:'."\n"."\n";

      foreach($branch_no_transaction_yesterday as $key => $value){

        // if($key == 3){
        //   break;
        // }

        $message .= $value->branch_name."\n";
      }

      $result = $this->itexmo('09750987852', $message, 'TR-R2GRO718975_HP7NZ');

      if ($result == ""){
        echo "iTexMo: No response from server!!! Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.	Please CONTACT US for help. ";	
      }
      else if($result == 0){
        echo "Message Sent!";
      }
      else{	
        echo "Error Num ". $result . " was encountered!";
      }
    }

    public function itexmo($number, $message, $apicode){
      $ch = curl_init();
			$itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
			curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			 curl_setopt($ch, CURLOPT_POSTFIELDS, 
			          http_build_query($itexmo));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			return curl_exec ($ch);
			curl_close ($ch);
    }
  }