<?php

  class Attendance_tracker extends CI_Controller{
    public function __construct(){
      parent::__construct();

  
      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Settings_model' => 'settings'
      ));
      
      date_default_timezone_set('Asia/Manila');
    }

    public function index(){
      $user = $this->session->userdata('user');

      $data = array(
        'page_title' => __CLASS__,
        'user' => $user
 
      );
      
      $this->render('superadmin/attendance_tracker/index', $data);
    }

    public function search_name(){

        $name_query = $this->input->post('name_query');

        $start = $this->input->post('start');

        $end = $this->input->post('end');

        $data = $this->settings->search_name($name_query, $start, $end);
    
        echo json_encode($data);

    }

    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }
    

  }