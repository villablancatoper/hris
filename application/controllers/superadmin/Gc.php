<?php

    class Gc extends CI_Controller{

        function __construct(){

            parent::__construct();
      
      
      
            $this->load->model(array(
      
              'superadmin/Gc_model' => 'gc'
      
            ));
      
      
      
            if(!$this->session->userdata('user')){
      
              redirect('login');
      
            }
      
        }

        public function index(){

            $user = $this->session->userdata('user');
    
    
    
            $data = array(
    
                'page_title' => "GC",
    
                'user' => $user
    
            );
    
    
    
            $this->render('superadmin/gc/index', $data);
    
        }

        public function search_gcs(){
            $search_item = $this->input->get('search_item');

            echo json_encode($this->gc->search_gcs($search_item)->result());
        }

        public function render($page, $data){

            $this->load->view('templates/head', $data);
    
            $this->load->view('templates/header', $data);
    
            $this->load->view('templates/sidebar', $data);
    
            $this->load->view($page, $data);
    
            $this->load->view('templates/footer', $data);
    
        }
    }