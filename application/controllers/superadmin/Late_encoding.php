<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Late_encoding extends CI_Controller {
    public $user;


	public function __construct(){
      
      parent::__construct();;
     
		$this->load->model('Commission_model');

		 $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
      ));
       if(!$this->session->userdata('user')){
        redirect('login');
      } 

    }
   
	public function index(){

		$data = array(
	        'page_title' => 'Allow Late Encoding',
	        'user' => $this->user,
	        'brands' => $this->report->get_brands()->result(),
	        'branches' => $this->joborder->get_branches()->result(),
	        'changelogs' => $this->dashboard->get_changelogs(),
	        'standard_users' => $this->dashboard->get_standard_users() 
	      );

		$this->render('superadmin/late_encoding/index', $data);

	}

	public function fetch_branches(){
		if ($this->input->post('brand_id')) {
			echo $this->Commission_model->fetch_branches($this->input->post('brand_id'));
		}
	}


	public function render($page, $data){

      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }

    public  function get_lateencoding(){
    	echo json_encode($this->dashboard->get_lateencoding());
    
    }




}