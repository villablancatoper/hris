<?php

    class Service extends CI_Controller{
        public $user;

        function __construct(){
            parent::__construct();

            $this->user = $this->session->userdata('user');

            $this->load->model(array(
                'superadmin/Service_model' => 'service'
            ));

            if(!$this->session->userdata('user')){
                redirect('login');
            }

            if($this->user->branch_id != 1){
                redirect('dashboard');
            }
        }

        public function index(){
            $data = array(
              'page_title' => __CLASS__,
              'user' => $this->user,
              'brands' => $this->service->get_brands()->result()
            );
      
            $this->render('superadmin/service/index', $data);
        }

        public function search_services(){
            $brand_id = $this->input->get('brand_id');
            echo json_encode($this->service->search_services($brand_id)->result());
        }

        public function get_servicemix(){
            $productservice_code = $this->input->get('productservice_code');
            echo json_encode($this->service->get_servicemix($productservice_code)->result());
        }

        public function get_services_by_brand_id(){
            $brand_id = $this->input->get('brand_id');
            echo json_encode($this->service->get_services_by_brand_id($brand_id)->result());
        }

        public function get_items_by_brand_id(){
            $brand_id = $this->input->get('brand_id');
            echo json_encode($this->service->get_items_by_brand_id($brand_id)->result());
        }

        public function add_servicemix(){
            $productservice_id = $this->input->post('productservice_id');
            $servicemix_data = $this->input->post('servicemix_data');

            $this->service->add_servicemix($productservice_id, $servicemix_data);
        }

        public function process_servicemix(){
            $servicemix_id = $this->input->post('servicemix_id');
            $servicemix_quantity = $this->input->post('servicemix_quantity');
            $action = $this->input->post('action');

            if($action == 'edit'){  
                $this->service->update_servicemix_quantity($servicemix_id, $servicemix_quantity);
            }

            if($action == 'delete'){
                $this->service->delete_servicemix_item($servicemix_id, $servicemix_quantity);
            }

            $data = array(
                'servicemix_id' => $servicemix_id,
                'action' => $action,
            );

            echo json_encode($data);
        }

        public function add_servicemix_item(){
            $productservice_code = $this->input->post('productservice_code');
            $item = $this->input->post('item_id');
            $servicemix_quantity = $this->input->post('servicemix_quantity');
            $item_uom = $this->input->post('item_uom');

            $this->service->add_servicemix_item($productservice_code, $item, $servicemix_quantity, $item_uom);
        }

        public function render($page, $data){
            $this->load->view('templates/head', $data);
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view($page, $data);
            $this->load->view('templates/footer', $data);
        }
    }