<?php



  class Inventory extends CI_Controller{

    function __construct(){

      parent::__construct();



      $this->load->model(array(

        'Inventory_model' => 'inventory',

        'Dashboard_model' => 'dashboard',

        'Joborder_model' => 'joborder',

        'MSSQL_model' => 'mssql'

      ));



      if(!$this->session->userdata('user')){

        redirect('login');

      }

    }



    public function index(){

        $user = $this->session->userdata('user');



        $data = array(

            'page_title' => __CLASS__,

            'user' => $user,

            'brands' => $this->inventory->get_brands()->result(),

            'branches' => $this->inventory->get_branches()->result(),

            'changelogs' => $this->dashboard->get_changelogs()

        );



        $this->render('inventory/index', $data);

    }

    public function clone(){

      $user = $this->session->userdata('user');



      $data = array(

          'page_title' => __CLASS__,

          'user' => $user,

          'brands' => $this->inventory->get_brands()->result(),

          'branches' => $this->joborder->get_branches()->result(),

          'changelogs' => $this->dashboard->get_changelogs()

      );



      $this->render('inventory/clone', $data);

    }



    public function search_currentinventory(){

      $search_item = $this->input->get('search_item');

      $branch_id = $this->input->get('branch_id');



      if($this->session->userdata('user')->brand_id != 100007){

        $search_item = $this->input->get('search_item');

        $branch_id = $this->session->userdata('user')->branch_id;

      }

      echo json_encode($this->inventory->search_currentinventory($search_item, $branch_id)->result());

    }

    public function search_currentinventory_clone(){

      $search_item = $this->input->get('search_item');

      $branch_id = $this->input->get('branch_id');



      if($this->session->userdata('user')->brand_id != 100007){

        $search_item = $this->input->get('search_item');

        $branch_id = $this->session->userdata('user')->branch_id;

      }

      echo json_encode($this->inventory->search_currentinventory_clone($search_item, $branch_id)->result());

    }




    public function transfer(){

      $user = $this->session->userdata('user');



      $data = array(

          'page_title' => __CLASS__,

          'user' => $user,

          'branches' => $this->inventory->get_branches()->result(),

          'items' => $this->inventory->get_items()->result(),

          'changelogs' => $this->dashboard->get_changelogs()

      );



      $this->render('inventory/transfer', $data);

    }



    public function get_currentinventory(){

      $branch_id = null;



      if($this->session->userdata('user')->brand_id != 100007){

        $branch_id = $this->session->userdata('user')->branch_id;

      }



      echo json_encode($this->inventory->get_currentinventory($branch_id)->result());

    }



    public function get_item_by_item_id(){

      $item_id = $this->input->get('search_item');



      echo json_encode($this->inventory->get_item_by_item_id($item_id)->row());

    }



    public function transfer_items(){

      $data = array(

        'to' => $this->input->post('to'),

        'from' => $this->input->post('from'),

        'transfer_items' => $this->input->post('transfer_items')

      );



      $this->inventory->transfer_items($data);

    }



    public function get_transfer_requests(){

      echo json_encode($this->inventory->get_transfer_requests()->result());

    }



    public function get_branch_name(){

      $branch_id = $this->input->get('branch_id');

      echo json_encode($this->inventory->get_branch_name($branch_id)->row());

    }



    public function get_transfer_request(){

      $transferinventory_id = $this->input->get('search_item');

      echo json_encode($this->inventory->get_transfer_request($transferinventory_id)->row());

    }



    public function get_transfer_request_items(){

      $transferinventory_id = $this->input->get('search_item');

      echo json_encode($this->inventory->get_transfer_request_items($transferinventory_id)->result());

    }



    public function search_transfer_requests(){

      $search_item = $this->input->get('search_item');

      echo json_encode($this->inventory->search_transfer_requests($search_item)->result());

    }



    public function check_respond_availability(){

      $branch_id = $this->input->get('branch_id');
      $branch = $this->inventory->get_branch_by_branch_id($branch_id)->row();

      $transferinventory_status = $this->input->get('status');

      $can_respond = '<div class="row">

      <div class="col-md-12">

        <label for="transfer_request_remarks">Remarks</label>

        <textarea id="transfer_request_remarks" name="" id="" rows="3" class="form-control"></textarea>

      </div>

      <div class="col-md-12" style="margin-top: 10px !important;">

        <button id="transfer_request_approve_button" type="button" onclick="approve_request()" class="btn btn-success pull-right">Approve

        </button>

        <button id="transfer_request_decline_button" type="button" onclick="decline_request()" class="btn btn-danger pull-right" style="margin-right: 5px;">Decline

        </button>

      </div>

    </div>';



      $cannot_respond = '<div class="row">

        <div class="col-md-12">

          <label for="transfer_request_remarks">Remarks</label>

          <textarea id="transfer_request_remarks" name="" id="" rows="3" class="form-control" disabled="disabled"></textarea>

        </div>

        <div class="col-md-12" style="margin-top: 10px !important;">

          <button type="button" class="btn btn-success pull-right"disabled="disabled">Approve

          </button>

          <button type="button" class="btn btn-danger pull-right" disabled="disabled" style="margin-right: 5px;" >Decline

          </button>

        </div>

      </div>';



      $user = $this->session->userdata('user');



      if($user->user_role == 'Standard User' && $user->user_position == 'Area Manager'){

        if($transferinventory_status == 'For Approval of Head Operations' || $transferinventory_status == 'For Approval of Supply Chain'){



        }

        if($transferinventory_status == 'For Approval of Area Manager'){

          if($branch->branch_areamanager == $user->user_id){
            echo $can_respond;
          }
          else{
            // echo $user->user_id;
          }
        }

        if($transferinventory_status == 'Approved' || $transferinventory_status == 'Declined'){



        }

      }



      // if($user->user_role == 'Supply Chain' && $user->user_position == 'Supply Chain'){

      if($user->user_role == 'Supply Chain'){

        if($transferinventory_status == 'For Approval of Head Operations' || $transferinventory_status == 'For Approval of Area Manager'){

          echo $cannot_respond;

        }

        if($transferinventory_status == 'For Approval of Supply Chain'){

          echo $can_respond;

        }

        if($transferinventory_status == 'Approved' || $transferinventory_status == 'Declined'){

          

        }

      }



      if($user->user_role == 'Operations' && $user->user_position == 'Head Operations'){

        if($transferinventory_status == 'For Approval of Area Manager' || $transferinventory_status == 'For Approval of Supply Chain'){

          echo $cannot_respond;

        }

        if($transferinventory_status == 'For Approval of Head Operations'){

          echo $can_respond;

        }

        if($transferinventory_status == 'Approved' || $transferinventory_status == 'Declined'){

          

        }

      }

    }



    public function decline_request(){

      $user = $this->session->userdata('user');

      $status = 'Declined';

      $update_data = array();



      if($user->user_role == 'Head Operations' && $user->user_position == 'Department Manager'){

        $update_data = array(

          'transferinventory_approver2id' => $user->user_id,

          'transferinventory_approver2declinedate' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }



      if($user->user_role == 'Supply Chain' && $user->user_position == 'Department Manager'){

        $update_data = array(

          'transferinventory_approver3id' => $user->user_id,

          'transferinventory_approver3declinedate' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }



      if($user->user_role == 'Standard User' && $user->user_position == 'Area Manager'){

        $update_data = array(

          'transferinventory_approver1id' => $user->user_id,

          'transferinventory_approver1declinedate' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }



      $data = array(

        'user_id' => $this->session->userdata('user')->user_id,

        'transferinventory_id' => $this->input->post('transferinventory_id'),

        'transferinventoryhistory_remarks' => $this->input->post('remarks'),

        'transferinventoryhistory_status' => $status

      );



      $this->inventory->update_transferinventory_status($data['transferinventory_id'], $update_data);



      $this->inventory->insert_transferinventoryhistory($data);

    }



    public function approve_request(){

      $user = $this->session->userdata('user');

      $status = null;

      $update_data = array();

      if($user->user_role == 'Operations' && $user->user_position == 'Head Operations'){

        $status = 'For Approval of Supply Chain';



        $update_data = array(

          'transferinventory_approver2id' => $user->user_id,

          'transferinventory_approver2date' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }

      if($user->user_role == 'Supply Chain' && $user->user_position == 'Supply Chain'){

        $status = 'Approved';



        $update_data = array(

          'transferinventory_approver3id' => $user->user_id,

          'transferinventory_approver3date' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }



      if($user->user_role == 'Standard User' && $user->user_position == 'Area Manager'){

        $status = 'For Approval of Head Operations';



        $update_data = array(

          'transferinventory_approver1id' => $user->user_id,

          'transferinventory_approver1date' => date('Y-m-d'),

          'transferinventory_status' => $status

        );

      }



      $data = array(

        'user_id' => $user->user_id,

        'transferinventory_id' => $this->input->post('transferinventory_id'),

        'transferinventoryhistory_remarks' => $this->input->post('remarks'),

        'transferinventoryhistory_status' => $status

      );

      $from = $this->input->post('from');

      $to = $this->input->post('to');

      $transfer_items = $this->input->post('transfer_items');

      $log = "From: $from <br> To: $to <br>";

      for($i = 0; $i < count($transfer_items); $i++){

        $item_id = $transfer_items[$i][0];

        $this->db->where('item_id', $item_id);
        $item = $this->db->get('item')->row();

        // $log .= "<br> Item: $item->item_description <br>";
        
        // FROM

        $this->db->where('branch_id', $from);
        $this->db->where('item_id', $item_id);
        $currentinventory = $this->db->get('currentinventory')->row();

        $converted_quantity = $currentinventory->currentinventory_quantity - ($item->item_uomsize * floatval($transfer_items[$i][2]));

        // $log .= "UOM: $item->item_uomsize <br>";
        // $log .= "Value: ".floatval($transfer_items[$i][2]).' <br>';
        // $log .= "Converted Quantity: $converted_quantity <br> Update FROM<br>";

        $this->db->where('currentinventory_id', $currentinventory->currentinventory_id);
        $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));


        // TO

        $this->db->where('branch_id', $from);
        $this->db->where('item_id', $item_id);
        $currentinventory = $this->db->get('currentinventory')->row();

        $converted_quantity = 0 * floatval($transfer_items[$i][2]);

        if($currentinventory){

          $converted_quantity = $currentinventory->currentinventory_quantity - ($item->item_uomsize * floatval($transfer_items[$i][2]));

          // $log .= "Converted Quantity: $converted_quantity <br> Updated TO <br>";

          $this->db->where('currentinventory_id', $currentinventory->currentinventory_id);
          $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));

        }
        else{
          
          $currentinventory_data = array(
            'branch_id' => $from,
            'item_id' => $item_id,
            'currentinventory_quantity' => $converted_quantity,
            'currentinventory_uom' => $item->item_uom
          );

          $this->db->insert('currentinventory', $currentinventory_data);

          // $log .= "Converted Quantity: $converted_quantity <br> <br> Inserted TO <br>";

        }

      }



      $this->inventory->update_transferinventory_status($data['transferinventory_id'], $update_data);



      $this->inventory->insert_transferinventoryhistory($data);

      // echo $log;

    }



    public function get_transferinventoryhistory($transferinventory_id){

      // $transferinventory_id = $this->input->post('transferinventory_id');

      echo json_encode($this->inventory->get_transferinventoryhistory($transferinventory_id)->result());



    }



    public function search_transferinventory(){

      $search_item = $this->input->get('search_item');

      $branch_id = $this->input->get('branch_id');



      if($this->session->userdata('user')->brand_id != 100007){

        $search_item = $this->input->get('search_item');

        $branch_id = $this->session->userdata('user')->branch_id;

      }

      echo json_encode($this->inventory->search_transferinventory($search_item, $branch_id)->result());

      // echo $search_item;

    }



    public function receive(){

      $user = $this->session->userdata('user');



      $data = array(

          'page_title' => __CLASS__,

          'user' => $user,

          'branches' => $this->joborder->get_branches()->result(),

          'brands' => $this->inventory->get_brands()->result(),

          'changelogs' => $this->dashboard->get_changelogs()

      );



      if($user->user_role == "Supply Chain"){

        $this->render('inventory/view_dr_sc', $data);

      }

      else{

        $this->render('inventory/view_dr_branch', $data);

      }

    }



    public function get_dr_items(){

      $user = $this->session->userdata('user');

      $branch_id = null;

      

      if($user->user_role != "Supply Chain"){

        $branch_id = $user->branch_id;

      }



      echo json_encode($this->inventory->get_dr_items($branch_id)->result());

    }



    public function create_dr_sap(){

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        $dr = array(

          'branch_id' => $this->input->post('branch_id'),

          'deliveryreceipt_date' => date('Y-m-d', strtotime($this->input->post('date'))),

          'deliveryreceipt_remarks' => $this->input->post('remarks'),

          'deliveryreceipt_status' => 'Waiting for delivery.'

        );



        $dr_details = array(

          'dr_items' => $this->input->post('dr_items'),

          'dr_type' => $this->input->post('dr_type')

        );



        $this->inventory->create_dr_sap($dr, $dr_details);

        // echo var_dump($dr_details);

      }

      else{

        $user = $this->session->userdata('user');



        $data = array(

            'page_title' => __CLASS__,

            'user' => $user,

            'branches' => $this->joborder->get_branches()->result(),

            'changelogs' => $this->dashboard->get_changelogs()

        );



        $this->render('inventory/create_dr_sap', $data);
      }		
    }		

    public function create_dr_manual(){		
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {		
        $dr = array(		
          'branch_id' => $this->input->post('branch_id'),		
          'deliveryreceipt_date' => date('Y-m-d', strtotime($this->input->post('date'))),		
          		
          'deliveryreceipt_packed' => date('Y-m-d', strtotime($this->input->post('packed'))),		
          'deliveryreceipt_pickup' => date('Y-m-d', strtotime($this->input->post('pickup'))),		
          'deliveryreceipt_no' => $this->input->post('no'),		
          'deliveryreceipt_remarks' => $this->input->post('remarks'),		
          'deliveryreceipt_status' => 'Waiting for delivery.'		
        );		
        $dr_items = $this->input->post('dr_items');		
        $this->inventory->create_dr_manual($dr, $dr_items);
      }		
      else{		
        $user = $this->session->userdata('user');		
        $data = array(		
            'page_title' => __CLASS__,		
            'user' => $user,		
            'branches' => $this->joborder->get_branches()->result(),		
            'changelogs' => $this->dashboard->get_changelogs()		
        );		
        $this->render('inventory/create_dr_manual', $data);		


      }

    }



    public function receive_dr($deliveryreceipt_id = null){

      

      if($_SERVER['REQUEST_METHOD'] == 'POST') {

        $dr_items = $this->input->post('items');

        $deliveryreceipt_id = $this->input->post('deliveryreceipt_id');

        $deliveryreceipt_receivedate = $this->input->post('deliveryreceipt_receivedate');

        $user_id = $this->session->userdata('user')->user_id;


        $this->inventory->receive_dr($deliveryreceipt_receivedate, $user_id, $deliveryreceipt_id, $dr_items);

      }

      else{

        $dr = null;

        $drd = null;



        if($deliveryreceipt_id){

          $dr = $this->inventory->get_dr($deliveryreceipt_id)->row();

          $drd = $this->inventory->get_drd($deliveryreceipt_id)->result();

        }



        $user = $this->session->userdata('user');



        $data = array(

            'page_title' => __CLASS__,

            'user' => $user,

            'dr' => $dr,

            'drd' => $drd,

            'changelogs' => $this->dashboard->get_changelogs()

        );



        $this->render('inventory/receive_dr', $data);

      }

    }



    public function get_pdn1_items_by_whs_code(){

      $branch_id = $this->input->get('branch_id');

      $table = $this->input->get('table');

      $branch = $this->inventory->get_branch_by_branch_id($branch_id)->row();



      // echo var_dump($branch);



      if($table == "PDN1"){

        $this->mssql->set_db($branch->branch_dbname);

        echo json_encode($this->mssql->get_pdn1_items_by_whs_code($branch->branch_whscode)->result());

      }

      else{

        $this->mssql->set_db($branch->branch_dbname);

        echo json_encode($this->mssql->get_wtr1_items_by_whs_code($branch->branch_whscode)->result());

      }

    }

    

    public function get_pdn_item_by_doc_entry_on_pdn1(){

      $this->mssql->set_db($this->session->userdata('db_name'));

      $doc_entry = $this->input->get('doc_entry');

      echo json_encode($this->mssql->get_pdn_item_by_doc_entry_on_pdn1($doc_entry)->row());

    }



    public function get_pdn_item_by_doc_entry_on_wtr1(){

      $this->mssql->set_db($this->session->userdata('db_name'));

      $doc_entry = $this->input->get('doc_entry');

      echo json_encode($this->mssql->get_pdn_item_by_doc_entry_on_wtr1($doc_entry)->row());

    }



    public function check_item_availability(){

      $branch_id = $this->input->get('branch_id');

      $item_code = $this->input->get('item_code');

      $doc_entry = $this->input->get('doc_entry');



      echo $this->inventory->check_item_availability($branch_id, $item_code, $doc_entry);

    }



    public function get_dr(){

      $deliveryreceipt_id = $this->input->get('deliveryreceipt_id');

      echo json_encode($this->inventory->get_dr($deliveryreceipt_id)->row());

    }



    public function get_drd(){

      $deliveryreceipt_id = $this->input->get('deliveryreceipt_id');

      echo json_encode($this->inventory->get_drd($deliveryreceipt_id)->result());

    }



    public function delete_dr(){

      $deliveryreceipt_id = $this->input->post('deliveryreceipt_id');

      $this->inventory->delete_dr($deliveryreceipt_id);

    }



    public function search_drs(){

      $data = array(

        'brand_id' => $this->input->get('brand'),

        'branch_id' => $this->input->get('branch'),

        'start' => date('Y-m-d', strtotime($this->input->get('start'))),

        'end' => date('Y-m-d', strtotime($this->input->get('end')))

      );



      echo json_encode($this->inventory->search_drs($data)->result());

    }

    public function get_brand_items_by_branch_id(){		
      $branch_id = $this->input->get('branch_id');		
      echo json_encode($this->inventory->get_brand_items_by_branch_id($branch_id)->result());		
    }		
    public function get_vendors_by_branch_id(){		
      $branch_id = $this->input->get('branch_id');		
      echo json_encode($this->inventory->get_vendors_by_branch_id($branch_id)->result());		
    }

    public function render($page, $data){

        $this->load->view('templates/head', $data);

        $this->load->view('templates/header', $data);

        $this->load->view('templates/sidebar', $data);

        $this->load->view($page, $data);

        $this->load->view('templates/footer', $data);

    }

    public function update_currentinventory(){
      $currentinventory_id = $this->input->post('currentinventory_id');
      $currentinventory_quantity = $this->input->post('currentinventory_quantity');

      $this->inventory->update_currentinventory($currentinventory_id, $currentinventory_quantity);
    }

    public function update_currentinventory_clone(){
      $currentinventory_id = $this->input->post('currentinventory_id');
      $currentinventory_quantity = $this->input->post('currentinventory_quantity');

      $this->inventory->update_currentinventory_clone($currentinventory_id, $currentinventory_quantity);
    }

  }