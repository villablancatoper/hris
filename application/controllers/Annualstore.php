<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Annualstore extends CI_Controller {



    public $user;



	public function __construct(){

      

      	parent::__construct();;



		 $this->user = $this->session->userdata('user');



      $this->load->model(array(

        'Dashboard_model' => 'dashboard',

        'Joborder_model' => 'joborder',

        'Inventory_model' => 'inventory',

        'Report_model' => 'report',

        'Annualstore_model' => 'annualstore',

      ));

       if(!$this->session->userdata('user')){

        redirect('login');

      }  



    }

	public function index(){

	 $data = array(

        'page_title' => 'Report',

        'user' => $this->user,

        'brands' => $this->report->get_brands()->result(),

        'branches' => $this->joborder->get_branches()->result(),

        // 'changelogs' => $this->dashboard->get_changelogs()

      );

	

		// $data['branches'] = $this->totalsales->get_branches();

		// echo "<pre>";

		// print_r($data);

		// echo "</pre>";

		$this->render('report/annualstore', $data);

	

		

	}


    public function add_user(){

      $data = array(

        'branch_id' => $this->input->post('branch_id'),
        
       'storetarget_month' => $this->input->post('storetarget_month'),

        'storetarget_original'=> $this->input->post('storetarget_original'),

         'storetarget_updatedsalesforecast' => $this->input->post('storetarget_updatedsalesforecast'),
        'storetarget_averagesalesperformance' => $this->input->post('storetarget_averagesalesperformance')
      
      );

      $this->annualstore->add_user($data);
      
    }

  public function test(){
    $data = array(
      'branch_id' => 24, 
      'storetarget_month' => 'November 2019', 

      'storetarget_original' => 111111, 

      'storetarget_updatedsalesforecast' => 22222, 

      'storetarget_averagesalesperformance' => 33333

    );
    $this->annualstore->add_user($data);
  }

 
	public function fetch_branches(){

		// if ($this->input->post('brand_id')) {

		// 	echo $this->CommissionModel->fetch_branches($this->input->post('brand_id'));

		// }

	}
    public function update_currentinventory_clone(){
      $storetarget_id = $this->input->post('storetarget_id');
      $storetarget_month = $this->input->post('storetarget_month');
      $storetarget_original = $this->input->post('storetarget_original');
      $Storetarget_forecast = $this->input->post('Storetarget_forecast');
      $Storetarget_average = $this->input->post('Storetarget_average');
      
      $this->annualstore->update_currentinventory_clone($storetarget_id, $storetarget_month,$storetarget_original, $Storetarget_forecast,$Storetarget_average);
    }


	public function compute(){










		$branch_id = $this->input->post('branch_id');

		$start = $this->input->post('start');

		$end = $this->input->post('end');

    $branch = $this->input->post('branch');

    $brand_id = $this->input->post('brand_id');

		//

     $year = $this->input->post('start');

    

   

    $month_list=array('January','February','March','April','May','June','July','August','September','October','November','December' );

    for ($i=0;$i<12;$i++)
    {

        $month=new DateTime($month_list[$i]." ".$year);
        $month_start=$month->format('Y-m-d');
        $month_end=$month->format('Y-m-t');
        $start_end=array('start'=>$month_start,'end'=>$month_end,'monthname'=>$month_list[$i]);
        $month_array[$month_list[$i]]=$start_end;
    }
    $datas=array('months'=>$month_array,'branch_id'=>$branch_id,'branch'=>$branch,'brand_id'=>$brand_id);
    echo json_encode( $this->annualstore->get($branch_id, $start, $end , $branch, $brand_id, $datas));
   //echo json_encode($this->annualstore->get($data));





		// $data['sum'] = $this->totalsales->get_sum($branch_id, $start, $end);

		// foreach ($data['sum'] as $row) {

		// 	$row->transaction_date = "TOTAL";

		// }

		// array_push($data['sales'], $data['sum']);

		

	}

	   public function get_categories_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());

    }



    public function get_productservices_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());

    }

public function delete_user($storetarget_id){
      $this->annualstore->delete_user($storetarget_id);
    }

    public function get_branches_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());

    }

public function fetch_branchess(){
    if ($this->input->post('brand_id')) {
      echo $this->annualstore->fetch_branchess($this->input->post('brand_id'));
    }
  }

     public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

    }





}