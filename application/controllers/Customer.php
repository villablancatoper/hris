<?php



  class Customer extends CI_Controller{

    public function __construct(){

      parent::__construct();



      $this->load->model(array(

        'Customer_model' => 'customer',

        'Dashboard_model' => 'dashboard'

      ));



      if(!$this->session->userdata('user')){

        redirect('login');

      }

    }



    public function entry(){

      $user = $this->session->userdata('user');



      $data = array(

        'page_title' => __CLASS__,

        'user' => $user,

        'locations' => $this->customer->get_locations()->result(),

        'occupations' => $this->customer->get_occupations()->result(),

        'ages' => $this->customer->get_ages()->result(),

        'changelogs' => $this->dashboard->get_changelogs()

      );



      if($_SERVER['REQUEST_METHOD'] == 'POST'){



      }

      else{

        $this->render('customer/index', $data);

      }

    }

    public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

    }



    public function get_customers(){

      $draw = intval($this->input->get("draw"));

      $start = intval($this->input->get("start"));

      $length = intval($this->input->get("length"));



      $customers = $this->customer->get_customers();



      $data = array();





      foreach($customers->result() as $customer){



        $data[] = array(

          $customer->customer_id,

          $customer->customer_firstname,

          $customer->customer_lastname,

          $customer->customer_email

        );



      }



      $output = array(

        "draw" => $customers->num_rows(),

        "recordsTotal" => $customers->num_rows(),

        "recordsFiltered" => $customers->num_rows(),

        "data" => $data

      );



      echo json_encode($output);

    }



    public function get_customer($id){

      $customer = $this->customer->get_customer($id);

      echo json_encode($customer);

    }



    public function update_customer($id){

      $data = array(

        'customer_firstname' => $this->input->post('customer_firstname'),

        'customer_lastname' => $this->input->post('customer_lastname'),

        'customer_gender' => $this->input->post('customer_gender'),

        'customer_phoneno' => $this->input->post('customer_phoneno'),

        'customer_email' => $this->input->post('customer_email'),

        'customer_mobileno' => $this->input->post('customer_mobileno'),

        'location_id' => $this->input->post('location_id'),

        'occupation_id' => $this->input->post('occupation_id'),

        'customer_birthday' => $this->input->post('customer_birthday'),

        'age_id' => $this->input->post('age_id'),

        'customer_aboutus' => $this->input->post('customer_hear_about_us') == 'Others' ? $this->input->post('customer_specified') : $this->input->post('customer_hear_about_us'),

      );



      echo $this->customer->update_customer($data, $id);

    }



    public function delete_customer($id){

      $this->customer->delete_customer($id);

    }



    public function add_customer(){

      $data = array(

        'customer_firstname' => $this->input->post('add_customer_firstname'),

        'branch_id' => $this->session->userdata('user')->branch_id,

        'customer_lastname' => $this->input->post('add_customer_lastname'),

        'customer_gender' => $this->input->post('add_customer_gender'),

        'customer_phoneno' => $this->input->post('add_customer_phoneno'),

        'customer_email' => $this->input->post('add_customer_email'),

        'customer_mobileno' => $this->input->post('add_customer_mobileno'),

        'location_id' => $this->input->post('add_customer_location_id'),

        'occupation_id' => $this->input->post('add_customer_occupation_id'),

        'customer_birthday' => $this->input->post('add_customer_customer_birthday'),

        'age_id' => $this->input->post('add_customer_age_id'),

        'customer_aboutus' => $this->input->post('add_hear_about_us') == 'Others' ? $this->input->post('add_specify') : $this->input->post('add_hear_about_us'),

      );



      echo $this->customer->add_customer($data);

    }



    public function search_customers_new(){

      $search_item = $this->input->get('search_item');

      echo json_encode($this->customer->search_customers_new($search_item)->result());

    }



    public function search_customers_old(){

      $search_item = $this->input->get('search_item');

      echo json_encode($this->customer->search_customers_old($search_item)->result());

    }



    public function get_customer_old($customerolddata_id){

      echo json_encode($this->customer->get_customer_old($customerolddata_id)->row());

    }



    public function is_customer_data_transferred($customerolddata_id){

      echo $this->customer->is_customer_data_transferred($customerolddata_id);

    }



    public function transfer_customer_old_data($C_IDNO){

      $branch_id = $this->session->userdata('user')->branch_id;

      $data = array(

        'customer_firstname' => $this->input->post('transfer_customer_firstname'),

        'branch_id' => $this->session->userdata('user')->branch_id,

        'customer_lastname' => $this->input->post('transfer_customer_lastname'),

        'customer_gender' => $this->input->post('transfer_customer_gender'),

        'customer_phoneno' => $this->input->post('transfer_customer_phoneno'),

        'customer_email' => $this->input->post('transfer_customer_email'),

        'customer_mobileno' => $this->input->post('transfer_customer_mobileno'),

        'location_id' => $this->input->post('transfer_customer_location_id'),

        'occupation_id' => $this->input->post('transfer_customer_occupation_id'),

        'customer_birthday' => $this->input->post('transfer_customer_birthday'),

        'age_id' => $this->input->post('transfer_customer_age_id'),

        'customer_foottraffic' => $this->customer->get_customer_old_status($C_IDNO, $branch_id)

      );

      

      $this->customer->transfer_customer_old_data($C_IDNO, $data);

    }

  }