<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commission extends CI_Controller {
    public $user;


	public function __construct(){
      
      parent::__construct();;
     
		$this->load->model('Commission_model');

		 $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
      ));
       if(!$this->session->userdata('user')){
        redirect('login');
      } 

    }
      public function get_categories_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());
    }

    public function get_productservices_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());
    }

    public function get_branches_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());
    }

	public function index(){

	$data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        'changelogs' => $this->dashboard->get_changelogs()
      );

		$this->render('report/commission_view', $data);

	}

	public function fetch_branches(){
		if ($this->input->post('brand_id')) {
			echo $this->Commission_model->fetch_branches($this->input->post('brand_id'));
		}
	}

	public function compute(){
		// get from ajax post 
		$brand_id = $this->input->post('brand_id');
		$branch_id = $this->input->post('branch_id');
		$commission_type = $this->input->post('commission_type');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$start1 = $this->input->post('start1');
		$end1 = $this->input->post('end1');
		// get from ajax post 

		$data = $this->Commission_model->get_commission($brand_id, $branch_id, $start, $end, $commission_type, $start1, $end1);
		
		$data = $this->formatFunc($data, $brand_id, $commission_type);
		echo json_encode($data);
		
	}
	// end of public function 

	public function formatFunc($data, $brand_id, $commission_type){
			foreach ($data as $row) {
				if ($row->total_retail == NULL) {
					$row->total_retail = "0.00";
				}
				if ($row->total_service == NULL) {
					$row->total_service = "0.00";
				}
				// print_r($data);
				
				if ($brand_id == 100004 || $brand_id == 100005 || $brand_id == 100008 || $brand_id == 100009) {
					if ($row->total_service >= 110000 && $row->total_service < 120000) {
						$row->service_incentives = 1000;
					}elseif ($row->total_service >= 120000 && $row->total_service < 130000) {
						$row->service_incentives = 2000;
					}elseif ($row->total_service >= 130000 && $row->total_service < 140000) {
						$row->service_incentives = 3000;
					}elseif ($row->total_service >= 140000 && $row->total_service < 150000) {
						$row->service_incentives = 4000;
					}elseif ($row->total_service >= 150000) {
						$row->service_incentives = 5000;
					}else{
						$row->service_incentives = "0.00";
					}
				}elseif ($brand_id == 100002 || $brand_id == 100006) {

					if ($row->total_service >= 95000.00 && $row->total_service < 100000.00) {
						$row->service_incentives = 700;
					}elseif ($row->total_service >= 100000.00 && $row->total_service < 105000.00) {
						$row->service_incentives = 1000;
					}elseif ($row->total_service >= 105000 && $row->total_service < 110000) {
						$row->service_incentives = 1200;
					}elseif ($row->total_service >= 110000 && $row->total_service < 120000) {
						$row->service_incentives = 1500;
					}elseif ($row->total_service >= 120000 && $row->total_service < 130000) {
						$row->service_incentives = 1800;
					}elseif ($row->total_service >= 130000 && $row->total_service < 140000 ) {
						$row->service_incentives = 2000;
					}elseif ($row->total_service >= 140000 && $row->total_service < 150000 ) {
						$row->service_incentives = 2500;
					}elseif ($row->total_service >= 150000) {
						$row->service_incentives = 3000;
					}else{
						$row->service_incentives = "0.00";
					}

				}elseif ($brand_id == 100003 && $commission_type == 'service') {
				    
					// $net_less_product_cost = (($row->technical_sales / 1.12) * 0.15);
				// 	$assist_sp_deduction = ($row->haircut_men_sp_deduc * 8) + ($row->haircut_childWomen_sp_deduc * 10) + ($row->technical_deduc_count * 20) + ($row->other_sp_deduction * 5);

					// $row->service_incentives = round((float)(($row->haircut_men / 1.12) * 0.3) + (($row->haircut_childWomen / 1.12) * 0.3)  + ((($row->technical_sales / 1.12) - $net_less_product_cost) * 0.3) + (($row->other_sales / 1.12) * 0.3), 2);
					// $row->service_incentives = round((float)$row->service_incentives - $assist_sp_deduction, 2);
					$row->total_retail = 0;
				    if($row->sp_level == 'BARBER'){
				        $net_less_product_cost = round((($row->technical_sales / 1.12) * 0.15),2);
    					// $assist_sp_deduction = ($row->haircut_men_sp_deduc * 8) + ($row->haircut_childWomen_sp_deduc * 10) + ($row->technical_deduc_count * 20) + ($row->other_sp_deduction * 5);
    					$assist_sp_deduction = 0;
    					$row->service_incentives = round((float)(($row->haircut_men / 1.12) * 0.3) + (($row->haircut_childWomen / 1.12) * 0.3)  + ((($row->technical_sales / 1.12) - $net_less_product_cost) * 0.3) + (($row->other_sales / 1.12) * 0.3), 2);
    					$row->service_incentives = round((float)$row->service_incentives - $assist_sp_deduction, 2);
				    }else{
				        $row->service_incentives = 0;
				    }
				}elseif ($brand_id == 100003 && $commission_type == 'all') {
				    if($row->sp_level == 'BARBER'){
				        $net_less_product_cost = round((($row->technical_sales / 1.12) * 0.15),2);
    					// $assist_sp_deduction = ($row->haircut_men_sp_deduc * 8) + ($row->haircut_childWomen_sp_deduc * 10) + ($row->technical_deduc_count * 20) + ($row->other_sp_deduction * 5);
    					$assist_sp_deduction = 0;
    					$row->service_incentives = round((float)(($row->haircut_men / 1.12) * 0.3) + (($row->haircut_childWomen / 1.12) * 0.3)  + ((($row->technical_sales / 1.12) - $net_less_product_cost) * 0.3) + (($row->other_sales / 1.12) * 0.3), 2);
    					$row->service_incentives = round((float)$row->service_incentives - $assist_sp_deduction, 2);
				    }else{
				        $row->service_incentives = 0;
				    }
				} 

				$row->otc_commission = round((float)(($row->total_retail / 1.12) * 0.1), 2);
				if ($row->otc_commission == 0) {
					$row->otc_commission = "0.00";
				}else{
					$row->otc_commission;
				}
				$row->total_commission = round((float)($row->otc_commission + $row->service_incentives), 2);
				if ($row->total_commission == 0) {
					$row->total_commission = "0.00";
				}
				// unset($row->total_sales);
				
			}
			foreach($data as $elementKey => $element) {
					    foreach($element as $valueKey => $value) {
					        if($valueKey == 'total_commission' && $value == 0){
					            //delete this particular object from the $array
					            unset($data[$elementKey]);
					     } 
				}
			}
			$new_data = array_values($data);
			return $new_data;
	}
	// end of function 

	public function test(){
		$brand_id = 25;
		$branch_id = 25;
  		$start = "2019-10-06";
  		$end = "2019-10-20";
  		$commission_type = 'otc';
  		$start1 = "";
  		$end1 = "";
		$temp = $this->Commission_model->get_commission($brand_id, $branch_id, $start, $end, $commission_type, $start1, $end1);
		// $data = $this->formatFunc($temp, $brand_id, $commission_type);
		echo "<pre>";
		print_r($temp);
		echo "</pre>";
	
	}

	public function test2(){
	
		$data = $this->Commission_model->get();
		// foreach ($data as $row) {
		// 	echo $row->branch_name;
		// }
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
	public function render($page, $data){

      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }



}