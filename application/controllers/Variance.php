<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Variance extends CI_Controller {


    public $user;
	public function __construct(){
      	parent::__construct();;
		 $this->user = $this->session->userdata('user');
      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
        'Variance_model' => 'variance',
      ));
       if(!$this->session->userdata('user')){
        redirect('login');
      }  
    }
	public function index(){

	 $data = array(

        'page_title' => 'Variance Analysis',
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        // 'changelogs' => $this->dashboard->get_changelogs()

      );
		// $data['branches'] = $this->totalsales->get_branches();
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		$this->render('report/variance_view', $data);

	}


	public function fetch_branches(){
		// if ($this->input->post('brand_id')) {
		// 	echo $this->CommissionModel->fetch_branches($this->input->post('brand_id'));
		// }
    $budget_date = "November 2019";
    $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime(preg_replace('/d+/u', '', $budget_date)))-1).'-15'));
    echo $wall_to_wall_date;

	}

  public function get_variance(){
    $branch_id = $this->input->post("branch_id");
    $beginning = $this->input->post("beginning");
    $delivered_start = $this->input->post("delivered_start");
    $delivered_end = $this->input->post("delivered_end");
    $ending = $this->input->post("ending");
    $data = $this->variance->get_variance($branch_id, $beginning, $delivered_start, $delivered_end, $ending );
    echo json_encode($data);
  }
  public function test(){
    $branch_id = 161;
    $beginning = "2019-08-30";
    $delivered_start = "2019-09-01";
    $delivered_end = "2019-09-30";
    $data = $this->variance->get_variance($branch_id, $beginning, $delivered_start, $delivered_end);
    echo "<pre>";
    print_r($data);
    echo "</pre>";

  }



  public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

  }





}