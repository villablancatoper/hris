<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Asmsales extends CI_Controller {



    public $user;



	public function __construct(){

      

      	parent::__construct();;



		 $this->user = $this->session->userdata('user');



      $this->load->model(array(

        'Dashboard_model' => 'dashboard',

        'Joborder_model' => 'joborder',

        'Inventory_model' => 'inventory',

        'Report_model' => 'report',

        'Asmsales_model' => 'asmsales',

      ));

       if(!$this->session->userdata('user')){

        redirect('login');

      }  



    }

	public function index(){

	 $data = array(

        'page_title' => 'Report',

        'user' => $this->user,

        'brands' => $this->report->get_brands()->result(),

        'branches' => $this->joborder->get_branches()->result(),

        'changelogs' => $this->dashboard->get_changelogs()

      );

	

		// $data['branches'] = $this->totalsales->get_branches();

		// echo "<pre>";

		// print_r($data);

		// echo "</pre>";

		$this->render('report/asmsales', $data);

	

		

	}





	public function fetch_branches(){

		// if ($this->input->post('brand_id')) {

		// 	echo $this->CommissionModel->fetch_branches($this->input->post('brand_id'));

		// }

	}
    public function test(){

	

// 		$start = $this->input->post('start');

// 		$end = $this->input->post('end');
        $start = "2019-10-01";
        $end = "2019-10-30";


		$data = $this->asmsales->get($start, $end);
        print_r($data);


	
	}


	public function compute(){

	

		$start = $this->input->post('start');

		$end = $this->input->post('end');



		$data = $this->asmsales->get($start, $end);
        // print_r($data);


		echo json_encode($data);

	}

	   public function get_categories_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());

    }



    public function get_productservices_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());

    }



    public function get_branches_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());

    }



     public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

    }





}