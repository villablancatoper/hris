<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Salesforecast extends CI_Controller {



    public $user;



	public function __construct(){

    parent::__construct();;

		$this->user = $this->session->userdata('user');

    $this->load->model(array(
      'Dashboard_model' => 'dashboard',
      'Joborder_model' => 'joborder',
      'Inventory_model' => 'inventory',
      'Report_model' => 'report',
      'Salesforecast_model' => 'salesforecast',
    ));

    if(!$this->session->userdata('user')){
      redirect('login');
    }  

  }

	public function index(){

	  $data = array(

      'page_title' => 'Report',

      'user' => $this->user,

      'brands' => $this->report->get_brands()->result(),

      'branches' => $this->joborder->get_branches()->result(),

      // 'changelogs' => $this->dashboard->get_changelogs()

    );

		// $data['branches'] = $this->totalsales->get_branches();

		// echo "<pre>";

		// print_r($data);

		// echo "</pre>";

		$this->render('budgetfolder/salesforecast', $data);
	}


  public function add_storetarget(){
    $data = array(
      'branch_id' => $this->input->post('branch_id'),
      'storetarget_month' => $this->input->post('storetarget_month'),
      'storetarget_original'=> $this->input->post('storetarget_original'),
      'storetarget_updatedsalesforecast' => $this->input->post('storetarget_updatedsalesforecast'),
      'storetarget_averagesalesperformance' => $this->input->post('storetarget_averagesalesperformance'),

      'storetarget_lastmonth_performance' => $this->input->post('storetarget_lastmonth_performance'),
      'last_year_sales' => $this->input->post('last_year_sales'),
      'last_year_walkin' => $this->input->post('last_year_walkin'),
      'last_year_regular' => $this->input->post('last_year_regular'),
      'target_walkin' => $this->input->post('target_walkin'),
      'target_regular' => $this->input->post('target_regular')
    );

    $this->salesforecast->add_storetarget($data);
  }

  public function test(){
    $data = array(
      'branch_id' => 24, 
      'storetarget_month' => 'November 2019', 
      'storetarget_original' => 111111, 
      'storetarget_updatedsalesforecast' => 22222, 
      'storetarget_averagesalesperformance' => 33333
    );
    $this->salesforecast->add_user($data);
  }

 
	public function fetch_branches(){
		// if ($this->input->post('brand_id')) {
		// 	echo $this->CommissionModel->fetch_branches($this->input->post('brand_id'));
		// }
  }
  
  public function update_storetarget(){
    $storetarget_id = $this->input->post('storetarget_id');
    $storetarget_month = $this->input->post('storetarget_month');
    $storetarget_original = $this->input->post('storetarget_original');
    $Storetarget_forecast = $this->input->post('Storetarget_forecast');
    $Storetarget_average = $this->input->post('Storetarget_average');

    $st_last_sales = $this->input->post('st_last_sales');
    $st_last_walkin = $this->input->post('st_last_walkin');
    $st_last_regular = $this->input->post('st_last_regular');
    $st_target_walkin = $this->input->post('st_target_walkin');
    $st_target_regular = $this->input->post('st_target_regular');
    $st_last_performance = $this->input->post('st_last_performance');
    
    $this->salesforecast->update_storetarget(
     $storetarget_id,
     $storetarget_month,
     $storetarget_original, 
     $Storetarget_forecast,
     $Storetarget_average,
    
     $st_last_sales,
     $st_last_walkin,
     $st_last_regular,
     $st_target_walkin,
     $st_target_regular,
     $st_last_performance
    );
  }

	public function compute(){
		$branch_id = $this->input->post('branch_id');
		$start = $this->input->post('start');
		$end = $this->input->post('end');
    $branch = $this->input->post('branch');
    $brand_id = $this->input->post('brand_id');
    $data = $this->salesforecast->get($branch_id, $start, $end , $branch, $brand_id);
    
		// $data['sum'] = $this->totalsales->get_sum($branch_id, $start, $end);
		// foreach ($data['sum'] as $row) {
		// 	$row->transaction_date = "TOTAL";
		// }
		// array_push($data['sales'], $data['sum']);
		echo json_encode($data);
  }
  
	public function get_lastmonth_performance(){
		$branch_id = $this->input->post('branch_id');
		$start = $this->input->post('start');
		$end =  $this->input->post('end');
    $branch = $this->input->post('branch');
    $brand_id = $this->input->post('brand_id');
    $data = $this->salesforecast->get_lastmonth_performance($branch_id, $start, $end , $branch, $brand_id);
    
	
		echo json_encode($data);
  }
  public function get_target_walkin(){
		
    $brand_id = $this->input->post('brand_id');
    $data = $this->salesforecast->get_target_walkin( $brand_id);
    
	
		echo json_encode($data);
	}

  public function get_categories_ho(){
    $brand_id = $this->input->get('brand_id');

    echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());
  }



  public function get_productservices_ho(){
    $brand_id = $this->input->get('brand_id');

    echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());
  }

  public function delete_user($storetarget_id){
    $this->salesforecast->delete_user($storetarget_id);
  }

  public function get_branches_ho(){
    $brand_id = $this->input->get('brand_id');

    echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());
  }

  public function fetch_branchess(){
    if ($this->input->post('brand_id')) {
      echo $this->salesforecast->fetch_branchess($this->input->post('brand_id'));
    }
  }

  public function render($page, $data){
    $this->load->view('templates/head', $data);
    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view($page, $data);
    $this->load->view('templates/footer', $data);
  }
}