<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Z_index extends CI_Controller {



    public $user;

	public function __construct(){

  
      	parent::__construct();;

		 $this->user = $this->session->userdata('user');

      $this->load->model(array(

        'Dashboard_model' => 'dashboard',

        'Joborder_model' => 'joborder',

        'Inventory_model' => 'inventory',

        'Report_model' => 'report',

        'Z_index_model' => 'totalsales',

      ));

       if(!$this->session->userdata('user')){

        redirect('login');

      }  

    }

	public function index(){

	 $data = array(

        'page_title' => 'Report',

        'user' => $this->user,

        'brands' => $this->report->get_brands()->result(),

        'branches' => $this->joborder->get_branches()->result(),

        'changelogs' => $this->dashboard->get_changelogs()

      );

		$this->render('report/z_index', $data);

	}




	public function compute(){

		$branch_id = $this->input->post('branch_id');

		$start = $this->input->post('start');

		$end = $this->input->post('end');

    $branch = $this->input->post('branch');

    $brand_id = $this->input->post('brand_id');

		$data = $this->totalsales->get($branch_id, $start, $end, $brand_id);


		echo json_encode($data);

	}

	   public function get_categories_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());

    }



    public function get_productservices_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());

    }

    public function fetch_branches(){

        if ($this->input->post('brand_id')) {
    
          echo $this->totalsales->fetch_branches($this->input->post('brand_id'));
    
        }

    }

    public function get_branches_ho(){

      $brand_id = $this->input->get('brand_id');



      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());

    }

    public function submitCashVariance(){
      $cash_card = $this->input->post('cash_card');

      if ($cash_card == 'cash') {
        $data = array(
              'brand_id' => $this->input->post('brand_id'), 
              'branch_id' => $this->input->post('branch_id'), 
              'transaction_date' => $this->input->post('transaction_date'),
              'total_sales' => $this->input->post('transaction_totalsales'),
              'expected_sales' => $this->input->post('transaction_totalsales'),
              'total_cash' => $this->input->post('transaction_paymentcash'),
              'expected_cash' => $this->input->post('expected_cash'),
              'cash_deposit' => 0,
              'deposit_date' => NULL,
              'total_card' => $this->input->post('transaction_paymentcard'),
              'expected_card' => $this->input->post('transaction_paymentcard'),
              'cash_variance' => $this->input->post('variance'),
              'card_variance' => 0,
              'cash_status' => 'Cash with Variance',
              'remarks' => '',
        );
      }
        $this->totalsales->submitCashVariance($data, $cash_card);
    }


    public function submitCardVariance(){
      $cash_card = $this->input->post('cash_card');

      if($cash_card == 'card'){
        $data = array(
              'brand_id' => $this->input->post('brand_id'), 
              'branch_id' => $this->input->post('branch_id'), 
              'transaction_date' => $this->input->post('transaction_date'),
              'total_sales' => $this->input->post('transaction_totalsales'),
              'expected_sales' => $this->input->post('transaction_totalsales'),
              'total_cash' => $this->input->post('transaction_paymentcash'),
              'expected_cash' => $this->input->post('transaction_paymentcash'),
              'cash_deposit' => 0,
              'deposit_date' => NULL,
              'total_card' => $this->input->post('transaction_paymentcard'),
              'expected_card' => $this->input->post('transaction_paymentcard'),
              'cash_variance' => 0,
              'card_variance' => $this->input->post('card_variance'),
              'card_status' => 'Card with Variance',
              'remarks' => '',
        );
      }
        $this->totalsales->submitCardVariance($data, $cash_card);
   }

  public function submitTally(){
    $cash_card = $this->input->post('cash_card');

    if ($cash_card == 'cash') {
      $cash_status = "Approved";
      $data = array(
            'brand_id' => $this->input->post('brand_id'), 
            'branch_id' => $this->input->post('branch_id'), 
            'transaction_date' => $this->input->post('transaction_date'),
            'total_sales' => $this->input->post('transaction_totalsales'),
            'expected_sales' => $this->input->post('transaction_totalsales'),
            'total_cash' => $this->input->post('transaction_paymentcash'),
            'expected_cash' => $this->input->post('transaction_paymentcash'),
            'cash_deposit' => 0,
            'deposit_date' => NULL,
            'total_card' => $this->input->post('transaction_paymentcard'),
            'expected_card' => $this->input->post('transaction_paymentcard'),
            'cash_variance' => 0,
            'card_variance' => 0,
            'remarks' => '',
            'cash_status' => $cash_status,
      );
    }elseif($cash_card = 'card'){
      $card_status = "Approved";
      $data = array(
            'brand_id' => $this->input->post('brand_id'), 
            'branch_id' => $this->input->post('branch_id'), 
            'transaction_date' => $this->input->post('transaction_date'),
            'total_sales' => $this->input->post('transaction_totalsales'),
            'expected_sales' => $this->input->post('transaction_totalsales'),
            'total_cash' => $this->input->post('transaction_paymentcash'),
            'expected_cash' => $this->input->post('transaction_paymentcash'),
            'cash_deposit' => 0,
            'deposit_date' => NULL,
            'total_card' => $this->input->post('transaction_paymentcard'),
            'expected_card' => $this->input->post('transaction_paymentcard'),
            'cash_variance' => 0,
            'card_variance' => 0,
            'remarks' => '',
            'card_status' => $card_status
      );
    }
      $this->totalsales->submitTally($data, $cash_card);
  } // end function


     public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

    }





}