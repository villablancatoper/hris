<?php

  class Dashboard extends CI_Controller{
    public function __construct(){
      parent::__construct();

      if(!$this->session->userdata('user')){
        redirect('login');
      }

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'superadmin/User_model' => 'user'
      ));
      
      date_default_timezone_set('Asia/Manila');
    }

    public function index(){
      $user = $this->session->userdata('user');

      $data = array(
        'page_title' => __CLASS__,
        'user' => $user,
        'changelogs' => $this->dashboard->get_changelogs()
      );
      
      $this->render('dashboard/index', $data);
    }
    
    public function get_branches(){
	    echo $this->dashboard->get_branches();
	}
    
    public function get_current_server_time(){
        echo date('His');
    }

    public function get_current_server_time_and_date(){
        echo date('F d, Y H:i:s');
    }

    public function get_current_server_date(){
      echo date('Ymd');
    }

    public function get_productservices_by_brand_id(){
      $brand_id = $this->session->userdata('user')->brand_id;

      echo json_encode($this->dashboard->get_productservices_by_brand_id($brand_id)->result());
    }

    public function get_productretails_by_brand_id(){
      $brand_id = $this->session->userdata('user')->brand_id;

      echo json_encode($this->dashboard->get_productretails_by_brand_id($brand_id)->result());
    }

    public function change_password(){
      $old_password = $this->input->post('old_password');
      $new_password = $this->input->post('new_password');

      echo $this->dashboard->change_password($old_password, $new_password);
    }

    public function welcome(){
      $user = $this->session->userdata('user');

      $data = array(
        'page_title' => __CLASS__,
        'user' => $user,
        'changelogs' => $this->dashboard->get_changelogs()
      );

      $this->render('dashboard/welcome_message', $data);
    }

    public function get_user_notice_status(){

      $user_id = $this->session->userdata('user')->user_id;

      echo $this->dashboard->get_user_notice_status($user_id);
    }

    public function update_user_notice_status(){
      $user_id = $this->session->userdata('user')->user_id;

      $this->dashboard->update_user_notice_status($user_id);
    }
    
    public function transfer_branch(){
      $date = new DateTime();
      $date = $date->format('Y-m-d');
      $user_id = $this->input->post("transfer_user_id");
      $branch_id = $this->input->post("transfer_branch");

      $message['response'] = $this->dashboard->transfer_branch($user_id, $branch_id, $date);
      
      echo json_encode($message);
    }

    public function get_user_late_encoding_status(){
      return $this->user->get_user_late_encoding_status()->row()->user_lateencode;
    }

    public function allow_lateencode(){
      $this->dashboard->allow_lateencode($this->input->post('branch_id'), $this->input->post('lateencode_date'), $this->input->post('requestor'), $this->input->post('reason'));
    }


    public function render($page, $data){
      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
    }
  }