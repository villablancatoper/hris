<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticketing extends CI_Controller {
    public $user;


	public function __construct(){
      
      parent::__construct();;
     
		$this->load->model('Ticket_model');

		 $this->user = $this->session->userdata('user');

      $this->load->model(array(
        'Dashboard_model' => 'dashboard',
        'Joborder_model' => 'joborder',
        'Inventory_model' => 'inventory',
        'Report_model' => 'report',
      ));
      if(!$this->session->userdata('user')){
        redirect('login');
      } 
     date_default_timezone_set('Asia/Manila');
    }
    public function get_categories_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_categories_by_brand_id($brand_id)->result());
    }

    public function get_productservices_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_productservices_by_brand_id($brand_id)->result());
    }

    public function get_branches_ho(){
      $brand_id = $this->input->get('brand_id');

      echo json_encode($this->report->get_branches_by_branch_id($brand_id)->result());
    }

	public function index(){
    $user = $this->session->userdata('user');
    $department = $user->user_department;
    $user_id = $user->user_id;
    $user_position = $user->user_position;
    if ($department == 'FACILITES') {
        $category = $this->Ticket_model->facilities_category();
    }
    if ($user_position == 'Area Manager') {
      $branches = $this->Ticket_model->asm_branches($user_id);
    }else{
      $branches = $this->Ticket_model->get_branches();
    }


	 $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $branches,
        'facilities_category' => $this->Ticket_model->facilities_category(),
        'mis_category' => $this->Ticket_model->mis_category(),
        'asm_branches' => $this->Ticket_model->asm_branches($user_id),
        // 'store_branches' => $this->Ticket_model->store_branches($user_id),
        'changelogs' => $this->dashboard->get_changelogs()
      );
		$this->render('ticketing/dashboard', $data);
	}


	public function render($page, $data){

      $this->load->view('templates/head', $data);
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view($page, $data);
      $this->load->view('templates/footer', $data);
  }

  public function get_ticket_list(){

    $user = $this->session->userdata('user');
    $department = $user->user_department;
    $branch_id = $user->branch_id;
    $user_role = $user->user_role;
    $user_position = $user->user_position;
    $user_id = $user->user_id;
   
 
    
    if ($department == 'FACILITIES') {
        $temp = $this->Ticket_model->facility_ticket();
    }elseif ($user_role == 'Standard User' && $user_position != 'Area Manager') {
        $temp = $this->Ticket_model->get_ticket_by_store($branch_id);
    }elseif ($user_position == 'Area Manager') {
        $temp = $this->Ticket_model->asm_ticket($user_id);
    }else{
        $temp = $this->Ticket_model->get_ticket_list();
    }
    $data = $this->get_aging($temp);
    foreach($data as $row){
        $dt = new DateTime($row->ticket_submit_date_time);
        $dt2 = new DateTime($row->ticket_last_update_date);
        $row->ticket_submit_date_time = $dt->format('M'." ".'d'.","." ".'Y g:i a');
        $row->ticket_last_update_date = $dt2->format('M'." ".'d'.","." ".'Y g:i a');
    }
    echo json_encode($data);
  }
  
    public function fiter_ticket_by_dept(){
      $user = $this->session->userdata('user');
      $user_position = $user->user_position;
      $branch_name = $user->branch_name;
        $dept = $this->input->post('dept');
    
      if ($user_position == 'Standard User') {
        if ($dept == 'IT') {
          $data = $this->Ticket_model->store_mis_ticket($branch_name);
        } else {
          $data = $this->Ticket_model->store_facility_ticket($branch_name);
        }
      } else {
        if ($dept == 'IT') {
          $data = $this->Ticket_model->mis_ticket();
        } else {
          $data = $this->Ticket_model->facility_ticket();
        }
      }


        
        echo json_encode($this->get_aging($data));
    }

  public function sort_ticket(){
    $user = $this->session->userdata('user');
    $branch_id = $user->branch_id;
    $department = $user->user_department;
    $sort_index = $this->input->post("sort_index");
    $user_role = $user->user_role;
    $user_position = $user->user_position;
    $user_id = $user->user_id;

    if ($branch_id == 1 && $department == 'IT' && $sort_index == 'All') {
        $temp = $this->Ticket_model->get_ticket_list();
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 210  && $sort_index == 'All') {
        $temp = $this->Ticket_model->facility_ticket();
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 210) {
        $temp = $this->Ticket_model->facility_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 1 && $department == 'IT' && $sort_index == 'New') {
        $sort_index = 'New';
        $temp = $this->Ticket_model->mis_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 1 && $department == 'IT' && $sort_index == 'In-progress') {
        $sort_index = 'In-progress';
        $temp = $this->Ticket_model->mis_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 1 && $department == 'IT' && $sort_index == 'Completed') {
        $sort_index = 'Completed';
        $temp = $this->Ticket_model->mis_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($user_role == 'Standard User' && $user_position != 'Area Manager') {
        $temp = $this->Ticket_model->store_sorting($branch_id, $sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($user_position == 'Area Manager' && $sort_index != 'All') {
        $temp = $this->Ticket_model->asm_sorting($user_id, $sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($user_position == 'Area Manager' && $sort_index == 'All') {
        $temp = $this->Ticket_model->asm_ticket($user_id);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }elseif ($branch_id == 198 && $sort_index == 'All') {
        $temp = $this->Ticket_model->general_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }else{
        $temp = $this->Ticket_model->mis_sorting($sort_index);
        $data = $this->get_aging($temp);
        echo json_encode($data);
    }
    // $data = $this->Ticket_model->sort_ticket($branch_id, $sort_index);
  }


  public function submit_ticket(){
      $user = $this->session->userdata('user');
      $ticket_requestor = $user->user_name;
      $job_title = $user->user_role;
      $data = $this->input->post();
      $data = array(
             'ticket_brand_branch' => $this->input->post('site_store'),
             'ticket_requestor' => $ticket_requestor,
             'ticket_requestor_post' =>$job_title,
             'ticket_category' => $this->input->post('concern_type'),
             'ticket_title' => $this->input->post('concern_details'),
             'ticket_status' => 'New',
             'ticket_last_update_details' => 'Ticket created',
             'ticket_submit_date_time' => date('Y-m-d h:i:s'),
             'ticket_last_update_date' => date('Y-m-d h:i:s')
          );
      $this->Ticket_model->submit_ticket($data);
      $this->Ticket_model->ticket_updates();
  }

  public function accept_ticket($ticket_id){
    $user = $this->session->userdata('user');
    $ticket_updater = $user->user_name;
    $ticket_id = $this->input->post('ticket_id');
    $ticket_update = 'Ticket accepted';
    $data = array('ticket_id' => $ticket_id,
           'ticket_updater' => $ticket_updater,
           'ticket_update' => $ticket_update
        );
    $this->Ticket_model->insert_latest_updates($data);
    $this->Ticket_model->accept_ticket($data);
    // $this->Ticket_model->mis_set_assignee($ticket_id,$ticket_updater);
  }

  public function close_ticket($ticket_id){
    $user = $this->session->userdata('user');
    $ticket_requestor = $user->user_name;
    $ticket_id = $this->input->post('ticket_id');
    $ticket_status = 'Completed';
    $remarks = $this->input->post('remarks')."\n";
    $date = $this->input->post('date');


    $this->Ticket_model->close_ticket($ticket_id, $ticket_status);
    $data = array('ticket_id' => $ticket_id,
           'ticket_updater' => $ticket_requestor,
           'ticket_update' => 'Ticket completed and now closed'."<br>"."Remarks :".$remarks."<br>"."Date Closed :".$date
        );
    $this->Ticket_model->insert_latest_updates($data);
    $this->Ticket_model->complete_ticket($ticket_id, $date);
  }
 

  public function assign_ticket(){
    $user = $this->session->userdata('user');
    $itmanager = $user->full_name;
    $ticket_id = $this->input->post('ticket_id');
    $full_name = $this->input->post('ticket_assignee');
    $ticket_update = "Reassigned to"." ".$full_name;
    $data = array('ticket_id' => $ticket_id,
            'ticket_updater' => $itmanager,
            'ticket_update' => $ticket_update
           );
    $this->Ticket_model->mis_set_assignee($ticket_id, $full_name);
    $this->Ticket_model->insert_latest_updates($data);
  }

  public function update(){
    $user = $this->session->userdata('user');
    $ticket_requestor = $user->user_name;
    $ticket_id = $this->input->post('ticket_id');
    $ticket_update = $this->input->post('update_details');
    $data = array(
           'ticket_id' => $ticket_id,
           'ticket_updater' => $ticket_requestor,
           'ticket_update' => $ticket_update
           );
    $this->Ticket_model->insert_latest_updates($data);
  }


  public function test(){
   $now = date("Y-m-d H:i:s");
   
  
   $now = new DateTime($now);
   $newDateString = $now->format('M'." ".'d'.","." ".'Y g:i a');

    echo $newDateString;
  }

  public function status($ticket_id){
    // $data['ticket_updates'] = $this->Ticket_model->get_ticket_status($ticket_id);
    // $data['last_update'] = $this->Ticket_model->get_last_update($ticket_id);
    // $data['ticket_status'] = $this->Ticket_model->status_completed($ticket_id);
    // $data['mis_list'] = $this->User_model->mis_list();
    $data = array(
        'page_title' => __CLASS__,
        'user' => $this->user,
        'brands' => $this->report->get_brands()->result(),
        'branches' => $this->joborder->get_branches()->result(),
        // 'changelogs' => $this->dashboard->get_changelogs()
        'ticket_updates' =>  $this->Ticket_model->get_ticket_status($ticket_id),
        'last_update' => $this->Ticket_model->get_last_update($ticket_id),
        'ticket_status' => $this->Ticket_model->status_completed($ticket_id)
      );

    $this->render('ticketing/ticket_status', $data);


  }

    public function search_overview(){
     // $start = "2019-01-01";

     $selected_month = $this->input->post("month");
     $selected_year = $this->input->post("year");
     $department = $this->input->post("department");
     $selected_month = new DateTime("$selected_year-$selected_month-01");

     $start = $selected_year.'-'.'01'.'-'.'01';


     $selected_month_start = $selected_month->format('Y-m-d');
     $selected_month_end = $selected_month->format('Y-m-t');

    
     if ($department == 'IT') {
        $data = $this->Ticket_model->search_overview_it($start, $department, $selected_month_start, $selected_month_end);
     } else {
        $data = $this->Ticket_model->search_overview_facilities($start, $department, $selected_month_start, $selected_month_end);
     }  
     
     $aging = $this->get_aging($data['list']);
     $output = array('list' => $aging, 'category' => $data['category']);
     echo json_encode($output);

  }

    public function get_aging($data){
    
        $now = date("Y-m-d H:i:s");


     
        foreach ($data as $ticket_row) {

           
              if ($ticket_row->ticket_status != 'Completed') {
                
                  $start = new DateTime($ticket_row->ticket_submit_date_time);
                  $end = new DateTime($now);
                  // otherwise the  end date is excluded (bug?)
                  // $end->modify('+1 day');
          
                  $interval = $end->diff($start);
          
                  // total days
                  $days = $interval->days;
          
                  // create an iterateable period of date (P1D equates to 1 day)
                  $period = new DatePeriod($start, new DateInterval('P1D'), $end);
          
                  // best stored as array, so you can add more than one
                  // $holidays = array('2012-09-07');
          
                  foreach($period as $dt) {
                      $curr = $dt->format('D');
          
                      // substract if Saturday or Sunday
                      if ($curr == 'Sun') {
                          $days--;
                      }
                  }
                  $ticket_row->ticket_aging = $days;
              } else {

                  $start = new DateTime($ticket_row->ticket_submit_date_time);
                  $end = new DateTime($ticket_row->ticket_date_completed);
                  $end->modify('+1 day');

                  $interval = $end->diff($start);

                  $days = $interval->days;

                  $period = new DatePeriod($start, new DateInterval('P1D'), $end);

                  foreach($period as $dt) {
                      $curr = $dt->format('D');
          
                      // substract if Saturday or Sunday
                      if ($curr == 'Sun') {
                          $days--;
                      }
                  }
                  $ticket_row->ticket_aging = $days;

              }


        }
     
        return $data;
    }





}


