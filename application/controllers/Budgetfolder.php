<?php

require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
// call iofactory instead of xlsx writer
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Color;

date_default_timezone_set('Asia/Manila');

class Budgetfolder extends CI_Controller{
    public $user;

    function __construct(){
        parent::__construct();

        $this->user = $this->session->userdata('user');

        $this->load->model(array(
            'Budgetfolder_model' => 'budgetfolder',
            'Report_model' => 'report'
        ));

        if(!$this->session->userdata('user')){
            redirect('login');
        }
    }

    public function index(){
        $data = array(
          'page_title' => __CLASS__,
          'user' => $this->user,
          // 'brands' => $this->user_model->get_brands()->result(),
          'branches' => $this->budgetfolder->get_branches()->result(),
        );
  
        $this->render('budgetfolder/index', $data);
    }

    public function generate_budgetfolder(){
        $branch_id = $this->input->get('branch_id');

        echo json_encode($this->budgetfolder->generate_budgetfolder($branch_id));
    }

    public function generate_budgetfolder_total(){
        $branch_id = $this->input->get('branch_id');

        echo $this->budgetfolder->generate_budgetfolder_total($branch_id);
    }

    public function store_budgetfolder($branch_id = NULL){
        $data = array(
            'page_title' => __CLASS__,
            'user' => $this->user
        );

        if($this->user->brand_id != 100007){

            $data['currentinventory'] = $this->budgetfolder->get_currentinventory()->result();

            $this->render('budgetfolder/store_budgetfolder_branch', $data);

        }

        else{

            // if($this->user->branch_id == 1){
            //     $data['brands'] = $this->budgetfolder->get_brands()->result();
            //     $this->render('budgetfolder/store_budgetfolder_ho', $data);
            // }
            
            $data['brands'] = $this->budgetfolder->get_brands()->result();
            $this->render('budgetfolder/store_budgetfolder_ho', $data);
        }

    }

    public function render($page, $data){
        $this->load->view('templates/head', $data);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function process_budgetfolder_inventory(){

        // $this->budgetfolder->update_currentinventory_quantity($ids);
        // $currentinventory_quantity = $this->input->post('currentinventory_quantity');
        // $action = $this->input->post('action');

        // if($action == 'edit'){  
        //     $this->budgetfolder->update_currentinventory_quantity($currentinventory_id, $currentinventory_quantity);
        // }

        // if($action == 'delete'){
        //     // $this->budgetfolder->delete_currentinventory_item($currentinventory_id, $currentinventory_quantity);
        // }

        // $data = array(
        //     'currentinventory_id' => $currentinventory_id,
        //     'action' => $action,
        // );

    }

    public function submit_budgetfolder(){
        $ids = $this->input->post('ids');
        $dateupdated = $this->input->post('dateupdated');
        $this->budgetfolder->submit_budgetfolder($ids, $dateupdated);
        
        // var_dump($ids);
        // var_dump($dateupdated);
    }

    public function check_brand_budgetfolder_availability(){
        $brand_id = $this->input->get('brand_id');
        $month = $this->input->get('month');
        $batch_no = $this->input->get('batch_no');
        $year = $this->input->get('year');

        echo json_encode($this->budgetfolder->check_brand_budgetfolder_availability($brand_id, $month, $batch_no, $year));
    }

    public function get_budgetfolder_inventory(){
        $branch_id = $this->input->get('branch_id');
        $wall_to_wall_date = $this->input->get('wall_to_wall_date');

        echo json_encode($this->budgetfolder->get_budgetfolder_inventory($branch_id, $wall_to_wall_date)->result());
    }

    public function create_budgetfolder($branch_id, $wall_to_wall_date, $month_and_year){

        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $budgetfolder_data = array(
                'user_id' => $this->input->post('user_id'),
                'branch_id' => $this->input->post('branch_id'),
                'budgetfolder_budgetdate' => $this->input->post('budgetfolder_budgetdate'),
                'budgetfolder_materialbeginningtotal' => $this->input->post('budgetfolder_materialbeginningtotal'),
                'budgetfolder_materialordertotal' => $this->input->post('budgetfolder_materialordertotal'),
                'budgetfolder_materialmaxallowance' => $this->input->post('budgetfolder_materialmaxallowance'),
                'budgetfolder_otcbeginningtotal' => $this->input->post('budgetfolder_otcbeginningtotal'),
                'budgetfolder_otcordertotal' => $this->input->post('budgetfolder_otcordertotal'),
                'budgetfolder_otcmaxallowance' => $this->input->post('budgetfolder_otcmaxallowance'),
                'budgetfolder_submitted' => date('Y-m-d')
            );

            $consumables = $this->input->post('consumables');
            $otcs = $this->input->post('otcs');

            $this->budgetfolder->create_budgetfolder($budgetfolder_data, $consumables, $otcs);

            // var_dump($budgetfolder_data);
        }
        else{

            $number = 1;

            if(date('d', strtotime($wall_to_wall_date)) != 15){
                $number = 2;
            }

            $budgetfolder_date = date('F', strtotime( date('Y', strtotime($wall_to_wall_date)).'-'.(date('m', strtotime($wall_to_wall_date))+$number).'-'.date('d', strtotime($wall_to_wall_date)) )) .' '.date('Y', strtotime($wall_to_wall_date));

            $data = array(
                'page_title' => __CLASS__,
                'user' => $this->user,
                'consumables' => $this->budgetfolder->get_budgetfolder_inventory_consumables_manual($branch_id, $wall_to_wall_date)->result(),
                'otcs' => $this->budgetfolder->get_budgetfolder_inventory_otcs($branch_id, $wall_to_wall_date)->result(),
                'store_target' => $this->budgetfolder->get_sales_forecast($branch_id, $wall_to_wall_date),
                'branch' => $this->budgetfolder->get_branch($branch_id)->row(),
                'budget_date' => $budgetfolder_date,
                'budgetfolder_data' => $this->budgetfolder->get_budgetfolder_data($branch_id, $budgetfolder_date)->row(),
                'budgetfolder_data_manual' => $this->budgetfolder->budgetfolder_autoorder($branch_id, $budgetfolder_date)
            );

            $this->render('budgetfolder/create_budgetfolder', $data);
        }

    }

    public function test(){
        print_r($this->budgetfolder->get_sales_forecast(147, '2019-11-30'));
    }

    public function another_test(){
        echo $this->budgetfolder->another_test()->result();
    }

    public function insert_new_items(){
        echo $this->budgetfolder->insert_new_items();
    }

    public function confirm_budgetfolder_inventory(){
        $branch_id = $this->input->post('branch_id');
        $wall_to_wall_date = $this->input->post('wall_to_wall_date');

        $this->budgetfolder->confirm_budgetfolder_inventory($branch_id, $wall_to_wall_date);
    }

    public function check_encoding_availability(){
        $date_as_of = $this->input->get('date_as_of');

        echo $this->budgetfolder->check_encoding_availability($date_as_of);
    }

    public function view_submitted_budgetfolder_consumables(){
        $branch_id = $this->input->get('branch_id');
        $budget_date = $this->input->get('budget_date');
        $wall_to_wall_date = urldecode($this->input->get('wall_to_wall_date'));

        echo json_encode($this->budgetfolder->view_submitted_budgetfolder_consumables($branch_id, $budget_date, $wall_to_wall_date));
    }

    public function view_submitted_budgetfolder_otcs(){
        $branch_id = $this->input->get('branch_id');
        $budget_date = urldecode($this->input->get('budget_date'));
        $wall_to_wall_date = urldecode($this->input->get('wall_to_wall_date'));

        echo json_encode($this->budgetfolder->view_submitted_budgetfolder_otcs($branch_id, $budget_date, $wall_to_wall_date));
    }

    public function add_items_currentinventory(){
        echo $this->budgetfolder->add_items_currentinventory();
    }

    public function add_items_currentinventory_copy(){
        echo $this->budgetfolder->add_items_currentinventory_copy();
    }

    public function budgetfolder(){
        $data = array(
            'page_title' => __CLASS__,
            'user' => $this->user
        );
    
        $this->render('budgetfolder/view_budgetfolder_ho', $data);
    }

    public function get_budgetfolders(){
        $month = $this->input->get('month');
        $batch_no = $this->input->get('batch_no');

        echo json_encode($this->budgetfolder->get_budgetfolders($month, $batch_no)->result());
    }
    
    public function show_topsheet(){
        $month = $this->input->post("month");
        $year = $this->input->post("year");
        $batch_no = $this->input->post("batch_no");
        $folders = $this->input->post("folders");

        $data = $this->budgetfolder->show_topsheet($month, $year, $batch_no, $folders);
        echo json_encode($data);
        
    }

    public function fetch_folders(){
        $month = $this->input->get('month');
        $batch_no = $this->input->get('batch_no');

        echo ($this->budgetfolder->fetch_folders($month, $batch_no));
    }

    public function view_budgetfolder_summary(){
        $folder = $this->input->get('folder');
        $month = $this->input->get('month');
        $year = $this->input->get('year');
        $batch = $this->input->get('batch');

        echo json_encode($this->budgetfolder->view_budgetfolder_summary($folder, $month, $batch, $year));
    }

    public function view_budgetfolder_orders(){
        $folder = $this->input->get('folder');
        $month = $this->input->get('month');
        $batch = $this->input->get('batch');

        echo json_encode($this->budgetfolder->view_budgetfolder_orders($folder, $month, $batch));

        // echo $this->budgetfolder->view_budgetfolder_orders($folder, $month, $batch);
    }
    
    public function view_budgetfolder_inventory(){
        $folder = $this->input->get('folder');
        $year = $this->input->get('year');
        $month = $this->input->get('month');
        $batch = $this->input->get('batch');

        echo json_encode($this->budgetfolder->view_budgetfolder_inventory($folder, $month, $batch, $year));
      
    }
    
    public function generate_excel($folder, $po, $supplier, $month, $batch){
        $now = date("F d, Y H:i:s");




        $budgetfolder_month = date("F", strtotime(date("Y")."-"."$month")).' '.date('Y');
        
        $convert1 = str_replace('%28', '(', $supplier);
        $convert2 = str_replace('%29', ')', $convert1);

        // Getting data from the model

        $data = $this->budgetfolder->generate_excel(urldecode($folder), urldecode($convert2), $budgetfolder_month, $batch); 
        
        // $data['branches'] = ['HS ABREEZA', 'HS ILOILO', 'HS SM CEBU'];

        $static_columns = ['Item Name', 'Color', 'Shade', 'Model', 'Unit Cost', 'UOM'];

        $top_style = [
            'font' => [
                'bold' => true,
            ],
            // 'alignment' => [
            //     'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            // ],
            // 'borders' => [
            //     'top' => [
            //         'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            //     ],
            // ],
            // 'fill' => [
            //     'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
            //     'rotation' => 90,
            //     'startColor' => [
            //         'argb' => 'FFA0A0A0',
            //     ],
            //     'endColor' => [
            //         'argb' => 'FFFFFFFF',
            //     ],
            // ],
        ];

        $header_style = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $number_cell_style = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_RIGHT,
            ]
        ];

        $text_cell_style = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
            ]
        ];

        $bottom_header_style = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $bottom_number_style = [
            'font' => [
                'bold' => true,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                ]
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_LEFT,
            ]
        ];

        // make new spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Instanciate new worksheet and define name

        $worksheet1 = new Worksheet($spreadsheet, $po);

        // Put worksheet again to spreadsheet

        $spreadsheet->addSheet($worksheet1, 0);

        // Remove default worksheet

        $sheetIndex = $spreadsheet->getIndex(
            $spreadsheet->getSheetByName('Worksheet')
        );
        $spreadsheet->removeSheetByIndex($sheetIndex);

        $spreadsheet->getSheet(0)->getStyle('A2')->applyFromArray($top_style);
        $spreadsheet->getSheet(0)->getStyle('B2')->applyFromArray($top_style);

        $spreadsheet->getSheet(0)->getStyle('A4')->applyFromArray($top_style);
        $spreadsheet->getSheet(0)->getStyle('B4')->applyFromArray($top_style);

        $spreadsheet->getSheet(0)->getStyle('A5')->applyFromArray($top_style);
        $spreadsheet->getSheet(0)->getStyle('B5')->applyFromArray($top_style);

        // set the default font
        $spreadsheet->getDefaultStyle()
                    ->getFont()
                    ->setName('Calibri')
                    ->setSize(12);

        // Top info start
                    
        $spreadsheet->getSheet(0)
                    ->setCellValue('A2', 'Budget folder month')
                    ->setCellValue('B2', $budgetfolder_month);

        
        $spreadsheet->getSheet(0)
                    ->setCellValue('A4', 'Vendor Name')
                    ->setCellValue('B4', urldecode($convert2));

        $spreadsheet->getSheet(0)
                    ->setCellValue('A5', 'PO')
                    ->setCellValue('B5', $po);

        // Top info end

        // Branch header start
        $col = 'G';
        $prev = '';
        $next = '';


        foreach($data['branches'] as $value){
        
            $prev = $col;

            $col++;

            $next = $col;

            $spreadsheet->getSheet(0)->mergeCells(($prev.(8)).':'.($next.(8)));
            $spreadsheet->getSheet(0)->mergeCells(($prev.(7)).':'.($next.(7)));

            $spreadsheet->getSheet(0)->getStyle($prev.(7))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)->getStyle($next.(7))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)->getStyle($prev.(8))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)->getStyle($next.(8))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($prev.(7), $value->branch_company);
            $spreadsheet->getSheet(0)
            ->setCellValue($prev.(8), $value->branch_name);

            # QTY and AMOUNT (Branches)
            
            $spreadsheet->getSheet(0)->getStyle($prev.(9))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)->getStyle($next.(9))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($prev.(9), 'Qty') #QTY
            ->setCellValue($next.(9), 'Amount'); #Amount

            $col++;
        }

        $prev = $col;

        $col++;

        $next = $col;

        $spreadsheet->getSheet(0)->mergeCells(($prev.(8)).':'.($next.(8)));

        $spreadsheet->getSheet(0)->getStyle($prev.(8))->applyFromArray($header_style);
        $spreadsheet->getSheet(0)->getStyle($next.(8))->applyFromArray($header_style);
        $spreadsheet->getSheet(0)
        ->setCellValue($prev.(8), 'TOTAL')->getStyle($prev.(8))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

        # QTY and AMOUNT (TOTAL)

        $spreadsheet->getSheet(0)->getStyle($prev.(9))->applyFromArray($header_style);
        $spreadsheet->getSheet(0)->getStyle($next.(9))->applyFromArray($header_style);
        $spreadsheet->getSheet(0)
        ->setCellValue($prev.(9), 'Qty') #QTY
        ->setCellValue($next.(9), 'Amount'); #Amount


        for($i = 'A'; $i <= $col; $i++){
            $spreadsheet->getSheet(0)->getColumnDimension($i)->setAutoSize(true);
        }

        // Branch header end

        // Static Column start

        $col = 'A';

        for($i = 0; $i < count($static_columns); $i++){

            $spreadsheet->getSheet(0)->getStyle($col.(9))->applyFromArray($header_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(9), $static_columns[$i]);
            $col++;
        }

        // Static Column end

        // Item rows start

        $branches_result = array();

        foreach($data['branches'] as $value){
        
            $branches_result[$value->branch_name] = 0;

        }

        $counter = 0;

        $row_qty_total = 0;

        $row_amount_total = 0;

        $last_row = 0;

        $items_qty = 0;

        foreach($data['items'] as $key1 => $item){

            $col = 'A';

            $row_qty_total = 0;

            $row_amount_total = 0;

            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($text_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_name);
            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($text_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_color);
            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($text_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_shade);
            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($text_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_model);
            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($number_cell_style);
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_00)
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_cost);
            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($text_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $item->item_sapuom);
            $col++;
                
            for($i = 0; $i < count($data['branches']); $i++){

                $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($number_cell_style);
                $spreadsheet->getSheet(0)
                ->setCellValue($col.(10+$key1), $data['rows'][$counter][1]);

                $row_qty_total += floatval($data['rows'][$counter][1]);

                $col++;
                $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($number_cell_style);
                $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_NUMBER_00)
                ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $spreadsheet->getSheet(0)
                ->setCellValue($col.(10+$key1), $data['rows'][$counter][2]);
                $col++;

                $row_amount_total += floatval($data['rows'][$counter][2]);

                $counter++;

            }

            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($number_cell_style);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $row_qty_total);

            $col++;
            $spreadsheet->getSheet(0)->getStyle($col.(10+$key1))->applyFromArray($number_cell_style)->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_00)
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$key1), $row_amount_total);

            $last_row++;
            $items_qty++;
        }

        // Item rows end

        // Total row start

        $col = 'A';

        $prev = $col;

        for($i = 0; $i < count($static_columns); $i++){
            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->applyFromArray($bottom_header_style);
            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');

            if($i != count($static_columns)-1){
                $col++;
            }
        }

        $spreadsheet->getSheet(0)->mergeCells(($prev.(10+$last_row)).':'.($col.(10+$last_row)));
        $spreadsheet->getSheet(0)->setCellValue($prev.(10+$last_row), 'TOTAL');
        $spreadsheet->getSheet(0)->setCellValue($prev.(10+$items_qty+3), 'Conforme:')->getStyle($prev.(10+$items_qty+3))->getFont()->setBold(true);
        $spreadsheet->getSheet(0)->setCellValue($prev.(10+$items_qty+6), 'Aileen Gonzales')->getStyle($prev.(10+$items_qty+6))->getFont()->setBold(true);
        $spreadsheet->getSheet(0)->setCellValue($prev.(10+$items_qty+8), 'Head of Operations');
        $spreadsheet->getSheet(0)->setCellValue($prev.(10+$items_qty+10), 'Date/Time Printed:'.' '.$now);
        $spreadsheet->getSheet(0)->setCellValue('F'.(10+$items_qty+3), 'Approved by:')->getStyle('F'.(10+$items_qty+3))->getFont()->setBold(true);
        $spreadsheet->getSheet(0)->setCellValue('F'.(10+$items_qty+6), 'Ramon de Ubago III')->getStyle('F'.(10+$items_qty+6))->getFont()->setBold(true);
         $spreadsheet->getSheet(0)->setCellValue('F'.(10+$items_qty+8), 'President & CEO');
        $spreadsheet->getSheet(0)->setCellValue('R'.(10+$items_qty+3), 'Approved by:')->getStyle('R'.(10+$items_qty+3))->getFont()->setBold(true);
        $spreadsheet->getSheet(0)->setCellValue('R'.(10+$items_qty+6), 'Roberto Carlos')->getStyle('R'.(10+$items_qty+6))->getFont()->setBold(true);
         $spreadsheet->getSheet(0)->setCellValue('R'.(10+$items_qty+8), 'EVP & CFO');




        

        $col++;
    

        // foreach($data['items'] as $key1 => $item){

            $row_amount_total = 0;
                
            for($i = 0; $i < count($data['branches']); $i++){

                $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->applyFromArray($number_cell_style);
                $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
                $spreadsheet->getSheet(0)->setCellValue($col.(10+$last_row), '');
                $col++;

                $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->applyFromArray($number_cell_style);
                $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
                $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_NUMBER_00)
                ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $spreadsheet->getSheet(0)
                ->setCellValue($col.(10+$last_row), $data['total_costs'][$i][0]);
                $col++;

                $row_amount_total += floatval($data['total_costs'][$i][0]);

            }
            
            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->applyFromArray($number_cell_style);
            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
            $spreadsheet->getSheet(0)->setCellValue($col.(10+$last_row), '');
            $col++;

            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF00');
            $spreadsheet->getSheet(0)->getStyle($col.(10+$last_row))->applyFromArray($number_cell_style)->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_00)
            ->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $spreadsheet->getSheet(0)
            ->setCellValue($col.(10+$last_row), $row_amount_total);
            // $spreadsheet->getSheet(0)->getPageSetup()->setFitToWidth(1);
            // $spreadsheet->getSheet(0)->getPageSetup()->setFitToWidth(0);


        // }

        // Total row end


        // set the header first, so the output will be treated as xlsx file
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        // make it an attachment so we can define the file name
        header('Content-Disposition: attachment;filename="'.$po.'.xlsx"');

        // create IOFactory object
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function budgetfolder_sc(){
        $data = array(
            'page_title' => __CLASS__,
            'user' => $this->user,
            'folders' => $this->budgetfolder->get_folder_names()->result(),
        );
    
        $this->render('budgetfolder/budgetfolder_sc', $data);
    }

    public function get_budgetfolder_sc(){
        $month = $this->input->get('month');
        $folder = $this->input->get('folder');
        $item_supplier_name = $this->input->get('item_supplier_name');
        $category_name = $this->input->get('category_name');

        echo json_encode($this->budgetfolder->get_budgetfolder_sc($month, $folder, $item_supplier_name, $category_name));
    }

    public function test1(){
        $month = 'January 2020';
        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y', strtotime($month));
        echo $month_and_year;

    }



    public function get_folder_costs(){
        $month = $this->input->get('month');
        $batch = $this->input->get('batch');
        $year = $this->input->get('year');
  
        echo json_encode($this->budgetfolder->get_folder_costs($month, $batch, $year));
    }

    public function auto_order($branch_id, $budgetmonth){

        $data = array(
            'page_title' => __CLASS__,
            'user' => $this->user,
            'brands' => $this->report->get_brands()->result(),
            'orders' => $this->budgetfolder->budgetfolder_autoorder($branch_id, urldecode($budgetmonth))
        );

        
        $this->render('budgetfolder/auto_order', $data);

    }

    public function submit_forecast(){
        $brand_id = $this->input->post('brand_id');
        $service_id = $this->input->post('service_id');
        $percentage = $this->input->post('percentage');
        // $year = $this->input->post('year');
        $budgetforecast_month = $this->input->post('month');
        // $budgetforecast_month = date('F', strtotime($month)).' '.$year;
        $data = array('brand_id' => $brand_id, 
                      'productservice_id' => $service_id,
                      'budgetforecast_month' => $budgetforecast_month,
                      'percentage' => $percentage
        );

        echo json_encode($this->budgetfolder->submit_forecast($data));
    }


    public function categorysalesforecast(){
        $data = array(
            'page_title' => __CLASS__,
            'user' => $this->user,
           'brands' => $this->report->get_brands()->result()
        );
    
        $this->render('budgetfolder/categorysalesforecast', $data);
    }

    public function get_categoryforecast(){
        $branch_id = $this->input->post('branch_id');
        $budgetforecast_month = $this->input->post('budgetforecast_month');


        echo json_encode($this->budgetfolder->get_categoryforecast($branch_id, $budgetforecast_month));
       
    }

    public function get_forecast(){
        echo json_encode($this->budgetfolder->get_forecast($this->input->post('brand_id'), $this->input->post('month_and_year')));
    }

    public function edit_percentage(){
        echo json_encode($this->budgetfolder->edit_percentage($this->input->post('budgetforecast_id'), $this->input->post('percentage')));
    }

    public function create_autoorder(){
            $budgetfolder_data = array(
                'user_id' => $this->input->post('user_id'),
                'branch_id' => $this->input->post('branch_id'),
                'budgetfolder_budgetdate' => $this->input->post('budgetfolder_budgetdate'),
                'budgetfolder_materialbeginningtotal' => $this->input->post('materials_beginning'),
                'budgetfolder_materialordertotal' => $this->input->post('total_autoorder'),
                'budgetfolder_materialmaxallowance' => $this->input->post('material_allowance'),
                'budgetfolder_otcbeginningtotal' => $this->input->post('otc_beginning'),
                'budgetfolder_otcordertotal' => 0,
                'budgetfolder_otcmaxallowance' => $this->input->post('otc_allowance'),
                'budgetfolder_submitted' => date('Y-m-d'),
                'budgetfolder_otcbudget' => $this->input->post('total_otcbudget'),
                'budgetfolder_autoorder' => $this->input->post('total_autoorder'),
                'budgetfolder_manualbudget' => $this->input->post('total_manualbudget'),
                'budgetfolder_nonconsumable' => $this->input->post('non_consumable')
            );

            $consumables = $this->input->post('consumables');

            echo json_encode($this->budgetfolder->create_autoorder($budgetfolder_data, $consumables));
           
    }


    public function print_test(){
        $folder = $this->input->get('folder');
        $month = $this->input->get('month');
        $year = $this->input->get('year');
        $batch = $this->input->get('batch');

        echo json_encode($this->budgetfolder->print_test($folder, $month, $year, $batch));

    }

}