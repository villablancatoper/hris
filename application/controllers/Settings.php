<?php



  class Settings extends CI_Controller{

    public function __construct(){

      parent::__construct();



      if(!$this->session->userdata('user')){

        redirect('login');

      }



      $this->load->model(array(

        'Dashboard_model' => 'dashboard',

        'Settings_model' => 'settings',

        'Survey_model' => 'survey'

      ));

      

      date_default_timezone_set('Asia/Manila');

    }



    public function add_sp(){

      $user = $this->session->userdata('user');



      $data = array(

        'page_title' => __CLASS__,

        'user' => $user,

        'changelogs' => $this->dashboard->get_changelogs(),

        'branches' => $this->dashboard->get_branches(),

        'sp_list' => $this->settings->sp_list()



      );

      

      $this->render('settings/index', $data);

    }





    public function gc_verification(){

      $user = $this->session->userdata('user');



      $data = array(

        'page_title' => __CLASS__,

        'user' => $user,

        'changelogs' => $this->dashboard->get_changelogs(),

        'branches' => $this->dashboard->get_branches()



      );

      

      $this->render('settings/gc_verification', $data);

    }





    

    public function get_sp(){

      $user = $this->session->userdata('user');

      $branch_id = $user->branch_id;



      echo json_encode($this->settings->get_sp($branch_id));

    }



    public function answer_later(){



      $user = $this->session->userdata('user');

      

      $data = array('branch_id' => $branch_id = $user->branch_id, 

        'user_name' => $user->user_name,

        'date_submitted' => '',

        'contact' => '',

        'internet' => '',

        'telephone' => '',

        'card_terminal' => '',

        'PC' => '',

        'others' => '',

        'internet' => '',

        'others' => ''
      );

      $this->survey->answer_later($data);

    }



    public function submit_survey(){

      $now = new Datetime();

      $now = $now->format('Y-m-d');

      $user = $this->session->userdata('user');

      $user_id = $user->user_id;

      $data = array('branch_id' => $branch_id = $user->branch_id, 

                    'user_name' => $user->user_name,

                    'date_submitted' => $now,

                    'contact' => $this->input->post('survey_contact'),

                    'internet' => $this->input->post('internet_survey'),

                    'telephone' => $this->input->post('telephone_survey'),

                    'card_terminal' => $this->input->post('terminal_survey'),

                    'PC' => $this->input->post('computer_survey'),

                    'others' => $this->input->post('other_survey'),

                    'flag_encode_later' => 0

      );

      $this->survey->submit_survey($data, $user_id);

    }



    public function get_branch_survey_status(){

      $user = $this->session->userdata('user');

      $branch_id = $user->branch_id;

      echo json_encode($this->survey->get_branch_survey_status($branch_id));

    }



    public function fetch_sp(){

        $sp_id = $this->input->post('sp_id');

        echo json_encode($this->settings->fetch_sp($sp_id));



    }

    public function add_serviceprovider(){

        $user = $this->session->userdata('user');

        $branch_id = $user->branch_id;

        $last_name = $this->input->post('last_name');

        $first_name = $this->input->post('first_name');

        $full_name = $first_name ." ".$last_name;

      

        echo json_encode($this->settings->add_sp($branch_id, $full_name));

    }




    public function render($page, $data){

      $this->load->view('templates/head', $data);

      $this->load->view('templates/header', $data);

      $this->load->view('templates/sidebar', $data);

      $this->load->view($page, $data);

      $this->load->view('templates/footer', $data);

    }

  }