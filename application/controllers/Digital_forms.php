<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digital_forms extends CI_Controller {

	public function __construct(){

        parent::__construct();
     
        $this->user = $this->session->userdata('user');

        $this->load->model([
            'Digitalforms_model' => 'df_model'
        ]);

        if (!$this->session->userdata('user')) {
            redirect('login');
        } 

        date_default_timezone_set('Asia/Manila');
    }

    /**
     * Fetches User info
     * 
     */
    public function index() {
        $user_branch_id = $this->user->branch_id;

        $data = [

            'page_title' => 'Digital Forms',
            'user' => $this->user,
            'branches' => $this->df_model->get_branches()->result(),
            'branch_users' => $this->df_model->get_branch_users($user_branch_id)->result(),

        ];

        $this->render('digital_forms/index', $data);
    }


    /**
     * Load approval view
     * 
     */
    public function approval() {
        $user_branch_id = $this->user->branch_id;

        $data = [

            'page_title' => 'Digital Forms',
            'user' => $this->user,

        ];

        $this->render('digital_forms/approval', $data);
    }

    /**
     * Load Dept form view
     * 
     */
    public function dept_forms() {
        $user_branch_id = $this->user->branch_id;

        $data = [

            'page_title' => 'Digital Forms',
            'user' => $this->user,

        ];

        $this->render('digital_forms/dept-forms', $data);
    }

    /**
     * Load Company Policy form view
     * 
     */
    public function company_policies() {
        $user_branch_id = $this->user->branch_id;

        $data = [

            'page_title' => 'Company Policies',
            'user' => $this->user,

        ];

        $this->render('digital_forms/company_policies', $data);
    }


    /**
     * Saves user request
     * 
     * 
     */
    public function add_new_request_form() {

        $type = $this->input->post('form_type');

        if ($type == 'leave_request') {

            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'from_date' => $this->input->post('from_date'),
                'to_date' => $this->input->post('to_date'),
                'request_type' => $this->input->post('leave_type'),
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => $this->input->post('leave_type'),
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);

        } else if ($type == 'bio') {

            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'time_in' => $this->input->post('time_in'),
                'time_out' => $this->input->post('time_out'),
                'request_type' => 'Biometric No In/No out',
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => 'Biometric No In/No out',
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);

        } else if ($type == 'cow') {

            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'time_in' => $this->input->post('time_in'),
                'time_out' => $this->input->post('time_out'),
                'new_time_in' => $this->input->post('new_time_in'),
                'new_time_out' => $this->input->post('new_time_out'),
                'request_type' => 'Change of Work Schedule',
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => 'Change of Work Schedule',
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);

        } else if ($type == 'branch_transfer') {

            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'transfer_branch' => $this->input->post('transfer_branch'),
                'request_type' => 'Branch Transfer',
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => 'Branch Transfer',
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);
            
        } else if ($type == 'ob') {
            
            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'covered_time' => $this->input->post('covered_time'),
                'time_in' => $this->input->post('from'),
                'time_out' => $this->input->post('to'),
                'request_type' => 'Official Business',
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => 'Official Business',
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);

        } else if ($type == 'ot') {
            
            $data = [

                'user_id' => $this->user->user_id,
                'branch_id' => $this->user->branch_id,
                'user_name' => $this->user->user_name,
                'user_position' => $this->user->user_position,
                'covered_time' => $this->input->post('covered_time'),
                'time_in' => $this->input->post('from'),
                'time_out' => $this->input->post('to'),
                'request_type' => 'Overtime/Offsetting',
                'remarks' => $this->input->post('comment'),
                'request_status' => 'For Approval'
    
            ];
            
            $id = $this->df_model->create_new_request($data);
    
            $history = [
    
                'form_id' => $id,
                'request_type' => 'Overtime/Offsetting',
                'form_history_remark' => 'For Approval'
    
            ];
    
            return $this->df_model->new_request_history($history);

        }

    }



    /**
     * Get All user Request
     * 
     * 
     */
    public function get_request() {
        $user_id = $this->user->user_id;
        echo json_encode($this->df_model->get_requests($user_id)->result());

    }


    /**
     * Get All major requests
     * 
     * 
     */
    public function get_major_request() {

        echo json_encode($this->df_model->show_major_requests()->result());

    }


    /**
     * Get All Pending Request
     * 
     * 
     */
    public function get_all_pending_requests() {
        $user_id = $this->user->branch_id;
        $dept = $this->user->user_access_level;


        if ($dept == "1") {

            echo json_encode($this->df_model->get_second_level($user_id)->result());

        } else if ($dept == "2") {

            echo json_encode($this->df_model->get_third_level($user_id)->result());

        };
        // echo json_encode($this->df_model->get_second_level($user_id)->result());

    }


    /**
     * Get Specific User Requests
     * 
     * 
     */
    public function post_request() {

        $form_id = $this->input->post('form_id');

        $history['req_history'] = $this->df_model->post_request_history($form_id)->result();
        $history['post_request'] = $this->df_model->post_request($form_id)->row();
    
        echo json_encode($history);

    }


    /**
     * Deletes request
     * 
     * 
     */
    public function delete_request($request_id) {

        $this->df_model->delete_request($request_id);

    }
    

    /**
     * Update current request
     * @param int $user_id
     * 
     */
    public function update_request($id) {

        $data = [
  
            'user_id' => $this->user->user_id,
            'branch_id' => $this->user->branch_id,
            'user_name' => $this->user->user_name,
            'user_position' => $this->user->user_position,
            'from_date' => $this->input->post('from_date'),
            'to_date' => $this->input->post('to_date'),
            'request_type' => $this->input->post('leave_type'),
            'remarks' => $this->input->post('remarks'),
            'request_status' => 'Pending'
        
        ];
  
        $this->df_model->update_request($id, $data);
        
    }


    /**
     * Approval of both SM and ASM
     * @param int $form_id
     * 
     */
    public function approve_request($form_id) {
        
        $access_level = $this->user->user_access_level;

        $history['form_id'] = $form_id;

        if ($access_level == "1") {

            $data['request_status'] = "Approved by ". $this->user->user_position .". Pending Notation";
            $data['request_level'] = 1;
            $history['form_history_remark'] = $this->input->post('approve_reason');

        } else if ($access_level == '2') {

            $data['request_status'] = "Noted by ". $this->user->user_position .". Request Approved";
            $data['request_level'] = 2;
            $history['form_history_remark'] = $this->input->post('approve_reason');

        }

        $this->df_model->update_request_history($history);

        return json_encode($this->df_model->approve_user_request($form_id, $data));

    }


    /**
     * Disapproval of request on both SM and ASM
     * @param int $form_id
     * 
     */
    public function disapprove_request($form_id) {

        $data['request_status'] = "Disapproved by " . $this->user->user_position . "";
        $history = [
            'form_id' => $form_id,
            'form_history_remark' => $this->input->post('disapprove_reason')
        ];

        $this->df_model->update_request_history($history);

        return json_encode($this->df_model->approve_user_request($form_id, $data));

    }


    /**
     * Get Department Specific Request
     * 
     * 
     */
    public function specific_requests() {
        $branch_id = $this->user->branch_id;
        $request_type = $this->input->post('type');

        if ($request_type == 'leave') {

            $request_type = [

                'Vacation Leave',
                'Sick Leave', 
                'Personal/Emergency Leave',
                'Paternity Leave',
                'Maternity Leave',
                'Others',
                 
            ];

            echo json_encode($this->df_model->get_certains($branch_id, $request_type)->result());

        } else {

            echo json_encode($this->df_model->get_certains($branch_id, $request_type)->result());

        };

    }

    

    // Page render
    public function render($page, $data) {
        $this->load->view('templates/head', $data);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view($page, $data);
        $this->load->view('templates/footer', $data);
    }
}


