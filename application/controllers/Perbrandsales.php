<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perbrandsales extends CI_Controller {

  public $user;

	public function __construct(){
    parent::__construct();

		$this->user = $this->session->userdata('user');

		$this->load->model(array(
			'Perbrandsales_model' => 'perbrandsales',
		));

		if(!$this->session->userdata('user')){
			redirect('login');
		}  
	}

	public function index(){
		$data = array(
			'page_title' => 'Report',
			'user' => $this->user,
		);

		$this->render('report/perbrandsales', $data);
	}

	public function compute(){
		$start = $this->input->post('start');
		echo json_encode($this->perbrandsales->get($start));
	}

	public function render($page, $data){
		$this->load->view('templates/head', $data);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view($page, $data);
		$this->load->view('templates/footer', $data);
	}
}
