<?php

  class Login extends CI_Controller{

    public function __construct(){
      parent::__construct();

      $this->load->model(array(
        'Login_model' => 'login'
      ));
      
      date_default_timezone_set('Asia/Manila');

    }

    public function login(){

      if($this->session->userdata('user')){
        redirect('dashboard');
      }

      $username = $this->input->post('username');
      $password = $this->input->post('password');
      
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if(!$username || !$password){
          $this->session->set_userdata('message', '<div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p class="text-light">Username or Password is empty!</p></div>');
          redirect('login');
        }
        else{

          $time = (int) date('Hi');
          
          $user = $this->login->check_user($username, $password);

          if($user != FALSE){
            if($time < '0800' && $time > '0000' && $user->brand_id != 100007){
                $this->session->set_userdata('message', '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p class="text-light">Login failed. Allowed login time is within 8:00 am until 12:00 midnight only.</p></div>');
                redirect('login');
            }
            else{
              $this->session->set_userdata('user', $user);
              $this->session->set_userdata('message', '<div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <p class="text-light">Successfully logged in!</p></div>');

              $user_id  = $this->session->userdata('user')->user_id;
              $branch_name  = $this->session->userdata('user')->branch_name;
              $datetime = date('Y-m-d H:i:s');
              $ip = $_SERVER['REMOTE_ADDR'];


              $this->login->update_last_login($user_id);

              $user_log = array(
                'userlog_datetime' => date('Y-m-d H:i:s'),
                'user_id' => $user_id,
                'userlog_ipaddress' => $ip,
                'userlog_type' => 'Login',
                'userlog_details' => "$username successfully logged in at $datetime on branch $branch_name"
              );

              $this->login->insert_user_log($user_log);

              if($this->session->userdata('user')->branch_id == 208){
                redirect('commission');
              }
              
              if($this->session->userdata('user')->branch_id == 210){
                redirect('ticketing');
              }

              if($user->user_role == 'Supply Chain'){
                redirect('dashboard/welcome');
              }

              if($user->branch_id == 198){
                if ($user->user_id == 3 || $user->user_id == 67 || $user->user_id == 68 || $user->user_id == 292 || $user->user_id == 250 || $user->user_id == 385 || $user->user_id == 308 || $user->user_id == 383 || $user->user_id == 151) {
                   redirect('dashboard');
                } else {
                   redirect('dashboard/welcome');
                }
               
              }
              if($user->branch_id == 211){
                if ($user->user_id == 292) {
                   redirect('dashboard');
                } else {
                   redirect('dashboard/welcome');
                }
              }

              redirect('dashboard');
            }
          }
          else{
            $this->session->set_userdata('message', '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p class="text-light">Incorrect login! </p></div>');
            redirect('login');
          }
        }

      }
      else{

        $this->load->view('login');
      }
    }

    public function start_maintenance(){
      $this->login->start_maintenance();
    }

    public function end_maintenance(){
      $this->login->end_maintenance();
    }

    public function get_maintenance_status(){
      echo json_encode($this->login->get_maintenance_status($this->session->userdata('user_id')));
    }


    public function logout(){
      $this->login->update_last_logout($this->session->userdata('user')->user_id);
      $this->session->unset_userdata('user');
      $this->session->set_userdata('message', '<div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p class="text-light">Successfully logged out!</p></div>');
      $this->session->sess_destroy();
      redirect('login');
    }



  }

