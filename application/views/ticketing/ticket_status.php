<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<style type="text/css">
    .dataTables_filter {
        float: left !important;
    }

    #code_color {
        width: 100px;
    }

    #create_new_ticket {
        float: right;

    }

</style>

<div class="box box-primary" id="result">

    <!-- /.box-header -->

    <!-- form start -->
<?php

$user = $this->session->userdata('user');
$department = $user->user_department;
$user_role = $user->user_role;
// $job_title = $user->job_title;
// print_r($user);
 foreach ($ticket_status as $row) {
    $ticket_status = $row->ticket_status;
    $branch = $row->ticket_brand_branch;
    $category = $row->ticket_category;
    $title = $row->ticket_title;
}
// print_r($ticket_status)
?>

    <div class="box-body">

        <hr style="margin-bottom: 0;">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">
             <!--    <h4 style="font-weight: bold;">Ticket Status : <?php echo $ticket_status; ?></h4>
                <h4 style="font-weight: bold;">Site/Store : <?php echo $branch; ?></h4>
                <h5 style="font-weight: bold;">Category : <?php echo $category; ?></h5>
                <h5 style="font-weight: bold;">Details of Concern : <?php echo $title; ?></h5> -->
                <table>
                    <tr>
                        <td align="right" style="font-weight: bold; font-size: 16px;">Ticket Status : </td>
                        <td style="font-size: 16px;"><?php echo $ticket_status; ?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold; font-size: 16px;">Site/Store : </td>
                        <td style="font-size: 16px;"><?php echo $branch; ?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold; font-size: 16px;">Category : </td>
                        <td style="font-size: 16px;"><?php echo $category; ?></td>
                    </tr>
                    <tr>
                        <td align="right" style="font-weight: bold; font-size: 16px;">Details of Concern : </td>
                        <td style="font-size: 16px; margin-bottom: 5px;"><?php echo $title; ?></td>
                    </tr>
                </table>

                <div class="table-responsive" id="ticket_align">


                    <table id="ticket_status" class="table table-bordered table-hover">

                        <thead>

                            <tr>

                                <th>Ticket No.</th>

                                <th>User</th>

                                <th>Time of Update</th>

                                <th>Update/Comment</th>

                            </tr>

                        </thead>

                        <tbody>
                            <?php
                           foreach ($ticket_updates as $row)
                            {
                                $dt = new DateTime($row->ticket_update_timestamp);
                                $row->ticket_update_timestamp = $dt->format('M'." ".'d'.","." ".'Y g:i a');
                                echo "<tr>";
                                    echo '<td>'; 
                                    echo $ticket_id = $row->ticket_id;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $row->ticket_updater;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $row->ticket_update_timestamp;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $ticket_update= $row->ticket_update;
                                    echo '</td>';
                                echo '</tr>'; 
                            }
                      ?>

                        </tbody>

                    </table>
                    <?php
                  foreach ($last_update as $key) {
                    $ticket_last_update = $key->ticket_update;
                  }
                 
                ?>
                    <button class="btn btn-primary" <?php if($ticket_last_update != 'Ticket created' || $user_role == 'Standard User'){echo "disabled";} ?>  onclick="accept_ticket(<?php echo $ticket_id;?>);" style="width: 120px;">Accept Ticket</button>
                    <button class="btn btn-primary" style="width: 120px; margin-left: 20px;" <?php if($ticket_last_update == 'Ticket created'){ echo "disabled";}elseif($ticket_status == 'Completed'){ echo "disabled";} ?>  data-toggle="modal"  data-target="#modal_close_ticket"  >Close Ticket</button>

                    <button class="btn btn-primary" <?php if($department != 'IT'){ echo "style = 'display:none;'";}elseif($ticket_last_update == 'Ticket created'){ echo "disabled"; } ?> style="height: 33px; width: 150px; margin-left: 20px; font-size: 12px; " data-toggle = "modal" data-target="#modal_new_assignee">Change Ticket Assignee</button>

                    <button class="btn btn-primary" style="float: right;" <?php if($ticket_status == 'Completed' ||$ticket_last_update == 'Ticket created'){ echo "disabled";}else{ echo "";} ?> data-toggle="modal" data-target="#modal_post_comment">Post Update/Comment</button>
                
                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

    </form>

</div>


<!-- MODAL NEW ASSIGNEE -->
<div class="modal fade" id="modal_new_assignee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Change Ticket Assignee</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_assign">
                    <div class="form-group" style="margin-left: -15px;">
                        <label for="ticket_assignee" class="col-sm-12 control-label">Assign to:</label>
                    </div>

                    <div class="form-group" style="margin-left: -15px;">
                         <div class="col-sm-12">
                            <select id="ticket_assignee" class="form-control select2" style="width: 570px;">

                                <option value="" hidden selected>Select one</option>
                                <?php
                    foreach($mis_list as $row){
                  ?>
                                <option value="<?php echo "$row->full_name" ?>"><?= $row->full_name ?></option>
                                <?php
                    }
                  ?>
                            </select>
                         
                    </div>
                    </div>
                </form>
            </div>
            <div id="message" style="margin-top: 100px;"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="post_update" name="post_update" onclick="new_assign(<?php echo $ticket_id;?>);" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL NEW ASSIGNEE -->



<div class="modal fade" id="modal_post_comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Post New Update/Comment</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_post_comment">
                    <div class="form-group">
                        <label for="update_details" class="col-form-label">Details of Update:</label>
                        <textarea id="update_details" name="update_details" placeholder="Tell us more about the concern" class="form-control" maxlength="255"></textarea>
                    </div>
                    <input type="hidden" id="base_url" value="<?= base_url()?>" name="base_url">
                </form>
                <p style="font-size: 10px; " class="pull-right" id="count_message"></p>
                <p style="font-size: 10px; color: green;">Maximum of 255 characters only</p>
            </div>
            <div id="success"></div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="post_update" name="post_update" onclick="post_update(<?php echo $ticket_id;?>);" class="btn btn-primary">Post</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL CONFIRMATION CLOSE TICKET -->
<div class="modal fade" id="modal_close_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Close Ticket</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
               <form id="form_close">
                    <div class="form-group">
                        <label for="remarks" class="col-form-label">Remarks:</label>
                        <textarea id="remarks" name="update_details" placeholder="" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="date" class="col-form-label">Date Completed:</label>
                        <input type="date" class="form-control" id="date">
                    </div>
                </form>
            </div>
            <div id="close_message"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" id="post_update" name="post_update" onclick="close_ticket(<?php echo $ticket_id;?>);" class="btn btn-primary">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL CONFIRMATION CLOSE TICKET -->

<div class="modal fade" id="modal_accept_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2 align="center">Ticket Accepted</h2>
            </div>
            <div id=""></div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script src="<?php echo base_url(); ?>assets/customs/js/ticketing/ticket_status.js"></script>


</div>

<!-- /.content-wrapper -->