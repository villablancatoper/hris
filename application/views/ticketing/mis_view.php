<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<style type="text/css">
    .dataTables_filter {
        float: left !important;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

    #create_new_ticket {
        float: right;

    }


}


</style>
<?php

$user = $this->session->userdata('user');
$department_id = $user->department_id;
// print_r($user);

// TEST GIT I CHANGE THIS LINE
?>


<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <button type="button" class="btn btn-light"  onclick="sort_all();" id="code_color">All</button>
        <button type="button" class="btn btn-primary" onclick="sort_new();" id="code_color">New</button>
        <button type="button" class="btn btn-info" onclick="sort_assigned();" id="code_color" style="font-size: 12px !important;">Assigned to me</button>
        <button type="button" class="btn btn-warning" onclick="sort_progress();" id="code_color">In-progress</button>
        <button type="button" class="btn btn-success" onclick="sort_completed();" id="code_color">Completed</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_new_ticket" id="create_new_ticket">Create New Ticket</button>
        <hr style="margin-bottom: 0;">

        <div class="row">

            <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">
                <div class="table-responsive">


                    <table id="mis_ticket_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                <th>Ticket No.</th>

                                <th>Date and Time Created</th>

                                <th>Dept/Site/Store</th>

                                <th style="width: 180px;">Type of Concern</th>

                                <th style="max-width: 180px; ">Details of Concern</th>

                                <th>Ticket Assigned To</th>

                                <th>Status</th>

                                <th>Aging</th>

                                <th>Latest Update Date</th>

                                <th>Latest Update Details</th>

                                <th>Action</th>

                            </tr>

                        </thead>

                        <tbody style="font-size: 12px; word-break: break-all !important;" >

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

    </form>

</div>

<div class="modal fade" id="modal_new_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Ticket</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_create_ticket">
                    <div class="form-group" style="margin-left: -15px;">
                        <label for="site_store" class="col-sm-12 control-label">Site and Store:</label>
                    </div>

                    <div class="form-group" style="margin-left: -15px;">
                         <div class="col-sm-12">
                            <select id="site_store" class="form-control select2" style="width: 570px;">

                                <option value="" hidden selected>*Select Branch</option>
                                <?php
                    foreach($branches as $branch){
                  ?>
                                <option value="<?= $branch->branch_name?>"><?= $branch->branch_name?></option>
                                <?php
                    }
                  ?>
                            </select>
                         
                    </div>
                    </div>
                          
                    <div class="form-group">
                        <label for="concern_type" class="col-form-label">Type of Concern:</label>
                        <select id="concern_type" class="form-control" required>
                            <option value="" hidden selected>*Choose Type of Concern</option>
                            <option>CCTV</option>
                            <option>Telephone/Landline</option>
                            <option>Card Terminal</option>
                            <option>Biometrics</option>
                            <option>Internet</option>
                            <option>Computer(Desktop or Laptop)</option>
                            <option>Other IT concern</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="concern_details" class="col-form-label">Details of Concern:</label>
                        <textarea id="concern_details" placeholder="Tell us more about the concern" class="form-control"></textarea>
                    </div>
                </form>
                <div style="font-size: 10px; color: green;"> Fields with * mark are required</div>
            </div>
            <div id="message"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="ticket_submit" onclick="ticket_submit();" class="btn btn-primary">Submit Ticket</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?php echo base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script src="<?php echo base_url(); ?>assets/scripts/mis_ticket.js"></script>


</div>

<!-- /.content-wrapper -->
