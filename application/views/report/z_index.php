<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">


<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 



</style>








<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
            <div class="col-md-12">

                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                        <option value="" hidden selected>Select Brand</option> 

                        <?php foreach($brands as $row) { ?>

                                <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                        <?php } ?>

                </select>

                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                        <option value="" hidden selected>Select Branch</option> 


                </select>

                <button type="button" class="btn btn-default" id="daterange_btn">

                <span id="category_date">

                <i class="fa fa-calendar" ></i> Select Date

                </span>

                <i class="fa fa-caret-down"></i>

                </button>

                <button onclick="compute();" class="btn btn-primary">Search</button>
        
                <button onclick="refresh();" class="btn btn-primary">Refresh</button>

            </div>
        </div>

        <hr>

        <div class="row">

            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="totalsales_table" class="table table-bordered table-hover">
                        <thead>
                            <tr style="font-size: 10px; font-weight: bolder; text-align: center; ">
                                <th>Date</th>
                                <th>Brand</th>
                                <th>Branch</th>
                                <th>Total Sales</th>
                                <th>Total Cash Payment</th>
                                <th>Cash Deposit</th>
                                <th>Deposit Date</th>
                                <th>Total Card Payment</th>
                                <th>Total GC Payment</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 10px;">
                        </tbody>
                        <tfoot style="font-size: 11px; font-weight: bolder; text-align: center; ">
                                <th colspan="3" style="font-weight: bold;"> Total</th>
                                <th id="total_sales"></th>
                                <th id="total_cash"></th>
                                <th id="total_deposit"></th>
                                <th></th>
                                <th id="total_card"></th>
                                <th id="total_gc"></th>
                                <th colspan="2"></th>
                        </tfoot>
                    </table>

                </div>



            </div>

        </div>


    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="branch_name">

    <input type="hidden" id="brand_id">
    <input type="hidden" id="branch_id">
 
    <input type="hidden" id="transaction_date">
    <input type="hidden" id="transaction_totalsales">
    <input type="hidden" id="transaction_paymentcash">
    <input type="hidden" id="transaction_paymentcard">

    <input type="hidden" id="cash_card">


    <!-- /.box-body -->



    </form>



</div>
<style type="text/css">
    .label{
        border: 1px solid #ccc;
        height: 30px;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        text-align: left;
        }
    .label-card{
        border: 1px solid #ccc;
        height: 30px;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        text-align: left;
        font-weight: bold;
        }
    }
</style>
    
<div class="modal fade" id="cashVarianceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" >
        <div class="form-group">
            <label for="branch_modal" style="font-size: 20px;">Branch: </label>
                 <h3 id="branch_modal" style="margin-left: 20px;"></h3>
        </div>
        <div class="form-group">
            <label for="date_modal" style="font-size: 20px;">Date: </label>
                 <h3 id="date_modal" style="margin-left: 20px;"></h3>
        </div>
      
      </div>
      <div class="modal-body">
        <form id="form_cash_variance">
                    <div class="form-group">
                        <div  class="label"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="" id="expected_cash" class="form-control" required placeholder="*Expected Cash Payment (e.g, 500.00, 1000.50)">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="variance" class="variance" placeholder="Variance">
                    </div>
                    <div class="form-group">
                        <textarea id="cash_concern_details" placeholder="Remarks" class="form-control" style="height: 100px;"></textarea>
                    </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="message"></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick = "cashVarianceSubmit();">Submit</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="cardVarianceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" >
         <div class="form-group">
            <label for="cardbranch_modal" style="font-size: 20px;">Branch: </label>
                 <h3 id="cardbranch_modal" style="margin-left: 20px;"></h3>
        </div>
        <div class="form-group">
            <label for="carddate_modal" style="font-size: 20px;">Date: </label>
                 <h3 id="carddate_modal" style="margin-left: 20px;"></h3>
        </div>
      </div>
      <div class="modal-body">
        <form id="form_card_variance">
                    <div class="form-group">
                        <div  class="label-card"></div>
                    </div>
                    <div class="form-group">
                        <input type="text" name="" id="expected_card" class="form-control" required placeholder="*Expected Card Payment (e.g, 500.00, 1000.50)">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="card_variance" class="card-variance" placeholder="Variance">
                    </div>
                    <div class="form-group">
                        <textarea id="card_concern_details" placeholder="Remarks" class="form-control" style="height: 100px;"></textarea>
                    </div>
                </form>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick = "cardVarianceSubmit();">Submit</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="tallyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
            <h3 style="text-align: center;">Submit tally?</h3>
      </div>
      <div class="modal-footer">
        <div id="message"></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" onclick="submitTally();">Confirm</button>
      </div>
    </div>
  </div>
</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>





<script src="<?php echo base_url(); ?>assets/customs/js/report/z_index.js?v=1.0.1"></script>





</div>



<!-- /.content-wrapper -->

