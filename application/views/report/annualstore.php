<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }
div.dataTables_processing { z-index: 1; };


    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 



</style>







<div class="box box-primary">



    <!-- /.box-header -->



    <!-- form start -->



    <div class="box-body">



        <div class="row">

            <div class="col-md-12">

                <!-- <div class="col-lg-2"> -->
                
    
                <!-- </div> -->
                <select id="brand" class="form-control select2" style="display: inline-block !important; text-align:right !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                <?php } ?>

                </select>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: -10px !important; width: 230px !important;"> -->

                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Branch</option> 

                </select>
                
                 <select id="month_and_year" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                        

                   <option value="" hidden selected>Select Year</option>
                   
                   
                    <option value="<?= date('Y')?>"><?= date('Y')?> </option>
                    <option value="<?= date('Y')+1?>" ><?= date('Y')+1?> </option>
                    
                </select>

                <button onclick="compute();" class="btn btn-primary">Search</button>
             
        
              
                
            </div>

        </div>

        <hr>

       <div class="row">

            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="totalsales_table" class="table table-bordered table-hover" style="width: 100% !important;">



                        <thead>



                            <tr>

                               

                                <th nowrap style="width:10px !important;">Brand</th>
                                <th nowrap style="width:10px !important;">Branch</th>
                                <th style="width:10px !important;">January</th>
                                <th style="width:10px !important;">February</th>

                                <th style="width:10px !important;">March</th>

                                <th style="width:10px !important;">April</th>

                                <th style="width:10px !important;">May</th>
                                 <th style="width:10px !important;">June</th>
                                  <th style="width:10px !important;">July</th>
                                   <th style="width:10px !important;">August</th>
                                    <th style="width:10px !important;">September</th>
                                     <th style="width:10px !important;">October</th>
                                      <th style="width:10px !important;">November</th>
                                       <th style="width:10px !important;">December</th>
                                        <th style="width:10px !important;">Total</th>

                              

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                        <tfoot>
                             <tr>
                                <td colspan="2"><b>TOTAL</b></td>
                                <td style=" font-weight: bold" id="jan"></td>
                                <td style=" font-weight: bold" id="feb"></td>
                                <td style=" font-weight: bold" id="mar"></td>
                                <td style=" font-weight: bold" id="apr"></td>
                                <td style=" font-weight: bold" id="may"></td>
                                <td style=" font-weight: bold" id="jun"></td>
                                <td style=" font-weight: bold" id="jul"></td>
                                <td style=" font-weight: bold" id="aug"></td>
                                <td style=" font-weight: bold" id="sep"></td>
                                <td style=" font-weight: bold" id="oct"></td>
                                <td style=" font-weight: bold"  id="nov"></td>
                                <td style=" font-weight: bold" id="dec"></td>
                                <td  style=" font-weight: bold !important;" id="tot"></td>
                           </tr>
                    </tfoot>



                    </table>



                </div>



            </div>

        </div>



    </div>

<!-- modal currentinv-->
   
 
  <!-- /.modal-dialog -->

</div>

    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="branch_name">

    <!-- /.box-body -->



    </form>



</div>





















<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>



<!-- Bootstrap 3.3.7 -->





<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->


<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<!-- <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->



<script src="<?php echo base_url(); ?>assets/customs/js/report/annualstore.js?v=1.0.2"></script>





</div>



<!-- /.content-wrapper -->

