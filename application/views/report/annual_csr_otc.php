<style>



  .dataTables_filter {



    display: none;



  }



  .table-hover{



    cursor: pointer;



  }







  @media (min-width: 992px){



      #customer_table_new_wrapper{



          border-right: 1px solid #eee; 



      }



  }



  th{



    vertical-align: middle !important;



  }



  .select2-selection{



    margin-bottom: 3px !important;



  }



  .align-right{

    text-align: right !important;

  }

    .align-center{

    text-align: center !important;

  }



  .dt-buttons{

      text-align: right;

  }



</style>







<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">



  <!-- /.box-header -->



  <!-- form start -->



    <div class="box-body">



      <div class="row">



        <div class="col-md-12">

          <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

            <option value="" hidden selected>Select Brand</option>

            <?php foreach($brands as $row) { ?>

                <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

            <?php } ?>

          </select>

          <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

            <option value="" hidden selected>Select Branch</option> 

          </select>

          <select id="otc" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

            <option value="" hidden selected>Select OTC</option> 

          </select>

          <select id="year" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

            <option value="" hidden selected>Select Year</option> 
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>

          </select>

         

          

          <button class="btn btn-primary btn-flat match-height" onclick="search_custom_csr_otc()" style="margin-bottom: 3px;">Search</button>

          <input type="hidden" id="start">

          <input type="hidden" id="end">

        </div>



      </div>



      <hr style="margin-bottom: 0;">



      <div class="row">



        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">



          <div class="table-responsive">



            <table id="csr_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead style="font-size: 12px;">
          
                                <tr>

                                    <th rowspan="2" style="text-align: left;">Item Name</th>

                                    <th colspan="2" style="text-align: center;">January</th>

                                    <th colspan="2" style="text-align: center;">February</th>

                                    <th colspan="2" style="text-align: center;">March</th>

                                    <th colspan="2" style="text-align: center;">April</th>

                                    <th colspan="2" style="text-align: center;">May</th>

                                    <th colspan="2" style="text-align: center;">June</th>

                                    <th colspan="2" style="text-align: center;">July</th>

                                    <th colspan="2" style="text-align: center;">August</th>

                                    <th colspan="2" style="text-align: center;">September</th>

                                    <th colspan="2" style="text-align: center;">October</th>

                                    <th colspan="2" style="text-align: center;">November</th>

                                    <th colspan="2" style="text-align: center;">December</th>

                                    <th colspan="2" style="text-align: center;">Total</th>

                                </tr>

                                <tr>
                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                     <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                    <th style="text-align: center;">Count</th>

                                    <th style="text-align: right;">Sales</th>

                                </tr>


              </thead>

              <tbody>

              </tbody>
 

            </table>

          </div>

        </div>



      </div>



    </div>



    <!-- /.box-body -->



  </form>



</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>



<!-- Bootstrap 3.3.7 -->



<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>







<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>







<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<!-- AdminLTE App -->



<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>





<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>





<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>







<script src="<?= base_url()?>assets/customs/js/report/annual_csr_otc.js?v=1.0.7"></script>







