<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">

<style type="text/css">
    .dataTables_filter {
        float: left !important;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

    #create_new_ticket {
        float: right;

    }
    .align-right{
        text-align:right; max-width:80px;
    }
    .align-left{
        text-align:left; max-width:80px;
    } 
    .align-center{
        text-align:center; max-width:80px;
    } 

</style>



<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
                 <div class="col-lg-2">
                    <select id="branch" class="form-control select2">
                            <option value="" hidden selected>Select Branch</option> 
                            <?php foreach($branches as $row) { ?>
                                    <option value="<?php echo $row->branch_id; ?>"><?php echo $row->branch_name; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-lg-2">
                    <select id="month" class="form-control select2">
                            <option value="" hidden selected>Month</option> 
                            <option value="2018-12-31.2019-01-31">January 2019</option>
                            <option value="2019-01-31.2019-02-28">February 2019</option>
                            <option value="2019-02-28.2019-03-31">March 2019</option>
                            <option value="2019-03-31.2019-04-30">April 2019</option>
                            <option value="2019-04-30.2019-05-31">May 2019</option>
                            <option value="2019-05-31.2019-06-30">June 2019</option>
                            <option value="2019-06-30.2019-07-31">July 2019</option>
                            <option value="2019-07-31.2019-08-31">August 2019</option>
                            <option value="2019-08-31.2019-09-30">September 2019</option>
                            <option value="2019-09-30.2019-10-31">October 2019</option>
                            <option value="2019-10-31.2019-11-30">November 2019</option>
                            <option value="2019-11-30.2019-12-31">December 2019</option>
                    </select>
                </div>

        <button onclick="select();" class="btn btn-primary" style="width: 300px; margin-top: 0px;">Select</button>
      

            <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">
                <div class="table-responsive">
                    <table id="variance_table" class="table table-bordered table-hover" style="width: 100% !important;">
                        <thead>
                            <tr style="font-size: 11px; font-weight: bolder; text-align: center; font-family: Century Gothic">
                                <th>Category</th>
                                <th>Item Name</th>
                                <th>Item Model</th>
                                <th>Item Color</th>
                                <th>Item Shade</th>
                                <th>UOM</th>
                                <th>Beginning Inventory</th>
                                <th>Delivered</th>
                                <th>Ending Inventory</th>
                                <th>Actual Usage</th>
                                <th>Theoretical</th>
                                <th>Variance</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px; font-family: Century Gothic">
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>

    

    <!-- /.box-body -->

    </form>

</div>
<style type="text/css">
    .label{
        border: 1px solid #ccc;
        height: 30px;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        text-align: left;
        }
    .label-card{
        border: 1px solid #ccc;
        height: 30px;
        display: block;
        width: 100%;
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        text-align: left;
        font-weight: bold;
        }
    }
</style>
<!-- Modal -->







<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->


<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->
<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script src="<?php echo base_url(); ?>assets/customs/js/report/variance.js"></script>


</div>

<!-- /.content-wrapper -->
