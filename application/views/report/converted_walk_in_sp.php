<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 

    .hide{

        display: none;

    }

    .shown{

        display: inline-block;

    }
    .blackout{
        background-color: black;
        border-color: black !important;

    }
    .bold_th{
        font-weight: bold !important;
    }
    .dt-buttons.btn-group {
        position: sticky !important;
    }
 
</style>







<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
            <div class="col-md-12">

                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                <?php } ?>

                </select>

  
                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                     <option value="" hidden selected>Select Branch</option> 

                </select>

                <select id="year" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                     <option value="" hidden selected>Select Month</option>
                     <option value="2019-06-01">June 2019</option> 
                     <option value="2019-07-01">July 2019</option>
                     <option value="2019-08-01">August 2019</option> 
                     <option value="2019-09-01">September 2019</option> 
                     <option value="2019-10-01">October 2019</option> 
                     <option value="2019-11-01">November 2019</option> 
                     <option value="2019-12-02">December 2019</option> 

                </select>



 

                <button onclick="search();" class="btn btn-primary match-height" id="compute">Compute

                </button>

                <div class="pull-right" id="button_here">
                    
                </div>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-12">

                <div class="table-responsive">

                    <table id="converted_walk_in_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th>Brand</th>

                                <th>Branch Name</th>

                                <th>Service Provider</th>

                                <th>Walk-in</th>

                                <th>Converted</th>
                                <th>Difference</th>
                                <th>Percentage</th>



                            </tr>

                        </thead>

                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="text-align: center;">Total</th>
                                <th id="total_walk_in"></th>
                                <th id="total_converted"></th>
                                <th id="total_diff"></th>
                                <th id="total_percentage"></th>

                            </tr>
                        </tfoot>

                    </table>

                </div>

            </div>
        </div>


    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="start1">

    <input type="hidden" id="end1">

    <!-- /.box-body -->



    </form>



</div>





















<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>







<script src="<?php echo base_url(); ?>assets/customs/js/report/converted_walk_in_sp.js?v=1.0.1"></script>





</div>



<!-- /.content-wrapper -->

