<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .align-right{
    text-align: right !important;
  }

  .dt-buttons{
      text-align: right;
  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <button type="button" class="btn btn-default match-height" id="daterange_btn" style="margin-bottom: 3px;">

              <span>

                <i class="fa fa-calendar"></i> Select date

              </span>

              <i class="fa fa-caret-down"></i>

          </button>

          <button class="btn btn-primary btn-flat match-height" onclick="search_dsrs_detailed()" style="margin-bottom: 3px;">Search</button>

          <input type="hidden" id="start">

          <input type="hidden" id="end">

        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="dsrs_detailed_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID</th>

                <th>Date</th>

                <th>Branch</th>

                <th>Customer</th>

                <th>Status</th>

                <th>SP</th>

                <th>Assisted By</th>

                <th>Service</th>

                <th>OTC</th>

                <th>Docket No</th>

                <th>OR No</th>

                <th>Sales</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

      <hr>
      
      <div style="display: none;">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="display-6" style="color: blue;"><?= $user->branch_name?></h2>
          </div>
        </div>

        <hr>

      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_sales">0.00</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales (OTC): </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_sales_otc">0.00</span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Cash: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_cash">0.00</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales (Services): </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_sales_services">0.00</span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Card: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_card">0.00</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total GC: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="total_gc">0.00</span></span></h4>
        </div>
      </div>

      <hr>
      

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Walk-In: </strong> <span id="total_walkin" class="pull-right text-center" style="margin-right: 50px;">0</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Turnaway: </strong> <span id="total_turnaway" class="pull-right text-center" style="margin-right: 50px;">0</span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Transfer: </strong> <span id="total_transfer" class="pull-right text-center" style="margin-right: 50px;">0</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Head Count: </strong> <span id="total_head_count" class="pull-right text-center" style="margin-right: 50px;">0</span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Regular: </strong> <span id="total_regular" class="pull-right text-center" style="margin-right: 50px;">0</span></span></h4>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date Total Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="todate_total_sales">0.00</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date OTC: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="todate_otc">0.00</span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date Services Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="todate_services">0.00</span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date TPH: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;&nbsp;<span id="todate_tph">0.00</span></span></h4>
        </div>
      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/js/jszip.min.js"></script>
<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>

<script src="<?= base_url()?>assets/customs/js/report/dsr_detailed_ho.js?v=1.0.9"></script>



