
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 

    .hide{

        display: none;

    }

    .shown{

        display: inline-block;

    }
    .bold {
      font-weight:bold;
    }
    tbody tr:last-child {
        font-weight: bold !important;
    }

</style>







<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
            <div class="col-md-12">

      
     

                 <select id="month_and_year" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">



                   <option value="" hidden selected>Select Month & Year</option>
                   
                   <!-- <option value="2019-05-01">May-<?= date('Y')?> </option>-->
                   <!-- <option value="2019-06-01" >June-<?= date('Y')?> </option>-->
                    <!--<option value="2019-07-01" >July-<?= date('Y')?> </option>-->
                    <!--<option value="2019-08-01" >August-<?= date('Y')?> </option>-->
                   <!-- <option value="2019-09-01" >September-<?= date('Y')?> </option>-->
                    <!--<option value="<?= date('Y')?>-10-01" >October <?= date('Y')?> </option>-->
                    <option value="<?= date('Y')?>-11-01" >November <?= date('Y')?> </option>
                    <option value="<?= date('Y')?>-12-01" >December <?= date('Y')?> </option>
                    <option value="<?= date('Y')+1?>-01-01" >January <?= date('Y')+1?> </option>
                    <option value="<?= date('Y')+1?>-02-01" >February <?= date('Y')+1?> </option>
                </select>

                <button class = "btn btn-primary" onclick = "compute();"> Search </button>


            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-12">

               <table id="totalsales_table" class="table table-bordered table-hover" style="width: 100% !important;">
                        <thead>

                            <tr>

                                <th>ASM NAME</th>
                                <th>Storetarget</th>
                                <th>Total Transaction Sales</th>

                                <th>MTD%</th>

                                <th>End of Month Trending</th>

                                <th>Trending %</th>

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                        <tfoot>

                               

                               

                        </tfoot>



                    </table>

            </div>
        </div>
    <input type = "hidden" id = "start">
    <input type = "hidden" id = "end">

   

    <!-- /.box-body -->



    </form>



</div>





















<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>





<script src="<?php echo base_url(); ?>assets/customs/js/report/asmsales.js?v=1.0.2"></script>





</div>



<!-- /.content-wrapper -->



