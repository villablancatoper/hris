<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .align-right{
    text-align: right !important;
  }

  .dt-buttons{
      text-align: right;
  }

  .hide{
    display: hidden !important;
  }

  .show{
    display: block !important;
  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">


<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <?php
          if($user->brand_id != 100007){
        ?>

        <!-- <div class="col-md-12">
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            You can now <strong><u>view</u></strong>, <strong><u>edit</u></strong>, or <strong><u>delete</u></strong> a transaction by double clicking an item on the table.
          </div>
        </div> -->

        <?php }?>

        <div class="col-md-12">

            <button type="button" class="btn btn-default match-height" id="daterange_btn" style="margin-bottom: 3px;">
    
                <span>
    
                    <i class="fa fa-calendar"></i> Select date
    
                </span>
    
                <i class="fa fa-caret-down"></i>
    
            </button>
    
            <button class="btn btn-primary btn-flat match-height" onclick="search_dsrs_detailed_branch()" style="margin-bottom: 3px;">Search</button>
    
            <input type="hidden" id="start">
    
            <input type="hidden" id="end">

            <input type="hidden" id="current_date" value="<?= date('Y-m-d')?>">

        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="dsr_detailed_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>Customer ID</th>

                <th>ID</th>

                <th>Date</th>

                <th>Customer</th>

                <th>Status</th>

                <th>Serviceprovider</th>

                <th>Assist Name</th>

                <th>Docket No</th>

                <th>OR No</th>

                <th>Service</th>

                <th>OTC</th>

                <th>Sales</th>

              </tr>

              </thead>

              <tbody>

<!--                 <?php

                  foreach($dsrs_detailed as $value1){

                ?>

                <tr>

                  <td><?= $value1->customer_id?></td>

                  <td><?= $value1->transaction_id?></td>

                  <td><?= $value1->transaction_date?></td>

                  <td><?= $value1->customer_firstname. ' ' .$value1->customer_lastname?></td>

                  <td><?= $value1->transaction_customerstatus?></td>

                  <td><?= $value1->serviceprovider_name?></td>

                  <td><?= $value1->assister_name?></td> 

                  <td><?= $value1->transaction_docketno?></td>

                  <td><?= $value1->transaction_orno?></td>

                  <td><?= $value1->productservice_description?></td>

                  <td><?= $value1->productretail_name?></td>

                  <td><?= number_format($value1->transactiondetails_totalsales, 2, '.', ',')?></td>

                </tr>

                <?php

                  }

                ?>
 -->
              </tbody>

            </table>

          </div>

        </div>

      </div>

      <hr>
      
      <!--<div style="display: none;">-->
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="display-6" style="color: blue;"><?= $user->branch_name?></h2>

          <button id="compute_summary" type="button" class="btn btn-primary hide" style="width: 200px; height: 50px;" onclick="compute_summary()">Compute Summary</button>
          <p id="time_info" class="hide" style="font-size: 16px; margin-top: 10px;">(May take 5-15mins to complete)</p>
       
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_sales"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales (OTC): </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_sales_otc"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Cash: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_cash"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Sales (Services): </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_sales_services"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Card: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_card"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total GC: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="total_gc"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
      </div>

      <hr>
      

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Walk-In: </strong> <span id="total_walkin" class="pull-right text-center" style="margin-right: 50px;"><?= 0?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Turnaway: </strong> <span id="total_turnaway" class="pull-right text-center" style="margin-right: 50px;"><?=   0?></span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Transfer: </strong> <span id="total_transfer" class="pull-right text-center" style="margin-right: 50px;"><?=   0?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>Total Head Count: </strong> <span id="total_head_count" class="pull-right text-center" style="margin-right: 50px;"><?=  0?></span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">

            <h4 class="display-4"><strong>Total Regular: </strong> <span id="total_regular" class="pull-right text-center" style="margin-right: 50px;"><?=  0?></span></span></h4>
        </div>
      </div>

      <hr>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date Total Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="todate_total_sales"><?=  number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date OTC: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="todate_otc"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date Services Sales: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="todate_services"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
        <div class="col-md-6">
            <h4 class="display-4"><strong>To-Date TPH: </strong> <span style="margin-right: 50px;" class="pull-right text-center">&#8369;<span id="todate_tph"><?= number_format(bcadd(0.00, 0.00, 2), 2, '.', ',')?></span></span></h4>
        </div>
      </div>

      <!--</div>-->

    </div>

    <!-- /.box-body -->

  </form>

</div>

<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
      <div class="modal-dialog">
        <div class="modal-content" style="background: none !important;">
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
                <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
                <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
                  </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
                  </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
                  </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
                  </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
                  </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
                  </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
                  </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
                  </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
                  </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
                  </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
                  </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
                  </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
                  </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
                  </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
                  </circle>
                </svg>
              </div>
              
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/js/jszip.min.js"></script>
<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>


<script src="<?= base_url()?>assets/customs/js/report/dsr_detailed_branch.js?v=1.0.38"></script>



