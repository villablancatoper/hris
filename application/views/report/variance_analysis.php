<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 

    .hide{

        display: none;

    }

    .shown{

        display: all;

    }

</style>



<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
            <div class="col-lg-12">

                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                <?php } ?>

                </select>

                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Branch</option> 



                </select>

                <select id="month_year" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Month</option> 
                    <!--                <option>June 2019</option>
                    <option>June 2019</option>
                    <option>June 2019</option>
                    <option>June 2019</option>
                    <option>June 2019</option>
                    <option>June 2019</option> -->
                    <option data-year = "2019" data-month = "12">December 2019</option>
                    <option data-year = "2020" data-month = "01">January 2020</option>
                    <option data-year = "2020" data-month = "02">February 2020</option>


                </select>


                <button onclick="compute();" class="btn btn-primary match-height" id="compute">Search

                </button>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-12">

                <div class="table-responsive shown">

                    <table id="variance_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th>Category</th>

                                <th>Item Name</th>

                                <th>Item Model</th>

                                <th>Item Color</th>

                                <th>Item Shade</th>

                                <th>UOM</th>

                                <th>Beginning</th>

                                <th>Delivered</th>

                                <th>Ending</th>

                                <th>Actual Usage</th>
                                <th>Theoretical</th>
                                <th>Variance</th>



                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>


            </div>
        </div>

 

    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="start1">

    <input type="hidden" id="end1">

    <!-- /.box-body -->



    </form>



</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?php echo base_url(); ?>assets/customs/js/report/variance_analysis.js?v=1.0.1"></script>


</div>



<!-- /.content-wrapper -->

