<style>
  .dataTables_filter {
    display: none;
  }
  .table-hover{
    cursor: pointer;
  }

  @media (min-width: 992px){
      #customer_table_new_wrapper{
          border-right: 1px solid #eee; 
      }
  }
  th{
    vertical-align: middle !important;
  }
  .select2-selection{
    margin-bottom: 3px !important;
  }
</style>

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">

<div class="box box-primary">
  <!-- /.box-header -->
  <!-- form start -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
            <h4 class="display-5" style="margin: 5px 0px 0px 0px !important;">Overall Report</h4>
        </div>
      </div>
      <hr style="margin-bottom: 0;">
      <div class="row">
        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">
          <div class="table-responsive">
            <table id="overall_report_table" class="table table-bordered table-hover" style="width: 100% !important;">
              <thead>
              <tr>
                <th>Total Sales</th>
                <th>Total Product Services</th>
                <th>Total Product Retails</th>
                <th>Total Cash</th>
                <th>Total Credit</th>
                <th>Total GC</th>
                <th>Total Turn Aways</th>
              </tr>
              </thead>
              <tbody>
                <?php
                  foreach($overall_report as $value1){
                ?>
                <tr>
                  <td><?= $value1->total_sales?></td>
                  <td><?= $productservices_count?></td>
                  <td><?= $productretails_count?></td>
                  <td><?= $value1->total_cash?></td>
                  <td><?= $value1->total_credit?></td>
                  <td><?= $value1->total_gc?></td>
                  <td><?= $turnaway_count?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </form>
</div>


<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/customs/js/report/overall_report_branch.js"></script>

