<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .align-right{
    text-align: right !important;
  }

  .dt-buttons{
      text-align: right;
  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">

          <select id="brands" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Brand</option>

              <?php

                foreach($brands as $brand){

              ?>

              <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

              <?php  

                }

              ?>

          </select>

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <button type="button" class="btn btn-default match-height" id="daterange_btn" style="margin-bottom: 3px;">

              <span>

                <i class="fa fa-calendar"></i> Select date

              </span>

              <i class="fa fa-caret-down"></i>

          </button>

          <button class="btn btn-primary btn-flat match-height" onclick="search_overall_report()" style="margin-bottom: 3px;">Search</button>

          <input type="hidden" id="start">

          <input type="hidden" id="end">

        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="overall_report_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <!-- <th>Date</th> -->

                <th>Branch</th>

                <th>Total Sales</th>

                <th>Total Product Services</th>

                <th>Total Product Retails</th>

                <th>Total Cash</th>

                <th>Total Credit</th>

                <th>Total GC</th>

                <th>Total Turn Aways</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/js/jszip.min.js"></script>
<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>

<script src="<?= base_url()?>assets/customs/js/report/overall_report_ho.js?v=1.0.2"></script>



