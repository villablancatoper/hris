<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 

    .hide{

        display: none;

    }

    .shown{

        display: all;

    }

</style>







<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <div class="row">
            <div class="col-lg-12">

                <!-- <div class="col-md-12"> -->

                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                <?php } ?>

                </select>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: -10px !important; width: 230px !important;"> -->

                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Branch</option> 

                </select>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: -10px !important; width: 230px !important;"> -->

                <select id="commission_type" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Commission Type</option>

                <option value="all">All</option> 

                <option value="service">Service Incentives</option> 

                <option value="otc">OTC Sales</option> 

                </select>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: -10px !important; width: 250px !important;"> -->

                <button type="button" class="btn btn-default" id="daterange_btn">

                <span id="category_date">

                <i class="fa fa-calendar" ></i> Select Date

                </span>

                <i class="fa fa-caret-down"></i>

                </button>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: 15px !important; width: 250px !important;"> -->

                <button type="button" class="btn btn-default match-height hide" id="daterange_btn1" style="margin-bottom: 3px;  font-size: 12px; height: 35px !important; width: 250px !important;">

                <span>

                <i class="fa fa-calendar"></i> Select date (OTC Commission)

                </span>

                <i class="fa fa-caret-down"></i>

                </button>

                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-left: 15px !important;"> -->

                <button onclick="compute();" class="btn btn-primary match-height" id="compute">Compute

                </button>

            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-lg-12">

                <div id="non_sb_table" class="table-responsive shown">

                    <table id="commission_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th>Brand</th>

                                <th>Branch Name</th>

                                <th>Service Provider</th>

                                <th>Total Services Sales</th>

                                <th>Total OTC Sales</th>

                                <th>Services Incentives</th>

                                <th>OTC Commission</th>

                                <th>Total Incentives/Commission</th>

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

                <div id="sb_table" class="table-responsive hide">

                    <table id="commission_table_sb" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th>Branch</th>

                                <th>ID</th>

                                <th>Name </th>

                                <th>Services</th>

                                <th>OTC</th>

                                <th>Total Sales</th>

                                <th>VAT 12%</th>

                                <th>Assist Commi</th>
                                <th>Product Cost</th>
                                <th>Chair Rental</th>
                                <th>Non-Operation Penalty</th>
                                <th>Incomplete Operation Penalty</th>
                                <th>Due to Lessee</th>

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>


                    </table>

                </div>

            </div>
        </div>

 





    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="start1">

    <input type="hidden" id="end1">

    <!-- /.box-body -->



    </form>



</div>





















<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>





<script src="<?php echo base_url(); ?>assets/customs/js/report/commission.js?v=1.0.1"></script>





</div>



<!-- /.content-wrapper -->

