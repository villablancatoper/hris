<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .align-right{
    text-align: right !important;
  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">

            <h4 class="display-5" style="margin: 5px 0px 0px 0px !important;">Daily Sales Report</h4>

        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="dsr_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>Customer</th>

                <th>Docket No</th>

                <th>OR No</th>

                <!-- <th>SP</th> -->

                <th>Total Sales</th>

                <th>Cash</th>

                <th>Card</th>

                <th>GC</th>

              </tr>

              </thead>

              <tbody>

                <?php

                  foreach($dsrs as $value1){

                ?>

                <tr onclick="edit_transaction(<?= $value1->transaction_id?>, <?= $value1->customer_id?>)">

                  <td><?= $value1->customer_firstname. ' ' .$value1->customer_lastname?></td>

                  <td><?= $value1->transaction_docketno?></td>

                  <td><?= $value1->transaction_orno?></td>

                  <td><?= '&#8369;'.number_format($value1->transaction_totalsales, 2, '.', ',');?></td>

                  <td><?= '&#8369;'.number_format($value1->transaction_paymentcash, 2, '.', ',');?></td>

                  <td><?= '&#8369;'.number_format($value1->transaction_paymentcard, 2, '.', ',')?></td>

                  <td><?= '&#8369;'.number_format($value1->transaction_paymentgc, 2, '.', ',')?></td>

                </tr>

                <?php

                  }

                ?>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/report/dsr_branch.js?v=1.0"></script>



