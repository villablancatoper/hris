<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <?php

            if($user->brand_id == 100007 || $user->user_position == "Area Manager"){

        ?>

        <div class="col-md-12">

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;"  style="margin-bottom: 10px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <label>

            <input id="search_box" type="search" class="form-control input-sm match-height" placeholder="Item Name or Code">

          </label>

          <button class="btn btn-primary btn-flat match-height" onclick="search_currentinventory()" style="margin-bottom: 3px;">Search</button>

          <!-- <button class="btn btn-info" onclick="refresh()" style="margin-bottom: 3px;"><i class="fa fa-refresh"></i></button> -->

        </div>

        <?php }else{?>

        <div class="col-md-12">

            <strong>Search: </strong>

            <label>

              <input id="search_box" type="search" class="form-control input-sm" placeholder="Item Name or Code" style="height: 34px !important;">

            </label>

            <button class="btn btn-primary btn-flat" onclick="search_currentinventory()" style="margin-left: -7px;">Search</button>

            <!-- <button class="btn btn-info" onclick="refresh()"><i class="fa fa-refresh"></i></button> -->



            <!-- <a class="btn btn-primary pull-right" href="javascript: test()" >New Customer</a> -->

        </div>

        <?php }?>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="currentinventory_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID No.</th>

                <th>Category</th>

                <th>Code</th>

                <th>Branch</th>

                <th>Name</th>

                <th>Description</th>

                <th>CRM Qty</th>

                <th>CRM UOM</th>
                
                <th>SAP Qty</th>
                
                <th>SAP UOM</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/inventory/index.js?v=1.0.4"></script>



