<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #actual_items_wrapper{

          border-left: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }



  .centered-modal.in {

      display: flex !important;

  }

  .centered-modal .modal-dialog {

      margin: auto;

  }



  .centered-modal .modal-content{

      -webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -moz-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -o-box-shadow: 0 5px 15px rgba(0,0,0,0);

      box-shadow: 0 5px 15px rgba(0,0,0,0);

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  input[type=number]::-webkit-inner-spin-button, 

    input[type=number]::-webkit-outer-spin-button { 

    -webkit-appearance: none; 

    margin: 0; 

    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/

bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

          <div class="col-md-12" style="margin-bottom: 20px;">

              <a class="btn btn-primary" href="<?= base_url()?>inventory/receive">Go Back</a>
          </div>

          <div class="col-md-12" style="margin-bottom: 20px;">
          
            <div class="row">

              <div class="col-md-6">
                
                <label for="">No</label>

                <input id="" name="" class="form-control" type="text" disabled="disabled" value="<?= $dr->deliveryreceipt_no?>"> 

              </div>

              <div class="col-md-6">
                
                <label for="">Date Packed</label>

                <input id="" name="" class="form-control" type="text" disabled="disabled" value="<?= date('m/d/Y', strtotime($dr->deliveryreceipt_packed))?>"> 

              </div>


            </div>

            <div class="row">

              <div class="col-md-6">
                
                <label for="">Pickup date</label>

                <input id="" name="" class="form-control" type="text" disabled="disabled" value="<?= date('m/d/Y', strtotime($dr->deliveryreceipt_pickup))?>"> 

              </div>

              <div class="col-md-6">
                
                <label for="">Target Date</label>

                <input id="" name="" class="form-control" type="text" disabled="disabled" value="<?= date('m/d/Y', strtotime($dr->deliveryreceipt_date))?>"> 

              </div>


            </div>

          </div>

      </div>

      <hr style="margin: 0;">

      <div class="row">

        <div class="col-md-6" id="expected_items_wrapper" style="padding-bottom: 5px;">

          <h3 class="display-5">Expected Items</h3>

          <input id="deliveryreceipt_id" type="hidden" value="<?= $dr->deliveryreceipt_id?>">

          <?php

            foreach($drd as $value1){

          ?>

          <div class="row" style="margin-bottom: 5px;">

            <div class="col-md-9">

              <input type="text" class="form-control" disabled="disabled" value="<?= $value1->item_description?>">

            </div>

            <div class="col-md-3">

              <input id="dr_id[<?= $value1->deliveryreceiptdetail_id?>]" type="text" class="form-control" disabled="disabled" value="<?= number_format($value1->deliveryreceiptdetail_quantity, 0)?>">

            </div>

          </div>

          <?php

            }

          ?>

        </div>

        <div class="col-md-6" id="actual_items_wrapper" style="padding-bottom: 5px;">

          <h3 class="display-5">Actual Items</h3>

          <?php

            foreach($drd as $value2){

          ?>

          <div class="row" style="margin-bottom: 5px;">

            <div class="col-md-9">

              <input type="text" class="form-control" disabled="disabled" value="<?= $value2->item_description?>">

            </div>

            <div class="col-md-3">

              <input id="drd_id[<?= $value2->deliveryreceiptdetail_id?>]" name="drd_id[<?= $value2->deliveryreceiptdetail_id?>]" type="number" class="form-control" value="<?= number_format('0.00', 0)?>">

            </div>

          </div>

          <?php

            }

          ?>

        </div>

      </div>

      <hr style="margin: 0px 0px 10px 0px;">

      <div class="row">

        <div class="col-md-6">

          <input id="dr_date_received" name="" class="form-control datepicker" type="text" placeholder="Date Received"> 

        </div>
        
        <div class="col-md-6">


          <button class="btn btn-success pull-right" onclick="receive_dr()">Submit</button>

        </div>

      </div>


    </div>

    <!-- /.box-body -->

  </form>

</div>





<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/inventory/receive_dr.js?v=1.0.0"></script>



