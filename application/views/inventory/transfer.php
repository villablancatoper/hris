<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }



  .centered-modal.in {

      display: flex !important;

  }

  .centered-modal .modal-dialog {

      margin: auto;

  }



  .centered-modal .modal-content{

      -webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -moz-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -o-box-shadow: 0 5px 15px rgba(0,0,0,0);

      box-shadow: 0 5px 15px rgba(0,0,0,0);

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <?php

            if($user->brand_id == 100007 || $user->user_position == "Area Manager"){

        ?>

        <div class="col-md-12">

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <label>

            <input id="search_box" type="search" class="form-control input-sm match-height" placeholder="Date or Status">

          </label>

          <button class="btn btn-primary btn-flat match-height" onclick="search_transfer_requests()">Search</button>

          <button class="btn btn-info" onclick="get_transfer_requests()"><i class="fa fa-refresh"></i></button>

        </div>

        <?php }else{?>

        <div class="col-md-12">

            <strong>Search: </strong><label><input id="search_box" type="search" class="form-control input-sm" placeholder="Date or Status" style="height: 34px !important;"></label><button class="btn btn-primary btn-flat" onclick="search_transfer_requests()">Search</button>

            <button class="btn btn-info" onclick="get_transfer_requests()"><i class="fa fa-refresh"></i></button>

            <?php

              if($user->user_role == 'Standard User' && $user->user_position != 'Area Manager'){

            ?>

            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_request_modal">Request Transfer</button>

            <?php

              }

            ?>

        </div>

        <?php }?>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;">

          <div class="table-responsive">

            <table id="transferrequest_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID No.</th>

                <th>Date Requested</th>

                <th>From</th>

                <th>To</th>

                <th>Status</th>

                <th>Action</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>



<div class="modal fade" id="transfer_request_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="transfer_request_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Request Info</h4>

      </div>

      <div class="modal-body">

        <div id="transfer_request_alert_success" class="alert alert-success alert-dismissible">

          <h4><i class="icon fa fa-check"></i> Success!</h4>

          You have successfully <span class="label label-info" id="transfer_request_function" style="font-size: 12px; margin-left: 5px;"></span> to branch:  <span id="transfer_request_function_name" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>

        </div>

        <form id="transfer_request_form" method="POST">

          <div class="row">

            <div class="col-md-6">

              <label for="transfer_request_from">From</label>

              <input type="hidden" name="" id="from">

              <input id="transfer_request_from" name="transfer_request_from" type="text" class="form-control" disabled="disabled">

              <input id="transfer_request_id" type="hidden">

            </div>

            <div class="col-md-6">

              <label for="transfer_request_to">To</label>

              <input type="hidden" name="" id="to">

              <input id="transfer_request_to" name="transfer_request_to" type="text" class="form-control" disabled="disabled">

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-md-12">

              <div class="table-responsive">

                <table id="transfer_item_table" class="table table-bordered table-hover" style="width: 100% !important;">

                  <thead>

                  <tr>

                    <th>ID</th>

                    <th>Item</th>

                    <th>UOM</th>

                    <th>Quantity</th>

                  </tr>

                  </thead>

                  <tbody>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

          <hr>



          <div id="response">



          </div>



          <div class="row">

            <div class="col-md-12">

              <div class="box" style="border-top: 3px solid white !important; margin-top: 10px;">

              <div class="box-header with-border">

                <h3 class="box-title">Request History</h3>



                <div class="box-tools pull-right">

                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>

                  </button>

                </div>

              </div>

              <!-- /.box-header -->

              <div class="box-body" id="request_history">

                

              </div>

              <!-- /.box-body -->

            </div>

            </div>

          </div>

        </form>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade" id="add_request_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="add_request_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Request Form</h4>

      </div>

      <div class="modal-body">

        <div id="add_request_alert_success" class="alert alert-success alert-dismissible">

          <h4><i class="icon fa fa-check"></i> Success!</h4>

          You have successfully <span class="label label-info" id="add_request_function" style="font-size: 12px; margin-left: 5px;"></span> to branch:  <span id="add_request_function_name" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>

        </div>

        <form id="add_request_form">

          <div id="add_request_error"></div>

          <div class="row">

            <div class="col-md-6">

              <label for="add_request_from">From</label>

              <select id="add_request_from" name="add_request_from" class="form-control select2" style="width: 100%;" disabled="disabled">

                <option value="" hidden selected>Select branch</option>

                <?php

                  foreach($branches as $branch){

                ?>

                <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                <?php

                  }

                ?>

              </select>

            </div>

            <div class="col-md-6">

              <label for="add_request_to">To</label>

              <select id="add_request_to" name="add_request_to" class="form-control select2" style="width: 100%;" disabled="disabled">

                <option value="" hidden selected>Select branch</option>

                <?php

                  foreach($branches as $branch){

                ?>

                <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                <?php

                  }

                ?>

              </select>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-md-7">

            <label for="quantity">Item</label>

              <select id="add_request_items" name="add_request_to" class="form-control select2" style="width: 100%;">

                <option value="" hidden selected>Select item</option>

                <?php

                  foreach($items as $item){

                ?>

                <option value="<?= $item->item_id?>"><?= $item->item_description?></option>

                <?php

                  }

                ?>

              </select>

            </div>

            <div class="col-md-5">

              <label for="add_request_quantity">Quantity</label>

              <input id="add_request_quantity" name="add_request_quantity" type="number" class="form-control" >

            </div>

          </div>

          <div class="row">

            <div class="col-md-12" style="margin-top: 10px;">

              <button id="add_request_add_item" type="button" onclick="add_item()" class="btn btn-danger  btn-flat btn-sm pull-right">Add</button>

            </div>

          </div>

          <hr>

          <div class="row">

            <div class="col-md-12">

              <div class="table-responsive">

                <table id="add_transfer_item_table" class="table table-bordered table-hover" style="width: 100% !important;">

                  <thead>

                  <tr>

                    <th>Item ID</th>

                    <th>Item</th>

                    <th>UOM</th>

                    <th>Quantity</th>

                    <th>Action</th>

                  </tr>

                  </thead>

                  <tbody>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </form>

      </div>

      <div class="modal-footer">

        <button id="add_request_button" type="button" onclick="add_request()" class="btn btn-success">Submit

        </button>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content" style="background: none !important;">

      <div class="modal-body">

        <div class="row">

          <div class="col-md-12 text-center">

            <h3 class="display-5" style="color: white !important">&nbsp;Loading...</h3>

            <svg class="lds-default" width="30%" height="30%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>

              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>

              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>

              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>

              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>

              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>

              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>

              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>

              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>

              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>

              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>

              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>

              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>

              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>

              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>

              </circle>

            </svg>

          </div>

          

        </div>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>







<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/inventory/transfer.js?v=1.0.7"></script>



