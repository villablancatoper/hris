<style>
  .dataTables_filter {
    display: none;
  }
  .table-hover{
    cursor: pointer;
  }

  @media (min-width: 992px){
      #dr_table_wrapper{
          border-left: 1px solid #eee; 
      }
  }
  th{
    vertical-align: middle !important;
  }

  .centered-modal.in {
      display: flex !important;
  }
  .centered-modal .modal-dialog {
      margin: auto;
  }

  .centered-modal .modal-content{
      -webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);
      -moz-box-shadow: 0 5px 15px rgba(0,0,0,0);
      -o-box-shadow: 0 5px 15px rgba(0,0,0,0);
      box-shadow: 0 5px 15px rgba(0,0,0,0);
  }
  .select2-selection{
    margin-bottom: 3px !important;
  }
</style>

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/
bootstrap-datepicker.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">

<div class="box box-primary">
  <!-- /.box-header -->
  <!-- form start -->
    <div class="box-body">
      <div class="row">
          <div class="col-md-12" style="margin-bottom: 20px;">
              <a class="btn btn-primary" href="<?= base_url()?>inventory/receive">Go Back</a>
          </div>
      </div>
      <hr style="margin: 0;">
      <div class="row">
        <div class="col-md-6" style="padding-top: 20px;">
          <div class="row">
            <div class="col-md-6">
              <label for="add_dr_branch">Branch</label>
              <select id="add_dr_branch" name="add_dr_branch" class="form-control select2" style="width: 100%;">
                <option value="" hidden selected>Select branch</option>
                <?php
                  foreach($branches as $branch){
                ?>
                <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>
                <?php
                  }
                ?>
              </select>
            </div>
            <div class="col-md-6">
              <label for="add_dr_date">Expected Date</label>
              <input id="add_dr_date" name="add_dr_date" class="form-control" type="text">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
              <label for="add_dr_remarks">Remarks</label>
              <textarea id="add_dr_remarks" name="add_dr_remarks" class="form-control" rows="3" ></textarea>
            </div>
          </div>
          <hr class="dr-item" hidden style="margin-bottom: 0px;">
          <div class="row dr-item" hidden>
            <div class="col-md-12">
              <h3 class="display-5">SAP Items</h3>
              <div class="table-responsive" style="padding-top: 20px; border-right: 1px solid white !important;">
                <table id="pdn1_table" class="table table-bordered table-hover" style="width: 100% !important;">
                  <select id="type_of_dr" class="pull-right form-control" style="width: 96px !important;">
                    <option value="PDN1" selected>Default</option>
                    <option value="WTR1">Transfer</option>
                  </select>
                  <thead>
                  <tr>
                    <th>DocEntry</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Unit Msr</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6" id="dr_table_wrapper">
          <h3 class="display-5">Delivery Items</h3>
          <div class="table-responsive" style="padding-top: 20px; border-right: 1px solid white !important;">
            <table id="add_dr_table" class="table table-bordered table-hover" style="width: 100% !important;">
              <thead>
              <tr>
                <th>DocEntry</th>
                <th>Code</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Unit Msr</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            <hr>
            <button onclick="create_dr()" class="btn btn-success pull-right">Create</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </form>
</div>


<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/customs/js/inventory/create_dr.js"></script>

