<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }



  .centered-modal.in {

      display: flex !important;

  }

  .centered-modal .modal-dialog {

      margin: auto;

  }



  .centered-modal .modal-content{

      -webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -moz-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -o-box-shadow: 0 5px 15px rgba(0,0,0,0);

      box-shadow: 0 5px 15px rgba(0,0,0,0);

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <!-- <div class="row"> -->

        <!-- <div class="col-md-12">

          <select id="brands" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Brand</option>

              <?php

                foreach($brands as $brand){

              ?>

              <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

              <?php  

                }

              ?>

          </select>

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <button type="button" class="btn btn-default match-height" id="daterange_btn" style="margin-bottom: 3px;">

              <span>

                <i class="fa fa-calendar"></i> Select date

              </span>

              <i class="fa fa-caret-down"></i>

          </button>

          <button class="btn btn-primary btn-flat match-height" onclick="search_drs()" style="margin-bottom: 3px;">Search</button>

          <a class="btn btn-primary pull-right" href="<?= base_url()?>inventory/create_dr">Create DR</a>

        </div>

      </div>

      <hr style="margin-bottom: 0;"> -->

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;">

          <!-- <input type="hidden" name="" id="start">

          <input type="hidden" name="" id="end"> -->

          <div class="table-responsive">

            <table id="deliveryreceipt_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>Date</th>

                <th>Branch</th>

                <th>Remarks</th>

                <th>Status</th>

                <th>Action</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>



<div class="modal fade" id="dr_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="dr_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">DR Info</h4>

      </div>

      <div class="modal-body">

        <div class="row">

          <div class="col-md-6">

            <label for="dr_branch">Branch</label>

            <input id="dr_branch" name="dr_branch" class="form-control" type="text" disabled="disabled">

          </div>

          <div class="col-md-6">

            <label for="dr_date">Expected Date</label>

            <input id="dr_date" name="dr_date" class="form-control" type="text" disabled="disabled">

          </div>

        </div>

        <div class="row">

          <div class="col-md-12" style="margin-top: 10px;">

            <label for="dr_remarks">Remarks</label>

            <textarea id="dr_remarks" name="dr_remarks" class="form-control" rows="3" disabled="disabled"></textarea>

          </div>

        </div>

        <hr>

        <div class="row">

          <div class="col-md-12">

            <div class="table-responsive">

              <table id="drd_items_table" class="table table-bordered table-hover" style="width: 100% !important;">

                <thead>

                <tr>

                  <th>DocEntry</th>

                  <th>Code</th>

                  <th>Description</th>

                  <th>Quantity</th>

                  <th>Unit Msr</th>

                </tr>

                </thead>

                <tbody>

                </tbody>

              </table>

            </div>

          </div>

        </div>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content" style="background: none !important;">

      <div class="modal-body">

        <div class="row">

          <div class="col-md-12 text-center">

            <h3 class="display-5" style="color: white !important">&nbsp;Loading...</h3>

            <svg class="lds-default" width="30%" height="30%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>

              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>

              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>

              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>

              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>

              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>

              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>

              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>

              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>

              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>

              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>

              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>

              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>

              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>

              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>

              </circle>

            </svg>

          </div>

          

        </div>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<script src="<?= base_url()?>assets/customs/js/inventory/view_dr_branch.js?v=1.0.0"></script>



