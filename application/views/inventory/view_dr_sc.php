<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }



  .centered-modal.in {

      display: flex !important;

  }

  .centered-modal .modal-dialog {

      margin: auto;

  }



  .centered-modal .modal-content{

      -webkit-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -moz-box-shadow: 0 5px 15px rgba(0,0,0,0);

      -o-box-shadow: 0 5px 15px rgba(0,0,0,0);

      box-shadow: 0 5px 15px rgba(0,0,0,0);

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .modal {
    text-align: center;
    padding: 0!important;
  }

  .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
  }

  .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
  }

  @media screen {
    #printSection {
        display: none;
    }
  }

  @media print {
    body * {
      visibility:hidden;
    }
    #printSection, #printSection * {
      visibility:visible;
    }
    #printSection {
      margin-left: 0;
      position:absolute;
      left:0;
      top:0;
    }
    .print-title{
      display: block;
    }

    .table>tbody>tr>td{
      padding: 0;
      vertical-align: middle;
      text-align: center;
    }

    .table>thead:first-child>tr:first-child>th {
      padding: 0;
      vertical-align: middle;
      text-align: center;
    }

    .print-branch, .print-date, .print-user, .print-position, .print-no, .print-packeddate, .print-pickup{
      color: red !important;
    }

    .print-header{
      background: pink !important;
    }
    .table-bordered>thead>tr>th, .table-bordered {
      border: 1px solid #777575;
    }

    #print_drd_table_2{
      margin-bottom: 3px !important;
    }
  }

  .print-branch{
    width: 200px;
    background: none;
    border: none;
    border-bottom: 1px solid black;
    font-size: 11px;
    color: red;
    padding: 0px;
    font-weight: bold;
  }

  .print-date, .print-no, .print-packeddate, .print-pickup{
    width: 90px;
    background: none;
    border: none;
    border-bottom: 1px solid black;
    font-size: 10px;
    color: red;
    padding: 0px 0px;
    font-weight: bold;
  }

  .print-receiver{
    width: 170px;
    background: none;
    border: none;
    border-bottom: 1px solid black;
    font-size: 14px;
    color: red;
    padding: 0px 0px;
    height: 28px;
    font-weight: bold;
  }

  .print-packed, .print-delivered{
    width: 155px;
    background: none;
    border: none;
    border-bottom: 1px solid black;
    font-size: 14px;
    color: red;
    padding: 0px 0px;
    height: 5px;
    font-weight: bold;
  }

  .print-user, .print-position{
    width: 477px;
    background: pink;
    border: none;
    font-size: 9px;
    color: red;
    padding: 0px 0px;
    height: 9px;
    font-weight: bold;
  }

  .print-receiveddate{
    width: 85px;
    background: none;
    border: none;
    border-bottom: 1px solid black;
    font-size: 14px;
    color: red;
    padding: 0px 0px;
    height: 28px;
    font-weight: bold;
  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">

          <select id="brands" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Brand</option>

              <?php

                foreach($brands as $brand){

              ?>

              <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

              <?php  

                }

              ?>

          </select>

          <select id="branches" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

              <option value="" hidden selected>Select Branch</option>

              <?php

                foreach($branches as $branch){

              ?>

              <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>            

              <?php  

                }

              ?>

          </select>

          <button type="button" class="btn btn-default match-height" id="daterange_btn" style="margin-bottom: 3px;">

              <span>

                <i class="fa fa-calendar"></i> Select date

              </span>

              <i class="fa fa-caret-down"></i>

          </button>

          <button class="btn btn-primary btn-flat match-height" onclick="search_drs()" style="margin-bottom: 3px;">Search</button>

          <a class="btn btn-primary pull-right" href="<?= base_url()?>inventory/create_dr_manual">Create DR</a>

        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;">

          <input type="hidden" name="" id="start">

          <input type="hidden" name="" id="end">

          <div class="table-responsive">

            <table id="deliveryreceipt_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID No.</th>

                <th>Date</th>

                <th>Branch</th>

                <th>Remarks</th>

                <th>Status</th>

                <th>Action</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>



<div class="modal fade" id="dr_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <button id="dr_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">DR Info</h4>

      </div>

      <div class="modal-body">

        <div id="printThis">

          <div class="row print-title" style="display: none;">
            <div class="col-md-12">
              <img class="print-img" style="width: 120px !important; margin-left: 195px;" src="">
              <img class="print-img" style="width: 120px !important; margin-left: 410px;" src="">
              <br>
              <hr style="border-top: none; margin-top: 0px; margin-bottom: 10px;">
              <a class="text-center" style="margin: 0px 0px 40px 150px; padding-top: 50px !important;">SUPPLIES ISSUANCE RECEIPT</a>
              <a class="text-center" style="margin: 0px 0px 40px 322px; ">SUPPLIES ISSUANCE RECEIPT</a>
            </div>
            <div class="col-md-12">
              <br>
              
              <label for="dr_branch" style="font-size: 10px;">BRANCH:</label>
              <input name="dr_branch" class="print-branch" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 110px;">NO:</label>
              <input class="print-no" type="text">

              <label for="dr_branch" style="font-size: 10px; margin-left: 60px;">BRANCH:</label>
              <input name="dr_branch" class="print-branch" type="text">
              
              <label for="dr_branch" style="font-size: 8px; margin-left: 110px;">NO:</label>
              <input class="print-no" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 320px;">DATE PACKED:</label>
              <input class="print-packeddate" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 380px;">DATE PACKED:</label>
              <input class="print-packeddate" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 323px;">PICKUP DATE:</label>
              <input class="print-pickup" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 383px;">PICKUP DATE:</label>
              <input class="print-pickup" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 321px;">TARGET DATE:</label>
              <input class="print-date" type="text">

              <label for="dr_branch" style="font-size: 8px; margin-left: 382px; margin-bottom: 15px;">TARGET DATE:</label>
              <input class="print-date" type="text">


              <br>
              <div style="display: inline-table;">
              
                <table id="print_drd_table_1" class="table table-bordered" style="width: 480px !important; font-size: 11px;">

                  <thead>

                    <tr class="print-header">

                      <th>Supplier</th>

                      <th>Description</th>

                      <th>UOM</th>

                      <th>Qty</th>

                    </tr>

                  </thead>

                  <tbody id="print_drd_table_1_tbody">

                  </tbody>

                </table>
            
              </div>

              <div style="display: inline-table; margin-left: 53px;">
              
                <table id="print_drd_table_2" class="table table-bordered" style="width: 480px !important; font-size: 11px; margin-bottom: 3px !important;">

                  <thead>

                    <tr class="print-header">

                      <th>Supplier</th>

                      <th>Description</th>

                      <th>UOM</th>

                      <th>Qty</th>

                    </tr>

                  </thead>

                  <tbody id="print_drd_table_2_tbody">

                  </tbody>

                </table>
            
              </div>
            </div>
            <div class="col-md-12">
              <div style="display: inline-table;">
                <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px;"><strong style="font-size: 8px;">Receive the above items in good order and in accordance with specification. Any complaint must be made within 24 hours, otherwise, the same will be considered valid.</strong></p>
              </div>
              <div style="display: inline-table; margin-left: 53px;">
                <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px;"><strong style="font-size: 8px;">Receive the above items in good order and in accordance with specification. Any complaint must be made within 24 hours, otherwise, the same will be considered valid.</strong></p>
              </div>
            </div>

            <div class="col-md-12">
              <input style="margin-left: 87px;" type="text" class="print-receiver">
              <input style="margin-left: 50px;" type="text" class="print-receiveddate">
              <input style="margin-left: 225px;" type="text" class="print-receiver">
              <input style="margin-left: 50px;" type="text" class="print-receiveddate">
              <br>
              <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 87px;"><strong style="font-size: 7px;">RECEIVED BY(SIGNATURE OVER PRINTED NAME)</strong></p>
              <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 63px;"><strong style="font-size: 7px;">DATE RECEIVED</strong></p>
              <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 241px;"><strong style="font-size: 7px;">RECEIVED BY(SIGNATURE OVER PRINTED NAME)</strong></p>
              <p style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 63px;"><strong style="font-size: 7px;">DATE RECEIVED</strong></p>
              <br>
              <span style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline;"><strong style="font-size: 7px;">Packed By</strong></span>
              <span style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 287px;"><strong style="font-size: 7px;">Delivered By</strong></span>
              <span style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 166px;"><strong style="font-size: 7px;">Packed By</strong></span>
              <span style="width: 480px !important; line-height: .9; text-align: center; margin-bottom: 0px; display: inline; margin-left: 287px;"><strong style="font-size: 7px;">Delivered By</strong></span>
              <br>
              <input type="text" class="print-packed">
              <input type="text" class="print-delivered" style="margin-left: 167px;">
              <input type="text" class="print-packed" style="margin-left: 53px;">
              <input type="text" class="print-delivered" style="margin-left: 167px;">
              <br>
              <input type="text" class="print-user">
              <input type="text" class="print-user" style="margin-left: 57px;">
              <br>
              <input type="text" class="print-position">
              <input type="text" class="print-position" style="margin-left: 57px;">

            </div>
            
          </div>

          <div class="row print-hide">

            <div class="col-md-6">

              <label for="dr_branch">Branch</label>

              <input id="dr_branch" name="dr_branch" class="form-control" type="text" disabled="disabled">

            </div>

            <div class="col-md-6">

              <label for="dr_no">No.</label>

              <input id="dr_no" name="dr_no" class="form-control" type="text" disabled="disabled">

            </div>

          </div>

          <div class="row print-hide">

            <div class="col-md-4" style="margin-top: 10px;">

              <label for="dr_packed">Date Packed</label>

              <input id="dr_packed" name="dr_packed" class="form-control" type="text" disabled="disabled">

            </div>

            <div class="col-md-4" style="margin-top: 10px;">

              <label for="dr_pickup">Pickup Date</label>

              <input id="dr_pickup" name="dr_pickup" class="form-control" type="text" disabled="disabled">

            </div>

            <div class="col-md-4" style="margin-top: 10px;">

              <label for="dr_date">Target Date</label>

              <input id="dr_date" name="dr_date" class="form-control" type="text" disabled="disabled">

            </div>

          </div>

          <div class="row print-hide">

            <div class="col-md-12" style="margin-top: 10px;">

              <label for="dr_remarks">Remarks</label>

              <textarea id="dr_remarks" name="dr_remarks" class="form-control" rows="3" disabled="disabled"></textarea>

            </div>

          </div>

          <hr>

          <div class="row print-hide">

            <div class="col-md-12">

              <div class="table-responsive">

                <table id="drd_items_table" class="table table-bordered table-hover" style="width: 100% !important;">

                  <thead>

                  <tr>

                    <th>Code</th>

                    <th>Description</th>

                    <th>Unit Msr</th>

                    <th>Expected Qty</th>

                    <th>Received Qty</th>

                    <th>Variants</th>

                  </tr>

                  </thead>

                  <tbody>

                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>

          <hr>

          <div class="row">

            <div class="col-md-12">

              <button id="btnPrintDR" class="btn btn-primary pull-right">Print DR</button>
              <!-- <button id="btnPrintRC" class="btn btn-success pull-right">Print Received Copy</button> -->

            </div>

          </div>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content" style="background: none !important;">

      <div class="modal-body">

        <div class="row">

          <div class="col-md-12 text-center">

            <h3 class="display-5" style="color: white !important">&nbsp;Loading...</h3>

            <svg class="lds-default" width="30%" height="30%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>

              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>

              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>

              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>

              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>

              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>

              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>

              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>

              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>

              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>

              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>

              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>

              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>

              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>

              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">

                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>

                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>

              </circle>

            </svg>

          </div>

          

        </div>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<script src="<?= base_url()?>assets/customs/js/inventory/view_dr_sc.js"></script>



