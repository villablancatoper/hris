

<div class="modal fade bd-example-modal-lg" id="services_otc_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <button id="services_otc_modal_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Services / OTC</h4>

      </div>

      <div class="modal-body">
        <?php
          if($user->brand_id == 100001){
        ?>
        <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">New OTCs has been added to the system. Kindly check barcode <strong>50240</strong> - <strong>50257</strong> in "OTC" table below.</p>
        </div>

        <?php
          }
        ?>

        <?php
          if($user->brand_id == 100002){
        ?>
        <!-- <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">New barcode has been created for <strong>The Great Argan Sale</strong>. Kindly check barcode <strong>50116</strong> in "OTC" table below.</p>
        </div> -->

        <?php
          }
        ?>

        <!-- <?php
          if($user->brand_id == 100003){
        ?>
        <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">New barcode has been created for <strong>American Crew</strong>. Kindly check barcode <strong>50101</strong> - <strong>50109</strong> in "OTC" table below.</p>
        </div>

        <?php
          }
        ?> -->

        <?php 
          if($user->brand_id == 100009){
        ?>
        <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">New barcode has been created for <strong>Castor Oil 30 ml</strong> worth P220. Kindly check barcode <strong>50258</strong> in "OTC" table below.</p>
        </div>

        <?php
          }
        ?>

        <?php 
          if($user->brand_id == 100006){
        ?>
        <!-- <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">Prices has been changed on the following services: <br> <strong> Bikini Waxing - 890, Brazilian Waxing - 1090, Full Leg Waxing - 990, Half Leg Waxing - 690, Underarm Waxing - 490, Half Arm Waxing - 490, Foot Reflexology (45 Minutes) - 560, Foot Massage (30 Minutes) - 360</strong></p>
          <br>
          <p style="color: black;">Note: <strong>Incoco OTCs</strong> has been created. Kindly check barcodes <strong>50091</strong> - <strong>50094</strong></p>
          <br>
          <p style="color: black;">Additional Update: 
          <br> <strong>Organic Foot Spa + Pedicure</strong> with a barcode of <strong>10430</strong> has been added to the services table below. 
          <br> <strong>Pedicure Nail Treatment (Breathable)</strong> is now <strong>520</strong> 
          <br> <strong>Eco Colours Hand (Gel Manicure and Hand Spa)</strong> is now <strong>990</strong> 
          <br> <strong>Eyebrow Threading</strong> is now <strong>350</strong> 
          <br> <strong>Lip Threading</strong> is now <strong>200</strong> 
          <br> <strong>Back or Chest Waxing</strong> is now <strong>790</strong> 
          <br> <br> <i>Prices above has been changed as per requested. Any additional corcern about lacking of service barcode, wrong pricing and such, please report to any MIS personnel immediately.</i></p>
        </div> -->

        <!-- <div class="alert alert-warning alert-dismissible text-dark">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
          <p style="color: black;">New barcode has been created for <strong>Footlogix and Footlogix (Intro Price)</strong>. Kindly check barcode <strong>10510 and 10529</strong> in "Services" table below.</p>
        </div> -->
        <?php
          }
        ?>
        <div class="row">
          <div class="col-md-12">
            <h3 class="display-4">Services</h3>
            <div class="table-responsive">

                <table id="services_table" class="table table-bordered table-striped" style="width: 100% !important;">

                <thead>

                  <tr>

                      <th>Code</th>

                      <th>Name</th>

                      <th>Amount</th>

                      <th>Service Validity</th>


                  </tr>

                </thead>

                <tbody>

                </tbody>

                </table>

            </div>
          </div>
        </div>

        <hr>

        <div class="row">
          <div class="col-md-12">
              <h3 class="display-4">OTC</h3>
              <div class="table-responsive">

                  <table id="otc_table" class="table table-bordered table-striped" style="width: 100% !important;">

                  <thead>

                    <tr>

                        <th>Code</th>

                        <th>Name</th>

                        <th>Amount</th>
                        <th>OTC Validity</th>


                    </tr>

                  </thead>

                  <tbody>

                  </tbody>

                  </table>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="change_password_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="change_password_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Change Password</h4>

      </div>

      <div class="modal-body">
        <div id="add_turnaway_alert_success" class="alert alert-success alert-dismissible" style="display: none;">
          <h4><i class="icon fa fa-check"></i> Success!</h4>
          You have successfully change your password!
        </div>
        <div id="input_wrapper" class="row">
            <div class="col-md-12">
                <input id="ip_local" type="hidden" value="">
                <input id="ip_public" type="hidden" value="<?= $_SERVER['REMOTE_ADDR']?>">
                <input type="hidden" id="current_time" name="">
                <input type="hidden" id="current_time_and_date" name="">
                <input type="hidden" id="seconds_workaround" value="0">
                <label for"old_password">Old Password</label>
                <input id="old_password" class="form-control" placeholder="Old Password" type="password"/>
                <label for"new_password">New Password</label>
                <input id="new_password" class="form-control" placeholder="New Password" type="password"/>
                <label for"confirm_password">Confirm Password</label>
                <input id="confirm_password" class="form-control" placeholder="Confirm Password" type="password"/>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-success pull-right" onclick="change_password()">Submit</button>
      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>




<div class="modal fade" id="new_ordering_notice_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button onclick="update_user_notice_status()" id="new_ordering_notice_close_button" type="button" class="close" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">New Ordering Feature starting November 15, 2019</h4>

      </div>

      <div class="modal-body">

        <div class="container-fluid">

          <div class="row">

            <div class="col">
    
              <p>Changes has been made on Job Order > View Entry > Job Order Module. The tutorial below shows how you can encode services with their respective SPs on "Product Services" table. The changes are limited only on "Product Services" table.</p>

              <hr>

              <h4 style="color: red;">1. Text box has been replaced with selection box.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/1.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">2. Instead of entering barcode into the text box, you just have to select an item in the selection box.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/2.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">3a. If <b>ANOTHER</b> selection box appeared, proceed to Step 4.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/3a.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">3a. If <b>NO</b> other selection box appeared, skip to Step 5.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/3b.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">4a. Select an item to be used.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/4a.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">4b. Select SP then click "Add" button.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/4b.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">4c. Finally, you'll see that your selected sp and service has been added to the table.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/4c.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">5a. Select SP then click "Add" button.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/5a.png" alt="" class="img-responsive" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">5b. Finally, you'll see that your selected sp and service has been added to the table.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/new-ordering/5b.png" alt="" class="img-responsive" style="border: 1px solid black">


            </div>

          </div>

        </div>

      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="transferbranch_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button onclick="update_user_notice_status()" id="transferbranch_modal_close" type="button" class="close" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Transfer of Branch Feature</h4>

      </div>

      <div class="modal-body">

        <div class="container-fluid">

          <div class="row">

            <div class="col">
    
              

              <hr>

              <h4 style="color: red;">1. Click "Transfer Branch" in the upper left corner of the screen.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/transfer-branch/1.jpg" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">2. A window will pop up and you have to select the branch you wish to transfer to.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/transfer-branch/2.jpg" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">3. Click "Update" to submit changes.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/transfer-branch/3.jpg" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">4. A success message will show if the process is completed. You will be redirected to login screen and required to relogin to the system.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/transfer-branch/4.jpg" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

              <br>

              <h4 style="color: red;">5. Transfer of branch is available once per day only. Thus, an error message will show if you already transferred to a branch.</h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/transfer-branch/5.jpg" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

  

            </div>

          </div>

        </div>

      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>
<div class="modal fade" id="required_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button onclick="update_user_notice_status()" id="required_modal_close" type="button" class="close" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Customer Entry</h4>

      </div>

      <div class="modal-body">

        <div class="container-fluid">

          <div class="row">

            <div class="col">
    
              <hr>

              <h4 style="color: red;">Starting today, March 05, 2020, Customer Entry requires the selection below. Please refer to the image: </h4>
              <img src="<?= base_url()?>assets/images/tutorial-images/required.png" alt="" class="img-responsive" width = "570" height = "400" style="border: 1px solid black">

              <br>

            </div>

          </div>

        </div>

      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="survey_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title">IT Department Store Survey</h4>

      </div>

      <div class="modal-body">
        <div id="survey_success">
        </div>
        <div class="col-md-12">
          <form id="survey_form">
              <label for="survey_branch">Branch</label>
              <input id="survey_branch" name="survey_branch" class="form-control" type="text" value="<?= $user->branch_name; ?>" disabled>
              <label for="survey_name">Name</label>
              <input id="survey_name" name="survey_name" class="form-control" type="text" value="<?= $user->user_name; ?>" disabled>
              <label for="survey_contact">*Contact</label>
              <input id="survey_contact" name="survey_contact" class="form-control" type="text">
              <label for="internet_survey">*Internet</label>
              <select id="internet_survey" name="internet_survey" class="form-control select2" style="width: 100%;">
                <option value="Okay/Good">Okay/Good</option>
                <option value="Temporary Only">Temporary Only</option>
                <option value="No Internet">No Internet</option>
                <option value="Very Slow Internet">Very Slow Internet</option>
              </select>
              <label for="telephone_survey">*Telephone</label>
              <select id="telephone_survey" name="telephone_survey" class="form-control select2" style="width: 100%;">
                <option value="Okay/Good">Okay/Good</option>
                <option value="No dial tone">No dial tone</option>
                <option value="With static">With static</option>
                <option value="No telephone">No telephone</option>
              </select>
              <label for="terminal_survey">*Card Terminal</label>
              <select id="terminal_survey" name="terminal_survey" class="form-control select2" style="width: 100%;">
                <option value="Okay/Good">Okay/Good</option>
                <option value="Not working">Not working</option>
                <option value="No card terminal">No card terminal</option>
              </select>
              <label for="computer_survey">*PC</label>
              <select id="computer_survey" name="computer_survey" class="form-control select2" style="width: 100%; margin-bottom: 5px;">
                <option value="Okay/Good">Okay/Good</option>
                <option value="Not working">Not working</option>
                <option value="No PC">No PC</option>
                <option value="Very slow PC">Very slow PC</option>
              </select>
              <label for="other_survey">Other Concern (IT)</label>
              <textarea class="form-control" id="other_survey"></textarea>
              <span style="font-size: 11px; color: red;">Note: Fields with * sign are required.</span>
              </div> 
          </form>
        </div>
      <div class="modal-footer">
        <button id="btn_submit_survey" class="btn btn-success pull-right" onclick="submit_survey()" style="margin-top: 5px; width: 100px; ">Submit</button>
      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

</section>

<!-- /.content -->

</div>

<!-- /.content-wrapper -->

<!-- Main Footer -->
    <script src="<?= base_url()?>assets/customs/js/footer.js?v=1.1.83"></script>
    <script src="<?= base_url()?>assets/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js"></script>
  </body>

</html>