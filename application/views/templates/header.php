<!--

BODY TAG OPTIONS:

=================

Apply one or more of the following classes to get the

desired effect

|---------------------------------------------------------|

| SKINS         | skin-blue                               |

|               | skin-black                              |

|               | skin-purple                             |

|               | skin-yellow                             |

|               | skin-red                                |

|               | skin-green                              |

|---------------------------------------------------------|

|LAYOUT OPTIONS | fixed                                   |

|               | layout-boxed                            |

|               | layout-top-nav                          |

|               | sidebar-collapse                        |

|               | sidebar-mini                            |

|---------------------------------------------------------|

-->

<style>

  .skin-black .main-header .navbar {

    background-color: #000000;

  }



  .skin-black .wrapper, .skin-black .main-sidebar, .skin-black .left-side {

    background-color: #333;

  }



  .skin-black .main-header>.logo {

    background-color: #333;

    color: #fff;

    border-bottom: 0 solid transparent;

    border-right: 1px solid #000000;

  }



  .skin-black .main-header .navbar>.sidebar-toggle {

    color: #fff;

    border-right: 1px solid #000000;

  }

  .skin-black .main-header .navbar .navbar-nav>li>a {

    border-right: 1px solid #000000;

  }



  .skin-black .main-header .navbar .navbar-custom-menu .navbar-nav>li>a, .skin-black .main-header .navbar .navbar-right>li>a {

    border-left: 1px solid #000000;

    color: #fff;

    border-right-width: 0;

  }



  #dashboard-menu .col-md-3 .sidebar-menu>li>a {

    padding: 15px;

    display: block;

  }

  @media (min-width: 992px){

    #dashboard-menu .col-md-3 {

        width: 32%;

    }

  }



  .navbar-custom-menu>.navbar-nav>li :hover{

    color: black !important;

  }



  .skin-black .main-header>.logo :hover{

    color: black;

  }



  .navbar-nav>.notifications-menu>.dropdown-menu, .navbar-nav>.messages-menu>.dropdown-menu, .navbar-nav>.tasks-menu>.dropdown-menu {

    padding: 0 0 0 0;

    margin: 0;

    top: 100%;

  }



  #dashboard-menu a{

    color: black;

  }



  #dashboard-menu a:hover{

    color: white;

  }



  .main-footer {

    background: #ecf0f5 !important;

    padding: 15px 15px 0px 15px !important;

    color: #444;

    border-top: #ecf0f5 !important;

  }



  .navbar-nav>.notifications-menu>.dropdown-menu>li .menu>li>a, .navbar-nav>.messages-menu>.dropdown-menu>li .menu>li>a, .navbar-nav>.tasks-menu>.dropdown-menu>li .menu>li>a {

    display: block;

    white-space: normal;

    border-bottom: 1px solid #f4f4f4;

    width: auto !important;

    width: auto;

  }



  .navbar-nav>.notifications-menu>.dropdown-menu, .navbar-nav>.messages-menu>.dropdown-menu, .navbar-nav>.tasks-menu>.dropdown-menu{

    width: auto !important;

  }



  .selected{

    background: #eee;

  }

  #services_table_filter {
    display: block;
  }

  #otc_table_filter{
    display: block;
  }

  .datepicker{
    z-index: 99999 !important;
  }

  #logout{
    color: white;
    height: 50px;
    background-color: Transparent;
    background-repeat: no-repeat;
    border: none;cursor: pointer;
    overflow: hidden;
    outline: none;
  }

  #logout :hover{
    color: white;
  }

</style>



<body class="hold-transition skin-black sidebar-mini" onload="startTime()">

<div class="wrapper">



  <!-- Main Header -->

  <header class="main-header">

  <input type="hidden" id="user_brand_id" value="<?= $user->brand_id?>">

  <input type="hidden" id="branch_batchno" value="<?= $user->branch_batchno?>">

  <input type="hidden" id="maintenance_mode" value = "0">


    <!-- Logo -->

    <a class="logo">

      <!-- mini logo for sidebar mini 50x50 pixels -->

      <span class="logo-mini"><b>C</b>FS</span>

      <!-- logo for regular state and mobile devices -->

      <span class="logo-lg" style="font-size: 15px;"><b><?= $user->brand_name?></b></span>

      <input type="hidden" id="base_url" value="<?= base_url()?>">      

    </a>



    <!-- Header Navbar -->

    <nav class="navbar navbar-static-top" role="navigation">

      <!-- Sidebar toggle button-->

      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">

        <span class="sr-only">Toggle navigation</span>

      </a>


      <ul class="nav navbar-nav">


      <li class="dropdown user user-menu">
        <a href="<?= base_url() ?>digital_forms/company_policies" class="dropdown-toggle">
            <span class="label label-danger" style="top: 7px;">New</span>  
            Company Policy
        </a>
      </li>

      <li class="dropdown tasks-menu">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
          <span class="label label-danger" style="top: 7px;">New</span>  
          Digital Forms
        </a>

        <ul class="dropdown-menu">

          <li>

            <!-- inner menu: contains the actual data -->

            <ul class="menu">

              <li>

                <a href="<?= base_url() ?>digital_forms">
                  <h3 style="margin-top: 10px;" >
                  <span class="label label-danger" style="top: 7px;">For Testing</span>  
                    My Forms/Request

                  </h3>

                </a>

              </li>

              <?php if ($user->user_access_level == '1' || $user->user_access_level == '2') { ?>

                <li>

                  <a href="<?= base_url() ?>digital_forms/approval">

                    <h3 style="margin-top: 10px;">
                    <span class="label label-danger" style="top: 7px;">For Testing</span>  
                      Forms/ Request For Approval
                    </h3>

                  </a>

                </li>

              <?php } ?>

              <li>

                <a href="<?= base_url() ?>digital_forms/dept_forms">
                  <h3 style="margin-top: 10px;">
                  <span class="label label-danger" style="top: 7px;">For Testing</span>  
                    Departmental Forms
                  </h3>

                </a>

              </li>

            </ul>

          </li>

        </ul>

      </li>

</ul>

      <!-- Navbar Right Menu -->

      <div class="navbar-custom-menu">

        <ul class="nav navbar-nav">

          <li>

            <p id="time"></p>

          </li>

          <!-- User Account Menu -->
          
          

            <!-- Menu Toggle Button -->
           
              <li class="dropdown user user-menu">
                <a href="<?= base_url() ?>customer_complaint" class="dropdown-toggle">
                    <span class="label label-danger" style="top: 7px;">New</span>  
                    Customer Complaint Monitoring
                </a>
              </li>
              
        
          <li class="dropdown user user-menu">
            <a href="<?= base_url() ?>ticketing" class="dropdown-toggle">
              <span class="label label-danger" style="top: 7px;">New</span>  
              Ticketing System
            </a>

          </li>

          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <span class="label label-danger" style="top: 7px;">New</span>  
              How To
            </a>
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="javascript: show_new_ordering_modal()">
                      <h3 style="margin-top: 10px;">
                        Job Order New Ordering Feature
                      </h3>
                    </a>
                  </li>
            <?php if ($user->user_id == 14) { ?>
                  <li><!-- Task item -->
                    <a href="javascript: show_transferbranch()">
                      <h3 style="margin-top: 10px;">
                        Transfer of Branch
                      </h3>
                    </a>
                  </li>
                  <!-- end task item -->
            <?php } ?>
                </ul>
              </li>
              <!-- <li class="footer">
                <a href="#">View all tasks</a>
              </li> -->
            </ul>
          </li>

          <li class="dropdown user user-menu">

            <!-- Menu Toggle Button -->

            <a href="javascript: show_change_password_modal()" class="dropdown-toggle">

              Change Password

            </a>

          </li>

          <li class="dropdown user user-menu">

            <!-- Menu Toggle Button -->

            <button id="logout" onclick="logout()" class="dropdown-toggle">

              Logout

            </button>

          </li>



        </ul>

      </div>

    </nav>

  </header>

  <!-- Left side column. contains the logo and sidebar -->



