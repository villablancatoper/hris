
<aside class="main-sidebar">

<!-- sidebar: style can be found in sidebar.less -->

<section class="sidebar">

  <!-- Sidebar user panel (optional) -->

  <div class="user-panel text-center">

    <div class="image">

      <img src="<?= base_url()?>assets/images/brands/<?= $user->brand_image?>"style="max-width: 110px; border-radius: 10%" alt="User Image" class="user-image">

    </div>

    <p id="user_user_name" class="user-panel-name" style="color:white; padding: 10px 0 0 0; margin-bottom: 0px;"><strong><?= $user->user_name?></strong></p>
    <p class="user-panel-position" style="color:white; padding: 0 0 0 0; margin-bottom: 0px;"><i id="user_branch_name"><?= $user->user_position == "Area Manager" ? "Area Manager" : $user->branch_name?></i></p>
        <?php if ($user->user_id == 14) { ?>
          <!-- <a data-target = "#transfer_modal" data-toggle = "modal" href="#transfer_modal"><u>Transfer Branch</u></a> -->
        <?php } ?>
        <?php if ($user->user_position == 'Standard User' && $user->brand_id != 100007) { ?>
           <h5 style="color: white;">Store Budget Folder - Batch <?php echo $user->branch_batchno; ?></h5>
        <?php } ?>

    <input type="hidden" id="user_branch_id" value="<?= $user->branch_id?>">
<!--     <?php if ($user->branch_id == 1) { ?>
        <p style="color: white; margin-top: 5px; margin-bottom: 5px;">Maintenance Mode</p>
        <div class="checkbox">
          <input type="checkbox" data-test = "" id="maintenance" name="maintenance" data-toggle="toggle">
        </div>
    <?php } ?> -->


  </div>



  <!-- Sidebar Menu -->

  <ul class="sidebar-menu" data-widget="tree">

    <li class="header" style="margin-bottom: 10px;">MENU</li>

    <!-- Hard coded access for Store -->

    <?php
      if($user->brand_id != 100007){
    ?>

    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

    <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

        <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

        <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>
        
        <?php
          if($user->user_id == 14) {
        ?>
            <li class="<?= $this->uri->segment(2) == 'deposit' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">
              <a href="<?= base_url()?>joborder/deposit" style="padding-left: 36px;">Deposit</a>
            </li>

        <?php } ?>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?> <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <!--<li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>-->

        <li class=" <?= $this->uri->segment(1) == 'inventory' && ($this->uri->segment(2) == 'receive' || $this->uri->segment(2) == 'create_dr' || $this->uri->segment(2) == 'receive_dr')  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/receive" style="padding-left: 36px;">Inventory Delivery</a></li>


        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

        <?php

          if(date('Y-m-d') > '2019-10-30' && date('Y-m-d') != '2019-11-30' && $user->brand_id != 100007){

        ?>

        <li class=" <?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>"><a href="javascript: allow_passage()" style="padding-left: 36px;">Wall-to-Wall Inventory</a></li>

        <?php

          }
          else{
            
        ?>

          <?php

            if((date('Y-m-d') > '2019-11-30' &&  (date('d') > 15 && date('d') <= 20) ) && $user->branch_batchno == 1){ 

          ?>

          <li class=" <?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>"><a href="javascript: allow_passage()" style="padding-left: 36px;">Wall-to-Wall Inventory</a></li>

          <?php
            
            }

          ?>


          <?php

            if((date('Y-m-d') > '2019-11-30' &&  (date('d') >= 1 && date('d') <= 5) ) && $user->branch_batchno == 2){ 

          ?>

          <li class=" <?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>"><a href="javascript: allow_passage()" style="padding-left: 36px;">Wall-to-Wall Inventory</a></li>

          <?php
            
            }

          ?>

        <?php
            
          }

        ?>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Report Viewer</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">Individual Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>

      </ul>

    </li>

    <li>
      <a href="javascript: show_services_otc_modal()"><i class="fa fa-qrcode"></i> <span>Services / OTC Barcodes 
        <?php
          if($user->brand_id == 100002){
        ?>
        <!-- <span class="label label-warning" style="color: black !important;">1</span> -->
        <?php
          }
        ?>

        <!-- <?php
          if($user->brand_id == 100003){
        ?>
        <span class="label label-warning" style="color: black !important;">1</span>
        <?php
          }
        ?> -->

        <?php
          if($user->brand_id == 100009){
        ?>
        <span class="label label-warning" style="color: black !important;">1</span>
        <?php
          }
        ?>

        <?php
          if($user->brand_id == 100006){
        ?>
        <!-- <span class="label label-warning" style="color: black !important;">1</span>  -->
        <?php
          }
        ?>

        <?php
          if($user->brand_id == 100001){
        ?>
        <span class="label label-warning" style="color: black !important;">1</span> 
        <?php
          }
        ?>
        </span>
      </a>
        
    </li>
  
    <?php }?>


    <!-- Hard coded access for AM -->

    <?php
      if($user->branch_id == 200){ # AM branch_id
    ?>
    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

    <!-- <li class="<?= $this->uri->segment(1) == 'customer' ? 'active' : ''?>"><a href="<?= base_url()?>customer/entry"><i class="fa fa-group"></i> <span>Customer Entry</span></a></li> -->

    <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

        <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

        <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?> <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Report Viewer</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_dsr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_dsr" style="padding-left: 36px;">DSR</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">ISR</a></li>

        <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Daily Sales Report</a></li>

         <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'head_count' ? 'active' : ''?>"><a href="<?= base_url()?>report/head_count" style="padding-left: 36px;">Head Count Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in" style="padding-left: 36px; font-size: 13px;">Converted Walk-in - Per Branch</a></li>
        
        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in_sp' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in_sp" style="padding-left: 36px;">Converted Walk-in - Per SP</a></li>



      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-folder-open"></i> <span>Budget Folder</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">
       
        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/store_budgetfolder" style="padding-left: 36px;">Store Budget Folder</a>
        </li>

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/budgetfolder" style="padding-left: 36px;">Budget Folder</a>
        </li>



      </ul>

    </li>

    <?php
      }
    ?>

    <!-- Hard coded access for OPERATIONS -->

    <?php
      if($user->branch_id == 198){ # Operations branch_id
    ?>
    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

    <!-- <li class="<?= $this->uri->segment(1) == 'customer' ? 'active' : ''?>"><a href="<?= base_url()?>customer/entry"><i class="fa fa-group"></i> <span>Customer Entry</span></a></li>

    <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

        <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

        <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>

      </ul>

    </li> -->

    <!-- <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?> <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

      </ul>

    </li> -->

    <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Repor Viewer</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr_otc' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr_otc" style="padding-left: 36px;">Category Sales Report - OTC</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_dsr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_dsr" style="padding-left: 36px;">DSR</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">ISR</a></li>

        <li class="<?= $this->uri->segment(1) == 'commission' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>commission" style="padding-left: 36px;">Commission Viewer</a></li>

        <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Daily Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'head_count' ? 'active' : ''?>"><a href="<?= base_url()?>report/head_count" style="padding-left: 36px;">Head Count Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in" style="padding-left: 36px; font-size: 13px;">Converted Walk-in - Per Branch</a></li>
        
        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in_sp' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in_sp" style="padding-left: 36px;">Converted Walk-in - Per SP</a></li>
        
        <li class="<?= $this->uri->segment(1) == 'asmsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>asmsales" style="padding-left: 36px;">Sales Report - ASM </a></li>
        <!--<li class="<?= $this->uri->segment(1) == 'budgetfoldersales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>budgetfoldersales" style="padding-left: 36px;">Sales Report - Budgetfolder</a></li>-->
        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_brand_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_brand_sales" style="padding-left: 36px;">Sales Report - Brand </a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_branch_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_branch_sales" style="padding-left: 36px;">Sales Report - Branch </a></li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-folder-open"></i> <span>Budget Folder</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">
        <!-- <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/store_budgetfolder" style="padding-left: 36px;">Store Budget Folder</a>
        </li> -->

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/budgetfolder" style="padding-left: 36px;">Budget Folder</a>
        </li>

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'index' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/index" style="padding-left: 36px;">Budget Folder Inventory</a>
        </li>

      </ul>

    </li>

    <?php
      }
    ?>

    <!-- Hard coded access for AUDIT -->

    <?php
      if($user->branch_id == 211){ # Audit branch_id
    ?>
    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

    <!-- <li class="<?= $this->uri->segment(1) == 'customer' ? 'active' : ''?>"><a href="<?= base_url()?>customer/entry"><i class="fa fa-group"></i> <span>Customer Entry</span></a></li>

    <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

        <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

        <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>

      </ul>

    </li> -->

    <!-- <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?> <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

      </ul>

    </li> -->

    <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Repor Viewer</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_dsr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_dsr" style="padding-left: 36px;">DSR</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">ISR</a></li>

        <li class="<?= $this->uri->segment(1) == 'commission' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>commission" style="padding-left: 36px;">Commission Viewer</a></li>

        <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Daily Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'head_count' ? 'active' : ''?>"><a href="<?= base_url()?>report/head_count" style="padding-left: 36px;">Head Count Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in" style="padding-left: 36px; font-size: 13px;">Converted Walk-in - Per Branch</a></li>
        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in_sp' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in_sp" style="padding-left: 36px;">Converted Walk-in - Per SP</a></li>
        
        <li class="<?= $this->uri->segment(1) == 'asmsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>asmsales" style="padding-left: 36px;">Sales Report - ASM </a></li>
        <!--<li class="<?= $this->uri->segment(1) == 'budgetfoldersales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>budgetfoldersales" style="padding-left: 36px;">Sales Report - Budgetfolder</a></li>-->
        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_brand_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_brand_sales" style="padding-left: 36px;">Sales Report - Brand </a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_branch_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_branch_sales" style="padding-left: 36px;">Sales Report - Branch </a></li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-folder-open"></i> <span>Budget Folder</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">
        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/store_budgetfolder" style="padding-left: 36px;">Store Budget Folder</a>
        </li>

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/budgetfolder" style="padding-left: 36px;">Budget Folder</a>
        </li>

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'index' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/index" style="padding-left: 36px;">Budget Folder Inventory</a>
        </li>

        <?php 
          if ($user->user_id == 292) {
          ?>
          <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>budgetfolder/categorysalesforecast" style="padding-left: 36px;">Category Sales Forecast</a>
          </li>
          <?php 
          } 
          ?>

      </ul>

    </li>

    <?php
      }
    ?>

    <!-- Hard coded access for HR -->

    <?php 
      if($user->branch_id == 208){ # HR branch_id
    ?>

    <li class="treeview <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Report Viewer</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">
        
        <li class="<?= $this->uri->segment(1) == 'commission' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>commission" style="padding-left: 36px;">Commision Viewer</a></li>

        <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

        <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Total Sales Report</a></li>
        
      </ul>

    </li>

    <?php 
      }
    ?>

    <!-- Hard coded access for SUPPLY CHAIN -->

    <?php
      if($user->user_role == 'Supply Chain'){
    ?>

    <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && ($this->uri->segment(2) == 'receive' || $this->uri->segment(2) == 'create_dr_manual' || $this->uri->segment(2) == 'receive_dr')  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/receive" style="padding-left: 36px;">Inventory Delivery</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-folder-open"></i> <span>Budget Folder</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder_sc' ? 'active' : ''?>">
          <a href="<?= base_url()?>budgetfolder/budgetfolder_sc" style="padding-left: 36px;">Budget Folder</a>
        </li>

      </ul>

    </li>

    <?php }?>

    <!-- Hard coded access for MARKETING-->

    <?php
      if($user->branch_id == 209){
    ?>
    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

        <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

          <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

        </a>

        <ul class="treeview-menu">

          <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

          <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>

        </ul>

      </li>


      <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

        <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Report Viewer</span>

          <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

        </a>

        <ul class="treeview-menu">

          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>
          
          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr_otc' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr_otc" style="padding-left: 36px;">Category Sales Report - OTC</a></li>

          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_dsr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_dsr" style="padding-left: 36px;">DSR</a></li>

          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">ISR</a></li>

          <!-- <li class="<?= $this->uri->segment(1) == 'commission' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>commission" style="padding-left: 36px;">Commission Viewer</a></li> -->

          <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Daily Sales Report</a></li>

           <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'head_count' ? 'active' : ''?>"><a href="<?= base_url()?>report/head_count" style="padding-left: 36px;">Head Count Report</a></li>

          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in" style="padding-left: 36px; font-size: 13px;">Converted Walk-in - Per Branch</a></li>
          <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in_sp' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in_sp" style="padding-left: 36px;">Converted Walk-in - Per SP</a></li>
        </ul>

      </li>    

    <?php
      }
    ?>

    
    <!-- Hard coded access for SUPERADMIN -->

    <?php
      if($user->user_role == 'Super Admin'){
    ?>

    <li class="<?= $this->uri->segment(1) == 'dashboard' ? 'active' : ''?>"><a href="<?= base_url()?>dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

    <!-- <li class="<?= $this->uri->segment(1) == 'customer' ? 'active' : ''?>"><a href="<?= base_url()?>customer/entry"><i class="fa fa-group"></i> <span>Customer Entry</span></a></li> -->

    <li class="treeview <?= $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-suitcase"></i> <span>Job Order</span>

        <span class="pull-right-container">

            <i class="fa fa-angle-left pull-right"></i>

          </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= ($this->uri->segment(2) == 'entry' || $this->uri->segment(2) == '') && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder" style="padding-left: 36px;">View Entry</a></li>

        <li class="<?= $this->uri->segment(2) == 'turnaway' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>"><a href="<?= base_url()?>joborder/turnaway" style="padding-left: 36px;">Turn Away</a></li>
        
        <li class="<?= $this->uri->segment(2) == 'deposit' && $this->uri->segment(1) == 'joborder' ? 'active' : ''?>">
          <a href="<?= base_url()?>joborder/deposit" style="padding-left: 36px;">Deposit</a>
        </li>

      </ul>

    </li>

    <li class="treeview <?= $this->uri->segment(1) == 'inventory' ? 'active' : ''?>">

      <a href="#"><i class="fa fa-barcode"></i> <span>Inventory</span>

        <span class="pull-right-container">

          <i class="fa fa-angle-left pull-right"></i>

        </span>

      </a>

      <ul class="treeview-menu">

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>inventory" style="padding-left: 36px;">View Inventory</a></li>

        <li class="<?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'clone' ? 'active' : ''?>"><a href="<?= base_url()?>inventory/clone" style="padding-left: 36px;">View Inventory (Cloned)</a></li>

        <li class=" <?= $this->uri->segment(1) == 'inventory' && $this->uri->segment(2) == 'transfer'  ? 'active' : ''?>"><a href="<?= base_url()?>inventory/transfer" style="padding-left: 36px;">Request Transfer</a></li>

      </ul>

    </li>

        <li class="treeview <?= $this->uri->segment(1) == 'report' ? 'active' : ''?> <?= $this->uri->segment(1) == 'commission' ? 'active' : ''?> <?= $this->uri->segment(1) == 'totalsales' ? 'active' : ''?>">

          <a href="#"><i class="fa fa-file-pdf-o"></i> <span>Report Viewer</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr" style="padding-left: 36px;">Category Sales Report</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr_otc' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr_otc" style="padding-left: 36px;">Category Sales Report - OTC</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_csr_item' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_csr_item" style="padding-left: 36px;">Category Sales Report - Item</a></li>


            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'annual_csr' ? 'active' : ''?>"><a href="<?= base_url()?>report/annual_csr" style="padding-left: 36px;">Annual CSR</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'annual_csr_otc' ? 'active' : ''?>"><a href="<?= base_url()?>report/annual_csr_otc" style="padding-left: 36px;">Annual CSR - OTC</a></li>


            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'dsr_detailed' ? 'active' : ''?>"><a href="<?= base_url()?>report/dsr_detailed" style="padding-left: 36px;">Detailed Sales Report</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_dsr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_dsr" style="padding-left: 36px;">DSR</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'custom_isr' ? 'active' : ''?>"><a href="<?= base_url()?>report/custom_isr" style="padding-left: 36px;">ISR</a></li>

            <li class="<?= $this->uri->segment(1) == 'commission' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>commission" style="padding-left: 36px;">Commission Viewer</a></li>

            <li class="<?= $this->uri->segment(1) == 'totalsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>totalsales" style="padding-left: 36px;">Daily Sales Report</a></li>
            
            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'head_count' ? 'active' : ''?>"><a href="<?= base_url()?>report/head_count" style="padding-left: 36px;">Head Count Report</a></li>
            
            <li class="<?= $this->uri->segment(1) == 'asmsales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>asmsales" style="padding-left: 36px;">Sales Report - ASM </a></li>
            <li class="<?= $this->uri->segment(1) == 'budgetfoldersales' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>budgetfoldersales" style="padding-left: 36px;">Sales Report - Budgetfolder</a></li>
            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_brand_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_brand_sales" style="padding-left: 36px;">Sales Report - Brand </a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'per_branch_sales' ? 'active' : ''?>"><a href="<?= base_url()?>report/per_branch_sales" style="padding-left: 36px;">Sales Report - Branch </a></li>
            <li class="<?= $this->uri->segment(1) == 'z_index' && $this->uri->segment(2) == '' ? 'active' : ''?>">
              <a href="<?= base_url()?>z_index" style="padding-left: 36px;">Z-Index</a>
            </li>
            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in" style="padding-left: 36px; font-size: 13px;">Converted Walk-in - Per Branch</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_walk_in_sp' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_walk_in_sp" style="padding-left: 36px;">Converted Walk-in - Per SP</a></li>
            
            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>report/variance_analysis" style="padding-left: 36px;">Variance Analysis</a></li>
            <li class="<?= $this->uri->segment(1) == 'annualbrand' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>annualbrand" style="padding-left: 36px;">Annual Sales - Brand</a></li>
            <li class="<?= $this->uri->segment(1) == 'annualstore' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>annualstore" style="padding-left: 36px;">Annual Sales - Store</a></li>

            <li class="<?= $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'converted_regular' ? 'active' : ''?>"><a href="<?= base_url()?>report/converted_regular" style="padding-left: 36px; font-size: 13px;">Converted Regular - Per Branch</a></li>
            

          </ul>

        </li>    

        <!-- BUDGETFOLDER Module -->

        <li class="treeview <?= $this->uri->segment(1) == 'budgetfolder' ? 'active' : ''?>">

          <a href="#"><i class="fa fa-folder-open"></i> <span>Budget Folder</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'price_list' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/price_list" style="padding-left: 36px;">Price List</a>
            </li>
            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'sales_forecast' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/sales_forecast" style="padding-left: 36px;">Sales Forecast</a>
            </li>
            <li class="<?= $this->uri->segment(1) == 'salesforecast' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>salesforecast" style="padding-left: 36px;">Sales Forecast - Test</a></li>


            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'store_budgetfolder' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/store_budgetfolder" style="padding-left: 36px;">Store Budget Folder</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/budgetfolder" style="padding-left: 36px;">Budget Folder</a>
            </li>

            <?php

              if($user->branch_id == 1){

            ?>

            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'index' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/index" style="padding-left: 36px;">Budget Folder Inventory</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == 'budgetfolder_sc' ? 'active' : ''?>">
              <a href="<?= base_url()?>budgetfolder/budgetfolder_sc" style="padding-left: 36px;">SC - Budget Folder</a>
            </li>
            
            <li class="<?= $this->uri->segment(1) == 'budgetfolder' && $this->uri->segment(2) == '' ? 'active' : ''?>"><a href="<?= base_url()?>budgetfolder/categorysalesforecast" style="padding-left: 36px;">Category Sales Forecast</a>
            </li>

            <?php

              }

            ?>

          </ul>

        </li>

        <!-- SUPERADMIN Module -->

        <li class="treeview <?= $this->uri->segment(1) == 'superadmin' ? 'active' : ''?>">

          <a href="#"><i class="fa fa-lock"></i> <span>Super Admin</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <!-- <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'inventory' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/inventory/index" style="padding-left: 36px;">Inventory</a>
            </li> -->
            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'user' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/user/index" style="padding-left: 36px;">User</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'serviceprovider' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/serviceprovider/index" style="padding-left: 36px;">SP</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'gc' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/gc/index" style="padding-left: 36px;">GC</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'service' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/service/index" style="padding-left: 36px;">Service</a>
            </li>
            
            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'userid' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/userid/index" style="padding-left: 36px;">User ID</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'attendance_tracker' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/attendance_tracker/index" style="padding-left: 36px;">Attendance Tracker</a>
            </li>

            <li class="<?= $this->uri->segment(1) == 'superadmin' && $this->uri->segment(2) == 'late_encoding' ? 'active' : ''?>">
              <a href="<?= base_url()?>superadmin/late_encoding/index" style="padding-left: 36px;">Branch Allow Encode</a>
            </li>

            
            


          </ul>

        </li>

    <?php
      }
    ?>

  </ul>

  <!-- /.sidebar-menu -->

</section>

<!-- /.sidebar -->

</aside>



<!-- Content Wrapper. Contains page content -->

<div class="content-wrapper">

<!-- Content Header (Page header) -->

<section class="content-header">

  <h1>

    <?= $page_title?>

    <small>Module</small>

  </h1>

</section>
<div class="modal fade" id="transfer_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">User Info</h4>

          </div>

          <div class="modal-body">

              <div id="transfer_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully transferred to a new branch. <br><br>
              You will be logged out in <span style="font-weight: bold;" id="counter"></span> 
              </div>

              <form id="transfer_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                      
                      <input type="hidden" id="transfer_user_id" value="<?= $user->user_id; ?>">

                      <label for="current_branch">Current Branch</label>

                      <input id="current_branch" name="current_branche" class="form-control" type="text" disabled value="<?= $user->branch_name; ?>">

                      <label for="transfer_branch">Transfer to</label>

                      <select id="transfer_branch" name="branches" class="form-control select2"  style="width: 100%;">
                        <option value="" hidden selected>Select Branch</option>
                        <?php
                            foreach($branches as $branch){
                        ?>

                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                        <?php }?>

                      </select>

                      <label for="transfer_user_name">Name</label>

                      <input id="transfer_user_name" name="user_name" class="form-control" type="text" disabled value="<?= $user->user_name; ?>">

                      <label for="transfer_user_username">Username</label>

                      <input id="transfer_user_username" name="user_username" class="form-control" type="text" disabled value="<?= $user->user_username; ?>">

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="transfer_update_button" type="button" onclick="transfer_branch()" class="btn btn-success" >Update

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="maintenance_modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">System Information</h4>
          </div>
          <div class="modal-body">
          <p id="maintenance_message"></p>
          </div>
          <div class="modal-footer">
              <button id="maintenance_cancel" type="button" onclick="cancel_maintenance();" class="btn btn-secondary" >Cancel
              </button>
              <button id="transfer_update_button" type="button" onclick="confirm_maintenance();" class="btn btn-success" >Confirm
              </button>
          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>


<!-- Main content -->

<section class="content container-fluid">



  <!--------------------------

    | Your Page Content Here |

    -------------------------->
    
    <?php

      if(date('Y-m-d') > '2019-10-30' && date('Y-m-d') <= '2019-11-05' && $user->brand_id != 100007){

    ?>

     <div class="alert alert-warning alert-dismissible text-dark">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
      <p style="color: black;">You can now encode your <strong>Wall-to-wall Inventory</strong> by going to <strong>Dashboard</strong> and click <strong>PHYSICAL INVENTORY ENTRY</strong> or by clicking the <strong>Wall-to-Wall Inventory</strong> under the <strong>Inventory</strong> module. </p>

      <input type="hidden" id="allowed">
      
    </div>

    <?php

      }
      else{
        
    ?>

      <?php
      
        $allowed = 0;

        if((date('Y-m-d') > '2019-11-30' &&  (date('d') > 15 && date('d') <= 20) ) && $user->branch_batchno == 2){ 
            
            $allowed = 1;
      ?>

      <div class="alert alert-warning alert-dismissible text-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
        <p style="color: black;">You can now encode your <strong>Wall-to-wall Inventory</strong> by going to <strong>Dashboard</strong> and click <strong>PHYSICAL INVENTORY ENTRY</strong> or by clicking the <strong>Wall-to-Wall Inventory</strong> under the <strong>Inventory</strong> module. </p>
      </div>

      <?php
        
        }

      ?>


      <?php

        if((date('Y-m-d') > '2019-11-30' &&  (date('d') >= 1 && date('d') <= 10) ) && $user->branch_batchno == 1){ 
            
            $allowed = 1;
      ?>

       <div class="alert alert-warning alert-dismissible text-dark">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4 class="display-4" style="color: black;">SPECIAL NOTICE!</h4>
        <p style="color: black;">You can now encode your <strong>Wall-to-wall Inventory</strong> by going to <strong>Dashboard</strong> and click <strong>PHYSICAL INVENTORY ENTRY</strong> or by clicking the <strong>Wall-to-Wall Inventory</strong> under the <strong>Inventory</strong> module. </p>
       </div> 

      

      <?php
        
        }

      ?>
      
      <input type="hidden" id="allowed" value="<?= $allowed?>">

    <?php
        
      }

    ?>



