<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }

    .leave {

        width: 50%;
        
        margin-left: 20px;

    }

    .datetimepicker1 {

        width: 50%;

        margin-left: 20px;

    }


    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

    ul.timeline {
    list-style-type: none;
    position: relative;
    }
    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul.timeline > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }
    #new_form {
        float: right;

    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

    <h3 style="margin-left: 40px"> My Forms/Requests </h3>

    <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

            <div style="margin-left: 30px">

                <button type="button" class="btn btn-light" value="All" onclick="sort_all(value);" id="code_color">All</button>
                <button type="button" class="btn btn-primary" value="For Approval" onclick="sort_all(value);" id="code_color">For Approval</button>
                <button type="button" class="btn btn-success" value="Approved" onclick="sort_all(value);" id="code_color">Approved</button>
                <button type="button" class="btn btn-danger" value="Disapproved" onclick="sort_all(value);" id="code_color">Disapproved</button>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_new_form" id="new_form">Create New Request</button>
                <hr style="margin-bottom: 10px;">
            </div>

                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">

                            <div class="table-responsive">

                                <table id="form_list_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                    <thead>

                                        <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                            <th>Form ID</th>

                                            <th>Date and Time Created</th>

                                            <th>Name of Form/Request</th>

                                            <th>Status</th>

                                            <th>Action</th>

                                        </tr>

                                    </thead>

                                    <tbody style="font-size: 12px;">

                                    </tbody>

                                </table>

                            </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>


<div class="modal fade" id="modal_new_form" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header" style="background-color:#add8e6">

              <button id="add_user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">New Request Form</h4>

          </div>

          <div class="modal-body">

              <div id="success_add_request" class="alert alert-success alert-dismissible" style="display: none">

                <h4><i class="icon fa fa-check"></i> Success!</h4>

                Request Submitted! Waiting for approval.

              </div>

              <form id="add_new_request_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                        <label for="form_type">Request Forms</label>

                        <select id="form_type" name="add_new_form" class="form-control select2" style="width: 100%; margin-bottom: 10px">
                            
                            <option selected disabled hidden>Select Form</option>

                            <option value="leave_request">Leave Request Form</option>

                            <option value="schedule_request">Schedule Request Form</option>

                        </select>


                        <div style="display: none" id="leave_form_selected">

                            <div class="form-group">

                                <hr />

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="user_name" name="add_new_form" class="form-control" style="margin-left: 15px; width: 50%; margin-bottom: 10px">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="user_name" style="margin-left: 20px; margin-bottom: 10px"><?= $user->user_name ?></h3>

                                <?php } ?>

                                <label for="user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="user_name" style="margin-left: 20px; margin-bottom: 10px"><?= $user->branch_name ?></h4>

                                <label for="position" class="col-md-12 control-label">Position:</label>

                                <h4 id="position" style="margin-left: 20px; margin-bottom: 10px"><?= $user->user_position ?></h4>

                                <div class="form-group">

                                    <label for="datetimepicker1" class="col-md-12 control-label">Leave Start Date:</label>

                                    <div class='input-group date' id='datetimepicker1' style='width: 50%; margin-left: 15px'>

                                        <input type='text' id="from_input" class="form-control"/>

                                        <span class="input-group-addon">

                                            <span class="glyphicon glyphicon-calendar"></span>

                                        </span>

                                    </div>

                                    <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                                </div>


                                <div class="form-group">

                                    <label for="datetimepicker2" class="col-md-12 control-label">Leave End Date:</label>

                                    <div class='input-group date' id='datetimepicker2' style='width: 50%; margin-left: 15px'>

                                        <input type='text' id="to_input" class="form-control"/>

                                        <span class="input-group-addon">

                                            <span class="glyphicon glyphicon-calendar"></span>

                                        </span>

                                    </div>

                                    <label id="to_error" style="margin-left: 15px" hidden>Please select End Date</label>

                                </div>
                                
                                <div class="form-group">

                                    <label for="leave_type" style="margin-left: 15px;">Reason for Leave</label>

                                    <select id="leave_type" name="add_new_form" class="form-control" style="width: 75%; margin-left: 15px;">
                                        
                                        <option selected disabled hidden>Leave type</option>

                                        <option value="Vacation Leave">Vacation Leave</option>

                                        <option value="Sick Leave">Sick Leave</option>

                                        <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                                        <option value="Maternity Leave">Maternity Leave</option>

                                        <option value="Paternity Leave">Paternity Leave</option>

                                        <option value="Others">Others, Please Specify on the Remarks below </option>

                                        <input style="display: none" id="other"></input>

                                    </select>

                                    <label id="leave_type_error" style="margin-left: 15px" hidden>Please fill out the required fields</label>
                                
                                </div>

                                <div class="form-group">

                                    <label for="comment" style="margin-left: 15px">Indicate Specific details of the Leave:</label>

                                    <textarea id="comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>
                        
                        </div>

                        <div style="display: none" id="schedule_form_selected">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden disabled selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime/Offsetting</option>

                                <option value="bio">Biometric No In/No Out</option>

                                <option value="cow">Change of Work Schedule</option>

                                <option value="branch_transfer">Request for Branch Transfer</option>

                            </select>

                        </div>
                        
                        <div style="display: none" id="bio">

                            <hr />

                            <div class="form-group">

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="user_name" name="add_new_form" class="form-control select2" style="margin-left: 50px; width: 50%;">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="bio_user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="bio_user_name" style="margin-left: 20px"><?= $user->user_name ?></h4>

                                <?php } ?>

                                <label for="bio_user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="bio_user_name" style="margin-left: 20px"><?= $user->branch_name ?></h4>

                                <label for="bio_position" class="col-md-12 control-label">Position:</label>

                                <h4 id="bio_position" style="margin-left: 20px"><?= $user->user_position ?></h4>

                                <label for="bio_time_in" class="col-md-12 control-label">Time in:</label>

                                <input type="time" id="bio_time_in" class="form-control" style="width: 75%; margin-left: 15px"></h3>

                                <label for="bio_time_out" class="col-md-12 control-label">Time out:</label>

                                <input type="time" id="bio_time_out" class="form-control" style="width: 75%; margin-left: 15px"></h3>

                                <div class="form-group">

                                    <label for="bio_comment" style="margin-left: 15px">Reason for failure in Log in/Log out :</label>

                                    <textarea id="bio_comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="bio_comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>

                        </div>

                        <div style="display: none" id="cow">

                            <hr />

                            <div class="form-group">

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="cow_user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="cow_user_name" name="add_new_form" class="form-control select2" style="margin-left: 15px; width: 50%; margin-bottom: 10px">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="bio_user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="bio_user_name" style="margin-left: 20px"><?= $user->user_name ?></h4>

                                <?php } ?>

                                <label for="cow_user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="cow_user_name" style="margin-left: 20px"><?= $user->branch_name ?></h4>

                                <label for="cow_position" class="col-md-12 control-label">Position:</label>

                                <h4 id="cow_position" style="margin-left: 20px"><?= $user->user_position ?></h4>

                                <hr />

                                <h4 style="margin-left: 15px">Official Work Schedule: </h4>

                                <label for="cow_time_in" class="col-md-6 control-label">Time in:</label>

                                <input type="time" id="cow_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></input>

                                <label for="cow_time_out" class="col-md-6 control-label">Time out:</label>

                                <input type="time" id="cow_time_out" class="form-control" style="width: 40%; margin-left: 15px"></input>

                                <h4 style="margin-left: 15px">Moved to: </h4>

                                <label for="cow_new_time_in" class="col-md-6 control-label">Time in:</label>

                                <input type="time" id="cow_new_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></input>

                                <label for="cow_new_time_out" class="col-md-6 control-label">Time out:</label>

                                <input type="time" id="cow_new_time_out" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></input>

                                <div class="form-group">

                                    <label for="cow_comment" style="margin-left: 15px">Justification:</label>

                                    <textarea id="cow_comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="cow_comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>

                        </div>

                        <div style="display: none" id="branch_transfer">

                            <hr />

                            <div class="form-group">

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="bt_user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="bt_user_name" name="add_new_form" class="form-control select2" style="margin-left: 15px; width: 50%; margin-bottom: 10px">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="bio_user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="bio_user_name" style="margin-left: 20px"><?= $user->user_name ?></h4>

                                <?php } ?>

                                <label for="bt_user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="bt_user_name" style="margin-left: 20px"><?= $user->branch_name ?></h4>

                                <label for="bt_position" class="col-md-12 control-label">Position:</label>

                                <h4 id="bt_position" style="margin-left: 20px"><?= $user->user_position ?></h4>

                                <hr />

                                <label for="bt_current_branch" class="col-md-12 control-label">Current Branch:</label>

                                <h4 id="bt_current_branch" style="margin-left: 15px; margin-bottom: 10px"><?php echo $user->branch_name?></h4>


                                <label for="transfer_branches" style="margin-left: 15px; margin-bottom: 10px">Branch to Transfer: </label>

                                <select id="transfer_branches" class="form-control select2" style="width: 75%; margin-left: 20px; margin-bottom: 10px">

                                    <option value="">Select Branch</option>

                                    <?php foreach($branches as $branch) { ?>

                                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                                    <?php }?>

                                </select>

                                <div class="form-group">

                                    <label for="bt_comment" style="margin-left: 15px; margin-top: 10px">Reason for Transfer:</label>

                                    <textarea id="bt_comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="bt_comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>

                        </div>

                        <div style="display: none" id="ob">

                            <hr />

                            <div class="form-group">

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="ob_user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="ob_user_name" name="add_new_form" class="form-control select2" style="margin-left: 15px; width: 50%; margin-bottom: 10px">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="ob_user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="ob_user_name" style="margin-left: 20px"><?= $user->user_name ?></h4>

                                <?php } ?>

                                <label for="ob_user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="ob_user_name" style="margin-left: 20px"><?= $user->branch_name ?></h4>

                                <label for="ob_position" class="col-md-12 control-label">Position:</label>

                                <h4 id="ob_position" style="margin-left: 20px"><?= $user->user_position ?></h4>

                                <hr />

                                <label for="ob_covered" class="col-md-6 control-label">Covered Time(Hrs):</label>

                                <input type="number" id="ob_covered" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></input>

                                <label for="ob_time_in" class="col-md-6 control-label">From:</label>

                                <input type="time" id="ob_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></input>

                                <label for="ob_time_out" class="col-md-6 control-label">To:</label>

                                <input type="time" id="ob_time_out" class="form-control" style="width: 40%; margin-left: 15px"></input>

                                <div class="form-group">

                                    <label for="ob_comment" style="margin-left: 15px">Purpose of Business:</label>

                                    <textarea id="ob_comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="ob_comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>
                            
                        </div>

                        <div style="display: none" id="ot">

                            <hr />

                            <div class="form-group">

                                <?php if ($this->user->user_access_level == '1' || $this->user->user_access_level == '2') { ?>

                                    <label for="ot_user_name" class="col-md-12 control-label">Name:</label>

                                    <select id="ot_user_name" name="add_new_form" class="form-control select2" style="margin-left: 15px; width: 50%; margin-bottom: 10px">

                                        <option value="" selected disabled>Please Choose a Member</option>

                                        <?php foreach($branch_users as $bu) { ?>

                                            <option value="<?= $bu->user_name?>"><?= $bu->user_name?></option>

                                        <?php }?>

                                    </select>

                                <?php } else { ?>

                                    <label for="ot_user_name" class="col-md-12 control-label">Name:</label>

                                    <h4 id="ot_user_name" style="margin-left: 20px"><?= $user->user_name ?></h4>

                                <?php } ?>

                                <label for="ot_user_name" class="col-md-12 control-label">Branch/Department:</label>

                                <h4 id="ot_user_name" style="margin-left: 20px"><?= $user->branch_name ?></h4>

                                <label for="ot_position" class="col-md-12 control-label">Position:</label>

                                <h4 id="ot_position" style="margin-left: 20px"><?= $user->user_position ?></h4>

                                <hr />

                                <label for="ot_covered" class="col-md-6 control-label">Covered Time(Hrs):</label>

                                <input type="number" id="ot_covered" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></h3>

                                <label for="ot_time_in" class="col-md-6 control-label">From:</label>

                                <input type="time" id="ot_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"></h3>

                                <label for="ot_time_out" class="col-md-6 control-label">To:</label>

                                <input type="time" id="ot_time_out" class="form-control" style="width: 40%; margin-left: 15px"></h3>

                                <div class="form-group">

                                    <label for="ot_comment" style="margin-left: 15px">Purpose and Actual Job done:</label>

                                    <textarea id="ot_comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="add_request_button" type="button" onclick="new_request_form()" class="btn btn-info">Create Request

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->

<div class="modal fade" id="request_information" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Request Form Information</h4>

          </div>

          <div class="modal-body">

              <div id="user_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="user_function" style="font-size: 12px; margin-left: 3px;"></span> your request.

              </div>

              <form id="user_form" method="POST">

                <div class="row">

                    <div id="info" class="col-md-12">

                      <input id="request_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                      <input id="form_id" name="form_id" class="form-control" type="hidden">

                      <label for="request_name">Name</label>

                      <input id="request_name" name="user_name" class="form-control" type="text" disabled>

                      <label for="branch">Branch/Department</label>

                      <input id="branch" name="user_username" class="form-control" type="text" disabled>

                      <label for="position_edit">Position</label>

                      <input id="position_edit" name="position" class="form-control" type="text" disabled>

                      <div id="dates" class="form-group" hidden>

                        <hr />

                        <label for="edit_from" class="col-md-12 control-label">Leave Start Date:</label>

                        <div class='input-group date' id='edit_from' style='width: 40%;'>

                            <input type='text' id="from_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                       <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                        <label for="edit_to" class="col-md-12 control-label">Leave End Date:</label>

                        <div class='input-group date' id='edit_to' style='width: 40%;'>

                            <input type='text' id="to_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                      </div>

                      <div id="reason_for_leave" class="form-group" hidden>

                        <label for="leave_type_edit">Reason for Leave</label>

                        <select id="leave_type_edit" name="add_new_form" class="form-control" disabled>
                            
                            <option selected disabled hidden>Leave type</option>

                            <option value="Vacation Leave">Vacation Leave</option>

                            <option value="Sick Leave">Sick Leave</option>

                            <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                            <option value="Maternity Leave">Maternity Leave</option>

                            <option value="Paternity Leave">Paternity Leave</option>

                            <option value="Others">Others, Please Specify on the Remarks below </option>

                        </select>

                        <label id="leave_type_edit_error" hidden>Please fill out the required fields</label>

                      </div>

                      <div id="bio_info" class="form-group" hidden>

                        <hr />

                        <label for="info_time_in" class="col-md-6 control-label">Time in:</label>

                        <input type="time" id="info_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                        <label for="info_time_out" class="col-md-6 control-label">Time out:</label>

                        <input type="time" id="info_time_out" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                      </div>

                      <div id="cow_info" class="form-group" hidden>

                        <hr />

                        <label for="cow_new_info_in" class="col-md-6 control-label">Moved Time in:</label>

                        <input type="time" id="cow_new_info_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                        <label for="cow_new_info_out" class="col-md-6 control-label">Moved Time out:</label>

                        <input type="time" id="cow_new_info_out" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                      </div>

                      <div id="bt_info" class="form-group" hidden>

                        <hr />

                        <label for="info_transfer_branches" style="margin-left: 15px; margin-bottom: 10px">Branch to Transfer: </label>

                        <select id="info_transfer_branches" class="form-control select2" style="width: 75%; margin-left: 20px; margin-bottom: 10px" disabled>

                            <option value="">Select Branch</option>

                            <?php foreach($branches as $branch) { ?>

                                <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                            <?php }?>

                        </select>

                      </div>

                      <div id="ob_info" class="form-group" hidden>

                        <label for="ob_info_covered" class="col-md-6 control-label">Covered Time:</label>

                        <input id="ob_info_covered" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px" disabled></input>

                      </div>

                      <label for="remarks">Remarks</label>

                      <input id="remarks" name="remarks" class="form-control" type="text" disabled>

                    </div>

                </div>

                <hr />
                <div class="container mt-3 mb-3">
                    <div class="row">
                        <div class="col-md-5 offset-md-3">
                            <h4>Request History</h4>
                            <ul id="history">
                            </ul>
                        </div>
                    </div>
                </div>

              </form>

          </div>

          <div class="modal-footer">

              <?php if ($user)
              
              ?>

              <button id="user_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit

              </button>

              <button id="user_update_button" type="button" onclick="update_request()" class="btn btn-success" disabled="disabled">Update

              </button>

              <button id="request_delete_button" type="button" class="btn btn-danger">Delete

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/digitalforms/index.js?v=1.0.9"></script>





