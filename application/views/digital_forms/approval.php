<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }

    .leave {

        width: 50%;
        
        margin-left: 20px;

    }

    .datetimepicker1 {

        width: 50%;

        margin-left: 20px;

    }


    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

    <h3 style="margin-left: 40px"> Forms/Request for Approval </h3>

    <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

            <div style="margin-left: 30px">

                <button type="button" class="btn btn-light" value="All" onclick="sort_all(value);" id="code_color">All</button>
                <button type="button" class="btn btn-primary" value="For Approval" onclick="sort_all(value);" id="code_color">Approval</button>
                <button type="button" class="btn btn-success" value="Approved" onclick="sort_all(value);" id="code_color">Approved</button>
                <button type="button" class="btn btn-danger" value="Disapproved" onclick="sort_all(value);" id="code_color">Disapproved</button>

            </div>

                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">

                            <div class="table-responsive">

                                <table id="form_list_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                    <thead>

                                        <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                            <th>Form ID</th>

                                            <th>Date and Time Created</th>

                                            <th> Requested By </th>

                                            <th>Name of Form/Request</th>

                                            <th>Request Status</th>

                                            <th>Action</th>

                                        </tr>

                                    </thead>

                                    <tbody style="font-size: 12px;">

                                    </tbody>

                                </table>

                            </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>


<div class="modal fade" id="modal_new_form" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header" style="background-color:#add8e6">

              <button id="add_user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">New Request Form</h4>

          </div>

          <div class="modal-body">

              <div id="success_add_request" class="alert alert-success alert-dismissible" style="display: none">

                <h4><i class="icon fa fa-check"></i> Success!</h4>

                Request Submitted! Waiting for approval.

              </div>

              <form id="add_new_request_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                        <label for="form_type">Form Type</label>

                        <select id="form_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                            
                            <option selected disabled hidden>Select Form</option>

                            <option value="leave_request">Leave Request Form</option>

                            <option value="schedule_request">Schedule Request Form</option>

                        </select>

                        <!-- <h3 style="display: none" id="error_message"> -->

                        <div style="display: none" id="leave_form_selected">

                            <div class="form-group" style="margin-left: -15px;">

                                <label for="user_name" class="col-md-12 control-label">Name:</label>

                                <h4 id="user_name" style="margin-left: 20px"><?= $user->user_name ?></h3>

                                <label for="user_name" class="col-md-12 control-label">Branch:</label>

                                <h4 id="user_name" style="margin-left: 20px"><?= $user->branch_name ?></h3>

                                <label for="position" class="col-md-12 control-label">Position:</label>

                                <h4 id="position" style="margin-left: 20px"><?= $user->user_position ?></h3>

                                <label for="department" class="col-md-6 control-label">Department:</label>

                                <h4 id="depatment" style="margin-left: 20px"><?= $user->user_department ?></h3>

                                <div class="form-group">

                                    <label for="datetimepicker1" class="col-md-12 control-label">From:</label>

                                    <div class='input-group date' id='datetimepicker1' style='width: 50%; margin-left: 15px'>

                                        <input type='text' id="from_input" class="form-control"/>

                                        <span class="input-group-addon">

                                            <span class="glyphicon glyphicon-calendar"></span>

                                        </span>

                                    </div>

                                    <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                                </div>

                                <div class="form-group">

                                    <label for="datetimepicker2" class="col-md-12 control-label">To:</label>

                                    <div class='input-group date' id='datetimepicker2' style='width: 50%; margin-left: 15px'>

                                        <input type='text' id="to_input" class="form-control"/>

                                        <span class="input-group-addon">

                                            <span class="glyphicon glyphicon-calendar"></span>

                                        </span>

                                    </div>

                                    <label id="to_error" style="margin-left: 15px" hidden>Please select End Date</label>

                                </div>
                                
                                <div class="form-group">

                                    <label for="leave_type" style="margin-left: 15px;">Reason for Leave</label>

                                    <select id="leave_type" name="add_new_form" class="form-control" style="width: 75%; margin-left: 15px;">
                                        
                                        <option selected disabled hidden>Leave type</option>

                                        <option value="Vacation Leave">Vacation Leave</option>

                                        <option value="Sick Leave">Sick Leave</option>

                                        <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                                        <option value="Maternity Leave">Maternity Leave</option>

                                        <option value="Paternity Leave">Paternity Leave</option>

                                        <option value="Others">Others, Please Specify </option>

                                        <input style="display: none" id="other"></input>

                                    </select>

                                    <label id="leave_type_error" style="margin-left: 15px" hidden>Please fill out the required fields</label>

                                
                                </div>

                                <div class="form-group">

                                    <label for="comment" style="margin-left: 15px">Indicate Specific details of the Leave:</label>

                                    <textarea id="comment" class="form-control" rows="4" id="comment" style="width: 75%; margin-left: 15px;"></textarea>

                                    <label id="comment_error" style="margin-left: 15px" hidden>Remarks are required</label>

                                </div>

                            </div>
                        
                        </div>

                        <div style="display: none" id="schedule_form_selected">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime</option>

                                <option value="os">Offsetting</option>

                                <option value="cow">Change of work schedule</option>

                            </select>

                        </div>
                        
                        <!-- <div style="display: none" id="ob">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime</option>

                                <option value="os">Offsetting</option>

                                <option value="cow">Change of work schedule</option>

                            </select>
                        </div>

                        <div style="display: none" id="ot">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime</option>

                                <option value="os">Offsetting</option>

                                <option value="cow">Change of work schedule</option>

                            </select>

                        </div>

                        <div style="display: none" id="os">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime</option>

                                <option value="os">Offsetting</option>

                                <option value="cow">Change of work schedule</option>

                            </select>
                            
                        </div>

                        <div style="display: none" id="cow">

                            <label for="form_type">Schedule Type</label>

                            <select id="schedule_type" name="add_new_form" class="form-control select2" style="width: 100%;">
                                
                                <option value="" hidden selected>Select Form</option>

                                <option value="ob">Official Business</option>

                                <option value="ot">Overtime</option>

                                <option value="os">Offsetting</option>

                                <option value="cow">Change of work schedule</option>

                            </select>

                        </div> -->

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="add_request_button" type="button" onclick="new_request_form()" class="btn btn-info">Add Form

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<div class="modal fade" id="request_approve" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Request Form Information</h4>

          </div>

          <div class="modal-body">

              <div id="user_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="user_function" style="font-size: 12px; margin-left: 3px;"></span> the request.

              </div>

              <form id="user_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                      <label for="request_id" hidden>Id</label>

                      <input id="request_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                      <label for="form_id" hidden>Form Id</label>

                      <input id="form_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                      <label for="request_name">Name</label>

                      <input id="request_name" name="user_name" class="form-control" type="text" disabled>

                      <label for="branch">Branch</label>

                      <input id="branch" name="user_username" class="form-control" type="text" disabled>

                      <label for="position_edit">Position</label>

                      <input id="position_edit" name="position" class="form-control" type="text" disabled>

                      <label for="department_edit">Department</label>

                      <input id="department_edit" name="department" class="form-control" type="text" disabled>

                      <div class="form-group">

                        <label for="edit_from" class="col-md-12 control-label">Leave Start Date:</label>

                        <div class='input-group date' id='edit_from' style='width: 40%;'>

                            <input type='text' id="from_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                        <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                        <label for="edit_to" class="col-md-12 control-label">Leave End Date:</label>

                        <div class='input-group date' id='edit_to' style='width: 40%;'>

                            <input type='text' id="to_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                      </div>

                      <div class="form-group">

                      <label for="leave_type_edit">Reason for Leave</label>

                      <select id="leave_type_edit" name="add_new_form" class="form-control" disabled>
                        
                        <option selected disabled hidden>Leave type</option>

                        <option value="Vacation Leave">Vacation Leave</option>

                        <option value="Sick Leave">Sick Leave</option>

                        <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                        <option value="Maternity Leave">Maternity Leave</option>

                        <option value="Paternity Leave">Paternity Leave</option>

                        <option value="Others">Others, Please Specify </option>

                        <input style="display: none" id="other"></input>

                      </select>

                      <label id="leave_type_edit_error" hidden>Please fill out the required fields</label>

                      </div>

                      <label for="remarks">Remarks</label>

                      <input id="remarks" name="remarks" class="form-control" type="textarea" disabled>

                      <label id="approval_reason_label" style="display: none">Remarks on Approval</label>

                      <input id="approval_reason" name="approval_reason" class="form-control" type="textarea" style="display: none">

                      <label id="disapproval_reason_label" style="display: none">Remarks on Disapproval</label>

                      <input id="disapproval_reason" name="disapproval_reason" class="form-control" type="textarea" style="display: none">

                    </div>

                </div>

                <hr />

                <div class="container mt-3 mb-3">
                    <div class="row">
                        <div class="col-md-5 offset-md-3">
                            <h4>Request History</h4>
                            <ul id="history">
                            </ul>
                        </div>
                    </div>
                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="request_approve_button" type="button" class="btn btn-success">Approve

              </button>

              <button id="request_disapprove_button" type="button" class="btn btn-danger">Disapprove

              </button>

              <button id="confirm_approve_button" type="button" class="btn btn-success" style="display: none">Confirm Approve

              </button>

              <button id="confirm_disapprove_button" type="button" class="btn btn-danger" style="display: none">Confirm Disapprove

              </button>

              <button id="request_cancel" type="button" class="btn" style="display: none">Cancel

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/digitalforms/approval.js"></script>





