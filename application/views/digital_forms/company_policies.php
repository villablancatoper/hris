<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }

    .leave {

        width: 50%;
        
        margin-left: 20px;

    }

    .datetimepicker1 {

        width: 50%;

        margin-left: 20px;

    }


    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

    .form {
	 width: 500px;
	 margin: 5% auto;
    }
    .form__container {
        position: relative;
        width: 100%;
        height: 200px;
        border: 2px dashed silver;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 18px;
        color: silver;
        margin-bottom: 5px;
    }
    .form__container.active {
        background-color: rgba(192, 192, 192, 0.2);
    }
    .form__file {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        cursor: pointer;
        opacity: 0;
    }
    .form__files-container {
        display: block;
        width: 100%;
        font-size: 0;
        margin-top: 20px;
    }
    .form__image-container {
        display: inline-block;
        width: 49%;
        height: 200px;
        margin-bottom: 10px;
        position: relative;
    }
    .form__image-container:not(:nth-child(2n)) {
        margin-right: 2%;
    }
    .form__image-container:after {
        content: "✕";
        position: absolute;
        line-height: 200px;
        font-size: 30px;
        margin: auto;
        top: 0;
        right: 0;
        left: 0;
        text-align: center;
        font-weight: bold;
        color: #fff;
        background-color: rgba(0, 0, 0, 0.4);
        opacity: 0;
        transition: opacity 0.2s ease-in-out;
    }
    .form__image-container:hover:after {
        opacity: 1;
        cursor: pointer;
    }
    .form__image {
        object-fit: contain;
        width: 100%;
        height: 100%;
    }
 

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

    <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

            <div class="box-body text-right">

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_new_policy" id="new_form">Add New Policy</button>

                <hr style="margin-bottom: 10px;">

            </div>

                <div class="col-md-12">

                    <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">

                        <div class="table-responsive">

                            <table id="form_list_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                <thead>

                                    <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                        <th>Branch/Department</th>

                                        <th>Title of policy</th>

                                        <th>Date uploaded</th>

                                        <th>Action</th>

                                    </tr>

                                </thead>

                                <tbody style="font-size: 12px;">

                                </tbody>

                            </table>

                        </div>

                        </div>
                        
                    </div>

                </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>


<div class="modal fade" id="modal_new_policy" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="add_user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">New Request Form</h4>

          </div>

          <div class="modal-body">

          <form class="form"><label class="form__container" id="upload-container">Choose or Drag & Drop Files<input class="form__file" id="upload-files" type="file" accept="image/*" multiple="multiple"/></label>
                <div class="form__files-container" id="files-list-container"></div>
            </form>

          </div>

          <div class="modal-footer">

              <button id="add_request_button" type="button" onclick="new_request_form()" class="btn btn-info">Add Form

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<div class="modal fade" id="request_approve" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Request Form Information</h4>

          </div>

          <div class="modal-body">

              <div id="user_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="user_function" style="font-size: 12px; margin-left: 3px;"></span> the request.

              </div>

              <form id="user_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                      <label for="request_id" hidden>Id</label>

                      <input id="request_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                      <label for="form_id" hidden>Form Id</label>

                      <input id="form_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                      <label for="request_name">Name</label>

                      <input id="request_name" name="user_name" class="form-control" type="text" disabled>

                      <label for="branch">Branch</label>

                      <input id="branch" name="user_username" class="form-control" type="text" disabled>

                      <label for="position_edit">Position</label>

                      <input id="position_edit" name="position" class="form-control" type="text" disabled>

                      <label for="department_edit">Department</label>

                      <input id="department_edit" name="department" class="form-control" type="text" disabled>

                      <div class="form-group">

                        <label for="edit_from" class="col-md-12 control-label">Leave Start Date:</label>

                        <div class='input-group date' id='edit_from' style='width: 40%;'>

                            <input type='text' id="from_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                        <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                        <label for="edit_to" class="col-md-12 control-label">Leave End Date:</label>

                        <div class='input-group date' id='edit_to' style='width: 40%;'>

                            <input type='text' id="to_input_edit" class="form-control" disabled/>

                            <span class="input-group-addon">

                                <span class="glyphicon glyphicon-calendar"></span>

                            </span>

                        </div>

                      </div>

                      <div class="form-group">

                      <label for="leave_type_edit">Reason for Leave</label>

                      <select id="leave_type_edit" name="add_new_form" class="form-control" disabled>
                        
                        <option selected disabled hidden>Leave type</option>

                        <option value="Vacation Leave">Vacation Leave</option>

                        <option value="Sick Leave">Sick Leave</option>

                        <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                        <option value="Maternity Leave">Maternity Leave</option>

                        <option value="Paternity Leave">Paternity Leave</option>

                        <option value="Others">Others, Please Specify </option>

                        <input style="display: none" id="other"></input>

                      </select>

                      <label id="leave_type_edit_error" hidden>Please fill out the required fields</label>

                      </div>

                      <label for="remarks">Remarks</label>

                      <input id="remarks" name="remarks" class="form-control" type="textarea" disabled>

                      <label id="approval_reason_label" style="display: none">Remarks on Approval</label>

                      <input id="approval_reason" name="approval_reason" class="form-control" type="textarea" style="display: none">

                      <label id="disapproval_reason_label" style="display: none">Remarks on Disapproval</label>

                      <input id="disapproval_reason" name="disapproval_reason" class="form-control" type="textarea" style="display: none">

                    </div>

                </div>

                <hr />

                <div class="container mt-3 mb-3">
                    <div class="row">
                        <div class="col-md-5 offset-md-3">
                            <h4>Request History</h4>
                            <ul id="history">
                            </ul>
                        </div>
                    </div>
                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="request_approve_button" type="button" class="btn btn-success">Approve

              </button>

              <button id="request_disapprove_button" type="button" class="btn btn-danger">Disapprove

              </button>

              <button id="confirm_approve_button" type="button" class="btn btn-success" style="display: none">Confirm Approve

              </button>

              <button id="confirm_disapprove_button" type="button" class="btn btn-danger" style="display: none">Confirm Disapprove

              </button>

              <button id="request_cancel" type="button" class="btn" style="display: none">Cancel

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/digitalforms/approval.js"></script>





