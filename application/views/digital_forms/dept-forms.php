<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }

    .leave {

        width: 50%;
        
        margin-left: 20px;

    }

    .datetimepicker1 {

        width: 50%;

        margin-left: 20px;

    }


    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

    ul.timeline {
    list-style-type: none;
    position: relative;
    }
    ul.timeline:before {
        content: ' ';
        background: #d4d9df;
        display: inline-block;
        position: absolute;
        left: 29px;
        width: 2px;
        height: 100%;
        z-index: 400;
    }
    ul.timeline > li {
        margin: 20px 0;
        padding-left: 20px;
    }
    ul.timeline > li:before {
        content: ' ';
        background: white;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #22c0e8;
        left: 20px;
        width: 20px;
        height: 20px;
        z-index: 400;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

    <h3 style="margin-left: 40px"> Departmental Forms </h3>

    <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

            <div style="margin-left: 30px">

                <button type="button" class="btn btn-light" value="All" onclick="sort_all(value);" id="code_color">All</button>
                <button type="button" class="btn btn-success" value="Approved" onclick="sort_all(value);" id="code_color">Approved</button>
                <button type="button" class="btn btn-danger" value="Disapproved" onclick="sort_all(value);" id="code_color">Disapproved</button>

                <hr style="margin-bottom: 10px;">

                <select id="selection" class="form-control select2" style="width: 200px;">

                    <option selected disabled hidden>Request type</option>

                    <option value="leave">Leave Request</option>

                    <option value="Official Business">Official Business</option>

                    <option value="Overtime/Offsetting">Overtime/Offsetting</option>

                    <option value="Biometric No In/No Out">Biometric No In/No Out</option>

                    <option value="Change of Work Schedule">Change of Work Schedule</option>

                    <option value="Branch Transfer">Request for Branch Transfer</option>

                </select>

                    <div class="col-md-12">

                        <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">

                                <div id="leave_table" class="table-responsive" hidden>

                                    <table id="form_list_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>From Date</th>

                                                <th>To Date</th>

                                                <th>Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                                <div id="ob_table" class="table-responsive" hidden>

                                    <table id="form_list_table_1" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>Time In</th>

                                                <th>Time Out</th>

                                                <th>Covered Time</th>

                                                <th>Request Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                                <div id="ot_table" class="table-responsive" hidden>

                                    <table id="form_list_table_2" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>Time In</th>

                                                <th>Time Out</th>

                                                <th>Covered Time</th>

                                                <th>Request Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                                <div id="bio_table" class="table-responsive" hidden>

                                    <table id="form_list_table_3" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>Time In</th>

                                                <th>Time Out</th>

                                                <th>Request Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                                <div id="cow_table" class="table-responsive" hidden>

                                    <table id="form_list_table_4" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>Time In</th>

                                                <th>Time Out</th>

                                                <th>Moved Time In</th>

                                                <th>Moved Time Out</th>

                                                <th>Request Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                                <div id="bt_table" class="table-responsive" hidden>

                                    <table id="form_list_table_5" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                                <th>Form ID</th>

                                                <th>Date and Time Created</th>

                                                <th>Requestor Name </th>

                                                <th>Branch/Department</th>

                                                <th>Request Type</th>

                                                <th>Transfer Branch</th>

                                                <th>Request Status</th>

                                                <th>Remarks</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody style="font-size: 12px;">

                                        </tbody>

                                    </table>

                                </div>

                            </div>
                            
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>

<!-- /.box-body -->

<div class="modal fade" id="request_information" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Request Form Information</h4>

          </div>

          <div class="modal-body">

              <form id="user_form" method="POST">

                <div class="row">

                <div id="info" class="col-md-12">

                <input id="request_id" name="request_id" value="<?= $user->user_id ?>" class="form-control" type="hidden">

                <input id="form_id" name="form_id" class="form-control" type="hidden">

                <label for="request_name">Name</label>

                <input id="request_name" name="user_name" class="form-control" type="text" disabled>

                <label for="branch">Branch/Department</label>

                <input id="branch" name="user_username" class="form-control" type="text" disabled>

                <label for="position_edit">Position</label>

                <input id="position_edit" name="position" class="form-control" type="text" disabled>

                <div id="dates" class="form-group" hidden>

                <hr />

                <label for="edit_from" class="col-md-12 control-label">Leave Start Date:</label>

                <div class='input-group date' id='edit_from' style='width: 40%;'>

                    <input type='text' id="from_input_edit" class="form-control" disabled/>

                    <span class="input-group-addon">

                        <span class="glyphicon glyphicon-calendar"></span>

                    </span>

                </div>

                <label id="from_error" style="margin-left: 15px" hidden>Please select Start Date</label>

                <label for="edit_to" class="col-md-12 control-label">Leave End Date:</label>

                <div class='input-group date' id='edit_to' style='width: 40%;'>

                    <input type='text' id="to_input_edit" class="form-control" disabled/>

                    <span class="input-group-addon">

                        <span class="glyphicon glyphicon-calendar"></span>

                    </span>

                </div>

                </div>

                <div id="reason_for_leave" class="form-group" hidden>

                <label for="leave_type_edit">Reason for Leave</label>

                <select id="leave_type_edit" name="add_new_form" class="form-control" disabled>
                    
                    <option selected disabled hidden>Leave type</option>

                    <option value="Vacation Leave">Vacation Leave</option>

                    <option value="Sick Leave">Sick Leave</option>

                    <option value="Personal/Emergency Leave">Personal/Emergency Leave</option>

                    <option value="Maternity Leave">Maternity Leave</option>

                    <option value="Paternity Leave">Paternity Leave</option>

                    <option value="Others">Others, Please Specify on the Remarks below </option>

                </select>

                <label id="leave_type_edit_error" hidden>Please fill out the required fields</label>

                </div>

                <div id="bio_info" class="form-group" hidden>

                <hr />

                <label for="info_time_in" class="col-md-6 control-label">Time in:</label>

                <input type="time" id="info_time_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                <label for="info_time_out" class="col-md-6 control-label">Time out:</label>

                <input type="time" id="info_time_out" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                </div>

                <div id="cow_info" class="form-group" hidden>

                <hr />

                <label for="cow_new_info_in" class="col-md-6 control-label">Moved Time in:</label>

                <input type="time" id="cow_new_info_in" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                <label for="cow_new_info_out" class="col-md-6 control-label">Moved Time out:</label>

                <input type="time" id="cow_new_info_out" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px"disabled></input>

                </div>

                <div id="bt_info" class="form-group" hidden>

                <hr />

                <label for="info_transfer_branches" style="margin-left: 15px; margin-bottom: 10px">Branch to Transfer: </label>

                <select id="info_transfer_branches" class="form-control select2" style="width: 75%; margin-left: 20px; margin-bottom: 10px" disabled>

                    <option value="">Select Branch</option>

                    <?php foreach($branches as $branch) { ?>

                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                    <?php }?>

                </select>

                </div>

                <div id="ob_info" class="form-group" hidden>

                <label for="ob_info_covered" class="col-md-6 control-label">Covered Time:</label>

                <input id="ob_info_covered" class="form-control" style="width: 40%; margin-left: 15px; margin-bottom: 10px" disabled></input>

                </div>

                <label for="remarks">Remarks</label>

                <input id="remarks" name="remarks" class="form-control" type="text" disabled>

                </div>

                </div>

                <hr />
                <div class="container mt-3 mb-3">
                    <div class="row">
                        <div class="col-md-5 offset-md-3">
                            <h4>Request History</h4>
                            <ul id="history">
                            </ul>
                        </div>
                    </div>
                </div>

              </form>

          </div>

          <div class="modal-footer">

              <!-- <button id="user_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit

              </button>

              <button id="user_update_button" type="button" onclick="update_request()" class="btn btn-success" disabled="disabled">Update

              </button>

              <button id="request_delete_button" type="button" class="btn btn-danger">Delete

              </button> -->

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/digitalforms/dept-forms.js?v=1.0.9"></script>





