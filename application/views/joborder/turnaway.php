<style>
    .dataTables_filter {
        display: none;
    }
    .table-hover{
        cursor: pointer;
    }

    @media (min-width: 992px){
        #customer_table_wrapper{
            border-right: 1px solid #eee; 
        }
    }

    .inputContainer {
        width: 100%;
        margin-bottom: 7px;
    }
    .inputContainer label {
        float: left;
        margin-right: 5px;
        margin-top: 3px;
        width: 100px;
    }
    .inputContainer div {
        overflow: hidden;
    }
    .inputContainer input {
        width: 100%;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        display: block
    }
    #fov{
        font-size: 12px;
        padding-top: 4px;
        font-weight: bold;
    }
</style>

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">

<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">

<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">


<div class="box box-primary">
  <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px;">
                <h4>Turn Away 
                    <?php
                        // $allowed_branch = [1, 169, 170, 171, 172, 173];
                        if($user->user_role == "Standard User" && $user->user_position == "Standard User" ){
                    ?>
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_turnaway_modal">Add Turn Away</button>
                    <?php
                        }
                    ?>
                    
                </h4>
            </div>
        </div>
        <hr style="margin: 0px 0px 20px 0px;">
        <div class="row">
            <div class="col-md-12">
                <?php
                    if($user->brand_id == 100007 || $user->user_position == "Area Manager"){
                ?>
                <select id="branches" class="form-control input-sm select2" style="display: inline-block !important; width: 15%; border-radius: 3px !important; height: 34px; font-size: 15px;">
                    <option value="" hidden selected>Select Branch</option>
                    <?php
                        foreach($branch as $key => $value){
                    ?>
                    <option value="<?= $value->branch_id?>"><?= $value->branch_name?></option>
                    <?php
                        }
                    ?>
                </select>
                <?php
                    }
                ?>
                <button type="button" class="btn btn-default" id="daterange_btn">
                    <span>
                      <i class="fa fa-calendar"></i> Select date
                    </span>
                    <i class="fa fa-caret-down"></i>
                </button>
                <button type="button" class="btn btn-info" onclick="daterange_search()">Search</button>
            </div>
            <div class="col-md-12">
                <input type="hidden" id="start">
                <input type="hidden" id="end">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-12">
                      <div class="table-responsive">
                        <table id="turnaways_table" class="table table-bordered table-hover" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Quantity</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>


<div class="modal fade" id="add_turnaway_modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button id="add_turnaway_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Turn Away</h4>
          </div>
          <div class="modal-body">
              <div id="add_turnaway_alert_success" class="alert alert-success alert-dismissible">
              <h4><i class="icon fa fa-check"></i> Success!</h4>
              You have successfully added a turn away!
              </div>
              <form id="add_turnaway_form" method="POST">
                <div id="add_turnaway_error"></div>
                <div class="row">
                    <div class="col-md-12">
                      <label for="add_turnaway_date">Date</label>
                      <input id="add_turnaway_date" name="add_turnaway_date" class="form-control" type="text">
                      <label for="add_turnaway_type">Type</label>
                      <select id="add_turnaway_type" name="add_turnaway_type" class="form-control" type="text">
                          <option value="" hidden selected>Select type</option>
                          <option value="No Card Terminal">No Card Terminal</option>
                          <option value="No Service Provider">No Service Provider</option>
                          <option value="Service Not Available">Service Not Available</option>
                          <option value="Others">Others</option>
                      </select>
                      <label for="add_turnaway_turnaway">Quantity</label>
                      <input id="add_turnaway_turnaway" name="add_turnaway_turnaway" class="form-control" type="text">
                      <label for="add_turnaway_remarks">Remarks</label>
                      <textarea id="add_turnaway_remarks" name="add_turnaway_remarks" class="form-control" type="text" rows="3"></textarea>
                    </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
              <button id="add_turnaway_button" type="button" onclick="add_turnaway()" class="btn btn-info">Add
              </button>
          </div>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.box-body -->

<div class="modal fade" id="turnaway_modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button id="turnaway_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Turn Away</h4>
          </div>
          <div class="modal-body">
              <div id="turnaway_alert_success" class="alert alert-success alert-dismissible">
              <h4><i class="icon fa fa-check"></i> Success!</h4>
              You have successfully <span class="label label-info" id="turnaway_function" style="font-size: 12px; margin-left: 3px;"></span> Turnaway ID:  <span id="turnaway_function_id" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>
              </div>
              <form id="turnaway_form" method="POST">
                <div id="turnaway_error"></div>
                <div class="row">
                    <div class="col-md-12">
                      <label for="turnaway_id">Id</label>
                      <input id="turnaway_id" name="turnaway_id" class="form-control" type="text" disabled>
                      <label for="turnaway_date">Date</label>
                      <input id="turnaway_date" name="turnaway_date" class="form-control" type="text" disabled>
                      <label for="turnaway_type">Type</label>
                      <select id="turnaway_type" name="turnaway_type" class="form-control">
                          <option value="" hidden selected>Select type</option>
                          <option value="No Card Terminal">No Card Terminal</option>
                          <option value="No Service Provider">No Service Provider</option>
                          <option value="Service Not Available">Service Not Available</option>
                          <option value="Others">Others</option>
                      </select>
                      <label for="turnaway_turnaway">Turn Away</label>
                      <input id="turnaway_turnaway" name="turnaway_turnaway" class="form-control" type="text" disabled>
                      <label for="turnaway_remarks">Remarks</label>
                      <textarea id="turnaway_remarks" name="turnaway_remarks" class="form-control" type="text" rows="3" disabled></textarea>
                    </div>
                </div>
              </form>
          </div>
            <?php
                // $allowed_branch = [1, 169, 170, 171, 172, 173];
                if($user->user_role == "Standard User" && $user->user_position == "Standard User" ){
            ?>
          <div class="modal-footer">
              <button id="turnaway_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit
              </button>
              <button id="turnaway_update_button" type="button" onclick="update_turnaway()" class="btn btn-success" disabled="disabled">Update
              </button>
              <button id="turnaway_delete_button" type="button" onclick="delete_turnaway()" class="btn btn-danger">Delete
              </button>
          </div>
          <?php
                }
          ?>
      </div>
      <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.box-body -->

<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- AdminLTE App -->
<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<!-- Select2 -->
<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/customs/js/joborder/turnaway.js"></script>


