<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

  .select2-selection{

    margin-bottom: 3px !important;

  }

  .align-right{
    text-align: right !important;
  }

  .align-center{
    text-align: center !important;
  }

  .align-left{
    text-align: left !important;
  }

  .dt-buttons{
      text-align: right;
  }
 

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">
            <h4>Deposit</h4>
      
<?php 
   
    
    $position = $user->user_position;
    $branch_id = $user->branch_id;
    $brand_id = $user->brand_id;
    // print_r($user);
?>  
          <div class="col-md-12" <?php if($user->user_role == 'Standard User'){ echo "style='display:none;'";} ?>>
              <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>Select Brand</option> 
                        <?php foreach($brands as $row) { ?>
                                 <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>
                        <?php } ?>
              </select>

              <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                      <option  value="" hidden selected>Select Branch</option> 
              </select>
          </div>
       
          <button class="btn btn-primary btn-flat match-height" data-toggle="modal" data-target="#add_deposit_modal" style="margin-bottom: 3px; margin-left: 15px;">Add Deposit</button>

          <input type="hidden" id="start">


        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-12"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="deposit_table" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>
                <th>Brand</th>
                <th>Branch</th>
                <th>Deposit for Date</th>
                <th>Deposit Amount</th>
                <th>Date of Deposit</th>
                <th>Action</th>
              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

  </form>

</div>

<div class="modal fade" id="add_deposit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" >

      
      </div>
      <div class="modal-body">
        <form id="form_deposit">
                    <div class="form-group">
                        <h3>Deposit Details</h3>
                    </div>
                    <div class="form-group">
                        <input id="datepicker" type="text" class="form-control match-height" placeholder="Date">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="amount" class="variance" placeholder="Amount">
                    </div>
                    <div class="form-group">
                        <textarea id="remarks" placeholder="Remarks" class="form-control" style="height: 100px;"></textarea>
                    </div>
        </form>
      </div>
      <div class="modal-footer">
        <div id="message"></div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick = "add_deposit();">Submit</button>
      </div>
    </div>
  </div>
</div>


<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/plugins/matchHeight/jquery.matchHeight-min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/js/jszip.min.js"></script>
<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>
<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>
<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/joborder/deposit.js?v=1.0.4"></script>



