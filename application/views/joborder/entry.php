<style>

    .dataTables_filter {

        display: none;

    }

    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-left: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    #joborder_content{

        border-left: 1px solid #eee !important; 

    }



    input[type=number]::-webkit-inner-spin-button, 

    input[type=number]::-webkit-outer-spin-button { 

    -webkit-appearance: none; 

    margin: 0; 

    }

    #dialog{

        font-family: 'Source Sans Pro';

    }

    .modal {
    text-align: center;
    padding: 0!important;
    }

    .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
    }

    .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<!-- bootstrap datepicker -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<!-- iCheck -->

<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">

<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/css/jquery-ui.css" /> 



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">
        
        <div class="alert alert-dark alert-dismissible text-light" style="background:#3c8dbc; color: white;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <!-- <h4 class="display-4">Note:</h4> -->
            <p>You can now add  multiple SPs in one transaction. Select SP before entering a barcode on "Service Rendered" textbox.</p>
        </div>

        <div class="row">

            <div class="col-md-12" style="margin-bottom: 20px;">

                <a class="btn btn-primary" href="<?= base_url()?>joborder">Go Back</a>

                <input type="hidden" id="transaction_id" value="<?php if($joborder){echo $joborder->transaction_id;}?>">

            </div>

        </div>

        <hr style="margin: 0px;">

        <div class="row">

            <div class="col-md-6">

                <div class="row">

                    <div class="col-md-12 text-center">

                        <h3>Customer: 

                            <input id="customer_id" type="hidden" name="" value="<?php if(isset($customer->customer_id)){echo $customer->customer_id;}if(isset($customer->customerolddata_id)){echo $customer->customerolddata_id;}?>">

                            <span style="color:blue;">

                                <?php



                                    if(isset($customer->customer_firstname) && isset($customer->customer_lastname)){

                                        echo $customer->customer_firstname. ' '.$customer->customer_lastname;

                                    }



                                    if(isset($customer->customerolddata_firstname) && isset($customer->customerolddata_lastname)){

                                        echo $customer->customerolddata_firstname. ' '.$customer->customerolddata_lastname;

                                    }

                                    

                                ?>

                            </span>

                        </h3>

                    </div>

                </div>

                <hr>

                <div class="row">

                    <div class="col-md-6">

                        <div class="inputContainer">

                            <label>Date Rendered</label>

                            <div>

                                <input id="date_rendered" class="form-control input-sm" type="text" 

                                value="<?php if($joborder){echo date('m/d/Y', strtotime($joborder->transaction_date));}?>" <?= $joborder ? 'disabled' : ''?>/>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="inputContainer">

                            <label>Docket No.</label>

                            <div><input id="docket_no" class="form-control input-sm" type="number" value="<?php if($joborder){echo $joborder->transaction_docketno;}?>" <?= $joborder ? 'disabled' : ''?>/></div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <div class="inputContainer">

                            <label id="fov">Frequent of Visit</label>

                            <div><input id="foottraffic" class="form-control input-sm" type="text" disabled value="<?= isset($foottraffic) ? $foottraffic : '' ?>"/></div>

                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="inputContainer">

                            <label>OR No.</label>

                            <div><input id="or_no" class="form-control input-sm" type="number" value="<?php if($joborder){echo $joborder->transaction_orno;}?>" <?= $joborder ? 'disabled' : ''?>/></div>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-6">

                        <label>Serviced by:</label>

                        <select id="service_providers" name="service_providers" class="form-control input-sm select2" <?= $joborder ? 'disabled' : ''?>>

                            <option value="" hidden selected>Select Nail Tech/Stylist Name</option>

                            <?php

                                foreach($serviceproviders as $serviceprovider){

                            ?>

                            <option value="<?= $serviceprovider->serviceprovider_id?>"><?= $serviceprovider->serviceprovider_name?></option>

                            <?php

                                }

                            ?>

                        </select>

                    </div>

                    <?php
                        if($user->brand_id == 100001 || $user->brand_id == 100003){
                    ?>

                    <div class="col-md-6">

                        <label>Assisted by:</label>

                        <select id="service_assisters" name="service_assisters" class="form-control input-sm select2" <?= $joborder ? 'disabled' : ''?>>

                            <option value="" hidden selected>Select Nail Tech/Stylist Name</option>

                            <?php

                                foreach($serviceproviders as $serviceprovider){

                            ?>

                            <option value="<?= $serviceprovider->serviceprovider_id?>"><?= $serviceprovider->serviceprovider_name?></option>

                            <?php

                                }

                            ?>

                        </select>

                    </div>

                    <?php }?>

                </div>

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                        <div class="nav-tabs-custom">

                            <ul class="nav nav-tabs">

                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Product Services</a></li>

                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">OTC Products</a></li>

                            </ul>

                            <div class="tab-content">

                                <div class="tab-pane active" id="tab_1">

                                    <div class="row">

                                        <div class="col-md-4 pull-right">

                                            <label>Service Rendered</label>

                                            <div class="input-group pull-right">

                                                <input id="search_product_service" type="number" class="form-control input-sm" style="height: 31px;"<?= $joborder ? 'disabled' : ''?>>



                                                <div class="input-group-addon" style="padding: 0;">

                                                <button id="search_product_service_button" onclick="search_product_service()" class="btn btn-primary btn-sm btn-flat" style="height: 30px;" <?= $joborder ? 'disabled="disabled"' : ''?>>Add</button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row" style="margin-top: 20px;">

                                        <div class="col-md-12">

                                            <div class="table-responsive">

                                                <table id="product_services_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                                    <thead>

                                                        <tr>

                                                            <th>Prod ID</th>

                                                            <th>PS Code</th>

                                                            <th>Product Service</th>

                                                            <th>Total Sales</th>

                                                            <th>SP ID</th>

                                                            <th>SP Name</th>

                                                            <th>Assist ID</th>

                                                            <th>Assisted By</th>

                                                            <th>Action</th>

                                                        </tr>

                                                    </thead>

                                                    <tbody>

                                                    </tbody>

                                                </table>

                                                <button style="display: none;" class="btn btn-primary pull-left" style="margin-right: 10px;" >Edit</button>

                                                <button style="display: none;" class="btn btn-danger pull-left" >Delete</button>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="tab_2">

                                    <div class="row">

                                        <div class="col-md-4 pull-right">

                                            <label>Service Rendered</label>

                                            <div class="input-group">

                                                <input id="search_retail_service" type="number" class="form-control input-sm" style="height: 31px;" <?= $joborder ? 'disabled' : ''?>>



                                                <div class="input-group-addon" style="padding: 0;">

                                                <button id="search_retail_service_button" class="btn btn-primary btn-sm btn-flat" style="height: 30px;" <?= $joborder ? 'disabled="disabled"' : ''?> onclick="search_product_service()">Add</button>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row" style="margin-top: 20px;">

                                        <div class="col-md-12">

                                            <div class="table-responsive">

                                                <table id="retail_services_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                                    <thead>

                                                        <tr>

                                                            <th>Prod ID</th>

                                                            <th>RS Code</th>

                                                            <th>Retail Service</th>

                                                            <th>Total Sales</th>

                                                            <th>SP ID</th>

                                                            <th>SP Name</th>

                                                            <th>Assister ID</th>

                                                            <th>Assister Name</th>

                                                            <th>Action</th>

                                                        </tr>

                                                    </thead>

                                                    <tbody>

                                                    </tbody>

                                                </table>

                                                <button style="display: none;" class="btn btn-primary btn-sm pull-left" style="margin-right: 10px;" >Edit</button>

                                                <button style="display: none;" class="btn btn-danger btn-sm pull-left" >Delete</button>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!-- /.tab-content -->

                        </div>

                    </div>

                </div>

            </div>

            <div id="customer_table_wrapper" class="col-md-6">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12" style="display: none;">

                        <p class="text-center" style="font-size: 15px; color: white; background: #888888"><strong>PRODUCT CONSUMABLE</strong></p>

                        <div class="table-responsive">

                            <table id="product_consumables_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                <thead>

                                    <tr>

                                        <th>Service Mix ID</th>

                                        <th>PS Code</th>

                                        <th>Consumable Code</th>

                                        <th>Consumable Name</th>

                                        <th>UOM Size</th>

                                        <th>Need QTY</th>

                                    </tr>

                                </thead>

                                <tbody>

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                        <p class="text-center" style="font-size: 15px; color: white; background: #888888"><strong>TYPE OF PAYMENT</strong></p>

                    </div>

                    <div class="col-md-12">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="row">

                                    <div class="col-md-12" style="padding: 3px 10px 13px 16px;">

                                        <input id="checkbox_cash" type="checkbox" class="minimal"><span style="margin-left: 20px;">CASH</span>

                                    </div>

                                    <div class="col-md-12" style="padding: 3px 10px 13px 16px;">

                                        <input id="checkbox_credit" type="checkbox" class="minimal"><span style="margin-left: 20px;">CREDIT</span>

                                    </div>

                                    <div class="col-md-12" style="padding: 3px 10px 13px 16px;">

                                        <input id="checkbox_gc" type="checkbox" class="minimal"><span style="margin-left: 20px;">GIFT CHEQUE</span>

                                    </div>

                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="inputContainer">

                                            <label style="font-weight: 400;">CASH AMOUNT</label>

                                            <div><input id="input_cash" style="text-align:right;" name="input_cash" class="form-control input-sm" type="number" disabled value="<?php if($joborder){echo $joborder->transaction_paymentcash;}?>"></div>

                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="inputContainer">

                                            <label style="font-weight: 400;">CREDIT AMOUNT</label>

                                            <div><input id="input_credit" style="text-align:right;" name="input_credit" class="form-control input-sm" type="number" disabled value="<?php if($joborder){echo $joborder->transaction_paymentcard;}?>"></div>

                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="inputContainer">

                                            <label style="font-weight: 400;">GC AMOUNT</label>

                                            <div><input id="input_gc" style="text-align:right;" name="input_gc" class="form-control input-sm" type="number" disabled value="<?php if($joborder){echo $joborder->transaction_paymentgc;}?>"></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <hr class="gc-details">

                        <div class="row gc-details">

                            <div class="col-md-12">

                                <div class="table-responsive">

                                    <table id="gc_details" class="table table-bordered table-hover" style="width: 100% !important;">

                                        <thead>

                                            <div class="input-group col-md-4">

                                                <input id="search_gc" type="number" class="form-control input-sm" style="height: 31px;"<?= $joborder ? 'disabled' : ''?>>



                                                <div class="input-group-addon" style="padding: 0;">

                                                <button id="search_gc_button" onclick="search_gc()" class="btn btn-primary btn-sm btn-flat" style="height: 30px;" <?= $joborder ? 'disabled="disabled"' : ''?>>Add</button>

                                                </div>

                                            </div>

                                            <tr>

                                                <th>ID</th>

                                                <th>GC Code</th>

                                                <th>Gift Name</th>

                                                <th>Gift Amount</th>

                                                <th>Action</th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                        </tbody>

                                    </table>

                                </div>

                            </div>

                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-6">

                                        <p style="margin: 5px 0px 0px 0px;">TOTAL PAYMENT</p>

                                    </div>

                                    <div class="col-md-6">

                                        <input id="total_payment" style="text-align:right;" class="form-control input-sm" type="text" disabled="disabled">

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                        <p class="text-center" style="font-size: 15px; color: white; background: #888888"><strong>PRODUCT RETAIL/SUB-TOTAL</strong></p>

                    </div>

                    <div class="col-md-12">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-6">

                                        <p style="margin: 5px 0px 0px 0px;">TOTAL SALES</p>

                                    </div>

                                    <div class="col-md-6">

                                        <input id="total_sales" style="text-align:right;" class="form-control input-sm" type="text" disabled="disabled" value="0.00" />

                                    </div>

                                </div>

                                <hr>

                                <?php

                                    if($this->uri->segment(5) == NULL){

                                ?>    

                                 

                                <div class="row">

                                    <div class="col-md-12"  <?= $joborder ? 'style="display: none;"' : ''?>>

                                        <button id="button_save_transaction" class="btn btn-success pull-right" style="margin-right: 10px;" onclick="insert_transaction()">Save</button>

                                    </div>

                                    <div class="col-md-12"  <?= !$joborder ? 'style="display: none;"' : ''?>>

                                        <button id="delete_joborder_button" class="btn btn-danger pull-right" style="margin-right: 10px;" onclick="delete_joborder()">Delete</button>

                                        <button id="update_joborder_button" class="btn btn-success pull-right" style="margin-right: 10px;" disabled="disabled" onclick="update_joborder()">Update</button>

                                        <button id="enable_editing" class="btn btn-warning pull-right" style="margin-right: 10px;" onclick="enable_editing()">Edit</button>

                                    </div>

                                </div>

                                <?php }?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

      

    </div>

    <!-- /.box-body -->

</div>

<div id="dialog" title="Gift Cheque">

</div>



<div id="dialog1" title="Gift Cheque">

</div>



<div id="gc_validation_dialog" title="Gift Cheque">

    <div id="error"></div>

    <div id="gc">

        <label for="gc_pin">Enter pin</label>

        <input id="gc_barcode" type="hidden">

        <input class="form-control" type="text" id="gc_pin" style="text-align:center; margin-bottom:10px;">

        <button class="btn btn-success btn-xs pull-right" onclick="validate_gc()">Submit</button>

    </div>

</div>

<!-- <script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script> -->


<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content" style="background: none !important;">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
            <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
            <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
              </circle>
            </svg>
          </div>
          
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script src="<?= base_url()?>assets/js/jquery-1.9.1.js"></script> 

<script src="<?= base_url()?>assets/js/jquery-ui.js"></script>


<!-- Bootstrap 3.3.7 -->


<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- Data Tables -->

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- bootstrap datepicker -->

<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- SlimScroll -->

<script src="<?= base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

<!-- iCheck 1.0.1 -->

<script src="<?= base_url()?>assets/plugins/iCheck/icheck.min.js"></script>

<!-- FastClick -->

<script src="<?= base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/joborder/entry.js?v=1.1.19"></script>



