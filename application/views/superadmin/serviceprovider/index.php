<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

  <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="serviceproviders_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>

                                    <th>Branch</th>

                                    <th>Name</th>

                                    <th>Action</th>

                                </tr>

                            </thead>

                            <tbody>

                            </tbody>

                        </table>

                    </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>





<div class="modal fade" id="add_serviceprovider_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="add_serviceprovider_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Add SP</h4>

          </div>

          <div class="modal-body">

              <div id="add_serviceprovider_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully added a serviceprovider!

              </div>

              <form id="add_serviceprovider_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                    <label for="add_branches">Branch</label>

                    <select id="add_branches" name="add_branches" class="form-control select2" style="width: 100%;">
                    <option value="">Select branch</option>
                    <?php
                        foreach($branches as $branch){
                    ?>

                    <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                    <?php }?>

                    </select>

                    <label for="add_serviceprovider_name">Name</label>

                    <input id="add_serviceprovider_name" name="add_serviceprovider_name" class="form-control" type="text">

                    <label for="add_serviceprovider_level">Position</label>

                    <select name="add_serviceprovider_level" id="add_serviceprovider_level" class="form-control select2" style="width: 100%;">
                        <option value="">Select position</option>

                        <?php
                            foreach($positions as $position){
                        ?>

                        <option value="<?= $position->position_id?>"><?= $position->position_name?></option>

                        <?php }?>
                    
                    </select>

                    <label for="add_serviceprovider_empid">Employee ID</label>

                    <input id="add_serviceprovider_empid" name="add_serviceprovider_empid" class="form-control" type="text">

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="add_serviceprovider_button" type="button" onclick="add_serviceprovider()" class="btn btn-info">Add

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<div class="modal fade" id="serviceprovider_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="serviceprovider_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">SP Info</h4>

          </div>

          <div class="modal-body">

              <div id="serviceprovider_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="serviceprovider_function" style="font-size: 12px; margin-left: 3px;"></span> a serviceprovider.

              </div>

              <form id="serviceprovider_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                      <input id="serviceprovider_id" name="serviceprovider_id" class="form-control" type="hidden">

                      <label for="branches">Branch</label>

                      <select id="branches" name="branches" class="form-control select2" disabled style="width: 100%;">
                        <?php
                            foreach($branches as $branch){
                        ?>

                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                        <?php }?>

                      </select>

                      <label for="serviceprovider_name">Name</label>

                      <input id="serviceprovider_name" name="serviceprovider_name" class="form-control" type="text" disabled>

                      <label for="serviceprovider_level">Position</label>

                      <select name="serviceprovider_level" id="serviceprovider_level" class="form-control select2" style="width: 100%;">
                        <option value="">Select position</option>
                        <?php
                            foreach($positions as $position){
                        ?>

                        <option value="<?= $position->position_id?>"><?= $position->position_name?></option>

                        <?php }?>
                    
                      </select>

                      <label for="serviceprovider_empid">Employee ID</label>

                      <input id="serviceprovider_empid" name="serviceprovider_empid" class="form-control" type="text" disabled>

                    </div>

                </div>

              </form>

          </div>


          <div class="modal-footer">

              <!-- <button id="serviceprovider_edit_button"  type="button" onclick="enable_editing()" class="btn btn-warning" <?php if($user->user_name != 'Super Admin'){ echo 'style="display: none;"';} ?>>Edit

              </button>

              <button id="serviceprovider_update_button" type="button" onclick="update_serviceprovider()" class="btn btn-success" disabled="disabled"  <?php if($user->user_name != 'Super Admin'){ echo 'style="display: none;"';} ?>>Update -->

              </button>

              <button id="serviceprovider_delete_button" type="button" onclick="delete_serviceprovider()" class="btn btn-danger">Delete

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/superadmin/serviceprovider/index.js?v=1.0.7"></script>





