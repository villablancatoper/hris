<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 

    .hide{

        display: none;

    }

    .shown{

        display: all;

    }

    .none{
      display: none;
    }
  

</style>





<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

  <form role="form">
      <div class="box box-primary">

  <!-- /.box-header -->

          <div class="box-body">

              <div class="row">

                  <div class="col-md-12">
                      <div>
                        <input type="text" name="" id="name_query" class="form-control" style="width: 200px;  display: inline-block;"  placeholder="">
                        
                        <button type="button" class="btn btn-default" id="daterange_btn" >

                          <span id="date_range" >

                          <i class="fa fa-calendar" ></i> Select Date

                          </span>

                          <i class="fa fa-caret-down"></i>

                        </button>

                        <button type="button" onclick="search_name();" class="btn btn-primary" style="width: 200px;">Search</button>
                      </div>

                      <div class="row" style="margin-top: 20px;">

                          <div class="col-md-12">

                            <div class="table-responsive">

                              <table id="attendance_table" class="table table-bordered table-hover" style="width: 100% !important;">

                                  <thead>

                                      <tr>
                                          <th>Department</th>
                                          <th>User No.</th>
                                          <th>User ID</th>
                                          <th>Name</th>
                                          <th>Date/Time</th>
                                          <th class="none">Status</th>
                                          <th class="none">Operation</th>
                                          <th class="none">Exception Description</th>
                                          <th class="none">Timetable</th>
                                          <th class="none">Identification Code</th>
                                          <th class="none">Identification</th>
                                          <th class="none">Work Code</th>
                                          <th>Device No.</th>
                                          <th class="none">Checked</th>
                                          <th>Device location</th>
                                      </tr>

                                  </thead>

                                  <tbody>

                                  </tbody>

                              </table>

                          </div>

                          </div>

                      </div>

                  </div>

              </div>

          </div>
          <input type="hidden" id="start" name="">
          <input type="hidden" id="end" name="">

          <!-- /.box-body -->

      </div>


  </form>

</div>






<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>




<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/settings/attendance.js"></script>



