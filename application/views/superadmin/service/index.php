<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: right !important;
    }

    .modal {
    text-align: center;
    padding: 0!important;
    }

    .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
    }

    .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

  <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

                <select id="brands" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Brand</option>

                    <?php

                        foreach($brands as $brand){

                    ?>

                    <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

                    <?php  

                        }

                    ?>

                </select>

                <button class="btn btn-primary btn-flat match-height" onclick="search_services()">Search</button>

                <button class="btn btn-primary match-height pull-right" data-toggle="modal" data-target="#add_servicemix_modal">Add Service Mix</button>

            </div>

        </div>

        <hr style="margin-bottom: 0;">

        <div class="row">

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="services_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>
                                  
                                    <th>Brand ID</th>

                                    <th>Brand</th>

                                    <th>Description</th>

                                    <th>Amount</th>

                                    <th>Action</th>

                                </tr>

                            </thead>

                            <tbody>

                            </tbody>

                        </table>

                      </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>





<div class="modal fade" id="add_servicemix_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="add_servicemix_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Add Service Mix</h4>

          </div>

          <div class="modal-body">

              <div id="add_servicemix_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully added a service mix!

              </div>

              <div id="add_servicemix_form">

                <div class="row">

                    <div class="col-md-12">

                        <label for="add_brands">Brand</label>

                        <select id="add_brands" name="add_brands" class="form-control select2" style="width: 100%;">
                        <option value="">Select branch</option>
                        <?php

                            foreach($brands as $brand){

                        ?>

                        <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

                        <?php  

                            }

                        ?>

                        </select>

                        <div id="productservice_selection" style="display: none;">

                            <label for="add_productservice">Service</label>

                            <select id="add_productservice" name="add_productservice" class="form-control select2" style="width: 100%;">
                            <option value="">Select service</option>

                            </select>
                        
                        </div>

                    </div>

                </div>

                <hr>

                <div id="item_selection" class="row" style="display: none;">

                    <div class="col-md-12">

                        <div class="row">
                            
                            <div class="col-md-8">

                                <label for="items">Item</label>

                                <select id="items" name="items" class="form-control select2" style="width: 100%;">

                                </select>

                            </div>

                            <div class="col-md-4">

                                <label for="item_quantity">Quantity</label>

                                <input id="item_quantity" name="item_quantity" class="form-control" type="text">

                            </div>

                        </div>

                        <div class="row" style="margin-top: 10px;">

                            <div class="col-md-12">

                                <button class="btn btn-primary match-height pull-right" onclick="add_item_on_servicemix()">Add</button>

                            </div>
                            
                        </div>

                    </div>
                            
                </div>

                <hr>

                <div class="row">

                    <div class="col-md-12">

                        <div class="table-responsive">

                            <table id="add_servicemix_datatable" class="table table-bordered table-hover" style="width: 100% !important;">

                                <thead>

                                    <tr>

                                        <th>Code</th>

                                        <th>Item</th>

                                        <th>Quantity</th>

                                        <th>UOM</th>

                                        <th>Action</th>

                                    </tr>

                                </thead>

                                <tbody>

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

          </div>

          <div class="modal-footer">

              <button id="add_servicemix_button" type="button" onclick="add_servicemix()" class="btn btn-info">Add

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<div class="modal fade" id="service_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="service_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Service Info</h4>

          </div>

          <div class="modal-body">

              <div id="service_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="service_function" style="font-size: 12px; margin-left: 3px;"></span> a service.

              </div>

              <div id="service_form">

                <div class="row">

                    <div class="col-md-12">

                      <input id="service_id" name="service_id" class="form-control" type="hidden">

                      <input id="productservice_code" name="productservice_code" class="form-control" type="hidden">

                      <!-- <label for="branches">Branches</label> -->

                      <!-- <select id="branches" name="branches" class="form-control select2" disabled style="width: 100%;">
                        <?php
                            foreach($branches as $branch){
                        ?>

                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                        <?php }?>

                      </select> -->

                      <label for="brand">Brand</label>

                      <input id="brand" name="brand" class="form-control" type="text" disabled>

                      <input type="hidden" id="brand_id">

                      <label for="service_description">Description</label>

                      <input id="service_description" name="service_description" class="form-control" type="text" disabled>

                      <hr>
                      
                      <div class="row item-selection">

                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-8">
                              
                              <label for="items">Item</label>

                              <select class="items" name="items" class="form-control select2" style="width: 100%;">

                              </select>

                            </div>

                            <div class="col-md-4">
                              <label for="add_item_quantity">Quantity</label>
                              <input id="add_item_quantity" name="add_item_quantity" type="text" class="form-control">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                          <div class="row">
                            <div class="col-md-12">
                              <button class="btn btn-primary pull-right" type="button" onclick="add_servicemix_item()">Add</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <hr>

                      <div class="row">

                          <div class="col-md-12">
                        
                            <div class="table-responsive">

                                <table id="servicemix_datatable" class="table table-bordered table-hover" style="width: 100% !important;">

                                    <thead>

                                        <tr>

                                            <th>ID</th>

                                            <th>Item</th>

                                            <th>Quantity</th>

                                            <th>UOM</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    </tbody>

                                </table>

                                <div id="servicemix_tabledit_wrapper"></div>

                            </div>

                          </div>

                      </div>

                    </div>

                </div>

            </div>

          </div>

          <div class="modal-footer">

              <button id="service_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit

              </button>

              <button id="service_update_button" type="button" onclick="update_service()" class="btn btn-success" disabled="disabled">Update

              </button>

              <button id="service_delete_button" type="button" onclick="delete_service()" class="btn btn-danger">Delete

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->

<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content" style="background: none !important;">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
            <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
            <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
              </circle>
            </svg>
          </div>
          
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/plugins/tabledit/jquery.tabledit.js"></script>



<script src="<?= base_url()?>assets/customs/js/superadmin/service/index.js?v=1.0.2"></script>





