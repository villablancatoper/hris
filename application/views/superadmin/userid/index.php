<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



                    <select  id="test_branches" name="test_branches" class="form-control select2" style="width: 100%;" hidden="hidden" >
                    <option value=""></option>
                    <?php
                        foreach($branches as $branch){
                    ?>

                    <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                    <?php }?>

                    </select>

 

<div class="box box-primary" style="margin-top: -40px;">

  <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="users_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>

                                    <th>Employee Number</th>
                                    <th>Biometric ID</th>

                                    <th>Last Name</th>

                                    <th>First Name</th>

                                   
                                    
                                    <th>Middle Name</th>
                                    <th>Company</th>

                                    <th >Branch</th>
                                    <th>Status</th>
                                    <th>Actions</th>

                                   

                                </tr>

                            </thead>

                            <tbody>

                            </tbody>

                        </table>

                    </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>





<div class="modal fade" id="add_user_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="add_user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Add Employee</h4>

          </div>

          <div class="modal-body">

              <div id="add_user_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully added/updated an employee!

              </div>

              <form id="add_user_form" method="POST">

                <div class="row">

                    <div class="col-md-12">
                    <label for="add_company">Company</label>

                    <select id="add_company" name="add_company" class="form-control select2" style="width: 100%;">
                    <option value="">Select Company</option>
                    <option value="BOCI">BOCI</option>
                    <option value="CGI">CGI</option>
                    <option value="GNEI">GNEI</option>
                    <option value="GTGI">GTGI</option>
                    <option value="GWEEI">GWEEI</option>
                    <option value="GWEI">GWEI</option>
                    <option value="JBEI">JBEI</option>
                    <option value="LIBI">LIBI</option>
                    <option value="LIGI">LIGI</option>
                    <option value="LMNI">LMNI</option>
                    <option value="NFI">NFI</option>
                    <option value="NKI">NKI</option>
                    <option value="SSI">SSI</option>
                    <option value="TBPI">TBPI</option>

                    

                    

                    </select>

                    <label for="add_branches">Branch</label>

                    <select id="add_branches" name="add_branches" class="form-control select2" style="width: 100%;">
                    <option value="">Select Branch</option>
                    <?php
                        foreach($branches as $branch){
                    ?>

                    <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                    <?php }?>

                    </select>
                    <label for="add_user_number">Employee Number</label>
                    <input id="add_user_number" name="add_user_number" class="form-control" type="text">
                    <label for="add_user_lastname">Last Name</label>
                    <input id="add_user_lastname" name="add_user_lastname" class="form-control" type="text">
                    <label for="add_user_firstname">First Name</label>
                    <input id="add_user_firstname" name="add_user_firstname" class="form-control" type="text">
                    <label for="add_user_middlename">Middle Name</label>


                    <input id="add_user_middlename" name="add_user_middlename" class="form-control" type="text">

                    

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="add_user_button" type="button" onclick="add_user()" class="btn btn-info">Add

              </button>

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>


<!-- /.box-body -->



<div class="modal fade" id="user_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

              <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>

              <h4 class="modal-title">Employee Info</h4>

          </div>

          <div class="modal-body">

              <div id="user_alert_success" class="alert alert-success alert-dismissible">

              <h4><i class="icon fa fa-check"></i> Success!</h4>

              You have successfully <span class="label label-info" id="user_function" style="font-size: 12px; margin-left: 3px;"></span> a employee.

              </div>

              <form id="user_form" method="POST">

                <div class="row">

                    <div class="col-md-12">
                       <input id="emp_num" name="emp_num" class="form-control hidden" type="text" disabled="disabled">
                      <label for="update_company">Company</label>

                    <select id="update_company" name="update_company" class="form-control select2" style="width: 100%;" disabled="disabled">
                    <option value="">Select Company</option>
                    <option value="BOCI">BOCI</option>
                    <option value="CGI">CGI</option>
                    <option value="GNEI">GNEI</option>
                    <option value="GTGI">GTGI</option>
                    <option value="GWEEI">GWEEI</option>
                    <option value="GWEI">GWEI</option>
                    <option value="JBEI">JBEI</option>
                    <option value="LIBI">LIBI</option>
                    <option value="LIGI">LIGI</option>
                    <option value="LMNI">LMNI</option>
                    <option value="NFI">NFI</option>
                    <option value="NKI">NKI</option>
                    <option value="SSI">SSI</option>
                    <option value="TBPI">TBPI</option>

                    

                    

                    </select>

                    <label for="update_branches">Branch</label>

                    <select id="update_branches" name="update_branches" class="form-control select2" style="width: 100%;" disabled="disabled">
                    <option value="">Select Branch</option>
                    <option value="404"></option>
                    <?php
                        foreach($branches as $branch){
                    ?>

                    <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                    <?php }?>

                    </select>
                    
                    <label for="update_user_lastname">Last Name</label>
                    <input id="update_user_lastname" name="update_user_lastname" class="form-control" type="text" disabled="disabled">
                    <label for="update_user_firstname">First Name</label>
                    <input id="update_user_firstname" name="update_user_firstname" class="form-control" type="text" disabled="disabled">
                    <label for="update_user_middlename">Middle Name</label>


                    <input id="update_user_middlename" name="update_user_middlename" class="form-control" type="text" disabled="disabled">
                    <label for="update_status">Set Active/Inactive</label>

                    <select id="update_status" name="update_status" class="form-control select2" style="width: 100%;" disabled="disabled">
                    <option value="">Set Active/Inactive </option>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                    

                    

                    

                    </select>
                    


                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">

              <button id="user_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit

              </button>

              <button id="user_update_button" type="button" onclick="update_user()" class="btn btn-success" disabled="disabled">Update

              </button>

             

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<!-- /.box-body -->



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/superadmin/userid/index.js?v=1.0.7"></script>





