<style>


    .table-hover{

        cursor: pointer;

    }

    div.dataTables_wrapper div.dataTables_filter {
        text-align: left !important;
    }

    .loader.small, .loader.small:after {
        width: 6em;
        height: 6em;
    }
    .loader, .loader:after {
        border-radius: 50%;
        width: 10em;
        height: 10em;
    }
    .loader, .loader.inverted {
        border-left: 1.1em solid #fff;
    }
    .loader {
        display: inline-block;
        font-size: 4px;
        position: relative;
        text-indent: -9999em;
        border-top: 1.1em solid hsla(0,0%,100%,.2);
        border-right: 1.1em solid hsla(0,0%,100%,.2);
        border-bottom: 1.1em solid hsla(0,0%,100%,.2);
        animation: fa-spin 1.1s infinite linear;
    }

    tr > th {
        text-align: center;
        vertical-align: middle !important;
    }

    .dataTables_filter{
        display: none;
    }
    /* td, th{
        padding: 0px 5px 0px 5px;
        border: 1px solid black;
    } */

    .td-hack {
        width: 100%;
        height: 100%;
        position: relative;
        text-align: right !important;
    }
    .overlay {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #ff88887d;
        display: table;
        text-align: center;
    }
    .excel {
        width: 100%;
        height: auto;
        display: table-cell;
        vertical-align: middle;
    }

    div.dataTables_wrapper div.dataTables_filter {
        display: none;
    }

    .align-right{
        text-align: right !important;
    }
    
    th{
        text-align: center !important;
        vertical-align: middle !important;
    }

    .modal {
    text-align: center;
    padding: 0!important;
    }

    .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
    }

    .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    }

    .dataTables_length{
        display: none;
    }

    button > a{
        color: white !important;
    }

    @media screen {
      #printSection {
          display: none;
      }
    }

    @media print {
      /* @page {
        size: landscape; 
      } */
      body * {
        visibility:hidden;
        /* overflow: auto; */
        
      }
      #printSection, #printSection * {
        visibility:visible;
        -webkit-print-color-adjust: exact !important;
      }
      #printSection {
        margin-left: 0;
        position:absolute;
        left:0;
        top:0;
      }
      .print-title{
        display: block;
      }

      .table>tbody>tr>td{
        padding: 0px 1px 0px 1px;
        vertical-align: middle;
        text-align: right;
        border: .5px solid #000000 !important;
        -webkit-print-color-adjust: exact !important;
        box-shadow: inset 0 0 0 1000px gold;
      }

      .table>tbody>tr>td:first-child {
        padding: 0px 1px 0px 1px;
        vertical-align: middle;
        text-align: left !important;
      }

      .table-bordered>thead>tr>th{
        border: .5px solid #000000 !important;
        padding: 0px !important;
        width: 100px !important;
      }

      .header{
        page-break-after: always;
        page-break-inside: avoid;
        display: block;
      }

      div {
        display: block;
      }

      .row {
          margin-right: -15px;
          margin-left: -15px;
      }

      .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {
          float: left;
      }

      .col-md-6 {
          width: 50%;
      }
      .col-md-7 {
          width: 58.33333333%;
      }

      .col-md-5 {
          width: 41.66666667%;
      }

      .bg-danger{
        /* background-color: red !important; */
        box-shadow: inset 0 0 0 1000px gold;
      }

      .general-content, p{
        font-size: 7px;
      }
      @page{
        margin: 10px 13px 0px 13px;
      }
    }

    @media screen {
      #printThis, #printSection {
          display: none;
      }
    }

    .top-content{
      margin-left: 100px; 
      font-size: 9px;
    }

    .align-left{
      text-align: left !important;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

  <!-- /.box-header -->

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

                <select id="month_and_year" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Month & Year</option>

                    <!-- <option value="01" data-batch="1">January <?= date('Y')?> - Batch 1</option>

                    <option value="01" data-batch="2">January <?= date('Y')?> - Batch 2</option>

                    <option value="02" data-batch="1">February <?= date('Y')?> - Batch 1</option>

                    <option value="02" data-batch="2">February <?= date('Y')?> - Batch 2</option>

                    <option value="03" data-batch="1">March <?= date('Y')?> - Batch 1</option>

                    <option value="03" data-batch="2">March <?= date('Y')?> - Batch 2</option>

                    <option value="04" data-batch="1">April <?= date('Y')?> - Batch 1</option>

                    <option value="04" data-batch="2">April <?= date('Y')?> - Batch 2</option>

                    <option value="05" data-batch="1">May <?= date('Y')?> - Batch 1</option>

                    <option value="05" data-batch="2">May <?= date('Y')?> - Batch 2</option>

                    <option value="06" data-batch="1">June <?= date('Y')?> - Batch 1</option>

                    <option value="06" data-batch="2">June <?= date('Y')?> - Batch 2</option>

                    <option value="07" data-batch="1">July <?= date('Y')?> - Batch 1</option>

                    <option value="07" data-batch="2">July <?= date('Y')?> - Batch 2</option>

                    <option value="08" data-batch="1">August <?= date('Y')?> - Batch 1</option>

                    <option value="08" data-batch="2">August <?= date('Y')?> - Batch 2</option>

                    <option value="09" data-batch="1">September <?= date('Y')?> - Batch 1</option>

                    <option value="09" data-batch="2">September <?= date('Y')?> - Batch 2</option> -->

                    <!-- <option value="10" data-batch="1">October <?= date('Y')?> - Batch 1</option> -->

                    <option value="10" data-batch="2" data-year="2019">October 2019 - Batch 2</option>

                    <option value="11" data-batch="1" data-year="2019">November 2019 - Batch 1</option>

                    <option value="11" data-batch="2" data-year="2019">November 2019 - Batch 2</option>

                    <option value="12" data-year="2019">December 2019</option>
                    
                    <option value="01" data-batch="1" data-year="2020">January 2020 - Batch 1</option>
                    
                    <option value="01" data-batch="2" data-year="2020">January 2020 - Batch 2</option>
                    <option value="02" data-batch="1" data-year="2020">February 2020 - Batch 1</option>
                    
                    <option value="02" data-batch="2" data-year="2020">February 2020 - Batch 2</option>

                    <option value="03" data-batch="1" data-year="2020">March 2020 - Batch 1</option>
                    
                    <option value="03" data-batch="2" data-year="2020">March 2020 - Batch 2</option>

                </select>

                <!-- <select id="folders" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>Select Folder Name</option>
                <?php
                    foreach($folders as $folder){
                ?>
                    <option value="<?= $folder->branch_budgetfolder?>"><?= $folder->branch_budgetfolder?></option>
                <?php
                    }
                ?>
                </select>

                <select id="item_suppliers" class="form-control input-sm select2" style="display: none; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>No data available</option>
                </select> -->

                <button class="btn btn-primary btn-flat" onclick="get_folder_costs()">Search</button>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-12">

                <div id="items_per_supplier_wrapper" class="table-responsive">

                    <table id="items_per_supplier" class="table table-bordered table-hover">

                        <thead>
<!-- 
                            <tr>

                                <th rowspan="2">Supplier</th>

                                <th rowspan="2">Item Name</th>

                                <th rowspan="2">Shade</th>

                                <th rowspan="2">Color</th>

                                <th rowspan="2">Model</th>

                                <th rowspan="2">Unit Cost</th>

                                <th rowspan="2">UOM</th>

                                <th colspan="2">TOTAL</th>

                            </tr>

                            <tr>
                                
                                <th>Qty</th>

                                <th>Amount</th>

                            </tr> -->

                            <tr>

                                <th>Supplier</th>

                                <th>Action</th>                                

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

                <div id="suppliers_folders_wrapper" class="table-responsive">

                </div>

            </div>

        </div>

        <div class="row" id="printThis">

          <div class="col-md-12 summary">


          </div>

        </div>

    </div>

    <!-- /.box-body -->

</div>

<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content" style="background: none !important;">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
            <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
            <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
              </circle>
            </svg>
          </div>
          
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/budgetfolder/budgetfolder_sc.js?v=1.0.4"></script>





