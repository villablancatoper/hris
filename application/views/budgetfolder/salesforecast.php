<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
<style type="text/css">
    .dataTables_filter {
        float: left !important;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }
    input[type=number]::-webkit-inner-spin-button,

input[type=number]::-webkit-outer-spin-button {

    -webkit-appearance: none;

    margin: 0;

}
    #create_new_ticket {
        float: right;}

    .align-right{
        text-align:right; max-width:80px;
    }

    .align-left{
        text-align:left; max-width:80px;
    } 

    .align-center{
        text-align:center; max-width:80px;
    } 
</style>

<div class="modal fade" id="add_user_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

      <div class="modal-content">

          <div class="modal-header">

               <button id="user_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

              <span aria-hidden="true">×</span></button>
              <h4><span style="display: inline;" id="Action_storetarget" ></span></h4>
              <h4 class="modal-title" >Storetarget</h4>

          </div>

          <div class="modal-body">

              <div id="add_user_alert_success" class="alert alert-success alert-dismissible">

                <h4><i class="icon fa fa-check"></i> Success!</h4>

                You have successfully added a storetarget!

              </div>

              <form id="add_user_form" method="POST">

                <div class="row">

                    <div class="col-md-12">

                        <div class="row1"  style="margin-bottom: 20px;"> 

                            <label for="add_brand">Brand</label>

                            <select id="add_brand" name="add_brand"  class="form-control select2" style="width: 100%;">

                                <option value="" hidden selected>Select Brand</option>

                                <?php foreach($brands as $row) { ?>

                                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                                <?php } ?>

                            </select>

                            <label for="add_branches">Branch</label>

                            <select id="add_branches"  name="add_branches" class="form-control select2" style="width: 100%">

                                <option value="" hidden selected>Select Branch</option> 

                            </select>
                            <label for="Storetarget_month">Store Target Month</label>

                            <select id="storetarget_month_and_year" class="form-control input-sm select2" style="width: 100%">
                                <option value="" hidden selected>Select Month & Year</option>
                                <option value="2019-10-01">October 2019</option>
                                <option value="2019-11-01">November 2019</option>
                                <option value="2019-12-01">December 2019</option>
                                <option value="2020-01-01">January 2020</option>
                                <option value="2020-02-01">February 2020</option>
                                <option value="2020-03-01">March 2020</option>
                                <option value="2020-04-01">April 2020</option>
                                <option value="2020-05-01">May 2020</option>
                                <option value="2020-06-01">June 2020</option>
                                <option value="2020-07-01">July 2020</option>
                                <option value="2020-08-01">August 2020</option>
                                <option value="2020-09-01">September 2020</option>
                                <option value="2020-10-01">October 2020</option>
                                <option value="2020-11-01">November 2020</option>
                                <option value="2020-12-01">December 2020</option>
                                
                            </select>
                            
                        </div>
                        <hr>

                        <div  class="row_clearable"  style="margin-bottom: 20px;"> 

                           
                            <div class="row">
                            <div class="col-md-4">
                            <label for="Storetarget_originals">Original</label>
                            <input id="Storetarget_originals" name="Storetarget_originals"  style="text-align: right"min="0" value="0"  type="number"></input>
                            </div>
                            <div class="col-md-4">
                            <label for="Storetarget_fore">Forecast</label>
                            <input id="Storetarget_fore" disabled="disabled" name="Storetarget_fore" style="text-align: right" value="0"  type="text" ></input>
                            </div>
                            <div class="col-md-4">
                            <label for="Storetarget_average">Average Sales</label>
                            <input id="Storetarget_averages" name="Storetarget_averages"  style="text-align: right"min="0" value="0" type="number"></input>

                            </div>
                            
                           
                        </div>

                        <div class="row1"  style="margin-bottom: 20px;"> 
                                <div class="row">

                                    <div class="col-md-4">
                                    
                                    <input id="commaseparated" disabled="disabled" name="commaseparated"  style="text-align: right; display:none;"  type="text"></input>
                                    </div>
                                    <div class="col-md-4">
                                    
                                  
                                    </div>
                                    <div class="col-md-4">
                                    
                                    <input id="commaseparatedaverage" disabled="disabled" name="commaseparatedaverage"  style="text-align: right; display:none;"  type="text"></input>
                                    </div>
                                    

                                </div>
                        </div>
                        <hr>
                        <div class="row">

                        
                            <div class="col-md-4">
                            <label for="Storetarget_last_sales">Last Year Sales</label>
                            <input id="Storetarget_last_sales" name="Storetarget_last_sales"  style="text-align: right" min="0" value="0" type="number"></input>
                            </div>
                            <div class="col-md-4">
                            <label for="Storetarget_last_walkin">Last Year Walk-In</label>
                            <input id="Storetarget_last_walkin" name="Storetarget_last_walkin"  style="text-align: right" value="0"  min="0"type="number" ></input>
                            </div>
                            <div class="col-md-4">
                            <label for="Storetarget_last_regular">Last Year Regular</label>
                            <input id="Storetarget_last_regular" name="Storetarget_last_regular"  style="text-align: right" value="0" min="0" type="number"></input>

                            </div>
                            
                           
                   

                        </div>
                        <div class="row1"  style="margin-bottom: 20px;"> 
                                <div class="row">

                                    <div class="col-md-4">
                                    
                                    <input id="commaseparated_last_year" disabled="disabled" name="commaseparated_last_year"   style="text-align: right; display:none;"  type="text"></input>
                                    </div>
                                    <div class="col-md-4">
                                    
                                    <input id="commaseparated_last_walkin" disabled="disabled" name="commaseparated_last_walkin"  style="text-align: right; display:none;"  type="text"></input>
                                    </div>
                                    <div class="col-md-4">
                                    
                                    <input id="commaseparated_last_regular" disabled="disabled" name="commaseparated_last_regular"  style="text-align: right; display:none;"  type="text"></input>
                                    </div>
                                    

                                </div>
                        </div>           
                        <div class="row">

                        
                            <div class="col-md-4">
                            <label for="Storetarget_target_walkin">Target Walk-In</label>
                            <input id="Storetarget_target_walkin" disabled="disabled" name="Storetarget_target_walkin" value="0" style="text-align: right"  type="text"></input>
                            
                            </div>
                            <div class="col-md-4">
                            <label for="Storetarget_target_regular">Target Regular</label>
                            <input id="Storetarget_target_regular" disabled="disabled" name="Storetarget_target_regular" value="0" style="text-align: right"type="text" ></input>
                            </div>




                        </div>
                        <hr>
                        <label >Last Month Performance</label>
                        <div class="row">

                       
                            <div class="col-md-4">
    
                            <input id="Storetarget_last_performance" disabled="disabled" name="Storetarget_last_performance" value="0" style="text-align: right"  type="text" ></input>
                            </div>




                        </div>
                           

                            

                           
                        
                        </div>

                    </div>

                </div>

              </form>

          </div>

          <div class="modal-footer">
             
              <button id="add_user_button" type="button" onclick="add_storetarget()" class="btn btn-info" >Add

              </button>
             

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="box box-primary">
    <!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <!-- <div class="col-lg-2"> -->
                
                <!-- </div> -->
                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>Select Brand</option>

                    <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                    <?php } ?>

                </select>
                <!-- </div> -->

                <!-- <div class="col-lg-2" style="margin-bottom: 10px; margin-left: -10px !important; width: 230px !important;"> -->

                <select id="branch" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>Select Branch</option> 
                </select>
                
                <select id="month_and_year" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">
                    <option value="" hidden selected>Select Month & Year</option>
                    <option value="2019-10-01">October 2019</option>
                    <option value="2019-11-01">November 2019</option>
                    <option value="2019-12-01">December 2019</option>
                    <option value="2020-01-01">January 2020</option>
                    <option value="2020-02-01">February 2020</option>
                    <option value="2020-03-01">March 2020</option>
                    <option value="2020-04-01">April 2020</option>
                    <option value="2020-05-01">May 2020</option>
                    <option value="2020-06-01">June 2020</option>
                    <option value="2020-07-01">July 2020</option>
                    <option value="2020-08-01">August 2020</option>
                    <option value="2020-09-01">September 2020</option>
                    <option value="2020-10-01">October 2020</option>
                    <option value="2020-11-01">November 2020</option>
                    <option value="2020-12-01">December 2020</option>


                </select>
                <button onclick="compute();" class="btn btn-primary">Search</button>
                <button  class="btn btn-primary match-height pull-right" id="compute" data-toggle="modal" data-target="#add_user_modal" >ADD 
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="totalsales_table" class="table table-bordered table-hover" style="width: 100% !important;">
                        <thead>
                            <tr>
                                <th>Brand Name</th>
                                <th>Branch Name</th>
                                <th>Storetarget Month</th>
                                <th>Original StoreTarget</th>
                                <th>Sales Forecast</th>
                                <th>Average Sales</th>
                                <th>Last Month Performance</th>
                                <th>Last Year Sales</th>
                                <th>Last Year Walkin</th>
                                <th>Last Year Regular</th>
                                <th>Target Walkin</th>
                                <th>Target Regular</th>
                                <th>Actions </th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- modal currentinv-->

<div class="modal fade" id="currentinventory_modal" data-backdrop="static" data-keyboard="false">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button id="currentinventory_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

                <span aria-hidden="true">×</span></button>

                <h4 class="modal-title">Update Store Target</h4>

            </div>

            <div class="modal-body">

                <div id="currentinventory_alert_success" class="alert alert-success alert-dismissible">

                    <h4><i class="icon fa fa-check"></i> Success!</h4>

                    You have successfully <span class="label label-info" id="currentinventory_function" style="font-size: 12px; margin-left: 3px;"></span> an storetarget.

                </div>

                <form id="currentinventory_form" method="POST">

                    <div class="row">

                        <div class="col-md-12">

                        <!-- <label for="currentinventory_id">Id</label> -->

                        <input id="storetarget_id" name="storetarget_id" class="form-control" type="hidden" disabled>

                        <!-- <label for="branches">Branch</label>

                        <select id="branches" name="branches" class="form-control select2" disabled style="width: 100%;">
                        <?php
                            foreach($branches as $branch){
                        ?>

                        <option value="<?= $branch->branch_id?>"><?= $branch->branch_name?></option>

                        <?php }?>

                      </select> -->
                        <div class="row1"  style="margin-bottom: 20px;">
                            <label for="Storetarget_branch" type="hidden"></label>

                            <input id="Storetarget_branch"  type="hidden" name="Storetarget_branch" style="display: inline; "class="form-control" type="text" disabled="">
                            <br>
                        </div>

                        <div class="row1"  style="margin-bottom: 20px;">    
                        <label for="Storetarget_month">Store Target Month</label>

                            <select id="Storetarget_month" class="form-control input-sm select2" style="width: 100%; text-align: right;">
                                <option value="2019-10-01">October 2019</option>
                                <option value="2019-11-01">November 2019</option>
                                <option value="2019-12-01">December 2019</option>
                                <option value="2020-01-01">January 2020</option>
                                <option value="2020-02-01">February 2020</option>
                                <option value="2020-03-01">March 2020</option>
                                <option value="2020-04-01">April 2020</option>
                                <option value="2020-05-01">May 2020</option>
                                <option value="2020-06-01">June 2020</option>
                                <option value="2020-07-01">July 2020</option>
                                <option value="2020-08-01">August 2020</option>
                                <option value="2020-09-01">September 2020</option>
                                <option value="2020-10-01">October 2020</option>
                                <option value="2020-11-01">November 2020</option>
                                <option value="2020-12-01">December 2020</option>
                            </select>

                            
                        </div>
                        <hr>

                            <div  class="row_clearable"  style="margin-bottom: 20px;"> 

                            
                                <div class="row">
                                <div class="col-md-4">
                                <label for="Storetarget_original">Original</label>
                                <input id="Storetarget_original" name="Storetarget_original"  style="text-align: right"min="0"  type="number"></input>
                                </div>
                                <div class="col-md-4">
                                <label for="Storetarget_fores">Forecast</label>
                                <input id="Storetarget_fores" disabled="disabled" name="Storetarget_fores" style="text-align: right"   type="text" ></input>
                                </div>
                                <div class="col-md-4">
                                <label for="Storetarget_average">Average Sales</label>
                                <input id="Storetarget_average" name="Storetarget_average"  style="text-align: right"min="0"  type="number"></input>

                                </div>
                                
                            
                            </div>

                            <div class="row1"  style="margin-bottom: 20px;"> 
                                    <div class="row">

                                        <div class="col-md-4">
                                        
                                        <input id="edit_commaseparated" disabled="disabled" name="commaseparated"  style="text-align: right; display:none;"  type="text"></input>
                                        </div>
                                        <div class="col-md-4">
                                        
                                    
                                        </div>
                                        <div class="col-md-4">
                                        
                                        <input id="edit_commaseparatedaverage" disabled="disabled" name="commaseparatedaverage"  style="text-align: right; display:none;"  type="text"></input>
                                        </div>
                                        

                                    </div>
                            </div>
            <hr>
            <div class="row">


                <div class="col-md-4">
                <label for="edit_Storetarget_last_sales">Last Year Sales</label>
                <input id="edit_Storetarget_last_sales" name="edit_Storetarget_last_sales"  style="text-align: right" min="0" type="number"></input>
                </div>
                <div class="col-md-4">
                <label for="edit_Storetarget_last_walkin">Last Year Walk-In</label>
                <input id="edit_Storetarget_last_walkin" name="edit_Storetarget_last_walkin"  style="text-align: right"  min="0"type="number" ></input>
                </div>
                <div class="col-md-4">
                <label for="edit_Storetarget_last_regular">Last Year Regular</label>
                <input id="edit_Storetarget_last_regular" name="edit_Storetarget_last_regular"  style="text-align: right" min="0" type="number"></input>

                </div>
                
            


            </div>
            <div class="row1"  style="margin-bottom: 20px;"> 
                    <div class="row">

                        <div class="col-md-4">
                        
                        <input id="edit_commaseparated_last_year" disabled="disabled" name="edit_commaseparated_last_year"  style="text-align: right; display:none;"  type="text"></input>
                        </div>
                        <div class="col-md-4">
                        
                        <input id="edit_commaseparated_last_walkin" disabled="disabled" name="edit_commaseparated_last_walkin"  style="text-align: right; display:none;"  type="text"></input>
                        </div>
                        <div class="col-md-4">
                        
                         </div>
                        

                    </div>
                    </div>           
                    <div class="row">


                        <div class="col-md-4">
                        <label for="edit_Storetarget_target_walkin">Target Walk-In</label>
                        <input id="edit_Storetarget_target_walkin" disabled="disabled" name="edit_Storetarget_target_walkin" style="text-align: right"  type="text"></input>
                        
                        </div>
                        <div class="col-md-4">
                        <label for="edit_Storetarget_target_regular">Target Regular</label>
                        <input id="edit_Storetarget_target_regular" disabled="disabled" name="edit_Storetarget_target_regular" style="text-align: right"type="text" ></input>
                        </div>




                    </div>
                    <hr>
                    <label >Last Month Performance</label>
                    <div class="row">


                        <div class="col-md-4">

                        <input id="edit_Storetarget_last_performance" disabled="disabled" name="edit_Storetarget_last_performance" style="text-align: right"  type="text" ></input>
                        </div>




                    </div>
                    </div>
                </div>
                </form>

          </div>

          <div class="modal-footer">

              <button id="currentinventory_edit_button" type="button" onclick="enable_editing()" class="btn btn-warning">Edit

              </button>
              <button id="currentinventory_delete_button" type="button" onclick="delete_record()" class="btn btn-danger" disabled="disabled">Delete

              </button>

              <button id="currentinventory_update_button" type="button" onclick="update_currentinventory()" class="btn btn-success" disabled="disabled">Update

              </button>

              <!-- <button id="currentinventory_delete_button" type="button" onclick="delete_currentinventory()" class="btn btn-danger">Delete

              </button> -->

          </div>

      </div>

      <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>


    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="branch_name">

    <!-- /.box-body -->



    </form>



</div>





















<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>



<!-- Bootstrap 3.3.7 -->





<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->


<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<!-- <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->



<script src="<?php echo base_url(); ?>assets/customs/js/budgetfolder/salesforecast.js?v=1.0.2"></script>





</div>



<!-- /.content-wrapper -->

