<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">

    <!-- Ionicons -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">

    <!-- Theme style -->

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<style type="text/css">

    .dataTables_filter {

        float: left !important;

    }



    #code_color {

        width: 100px;

        border-radius: 40px/24px;

        outline: none;

    }



    #create_new_ticket {

        float: right;



    }

    .align-right{

        text-align:right; max-width:80px;

    }

    .align-left{

        text-align:left; max-width:80px;

    } 

    .align-center{

        text-align:center; max-width:80px;

    } 



</style>




<div class="box box-primary">



    <!-- /.box-header -->



    <!-- form start -->



    <div class="box-body">



        <div class="row">

            <div class="col-md-12">

                <!-- <div class="col-lg-2"> -->
                
    
                <!-- </div> -->
                <select id="brand" class="form-control select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php foreach($brands as $row) { ?>

                    <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>

                <?php } ?>

                </select>
     
                 <select id="month_and_year" class="form-control input-sm select2" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Month & Year</option>
                    <option value="01" data-year="2020">January 2020</option>
                    <option value="02" data-year="2020">February 2020</option>
                    <option value="03" data-year="2020">March 2020</option>
                    <option value="04" data-year="2020">April 2020</option>

          
                </select>

              <button onclick="compute();" class="btn btn-primary">Search</button>

              <?php
                if ($user->user_role == 'Super Admin') {
              ?>

                  <button  class="btn btn-primary match-height pull-right" id="compute" data-toggle="modal" data-target="#categorysalesforecast_modal" >Add Category Salesforecast 

              <?php  } ?>
               

        
              
                
            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="categorysalesforecast_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>
                            <tr>
                                <th>Brand</th>
                                <th>Month and Year</th>
                                <th>Category</th>
                                <th style="text-align: center;">Service Name</th>
                                <th>Percentage</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>

                        </tbody>

                        <tfoot>
                              <tr>
                                <td colspan="4" style="text-align: center; font-weight: bold;">Total</td>
                                <td class="total_percentage" style="text-align: center; font-weight: bold;"></td>
                                <td></td> 
                              </tr>
                        </tfoot>

                    </table>



                </div>



            </div>

        </div>



    </div>




<div class="modal fade" id="edit_modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
              <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit Percentage</h4>
      </div>
        <div class="modal-body">
        <div class="col-md-12">
          <form id="edit_form">
            <div id="edit_success">
            </div>
              <div style="margin-bottom: 10px;">
                <input type="" class="form-control" name="" id="service_name" disabled="disabled">
              </div>
              <div style="margin-bottom: 10px;">
                <input type="" class="form-control" name=""  id="service_percentage" >
              </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="edit_percentage();">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>






<div class="modal fade" id="categorysalesforecast_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">
              <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Add New Category Sales Forecast</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <form id="modal_form">
            <div id="submit_success">
            </div>
              <div style="margin-bottom: 10px;">
                <select id="modal_brand" name="brand" class="form-control select2" style="width: 100%; margin-bottom: 8px;">
                  <option value="" hidden selected>Select Brand</option>
                  <?php foreach($brands as $row) { ?>
                      <option value="<?php echo $row->brand_id; ?>"><?php echo $row->brand_name; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div style="margin-bottom: 10px;">
                <select id="modal_service" name="service" class="form-control select2" style="width: 100%;">
                  <option value="" hidden selected>Select Service</option> 
                </select>
              </div>
              <div style="margin-bottom: 10px;">
                <select id="modal_month_and_year" name="month_and_year" class="form-control select2" style="width: 100%; margin-bottom: 5px;">
                    <option value="" hidden selected>Select Month & Year</option>
                    <option value="01" data-year="2020">January 2020 </option>

                    <option value="02" data-year="2020">February 2020</option>
     
                    <option value="03" data-year="2020">March 2020</option>
               
                </select>
              </div>
              <div style="margin-bottom: 10px;">
                <input type="number" id="modal_percentage" name="percentage" class="form-control" placeholder="Percentage">
              </div>
          </form>
        </div>
      </div>
     

      <div class="modal-footer">
        <button id="btn_submit_survey" class="btn btn-success pull-right" onclick="submit_forecast()" style="margin-top: 5px; width: 100px; ">Submit</button>
      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>


    <input type="hidden" id="start">

    <input type="hidden" id="end">

    <input type="hidden" id="branch_name">

    <input type="hidden" id="base_url" value="<?= base_url(); ?>">

    <input type="hidden" id="budgetforecast_id">



    <!-- /.box-body -->



    </form>



</div>





















<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>



<!-- Bootstrap 3.3.7 -->





<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>





<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->


<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>



<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>


<!-- <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->



<script src="<?php echo base_url(); ?>assets/customs/js/budgetfolder/categorysalesforecast.js?v=1.0.2"></script>





</div>



<!-- /.content-wrapper -->

