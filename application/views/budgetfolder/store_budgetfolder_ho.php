<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        display: none;
    }

    .align-right{
        text-align: right !important;
    }

    .dt-buttons{
      text-align: right;
    }

    th{
        text-align: center !important;
        vertical-align: middle !important;
    }

    .modal {
    text-align: center;
    padding: 0!important;
    }

    .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
    }

    .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    }

    .dataTables_length{
        display: none;
    }

    button > a{
        color: white !important;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">





<div class="box box-primary">

    <div class="box-body">

        <div class="row">

            <div class="col-md-12">

                <select id="month_and_year" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

                    <option value="" hidden selected>Select Month & Year</option>

                    <!-- <option value="01" data-batch="1">January <?= date('Y')?> - Batch 1</option>

                    <option value="01" data-batch="2">January <?= date('Y')?> - Batch 2</option>

                    <option value="02" data-batch="1">February <?= date('Y')?> - Batch 1</option>

                    <option value="02" data-batch="2">February <?= date('Y')?> - Batch 2</option>

                    <option value="03" data-batch="1">March <?= date('Y')?> - Batch 1</option>

                    <option value="03" data-batch="2">March <?= date('Y')?> - Batch 2</option>

                    <option value="04" data-batch="1">April <?= date('Y')?> - Batch 1</option>

                    <option value="04" data-batch="2">April <?= date('Y')?> - Batch 2</option>

                    <option value="05" data-batch="1">May <?= date('Y')?> - Batch 1</option>

                    <option value="05" data-batch="2">May <?= date('Y')?> - Batch 2</option>

                    <option value="06" data-batch="1">June <?= date('Y')?> - Batch 1</option>

                    <option value="06" data-batch="2">June <?= date('Y')?> - Batch 2</option>

                    <option value="07" data-batch="1">July <?= date('Y')?> - Batch 1</option>

                    <option value="07" data-batch="2">July <?= date('Y')?> - Batch 2</option>

                    <option value="08" data-batch="1">August <?= date('Y')?> - Batch 1</option>

                    <option value="08" data-batch="2">August <?= date('Y')?> - Batch 2</option>

                    <option value="09" data-batch="1">September <?= date('Y')?> - Batch 1</option>

                    <option value="09" data-batch="2">September <?= date('Y')?> - Batch 2</option> -->

                    <!-- <option value="10" data-batch="1">October <?= date('Y')?> - Batch 1</option> -->

                    <!-- <option value="10" data-batch="2" data-year="2019">October 2019 - Batch 2</option> -->

                    <option value="11" data-batch="1" data-year="2019">November 2019 - Batch 1</option>

                    <option value="11" data-batch="2" data-year="2019">November 2019 - Batch 2</option>

                    <option value="12" data-year="2019">December 2019</option>
                    
                    <option value="01" data-batch="1" data-year="2020">January 2020 - Batch 1</option>
                    
                    <option value="01" data-batch="2" data-year="2020">January 2020 - Batch 2</option>
                    
                    <option value="02" data-batch="1" data-year="2020">February 2020 - Batch 1</option>
                    
                    <option value="02" data-batch="2" data-year="2020">February 2020 - Batch 2</option>

                    <option value="03" data-batch="1" data-year="2020">March 2020 - Batch 1</option>
                    
                    <option value="03" data-batch="2" data-year="2020">March 2020 - Batch 2</option>

                    <option value="04" data-batch="1" data-year="2020">April 2020 - Batch 1</option>
                    
                    <option value="04" data-batch="2" data-year="2020">April 2020 - Batch 2</option>



                </select>


                <select id="brands" class="form-control input-sm select2 match-height" style="display: inline-block !important; width: 15%;font-size: 15px;">

                <option value="" hidden selected>Select Brand</option>

                <?php

                foreach($brands as $brand){

                ?>

                <option value="<?= $brand->brand_id?>"><?= $brand->brand_name?></option>            

                <?php  

                }

                ?>

                </select>
                
                <button class="btn btn-primary btn-flat" onclick="check_brand_budgetfolder_availability()">Search</button>

            </div>

        </div>

        <hr style="margin-bottom: 0;">

        <div class="row" id="content">

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="checklist_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>

                                    <th rowspan="2">Budget Folder Month & Year</th>

                                    <th rowspan="2">Brand</th>

                                    <th rowspan="2">Branch <h5 id="no_branches"></h5> </th>

                                    <th colspan="4">Status</th>

                                    <th rowspan="2">Action</th>

                                </tr>

                                <tr>

                                    <th>Wall to Wall Inventory <h5 id="wall_to_wall_done"></h5></th>

                                    <th>Sales Forecast</th>

                                    <th>Order</th>

                                    <th>Budget Folder <h5 id="budgetfolder_ready"></h5></th>

                                </tr>

                            </thead>

                            <tbody>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="store_budgetfolder" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <button id="store_budgetfolder_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Store Budget Folder</h4>

      </div>

      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <label for="">Branch:</label>
                <input id="branch_name" type="text" class="form-control" disabled="disabled">
                <input id="branch_id" type="hidden">
            </div>
            <div class="col-md-6">
                <label for="">As of:</label>
                <input id="wall_to_wall_date" type="text" class="form-control" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h4 style="margin-top: 20px !important;">OTC:<p class="pull-right" id="otc_up"></p></h4>
                <hr>
                <h4>Consumables:<p class="pull-right" id="consumables_up"></h4>
                <hr>
                <h4>Grand Total:<p class="pull-right" id="total_up"></h4>
                <hr>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="budgetfolder_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th>Category</th>

                                <th>Material</th>

                                <th>Shade</th>
                                
                                <th>Color</th>

                                <th>Model</th>

                                <th>QTY</th>
                                
                                <th>UOM</th>
                                
                                <th>Cost</th>

                                <th>Total Cost</th>

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

            </div>
        </div>
      </div>
      <div class="modal-footer">

          <button onclick="confirm_budgetfolder_inventory()" class="btn btn-info">Confirm</button>

      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="submitted_budgetfolder" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <button id="store_budgetfolder_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Store Budget Folder</h4>

      </div>

      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <label for="">Branch:</label>
                <input id="submitted_branch_name" type="text" class="form-control" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label for="">Budget Date:</label>
                <input id="submitted_budget_date" type="text" class="form-control" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="">Company:</label>
                <input id="submitted_company" type="text" class="form-control" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label for="">Folder:</label>
                <input id="submitted_folder" type="text" class="form-control" disabled="disabled">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="">Original Target:</label>
                <input id="submitted_original_target" type="text" class="form-control" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label for="">Sales Forecast:</label>
                <input id="submitted_sales_forecast" type="text" class="form-control" disabled="disabled">
            </div>
        </div>
        <hr>
        <div class="row">

            <div class="col-md-12">

                <h4 class="display-4">Consumables</h4>

            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="consumables_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                          <tr>

                              <th rowspan="2">Material</th>

                              <th rowspan="2">Shade</th>

                              <th rowspan="2">Color</th>

                              <th rowspan="2">Model</th>

                              <th colspan="4">Beginning Inventory</th>
                              
                              <th colspan="3">Order</th>                                    

                          </tr>

                          <tr>

                              <th>Unit Price</th>

                              <th>QTY</th>

                              <th>UOM</th>

                              <th>Amount</th>

                              <th>QTY</th>

                              <th>UOM</th>

                              <th>Amount</th>

                          </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

            </div>
        </div>
        <hr>
        <div class="row">

            <div class="col-md-12">

                <h4 class="display-4">OTCs</h4>

            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="otcs_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                          <tr>

                            <th rowspan="2">Material</th>

                            <th rowspan="2">Shade</th>

                            <th rowspan="2">Color</th>

                            <th rowspan="2">Model</th>

                            <th colspan="4">Beginning Inventory</th>

                            <th colspan="3">Order</th>                                    

                          </tr>

                          <tr>

                            <th>Unit Price</th>

                            <th>QTY</th>

                            <th>UOM</th>

                            <th>Amount</th>

                            <th>QTY</th>

                            <th>UOM</th>

                            <th>Amount</th>

                          </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

            </div>
        </div>
      </div>
      <!-- <div class="modal-footer">

          <button onclick="confirm_budgetfolder_inventory()" class="btn btn-info">Confirm</button>

      </div> -->
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>

<div class="modal fade" id="categoryforecast" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog modal-lg">

    <div class="modal-content">

      <div class="modal-header">

        <button id="categoryforecast_close" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Category Sales Forecast</h4>

      </div>

      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <label for="forecast_branch">Branch:</label>
                <input id="forecast_branch" type="text" class="form-control" disabled="disabled">
            </div>
            <div class="col-md-6">
                <label for="forecast_salesforecast">Sales Forecast</label>
                <input id="forecast_salesforecast" type="text" class="form-control" disabled="disabled">
            </div>

        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">

                <div class="table-responsive">

                    <table id="forecast_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr>

                                <th style="text-align: left !important;">Category</th>

                                <th style="text-align: left !important;">Service</th>

                                <th>Price</th>
                                
                                <th>Percentage</th>

                                <th>QTY</th>

                                <th>Amount</th>
                                
 
                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

            </div>
        </div>
      </div>
      <div class="modal-footer">

          <button onclick="generate_order()" class="btn btn-info">Generate Order</button>

      </div>
    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>


<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content" style="background: none !important;">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
            <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
            <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
              </circle>
            </svg>
          </div>
          
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/plugins/tabledit/jquery.tabledit.js"></script>


<script src="<?= base_url()?>assets/customs/js/budgetfolder/store_budgetfolder_ho.js?v=1.0.42"></script>





