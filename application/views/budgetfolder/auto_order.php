<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">



<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">



    <!-- Font Awesome -->



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">



    <!-- Ionicons -->



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">



    <!-- Theme style -->



    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">







<style type="text/css">



    .dataTables_filter {



        float: left !important;



    }







    #code_color {



        width: 100px;



        border-radius: 40px/24px;



        outline: none;



    }







    #create_new_ticket {



        float: right;







    }



    .align-right{



        text-align:right; max-width:80px;



    }



    .align-left{



        text-align:left; max-width:80px;



    } 



    .align-center{



        text-align:center; max-width:80px;



    } 



    .gray-bg{

      background-color: #EEEBEB !important;

    }









</style>





<?php

  // echo "<pre>";

  // print_r($orders['items']['manual']);

  // echo "</pre>";



  $budgetmonth = date('F', strtotime($orders['storetarget_month'])).' '.date('Y', strtotime($orders['storetarget_month']));

  $otc_budget = $orders['otc_percentage'] * $orders['sales_forecast'] / 100;



  $cost_percentage_multiplier = .05;

  $projected_sales_multiplier = .94;

  $otc_multiplier = .06;



  if($orders['brand_id'] == 100005){

    $cost_percentage_multiplier = .08;

  }



  if($orders['brand_id'] == 100003){

    $cost_percentage_multiplier = .05;

    $projected_sales_multiplier = .96;

    $otc_multiplier = 0.04;

  }



  if($orders['brand_id'] == 100001){

    $cost_percentage_multiplier = .08;

    $projected_sales_multiplier = .95;

    $otc_multiplier = 0.05;

  }



  if($orders['brand_id'] == 100006 && $budgetmonth == 'February 2020'){

    $cost_percentage_multiplier = .08;

  }



  //materials

  $projected_sales = $orders['sales_forecast'] * $projected_sales_multiplier;

  $material_cost = $projected_sales * $cost_percentage_multiplier;



  //otcs

  $projected_sales_otc = $orders['sales_forecast'] * $otc_multiplier;

  $otc_cost = $projected_sales_otc * .60;



  //non consumable

  $non_consumable = ($orders['sales_forecast'] * 0.94) * 0.0025;





?>









<div class="box box-primary">







    <!-- /.box-header -->







    <!-- form start -->







    <div class="box-body">







        <div class="row">



            <div class="col-md-4">

                <label for="branch_name" style="margin-right: 1em;">Branch Name</label>

                <input type="text" id="branch_name" style="height: 2.4em; text-align: center; width: 15em;" disabled="disabled" value="<?= $orders['branch_name']; ?>">

            </div>

            <div class="col-md-4">

                <label for="sales_forecast" style="margin-right: 1em;">Sales Forecast</label>

                <input type="text" id="sales_forecast" style="height: 2.4em; text-align: center; width: 15em;" disabled="disabled" value="<?php echo number_format($orders['sales_forecast'], 2, '.', ','); ?>">

            </div>

            <div class="col-md-4">

                 <label for="budgetmonth" style="margin-right: 1em;">Budget Month and Year</label>

                <input type="text" id="budgetmonth" style="height: 2.4em; text-align: center; width: 15em;" disabled="disabled" value="<?= $budgetmonth; ?>">

            </div>



        </div>



        <hr>



        <div class="row">



            <div class="col-md-12">

              <h3>Auto Order</h3>

                <div class="table-responsive">



                    <table id="autoorder_table" class="table table-bordered table-hover" style="width: 100% !important;">



                        <thead>

                            <tr>

                              <th colspan="2"></th>

                              <th colspan="2" style="text-align: center;">Required Materials</th>

                              <th colspan="8"></th>



                            </tr>

                            <tr>

                                <th>Item Name</th>

                                <th style="text-align: center;">Forecast Services QTY</th>

                                <th style="text-align: right;">QTY</th>

                                <th style="text-align: left;">UOM</th>

                              <!--   <th style="text-align: center;">Total QTY</th> -->

                                <th style="text-align: center;">Item UOM</th>

                                <th style="text-align: center;">QTY Needed</th>

                                <th style="text-align: center;">CurrentInventory</th>

                                <th style="text-align: center; background-color: #f4f4f4;">Order QTY</th>

                                <th style="text-align: left;">UOM</th>

                                <th style="text-align: right;">Cost</th>
                                
                                <th style="text-align: right;  background-color: #f4f4f4;">TOTAL</th>

                                <th style="display: none;">Currentinventory</th>

                                <th style="display: none;">Supplier</th>


                            </tr>

                        </thead>



                        <tbody>

                           <?php $total_autoorder = 0; ?>

                           

                          <?php foreach ($orders['items']['auto'] as $row) { 



                            ?>

                              

                                <tr>

                                    <td><?= $row->item_name; ?></td>

                                    <td style="text-align: center;"><?= $row->forecast_qty; ?></td>

                                    <td style="text-align: right;"><?= number_format($row->order_quantity, 2, '.', ''); ?></td>

                                    <td><?= $row->servicemix_uom; ?></td>

                                    <td style="text-align: center;"><?= $row->item_uomsize; ?></td>

                                    <td style="text-align: center;"><?= number_format(ceil($row->order_quantity/$row->item_uomsize), 2, '.', ''); ?></td>

                                    <td style="text-align: center;"><?= number_format($row->current_quantity, 2, '.', ''); ?></td>

                                    <?php $autoorder = ceil($row->order_quantity/$row->item_uomsize) - $row->current_quantity;  ?>

                                    <td style="text-align: center; background-color: #f4f4f4;"><?= number_format(ceil($autoorder > 0 ? $autoorder: 0), 2, '.', ''); ?></td>

                                    <td style="text-align: left;"><?= $row->item_sapuom; ?></td>

                                    <td style="text-align: right;"><?= number_format($row->item_cost, 2, '.', ''); ?></td>

                                    <?php $amount = ceil($autoorder) * $row->item_cost; ?>

                                    <td style="text-align: right;  background-color: #f4f4f4;"><?= number_format($amount > 0 ? $amount : 0, 2, '.', ','); ?></td>

                                    <?php $total_autoorder = $total_autoorder + ($amount > 0 ? $amount : 0); ?>

                                    <td style="display: none;"><?= $row->currentinventory_id; ?></td>

                                    <td style="display: none;"><?= $row->item_supplier; ?></td>






                                </tr>    

                          <?php } ?>



                        </tbody>    



                        <tfoot>



                        </tfoot>



                    </table>



                        

                    </div>

                    

                    <h3>Manual Budget</h3>



                    <div class="table-responsive">

                        <table id="manualorder_table" class="table table-bordered table-hover" style="width: 100% !important;">

                          <thead>

                            <tr>

                              <th colspan="2"></th>

                              <th colspan="2" style="text-align: center;">Required Material</th>

                              <th colspan="8"></th>

                            </tr>

                            <th>Item Name</th>

                                <th style="text-align: center;">Forecast Services QTY</th>

                                <th style="text-align: right;">QTY</th>

                                <th style="text-align: left;">UOM</th>
                           
                                <th style="text-align: center;">Item UOM</th>

                                <th style="text-align: center;">QTY Needed</th>

                                <th style="text-align: center;">CurrentInventory</th>

                                <th style="text-align: center; background-color: #f4f4f4;">Order QTY</th>

                                <th style="text-align: left;">UOM</th>
                                
                                <th style="text-align: right;">Cost</th>

                                <th style="text-align: right;  background-color: #f4f4f4;">TOTAL</th>

                                <th style="display: none;">Currentinventory</th>

                            </tr>

                          </thead>

                          <tbody>

                           <?php $total_manualorder = 0; ?>

                           

                          <?php foreach ($orders['items']['manual'] as $row) { ?>

                                <tr>

                                    <td><?= $row->item_group; ?></td>

                                    <td style="text-align: center;"><?= $row->forecast_qty; ?></td>

                                    <td style="text-align: right;"><?= number_format($row->order_quantity, 2, '.', ''); ?></td>

                                    <td><?= $row->servicemix_uom; ?></td>


                                    <td style="text-align: center;"><?= $row->item_uomsize; ?></td>

                                    <td style="text-align: center;"><?= number_format(ceil($row->manual_quantity/$row->item_uomsize), 2, '.', ''); ?></td>

                                    <td style="text-align: center;"><?= number_format($row->current_quantity, 2, '.', ''); ?></td>
                                   
                                    <?php $manualorder = ceil($row->manual_quantity/$row->item_uomsize) - $row->current_quantity;  ?>


                                    <td style="text-align: center; background-color: #f4f4f4;"><?= number_format(ceil($manualorder > 0 ? $manualorder : 0), 2, '.', ''); ?></td>
                                    
                                    <td style="text-align: left;"><?= $row->item_sapuom; ?></td>

                                    <td style="text-align: right;"><?= number_format($row->item_cost, 2, '.', ''); ?></td>

                                    <?php $amount_manual = ceil($manualorder) * $row->item_cost; ?>

                                    <td style="text-align: right;  background-color: #f4f4f4;"><?= number_format($amount_manual > 0 ? $amount_manual : 0, 2, '.', ','); ?></td>

                                    <?php $total_manualorder = $total_manualorder + ($amount_manual > 0 ? $amount_manual : 0); ?>

                                    <td style="display: none;"><?= $row->currentinventory_id; ?></td>





                                </tr>    

                          <?php } ?>

                        </tbody>  





                        </table>

                    </div>



                    <div class="pull-right">

                        <h3>Total Auto Order : <input id="total_autoorder" style="text-align: right; margin-left: 1.8em; padding-right: 10px;" type="text" disabled="disabled" value="<?php echo number_format($total_autoorder, 2, '.', ','); ?>"></h3>

                        

                        <h3>OTC Budget : <input id="total_otcbudget" style="text-align: right; margin-left: 3.6em; padding-right: 10px;" type="text" disabled="disabled" value="<?php echo number_format($otc_budget, 2, '.', ','); ?>"></h3>

                        <h3>Total Manual Budget : <input id="total_manualbudget" style="text-align: right; padding-right: 10px;" type="text" disabled="disabled" value="<?php echo number_format($total_manualorder, 2, '.', ','); ?>"></h3>

                        <h3>Non Consumable : <input id="non_consumable" style="text-align: right; margin-left: 1.4em; padding-right: 10px;" type="text" disabled="disabled" value="<?php echo number_format($non_consumable, 2, '.', ','); ?>"></h3>



                        <button class="btn btn-primary btn-flat match-height pull-right" onclick="create_autoorder();" style="margin-top: 7em;">Create Order</button>

                     </div>



                </div>

    

            </div>



        </div>







    </div>



    <input type="hidden" id="start">



    <input type="hidden" id="end">



    <input type="hidden" id="branch_name">



    <input type="hidden" id="base_url" value="<?= base_url(); ?>">



    <input type="hidden" id="branch_id" value="<?= $orders['branch_id']; ?>">



    <input type="hidden" id="user_id" value="<?= $user->user_id; ?>">



    <input type="hidden" id="materials_beginning" value="<?= $orders['beginning_materials']; ?>">



    <input type="hidden" id="otc_beginning" value="<?= $orders['beginning_otc']; ?>">



    <input type="hidden" id="material_allowance" value="<?= $total_autoorder + $total_manualorder; ?>">



    <input type="hidden" id="otc_allowance" value="<?= $otc_budget; ?>">





    <!-- /.box-body -->



    <div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >

      <div class="modal-dialog">

        <div class="modal-content" style="background: none !important;">

          <div class="modal-body">

            <div class="row">

              <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">

                <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>

                <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>

                  </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>

                  </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>

                  </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>

                  </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>

                  </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>

                  </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>

                  </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>

                  </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>

                  </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>

                  </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>

                  </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>

                  </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>

                  </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>

                  </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">

                    <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>

                    <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>

                  </circle>

                </svg>

              </div>

              

            </div>

          </div>

        </div>

        <!-- /.modal-content -->

      </div>

      <!-- /.modal-dialog -->

    </div>







    </form>







</div>











































<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>







<!-- Bootstrap 3.3.7 -->











<script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>











<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>











<script src="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>







<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>







<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- Select2 -->





<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<!-- AdminLTE App -->



<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>



<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>



<script src="<?= base_url()?>assets/js/jszip.min.js"></script>



<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>



<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>



<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>



<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>







<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>





<!-- <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> -->







<script src="<?php echo base_url(); ?>assets/customs/js/budgetfolder/autoorder.js?v=1.0.7"></script>











</div>







<!-- /.content-wrapper -->



