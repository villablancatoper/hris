<style>


    .table-hover{

        cursor: pointer;

    }



    @media (min-width: 992px){

        #customer_table_wrapper{

            border-right: 1px solid #eee; 

        }

    }



    .inputContainer {

        width: 100%;

        margin-bottom: 7px;

    }

    .inputContainer label {

        float: left;

        margin-right: 5px;

        margin-top: 3px;

        width: 100px;

    }

    .inputContainer div {

        overflow: hidden;

    }

    .inputContainer input {

        width: 100%;

        -moz-box-sizing: border-box;

        -webkit-box-sizing: border-box;

        box-sizing: border-box;

        display: block

    }

    #fov{

        font-size: 12px;

        padding-top: 4px;

        font-weight: bold;

    }

    div.dataTables_wrapper div.dataTables_filter {
        display: none;
    }

    .align-right{
        text-align: right !important;
    }

    .dt-buttons{
      text-align: right;
    }

    th{
        text-align: center !important;
        vertical-align: middle !important;
    }

    .modal {
    text-align: center;
    padding: 0!important;
    }

    .modal:before {
    content: '';
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    margin-right: -4px;
    }

    .modal-dialog {
    display: inline-block;
    text-align: left;
    vertical-align: middle;
    }

    .dataTables_length{
        display: none;
    }

    button > a{
        color: white !important;
    }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">



<link rel="stylesheet" href="<?= base_url()?>assets/plugins/iCheck/all.css">



<!-- Theme style -->

<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">

<?php
  
  // print_r($budgetfolder_data_manual['items']['manual']);
  // print_r($budgetfolder_data);

  $cost_percentage_multiplier = .05;
  $projected_sales_multiplier = .94;
  $otc_multiplier = .06;

  if($branch->brand_id == 100005){
    $cost_percentage_multiplier = .08;
  }

  if($branch->brand_id == 100003){
    $cost_percentage_multiplier = .05;
    $projected_sales_multiplier = .96;
    $otc_multiplier = 0.04;
  }

  if($branch->brand_id == 100001){
    $cost_percentage_multiplier = .08;
    $projected_sales_multiplier = .95;
    $otc_multiplier = 0.05;
  }

  if($branch->brand_id == 100006 && $budget_date == 'February 2020'){
    $cost_percentage_multiplier = .08;
  }

  $total_allocated = $budgetfolder_data->budgetfolder_manualbudget + $budgetfolder_data->budgetfolder_nonconsumable;

?>



<div class="box box-primary">

    <div class="box-body">

        <div class="row">

            <div class="col-md-6">

                <h3 class="display-4 text-center">
                  STORE: <span id="branch_name" style="color: blue;"><?= $branch->branch_name?></span>
                </h3>

            </div>

            <div class="col-md-6">

                <h3 class="display-4 text-center">
                  BUDGET DATE: <span style="color: blue;"><?= $budget_date?></span>
                </h3>
                <input type="hidden" id="budget_date" value="<?= $budget_date?>">
                <input type="hidden" id="user_id" value="<?= $this->session->userdata('user')->user_id?>">

            </div>

        </div>

        <div class="row">

            <div class="col-md-6">

                <h3 class="display-4 text-center">
                  ORIGINAL TARGET: <span style="color: blue;"><?= $store_target->storetarget_original?></span>
                </h3>

            </div>

            <div class="col-md-6">

                <h3 class="display-4 text-center">
                  SALES FORECAST: <span style="color: blue;"><?= $store_target->storetarget_updatedsalesforecast?></span>
                </h3>
                <input id="branch_id" type="hidden" value="<?= $branch->branch_id?>">
                <input id="brand_id" type="hidden" value="<?= $branch->brand_id?>">

            </div>

        </div>

        <hr>


        <div class="row">
            <div class="col-md-12">
                 
                  <h4 class="display-4">MANUAL ORDERING GUIDE</h4>

            </div>

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">
                      <div class="col-md-12">

                        <div class="table-responsive">

                          <table id="guide_table" class="table table-bordered table-hover" style="width: 100% !important;">

                              <thead>

                                  <tr>

                                      <th >Material</th>

                                      <th >Quantity</th>

                                      <th >CurrentInventory</th>

                                      <th >Suggested Quantity</th>

                                      <th style="text-align: right;">Cost</th>

                                      <th style="text-align: left;">UOM</th>                                    

                                      <th >Amount</th>                                    

                                  </tr>


                              </thead>

                              <tbody>

                                  <?php 
                                    

                                      foreach($budgetfolder_data_manual['items']['manual'] as $manual){
                                        
                                  ?>

                                  <tr>
                                      <td><?= $manual->item_group?></td>
                                      <td style="text-align: center;"><?= number_format(ceil($manual->manual_quantity/$manual->item_uomsize), 2, '.', ''); ?></td>
                                      <td style="text-align: center;"><?= number_format($manual->current_quantity, 2, '.', ''); ?></td>
                                      <?php $suggested_qty = ceil($manual->manual_quantity/$manual->item_uomsize) - $manual->current_quantity;  ?>
                                      <td style="text-align: center;"><?= number_format($suggested_qty > 0 ? $suggested_qty : 0, 2, '.', ''); ?></td>
                                      <td style="text-align: right;"><?= $manual->item_cost?></td>
                                      <td style="text-align: left;"><?= $manual->item_sapuom?></td>
                                      <?php $amount_manual = $suggested_qty * $manual->item_cost; ?>
                                      <td style="text-align: center;"><?= number_format($amount_manual > 0 ? $amount_manual : 0, 2, '.', ',')?></td>
                                  </tr>


                                <?php } ?>


 
                              </tbody>

                          </table>

                        </div>

                      </div>
                   

                        <div class="col-md-12">

                            <h4 class="display-4">CONSUMABLES</h4>

                            <input type="hidden" name="sales_forecast" id="sales_forecast" value="<?= $store_target->storetarget_updatedsalesforecast?>">

                        </div>

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="consumables_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>

                                    <th rowspan="2">Material</th>

                                    <th rowspan="2">Shade</th>

                                    <th rowspan="2">Color</th>

                                    <th rowspan="2">Model</th>

                                    <th colspan="4">Beginning Inventory</th>
                                    
                                    <th colspan="3">Order</th>                                    

                                </tr>

                                <tr>

                                    <th>Unit Price</th>

                                    <th>QTY</th>

                                    <th>UOM</th>

                                    <th>Amount</th>

                                    <th>QTY</th>

                                    <th>UOM</th>

                                    <th>Amount</th>
                               


                                </tr>

                            </thead>

                            <tbody>

                                <?php 
                                    $current_quantity_total = 0;

                                    foreach($consumables as $consumable){
                                      
                                      $current_quantity = $consumable->current_quantity < 0 ? 0 : $consumable->current_quantity;
                                ?>

                                <tr>
                                    <td><?= $consumable->item_description?></td>
                                    <td><?= $consumable->item_shade?></td>
                                    <td><?= $consumable->item_color?></td>
                                    <td><?= $consumable->item_model?></td>
                                    <td><?= $consumable->item_cost?></td>

                                    <!-- <td><input id="current_qty[<?= $consumable->currentinventory_id?>]" type="text" class="form-control align-right" data-item-cost=""></td> -->

                                    <td><?= $current_quantity?></td>

                                    <td><?= $consumable->item_sapuom?></td>

                                    <td><input class="consumables-total-amount-<?= $consumable->currentinventory_id?> form-control align-right" type="text" value="<?= round($current_quantity * $consumable->item_cost, 2)?>" disabled="disabled"></td>

                                    <td><input id="consumables_order_qty[<?= $consumable->currentinventory_id?>]" class="form-control align-right" type="text" data-item-cost="<?= $consumable->item_cost?>" value="" <?= $consumable->item_ordermodule == 0 ? 'disabled="disabled"' : ''?> data-currentinventory-id="<?= $consumable->currentinventory_id?>" data-item-supplier="<?= $consumable->item_supplier?>" data-item-cost="<?= $consumable->item_cost?>"></td>

                                    <td><?= $consumable->item_sapuom?></td>

                                    <td><input class="consumables-order-amount-<?= $consumable->currentinventory_id?> form-control align-right" type="text" value="" disabled="disabled"></td>

                                </tr>

                                <?php 

                                    $current_quantity_total = $current_quantity_total + (round($current_quantity * $consumable->item_cost, 2));
                                }?>

                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>

                                  <td colspan="3" style="text-align: left !important;">
                                    <label for="">Total Ordered Amount</label>
                                    <input id="consumables_order_qty_total" class="form-control align-right" type="text" value="0.00" disabled="disabled">
                                  </td>
                                </tr>

                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td colspan="3">
                                    <label for="">Manual Order Budget</label>
                                    <input class="form-control align-right" type="text" value="<?= $budgetfolder_data->budgetfolder_manualbudget?>" disabled="disabled">
                                  </td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td colspan="3">
                                    <label for="">Non-Consumables Budget</label>
                                    <input class="form-control align-right" type="text" value="<?= $budgetfolder_data->budgetfolder_nonconsumable?>" disabled="disabled">
                                  </td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td colspan="3">
                                    <label for="">Total Allocated Budget</label>
                                    <input id="consumables_maximum_allowance" class="form-control align-right" type="text" value="<?= number_format($total_allocated, 2, '.', '')?>" disabled="disabled">
                                  </td>
                                </tr>
                             
                            </tbody>

                        </table>

                      </div>

                    </div>

                </div>

            </div>

        </div>

        <hr>

        <div class="row">

            <div class="col-md-12">

                <h4 class="display-4">OTCs</h4>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="row" style="margin-top: 20px;">

                    <div class="col-md-12">

                      <div class="table-responsive">

                        <table id="otcs_table" class="table table-bordered table-hover" style="width: 100% !important;">

                            <thead>

                                <tr>

                                    <th rowspan="2">Material</th>

                                    <th rowspan="2">Shade</th>

                                    <th rowspan="2">Color</th>

                                    <th rowspan="2">Model</th>

                                    <th colspan="4">Beginning Inventory</th>
                                    
                                    <th colspan="3">Order</th>                                    

                                </tr>

                                <tr>

                                    <th>Unit Price</th>

                                    <th>QTY</th>

                                    <th>UOM</th>

                                    <th>Amount</th>

                                    <th>QTY</th>

                                    <th>UOM</th>

                                    <th>Amount</th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php 
                                    $current_quantity_total = 0;

                                    foreach($otcs as $otc){
                                      
                                      $current_quantity = $otc->current_quantity < 0 ? 0 : $otc->current_quantity;
                                ?>

                                <tr>
                                    <td><?= $otc->item_description?></td>
                                    <td><?= $otc->item_shade?></td>
                                    <td><?= $otc->item_color?></td>
                                    <td><?= $otc->item_model?></td>
                                    <td><?= $otc->item_cost?></td>

                                    <!-- <td><input id="current_qty[<?= $otc->currentinventory_id?>]" type="text" class="form-control align-right" data-item-cost=""></td> -->

                                    <td><?= $current_quantity?></td>

                                    <td><?= $otc->item_sapuom?></td>

                                    <td><input class="otcs-total-amount-<?= $otc->currentinventory_id?> form-control align-right" type="text" value="<?= round($current_quantity * $otc->item_cost, 2)?>" disabled="disabled"></td>

                                    <td><input id="otcs_order_qty[<?= $otc->currentinventory_id?>]" class="form-control align-right" type="text" data-item-cost="<?= $otc->item_cost?>" value="" <?= $otc->item_ordermodule == 0 ? 'disabled="disabled"' : ''?>></td>

                                    <td><?= $otc->item_sapuom?></td>

                                    <td><input class="otcs-order-amount-<?= $otc->currentinventory_id?> form-control align-right" type="text" value="" disabled="disabled"></td>

                                </tr>

                                <?php 

                                    $current_quantity_total = $current_quantity_total + (round($current_quantity * $otc->item_cost, 2));
                                }?>

                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
   
                                  <td colspan="4" style="text-align: left !important;">
                                    <label for="">Total Ordered Amount</label>
                                    <input id="otcs_order_qty_total" class="form-control align-right" type="text" value="0.00" disabled="disabled">
                                  </td>
                                </tr>

                                <tr>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td colspan="4">
                                    <label for="">OTC Budget</label>
                                    <input id="otcs_maximum_allowance" class="form-control align-right" type="text" value="<?= $budgetfolder_data->budgetfolder_otcbudget?>" disabled="disabled">
                                  </td>
                                </tr>


                            </tbody>

                        </table>

                      </div>

                    </div>

                </div>

            </div>

        </div>

        <hr>

        <div class="row">

          <div class="col-md-12">
            
            <button class="btn btn-primary pull-right" onclick="create_budgetfolder()">Submit</button>

          </div>

        </div>

    </div>

</div>

    <input type="hidden" id="materials_beginning" value="<?= $budgetfolder_data->budgetfolder_materialbeginningtotal; ?>">

    <input type="hidden" id="otc_beginning" value="<?= $budgetfolder_data->budgetfolder_otcbeginningtotal; ?>">

    <input type="hidden" id="material_allowance" value="<?= $budgetfolder_data->budgetfolder_materialmaxallowance; ?>">

    <input type="hidden" id="otc_allowance" value="<?= $budgetfolder_data->budgetfolder_otcmaxallowance; ?>">

    <input type="hidden" id="auto_order" value="<?= $budgetfolder_data->budgetfolder_autoorder; ?>">


<div class="modal fade centered-modal" data-backdrop="static" id="loading_modal" data-keyboard="false" >
  <div class="modal-dialog">
    <div class="modal-content" style="background: none !important;">
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 text-center" style="padding-bottom: 100px !important;">
            <h3 class="display-5" style="color: white !important; margin: 0; padding: 0;">&nbsp;Loading...</h3>
            <svg class="lds-default" width="25%" height="25%" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="75" cy="50" fill="undefined" r="3.81357" style="padding-bottom: 100px !important;">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.56s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.56s"></animate>
              </circle><circle cx="72.839" cy="60.168" fill="undefined" r="4.34691">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.52s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.52s"></animate>
              </circle><circle cx="66.728" cy="68.579" fill="undefined" r="4.88024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.48s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.48s"></animate>
              </circle><circle cx="57.725" cy="73.776" fill="undefined" r="4.58643">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.44s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.44s"></animate>
              </circle><circle cx="47.387" cy="74.863" fill="undefined" r="4.05309">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.4s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.4s"></animate>
              </circle><circle cx="37.5" cy="71.651" fill="undefined" r="3.51976">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.36s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.36s"></animate>
              </circle><circle cx="29.775" cy="64.695" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.32s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.32s"></animate>
              </circle><circle cx="25.546" cy="55.198" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.28s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.28s"></animate>
              </circle><circle cx="25.546" cy="44.802" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.24s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.24s"></animate>
              </circle><circle cx="29.775" cy="35.305" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.2s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.2s"></animate>
              </circle><circle cx="37.5" cy="28.349" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.16s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.16s"></animate>
              </circle><circle cx="47.387" cy="25.137" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.12s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.12s"></animate>
              </circle><circle cx="57.725" cy="26.224" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.08s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.08s"></animate>
              </circle><circle cx="66.728" cy="31.421" fill="undefined" r="3">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="-0.04s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="-0.04s"></animate>
              </circle><circle cx="72.839" cy="39.832" fill="undefined" r="3.28024">
                <animate attributeName="r" values="3;3;5;3;3" times="0;0.1;0.2;0.3;1" dur="0.6s" repeatCount="indefinite" begin="0s"></animate>
                <animate attributeName="fill" values="#f6eddc;#f6eddc;#f08d43;#f6eddc;#f6eddc" repeatCount="indefinite" times="0;0.1;0.2;0.3;1" dur="0.6s" begin="0s"></animate>
              </circle>
            </svg>
          </div>
          
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>



<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>



<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script src="<?= base_url()?>assets/plugins/tabledit/jquery.tabledit.js"></script>


<script src="<?= base_url()?>assets/customs/js/budgetfolder/create_budgetfolder.js?v=1.0.8"></script>





