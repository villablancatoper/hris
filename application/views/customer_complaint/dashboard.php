<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
<style type="text/css">
    .dataTables_filter {
        float: left !important;
    }

    #code_color {
        width: 100px;
        border-radius: 40px/24px;
        outline: none;
    }

    #create_new_ticket {
        float: right;

    }
    .left{
        margin-left: 53px;
    }
</style>
<!-- <?php


$department_id = $user->department_id;
// print_r($branches);
?>
 -->
<?php
$user = $this->session->userdata('user');
$department = $user->user_department;
$user_role = $user->user_role;
$user_id = $user->user_id;
$user_position = $user->user_position;
$branch_id = $user->branch_id;
$branch_name = $user->branch_name;

// if ($department == 'FACILITIES') {
//     $category = $facilities_category;
// }elseif ($user_position == 'Area Manager') {
//     $category = array_merge($facilities_category, $mis_category);
// }elseif ($department == 'IT'){
//     $category = $mis_category;
// }elseif ($branch_id == 198) {
//     $category = array_merge($facilities_category, $mis_category);
// }elseif ($user_position == 'Standard User') {
//     $category = array_merge($facilities_category, $mis_category);
// }

// print_r($branches);

?>
<div class="box box-primary">

    <!-- /.box-header -->

    <!-- form start -->

    <div class="box-body">

        <button type="button" class="btn btn-light" value="All" onclick="sort_all(value);" id="code_color">All</button>
        <button type="button" class="btn btn-primary" value="New" onclick="sort_all(value);" id="code_color">New</button>
        <button type="button" class="btn btn-warning" value="In-progress" onclick="sort_all(value);" id="code_color">In-progress</button>
        <button type="button" class="btn btn-success" value="Completed" onclick="sort_all(value);" id="code_color">Completed</button>
        
        <button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#modal_new_complaint" id="create_new_ticket">New Customer Complaint</button>
      
        <hr style="margin-bottom: 10px;">
        
<!--         <div class="form-group form-inline" style = "margin-left: -50px !important;">
            <div class="left">
                <label for="filter_dept">Department</label>
                <select id="filter_dept" class="form-control select2" style="width: 170px;">
                    <option value="" hidden selected>Select Department</option>
                    <option value="IT">IT</option>
                    <option value="Facilities">Facilities</option>
                </select>
            </div>
         </div> -->
        <div class="row">
        
            <div class="col-md-12 col-sm-12 col-lg-12" style="padding-top: 20px;" id="">
                <div class="table-responsive">


                    <table id="complaint_table" class="table table-bordered table-hover" style="width: 100% !important;">

                        <thead>

                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">

                                <th>Concern ID</th>

                                <th>Date and Time Created</th>

                                <th>Dept/Site/Store</th>

                                <th>Type of Concern</th>

                                <th>Details of Concern</th>

                                <th>Status</th>

                                <th>Aging</th>

                                <th>Latest Update Date</th>

                                <th>Latest Update Details</th>

                                <th>Action</th>

                            </tr>

                        </thead>

                        <tbody style="font-size: 12px;">

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

    <!-- /.box-body -->

    </form>

</div>



<!-- <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">

                <select id="department" class="form-control select2" style="width: 170px;">
                    <option value="" hidden selected>Select Department</option>
                    <option value="IT">IT</option>
                    <option value="Facilities">Facilities</option>
                </select>
                <select id="month" class="form-control select2" style="width: 170px;">
                    <option value="" hidden selected>Select Month</option>
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05">May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <select id="year" class="form-control select2" style="width: 170px;">
                    <option value="" hidden selected>Select Year</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                </select>
                <button type="button" id="ticket_submit" onclick="search_overview();" class="btn btn-primary">Search</button>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Ticket Overview</h3>
                <div style="display: inline-block;">
                    <h4 style="display: inline-block;">Department: </h4>
                    <div style="font-weight: bold; font-size: 20px; display: inline-block;" id="dept"></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="overview_table" class="table table-bordered table-hover" style="width: 100% !important;">
                        <thead>
                            <tr style="font-size: 12px; font-weight: bolder; text-align: center;">
                                <th rowspan="2" style="text-align: center;">Type of Concern</th>
                                <th rowspan="2" style="text-align: center;">No. Of Concern</th>
                                <th rowspan="2" style="text-align: center;">Completed</th>
                                <th colspan="5" style="text-align: center;">Pending</th>
                                <th rowspan="2" style="text-align: center;">Percentage</th>
                            </tr>
                            <tr>
                                <th style="width: 80px; text-align: center;">(1-15)</th>
                                <th style="width: 80px; text-align: center;">(16-30)</th>
                                <th style="width: 80px; text-align: center;">(31-60)</th>
                                <th style="width: 80px; text-align: center;">(61-90)</th>
                                <th style="width: 80px; text-align: center;">( > 90)</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px; text-align: center;">
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="text-align: center;">Total</th>
                                <th style="text-align: center;" id="total_concern"></th>
                                <th style="text-align: center;" id="total_completed"></th>
                                <th style="text-align: center;" id="total_cond1"></th>
                                <th style="text-align: center;" id="total_cond2"></th>
                                <th style="text-align: center;" id="total_cond3"></th>
                                <th style="text-align: center;" id="total_cond4"></th>
                                <th style="text-align: center;" id="total_cond5"></th>
                                <th style="text-align: center;" id="total_percentage"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div> -->

<div class="modal fade" id="modal_new_complaint" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Customer Complaint</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_create_ticket">
                    <div class="form-group" style="margin-left: -15px;">
                        <label for="site_store" class="col-sm-12 control-label">Site and Store:</label>
                    </div>

                    <div class="form-group" style="margin-left: -15px;">
                        <div class="col-sm-12">
                          
                <?php if($user_position == 'Standard User') { ?>
                    
                    <select id="site_store" class="form-control select2" disabled style="width: 570px;">
                        <option value="<?= $branch_name ?>"><?=  $branch_name; ?></option>
                    </select>

                <?php } else { ?>
                            <select id="site_store" class="form-control select2" style="width: 570px;">

                                <option value="" hidden selected>*Select Branch</option>
                                <?php
                    foreach($branches as $branch){
                  ?>
                                <option value="<?= $branch->branch_name?>"><?= $branch->branch_name?></option>

                                <?php
                    }
                  ?>
                            
                            </select>
                    <?php   } ?>  
                        </div>
                    </div>

                    <div class="form-group" style="margin-left: -15px;">
                        <label for="concern_type" class="col-sm-12 control-label">Category of Concern:</label>
                    </div>

                    <div class="form-group" style="margin-left: -15px;">
                        <div class="col-sm-12">
                            <select id="concern_type" class="form-control select2" style="width: 570px;">
                                <option value="" hidden selected>*Category of Concern</option>
                            <?php   foreach ($category as $row) { ?>
                                <option value="<?= $row->category?>"><?= $row->category?></option>
                           <?php     }   ?>    
                         
                            </select>
                         
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="concern_details" class="col-form-label">Details of Concern:</label>
                        <textarea id="concern_details" placeholder="Tell us more about the concern" class="form-control"></textarea>
                    </div>
                </form>
                <div style="font-size: 10px; color: green;"> Fields with * mark are required</div>
            </div>

            <div id="message"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="ticket_submit" onclick="ticket_submit();" class="btn btn-primary">Submit Ticket</button>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/moment/min/moment.min.js"></script>

<script src="<?= base_url()?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="<?= base_url()?>assets/js/dataTables.buttons.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.flash.min.js"></script>

<script src="<?= base_url()?>assets/js/jszip.min.js"></script>

<script src="<?= base_url()?>assets/js/pdfmake.min.js"></script>

<script src="<?= base_url()?>assets/js/vfs_fonts.js"></script>

<script src="<?= base_url()?>assets/js/buttons.html5.min.js"></script>

<script src="<?= base_url()?>assets/js/buttons.print.min.js"></script>


<script src="<?php echo base_url(); ?>assets/customs/js/customer_complaint.js"></script>





</div>

<!-- /.content-wrapper -->
