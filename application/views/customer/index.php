<style>

  .dataTables_filter {

    display: none;

  }

  .table-hover{

    cursor: pointer;

  }



  @media (min-width: 992px){

      #customer_table_new_wrapper{

          border-right: 1px solid #eee; 

      }

  }

  th{

    vertical-align: middle !important;

  }

</style>



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">



<!-- Select2 -->

<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/select2/dist/css/select2.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">



<link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">



<div class="box box-primary">

  <!-- /.box-header -->

  <!-- form start -->

    <div class="box-body">

      <div class="row">

        <div class="col-md-12">

            <strong>Search: </strong><label><input id="search_box" type="search" class="form-control input-sm" placeholder="Customer Name" style="height: 34px !important;"></label><button class="btn btn-primary btn-flat" onclick="search_customer()">Search</button>

            <input id="data_recipient" type="hidden">
            <?php 
              if($user->brand_id != 100007 || $user->user_position == 'Super Admin'){
            ?>
            <a class="btn btn-primary pull-right" href="javascript: test()" >New Customer</a>
            <?php }?>
        </div>

      </div>

      <hr style="margin-bottom: 0;">

      <div class="row">

        <div class="col-md-5"style="padding-top: 20px;" id="customer_table_new_wrapper">

          <div class="table-responsive">

            <table id="customers_table_new" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID No.</th>

                <th>First Name</th>

                <th>Last Name</th>

                <th>Email Address</th>

                <th>Action</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

        <div class="col-md-7" style="padding-top: 20px;">

          <div class="table-responsive">

            <table id="customers_table_old" class="table table-bordered table-hover" style="width: 100% !important;">

              <thead>

              <tr>

                <th>ID No.</th>
                
                <th>Branch</th>

                <th>First Name</th>

                <th>Last Name</th>

                <th>Contact No</th>

                <th>Sex</th>

                <th>Age Range</th>

                <th>Location</th>

                <th>Action</th>

              </tr>

              </thead>

              <tbody>

              </tbody>

            </table>

          </div>

        </div>

      </div>

    </div>

    <!-- /.box-body -->

  </form>

</div>



<div class="modal fade" id="customer_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="customer_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Customer Info</h4>

      </div>

      <div class="modal-body">

        <div id="customer_alert_success" class="alert alert-success alert-dismissible">

          <h4><i class="icon fa fa-check"></i> Success!</h4>

          You have successfully <span class="label label-info" id="function" style="font-size: 12px; margin-left: 5px;"></span> Customer ID: <span id="function_id" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>

        </div>

        <form id="customer_form" method="POST">

          <div id="customer_error"></div>

          <div class="row">

            <div class="col-md-6">

              <label for="customer_id">Customer ID</label>

              <input id="customer_id" name="customer_id" class="form-control" type="text" disabled="disabled" >

              <label for="customer_firstname">Firstname</label>

              <input id="customer_firstname" name="customer_firstname" class="form-control" type="text" disabled="disabled">

              <label for="customer_lastname">Lastname</label>

              <input id="customer_lastname" name="customer_lastname" class="form-control" type="text" disabled="disabled">

              <label for="customer_gender">Gender</label>

              <select id="customer_gender" name="customer_gender" class="form-control" disabled="disabled"></select>

              <label for="customer_phoneno">Phone No.</label>

              <input id="customer_phoneno" name="customer_phoneno" class="form-control" type="text" disabled="disabled">

              <label for="customer_email">Email Address</label>

              <input id="customer_email" name="customer_email" class="form-control" type="text" disabled="disabled">

            </div>

            <div class="col-md-6">

              <label for="customer_mobileno">Mobile No.</label>

              <input name="customer_mobileno" id="customer_mobileno" class="form-control" type="number" disabled="disabled">

              <label for="location_id">Location</label>

              <select id="location_id" name="location_id" class="form-control select2" type="text" disabled="disabled" style="width: 100%;">

              <?php

                foreach($locations as $location){

              ?>

              <option value="<?= $location->location_id?>"><?= $location->location_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="occupation_id">Occupation</label>

              <select id="occupation_id" name="occupation_id" class="form-control select2" type="text" disabled="disabled" style="width: 100%;">

              <?php

                foreach($occupations as $occupation){

              ?>

              <option value="<?= $occupation->occupation_id?>"><?= $occupation->occupation_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="customer_birthday">Birthday</label>

              <input id="customer_birthday" name="customer_birthday" class="form-control" type="text" disabled="disabled">

              <label for="age_id">Age Range</label>

              <select id="age_id" name="age_id" class="form-control select2" type="text" disabled="disabled" style="width: 100%;">

              <?php

                foreach($ages as $age){

              ?>

              <option value="<?= $age->age_id?>"><?= $age->age_range?></option>

              <?php

                }

              ?>

              </select>

              <label for="customer_foottraffic">Foottraffic</label>

              <input id="customer_foottraffic" name="customer_foottraffic" class="form-control" type="text" disabled="disabled">

            </div>

          </div>

        </form>

      </div>

      <div class="modal-footer">

        <button id="edit_customer_button" type="button" onclick="enable_editing()" class="btn btn-warning pull-left">Edit

        </button>

        <button id="update_customer_button" type="button" onclick="update_customer()" class="btn btn-success pull-left">Update

        </button>

        <button id="delete_customer_button" type="button" onclick="delete_customer()" class="btn btn-danger pull-left">Delete

        </button>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade" id="add_customer_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="add_customer_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Customer Info</h4>

      </div>

      <div class="modal-body">

        <div id="add_customer_alert_success" class="alert alert-success alert-dismissible">

          <h4><i class="icon fa fa-check"></i> Success!</h4>

          You have successfully <span class="label label-info" id="add_customer_function" style="font-size: 12px; margin-left: 5px;"></span>  <span id="add_customer_function_name" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>

        </div>

        <form id="add_customer_form" method="POST">

          <div id="add_customer_error"></div>

          <div class="row">

            <div class="col-md-6">

              <label for="add_customer_firstname">Firstname</label>

              <input id="add_customer_firstname" name="add_customer_firstname" class="form-control" type="text">

              <label for="add_customer_lastname">Lastname</label>

              <input id="add_customer_lastname" name="add_customer_lastname" class="form-control" type="text">

              <label for="add_customer_gender">Gender</label>

              <select id="add_customer_gender" name="add_customer_gender" class="form-control">

                <option value="">Select gender</option>

                <option value="Male">Male</option>

                <option value="Female">Female</option>

              </select>

              <label for="add_customer_phoneno">Phone No.</label>

              <input id="add_customer_phoneno" name="add_customer_phoneno" class="form-control" type="number">

              <label for="add_customer_email">Email Address</label>

              <input id="add_customer_email" name="add_customer_email" class="form-control" type="text">

            </div>

            <div class="col-md-6">

              <label for="add_customer_mobileno">Mobile No.</label>

              <input name="add_customer_mobileno" id="add_customer_mobileno" class="form-control" type="number">

              <label for="add_customer_location_id">Location</label>

              <select id="add_customer_location_id" name="add_customer_location_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select location</option>

              <?php

                foreach($locations as $location){

              ?>

              <option value="<?= $location->location_id?>"><?= $location->location_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="add_customer_occupation_id">Occupation</label>

              <select id="add_customer_occupation_id" name="add_customer_occupation_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select occupation</option>

              <?php

                foreach($occupations as $occupation){

              ?>

              <option value="<?= $occupation->occupation_id?>"><?= $occupation->occupation_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="add_customer_customer_birthday">Birthday</label>

              <input id="add_customer_customer_birthday" name="add_customer_customer_birthday" class="form-control" type="text">

              <label for="add_customer_age_id">Age Range</label>

              <select id="add_customer_age_id" name="add_customer_age_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select age range</option>

              <?php

                foreach($ages as $age){

              ?>

              <option value="<?= $age->age_id?>"><?= $age->age_range?></option>

              <?php

                }

              ?>

              </select>

            </div>

          </div>

        </form>

      </div>

      <div class="modal-footer">

        <button id="add_customer_button" type="button" onclick="add_customer()" class="btn btn-info">Add

        </button>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>



<div class="modal fade" id="transfer_customer_modal" data-backdrop="static" data-keyboard="false">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <button id="transfer_customer_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">

        <span aria-hidden="true">×</span></button>

        <h4 class="modal-title">Transfer Info</h4>

      </div>

      <div class="modal-body">

        <div id="transfer_customer_alert_success" class="alert alert-success alert-dismissible">

          <h4><i class="icon fa fa-check"></i> Success!</h4>

          You have successfully <span class="label label-info" id="transfer_customer_function" style="font-size: 12px; margin-left: 5px;"></span>  <span id="transfer_customer_function_name" class="label label-info" style="font-size: 12px; margin-left: 5px;"></span>

        </div>

        <form id="transfer_customer_form" method="POST">

          <div id="transfer_customer_error"></div>

          <div class="row">

            <div class="col-md-6">

              <input type="hidden" id="transfer_customer_customerolddata_id">

              <label for="transfer_customer_firstname">Firstname</label>

              <input id="transfer_customer_firstname" name="transfer_customer_firstname" class="form-control" type="text">

              <label for="transfer_customer_lastname">Lastname</label>

              <input id="transfer_customer_lastname" name="transfer_customer_lastname" class="form-control" type="text">

              <label for="transfer_customer_gender">Gender</label>

              <select id="transfer_customer_gender" name="transfer_customer_gender" class="form-control">

                <option value="">Select gender</option>

                <option value="Male">Male</option>

                <option value="Female">Female</option>

              </select>

              <label for="transfer_customer_phoneno">Phone no</label>

              <input id="transfer_customer_phoneno" name="transfer_customer_phoneno" class="form-control" type="number">

              <label for="transfer_customer_email">Email Address</label>

              <input id="transfer_customer_email" name="transfer_customer_email" class="form-control" type="text">

            </div>

            <div class="col-md-6">

              <label for="transfer_customer_mobileno">Mobile No.</label>

              <input name="transfer_customer_mobileno" id="transfer_customer_mobileno" class="form-control" type="number">

              <label for="transfer_customer_location_id">Location</label>

              <select id="transfer_customer_location_id" name="transfer_customer_location_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select location</option>

              <?php

                foreach($locations as $location){

              ?>

              <option value="<?= $location->location_id?>"><?= $location->location_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="transfer_customer_occupation_id">Occupation</label>

              <select id="transfer_customer_occupation_id" name="transfer_customer_occupation_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select occupation</option>

              <?php

                foreach($occupations as $occupation){

              ?>

              <option value="<?= $occupation->occupation_id?>"><?= $occupation->occupation_name?></option>

              <?php

                }

              ?>

              </select>

              <label for="transfer_customer_birthday">Birthday</label>

              <input id="transfer_customer_birthday" name="transfer_customer_birthday" class="form-control" type="text">

              <label for="transfer_customer_age_id">Age Range</label>

              <select id="transfer_customer_age_id" name="transfer_customer_age_id" class="form-control select2" type="text" style="width: 100%;">

              <option value="">Select age range</option>

              <?php

                foreach($ages as $age){

              ?>

              <option value="<?= $age->age_id?>"><?= $age->age_range?></option>

              <?php

                }

              ?>

              </select>

            </div>

          </div>

        </form>

      </div>

      <div class="modal-footer">

        <button id="transfer_customer_button" type="button" onclick="transfer_customer_old_data()" class="btn btn-success">Save

        </button>

      </div>

    </div>

    <!-- /.modal-content -->

  </div>

  <!-- /.modal-dialog -->

</div>







<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->

<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- Select2 -->

<script src="<?= base_url()?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>

<!-- AdminLTE App -->

<script src="<?= base_url()?>assets/dist/js/adminlte.min.js"></script>



<script src="<?= base_url()?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>



<script src="<?= base_url()?>assets/customs/js/customer/index.js?v=1.0"></script>



