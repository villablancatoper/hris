
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login | CRM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel='icon' type='image/png' href="<?= base_url().'assets/images/website-logo.png';?>">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url()?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
  
  <style>
      .login-box-body {
        background: rgba(0, 0, 0, 0.4) !important;
        border-radius: 3px;
      }
      
      .login-page {
          background: url("assets/images/login-background.jpg?v=1.0.0") no-repeat center center fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            
      }
      .login-logo a, .register-logo a {
            color: #FFFFFF; 
      }
      
      .form-group input[type='username'], .form-group input[type='password'], .form-group textarea {
          width: 100%;
          background: none;
          border: none;
          border-bottom: 1px solid #fff;
          color: #fff;
          font-size: 14px;
          padding: 5px 5px;
          margin-bottom: 20px;  
      }
      .login-box-body .form-control-feedback, .register-box-body .form-control-feedback {
        color: #FFFFFF;
      }
      .login-box{
        margin-top: 25vh !important;
      }
      html{
        height: auto !important;
      }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <img src="<?= base_url()?>assets/images/CRMLogo.png?v=1.0.0" alt="" style="max-width: 210px;">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <div id="info_message">
      <?php
        if($this->session->userdata('message')){
          echo $this->session->userdata('message');
          $this->session->unset_userdata('message');
        }
      ?>
    </div>

    <form action="<?= base_url()?>login" method="post">
      <div class="form-group has-feedback">
        <input id="ip_local" type="hidden" value="">
        <input id="ip_public" type="hidden" value="<?= $_SERVER['REMOTE_ADDR']?>">
        <input type="username" class="form-control" name="username" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?= base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/customs/js/login.js?v=1.0.3"></script>
</body>
</html>