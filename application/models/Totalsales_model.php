<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Totalsales_model extends CI_Model {
	
    function __construct(){
        parent::__construct();
  
    }
    public function get($branch_id, $start, $end){
    	$dailysales = $this->db->query("SELECT ds.branch_id, ds.transaction_date, ds.cash_status, ds.card_status, ds.cash_deposit, ds.deposit_date 
    									FROM dailysales ds 
    									WHERE ds.branch_id = $branch_id
    									")->result();
        if($branch_id == 100001 || $branch_id == 100002 ||$branch_id == 100003 ||$branch_id == 100004 ||$branch_id == 100005 ||
        $branch_id == 100006 ||$branch_id == 100008 ||$branch_id == 100009){
            $query = $this->db->query("SELECT br.brand_name, b.branch_name,t.transaction_date, SUM(t.transaction_totalsales) as transaction_totalsales, SUM(t.transaction_paymentcash) as transaction_paymentcash, 
									SUM(t.transaction_paymentcard) as transaction_paymentcard, SUM(t.transaction_paymentgc) as transaction_paymentgc, SUM(0) as cash_status, SUM(0) as card_status, FORMAT(0,0) as cash_deposit, NULL as deposit_date
									from transaction t
									LEFT JOIN branch b on b.branch_id = t.branch_id
									LEFT JOIN brand br ON br.brand_id = t.brand_id
									WHERE t.brand_id = $branch_id AND
									t.transaction_status = 1 AND
									t.transaction_date BETWEEN '$start' and '$end'
                                    GROUP BY b.branch_name, t.transaction_date
								")->result();
        }else{
            $query = $this->db->query("SELECT br.brand_name, b.branch_name,t.transaction_date, SUM(t.transaction_totalsales) as transaction_totalsales, SUM(t.transaction_paymentcash) as transaction_paymentcash, 
									SUM(t.transaction_paymentcard) as transaction_paymentcard, SUM(t.transaction_paymentgc) as transaction_paymentgc, SUM(0) as cash_status, SUM(0) as card_status, FORMAT(0,0) as cash_deposit, NULL as deposit_date
									from transaction t
									LEFT JOIN branch b on b.branch_id = t.branch_id
									LEFT JOIN brand br ON b.branch_id = br.brand_id
									WHERE t.branch_id = $branch_id AND
									t.transaction_status = 1 AND
									t.transaction_date BETWEEN '$start' and '$end'
                                    GROUP BY t.transaction_date
								")->result();
        }

	
			
			
			
			
			foreach ($query as $all_dates) {
					if ($all_dates->cash_status == 0) {
						$all_dates->cash_status = "For Approval";
					}
					if ($all_dates->card_status == 0) {
						$all_dates->card_status = "For Approval";
					}
				foreach ($dailysales as $per_date) {
					if ($all_dates->transaction_date == $per_date->transaction_date ) {
						$all_dates->cash_status = $per_date->cash_status;
						$all_dates->card_status = $per_date->card_status;
            $all_dates->cash_deposit = $per_date->cash_deposit;
            $all_dates->deposit_date = $per_date->deposit_date;
					}
				}
			}
		return $query;

    }

    public function get_sum($branch_id, $start, $end){
		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 
									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc
									from transaction t
									WHERE t.branch_id = $branch_id AND 
									t.transaction_date BETWEEN '$start' and '$end'
								");
		return $query->result();
    }
  
  	public function get_branches(){
  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");
  		return $query->result();
  	}

  	public function getStatus($branch_id, $transaction_date){
  		$query = $this->db->query("SELECT ds.cash_status, ds.card_status FROM dailysales ds WHERE ds.transaction_date = $transaction_date AND
  							ds.branch_id = $branch_id
  							")->row();
  		if ($query != NULL) {
  			return $query;
  		}else{
  			$query = array('cash_status' => 'For Approval', 'card_status' => 'For Approval' );
  			return $query;
  		}
  	}
  	// START
  	public function submitTally($data, $cash_card){
  		$branch_id = $data['branch_id'];
  		$transaction_date = $data['transaction_date'];
  		
  		$query = $this->db->select('id')
  						  ->from('dailysales')
  						  ->where('branch_id', $branch_id)
  						  ->where('transaction_date' , $transaction_date)
  						  ->get()->num_rows();;
  		if ($query > 0) {
  			if ($cash_card == 'cash') {
  				$this->db->set('cash_status', $data['cash_status']);
  				$this->db->where('branch_id', $data['branch_id']);
  				$this->db->where('transaction_date', $data['transaction_date']);
  				$this->db->update('dailysales');
  			}elseif ($cash_card == 'card') {
  				$this->db->set('card_status', $data['card_status']);
  				$this->db->where('branch_id', $data['branch_id']);
  				$this->db->where('transaction_date', $data['transaction_date']);
  				$this->db->update('dailysales');
  			}
  		}else{
  			$this->db->insert('dailysales', $data);
  		}
  	}// END 

  	public function submitCashVariance($data, $cash_card){
  		$branch_id = $data['branch_id'];
  		$transaction_date = $data['transaction_date'];
  		$cash_variance = $data['cash_variance'];
  		$cash_status = $data['cash_status'];


  		$query = $this->db->select('id')
  						  ->from('dailysales')
  						  ->where('branch_id', $branch_id)
  						  ->where('transaction_date' , $transaction_date)
  						  ->get()->num_rows();;
  		if ($query > 0) {
  				$this->db->query("UPDATE dailysales SET cash_variance = $cash_variance, cash_status = '$cash_status' WHERE branch_id = $branch_id AND transaction_date = '$transaction_date'");
  				
  		}else{
  			$this->db->insert('dailysales', $data);
  		}
  	}

  	public function submitCardVariance($data, $cash_card){
  		$branch_id = $data['branch_id'];
  		$transaction_date = $data['transaction_date'];
  		$card_variance = $data['card_variance'];
  		$card_status = $data['card_status'];

  		$query = $this->db->select('id')
  						  ->from('dailysales')
  						  ->where('branch_id', $branch_id)
  						  ->where('transaction_date' , $transaction_date)
  						  ->get()->num_rows();;
  		if ($query > 0) {
  				$this->db->query("UPDATE dailysales SET card_variance = $card_variance, card_status = '$card_status' WHERE branch_id = $branch_id AND transaction_date = '$transaction_date'");
  		}else{
  			$this->db->insert('dailysales', $data);
  		}
  	}


    public function add_deposit($data){
        $cash_deposit = $data['cash_deposit'];
        $branch_id = $data['branch_id'];
        $transaction_date = $data['datepicker'];
        $now = $data['deposit_date'];
        $query = $this->db->select('id')
                ->from('dailysales')
                ->where('branch_id', $branch_id)
                ->where('transaction_date' , $transaction_date)
                ->where('deposit_date', NULL)
                ->get()->num_rows();
        if ($query > 0 ) {
            $this->db->query("UPDATE dailysales SET cash_deposit = $cash_deposit, deposit_date = '$now' WHERE branch_id = $branch_id AND transaction_date = '$transaction_date'");

        }else{
           $this->db->insert('dailysales', $data);
        }

    }
 

}