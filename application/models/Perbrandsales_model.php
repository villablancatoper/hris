<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perbrandsales_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function get($start){

		$end = date('Y-m-t', strtotime($start));
		$data = array();

		$data['rows'] = $this->db->query("SELECT A.brand_name, A.total_storetarget_original, B.transaction_totalsales FROM(
				SELECT SUM(st.storetarget_original) AS total_storetarget_original, br.brand_name 
				FROM storetarget st
				LEFT JOIN branch b ON b.branch_id = st.branch_id
				LEFT JOIN brand br ON br.brand_id = b.brand_id 
				WHERE st.storetarget_month BETWEEN '$start' AND '$end'
				GROUP BY br.brand_name
			) AS A
	
			LEFT JOIN(
				SELECT br.brand_name, SUM(t.transaction_totalsales) AS transaction_totalsales
				FROM transaction t 
				LEFT JOIN branch b ON b.branch_id = t.branch_id 
				LEFT JOIN brand br ON br.brand_id = b.brand_id 
				WHERE b.brand_id != 100007 
				AND transaction_date BETWEEN '$start' AND '$end'
				AND t.transaction_status = 1
				GROUP BY br.brand_name
			) AS B
			ON A.brand_name = B.brand_name
			ORDER BY B.transaction_totalsales DESC
		")->result();

		// $data['days_count'] = date('t', strtotime($start));

		return $data;
    }
}
