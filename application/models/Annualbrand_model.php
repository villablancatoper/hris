<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Annualbrand_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }

    public function get($data){
        $brands = $this->db->query("
        SELECT brand_name, brand_id
        FROM brand
        WHERE brand_id != 100007 
        ORDER BY brand_name ")->result();

        foreach ($brands as $value1) {
         
        	foreach ($data['months'] as $month) {
                $start = $month['start'];
                $end = $month['end'];
                $monthname=$month['monthname'];
                $sale = 0;

                $totalsale = $this->db->query("SELECT COALESCE(SUM(t.transaction_totalsales), 0) AS totalsale
                FROM transaction t 
                LEFT JOIN branch br ON t.branch_id = br.branch_id 
                LEFT JOIN brand b ON b.brand_id = br.brand_id 
                WHERE b.brand_id = $value1->brand_id
                AND transaction_date BETWEEN '$start' AND '$end' 
                AND t.transaction_totalsales IS NOT NULL
                GROUP BY b.brand_name")->row();

                if($totalsale){
                    $sale = $totalsale->totalsale;
                }

                $output['list'][] = array(
                    'date' => $monthname, 
                    'brand' => $value1->brand_name,
                    'sales' => $sale
                );       
            }
        }
    	

		return $output;

    }



    public function get_sum($branch_id, $start, $end){

		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 

									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc

									from transaction t

									WHERE t.branch_id = $branch_id AND 

									t.transaction_date BETWEEN '$start' and '$end'

								");

		return $query->result();

    }


  	public function get_branches(){

  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");

  		return $query->result();

  	}







  	// START



  	// END 

 



}