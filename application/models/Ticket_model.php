<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket_model extends CI_Model {
    
    function __construct(){
      parent::__construct();
      $this->ticket_db = $this->load->database('ticketing', TRUE);

    }

    public function search_overview_it($start, $department, $selected_month_start, $selected_month_end){

        $query = $this->ticket_db->query("
            SELECT ticket_category, ticket_status, ticket_submit_date_time, ticket_aging, ticket_date_completed
            FROM tbl_ticket 
            WHERE ticket_category = 'CCTV'
            OR ticket_category = 'Telephone/Landline'
            OR ticket_category = 'Card Terminal'
            OR ticket_category = 'Biometrics'
            OR ticket_category = 'Internet'
            OR ticket_category = 'Computer(Desktop or Laptop)'
            OR ticket_category = 'Other IT concern'
            AND ticket_submit_date_time BETWEEN '$start' AND '$selected_month_end' ")->result();
        
            $category = $this->mis_category();
            $data = array('list' => $query, 'category' => $category);
            return $data;
    }

    public function search_overview_facilities($start, $department, $selected_month_start, $selected_month_end){
        $query = $this->ticket_db->query("
            SELECT t.ticket_category, t.ticket_status, t.ticket_submit_date_time, t.ticket_aging, t.ticket_date_completed
            FROM tbl_ticket t
            INNER JOIN facilities_category cat ON cat.category = t.ticket_category
            WHERE t.ticket_submit_date_time BETWEEN '$start' AND '$selected_month_end'")->result();
        $category = $this->facilities_category();

        $data = array('list' => $query, 'category' => $category);
        return $data;
    }

    public function mis_category(){
         $result = $this->ticket_db->select("*");
         $result = $this->ticket_db->get('mis_category');
         return $result->result();
    }

     public function facilities_category(){
         $result = $this->ticket_db->select("*");
         $result = $this->ticket_db->get('facilities_category');
         return $result->result();
    }


    public function get_ticket_list(){
        $result = $this->ticket_db->select('ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,
             ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details');
        $result = $this->ticket_db->order_by('ticket_submit_date_time', 'DESC');
        $result = $this->ticket_db->get('tbl_ticket as t1');
        return $result->result();
    }

     public function facility_ticket(){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket 
            WHERE ticket_category = 'Aircondition'
            OR ticket_category = 'Equipments'
            OR ticket_category = 'Furnitures'
            OR ticket_category = 'Civil'
            OR ticket_category = 'Plumbing'
            OR ticket_category = 'Electrical'
            OR ticket_category = 'Permits'
            OR ticket_category = 'Glass'
            ORDER BY ticket_submit_date_time DESC")->result();
        return $result;
    }

    public function store_facility_ticket($branch_name){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket t
            LEFT JOIN branch b ON b.branch_name = t.ticket_brand_branch
            WHERE t.ticket_brand_branch = '$branch_name'
            AND (ticket_category = 'Aircondition'
            OR ticket_category = 'Equipments'
            OR ticket_category = 'Furnitures'
            OR ticket_category = 'Civil'
            OR ticket_category = 'Plumbing'
            OR ticket_category = 'Electrical'
            OR ticket_category = 'Permits'
            OR ticket_category = 'Glass')
            ORDER BY ticket_submit_date_time DESC")->result();
        return $result;
    }
    
    public function mis_ticket(){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket 
            WHERE ticket_category = 'CCTV'
            OR ticket_category = 'Telephone/Landline'
            OR ticket_category = 'Card Terminal'
            OR ticket_category = 'Biometrics'
            OR ticket_category = 'Internet'
            OR ticket_category = 'Computer(Desktop or Laptop)'
            OR ticket_category = 'Other IT concern'
            ORDER BY ticket_submit_date_time DESC")->result();
        return $result;
    }
    
    public function store_mis_ticket($branch_name){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket t
            LEFT JOIN branch b ON b.branch_name = t.ticket_brand_branch
            WHERE t.ticket_brand_branch = '$branch_name'
            AND (t.ticket_category = 'CCTV'
            OR t.ticket_category = 'Telephone/Landline'
            OR t.ticket_category = 'Card Terminal'
            OR t.ticket_category = 'Biometrics'
            OR t.ticket_category = 'Internet'
            OR t.ticket_category = 'Computer(Desktop or Laptop)'
            OR t.ticket_category = 'Other IT concern')
            ORDER BY t.ticket_submit_date_time DESC")->result();
        return $result;
    }

    public function facility_sorting($sort_index){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM (SELECT * from tbl_ticket 
                WHERE ticket_category = 'Aircondition'
                OR ticket_category = 'Equipments'
                OR ticket_category = 'Furnitures'
                OR ticket_category = 'Civil'
                OR ticket_category = 'Plumbing'
                OR ticket_category = 'Electrical'
                OR ticket_category = 'Permits'
                OR ticket_category = 'Glass') as ticket_list
            WHERE ticket_status = '$sort_index'")->result();
        return $result;
    }

    public function mis_sorting($sort_index){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket WHERE ticket_status = '$sort_index'")->result();
        return $result;
    }

    public function general_sorting($sort_index){
        $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
            FROM tbl_ticket")->result();
        return $result;
    }

  

    public function store_sorting($branch_id, $sort_index){
        $result = $this->ticket_db->query("SELECT tbl_ticket.ticket_id, tbl_ticket.ticket_submit_date_time, tbl_ticket.ticket_brand_branch, tbl_ticket.ticket_category, tbl_ticket.ticket_title,tbl_ticket.ticket_assignee, tbl_ticket.ticket_status, tbl_ticket.ticket_aging, tbl_ticket.ticket_last_update_date, tbl_ticket.ticket_last_update_details, tbl_ticket.ticket_date_completed
            FROM tbl_ticket
            LEFT JOIN branch ON branch.branch_name = tbl_ticket.ticket_brand_branch 
            WHERE tbl_ticket.ticket_status = '$sort_index' AND branch.branch_id = $branch_id")->result();
        return $result;
    }

    public function asm_sorting($user_id, $sort_index){
        $result = $this->ticket_db->query("SELECT tbl_ticket.ticket_id, tbl_ticket.ticket_submit_date_time, tbl_ticket.ticket_brand_branch, tbl_ticket.ticket_category, tbl_ticket.ticket_title,tbl_ticket.ticket_assignee, tbl_ticket.ticket_status, tbl_ticket.ticket_aging, tbl_ticket.ticket_last_update_date, tbl_ticket.ticket_last_update_details, tbl_ticket.ticket_date_completed
            FROM tbl_ticket
            LEFT JOIN branch ON branch.branch_name = tbl_ticket.ticket_brand_branch 
            LEFT JOIN user ON user.user_id = branch.branch_areamanager
            WHERE tbl_ticket.ticket_status = '$sort_index' AND user.user_id = $user_id")->result();
        return $result;
    }


    public function submit_ticket($data){
 
        $this->ticket_db->insert('tbl_ticket', $data);
    }

    public function get_ticket_status($ticket_id){
    	$this->ticket_db->where('ticket_id', $ticket_id);
        $this->ticket_db->order_by('ticket_update_timestamp', "DESC");
    	$result = $this->ticket_db->get('tbl_ticket_new_updates');
    	return $result->result();
     
    }


    public function get_site_store(){
       $this->ticket_db->select('branch_name, branch_id');
       $this->ticket_db->where('branch_id !=',1);
       $this->ticket_db->order_by('branch_name','ASC');
       $result = $this->ticket_db->get('branch');
       return $result->result();
    }

    public function ticket_updates(){
        $ticket_id = $this->ticket_db->insert_id();
        $this->ticket_db->where('ticket_id', $ticket_id);
        $data = $this->ticket_db->get('tbl_ticket');
        $data->row();
        $this->ticket_db->set('ticket_id', $ticket_id);
        $this->ticket_db->set('ticket_updater', $data->row()->ticket_requestor);
        $this->ticket_db->set('ticket_update', $data->row()->ticket_last_update_details);
        $this->ticket_db->insert('tbl_ticket_new_updates');
    }

    public function insert_latest_updates($data){
        $timestamp = date('Y-m-d h:i:s');
        $this->ticket_db->set('ticket_id', $data['ticket_id']);
        $this->ticket_db->set('ticket_updater', $data['ticket_updater']);
        $this->ticket_db->set('ticket_update', $data['ticket_update']);
        $this->ticket_db->set('ticket_update_timestamp', $timestamp);
        $this->ticket_db->insert('tbl_ticket_new_updates');
        $this->reflect_update($data);

    }

    public function close_ticket($ticket_id, $ticket_status){
        $this->ticket_db->where('ticket_id', $ticket_id);
        $this->ticket_db->set('ticket_status', $ticket_status);
        $this->ticket_db->update('tbl_ticket');
    }

    public function complete_ticket($ticket_id, $date){
        $this->ticket_db->where('ticket_id', $ticket_id);
        $this->ticket_db->set('ticket_date_completed', $date);
        $this->ticket_db->update('tbl_ticket');
    }

    public function get_last_update($ticket_id){
        $this->ticket_db->where('ticket_id', $ticket_id);
        $this->ticket_db->order_by('ticket_update_timestamp', 'DESC');
        $this->ticket_db->limit(1);
        $result = $this->ticket_db->get('tbl_ticket_new_updates');
        return $result->result();
    }
    public function accept_ticket($data){
        $ticket_status = 'In-progress';
        $now = date('Y-m-d h:i:s');
        $this->ticket_db->where('ticket_id', $data['ticket_id']);
        $this->ticket_db->set('ticket_status', $ticket_status);
        $this->ticket_db->set('ticket_last_update_details', $data['ticket_update']);
        $this->ticket_db->set('ticket_last_update_date', $now);
        $this->ticket_db->update('tbl_ticket');
    }

    public function reflect_update($data){
        $now = date('Y-m-d h:i:s');
        $this->ticket_db->where('ticket_id', $data['ticket_id']);
        $this->ticket_db->set('ticket_last_update_details', $data['ticket_update']);
        $this->ticket_db->set('ticket_last_update_date', $now);
        $this->ticket_db->update('tbl_ticket');
    }

    public function get_ticket_by_store($branch_id){
   $query = $this->ticket_db->select('ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,
             ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed')
                 ->from('tbl_ticket')
                 ->join('branch', 'tbl_ticket.ticket_brand_branch = branch.branch_name', 'LEFT')
                 ->where('branch.branch_id', $branch_id )
                 ->get();
    return $query->result();
    }


    public function get_branch($branch_id){
    $query = $this->ticket_db->query("SELECT branch_name from branch where branch_id = $branch_id and branch_status = 1");
    return $query->result();
    }

    public function asm_ticket($user_id){
    $query = $this->ticket_db->query("SELECT tt.ticket_id, tt.ticket_submit_date_time, tt.ticket_brand_branch, tt.ticket_category, tt.ticket_title, tt.ticket_status, tt.ticket_aging, tt.ticket_last_update_date, tt.ticket_last_update_details, tt.ticket_date_completed
        FROM tbl_ticket tt
        LEFT JOIN branch b on b.branch_name = tt.ticket_brand_branch
        LEFT JOIN user u on u.user_id = b.branch_areamanager
        WHERE u.user_id = $user_id
        ORDER BY tt.ticket_submit_date_time DESC");
    return $query->result();
    }


    public function asm_branches($user_id){
    $query = $this->db->query("SELECT u.user_name, b.branch_name, b.branch_id
            FROM branch b
            LEFT JOIN user u on u.user_id = b.branch_areamanager
            WHERE u.user_id = $user_id
            AND b.branch_status = 1
            ORDER BY b.branch_name ASC");
    return $query->result();
    }

    public function get_branches(){
    $query = $this->db->query("SELECT b.branch_name, b.branch_id
            FROM branch b
            WHERE b.branch_status = 1 AND b.brand_id != 100007
            OR branch_status = 1 AND branch_name LIKE  '%head office%'
            OR branch_status = 1 AND branch_name LIKE '%KC%'
            ORDER BY b.branch_name ASC");
    return $query->result();
    }

     public function store_branches($user_id){
    $query = $this->ticket_db->query("SELECT u.user_name, b.branch_name
            FROM branch b
            LEFT JOIN user u on u.user_id = b.branch_areamanager
            WHERE u.user_id = $user_id
            AND b.branch_status = 1
            ORDER BY b.branch_name ASC");
    return $query->result();
    }



    public function mis_set_assignee($ticket_id,$full_name){
        $now = date('Y-m-d H:i:s');
        $this->ticket_db->where('ticket_id', $ticket_id);
        $this->ticket_db->set('ticket_assignee', $full_name);
        $this->ticket_db->set('ticket_last_update_date', $now);
        $this->ticket_db->update('tbl_ticket');
    }

    public function status_completed($ticket_id){
        $query = $this->ticket_db->query("SELECT ticket_status, ticket_brand_branch, ticket_category, ticket_title from tbl_ticket where ticket_id = $ticket_id");
        return $query->result();
    }


    public function get_assigned($ticket_assignee){
        $query = $this->ticket_db->select("ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,
            ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details")
                        ->from("tbl_ticket")
                        ->where("ticket_assignee", $ticket_assignee)
                          ->get();
        return $query->result();
    }

    


}