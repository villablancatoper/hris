<?php

class Budgetfolder_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_branches(){

        $am_query = '';

        $user_id = $this->session->userdata('user')->user_id;

        if($this->session->userdata('user')->branch_id == 200){
            $am_query = "AND branch_areamanager = $user_id";
        }

        return $this->db->query("SELECT * 
        FROM branch 
        WHERE branch_status = 1
        AND brand_id != 100007
        $am_query
        ORDER BY branch_name ASC");

    }

    public function generate_budgetfolder($branch_id){

        $items = $this->db->query("SELECT currentinventory_copy.item_id, currentinventory_copy.currentinventory_dateupdated
        FROM currentinventory_copy
        LEFT JOIN item ON item.item_id = currentinventory_copy.item_id
        WHERE branch_id = $branch_id
        AND currentinventory_copy.currentinventory_dateupdated = (
            SELECT MAX(currentinventory_dateupdated)
            FROM currentinventory_copy
            WHERE branch_id = $branch_id
        )
        AND currentinventory_copy.currentinventory_quantity > 0
        ORDER BY item.item_description ASC")->result();

        $data = array();

        $total_otc = 0;
        $total_consumables = 0;

        $grand_total = 0;

        foreach($items as $item){
            $budgetfolder = $this->db->query("SELECT c.currentinventory_dateupdated, cat.category_name, i.item_name, i.item_shade, i.item_color, i.item_model, c.currentinventory_quantity, c.currentinventory_uom, i.item_uomsize, i.item_sapuom, c.item_cost
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE c.branch_id = $branch_id
            AND i.item_id = $item->item_id
            AND c.currentinventory_dateupdated = '$item->currentinventory_dateupdated'
            ORDER BY i.item_name ASC")->row();

            $sap_conversion = 0;

            if($budgetfolder->currentinventory_quantity > 0){
                $sap_conversion = ROUND(($budgetfolder->currentinventory_quantity / $budgetfolder->item_uomsize), 2);
            }

            $total_cost = 0;

            if($sap_conversion > 0){
                $total_cost = ROUND(($budgetfolder->currentinventory_quantity / $budgetfolder->item_uomsize) * $budgetfolder->item_cost, 2);
            }

            if(strpos($budgetfolder->category_name, "Consumables") == TRUE){
                $total_consumables = $total_consumables + $total_cost;
            }
            else{
                $total_otc = $total_otc + $total_cost;
            }

            $grand_total = $grand_total + $total_cost;

            $data['data'][] = array(
                'category_name' => $budgetfolder->category_name,
                'item_name' => $budgetfolder->item_name,
                'currentinventory_quantity' => $budgetfolder->currentinventory_quantity,
                'currentinventory_uom' => $budgetfolder->currentinventory_uom,
                'item_uomsize' => $budgetfolder->item_uomsize,
                'item_sapuom' => $budgetfolder->item_sapuom,
                'sap_conversion' => $sap_conversion ,
                'item_cost' => $budgetfolder->item_cost,
                'total_cost' => $total_cost,
                'currentinventory_dateupdated' => $budgetfolder->currentinventory_dateupdated
            );
        }

        $data['grand_total'] = ROUND($grand_total, 2);
        $data['total_otc'] = ROUND($total_otc, 2);
        $data['total_consumables'] = ROUND($total_consumables, 2);

        return $data;
        
    }

    public function generate_budgetfolder_total($branch_id){

        $total_total_cost = 0;

        $data = $this->db->query("SELECT ROUND((c.currentinventory_quantity / i.item_uomsize) * c.item_cost, 2) 'total_cost'
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        WHERE c.branch_id = $branch_id")->result();

        foreach($data as $total_cost){
            if($total_cost->total_cost > 0){
                $total_total_cost += $total_cost->total_cost ;
            }
        }

        return $total_total_cost;
    }

    public function get_currentinventory(){
        $branch_id = $this->session->userdata('user')->branch_id;

        return $this->db->query("SELECT c.currentinventory_id, cat.category_name, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function update_currentinventory_quantity($ids){
        
        // Updates currentinventory

        for($i = 0; $i < count($ids); $i++){
            
            $currentinventory = $this->db->query("SELECT c.branch_id, i.item_id, i.item_uomsize, i.item_uom 
            FROM item i 
            LEFT JOIN currentinventory c ON i.item_id = c.item_id
            WHERE c.currentinventory_id = $ids[$i][0]")->row();

            $converted_quantity = $ids[$i][1] * $currentinventory->item_uomsize;

            if($ids[$i][1] != ''){

                if($currentinventory->currentinventory_quantity > $converted_quantity){
                    $this->db->where('currentinventory_id', $ids[$i][0]);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));
                }
                else{
                    $data = array(
                        'branch_id' => $currentinventory->branch_id,
                        'item_id' => $currentinventory->item_id,
                        'inventory_quantity' => $ids[$i][1],
                        'inventory_status' => 'PENDING'
                    );
    
                    $this->db->insert('inventoryaudit', $data);
                }
    
                // Updates or insert currentinventory
    
                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();
    
                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $ids[$i][1] * $currentinventory->item_uomsize,
                        // 'currentinventory_dateupdated' => date('Y-m-d')
                    ));
                }
                else{
                    $branch_id = $this->session->userdata('user')->branch_id;
    
                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'currentinventory_quantity' => $ids[$i][1] * $currentinventory->item_uomsize,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        // 'currentinventory_dateupdated' => date('Y-m-d')
                    );
    
                    $this->db->insert('currentinventory_copy', $data);
                }

            }
        }

    }

    public function submit_budgetfolder($ids, $dateupdated){
        $branch_id = $this->session->userdata('user')->branch_id;

        // Updates currentinventory

        for($i = 0; $i < count($ids); $i++){

            $id = $ids[$i]['id'];
            $value = floatval($ids[$i]['value']);
            
            $currentinventory = $this->db->query("SELECT c.branch_id, i.item_id, i.item_uomsize, i.item_uom, c.currentinventory_quantity 
            FROM item i 
            LEFT JOIN currentinventory c ON i.item_id = c.item_id
            WHERE c.currentinventory_id = $id")->row();

            $converted_quantity = $value * $currentinventory->item_uomsize;

            if($ids[$i]['value'] != NULL){

                if($currentinventory->currentinventory_quantity < $converted_quantity){
                    $this->db->where('currentinventory_id', $id);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));
                }
                else{

                    $this->db->where('currentinventory_id', $id);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));

                    $this->db->where('branch_id', $currentinventory->branch_id);
                    $this->db->where('item_id', $currentinventory->item_id);
                    $this->db->where('inventoryaudit_date', $dateupdated);
                    $inventoryaudit = $this->db->get('inventoryaudit')->row();
                    
                    if($inventoryaudit){

                        $data = array(
                            'branch_id' => $inventoryaudit->branch_id,
                            'item_id' => $inventoryaudit->item_id,
                            'inventoryaudit_quantity' => $currentinventory->currentinventory_quantity - ($value * $currentinventory->item_uomsize),
                            'inventoryaudit_date' => $dateupdated,
                            'inventoryaudit_status' => 'PENDING'
                        );

                        $this->db->where('inventoryaudit_id', $inventoryaudit->branch_id);
                        $this->db->update('inventoryaudit', $data);

                    }
                    else{
                        $data = array(
                            'branch_id' => $currentinventory->branch_id,
                            'item_id' => $currentinventory->item_id,
                            'inventoryaudit_quantity' => $currentinventory->currentinventory_quantity - ($value * $currentinventory->item_uomsize),
                            'inventoryaudit_date' => $dateupdated,
                            'inventoryaudit_status' => 'PENDING'
                        );
        
                        $this->db->insert('inventoryaudit', $data);
                    }

                }

                // Updates or insert currentinventory

                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $this->db->where('currentinventory_dateupdated', $dateupdated);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $converted_quantity
                    ));
                }
                else{
                    $branch_id = $this->session->userdata('user')->branch_id;

                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'item_cost' => $currentinventory->item_cost,
                        'currentinventory_quantity' => $converted_quantity,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        'currentinventory_dateupdated' => $dateupdated
                    );

                    $this->db->insert('currentinventory_copy', $data);
                }

            }
            else{

                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $this->db->where('currentinventory_dateupdated', $dateupdated);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $currentinventory->currentinventory_quantity
                    ));
                }
                else{

                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'item_cost' => $currentinventory->item_cost,
                        'currentinventory_quantity' => $currentinventory->currentinventory_quantity,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        'currentinventory_dateupdated' => $dateupdated
                    );

                    $this->db->insert('currentinventory_copy', $data);
                }

            }
        }
        
    }

    public function get_brands(){
        return $this->db->query("SELECT brand_id, brand_name FROM brand WHERE brand_id != 100007 ORDER BY brand_name ASC");
    }

    public function check_brand_budgetfolder_availability($brand_id, $month, $batch_no){

        $branch_id = $this->session->userdata('user')->branch_id;

        $user_id = $this->session->userdata('user')->user_id;

        $am_query = null;

        if($branch_id == 200){
            $am_query = "AND b.branch_areamanager = $user_id";
        }

        $branches = $this->db->query("SELECT b.branch_id, b.branch_name, br.brand_name, b.branch_company, b.branch_budgetfolder
        FROM branch b 
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        WHERE b.brand_id = $brand_id
        AND b.branch_status = 1
        $am_query
        AND b.branch_batchno = $batch_no
        ORDER BY b.branch_name ASC")->result();

        $sales_forecast_query_date = date('Y')."-$month-01";

        $month_and_year = date('F', strtotime($sales_forecast_query_date)).' '.date('Y');

        $budgetfolder_wall_to_wall_date1 = date('Y-m-d', strtotime(date('Y').'-'.($month-1).'-15'));
        $budgetfolder_wall_to_wall_date2 = date('Y-m-d', strtotime(date('Y').'-'.($month-2).'-31'));
        $budgetfolder_wall_to_wall_date3 = date('Y-m-d', strtotime(date('Y').'-'.($month-2).'-30'));

        $data = array();

        foreach($branches as $branch){

            $wall_to_wall_date = $this->db->query("SELECT currentinventory_dateupdated, currentinventory_status FROM currentinventory_copy WHERE branch_id = $branch->branch_id AND ((currentinventory_dateupdated = '$budgetfolder_wall_to_wall_date1') OR (currentinventory_dateupdated = '$budgetfolder_wall_to_wall_date2') OR (currentinventory_dateupdated = '$budgetfolder_wall_to_wall_date3'))")->row();

            $wall_to_wall_month = '';

            $currentinventory_status = '';

            if($wall_to_wall_date){
                $wall_to_wall_month = $wall_to_wall_date->currentinventory_dateupdated != NULL ? date('m', strtotime($wall_to_wall_date->currentinventory_dateupdated))+1 : NULL;

                $currentinventory_status = $wall_to_wall_date->currentinventory_status;
            }
            
            $wall_to_wall_status = '';

            if((($wall_to_wall_month == $month)||($wall_to_wall_month == $month-1)) && $currentinventory_status != 'DONE'){
                $wall_to_wall_status = 'VIEW SUBMITTED';
            }
            else if( $currentinventory_status == 'DONE'){
                $wall_to_wall_status = 'DONE';
            }
            else{
                $wall_to_wall_status = 'PENDING';
            }


            $sales_forecast_date = $this->db->query("SELECT storetarget_month FROM storetarget WHERE branch_id = $branch->branch_id AND storetarget_month = '$sales_forecast_query_date'")->row();

            $sales_forecast_month = '';

            if($sales_forecast_date){
                $sales_forecast_month = $sales_forecast_date->storetarget_month != NULL ? date('m', strtotime($sales_forecast_date->storetarget_month)) : NULL;
            }


            $sales_forecast_status = $sales_forecast_month == $month ? 'DONE' : 'PENDING';

            $this->db->where('branch_id', $branch->branch_id);
            $this->db->where('budgetfolder_budgetdate', $month_and_year);
            $budgetfolder = $this->db->get('budgetfolder')->row();

            $budgetfolder_status = $sales_forecast_status == 'DONE' && $wall_to_wall_status == 'DONE' ? 'READY FOR CREATION' : 'NOT READY';

            $budget_date = '';

            if($budgetfolder){
                $budgetfolder_status = $budgetfolder->budgetfolder_status;
                $budget_date = $budgetfolder->budgetfolder_budgetdate;
            }

            $this->db->where('branch_id', $branch->branch_id);
            $this->db->where('storetarget_month', date('Y').'-'.$month.'-01');
            $storetarget = $this->db->get('storetarget')->row();

            $storetarget_original = null;
            $storetarget_updatedsalesforecast = null;

            if($storetarget){
                $storetarget_original = $storetarget->storetarget_original;
                $storetarget_updatedsalesforecast = $storetarget->storetarget_updatedsalesforecast;
            }


            $data[] = array(
                'branch_id' => $branch->branch_id,
                'month_and_year' => $month_and_year,
                'brand' => $branch->brand_name,
                'branch' => $branch->branch_name,
                'wall_to_wall_status' => $wall_to_wall_status,
                'sales_forecast_status' => $sales_forecast_status,
                'budgetfolder_status' => $budgetfolder_status,
                'wall_to_wall_date' => $wall_to_wall_date,
                'sales_forecast_date' => $sales_forecast_date,
                'budget_date' => $budget_date,
                'company' => $branch->branch_company,
                'folder' => $branch->branch_budgetfolder,
                'original_target' => $storetarget_original,
                'sales_forecast' => $storetarget_updatedsalesforecast,
                'wall_to_wall_month' => $wall_to_wall_month
            );
        }

        return $data;

    }

    public function get_budgetfolder_inventory($branch_id, $wall_to_wall_date){

        if($wall_to_wall_date){

            return $this->db->query("SELECT cat.category_name, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, c.item_cost
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON cat.category_id = i.category_id
            WHERE c.branch_id = $branch_id
            AND c.currentinventory_dateupdated = '$wall_to_wall_date'
            ORDER BY cat.category_name DESC, i.item_description ASC");

        }

        return $this->db->query("SELECT cat.category_name, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, c.item_cost
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        ORDER BY cat.category_name DESC, i.item_description ASC");


    }

    public function get_budgetfolder_inventory_consumables($branch_id, $wall_to_wall_date){

        return $this->db->query("SELECT c.currentinventory_id, i.item_description, i.item_ordermodule, i.item_shade, i.item_color, i.item_model, c.item_cost, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND cat.category_name LIKE '%Consumables%'
        AND c.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC");

    }

    public function get_budgetfolder_inventory_otcs($branch_id, $wall_to_wall_date){

        return $this->db->query("SELECT c.currentinventory_id, i.item_description, i.item_ordermodule, i.item_shade, i.item_color, i.item_model, c.item_cost, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND cat.category_name LIKE '%OTC%'
        AND c.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC");

    }

    public function get_sales_forecast($branch_id){
        $this->db->where('branch_id', $branch_id);
        return $this->db->get('storetarget');
    }

    // public function test(){
    //     $items = $this->db->query("SELECT * FROM item WHERE category_id IN (1065, 1064)
    //     ORDER BY category_id DESC, item_description ASC")->result();

    //     $nh_branches = $this->db->query("SELECT * FROM branch WHERE branch_status = 1 AND brand_id = 100002 ORDER BY branch_name ASC")->result();

    //     foreach($nh_branches as $nh_branch){

    //         foreach($items as $item){

    //             $data = array(
    //                 'branch_id' => $nh_branch->branch_id,
    //                 'item_id' => $item->item_id,
    //                 'currentinventory_quantity' => 0.00,
    //                 'currentinventory_uom' => $item->item_uom
    //             );

    //             $this->db->insert('currentinventory', $data);

    //         }

    //     }

    //     return 'Completed';
    // }

    public function add_items(){
        $items = $this->db->query("SELECT * FROM item WHERE item_id IN(51508, 51510, 51512)")->result();

        $nh_branches = $this->db->query("SELECT * FROM branch WHERE branch_status = 1 AND brand_id = 100004 AND branch_id IN(146) ORDER BY branch_name ASC")->result();

        foreach($nh_branches as $nh_branch){

            foreach($items as $item){

                $data = array(
                    'branch_id' => $nh_branch->branch_id,
                    'item_id' => $item->item_id,
                    'item_cost' => $item->item_cost,
                    'currentinventory_quantity' => 0.00,
                    'currentinventory_uom' => $item->item_uom,
                    'currentinventory_dateupdated' => '2019-09-30',
                    'currentinventory_status' => 'DONE'
                );

                // $this->db->insert('currentinventory', $data);

                $this->db->insert('currentinventory_copy', $data);

            }

        }

        return 'Completed';
    }

    // public function another_test(){
    //     return $this->db->query("SELECT ci1.item_id, ci1.currentinventory_uom, ci2.item_id, ci2.currentinventory_uom
    //     FROM currentinventory AS ci1, currentinventory AS ci2
    //     WHERE ci1.item_id = ci2.item_id and ci1.currentinventory_uom != ci2.currentinventory_uom");
    // }

    public function confirm_budgetfolder_inventory($branch_id, $wall_to_wall_date){
        $this->db->where('branch_id', $branch_id);
        $this->db->where('currentinventory_dateupdated', $wall_to_wall_date);
        $this->db->update('currentinventory_copy', array('currentinventory_status' => 'DONE'));
    }

    public function check_encoding_availability($date_as_of){
        $branch_id = $this->session->userdata('user')->branch_id;

        $this->db->where('branch_id', $branch_id);
        $this->db->where('currentinventory_dateupdated', $date_as_of);
        $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

        if($currentinventory_copy){

            if(!$currentinventory_copy->currentinventory_status){
                return TRUE;
            }

            else if($currentinventory_copy->currentinventory_status == "DONE"){
        
                // if($currentinventory_copy->currentinventory_status == "DONE"){
                //     return FALSE;
                // }

                // else{
                    return FALSE;
                // }

            }
            else{
                return TRUE;
            }

            return json_encode($currentinventory_copy);

        }

        return TRUE;
    }

    public function get_branch($branch_id){
        $this->db->where('branch_id', $branch_id);
        return $this->db->get('branch');
    }

    public function create_budgetfolder($budgetfolder_data, $consumables, $otcs){
        $this->db->where('branch_id', $budgetfolder_data['branch_id']);
        $this->db->where('budgetfolder_budgetdate', $budgetfolder_data['budgetfolder_budgetdate']);
        $budgetfolder = $this->db->get('budgetfolder')->row();

        if($budgetfolder){
            $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
            $this->db->update('budgetfolder', $budgetfolder_data);

            if(!empty($consumables)){
                $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
                $this->db->delete('budgetfoldermaterialorder');

                for($i = 0; $i < count($consumables); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder->budgetfolder_id,
                        'currentinventory_id' => $consumables[$i]['currentinventory_id'],
                        'budgetfoldermaterialorder_quantity' => $consumables[$i]['order_quantity']
                    );
        
                    $this->db->insert('budgetfoldermaterialorder', $data);
                }
            }
    
            if(!empty($otcs)){
                $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
                $this->db->delete('budgetfolderotcorder');

                for($i = 0; $i < count($otcs); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder->budgetfolder_id,
                        'currentinventory_id' => $otcs[$i]['currentinventory_id'],
                        'budgetfolderotcorder_quantity' => $otcs[$i]['order_quantity']
                    );
    
                    $this->db->insert('budgetfolderotcorder', $data);
                }
            }
        }

        else{
            $this->db->insert('budgetfolder', $budgetfolder_data);

            $budgetfolder_id = $this->db->insert_id();

            if(!empty($consumables)){
                for($i = 0; $i < count($consumables); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder_id,
                        'currentinventory_id' => $consumables[$i]['currentinventory_id'],
                        'budgetfoldermaterialorder_quantity' => $consumables[$i]['order_quantity']
                    );
        
                    $this->db->insert('budgetfoldermaterialorder', $data);
                }
            }

            if(!empty($otcs)){
                for($i = 0; $i < count($otcs); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder_id,
                        'currentinventory_id' => $otcs[$i]['currentinventory_id'],
                        'budgetfolderotcorder_quantity' => $otcs[$i]['order_quantity']
                    );

                    $this->db->insert('budgetfolderotcorder', $data);
                }
            }
        }
    }

    public function view_submitted_budgetfolder_consumables($branch_id, $budget_date){
        $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime(preg_replace('/d+/u', '', $budget_date)))-1).'-15'));

        $items = $this->db->query("SELECT i.item_description, i.item_shade, i.item_color, i.item_model, bfm.budgetfoldermaterialorder_cost, ROUND(cc.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, bfm.budgetfoldermaterialorder_quantity
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id        
        WHERE bf.budgetfolder_budgetdate = '$budget_date'
        AND bf.branch_id = $branch_id
        AND cc.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC")->result();

        $this->db->where('branch_id', $branch_id);
        $this->db->where('budgetfolder_budgetdate', $budget_date);
        $budget_folder = $this->db->get('budgetfolder')->row();

        $data = array(
            'items' => $items,
            'beginning_total' => $budget_folder->budgetfolder_materialbeginningtotal,
            'order_total' => $budget_folder->budgetfolder_materialordertotal,
            'max_allowance' => $budget_folder->budgetfolder_materialmaxallowance,
        );

        return $data;
    }

    public function view_submitted_budgetfolder_otcs($branch_id, $budget_date){
        $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime(preg_replace('/d+/u', '', $budget_date)))-1).'-15'));

        $items = $this->db->query("SELECT i.item_description, i.item_shade, i.item_color, i.item_model, bfo.budgetfolderotcorder_cost, ROUND(cc.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, bfo.budgetfolderotcorder_quantity
        FROM budgetfolderotcorder bfo
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id        
        WHERE bf.budgetfolder_budgetdate = '$budget_date'
        AND bf.branch_id = $branch_id
        AND cc.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC")->result();

        $this->db->where('branch_id', $branch_id);
        $this->db->where('budgetfolder_budgetdate', $budget_date);
        $budget_folder = $this->db->get('budgetfolder')->row();

        $data = array(
            'items' => $items,
            'beginning_total' => $budget_folder->budgetfolder_otcbeginningtotal,
            'order_total' => $budget_folder->budgetfolder_otcordertotal,
            'max_allowance' => $budget_folder->budgetfolder_otcmaxallowance,
        );

        return $data;
    }

    public function get_budgetfolders($month, $batch_no){
        return $this->db->query("SELECT DISTINCT branch_budgetfolder 
        FROM branch 
        WHERE branch_batchno = $batch_no
        ORDER BY branch_budgetfolder ASC"); 
    }
    
    public function view_budgetfolder_summary($folder, $month, $batch){

        // $this->db->where('branch_budgetfolder', $folder);
        // $this->db->where('branch_batchno', $batch);
        // $branches = $this->db->get('branch')->result();

        $branches = $this->db->query("SELECT branch.*, brand_name 
        FROM branch 
        LEFT JOIN brand ON brand.brand_id = branch.brand_id
        WHERE branch_budgetfolder = '$folder' AND branch_batchno = $batch
        AND branch_status = 1
        ORDER BY branch_name ASC")->result();

        $material_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folder' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%Consumables%'
        ORDER BY i.item_supplier ASC")->result();

        $otc_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folder' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%OTC%'
        ORDER BY i.item_supplier ASC")->result();

        $storetarget_month = date('Y').'-'.$month.'-01';

        $month_name = date('F', strtotime($storetarget_month)).' '.date('Y');

        $data['columns'] = array();

        foreach($branches as $branch){

            $budgetfolder = $this->db->query("SELECT br.brand_name, st.storetarget_original 'original', st.storetarget_updatedsalesforecast 'sales_forecast', st.storetarget_averagesalesperformance 'average_sales_performance', st.storetarget_updatedsalesforecast*0.94 'ninety_four_percent', (st.storetarget_updatedsalesforecast*0.94)*0.05 'six_percent', bf.budgetfolder_materialbeginningtotal 'beginning', bf.budgetfolder_materialmaxallowance 'material_max_allowance', bf.budgetfolder_materialordertotal 'material_order_total', bf.budgetfolder_materialmaxallowance-bf.budgetfolder_materialordertotal 'material_over_under', bf.budgetfolder_otcbeginningtotal 'otc_beginning', bf.budgetfolder_otcmaxallowance 'otc_max_allowance', bf.budgetfolder_otcordertotal 'otc_order_total', bf.budgetfolder_otcordertotal-bf.budgetfolder_otcmaxallowance 'otc_over_under'
            FROM storetarget st
            LEFT JOIN budgetfolder bf ON bf.branch_id = st.branch_id
            LEFT JOIN branch b ON b.branch_id = st.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE st.storetarget_month = '$storetarget_month'
            AND bf.branch_id = $branch->branch_id")->row();

            $materials = array();

            foreach($material_suppliers as $material_supplier){

                $material_total_cost = 0;

                $material = $this->db->query("SELECT SUM(cc.item_cost*bfm.budgetfoldermaterialorder_quantity) 'total_cost'
                FROM budgetfoldermaterialorder bfm
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE i.item_supplier LIKE '%$material_supplier->item_supplier%'
                AND cc.branch_id = $branch->branch_id
                AND bf.budgetfolder_budgetdate = '$month_name'")->row();

                if($material){
                    $material_total_cost = $material->total_cost;
                }

                $materials[] = $material_total_cost;

            }

            $otcs = array();

            foreach($otc_suppliers as $otc_supplier){
                
                $otc_total_cost = 0;

                $otc = $this->db->query("SELECT SUM(cc.item_cost*bfo.budgetfolderotcorder_quantity) 'total_cost'
                FROM budgetfolderotcorder bfo
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE i.item_supplier LIKE '%$otc_supplier->item_supplier%'
                AND cc.branch_id = $branch->branch_id
                AND bf.budgetfolder_budgetdate = '$month_name'")->row();

                if($otc){
                    $otc_total_cost = $otc->total_cost;
                }

                $otcs[] = $otc_total_cost;

            }

            $data['columns'][] = array(
                'branch_name' => $branch->branch_name,
                'original' => $budgetfolder ? $budgetfolder->original : NULL,
                'sales_forecast' => $budgetfolder ? $budgetfolder->sales_forecast : NULL,
                'average_sales_performance' => $budgetfolder ? $budgetfolder->average_sales_performance : NULL ,
                'beginning' => $budgetfolder ? $budgetfolder->beginning : NULL,
                'otc_beginning' => $budgetfolder ? $budgetfolder->otc_beginning : NULL,
                'material_max_allowance' => $budgetfolder ? $budgetfolder->material_max_allowance : NULL,
                'material_order_total' => $budgetfolder ? $budgetfolder->material_order_total : NULL,
                'material_over_under' => $budgetfolder ? $budgetfolder->material_over_under : NULL,
                'otc_max_allowance' => $budgetfolder ? $budgetfolder->otc_max_allowance : NULL,
                'otc_order_total' => $budgetfolder ? $budgetfolder->otc_order_total : NULL,
                'otc_over_under' => $budgetfolder ? $budgetfolder->otc_over_under : NULL,
                'material_cost' => $materials,
                'otc_cost' => $otcs
            );

            $data['side_infos']['brand_name'] = $branch->brand_name;

            $data['side_infos']['branch_company'] = $branch->branch_company;

            $data['side_infos']['budgetfolder_month'] = $month_name;

            $data['side_infos']['budgetfolder'] = $folder;

            $data['side_infos']['material_suppliers'] = $material_suppliers;

            $data['side_infos']['otc_suppliers'] = $otc_suppliers;

        }

        return $data;
    }

    public function view_budgetfolder_orders($folder, $month, $batch){

        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y');

        $data = array();

        $branches = $this->db->query("SELECT DISTINCT branch.*, brand_name 
        FROM branch 
        LEFT JOIN brand ON brand.brand_id = branch.brand_id
        WHERE branch_budgetfolder = '$folder' AND branch_batchno = $batch
        AND branch_status = 1
        AND branch.branch_id IN (
            SELECT DISTINCT b.branch_id
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            ORDER BY b.branch_name ASC
        )
        OR branch.branch_id IN (
            SELECT DISTINCT b.branch_id
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            ORDER BY b.branch_name ASC
        )
        ORDER BY branch_name ASC")->result();


        $material_suppliers_result = $this->db->query("SELECT DISTINCT item_supplier
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY item_supplier
        ORDER BY item_supplier ASC,  branch_name ASC")->result();

        $material_suppliers = [];

        foreach($material_suppliers_result as $value){
            array_push($material_suppliers, $value->item_supplier);
        }

        $otc_suppliers_result = $this->db->query("SELECT DISTINCT item_supplier
        FROM budgetfolderotcorder bfo
        LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY item_supplier
        ORDER BY item_supplier ASC,  branch_name ASC")->result();

        $otc_suppliers = [];

        foreach($otc_suppliers_result as $value){
            array_push($otc_suppliers, $value->item_supplier);
        }

        $branch_ids = [];

        $branches_array = $branches;

        foreach($branches as $branch){
            array_push($branch_ids, $branch->branch_id);
        }

        $branches = [];

        $branches_result = $this->db->query("SELECT b.branch_name
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE i.item_supplier IN ('".implode("','",$material_suppliers)."')
        AND b.branch_id IN ('".implode("','",$branch_ids)."')
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        ORDER BY i.item_supplier ASC, i.item_name ASC, b.branch_name ASC")->result();

        foreach($branches_result as $key => $value){
            $branches[] = $value->branch_name;
        }

        $branches = array_values(array_unique($branches));

        $materials = $this->db->query("SELECT DISTINCT i.item_id, i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE i.item_supplier IN ('".implode("','",$material_suppliers)."')
        AND b.branch_id IN ('".implode("','",$branch_ids)."')
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY i.item_id
        ORDER BY i.item_supplier ASC, i.item_name ASC, i.item_shade ASC, i.item_color ASC, i.item_model ASC, b.branch_name ASC")->result();

        $otcs = $this->db->query("SELECT DISTINCT i.item_id, i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom
        FROM budgetfolderotcorder bfo
        LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE i.item_supplier IN ('".implode("','",$otc_suppliers)."')
        AND b.branch_id IN ('".implode("','",$branch_ids)."')
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY i.item_id
        ORDER BY i.item_supplier ASC, i.item_name ASC, i.item_shade ASC, i.item_color ASC, i.item_model ASC, b.branch_name ASC")->result();

        foreach($materials as $material){

            foreach($branches_array as $value){

                $data['columns']['materials'][] = $this->db->query("SELECT b.branch_name, i.item_name, i.item_supplier, bfm.budgetfoldermaterialorder_quantity, bfm.budgetfoldermaterialorder_quantity * cc.item_cost 'amount'
                FROM budgetfoldermaterialorder bfm
                LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
                LEFT JOIN branch b ON b.branch_id = bf.branch_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE b.branch_id = $value->branch_id
                AND bf.budgetfolder_budgetdate = '$month_and_year'
                AND i.item_id = $material->item_id")->result();

            }

        }

        foreach($otcs as $otc){

            foreach($branches_array as $value){

                $data['columns']['otcs'][] = $this->db->query("SELECT b.branch_name, i.item_name, i.item_supplier, bfo.budgetfolderotcorder_quantity, bfo.budgetfolderotcorder_quantity * cc.item_cost 'amount'
                FROM budgetfolderotcorder bfo
                LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
                LEFT JOIN branch b ON b.branch_id = bf.branch_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE b.branch_id = $value->branch_id
                AND bf.budgetfolder_budgetdate = '$month_and_year'
                AND i.item_id = $otc->item_id")->result();

            }

        }


        // $data = array(
        //     'folder' => $folder,
        //     'month_and_year' => $month_and_year,
        //     'branches' => $branches,
        //     'materials' => $materials,
        //     'otcs' => $otcs,
        //     'material_suppliers' => $material_suppliers,
        //     'otc_suppliers' => $otc_suppliers,
        //     'columns' => $data['columns']
        // );

        // natcasesort($branches);

        $data['folder'] = $folder;
        $data['month_and_year'] = $month_and_year;
        $data['branches'] = $branches;
        $data['materials'] = $materials;
        $data['otcs'] = $otcs;
        $data['material_suppliers'] = $material_suppliers;
        $data['otc_suppliers'] = $otc_suppliers;
        $data['branches_array'] = $branches_array;

        return $data;
    }
}