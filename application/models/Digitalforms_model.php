<?php

class Digitalforms_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }


    public function get_branches() {

        return $this->db->query("SELECT * 
        FROM branch 
        WHERE branch_status = 1
        ORDER BY branch_name ASC");

    }

    public function get_branch_users($user_branch_id) {

        return $this->db->query("SELECT * 
        FROM user 
        WHERE branch_id = $user_branch_id");

    }


    public function get_requests($user_id) {

        $this->db->where('user_id', $user_id);
        $this->db->where('is_deleted', 0);
        return $this->db->get('requests');


    }

    public function post_request($form_id) {

        return $this->db->query("SELECT br.branch_id, br.branch_name, rq.user_name,rq.form_id, rq.date_created, rq.request_level, rq.transfer_branch, rq.covered_time, rq.request_status, rq.user_position, rq.from_date, rq.to_date, rq.request_type, rq.remarks,rq.time_in,rq.time_out,rq.new_time_in, rq.new_time_out 
        FROM requests rq
        LEFT JOIN branch br ON rq.branch_id = br.branch_id
        WHERE form_id = $form_id");

    }


    public function post_request_history($form_id) {

        return $this->db->query("SELECT *, DATE_FORMAT(date_created, '%Y/%m/%d %h:%m') as converted_date
        FROM request_history
        WHERE form_id = $form_id
        ORDER BY date_created DESC");

    }

    
    public function get_pendings($branch_id) {

        return $this->db->query("SELECT * 
        FROM requests
        WHERE branch_id = $branch_id AND is_deleted = 0");

    }

    public function get_second_level($branch_id) {

        return $this->db->query("SELECT * 
        FROM requests
        WHERE (branch_id = $branch_id AND is_deleted = 0 AND request_level = 0)");

    }

    public function get_third_level($branch_id) {

        return $this->db->query("SELECT * 
        FROM requests
        WHERE (branch_id = $branch_id AND is_deleted = 0 AND request_level = 0 OR request_level = 1)");

    }

    //continue
    public function show_major_requests() {

        return $this->db->query("SELECT br.branch_id, br.branch_name, rq.user_name,rq.form_id, rq.date_created, rq.request_status, rq.user_position, rq.from_date, rq.to_date, rq.request_type, rq.remarks 
        FROM requests rq
        LEFT JOIN branch br ON rq.branch_id = br.branch_id
        WHERE (is_deleted = 0 AND request_level BETWEEN 1 AND 3)");

    }

    public function update_request($id, $data) {

        $this->db->where('form_id', $id);
        $this->db->update('requests', $data);

    }

    public function delete_request($request_id) {

        $this->db->where('form_id', $request_id);
        $this->db->update('requests', array('is_deleted' => 1));
        
    }

    //create new user with request history

    public function create_new_request($data) {

        $this->db->insert('requests', $data);

        return $this->db->insert_id();

    }

    public function new_request_history($history) {

        $this->db->insert('request_history', $history);

    }

    //Approve user with request history

    public function approve_user_request($form_id, $data) {

        $this->db->where('form_id', $form_id);
        $this->db->update('requests', $data);

    }


    public function update_request_history($history) {

        $this->db->insert('request_history', $history);

    }

    public function get_certains($branch, $leave) {

        // return $this->db->query("SELECT br.branch_id, br.branch_name, rq.user_name,rq.form_id, rq.date_created, rq.request_status, rq.user_position, rq.from_date, rq.to_date, rq.request_type, rq.remarks 
        // FROM requests rq
        // LEFT JOIN branch br ON rq.branch_id = br.branch_id
        // WHERE request_type = 'Personal/Emergency Leave'
        // -- AND branch_id = $branch");

        $this->db->where_in('requests.request_type', $leave);
        $this->db->where('branch.branch_id', $branch);
        $this->db->select('requests.form_id, requests.date_created, requests.user_name, branch.branch_name, requests.request_type, requests.from_date, requests.to_date,requests.time_in,requests.time_out ,requests.new_time_in, requests.transfer_branch, requests.new_time_out, requests.covered_time, requests.request_status, requests.remarks');
        $this->db->from('requests');
        $this->db->join('branch', 'requests.branch_id = branch.branch_id');
        return $this->db->get();

    }

}
