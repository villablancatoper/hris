<?php



  class Customer_model extends CI_Model{

    public function __construct(){

      parent::__construct();
      ini_set('memory_limit', '-1');

    }



    public function get_customers(){

      return $this->db->get('customer');

    }



    public function get_customer($id){

      $this->db->where('customer_id', $id);

      return $this->db->get('customer')->row();

    }



    public function update_customer($data, $id){

      $this->db->where('customer_id', $id);

      $update_customer = $this->db->update('customer', $data);



      if($update_customer){

        return TRUE;

      }

      return FALSE;

    }



    public function delete_customer($id){

      $this->db->where('customer_id', $id);

      $this->db->update('customer', array('customer_status' => 0));

    }



    public function add_customer($data){
        
      $this->db->where('customer_firstname', $data['customer_firstname']);
      
      $this->db->where('customer_lastname', $data['customer_lastname']);
      
      $customer = $this->db->get('customer')->row();
      
      if($customer){
          
      }
      else{
        $add_customer = $this->db->insert('customer', $data);



        if($add_customer){
    
          return TRUE;
    
        }
    
        return FALSE;   
      }
    }



    public function get_locations(){

      return $this->db->get('location');

    }



    public function get_occupations(){

      return $this->db->get('occupation');

    }



    public function get_ages(){

      return $this->db->get('age');

    }



    public function search_customers_new($search_item){

      return $this->db->query("SELECT * 

      FROM customer 

      WHERE (customer_firstname LIKE '%$search_item%' 

      OR customer_lastname LIKE '%$search_item%' 

      OR customer_email LIKE '%$search_item%' 

      OR customer_mobileno LIKE '%$search_item%'

      OR (CONCAT(customer_firstname, CONCAT(' ', customer_lastname))) LIKE '%$search_item%' )

      AND customer_status != 0;");

    }



    public function search_customers_old($search_item){

      $branch_id = $this->session->userdata('user')->branch_id;

      return $this->db->query("SELECT c.C_IDNO, c.C_BRANCH_ID, b.branch_name, c.C_FNAME, c.C_LNAME, c.C_MOBILENO, c.C_SEX,  a.age_id, a.age_range, l.location_id, l.location_name

      FROM customer_e c

      LEFT JOIN age a ON c.C_AGERANGE = a.age_id

      LEFT JOIN location l ON c.C_L_IDNO = l.location_id

      LEFT JOIN branch b ON b.branch_id = c.C_BRANCH_ID 

      WHERE (C_FNAME LIKE '%$search_item%' 

      OR C_LNAME LIKE '%$search_item%'

      OR C_MOBILENO LIKE '%$search_item%'
      
      OR (CONCAT(C_FNAME, CONCAT(' ', C_LNAME))) LIKE '%$search_item%'

      )
      
      AND C_BRANCH_ID = $branch_id");

    }



    public function get_customer_old($C_IDNO){

      $branch_id = $this->session->userdata('user')->branch_id;

      return $this->db->query("SELECT c.C_IDNO, c.C_FNAME, c.C_LNAME, c.C_MOBILENO, c.C_SEX,  a.age_id, a.age_range, l.location_id, l.location_name

      FROM customer_e c

      LEFT JOIN age a ON c.C_AGERANGE = a.age_id

      LEFT JOIN location l ON c.C_L_IDNO = l.location_id

      WHERE c.C_IDNO = $C_IDNO
      AND C_BRANCH_ID = $branch_id");

    }



    public function is_customer_data_transferred($C_IDNO){

      $branch_id = $this->session->userdata('user')->branch_id;

      $is_transferred = $this->db->query("SELECT * 
      FROM customer_e
      WHERE C_COPIED IS NOT NULL
      AND C_IDNO = $C_IDNO
      AND C_BRANCH_ID = $branch_id
      ")->row();



      if($is_transferred){

        return TRUE;

      }

      return FALSE;

    }



    public function transfer_customer_old_data($C_IDNO, $data){

      $branch_id = $this->session->userdata('user')->branch_id;

      $this->db->insert('customer', $data);


      $customer_id = $this->db->insert_id();


      $this->db->where('C_IDNO', $C_IDNO);

      $this->db->where('C_BRANCH_ID', $branch_id);

      $this->db->update('customer_e', array('C_COPIED' => $customer_id));

    }

    public function get_customer_old_status($C_IDNO, $C_BRANCH_ID){

      $latest_joborder_date = $this->db->query("SELECT j.JT_DATESERVE, br.brand_name, b.branch_name, s.serviceprovider_name

      FROM job_trans_e j

      LEFT JOIN branch b ON b.branch_id = j.JT_BRANCH_ID

      LEFT JOIN brand br ON br.brand_id = b.brand_id

      LEFT JOIN serviceprovider s ON s.serviceprovider_id = j.JT_S_IDNO

      WHERE j.JT_C_IDNO = $C_IDNO

      AND j.JT_BRANCH_ID = $C_BRANCH_ID

      ORDER BY j.JT_DATESERVE DESC")->row();


      $date_now = strtotime(date('Y-m-d'));

      $date_transact = strtotime($latest_joborder_date->JT_DATESERVE);

      $variant =  ($date_now - $date_transact) / 86400;


      if($variant < 90){

          return "Regular";

      }
      else{
          return "Walk-in";
      }

    }

    public function get_all_customers(){
      return $this->db->query("SELECT c.customer_id, c.customer_firstname, c.customer_lastname, c.customer_mobileno
      FROM customer c
      WHERE c.customer_status != 0");
    }

    public function update_customer_foottraffic($customer_id, $foottraffic){
      $this->db->where('customer_id', $customer_id);
      $customer = $this->db->get('customer')->row();

      $this->db->where('customer_id', $customer_id);
      $this->db->update('customer', array('customer_laststatus', $customer->customer_foottraffic));

      $this->db->where('customer_id', $customer_id);
      $this->db->update('customer', array('customer_foottraffic' => $foottraffic));
    }

  }