<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_model extends CI_Model {
    
    function __construct(){
      parent::__construct();
      $this->survey_db = $this->load->database('survey', TRUE);
    }

    public function answer_later($data){

        $query = $this->survey_db->insert('it_dept', $data);
       
    }

    public function submit_survey($data, $user_id){
    	$branch_id = $data['branch_id'];
    	$query = $this->survey_db->query("SELECT id 
    		FROM it_dept WHERE branch_id = $branch_id")->num_rows();

    	if ($query > 0) {
    		$this->survey_db->set('flag_encode_later', 0);
    		$this->survey_db->set('user_name', $data['user_name']);
    		$this->survey_db->set('contact', $data['contact']);
    		$this->survey_db->set('date_submitted', $data['date_submitted']);
    		$this->survey_db->set('internet', $data['internet']);
    		$this->survey_db->set('telephone', $data['telephone']);
    		$this->survey_db->set('card_terminal', $data['card_terminal']);
    		$this->survey_db->set('PC', $data['PC']);
    		$this->survey_db->set('others', $data['others']);
        $this->survey_db->where('branch_id', $branch_id);
       	$this->survey_db->update('it_dept');

       		$this->db->set('user_contact', $data['contact']);
       		$this->db->where('user_name', $data['user_name']);
       		$this->db->update('user');
       		$this->update_branch_survey_status($user_id);
    	} else {
    		$query = $this->survey_db->insert('it_dept', $data);
    		$this->db->set('user_contact', $data['contact']);
    		$this->db->where('user_name', $data['user_name']);
       	$this->db->update('user');
       	$this->update_branch_survey_status($branch_id);
    	}

    }

    public function get_branch_survey_status($branch_id){
  	  $this->db->select('branch_surveystatus');
      $this->db->where('branch_id', $branch_id);
   
      $query = $this->db->get('branch')->row();
      return $query;

    }

    public function update_branch_survey_status($branch_id){
      $this->db->where('branch_id', $branch_id);
      $this->db->set('branch_surveystatus', 0);
      $this->db->update('branch');
    }

}