<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Annualstore_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }

    public function get($branch_id, $start, $end , $branch, $brand_id, $datas){

        $branch_id=$branch_id;
        $branch=$branch;
        $brand=$brand_id;
      

    






    	if ($branch == 'All Branches') {

         $brands = $this->db->query("
         SELECT brand_name
         FROM brand
          WHERE brand_id = '$brand'
          ORDER BY brand_name ")->result();

        

         $branchs = $this->db->query("
          SELECT branch_name, brand_id,branch_id
          FROM branch
          WHERE brand_id = '$brand'
          ORDER BY branch_name ")->result();
          foreach ($branchs as $value1) {
           
          foreach ($datas['months'] as $month) {
                $start = $month['start'];
                $end = $month['end'];
                $monthname=$month['monthname'];
                $sale = 0;

                $totalsale = $this->db->query("SELECT COALESCE(SUM(t.transaction_totalsales), 0) AS totalsale
                FROM transaction t 
                LEFT JOIN branch br ON t.branch_id = br.branch_id 
                LEFT JOIN brand b ON b.brand_id = br.brand_id 
                WHERE br.branch_id = '$value1->branch_id'
                AND transaction_date BETWEEN '$start' AND '$end' 
                AND t.transaction_totalsales IS NOT NULL
                GROUP BY br.branch_name")->row();

                if($totalsale){
                    $sale = $totalsale->totalsale;
                }
                foreach ($brands as $value2) {
                $output['list'][] = array(
                    'date' => $monthname, 
                    
                      'brand_name' => $value2->brand_name,
                    
                    'brand' => $value1->branch_name,
                    'sales' => $sale
                 );  
                }     
            }
             
            }
            return $output;
        }

      else if ($branch != 'All Branches') {

         $brands = $this->db->query("
         SELECT brand_name
         FROM brand
          WHERE brand_id = '$brand'
          ORDER BY brand_name ")->result();

        

         $branchs = $this->db->query("
          SELECT branch_name, brand_id,branch_id
          FROM branch
          WHERE  branch_id= '$branch_id'
          ORDER BY branch_name ")->result();
          foreach ($branchs as $value1) {
           
          foreach ($datas['months'] as $month) {
                $start = $month['start'];
                $end = $month['end'];
                $monthname=$month['monthname'];
                $sale = 0;

                $totalsale = $this->db->query("SELECT COALESCE(SUM(t.transaction_totalsales), 0) AS totalsale
                FROM transaction t 
                LEFT JOIN branch br ON t.branch_id = br.branch_id 
                LEFT JOIN brand b ON b.brand_id = br.brand_id 
                WHERE br.branch_id = '$value1->branch_id'
                AND transaction_date BETWEEN '$start' AND '$end' 
                AND t.transaction_totalsales IS NOT NULL
                GROUP BY br.branch_name")->row();

                if($totalsale){
                    $sale = $totalsale->totalsale;
                }
                foreach ($brands as $value2) {
                $output['list'][] = array(
                    'date' => $monthname, 
                    
                      'brand_name' => $value2->brand_name,
                    
                    'brand' => $value1->branch_name,
                    'sales' => $sale
                 );  
                }     
            }
             
            }
            return $output;
        }


   





        }

 

    	


    
 public function delete_user($storetarget_id){
        $this->db->where('storetarget_id', $storetarget_id);
      $this->db->delete('storetarget');
    }

  public function update_currentinventory_clone($storetarget_id, $storetarget_month,$storetarget_original, $Storetarget_forecast,$Storetarget_average){
      $this->db->where('storetarget_id', $storetarget_id);
      $this->db->set('storetarget_month',  $storetarget_month);
      $this->db->set('storetarget_original',  $storetarget_original);
      $this->db->set('storetarget_updatedsalesforecast',  $Storetarget_forecast);
      $this->db->set('storetarget_averagesalesperformance',  $Storetarget_average);
      $this->db->update('storetarget');
    }

    public function get_sum($branch_id, $start, $end){

		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 

									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc

									from transaction t

									WHERE t.branch_id = $branch_id AND 

									t.transaction_date BETWEEN '$start' and '$end'

								");

		return $query->result();

    }
public function add_user($data){
       $branch_id=$data['branch_id'];
       $storetarget_month=$data['storetarget_month'];
       $storetarget_original=$data['storetarget_original'];
       $storetarget_updatedsalesforecast=$data['storetarget_updatedsalesforecast'];
       $storetarget_averagesalesperformance=$data['storetarget_averagesalesperformance'];


       $query=$this->db->select('branch_id')
            ->from('storetarget')
            ->where('branch_id',$branch_id)
            ->where('storetarget_month',$storetarget_month)
            ->get()->num_rows();

            
if($query>0)
{
      $this->db->set('storetarget_month',  $storetarget_month);
      $this->db->set('storetarget_original',  $storetarget_original);
      $this->db->set('storetarget_updatedsalesforecast',  $storetarget_updatedsalesforecast);
      $this->db->set('storetarget_averagesalesperformance',  $storetarget_averagesalesperformance);
      $this->db->where('branch_id', $branch_id);
      $this->db->where('storetarget_month', $storetarget_month);
      $this->db->update('storetarget');
}
else
{
  $this->db->insert('storetarget', $data);

}


         
        
        
    }
    public function fetch_branchess($brand_id){

      $this->db->select("branch_id,branch_name");

      $this->db->where("brand_id" , $brand_id);

      $query = $this->db->get("branch");

      $output = '<option value="">Select Branch</option>';

      

      foreach ($query->result() as $row) {

        $output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

      }

      return $output;

    }

  	public function get_branches(){

  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");

  		return $query->result();

  	}







  	// START



  	// END 

 



}