<?php



  class Settings_model extends CI_Model{

    public function __construct(){

      parent::__construct();

    }


    public function get_sp($branch_id){

      $query['list'] = $this->db->query("SELECT * from serviceprovider 
        WHERE branch_id = $branch_id")->result();
      $query['search'] = $this->db->query("SELECT * from employee_id")->result();
      return $query;
    }

    public function sp_list(){
      $query = $this->db->query("SELECT * from employee_id WHERE emp_number NOT LIKE '%KC%'")->result();
      return $query;
    }

    public function fetch_sp($sp_id){
      $query = $this->db->query("SELECT * from employee_id WHERE emp_number = '$sp_id'")->row();
      return $query;
    }

    public function add_sp($branch_id, $full_name){
        $data = array('serviceprovider_name' => $full_name, 'branch_id' => $branch_id);
        $return = $this->db->insert('serviceprovider', $data);
        return $return;
    }


    
    public function get_like($query){

      $result = $this->db->like('emp_number', $query);
      $result = $this->db->or_like('emp_lastname', $query);
      $result = $this->db->or_like('emp_firstname', $query);
      $result = $this->db->or_like('emp_middlename', $query);
      $result = $this->db->get("employee_id")->result();
      return $result;
    }

    public function search_name($name_query, $start, $end){


      $result = $this->db->query("SELECT at.*, d.location

        FROM attendance_table at

        LEFT JOIN devices d ON d.id = at.device_no

        WHERE at.name LIKE '%$name_query%' AND at.date_time BETWEEN '$start' AND '$end'

        OR at.user_no LIKE '%$name_query%' AND at.date_time BETWEEN '$start' AND '$end'

        OR at.user_id LIKE '%$name_query%' AND at.date_time BETWEEN '$start' AND '$end'

        OR d.location LIKE '%$name_query%' AND at.date_time BETWEEN '$start' AND '$end'

        ")->result();

      return $result;

    }



  }