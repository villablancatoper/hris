<?php



class MSSQL_model extends CI_Model

{

    var $sap;



    function __construct(){

        parent::__construct();
        ini_set('include_path', '.:/home/r2group/php:' . ini_get("include_path"));
    }



    public function set_db($db_name){

        $this->sap = $this->load->database($db_name, TRUE); #Final HOPE is to manually add the db name on database.php



        // $this->sap = $this->load->database('sap', TRUE);



        $this->sap->database = $db_name;

        $this->session->set_userdata('db_name', $db_name);

        // return var_dump($this->sap);



        // if($this->sap){

        //     return "SUCCESS";

        // }

        // else{

        //     return "FAILED";

        // }
        
        // $serverName = "192.168.2.37";
        // $connectionOptions = array("Database"=>$db_name, "Uid"=>"sa", "PWD"=>"Icspass.1");

        // //Establishes the connection
        // $this->sap = sqlsrv_connect($serverName, $connectionOptions);
        // if($this->sap){
        //     echo "SUCCESS";
        // }
        // else{
        //     echo "FAILED";
        // }

    }



    public function get_pdn1_items_by_whs_code($whs_code){

        $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM PDN1 WHERE WhsCode = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

        return $this->sap->query($sql, $whs_code);

    }



    public function get_wtr1_items_by_whs_code($whs_code){

        $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM WTR1 WHERE WhsCode = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

        return $this->sap->query($sql, $whs_code);

    }



    public function get_pdn_item_by_doc_entry_on_pdn1($doc_entry, $item_code = NULL, $whs_code = NULL){

        if($item_code == NULL || $whs_code == NULL){

            $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM PDN1 WHERE DocEntry = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

            return $this->sap->query($sql, $doc_entry);

        }

        $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM PDN1 WHERE DocEntry = ? AND ItemCode = ? AND WhsCode = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

        return $this->sap->query($sql, array($doc_entry, $item_code, $whs_code));

    }



    public function get_pdn_item_by_doc_entry_on_wtr1($doc_entry, $item_code = NULL, $whs_code = NULL){

        if($item_code == NULL || $whs_code == NULL){

            $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM WTR1 WHERE DocEntry = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

            return $this->sap->query($sql, $doc_entry);

        }

        $sql = "SELECT DocEntry, ItemCode, Dscription, Quantity, unitMsr FROM WTR1 WHERE DocEntry = ? AND ItemCode = ? AND WhsCode = ? AND DocDate >= '2019-05-01 00:00:00.000' ORDER BY DocDate DESC";

        return $this->sap->query($sql, $doc_entry, $item_code, $whs_code);

    }

}