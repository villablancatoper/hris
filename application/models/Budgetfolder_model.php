<?php

class Budgetfolder_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_branches(){

        $am_query = '';

        $user_id = $this->session->userdata('user')->user_id;

        if($this->session->userdata('user')->branch_id == 200){
            $am_query = "AND branch_areamanager = $user_id";
        }

        return $this->db->query("SELECT * 
        FROM branch 
        WHERE branch_status = 1
        AND brand_id != 100007
        $am_query
        ORDER BY branch_name ASC");

    }

    public function generate_budgetfolder($branch_id){

        $items = $this->db->query("SELECT currentinventory_copy.item_id, currentinventory_copy.currentinventory_dateupdated
        FROM currentinventory_copy
        LEFT JOIN item ON item.item_id = currentinventory_copy.item_id
        WHERE branch_id = $branch_id
        AND currentinventory_copy.currentinventory_dateupdated = (
            SELECT MAX(currentinventory_dateupdated)
            FROM currentinventory_copy
            WHERE branch_id = $branch_id
        )
        AND currentinventory_copy.currentinventory_quantity > 0
        ORDER BY item.item_description ASC")->result();

        $data = array();

        $total_otc = 0;
        $total_consumables = 0;

        $grand_total = 0;

        foreach($items as $item){
            $budgetfolder = $this->db->query("SELECT c.currentinventory_dateupdated, cat.category_name, i.item_name, i.item_shade, i.item_color, i.item_model, c.currentinventory_quantity, c.currentinventory_uom, i.item_uomsize, i.item_sapuom, c.item_cost
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE c.branch_id = $branch_id
            AND i.item_id = $item->item_id
            AND c.currentinventory_dateupdated = '$item->currentinventory_dateupdated'
            ORDER BY i.item_name ASC")->row();

            $sap_conversion = 0;

            if($budgetfolder->currentinventory_quantity > 0){
                $sap_conversion = ROUND(($budgetfolder->currentinventory_quantity / $budgetfolder->item_uomsize), 2);
            }

            $total_cost = 0;

            if($sap_conversion > 0){
                $total_cost = ROUND(($budgetfolder->currentinventory_quantity / $budgetfolder->item_uomsize) * $budgetfolder->item_cost, 2);
            }

            if(strpos($budgetfolder->category_name, "Consumables") == TRUE){
                $total_consumables = $total_consumables + $total_cost;
            }
            else{
                $total_otc = $total_otc + $total_cost;
            }

            $grand_total = $grand_total + $total_cost;

            $data['data'][] = array(
                'category_name' => $budgetfolder->category_name,
                'item_name' => $budgetfolder->item_name,
                'currentinventory_quantity' => $budgetfolder->currentinventory_quantity,
                'currentinventory_uom' => $budgetfolder->currentinventory_uom,
                'item_uomsize' => $budgetfolder->item_uomsize,
                'item_sapuom' => $budgetfolder->item_sapuom,
                'sap_conversion' => $sap_conversion ,
                'item_cost' => $budgetfolder->item_cost,
                'total_cost' => $total_cost,
                'currentinventory_dateupdated' => $budgetfolder->currentinventory_dateupdated
            );
        }

        $data['grand_total'] = ROUND($grand_total, 2);
        $data['total_otc'] = ROUND($total_otc, 2);
        $data['total_consumables'] = ROUND($total_consumables, 2);

        return $data;
        
    }
    public function show_topsheet($month, $year, $batch_no, $folders){
        $storetarget_month = $year.'-'.$month.'-01';
        $month_name = date('F', strtotime($storetarget_month)).' '.$year;

        $last_month_start = strtotime("-2 months", strtotime($storetarget_month));
        $last_month_start = strftime ( '%Y-%m-01' , $last_month_start );

        $last_month_end = date('Y-m-t', strtotime($last_month_start));

        $batch_query = null;

        $date_used_in_query_for_budgetstart =  $year.'-'.$month.'-01';

        $budgetfolderstart_query = "AND (b.branch_budgetfolderstart <= '$date_used_in_query_for_budgetstart' OR b.branch_budgetfolderstart IS NULL)";

        if($batch_no){
            $batch_query = 'AND branch_batchno = '.$batch_no;
        }

        $data['columns'] = array();
        $branches = $this->db->query("SELECT b.*, br.brand_name 
        FROM branch b
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        WHERE b.branch_budgetfolder = '$folders' $batch_query
        AND b.branch_status = 1
        $budgetfolderstart_query
        ORDER BY b.branch_name ASC")->result();

        $material_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folders' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%Consumables%'
        ORDER BY i.item_supplier ASC")->result();

        $otc_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folders' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%OTC%'
        ORDER BY i.item_supplier ASC")->result();

        foreach ($branches as $branch) {

            $last_month_sales = $this->db->query("
                SELECT SUM(t.transaction_totalsales) as sales
                from transaction t
                WHERE t.branch_id = $branch->branch_id 
                AND t.transaction_status = 1 AND
                t.transaction_date BETWEEN '$last_month_start' and '$last_month_end'
                ")->row()->sales;

            $budgetfolder = $this->db->query("SELECT br.brand_name, st.storetarget_original 'original', st.storetarget_updatedsalesforecast 'sales_forecast', st.storetarget_averagesalesperformance 'average_sales_performance', st.storetarget_updatedsalesforecast*0.94 'ninety_four_percent', (st.storetarget_updatedsalesforecast*0.94)*0.05 'six_percent', bf.budgetfolder_materialbeginningtotal 'beginning', bf.budgetfolder_materialmaxallowance 'material_max_allowance', bf.budgetfolder_materialordertotal 'material_order_total', bf.budgetfolder_materialmaxallowance-bf.budgetfolder_materialordertotal 'material_over_under', bf.budgetfolder_otcbeginningtotal 'otc_beginning', bf.budgetfolder_otcmaxallowance 'otc_max_allowance', bf.budgetfolder_otcordertotal 'otc_order_total', bf.budgetfolder_otcordertotal-bf.budgetfolder_otcmaxallowance 'otc_over_under'
            FROM storetarget st
            LEFT JOIN budgetfolder bf ON bf.branch_id = st.branch_id
            LEFT JOIN branch b ON b.branch_id = st.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE st.storetarget_month = '$storetarget_month'
            AND bf.branch_id = $branch->branch_id
            AND bf.budgetfolder_budgetdate = '$month_name'")->row();
            $materials = array();

            foreach($material_suppliers as $material_supplier){
                $material_total_cost = 0;
                $material = $this->db->query("SELECT SUM(cc.item_cost*bfm.budgetfoldermaterialorder_quantity) 'total_cost'
                FROM budgetfoldermaterialorder bfm
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE i.item_supplier LIKE '%$material_supplier->item_supplier%'
                AND bf.branch_id = $branch->branch_id
                AND bf.budgetfolder_budgetdate = '$month_name'")->row();

                if($material){
                    $material_total_cost = $material->total_cost;
                }
                $materials[] = $material_total_cost;
            }

            $otcs = array();

            foreach($otc_suppliers as $otc_supplier){
                
                $otc_total_cost = 0;

                $otc = $this->db->query("SELECT SUM(cc.item_cost*bfo.budgetfolderotcorder_quantity) 'total_cost'
                FROM budgetfolderotcorder bfo
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE i.item_supplier LIKE '%$otc_supplier->item_supplier%'
                AND bf.branch_id = $branch->branch_id
                AND bf.budgetfolder_budgetdate = '$month_name'")->row();

                if($otc){
                    $otc_total_cost = $otc->total_cost;
                }

                $otcs[] = $otc_total_cost;

            }

            $data['columns'][] = array(
                'branch_name' => $branch->branch_name,
                'original' => $budgetfolder ? $budgetfolder->original : NULL,
                'sales_forecast' => $budgetfolder ? $budgetfolder->sales_forecast : NULL,
                'average_sales_performance' => $budgetfolder ? $budgetfolder->average_sales_performance : NULL ,
                'beginning' => $budgetfolder ? $budgetfolder->beginning : NULL,
                'otc_beginning' => $budgetfolder ? $budgetfolder->otc_beginning : NULL,
                'material_max_allowance' => $budgetfolder ? $budgetfolder->material_max_allowance : NULL,
                'material_order_total' => $budgetfolder ? $budgetfolder->material_order_total : NULL,
                'material_over_under' => $budgetfolder ? $budgetfolder->material_over_under : NULL,
                'otc_max_allowance' => $budgetfolder ? $budgetfolder->otc_max_allowance : NULL,
                'otc_order_total' => $budgetfolder ? $budgetfolder->otc_order_total : NULL,
                'otc_over_under' => $budgetfolder ? $budgetfolder->otc_over_under : NULL,
                'material_cost' => $materials,
                'otc_cost'=> $otcs,
                'date' => $date_used_in_query_for_budgetstart,
                'last_month_start' => $last_month_start,
                'last_month_end' => $last_month_end,
                'last_month_sales' => $last_month_sales

            );

        }
        $data['branches'] = $branches;
        return $data;
    }
    
    public function fetch_folders($month, $batch_no){
        $batch_query = null;

        if($batch_no){
            $batch_query = 'WHERE branch_batchno = '.$batch_no;
        }

        $query = $this->db->query("SELECT DISTINCT branch_budgetfolder 
        FROM branch
        $batch_query
        ORDER BY branch_budgetfolder ASC")->result();
        
        $output = '<option value="">Select Budget Folder</option>';
        foreach ($query as $row) {
            $output .= '<option value = "'.$row->branch_budgetfolder.'">'.$row->branch_budgetfolder.'</option>';
        }
        return $output;

    }

    public function generate_budgetfolder_total($branch_id){

        $total_total_cost = 0;

        $data = $this->db->query("SELECT ROUND((c.currentinventory_quantity / i.item_uomsize) * c.item_cost, 2) 'total_cost'
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        WHERE c.branch_id = $branch_id")->result();

        foreach($data as $total_cost){
            if($total_cost->total_cost > 0){
                $total_total_cost += $total_cost->total_cost ;
            }
        }

        return $total_total_cost;
    }

    public function get_currentinventory(){
        $branch_id = $this->session->userdata('user')->branch_id;

        return $this->db->query("SELECT c.currentinventory_id, cat.category_name, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function update_currentinventory_quantity($ids){
        
        // Updates currentinventory

        for($i = 0; $i < count($ids); $i++){
            
            $currentinventory = $this->db->query("SELECT c.branch_id, i.item_id, i.item_uomsize, i.item_uom 
            FROM item i 
            LEFT JOIN currentinventory c ON i.item_id = c.item_id
            WHERE c.currentinventory_id = $ids[$i][0]")->row();

            $converted_quantity = $ids[$i][1] * $currentinventory->item_uomsize;

            if($ids[$i][1] != ''){

                if($currentinventory->currentinventory_quantity > $converted_quantity){
                    $this->db->where('currentinventory_id', $ids[$i][0]);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));
                }
                else{
                    $data = array(
                        'branch_id' => $currentinventory->branch_id,
                        'item_id' => $currentinventory->item_id,
                        'inventory_quantity' => $ids[$i][1],
                        'inventory_status' => 'PENDING'
                    );
    
                    $this->db->insert('inventoryaudit', $data);
                }
    
                // Updates or insert currentinventory
    
                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();
    
                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $ids[$i][1] * $currentinventory->item_uomsize,
                        // 'currentinventory_dateupdated' => date('Y-m-d')
                    ));
                }
                else{
                    $branch_id = $this->session->userdata('user')->branch_id;
    
                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'currentinventory_quantity' => $ids[$i][1] * $currentinventory->item_uomsize,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        // 'currentinventory_dateupdated' => date('Y-m-d')
                    );
    
                    $this->db->insert('currentinventory_copy', $data);
                }

            }
        }

    }

    public function submit_budgetfolder($ids, $dateupdated){
        $branch_id = $this->session->userdata('user')->branch_id;

        // Updates currentinventory

        for($i = 0; $i < count($ids); $i++){

            $id = $ids[$i]['id'];
            $value = floatval($ids[$i]['value']);
            
            $currentinventory = $this->db->query("SELECT c.branch_id, i.item_id, i.item_uomsize, i.item_uom, c.currentinventory_quantity 
            FROM item i 
            LEFT JOIN currentinventory c ON i.item_id = c.item_id
            WHERE c.currentinventory_id = $id")->row();

            $this->db->where('item_id', $currentinventory->item_id);
            $item = $this->db->get('item')->row();

            $converted_quantity = $value * $currentinventory->item_uomsize;

            if($ids[$i]['value'] != NULL){

                if($currentinventory->currentinventory_quantity < $converted_quantity){
                    $this->db->where('currentinventory_id', $id);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));
                }
                else{

                    $this->db->where('currentinventory_id', $id);
                    $this->db->update('currentinventory', array('currentinventory_quantity' => $converted_quantity));

                    $this->db->where('branch_id', $currentinventory->branch_id);
                    $this->db->where('item_id', $currentinventory->item_id);
                    $this->db->where('inventoryaudit_date', $dateupdated);
                    $inventoryaudit = $this->db->get('inventoryaudit')->row();
                    
                    if($inventoryaudit){

                        $data = array(
                            'branch_id' => $inventoryaudit->branch_id,
                            'item_id' => $inventoryaudit->item_id,
                            'inventoryaudit_quantity' => $currentinventory->currentinventory_quantity - ($value * $currentinventory->item_uomsize),
                            'inventoryaudit_date' => $dateupdated,
                            'inventoryaudit_status' => 'PENDING'
                        );

                        $this->db->where('inventoryaudit_id', $inventoryaudit->branch_id);
                        $this->db->update('inventoryaudit', $data);

                    }
                    else{
                        $data = array(
                            'branch_id' => $currentinventory->branch_id,
                            'item_id' => $currentinventory->item_id,
                            'inventoryaudit_quantity' => $currentinventory->currentinventory_quantity - ($value * $currentinventory->item_uomsize),
                            'inventoryaudit_date' => $dateupdated,
                            'inventoryaudit_status' => 'PENDING'
                        );
        
                        $this->db->insert('inventoryaudit', $data);
                    }

                }

                // Updates or insert currentinventory

                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $this->db->where('currentinventory_dateupdated', $dateupdated);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $converted_quantity
                    ));
                }
                else{
                    $branch_id = $this->session->userdata('user')->branch_id;

                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'item_cost' => $item->item_cost,
                        'currentinventory_quantity' => $converted_quantity,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        'currentinventory_dateupdated' => $dateupdated
                    );

                    $this->db->insert('currentinventory_copy', $data);
                }

            }
            else{

                $this->db->where('branch_id', $currentinventory->branch_id);
                $this->db->where('item_id', $currentinventory->item_id);
                $this->db->where('currentinventory_dateupdated', $dateupdated);
                $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

                if($currentinventory_copy){
                    $this->db->where('currentinventory_id', $currentinventory_copy->currentinventory_id);
                    $this->db->update('currentinventory_copy', array(
                        'currentinventory_quantity' => $currentinventory->currentinventory_quantity
                    ));
                }
                else{

                    $data = array(
                        'branch_id' => $branch_id,
                        'item_id' => $currentinventory->item_id,
                        'item_cost' => $item->item_cost,
                        'currentinventory_quantity' => $currentinventory->currentinventory_quantity,
                        'currentinventory_uom' => $currentinventory->item_uom,
                        'currentinventory_dateupdated' => $dateupdated
                    );

                    $this->db->insert('currentinventory_copy', $data);
                }

            }
        }
        
    }

    public function get_brands(){
        return $this->db->query("SELECT brand_id, brand_name FROM brand WHERE brand_id != 100007 ORDER BY brand_name ASC");
    }

    public function check_brand_budgetfolder_availability($brand_id, $month, $batch_no, $year){

        $branch_id = $this->session->userdata('user')->branch_id;

        $user_id = $this->session->userdata('user')->user_id;

        $wall_to_wall_year = $year;

        $am_query = null;

        if($branch_id == 200){
            $am_query = "AND b.branch_areamanager = $user_id";
        }

        $batch_query = null;

        if($batch_no){
            $batch_query = "AND b.branch_batchno = $batch_no";
        }

        $brand_exception_query = "AND b.brand_id NOT IN (100001)";

        if($branch_id == 1){
            $brand_exception_query = "";
        }

        $sales_forecast_query_date = $year."-$month-01";

        // $date_used_in_query_for_budgetstart =  $year.'-'.date('m', strtotime($month)).'-01';

        $budgetfolderstart_query = "AND (b.branch_budgetfolderstart <= '$sales_forecast_query_date' OR b.branch_budgetfolderstart IS NULL)";

        $month_and_year = date('F', strtotime($sales_forecast_query_date)).' '.$year;

        $branches = $this->db->query("SELECT b.branch_id, b.branch_name, br.brand_name, b.branch_company, b.branch_budgetfolder, b.branch_budgetfolderstart
        FROM branch b 
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        WHERE b.brand_id = $brand_id
        AND b.branch_status = 1
        $brand_exception_query
        $am_query
        $batch_query
        -- $budgetfolderstart_query
        ORDER BY b.branch_name ASC")->result();


        $months = [ 
            '01' => [ 
                1 => 11, 2 => 12,
            ],
            '02' => [ 
                1 => 12, 2 => '01',
            ],
            '03' => [ 
                1 => '01', 2 => '02',
            ],
            '04' => [ 
                1 => '02', 2 => '03',
            ],
            '05' => [ 
                1 => '03', 2 => '04',
            ],
            '06' => [ 
                1 => '04', 2 => '05',
            ],
            '07' => [ 
                1 => '05', 2 => '06',
            ],
            '08' => [ 
                1 => '06', 2 => '07',
            ],
            '09' => [ 
                1 => '07', 2 => '08',
            ],
            '10' => [ 
                1 => '08', 2 => '09',
            ],
            '11' => [ 
                1 => '09', 2 => '10',
            ],
            '12' => [ 
                1 => '10', 2 => '11',
            ],
        ];

        if($month == '01' || $month == '02' && $batch_no == '01'){
            $wall_to_wall_year = $wall_to_wall_year - 1;
        }

        $budgetfolder_wall_to_wall_date1 = $wall_to_wall_year.'-'.( $batch_no ? $months[$month][$batch_no] : $months[$month][1] ).'-15';
        $budgetfolder_wall_to_wall_date2 = $wall_to_wall_year.'-'.( $batch_no ? $months[$month][$batch_no] : $months[$month][1] ).'-31';
        $budgetfolder_wall_to_wall_date3 = $wall_to_wall_year.'-'.( $batch_no ? $months[$month][$batch_no] : $months[$month][1] ).'-30';
        $budgetfolder_wall_to_wall_date4 = $wall_to_wall_year.'-'.( $batch_no ? $months[$month][$batch_no] : $months[$month][1] ).'-29';
        $budgetfolder_wall_to_wall_date5 = $wall_to_wall_year.'-'.( $batch_no ? $months[$month][$batch_no] : $months[$month][1] ).'-28';

        $data = array();



        foreach($branches as $branch){

            $wall_to_wall_date = $this->db->query("SELECT currentinventory_dateupdated, currentinventory_status 
            FROM currentinventory_copy 
            WHERE branch_id = $branch->branch_id 
            AND currentinventory_dateupdated IN (
                '$budgetfolder_wall_to_wall_date1', '$budgetfolder_wall_to_wall_date2', '$budgetfolder_wall_to_wall_date3', '$budgetfolder_wall_to_wall_date4', '$budgetfolder_wall_to_wall_date5'
            )")->row();

            $currentinventory_status = '';

            if($wall_to_wall_date){

                $currentinventory_status = $wall_to_wall_date->currentinventory_status;
            }
            
            $wall_to_wall_status = '';

            if($currentinventory_status != 'DONE' && $wall_to_wall_date){
                $wall_to_wall_status = 'VIEW SUBMITTED';
            }
            else if( $currentinventory_status == 'DONE'){
                $wall_to_wall_status = 'DONE';
            }
            else{
                $wall_to_wall_status = 'PENDING';
            }


            $sales_forecast_date = $this->db->query("SELECT storetarget_month, storetarget_updatedsalesforecast FROM storetarget WHERE branch_id = $branch->branch_id AND storetarget_month = '$sales_forecast_query_date'")->row();

            $sales_forecast_month = '';
            $sales_forecast_value = 0;

            if($sales_forecast_date){
                $sales_forecast_month = $sales_forecast_date->storetarget_month != NULL ? date('m', strtotime($sales_forecast_date->storetarget_month)) : NULL;
                $sales_forecast_value = $sales_forecast_date->storetarget_updatedsalesforecast;
            }


            $sales_forecast_status = $sales_forecast_month == $month ? 'DONE' : 'PENDING';

            $this->db->where('branch_id', $branch->branch_id);
            $this->db->where('budgetfolder_budgetdate', $month_and_year);
            $budgetfolder = $this->db->get('budgetfolder')->row();

            $budgetfolder_status = 'NOT READY';

            $autoorder_status = $sales_forecast_status == 'DONE' && $wall_to_wall_status == 'DONE' ? 'READY FOR CREATION' : 'NOT READY';

            $budget_date = '';

            if($budgetfolder){
                $budgetfolder_status = $budgetfolder->budgetfolder_status;
                $autoorder_status = $budgetfolder->budgetfolder_autoorderstatus;
                $budget_date = $budgetfolder->budgetfolder_budgetdate;
            }

            $this->db->where('branch_id', $branch->branch_id);
            $this->db->where('storetarget_month', date('Y').'-'.$month.'-01');
            $storetarget = $this->db->get('storetarget')->row();

            $storetarget_original = null;
            $storetarget_updatedsalesforecast = null;

            if($storetarget){
                $storetarget_original = $storetarget->storetarget_original;
                $storetarget_updatedsalesforecast = $storetarget->storetarget_updatedsalesforecast;
            }


            $data[] = array(
                'branch_id' => $branch->branch_id,
                'month_and_year' => $month_and_year,
                'brand' => $branch->brand_name,
                'branch' => $branch->branch_name,
                'wall_to_wall_status' => $wall_to_wall_status,
                'sales_forecast_status' => $sales_forecast_status,
                'budgetfolder_status' => $budgetfolder_status,
                'wall_to_wall_date' => $wall_to_wall_date,
                'sales_forecast_date' => $sales_forecast_date,
                'budget_date' => $budget_date,
                'company' => $branch->branch_company,
                'folder' => $branch->branch_budgetfolder,
                'original_target' => $storetarget_original,
                'sales_forecast' => $storetarget_updatedsalesforecast,
                'budgetfolder_wall_to_wall_date3' => $budgetfolder_wall_to_wall_date3,
                'year' => $year,
                'sales_forecast_value' => $sales_forecast_value,
                'budgetfolder' => $budgetfolder,
                'budgetfolder_wall_to_wall_date1' => $budgetfolder_wall_to_wall_date1,
                'budgetfolder_wall_to_wall_date2' => $budgetfolder_wall_to_wall_date2,
                'budgetfolder_wall_to_wall_date3' => $budgetfolder_wall_to_wall_date3,
                'date' => $sales_forecast_query_date,
                'autoorder_status' => $autoorder_status

            );
        }

        return $data;

    }

    public function get_budgetfolder_inventory($branch_id, $wall_to_wall_date){

        if($wall_to_wall_date){

            return $this->db->query("SELECT cat.category_name, i.item_id, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON cat.category_id = i.category_id
            WHERE c.branch_id = $branch_id
            AND c.currentinventory_dateupdated = '$wall_to_wall_date'
            ORDER BY cat.category_name DESC, i.item_description ASC");

        }

        return $this->db->query("SELECT cat.category_name, i.item_id, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function get_budgetfolder_inventory_group($branch_id, $wall_to_wall_date){

        if($wall_to_wall_date){

            return $this->db->query("SELECT i.itemgroup, cat.category_name, i.item_id, i.item_description, i.item_shade, i.item_model, i.item_color, SUM(ROUND(c.currentinventory_quantity / i.item_uomsize, 2)) 'current_quantity', i.item_sapuom, i.item_cost, (
                CASE 
                    WHEN i.itemgroup IS NULL
                        THEN i.item_name
                    ELSE ig.itemgroup_name
                    END
                ) AS item_group
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN itemgroup ig ON ig.itemgroup_id = i.itemgroup 
            LEFT JOIN category cat ON cat.category_id = i.category_id
            WHERE c.branch_id = $branch_id
            AND c.currentinventory_dateupdated = '$wall_to_wall_date'
            GROUP BY item_group
            ORDER BY cat.category_name DESC, i.item_description ASC");

        }

        return $this->db->query("SELECT cat.category_name, i.item_id, i.item_description, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function get_budgetfolder_inventory_autoorder($branch_id, $wall_to_wall_date){

        if($wall_to_wall_date){

            return $this->db->query("SELECT cat.category_name, i.item_description, i.item_id, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON cat.category_id = i.category_id
            WHERE c.branch_id = $branch_id
            AND c.currentinventory_dateupdated = '$wall_to_wall_date'
            AND i.item_ordertype = 1
            ORDER BY cat.category_name DESC, i.item_description ASC");

        }

        return $this->db->query("SELECT cat.category_name, i.item_description, i.item_id, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost
        FROM currentinventory c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND i.item_ordertype = 1
        ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function get_budgetfolder_inventory_manualorder($branch_id, $wall_to_wall_date){

        if($wall_to_wall_date){

            return $this->db->query("SELECT cat.category_name, i.item_description, i.item_id, i.item_name, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost, i.item_group
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON cat.category_id = i.category_id
            WHERE c.branch_id = $branch_id
            AND c.currentinventory_dateupdated = '$wall_to_wall_date'
            AND i.item_ordertype = 2
            ORDER BY cat.category_name DESC, i.item_description ASC");

        }

        return $this->db->query("SELECT ig.item_group_name, cat.category_name, i.item_description, i.item_id, i.item_shade, i.item_model, i.item_color, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, i.item_cost, i.item_group
            FROM currentinventory_copy c
            LEFT JOIN item i ON i.item_id = c.item_id
            LEFT JOIN category cat ON cat.category_id = i.category_id
            LEFT JOIN item_group ig ON ig.item_group_id = i.item_group
            WHERE c.branch_id = $branch_id
            AND i.item_ordertype = 2
            GROUP BY ig.item_group_name
            ORDER BY cat.category_name DESC, i.item_description ASC");
    }

    public function get_budgetfolder_inventory_consumables($branch_id, $wall_to_wall_date){

        return $this->db->query("SELECT c.currentinventory_id, i.item_description, i.item_ordermodule, i.item_shade, i.item_color, i.item_model, c.item_cost, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND cat.category_name LIKE '%Consumables%'
        AND c.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC");

    }

    public function get_budgetfolder_inventory_consumables_manual($branch_id, $wall_to_wall_date){

        return $this->db->query("SELECT c.currentinventory_id, i.item_description, i.item_ordermodule, i.item_shade, i.item_color, i.item_model, i.item_supplier, c.item_cost, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND cat.category_name LIKE '%Consumables%'
        AND c.currentinventory_dateupdated = '$wall_to_wall_date'
        AND i.item_ordertype = 2
        AND i.item_ordermodule = 1
        ORDER BY i.item_description ASC");

    }

    public function get_budgetfolder_inventory_otcs($branch_id, $wall_to_wall_date){

        return $this->db->query("SELECT c.currentinventory_id, i.item_description, i.item_ordermodule, i.item_shade, i.item_color, i.item_model, c.item_cost, ROUND(c.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom
        FROM currentinventory_copy c
        LEFT JOIN item i ON i.item_id = c.item_id
        LEFT JOIN category cat ON cat.category_id = i.category_id
        WHERE c.branch_id = $branch_id
        AND cat.category_name LIKE '%OTC%'
        AND c.currentinventory_dateupdated = '$wall_to_wall_date'
        AND i.item_ordermodule = 1
        ORDER BY i.item_description ASC");

    }

    public function get_sales_forecast($branch_id, $wall_to_wall_date){
        $storetarget_month = date('Y', strtotime($wall_to_wall_date)).'-'.date('m', strtotime($wall_to_wall_date)).'-01';
        
        $this->db->where('branch_id', $branch_id);
        $branch = $this->db->get('branch')->row();

        // $storetarget_month = date('Y', strtotime($wall_to_wall_date)).'-'.(date('m', strtotime($wall_to_wall_date))+2).'-01';

        

        if($branch->branch_batchno == 2){
            $storetarget_month = strtotime("+1 months", strtotime($storetarget_month));
            $storetarget_month = strftime ( '%Y-%m-%d' , $storetarget_month );
        }else{
            $storetarget_month = strtotime("+2 months", strtotime($storetarget_month));
            $storetarget_month = strftime ( '%Y-%m-%d' , $storetarget_month );
        }

        
        $this->db->where('branch_id', $branch_id);
        $this->db->where('storetarget_month', date('Y-m-d', strtotime($storetarget_month)));
        return $this->db->get('storetarget')->row();
    }

    public function add_items_currentinventory(){
        $items = $this->db->query("SELECT * FROM item WHERE item_id IN (51918,51919,51920,51921)")->result();

        $branches = $this->db->query("SELECT * FROM branch WHERE branch_status = 1 AND brand_id = 100001 ORDER BY branch_name ASC")->result();

        foreach($branches as $branch){

            $curreninventory_items = $this->db->query("SELECT item_id FROM currentinventory WHERE branch_id = $branch->branch_id")->result();

            $currentinventory_ids  = array_column($curreninventory_items, 'item_id');

            foreach($items as $item){

                if(!in_array($item->item_id, $currentinventory_ids)){
                    $data = array(
                        'branch_id' => $branch->branch_id,
                        'item_id' => $item->item_id,
                        'currentinventory_quantity' => 0.00,
                        'currentinventory_uom' => $item->item_uom
                    );
    
                    $this->db->insert('currentinventory', $data);
                }
            }

        }

        return $this->db->affected_rows();
    }

    public function add_items_currentinventory_copy(){
        $items = $this->db->query("SELECT * FROM item WHERE item_id IN (50976, 50975)")->result();

        $vs_branches = $this->db->query("SELECT * FROM branch WHERE branch_status = 1 AND brand_id = 100002 AND branch_batchno = 2 ORDER BY branch_name ASC")->result();

        foreach($vs_branches as $vs_branch){

            foreach($items as $item){

                $data = array(
                    'branch_id' => $vs_branch->branch_id,
                    'item_id' => $item->item_id,
                    'item_cost' => $item->item_cost,
                    'currentinventory_quantity' => 0.00,
                    'currentinventory_uom' => $item->item_sapuom,
                    'currentinventory_dateupdated' => '2020-02-15',
                    'currentinventory_status' => 'DONE'
                );

                $this->db->insert('currentinventory_copy', $data);

            }

        }

        return $this->db->affected_rows();
    }


    public function confirm_budgetfolder_inventory($branch_id, $wall_to_wall_date){
        $this->db->where('branch_id', $branch_id);
        $this->db->where('currentinventory_dateupdated', $wall_to_wall_date);
        $this->db->update('currentinventory_copy', array('currentinventory_status' => 'DONE'));
    }

    public function check_encoding_availability($date_as_of){
        $branch_id = $this->session->userdata('user')->branch_id;

        $this->db->where('branch_id', $branch_id);
        $this->db->where('currentinventory_dateupdated', $date_as_of);
        $currentinventory_copy = $this->db->get('currentinventory_copy')->row();

        if($currentinventory_copy){

            if(!$currentinventory_copy->currentinventory_status){
                return TRUE;
            }

            else if($currentinventory_copy->currentinventory_status == "DONE"){
        
                // if($currentinventory_copy->currentinventory_status == "DONE"){
                //     return FALSE;
                // }

                // else{
                    return FALSE;
                // }

            }
            else{
                return TRUE;
            }

            return json_encode($currentinventory_copy);

        }

        return TRUE;
    }

    public function get_branch($branch_id){
        $this->db->where('branch_id', $branch_id);
        return $this->db->get('branch');
    }

    public function create_budgetfolder($budgetfolder_data, $consumables, $otcs){
        $this->db->where('branch_id', $budgetfolder_data['branch_id']);
        $this->db->where('budgetfolder_budgetdate', $budgetfolder_data['budgetfolder_budgetdate']);
        $budgetfolder = $this->db->get('budgetfolder')->row();

            // $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
            // $this->db->update('budgetfolder', $budgetfolder_data);

            if(!empty($consumables)){
                // $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
                // $this->db->delete('budgetfoldermaterialorder');

                for($i = 0; $i < count($consumables); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder->budgetfolder_id,
                        'currentinventory_id' => $consumables[$i]['currentinventory_id'],
                        'budgetfoldermaterialorder_quantity' => $consumables[$i]['order_quantity'],
                        'budgetfoldermaterialorder_cost' => $consumables[$i]['item_cost'],
                        'item_supplier' => $consumables[$i]['item_supplier'],
                        'item_ordertype' => 2
                    );
        
                    $this->db->insert('budgetfoldermaterialorder', $data);
                }
            }
    
            if(!empty($otcs)){
                // $this->db->where('budgetfolder_id', $budgetfolder->budgetfolder_id);
                // $this->db->delete('budgetfolderotcorder');

                for($i = 0; $i < count($otcs); $i++){
                    $data = array(
                        'budgetfolder_id' => $budgetfolder->budgetfolder_id,
                        'currentinventory_id' => $otcs[$i]['currentinventory_id'],
                        'budgetfolderotcorder_quantity' => $otcs[$i]['order_quantity'],
                        'item_ordertype' => 2

                    );
    
                    $this->db->insert('budgetfolderotcorder', $data);
                }
            }

 
        $this->update_budgetfolderstatus($budgetfolder_data['branch_id'], $budgetfolder_data['budgetfolder_budgetdate'], $budgetfolder_data['budgetfolder_materialordertotal']);

    }

    public function update_budgetfolderstatus($branch_id, $budget_date, $total_material){
        $this->db->set('budgetfolder_status', 'VIEW CREATED');
        $this->db->set('budgetfolder_materialordertotal', $total_material);
        $this->db->where('branch_id', $branch_id);
        $this->db->where('budgetfolder_budgetdate', $budget_date);
        $this->db->update('budgetfolder');

    }

    public function view_submitted_budgetfolder_consumables($branch_id, $budget_date, $wall_to_wall_date){
        $this->db->where('branch_id', $branch_id);
        $branch = $this->db->get('branch')->row();

        // $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime($budget_date))-2).'-'.(date('t', strtotime($budget_date))) ));

        // if($branch->branch_batchno == 2){
        //     $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime($budget_date))-1).'-15'));
        // }
        
        if($budget_date == 'December 2019'){
            $wall_to_wall_date = '2019-10-31';
        }

        $items = $this->db->query("SELECT i.item_description, i.item_shade, i.item_color, i.item_model, cc.item_cost, bfm.budgetfoldermaterialorder_cost, ROUND(cc.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, bfm.budgetfoldermaterialorder_quantity, cc.currentinventory_id
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id        
        WHERE bf.budgetfolder_budgetdate = '$budget_date'
        AND bf.branch_id = $branch_id
        AND cc.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC")->result();

        $this->db->where('branch_id', $branch_id);
        $this->db->where('budgetfolder_budgetdate', $budget_date);
        $budget_folder = $this->db->get('budgetfolder')->row();

        $data = array(
            'items' => $items,
            'beginning_total' => $budget_folder->budgetfolder_materialbeginningtotal,
            'order_total' => $budget_folder->budgetfolder_materialordertotal,
            'max_allowance' => $budget_folder->budgetfolder_materialmaxallowance,
            'auto_order' => $budget_folder->budgetfolder_autoorder,
            'manual_budget' => $budget_folder->budgetfolder_manualbudget,
            'non_consumable' => $budget_folder->budgetfolder_nonconsumable
        );

        return $data;
    }

    public function view_submitted_budgetfolder_otcs($branch_id, $budget_date, $wall_to_wall_date){
        $this->db->where('branch_id', $branch_id);
        $branch = $this->db->get('branch')->row();

        // $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime($budget_date))-2).'-'.(date('t', strtotime($budget_date))) ));

        // if($branch->branch_batchno == 2){
        //     $wall_to_wall_date = date('Y-m-d', strtotime('2019-'.(date('m', strtotime($budget_date))-1).'-15'));
        // }

        if($budget_date == 'December 2019'){
            $wall_to_wall_date = '2019-10-31';
        }

        $items = $this->db->query("SELECT i.item_description, i.item_shade, i.item_color, i.item_model, cc.item_cost, bfo.budgetfolderotcorder_cost, ROUND(cc.currentinventory_quantity / i.item_uomsize, 2) 'current_quantity', i.item_sapuom, bfo.budgetfolderotcorder_quantity
        FROM budgetfolderotcorder bfo
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id        
        WHERE bf.budgetfolder_budgetdate = '$budget_date'
        AND bf.branch_id = $branch_id
        AND cc.currentinventory_dateupdated = '$wall_to_wall_date'
        ORDER BY i.item_description ASC")->result();

        $this->db->where('branch_id', $branch_id);
        $this->db->where('budgetfolder_budgetdate', $budget_date);
        $budget_folder = $this->db->get('budgetfolder')->row();

        $data = array(
            'items' => $items,
            'beginning_total' => $budget_folder->budgetfolder_otcbeginningtotal,
            'order_total' => $budget_folder->budgetfolder_otcordertotal,
            'max_allowance' => $budget_folder->budgetfolder_otcmaxallowance,
            'budget_date' => $budget_date,
            'branch_id' => $branch_id,
            'wall_to_wall_date' => $wall_to_wall_date
        );

        return $data;
    }

    public function get_budgetfolders($month, $batch_no){
        $batch_query = null;

        if($batch_no){
            $batch_query = 'WHERE branch_batchno = '.$batch_no;
        }
        
        return $this->db->query("SELECT DISTINCT branch_budgetfolder 
        FROM branch 
        $batch_query
        ORDER BY branch_budgetfolder ASC"); 
    }
    
    public function view_budgetfolder_summary($folder, $month, $batch, $year){

        $storetarget_month = $year.'-'.$month.'-01';
        $year = date('Y', strtotime($storetarget_month));
        $storetarget_month = $year.'-'.$month.'-01';
        $month_name = date('F', strtotime($storetarget_month)).' '.$year;

        $last_month_start = strtotime("-2 months", strtotime($storetarget_month));
        $last_month_start = strftime ( '%Y-%m-01' , $last_month_start );

        $last_month_end = date('Y-m-t', strtotime($last_month_start));

        $batch_query = null;

        if($batch && $batch != 'undefined'){
            $batch_query = 'AND branch_batchno = '.$batch;
        }

        $budgetfolderstart_query = "AND (b.branch_budgetfolderstart <= '$storetarget_month' OR b.branch_budgetfolderstart IS NULL)";

        $branches = $this->db->query("SELECT b.*, br.brand_name 
        FROM branch b
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        WHERE b.branch_budgetfolder = '$folder' 
        $batch_query
        $budgetfolderstart_query
        AND branch_status = 1
        ORDER BY branch_name ASC")->result();

        $material_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folder' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%Consumables%'
        ORDER BY i.item_supplier ASC")->result();

        $otc_suppliers = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id
        LEFT JOIN branch b ON b.branch_id = cc.branch_id
        WHERE b.branch_budgetfolder = '$folder' AND i.item_ordermodule = 1
        AND c.category_name LIKE '%OTC%'
        ORDER BY i.item_supplier ASC")->result();

       

        $data['columns'] = array();

        foreach($branches as $branch){

            $budgetfolder = $this->db->query("SELECT br.brand_name, st.storetarget_original 'original', st.storetarget_updatedsalesforecast 'sales_forecast', st.storetarget_averagesalesperformance 'average_sales_performance', st.storetarget_updatedsalesforecast*0.94 'ninety_four_percent', (st.storetarget_updatedsalesforecast*0.94)*0.05 'six_percent', bf.budgetfolder_materialbeginningtotal 'beginning', bf.budgetfolder_materialmaxallowance 'material_max_allowance', bf.budgetfolder_materialordertotal 'material_order_total', bf.budgetfolder_materialmaxallowance-bf.budgetfolder_materialordertotal 'material_over_under', bf.budgetfolder_otcbeginningtotal 'otc_beginning', bf.budgetfolder_otcmaxallowance 'otc_max_allowance', bf.budgetfolder_otcordertotal 'otc_order_total', bf.budgetfolder_otcordertotal-bf.budgetfolder_otcmaxallowance 'otc_over_under'
            FROM storetarget st
            LEFT JOIN budgetfolder bf ON bf.branch_id = st.branch_id
            LEFT JOIN branch b ON b.branch_id = st.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE st.storetarget_month = '$storetarget_month'
            AND bf.branch_id = $branch->branch_id
            AND bf.budgetfolder_budgetdate = '$month_name'")->row();

            $last_month_sales = $this->db->query("
                SELECT SUM(t.transaction_totalsales) as sales
                from transaction t
                WHERE t.branch_id = $branch->branch_id 
                AND t.transaction_status = 1 AND
                t.transaction_date BETWEEN '$last_month_start' and '$last_month_end'
                ")->row()->sales;

            $materials = array();

            foreach($material_suppliers as $material_supplier){

                $material_total_cost = 0;

                $material = $this->db->query("SELECT COALESCE(SUM(bfm.budgetfoldermaterialorder_quantity * cc.item_cost), 0) 'total_cost'
                    FROM budgetfoldermaterialorder bfm
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                    WHERE i.item_supplier LIKE '%$material_supplier->item_supplier%'
                    AND bf.branch_id = $branch->branch_id
                    AND bf.budgetfolder_budgetdate = '$month_name'")->row();


                    if($material){
                        $material_total_cost = $material->total_cost;
                    }

                    $materials[] = $material_total_cost;

            }

            $otcs = array();

            foreach($otc_suppliers as $otc_supplier){
                
                $otc_total_cost = 0;

                $otc = $this->db->query("SELECT SUM(cc.item_cost*bfo.budgetfolderotcorder_quantity) 'total_cost'
                FROM budgetfolderotcorder bfo
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                LEFT JOIN branch b ON b.branch_id = cc.branch_id
                WHERE i.item_supplier LIKE '%$otc_supplier->item_supplier%'
                AND bf.branch_id = $branch->branch_id
                AND bf.budgetfolder_budgetdate = '$month_name'
                AND b.branch_budgetfolder = '$folder'
                ORDER BY i.item_supplier ASC")->row();

                if($otc){
                    $otc_total_cost = $otc->total_cost;
                }

                $otcs[] = $otc_total_cost;

            }

            $data['columns'][] = array(
                'branch_name' => $branch->branch_name,
                'original' => $budgetfolder ? $budgetfolder->original : NULL,
                'sales_forecast' => $budgetfolder ? $budgetfolder->sales_forecast : NULL,
                'average_sales_performance' => $budgetfolder ? $budgetfolder->average_sales_performance : NULL ,
                'beginning' => $budgetfolder ? $budgetfolder->beginning : NULL,
                'otc_beginning' => $budgetfolder ? $budgetfolder->otc_beginning : NULL,
                'material_max_allowance' => $budgetfolder ? $budgetfolder->material_max_allowance : NULL,
                'material_order_total' => $budgetfolder ? $budgetfolder->material_order_total : NULL,
                'material_over_under' => $budgetfolder ? $budgetfolder->material_over_under : NULL,
                'otc_max_allowance' => $budgetfolder ? $budgetfolder->otc_max_allowance : NULL,
                'otc_order_total' => $budgetfolder ? $budgetfolder->otc_order_total : NULL,
                'otc_over_under' => $budgetfolder ? $budgetfolder->otc_over_under : NULL,
                'material_cost' => $materials,
                'material_suppliers' => $material_suppliers,
                'otc_cost' => $otcs,
                'last_month_sales' => $last_month_sales,
                'last_month_start' => $last_month_start,
                'last_month_end' => $last_month_end

            
            );

            $data['side_infos']['brand_name'] = $branch->brand_name;

            $data['side_infos']['branch_company'] = $branch->branch_company;

            $data['side_infos']['budgetfolder_month'] = $month_name;

            $data['side_infos']['budgetfolder'] = $folder;

            $data['side_infos']['material_suppliers'] = $material_suppliers;

            $data['side_infos']['otc_suppliers'] = $otc_suppliers;
            $data['side_infos']['storetarget_month'] = $storetarget_month;


        }

        return $data;
    }

    public function view_budgetfolder_orders($folder, $month, $batch){

        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y');

        $data = array();

        $batch_query = null;

        if($batch){
            $batch_query = 'AND branch_batchno = '.$batch;
        }

        $branches = $this->db->query("SELECT DISTINCT branch.*, brand_name 
        FROM branch 
        LEFT JOIN brand ON brand.brand_id = branch.brand_id
        WHERE branch_budgetfolder = '$folder' $batch_query
        AND branch_status = 1
        AND branch.branch_id IN (
            SELECT DISTINCT b.branch_id
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            ORDER BY b.branch_name ASC
        )
        OR branch.branch_id IN (
            SELECT DISTINCT b.branch_id
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            ORDER BY b.branch_name ASC
        )
        ORDER BY branch_name ASC")->result();


        $material_suppliers_result = $this->db->query("SELECT DISTINCT item_supplier
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY item_supplier
        ORDER BY item_supplier ASC,  branch_name ASC")->result();

        $material_suppliers = array_column($material_suppliers_result, 'item_supplier');



        $otc_suppliers_result = $this->db->query("SELECT DISTINCT item_supplier
        FROM budgetfolderotcorder bfo
        LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY item_supplier
        ORDER BY item_supplier ASC,  branch_name ASC")->result();

        $otc_suppliers = [];

        foreach($otc_suppliers_result as $value){
            array_push($otc_suppliers, $value->item_supplier);
        }

        $branch_ids = array_column($branches, 'branch_id');

 
        $branches = array_column($branches, 'branch_name');


        $materials = $this->db->query("SELECT DISTINCT i.item_id, i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE i.item_supplier IN ('".implode("','",$material_suppliers)."')
        AND b.branch_id IN ('".implode("','",$branch_ids)."')
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY i.item_id
        ORDER BY i.item_supplier ASC, i.item_name ASC, i.item_shade ASC, i.item_color ASC, i.item_model ASC, b.branch_name ASC")->result();

        $otcs = $this->db->query("SELECT DISTINCT i.item_id, i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom
        FROM budgetfolderotcorder bfo
        LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE i.item_supplier IN ('".implode("','",$otc_suppliers)."')
        AND b.branch_id IN ('".implode("','",$branch_ids)."')
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY i.item_id
        ORDER BY i.item_supplier ASC, i.item_name ASC, i.item_shade ASC, i.item_color ASC, i.item_model ASC, b.branch_name ASC")->result();

        foreach($materials as $material){

            foreach($branch_ids as $value){

                $data['columns']['materials'][] = $this->db->query("SELECT b.branch_name, i.item_name, i.item_supplier, bfm.budgetfoldermaterialorder_quantity, bfm.budgetfoldermaterialorder_quantity * cc.item_cost 'amount'
                FROM budgetfoldermaterialorder bfm
                LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
                LEFT JOIN branch b ON b.branch_id = bf.branch_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE b.branch_id = $value
                AND bf.budgetfolder_budgetdate = '$month_and_year'
                AND i.item_id = $material->item_id")->result();

            }

        }

        foreach($otcs as $otc){

            foreach($branch_ids as $value){

                $data['columns']['otcs'][] = $this->db->query("SELECT b.branch_name, i.item_name, i.item_supplier, bfo.budgetfolderotcorder_quantity, bfo.budgetfolderotcorder_quantity * cc.item_cost 'amount'
                FROM budgetfolderotcorder bfo
                LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
                LEFT JOIN branch b ON b.branch_id = bf.branch_id
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE b.branch_id = $value
                AND bf.budgetfolder_budgetdate = '$month_and_year'
                AND i.item_id = $otc->item_id")->result();

            }

        }

        $data['folder'] = $folder;
        $data['month_and_year'] = $month_and_year;
        $data['materials'] = $materials;
        $data['otcs'] = $otcs;
        $data['material_suppliers'] = $material_suppliers;
        $data['otc_suppliers'] = $otc_suppliers;
        $data['branches'] = $branches;

        return $data;
    }

    public function view_budgetfolder_inventory($folder, $month, $batch, $year){

        $month_and_year = date('F', strtotime($year.'-'.$month)).' '.$year;

        $wall_to_wall_date = date('Y-m-d', strtotime($year.'-'.(date('m', strtotime($month_and_year))-2).'-'.(date('t', strtotime($month_and_year))) ));

        if($batch == 1 && $month_and_year == 'January 2020'){
            $wall_to_wall_date = '2019-11-30';
        }

        if($batch == 2 || !$batch){
            $wall_to_wall_date = date('Y-m-d', strtotime($year.'-'.(date('m', strtotime($month_and_year))-1).'-15'));
        }

        if($month == 12 && !$batch){
            $wall_to_wall_date = '2019-10-31';
        }

        $data = array();

        $batch_query = null;

        if($batch && $batch != 'undefined'){
            $batch_query = 'AND branch_batchno = '.$batch;
        }

        $branches = $this->db->query("SELECT branch_id, branch_name
        FROM branch
        WHERE branch_budgetfolder = '$folder'
        $batch_query
        ORDER by branch_name ASC")->result();


        $material_suppliers_result = $this->db->query("SELECT DISTINCT item_supplier
        FROM budgetfoldermaterialorder bfm
        LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
        LEFT JOIN branch b ON b.branch_id = bf.branch_id
        LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
        LEFT JOIN item i ON i.item_id = cc.item_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY item_supplier
        ORDER BY item_supplier ASC,  branch_name ASC")->result();

        $material_suppliers = array_column($material_suppliers_result, 'item_supplier');

        $branch_ids = array_column($branches, 'branch_id');

        $branches_array = $branches;

        $branches = array_column($branches_array, 'branch_name');

        $materials = $this->db->query("SELECT DISTINCT i.item_id, i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom
        from currentinventory_copy cc
        LEFT JOIN item i on cc.item_id = i.item_id
        LEFT JOIN branch br on br.branch_id = cc.branch_id
        LEFT JOIN budgetfolder bf on bf.branch_id = br.branch_id
        WHERE br.branch_id IN ('".implode("','",$branch_ids)."')
        AND cc.currentinventory_quantity > 0
        AND bf.budgetfolder_budgetdate = '$month_and_year'
        GROUP BY i.item_name, i.item_shade, i.item_color, i.item_model
        ORDER BY i.item_name ASC, i.item_shade ASC, i.item_color ASC, i.item_model ASC")->result();

        foreach($materials as $material){

            foreach($branches_array as $value){

                $data['columns']['materials'][] = $this->db->query("SELECT br.branch_name, i.item_name, cc.currentinventory_quantity, i.item_uom, i.item_uomsize, FORMAT((cc.currentinventory_quantity / i.item_uomsize),2) as quantity, cc.item_cost, ROUND((cc.currentinventory_quantity / i.item_uomsize * cc.item_cost),2) AS amount
                    FROM currentinventory_copy cc
                    LEFT JOIN item i on cc.item_id = i.item_id
                    LEFT JOIN branch br on br.branch_id = cc.branch_id
                    LEFT JOIN budgetfolder bf on bf.branch_id = br.branch_id
                    WHERE br.branch_id = $value->branch_id
                    AND cc.currentinventory_quantity > 0
                    AND bf.budgetfolder_budgetdate = '$month_and_year'
                    AND i.item_id = $material->item_id
                    AND cc.currentinventory_dateupdated = '$wall_to_wall_date'
                    GROUP BY i.item_name  
                    ORDER BY `i`.`item_name` ASC")->result();
            }
        }

        $data['folder'] = $folder;
        $data['month_and_year'] = $month_and_year;
        $data['branches'] = $branches;
        $data['materials'] = $materials;
        $data['wall_to_wall_date'] = $wall_to_wall_date;
        $data['material_suppliers'] = $material_suppliers;
        // $data['branches_array'] = $branches_array;
        $data['branch_ids'] = $branch_ids;
        return $data;
    }

    public function generate_excel($folder, $supplier, $budgetfolder_month, $batch){

        $data = array();

        $batch_query = null;

        if($batch && $batch != 'undefined'){
            $batch_query = 'AND branch_batchno = '.$batch;
        }

        $branches = $this->db->query("SELECT DISTINCT branch_id, branch_name, branch_company
        FROM branch
        WHERE branch_budgetfolder = '$folder'
        $batch_query
        ORDER by branch_name ASC")->result();

        $branch_ids = [];

        foreach($branches as $branch){
            array_push($branch_ids, $branch->branch_id);
        }

        $category = $this->db->query("SELECT DISTINCT cat.category_name, br.branch_budgetfolder
        FROM item i
        LEFT JOIN category cat ON cat.category_id = i.category_id
        LEFT JOIN branch br ON br.brand_id = cat.brand_id
        WHERE i.item_supplier = '$supplier'
        AND br.branch_budgetfolder = '$folder'")->row();

        $items = null;

        $item_ids = [];

        if(strpos($category->category_name, 'Consumables') == TRUE){
            $items = $this->db->query("SELECT DISTINCT i.item_id, i.item_name, i.item_color, i.item_shade, i.item_model, cc.item_cost, i.item_sapuom
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
            LEFT JOIN branch br ON br.branch_id = bf.branch_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
            AND i.item_supplier LIKE '%$supplier%'
            AND br.branch_budgetfolder = '$folder'
            ORDER BY i.item_name, i.item_color, i.item_shade, i.item_model")->result();

            foreach($items as $key => $item){

                foreach($branches as $branch){
                    $row = $this->db->query("SELECT br.branch_name, bfm.budgetfoldermaterialorder_quantity, bfm.budgetfoldermaterialorder_quantity * cc.item_cost 'total_cost'
                    FROM budgetfoldermaterialorder bfm
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                    LEFT JOIN branch br ON br.branch_id = bf.branch_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
                    AND i.item_id = $item->item_id
                    AND bf.branch_id = $branch->branch_id")->row();

                    if($row){
                        $data['rows'][] = array(
                            0 => $row->branch_name,
                            1 => $row->budgetfoldermaterialorder_quantity,
                            2 => $row->total_cost
                        );
                    }
                    else{
                        $data['rows'][] = array(
                            0 => ' ',
                            1 => ' ',
                            2 => ' '
                        );
                    }

                }

                $item_ids[] = $item->item_id;

            }

            foreach($branches as $branch){
                $row = $this->db->query("SELECT SUM(bfm.budgetfoldermaterialorder_quantity * cc.item_cost) 'total_cost'
                FROM budgetfoldermaterialorder bfm
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                LEFT JOIN branch br ON br.branch_id = bf.branch_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
                AND i.item_id IN ('".implode("','",$item_ids)."')
                AND bf.branch_id = $branch->branch_id")->row();

                if($row){
                    $data['total_costs'][] = array(
                        0 => $row->total_cost,
                    );
                }
                else{
                    $data['total_costs'][] = array(
                        0 => ' ',
                    );
                }

            }
        } 
        else{
            $items = $this->db->query("SELECT DISTINCT i.item_id, i.item_name, i.item_color, i.item_shade, i.item_model, cc.item_cost, i.item_sapuom
            FROM budgetfolderotcorder bfo
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
            LEFT JOIN branch br ON br.branch_id = bf.branch_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
            AND i.item_supplier LIKE '%$supplier%'
            AND br.branch_budgetfolder = '$folder'
            ORDER BY i.item_name, i.item_color, i.item_shade, i.item_model")->result();

            foreach($items as $key => $item){

                foreach($branches as $branch){
                    $row = $this->db->query("SELECT br.branch_name, bfo.budgetfolderotcorder_quantity, bfo.budgetfolderotcorder_quantity * cc.item_cost 'total_cost'
                    FROM budgetfolderotcorder bfo
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                    LEFT JOIN branch br ON br.branch_id = bf.branch_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
                    AND i.item_id = $item->item_id
                    AND bf.branch_id = $branch->branch_id")->row();

                    if($row){
                        $data['rows'][] = array(
                            0 => $row->branch_name,
                            1 => $row->budgetfolderotcorder_quantity,
                            2 => $row->total_cost
                        );
                    }
                    else{
                        $data['rows'][] = array(
                            0 => ' ',
                            1 => ' ',
                            2 => ' '
                        );
                    }
                }

                $item_ids[] = $item->item_id;

            }

            foreach($branches as $branch){
                $row = $this->db->query("SELECT SUM(bfo.budgetfolderotcorder_quantity * cc.item_cost) 'total_cost'
                FROM budgetfolderotcorder bfo
                LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                LEFT JOIN branch br ON br.branch_id = bf.branch_id
                LEFT JOIN item i ON i.item_id = cc.item_id
                WHERE bf.budgetfolder_budgetdate = '$budgetfolder_month'
                AND i.item_id IN ('".implode("','",$item_ids)."')
                AND bf.branch_id = $branch->branch_id")->row();

                if($row){
                    $data['total_costs'][] = array(
                        0 => $row->total_cost,
                    );
                }
                else{
                    $data['total_costs'][] = array(
                        0 => ' ',
                    );
                }

            }
        }


        $data['branches'] = $branches;
        $data['items'] = $items;
        $data['supplier'] = $supplier;

        return $data;
    }

    public function get_folder_names(){
        return $this->db->query("SELECT DISTINCT branch_budgetfolder 
        FROM branch 
        WHERE branch_budgetfolder IS NOT NULL
        ORDER BY branch_budgetfolder ASC");
    }

    public function get_budgetfolder_sc($month, $folder, $item_supplier_name, $category_name){
        
        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y');
        
        if(strpos($category_name, 'Consumables')){
            $branches = $this->db->query("SELECT DISTINCT b.branch_name, cat.category_name
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%'")->result();

            $materialorders = $this->db->query("SELECT i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom, b.branch_name, bfm.budgetfoldermaterialorder_quantity
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%'")->result();

            return array(
                'branches' => $branches,
                'materialorders' =>$materialorders,
                'month_and_year' => $month_and_year,
                'item_supplier' => $item_supplier_name
            );


        }
        else{
            $branches = $this->db->query("SELECT DISTINCT b.branch_name, cat.category_name
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%'")->result();

            $otcorders = $this->db->query("SELECT i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, cc.item_cost, i.item_sapuom, b.branch_name, bfo.budgetfolderotcorder_quantity
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%';")->result();

            return array(
                'branches' => $branches,
                'otcorders' => $otcorders,
                'month_and_year' => $month_and_year
            );
        }
        
    }

    public function get_folder_costs($month, $batch, $year){

        $month_and_year = date('F', strtotime($year.'-'.$month)).' '.$year;

        $data = array();

        $batch_query = null;

        if($batch){
            $batch_query = 'AND branch_batchno = '.$batch;
        }

        $folders = array();

        $data['folders'] = $this->db->query("SELECT DISTINCT branch_budgetfolder, brand_id
        FROM branch 
        WHERE branch_budgetfolder IS NOT NULL
        $batch_query
        ORDER BY branch_budgetfolder ASC")->result();

        foreach($data['folders'] as $value){
            array_push($folders, $value->branch_budgetfolder);
        }

        $data['suppliers'] = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category cat ON cat.category_id = i.category_id
        LEFT JOIN brand br ON br.brand_id = cat.brand_id
        LEFT JOIN branch b ON b.brand_id = br.brand_id
        WHERE b.branch_budgetfolder IN ('".implode("','", $folders)."')
        AND i.item_supplier IS NOT NULL
        ORDER BY CASE WHEN i.item_supplier LIKE '%OTC%' THEN 2 ELSE 1 END, i.item_supplier ASC")->result();

        $total = 0;

        foreach($data['suppliers'] as $supplier){

            foreach($data['folders'] as $folder){

                $category_type = $this->db->query("SELECT DISTINCT cat.category_name, br.branch_budgetfolder
                FROM item i
                LEFT JOIN category cat ON cat.category_id = i.category_id
                LEFT JOIN branch br ON br.brand_id = cat.brand_id
                WHERE i.item_supplier = '$supplier->item_supplier'
                AND br.branch_budgetfolder = '$folder->branch_budgetfolder'")->row();

                $category = null;

                if($category_type){
                    $category = $category_type->category_name;
                }

                // $category_type = $this->db->query("SELECT DISTINCT category_name
                // FROM category cat
                // LEFT JOIN item i ON i.category_id = cat.category_id
                // WHERE i.item_supplier LIKE '%$supplier->item_supplier%'")->row()->category_name;

                $order = null;
                $total_cost = null;

                if(strpos($category, 'Consumables')){

                    $order = $this->db->query("SELECT COALESCE(SUM(bfm.budgetfoldermaterialorder_quantity * i.item_cost), 0) 'total_cost'
                    FROM budgetfoldermaterialorder bfm
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                    LEFT JOIN branch b ON b.branch_id = bf.branch_id
                    WHERE b.branch_budgetfolder LIKE '%$folder->branch_budgetfolder%'
                    AND bf.budgetfolder_budgetdate = '$month_and_year'
                    AND i.item_supplier LIKE '%$supplier->item_supplier%'")->row();

                    if($order){
                        $total_cost = $order->total_cost;
                    }
    
                    $data['rows'][] = array(
                        'total_cost' => $total_cost,
                        'folder' => $folder->branch_budgetfolder,
                        'item_supplier' => $supplier->item_supplier,
                        'category' => $category,
                        'folder' => $folder->branch_budgetfolder,
                        'month_and_year' => $month_and_year,
                    );

                }
                else{

                    $order = $this->db->query("SELECT COALESCE(SUM(bfo.budgetfolderotcorder_quantity * cc.item_cost), 0) 'total_cost'
                    FROM budgetfolderotcorder bfo
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    LEFT JOIN branch b ON b.branch_id = bf.branch_id
                    WHERE b.branch_budgetfolder LIKE '%$folder->branch_budgetfolder%'
                    AND bf.budgetfolder_budgetdate = '$month_and_year'
                    AND i.item_supplier LIKE '%$supplier->item_supplier%'")->row();
    
                    if($order){
                        $total_cost = $order->total_cost;
                    }
    
                    $data['rows'][] = array(
                        'total_cost' => $total_cost,
                        'folder' => $folder->branch_budgetfolder,
                        'item_supplier' => $supplier->item_supplier,
                        'category' => $category,
                        'folder' => $folder->branch_budgetfolder,
                        'month_and_year' => $month_and_year,
                    );
                }
                $total = $total + $total_cost;
            }

        }

        $data['total'] = $total;
        $data['category_type'] = $category_type;

        return $data;
    }

    public function submit_forecast($data){

        return $this->db->insert('budgetforecast', $data);

    }

    public function get_categoryforecast($branch_id, $budgetforecast_month){

        $brand = $this->db->query("
            SELECT brand_id
            FROM branch 
            WHERE branch_id = $branch_id")->row();

        $data = $this->db->query("
            SELECT c.category_name, bfc.budgetforecast_month, bfc.percentage, ps.productservice_name, ps.productservice_amount, ps.productservice_id
            FROM budgetforecast bfc
            LEFT JOIN productservice ps ON ps.productservice_id = bfc.productservice_id
            LEFT JOIN branch b ON b.brand_id = bfc.brand_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE bfc.brand_id = $brand->brand_id
            AND bfc.budgetforecast_month = '$budgetforecast_month'
            GROUP BY bfc.productservice_id
            ORDER BY ps.productservice_name
            ")->result();


        foreach ($data as $key) {
            if ($key->productservice_id == NULL) {
                $otc = $this->db->query("
                    SELECT *
                    FROM category
                    WHERE brand_id = $brand->brand_id
                    AND category_name LIKE '%OTC%'"
                )->row();
                $key->category_name = $otc->category_name;
                $key->productservice_name = 'OTC';
                $key->productservice_amount = 'N/A';

            }
        }

        return $data;

    }

    public function get_categoryforecast_testprint($brand_id, $budgetforecast_month){

 
        $data = $this->db->query("
            SELECT c.category_name, bfc.budgetforecast_month, bfc.percentage, ps.productservice_name, ps.productservice_amount, ps.productservice_id
            FROM budgetforecast bfc
            LEFT JOIN productservice ps ON ps.productservice_id = bfc.productservice_id
            LEFT JOIN branch b ON b.brand_id = bfc.brand_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE bfc.brand_id = $brand_id
            AND bfc.budgetforecast_month = '$budgetforecast_month'
            GROUP BY bfc.productservice_id
            ORDER BY ps.productservice_name
            ")->result();


        foreach ($data as $key) {
            if ($key->productservice_id == NULL) {
                $otc = $this->db->query("
                    SELECT *
                    FROM category
                    WHERE brand_id = $brand_id
                    AND category_name LIKE '%OTC%'"
                )->row();
                $key->category_name = $otc->category_name;
                $key->productservice_name = 'OTC';
                $key->productservice_amount = 'N/A';

            }
        }

        return $data;

    }



    public function get_forecast($brand_id, $month_and_year){

        $data = $this->db->query("
            SELECT c.category_name, bfc.budgetforecast_id, b.brand_name, bfc.budgetforecast_month, ps.productservice_name, bfc.percentage
            FROM budgetforecast bfc
            LEFT JOIN productservice ps ON ps.productservice_id = bfc.productservice_id
            LEFT JOIN brand b ON b.brand_id = bfc.brand_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE bfc.brand_id = $brand_id
            AND bfc.budgetforecast_month = '$month_and_year'
            ORDER BY ps.productservice_name, c.category_name ")->result();

        foreach ($data as $key) {
            if ($key->productservice_name == NULL) {
                $otc = $this->db->query("
                    SELECT *
                    FROM category
                    WHERE brand_id = $brand_id
                    AND category_name LIKE '%OTC%'"
                )->row();
                $key->category_name = $otc->category_name;
                $key->productservice_name = 'OTC';
                $key->productservice_amount = 'N/A';

            }
        }

        return $data;

    }

    public function edit_percentage($budgetforecast_id, $percentage){

       $this->db->set('percentage', $percentage);
       $this->db->where('budgetforecast_id', $budgetforecast_id);
       $this->db->update('budgetforecast');

    }

    public function budgetfolder_autoorder($branch_id, $budgetmonth){
        $budgetforecast_month = $budgetmonth;
        $branch_id = $branch_id;
     

        $storetarget_month = date('Y', strtotime($budgetforecast_month)).'-'.date('m', strtotime($budgetforecast_month)).'-01';
        $wall_to_wall_date = '';
        $branch = $this->db->query("
            SELECT branch_name, branch_batchno, brand_id, branch_budgetfolder
            FROM branch 
            WHERE branch_id = $branch_id")->row();

        if($branch->branch_batchno == 2){
            $wall_to_wall_date = strtotime("-1 months", strtotime($storetarget_month));
            $wall_to_wall_date = strftime ( '%Y-%m-15' , $wall_to_wall_date );
        }else{
            $wall_to_wall_date = strtotime("-2 months", strtotime($storetarget_month));
            $wall_to_wall_date = strftime ( '%Y-%m-%y' , $wall_to_wall_date );
            $wall_to_wall_date = date('Y-m-t', strtotime($wall_to_wall_date));
        }

        $sales_forecast = $this->db->query("SELECT storetarget_updatedsalesforecast as sales_forecast
                    FROM storetarget
                    WHERE branch_id = $branch_id
                    AND storetarget_month = '$storetarget_month'
        ")->row();



        $current_inventory = $this->get_budgetfolder_inventory($branch_id, $wall_to_wall_date)->result();

        $current_inventory_group = $this->get_budgetfolder_inventory_group($branch_id, $wall_to_wall_date)->result();


        $get_beginning_materials = $this->get_budgetfolder_inventory_consumables($branch_id, $wall_to_wall_date)->result();

        $get_beginning_otc = $this->get_budgetfolder_inventory_otcs($branch_id, $wall_to_wall_date)->result();

        $forecast = $this->get_categoryforecast($branch_id, $budgetforecast_month);

        $service_ids = array_column($forecast, 'productservice_id');

        $items['auto'] = $this->db->query("
            SELECT  cc.currentinventory_id, i.item_id, i.item_name, i.item_cost, i.item_uomsize, i.item_supplier, i.item_ordertype, i.item_sapuom, i.item_sapuomsize, sm.servicemix_quantity, sm.servicemix_uom, FORMAT(0, 2) as order_quantity, FORMAT(0, 2) as current_quantity, FORMAT(0, 0) as forecast_qty
            FROM item i
            LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id 
            LEFT JOIN servicemix sm ON sm.item_id = i.item_id
            LEFT JOIN productservice ps ON ps.productservice_code = sm.productservice_code
            WHERE ps.productservice_id IN ('".implode("','",$service_ids)."')
            AND i.item_ordertype = 1
            AND i.item_ordermodule = 1
            AND cc.currentinventory_dateupdated = '$wall_to_wall_date' 
            GROUP BY i.item_id
            ORDER BY i.item_name ASC
            ")->result();

        $items['manual'] = $this->db->query("
          SELECT  cc.currentinventory_id, i.item_id, i.itemgroup i.item_name, i.item_cost, i.item_uomsize, i.item_ordertype, i.item_sapuom, i.item_sapuomsize, sm.servicemix_quantity, sm.servicemix_uom, FORMAT(0, 2) as order_quantity, FORMAT(0, 2) as current_quantity, FORMAT(0, 0) as forecast_qty, FORMAT(0, 0) as manual_quantity, (
                CASE 
                    WHEN i.itemgroup IS NULL
                        THEN i.item_name
                    ELSE ig.itemgroup_name
                    END
                ) AS item_group
            FROM item i
            LEFT JOIN itemgroup ig ON ig.itemgroup_id = i.itemgroup
            LEFT JOIN currentinventory_copy cc ON cc.item_id = i.item_id 
            LEFT JOIN servicemix sm ON sm.item_id = i.item_id
            LEFT JOIN productservice ps ON ps.productservice_code = sm.productservice_code
            WHERE ps.productservice_id IN ('".implode("','",$service_ids)."')
            AND i.item_ordertype = 2
            AND i.item_ordermodule = 1
            AND cc.currentinventory_dateupdated = '$wall_to_wall_date' 
            GROUP BY item_group
            ORDER BY i.item_name ASC
            ")->result();

        $otc_percentage = 0;

        foreach ($forecast as $key) {
            if ($key->productservice_id == NULL) {
                $otc_percentage = $key->percentage;
                unset($key);
            } else {
                $productservice_id = $key->productservice_id;
                
                $data['details'][] = $this->db->query("
                    SELECT i.item_id, i.item_name, i.item_shade, i.item_color, i.item_model, i.item_ordertype, i.item_sapuomsize, i.item_sapuom, i.item_cost, sm.servicemix_quantity, sm.servicemix_uom, bfc.percentage, bfc.budgetforecast_month, ps.productservice_id, ps.productservice_name, ps.productservice_amount, c.category_name, ceil((bfc.percentage * $sales_forecast->sales_forecast / 100) / ps.productservice_amount) as qty, ceil(ceil((bfc.percentage * $sales_forecast->sales_forecast / 100) / ps.productservice_amount) * sm.servicemix_quantity) as order_quantity, (
                    CASE 
                        WHEN i.itemgroup IS NULL
                            THEN i.item_name
                        ELSE ig.itemgroup_name
                        END
                    ) AS item_group
                    FROM productservice ps
                    LEFT JOIN servicemix sm on sm.productservice_code = ps.productservice_id
                    LEFT JOIN item i ON i.item_code = sm.item_id
                    LEFT JOIN itemgroup ig ON ig.itemgroup_id = i.itemgroup
                    LEFT JOIN category c ON c.category_id = i.category_id
                    LEFT JOIN budgetforecast bfc ON bfc.productservice_id = ps.productservice_id
                    WHERE bfc.budgetforecast_month = '$budgetforecast_month'
                    AND ps.productservice_id = $productservice_id
                    AND i.item_ordermodule = 1
                    GROUP BY i.item_id")->result_array();

            }

        }

        $materials_beginning_amount = 0;

        $otc_beginning_amount = 0;

        foreach ($get_beginning_materials as $beginning) {

            if ($beginning->current_quantity < 0) {
                $beginning->current_quantity = 0;
            }
            
            $materials_beginning_amount =  $materials_beginning_amount + $beginning->current_quantity * $beginning->item_cost;
   
        }


        foreach ($get_beginning_otc as $beginning_otc) {

            if ($beginning_otc->current_quantity < 0) {
                $beginning_otc->current_quantity = 0;
            }
           
            $otc_beginning_amount =  $otc_beginning_amount + $beginning_otc->current_quantity * $beginning_otc->item_cost;
       
        }


        foreach ($items['auto'] as $auto) {
            
            $total = 0;

            $forecast_qty_total = 0;

            for ($i=0; $i < count($data['details']); $i++) {
               
                for ($y=0; $y < count($data['details'][$i]); $y++) { 
                   
                    if ($auto->item_id == $data['details'][$i][$y]['item_id']) {

                        $total = $data['details'][$i][$y]['order_quantity'];

                        $auto->order_quantity = $auto->order_quantity + $total;

                        $forecast_qty_total =  $data['details'][$i][$y]['qty'];

                        $auto->forecast_qty = $auto->forecast_qty + $forecast_qty_total;

                        // $auto->currentinventory_id =  $data['details'][$i][$y]['currentinventory_id'];

                        $auto->auto_quantity = $auto->servicemix_quantity * $auto->forecast_qty;                       


                    }

                }

            }

            foreach ($current_inventory as $row) {
                if ($auto->item_id == $row->item_id) {
                    if ($row->current_quantity < 0) {
                        $auto->current_quantity = 0;
                    } else {
                        $auto->current_quantity = $row->current_quantity;
                    }

                }
            }

        }




        foreach ($items['manual'] as $manual) {
            
            $total_manual = 0;

            $forecast_qty_total_manual = 0;

            for ($i=0; $i < count($data['details']); $i++) {
               
                for ($y=0; $y < count($data['details'][$i]); $y++) { 
                   
                    if ($manual->item_id == $data['details'][$i][$y]['item_id'] || $manual->item_group == $data['details'][$i][$y]['item_group']) {

                        if ($manual->item_id == 52267 || $manual->itemgroup == 4 || $manual->itemgroup == 8) {
                            
                        }


                        $total_manual = $data['details'][$i][$y]['order_quantity'];

                        $manual->order_quantity = $manual->order_quantity + $total_manual;


                        $forecast_qty_total_manual =  $data['details'][$i][$y]['qty'];

                        $manual->forecast_qty = $manual->forecast_qty + $forecast_qty_total_manual;

                        $manual->manual_quantity = $manual->servicemix_quantity * $manual->forecast_qty;                       


                        // $manual->currentinventory_id =  $data['details'][$i][$y]['currentinventory_id'];

                        
                    }

                }

            }

            foreach ($current_inventory_group as $row) {
                if ($manual->item_id == $row->item_id) {
                    
                    $manual->current_quantity = $row->current_quantity;
                }
            }

        }


        $data['budget_month'] = $budgetforecast_month;

        $data['branch_id'] = $branch_id;

        $data['brand_id'] = $branch->brand_id;

        $data['branch_budgetfolder'] = $branch->branch_budgetfolder;

        $data['items'] = $items;

        $data['beginning_materials'] = $materials_beginning_amount;

        $data['beginning_otc'] = $otc_beginning_amount;

        

        $data['sales_forecast'] = $sales_forecast->sales_forecast;

        $data['branch_name'] = $branch->branch_name;

        $data['storetarget_month'] = $storetarget_month;

        $data['wall_to_wall_date'] = $wall_to_wall_date;

        $data['otc_percentage'] = $otc_percentage;

        
        return $data;

    }


    public function create_autoorder($budgetfolder_data, $consumables){

        $this->db->insert('budgetfolder', $budgetfolder_data);
        $budgetfolder_id = $this->db->insert_id();

        if(!empty($consumables)){
            for($i = 0; $i < count($consumables); $i++){
                $data = array(
                    'budgetfolder_id' => $budgetfolder_id,
                    'currentinventory_id' => $consumables[$i]['currentinventory_id'],
                    'budgetfoldermaterialorder_quantity' => $consumables[$i]['autoorder_qty'],
                    'budgetfoldermaterialorder_cost' => $consumables[$i]['item_cost'],
                    'item_supplier' => $consumables[$i]['item_supplier'],
                    'item_ordertype' => 1
                );
                $this->db->insert('budgetfoldermaterialorder', $data);
            }
        }

    }

    public function get_budgetfolder_data($branch_id, $budgetfolder_date){
        return $this->db->query("SELECT bf.* FROM budgetfolder bf where bf.branch_id = $branch_id
            AND bf.budgetfolder_budgetdate = '$budgetfolder_date'");

    }

    public function budgetfolder_totalforecast($brand_id, $folder, $month_and_year){

        $storetarget_month = date('Y-m-01', strtotime($month_and_year));

        return $this->db->query("
            SELECT SUM(st.storetarget_updatedsalesforecast) as total_forecast
            FROM storetarget st
            LEFT JOIN branch b ON b.branch_id = st.branch_id
            WHERE b.brand_id = $brand_id
            AND b.branch_budgetfolder = '$folder'
            AND st.storetarget_month = '$storetarget_month'")->row()->total_forecast;
    }


    public function print_test($folder, $month, $year, $batch){

        $temp = $year.'-'.$month.'-01';

        $month_and_year = date('F', strtotime($temp)).' '.$year;

        $brand_id = $this->db->query("
            SELECT DISTINCT brand_id
            FROM branch
            WHERE branch_budgetfolder = '$folder'")->row()->brand_id;

        $total_forecast = $this->budgetfolder_totalforecast($brand_id, $folder, $month_and_year);

        $category_forecast = $this->get_categoryforecast_testprint($brand_id, $month_and_year);

        $data['category_forecast'] = $category_forecast;
        
        $data['month_and_year'] = $month_and_year;

        $data['total_forecast'] = $total_forecast;


        return $data;

    }




}