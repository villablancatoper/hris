<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Variance_model extends CI_Model {

	
    function __construct(){

        parent::__construct();


    }


    public function get_variance($branch_id, $beginning, $delivered_start, $delivered_end, $ending ){
   //  	

    	// foreach ($items as $item_code) {
    		$delivery = $this->db->query("SELECT SUM(drd.deliveryreceiptdetail_quantity) as temp , ROUND(SUM(drd.deliveryreceiptdetail_quantity) * i.item_uomsize, 2) as deliveryreceiptdetail_quantity , i.item_uomsize , cc.currentinventory_quantity , i.item_uom, i.item_name, br.branch_id, i.item_code 
		 	 FROM item i
			 LEFT JOIN category c on c.category_id = i.category_id
			 LEFT JOIN currentinventory_copy cc on cc.item_id = i.item_id
			 LEFT JOIN branch br on br.branch_id = cc.branch_id
			 LEFT JOIN deliveryreceiptdetail drd on drd.item_code = i.item_code
			 LEFT JOIN deliveryreceipt dr on dr.deliveryreceipt_id = drd.deliveryreceipt_id
			 WHERE br.branch_id = $branch_id
			 AND dr.deliveryreceipt_date BETWEEN '$delivered_start' AND '$delivered_end'
			 AND cc.currentinventory_quantity > 0
             GROUP BY i.item_name
             ORDER BY i.item_name")->result();
    		
    	// }
    		$items = $this->db->query("SELECT br.branch_name, c.category_name, i.item_code, i.item_name, i.item_shade, i.item_color, i.item_model, i.item_uom, cc.currentinventory_quantity, i.item_uomsize ,SUM(ROUND(cc.currentinventory_quantity/ i.item_uomsize,2)) as beginning_quantity, i.item_uom,  cc.currentinventory_dateupdated, FORMAT(0, 0) as deliveryreceiptdetail_quantity, FORMAT(0, 0) as ending_inventory, FORMAT(0, 0) as actual_usage, FORMAT(0, 0) as theoretical
             FROM currentinventory_copy cc
             LEFT JOIN item i on cc.item_id = i.item_id
             LEFT JOIN category c on c.category_id = i.category_id
             LEFT JOIN branch br on br.branch_id = cc.branch_id
             WHERE br.branch_id = $branch_id
             AND cc.currentinventory_dateupdated = '$beginning'
             GROUP BY i.item_name
             ORDER BY i.item_name")->result();


    		$ending = $this->db->query("SELECT br.branch_name, i.item_code, i.item_name, i.item_uom, cc.currentinventory_quantity, i.item_uomsize,ROUND(cc.currentinventory_quantity/ i.item_uomsize,2) as ending_inventory, i.item_uom
			 FROM item i
			 LEFT JOIN category c on c.category_id = i.category_id
			 LEFT JOIN currentinventory_copy cc on cc.item_id = i.item_id
			 LEFT JOIN branch br on br.branch_id = cc.branch_id
			 WHERE br.branch_id = $branch_id
			 AND cc.currentinventory_dateupdated = '$ending'
			 ORDER BY i.item_name ASC")->result();

            $theoretical = $this->db->query("SELECT i.item_name, i.item_code, t.transaction_date, ps.productservice_name, SUM(sm.servicemix_quantity) as used_quantity
                FROM transaction t
                LEFT JOIN transactiondetail td on td.transaction_id = t.transaction_id
                LEFT JOIN productservice ps on td.productservice_id = ps.productservice_id
                LEFT JOIN servicemix sm on sm.productservice_code = ps.productservice_code
                LEFT JOIN item i on i.item_id = sm.item_id
                LEFT JOIN branch b on b.branch_id = t.branch_id
                WHERE t.transaction_date BETWEEN '$delivered_start' AND '$delivered_end'
                AND b.branch_id = $branch_id
                GROUP BY i.item_id
                ORDER by i.item_name ASC")->result();



    		foreach ($ending as $end) {
    			foreach ($items as $item) {
    				if ($item->item_code == $end->item_code) {
    					$item->ending_inventory = number_format((float)$end->ending_inventory, 2, '.', '');
    				}
    			}
    		}

    		foreach ($delivery as $delivery_qty) {
    			foreach ($items as $item) {
    				if ($item->item_code == $delivery_qty->item_code) {
    							$item->deliveryreceiptdetail_quantity = number_format((float)$delivery_qty->deliveryreceiptdetail_quantity, 2, '.', '');
    				}
    			}
    		}

              foreach ($theoretical as $theoretical_qty) {
                foreach ($items as $item) {
                    if ($item->item_code == $theoretical_qty->item_code) {
                        $item->theoretical = number_format((float)($theoretical_qty->used_quantity/ $item->item_uomsize), 2, '.', '');
                    }
                }
            }

			foreach ($items as $row) {
			    if ($row->deliveryreceiptdetail_quantity == 0) {
			    	$row->deliveryreceiptdetail_quantity = "";
			    }
			}   




    	$data['items'] = $items;
    	$data['delivery'] = $delivery;
    	$data['ending_inventory'] = $ending;
        $data['theoretical'] = $theoretical;
    	return $data;
    }



}