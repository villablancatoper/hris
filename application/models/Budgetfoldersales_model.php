<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Budgetfoldersales_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }

    public function get($branch_id, $start, $end , $branch, $brand_id){

    	
    		$query = $this->db->query("
    			SELECT * FROM(
              SELECT br.branch_budgetfolder as budgetfoldername, SUM(stt.storetarget_original) as storetarget_originals,stt.branch_id as bid
              FROM storetarget stt
              LEFT JOIN branch br ON stt.branch_id = br.branch_id 
              LEFT JOIN brand b ON b.brand_id = br.brand_id 
                
              WHERE b.brand_id != 100007 
              and storetarget_month BETWEEN '$start' AND '$end'
              GROUP BY budgetfoldername
              ORDER BY SUM(stt.storetarget_original) DESC)
               AS A
        
LEFT JOIN
                (SELECT br.branch_budgetfolder as budgetfoldername, SUM(t.transaction_totalsales) as transaction_totalsales, t.branch_id as tid
         FROM transaction t 
         LEFT JOIN branch br ON t.branch_id = br.branch_id 
         LEFT JOIN brand b ON b.brand_id = br.brand_id 
         WHERE b.brand_id != 100007 
         and transaction_date BETWEEN '$start' AND '$end' 
         GROUP BY budgetfoldername ORDER BY SUM(t.transaction_totalsales) DESC) AS B
        ON A.budgetfoldername=B.budgetfoldername");

    	

		return $query->result();

    }



    public function get_sum($branch_id, $start, $end){

		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 

									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc

									from transaction t

									WHERE t.branch_id = $branch_id AND 

									t.transaction_date BETWEEN '$start' and '$end'

								");

		return $query->result();

    }


  	public function get_branches(){

  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");

  		return $query->result();

  	}







  	// START



  	// END 

 



}