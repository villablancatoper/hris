<?php



  class Inventory_model extends CI_Model{

    function __construct(){

      parent::__construct();

      $this->load->model(array('MSSQL_model' => 'mssql'));

    }



    public function search_currentinventory($search_item, $branch_id){



      if($search_item==null){

        return $this->db->query("SELECT cur.currentinventory_id, i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity

        FROM  currentinventory cur

        LEFT JOIN item i ON cur.item_id = i.item_id

        LEFT JOIN category cat ON cat.category_id = i.category_id

        LEFT JOIN branch b ON b.branch_id = cur.branch_id

        WHERE cur.branch_id = $branch_id");

      }

      else if($search_item){

        return $this->db->query("SELECT cur.currentinventory_id, i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity

        FROM  currentinventory cur

        LEFT JOIN item i ON cur.item_id = i.item_id

        LEFT JOIN category cat ON cat.category_id = i.category_id

        LEFT JOIN branch b ON b.branch_id = cur.branch_id

        WHERE (i.item_code LIKE '%$search_item%' 

        OR i.item_name LIKE '%$search_item%'

        OR i.item_description LIKE '%$search_item%') 

        AND cur.branch_id = $branch_id");

      }

    }

    public function search_currentinventory_clone($search_item, $branch_id){



      if($search_item==null){

        return $this->db->query("SELECT cur.currentinventory_id, i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity, cur.currentinventory_dateupdated

        FROM  currentinventory_copy cur

        LEFT JOIN item i ON cur.item_id = i.item_id

        LEFT JOIN category cat ON cat.category_id = i.category_id

        LEFT JOIN branch b ON b.branch_id = cur.branch_id

        WHERE cur.branch_id = $branch_id");

      }

      else if($search_item){

        return $this->db->query("SELECT cur.currentinventory_id, i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity, cur.currentinventory_dateupdated

        FROM  currentinventory_copy cur

        LEFT JOIN item i ON cur.item_id = i.item_id

        LEFT JOIN category cat ON cat.category_id = i.category_id

        LEFT JOIN branch b ON b.branch_id = cur.branch_id

        WHERE (i.item_code LIKE '%$search_item%' 

        OR i.item_name LIKE '%$search_item%'

        OR i.item_description LIKE '%$search_item%') 

        AND cur.branch_id = $branch_id");

      }

    }



    public function get_currentinventory($branch_id = null){

      if($branch_id != null){

        return $this->db->query("SELECT i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity

        FROM  currentinventory cur

        LEFT JOIN item i ON cur.item_id = i.item_id

        LEFT JOIN category cat ON cat.category_id = i.category_id

        LEFT JOIN branch b ON b.branch_id = cur.branch_id

        WHERE cur.branch_id = $branch_id");        

      }



      return $this->db->query("SELECT i.item_id, cat.category_name, b.branch_name, i.item_code, i.item_name, i.item_description, i.item_uomsize, i.item_uom, i.item_sapuom, cur.currentinventory_quantity

      FROM  currentinventory cur

      LEFT JOIN item i ON cur.item_id = i.item_id

      LEFT JOIN category cat ON cat.category_id = i.category_id

      LEFT JOIN branch b ON b.branch_id = cur.branch_id");

    }



    public function get_items(){

      $brand_id = $this->session->userdata('user')->brand_id;

      // CONCAT(i.item_description, CONCAT(' - ', CONCAT(i.item_shade, CONCAT(' - ', CONCAT(i.item_color, CONCAT(' - ', i.item_model))))) )

      return $this->db->query("SELECT i.item_id, CONCAT(i.item_description,
      CASE
          WHEN i.item_shade IS NULL OR i.item_shade = ' ' then ''
          ELSE CONCAT(' | ', i.item_shade)
      END, 
      CASE
          WHEN i.item_color IS NULL OR i.item_color = ' ' then ''
          ELSE CONCAT(' | ', i.item_color)
      END, 
      CASE
          WHEN i.item_model IS NULL OR i.item_model = ' ' then ''
          ELSE CONCAT(' | ', i.item_model)
      END) AS 'item_description', i.item_sapuom 
      FROM item i 
      LEFT JOIN category c ON c.category_id = i.category_id
      WHERE c.brand_id = $brand_id
      ORDER BY c.category_name DESC, i.item_description ASC");

    }



    public function get_item_by_item_id($item_id){

      return $this->db->query("SELECT *
      FROM item i
      WHERE i.item_id = $item_id");

    }



    public function transfer_items($data){

      $transfer_request = array(

        'branch_id' => $data['from'],

        'transferinventory_receiverid' => $data['to'],

        'transferinventory_requestdate' => date('Y-m-d H:i:s'),

      );



      $this->db->insert('transferinventory', $transfer_request);



      $transferinventory_id = $this->db->insert_id();



      foreach($data['transfer_items'] as $transfer_item){

        $item = $this->get_item_by_item_id($transfer_item['id'])->row();



        $transferinventoryitem = array(

          'transferinventory_id' => $transferinventory_id,

          'item_id' => $item->item_id,

          'transferinventoryitem_quantity' => $transfer_item['quantity'],

          'transferinventoryitem_uom' => $item->item_uom

        );



        $this->db->insert('transferinventoryitem', $transferinventoryitem);

      }

    }



    public function get_transfer_requests(){

      $user = $this->session->userdata('user');



      if($user->brand_id != 100007){

        $branch_id = $this->session->userdata('user')->branch_id;

        return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b1.branch_name 'sender', b2.branch_name 'receiver', t.transferinventory_status

        FROM transferinventory t

        LEFT JOIN branch b1 ON t.branch_id = b1.branch_id

        LEFT JOIN branch b2 ON t.transferinventory_receiverid= b2.branch_id

        WHERE t.branch_id = $branch_id");

      }

      else{

        if($user->branch_id == 200){

          return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b1.branch_name 'sender', b2.branch_name 'receiver', t.transferinventory_status

          FROM transferinventory t

          LEFT JOIN branch b1 ON t.branch_id = b1.branch_id

          LEFT JOIN branch b2 ON t.transferinventory_receiverid= b2.branch_id
          
          WHERE b1.branch_areamanager = $user->user_id");

        }
        else{

          return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b1.branch_name 'sender', b2.branch_name 'receiver', t.transferinventory_status

          FROM transferinventory t

          LEFT JOIN branch b1 ON t.branch_id = b1.branch_id

          LEFT JOIN branch b2 ON t.transferinventory_receiverid= b2.branch_id");

        }
      }

    }



    public function get_branch_name($branch_id){

      $this->db->where('branch_id', $branch_id);

      return $this->db->get('branch');

    }



    public function get_transfer_request($transferinventory_id){

      return $this->db->query("SELECT t.transferinventory_id, b1.branch_id 'from', b1.branch_name 'sender', b2.branch_id 'to', b2.branch_name 'receiver', t.transferinventory_status 

      FROM transferinventory t

      LEFT JOIN branch b1 ON t.branch_id = b1.branch_id

      LEFT JOIN branch b2 ON t.transferinventory_receiverid= b2.branch_id

      WHERE t.transferinventory_id = $transferinventory_id");

    }



    public function get_transfer_request_items($transferinventory_id){

      return $this->db->query("SELECT i.item_id, i.item_description, t.transferinventoryitem_quantity, t.transferinventoryitem_uom 

      FROM transferinventoryitem t

      LEFT JOIN item i ON i.item_id = t.item_id

      WHERE t.transferinventory_id = $transferinventory_id");

    }



    public function search_transfer_requests($search_item){

      return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b.branch_name, t.transferinventory_receiverid, t.transferinventory_status

      FROM transferinventory t

      LEFT JOIN branch b ON t.branch_id = b.branch_id

      WHERE t.transferinventory_requestdate LIKE '%$search_item%'

      OR t.transferinventory_status LIKE '%$search_item%'");

    }



    public function update_transferinventory_status($transferinventory_id, $data){

      $this->db->where('transferinventory_id', $transferinventory_id);

      $this->db->update('transferinventory', $data);

    }



    public function insert_transferinventoryhistory($data){

      $this->db->insert('transferinventoryhistory',$data);

    }



    public function get_transferinventoryhistory($transferinventory_id){

      return $this->db->query("SELECT t.transferinventoryhistory_date, t.transferinventoryhistory_status, u.user_name, t.transferinventoryhistory_remarks 

      FROM transferinventoryhistory t 

      LEFT JOIN user u ON t.user_id = u.user_id 

      WHERE t.transferinventory_id = $transferinventory_id

      ORDER BY t.transferinventoryhistory_date DESC");

    }



    public function search_transferinventory($search_item, $branch_id){



      if($search_item==null){

        return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b.branch_name, t.transferinventory_receiverid, t.transferinventory_status

        FROM transferinventory t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        WHERE t.branch_id = $branch_id");

      }

      else if($search_item){

        return $this->db->query("SELECT t.transferinventory_id, t.transferinventory_requestdate, b.branch_name, t.transferinventory_receiverid, t.transferinventory_status

        FROM transferinventory t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        WHERE (t.transferinventory_requestdate LIKE '%$search_item%'

        OR t.transferinventory_status LIKE '%$search_item%') 

        AND t.branch_id = $branch_id");

      }

    }



    public function get_brands(){

      return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");

    }



    public function create_dr_sap($dr, $dr_details){

      $this->db->where('branch_id', $dr['branch_id']);

      $branch = $this->db->get('branch')->row();



      $data = array(

        'branch_id' => $branch->branch_id,

        'deliveryreceipt_date' => $dr['deliveryreceipt_date'],

        'deliveryreceipt_remarks' => $dr['deliveryreceipt_remarks'],

        'deliveryreceipt_status' => $dr['deliveryreceipt_status'],

      );



      $this->db->insert('deliveryreceipt', $data);



      $deliveryreceipt_id = $this->db->insert_id();



      $dr_type = "Receive";



      if($dr_details['dr_type'] == "WTR1"){

        $dr_type = "Transfer";

      }



      for($i = 0; $i < count($dr_details['dr_items']); $i++){

        $this->db->where('item_sapcode', $dr_details['dr_items'][$i][1]);

        $item = $this->db->get('item')->row();



        if($item){

          // $dr_item = null;

          // if($dr_details['dr_type'] == 'PDN1'){

          //   $this->mssql->set_db($branch->branch_dbname);

          //   $dr_item = $this->mssql->get_pdn_item_by_doc_entry_on_pdn1($dr_details['dr_items'][$i][0], $dr_details['dr_items'][$i][1], $branch->branch_whscode)->row();

          // }

          // else{

          //   $this->mssql->set_db($branch->branch_dbname);

          //   $dr_item = $this->mssql->get_pdn_item_by_doc_entry_on_wtr1($dr_details['dr_items'][$i][0], $dr_details['dr_items'][$i][1], $branch->branch_whscode)->row();

          // }

          

          $dr_data = array(

            'deliveryreceipt_id' => $deliveryreceipt_id,

            'item_id' => $item->item_id,

            'deliveryreceiptdetail_quantity' => $dr_details['dr_items'][$i][3],

            'deliveryreceiptdetail_type' => $dr_type,

            'branch_id' => $branch->branch_id,

            'deliveryreceiptdetail_refid' => $dr_details['dr_items'][$i][0],

            // 'deliveryreceiptdetail_vendor' => $dr_item->Vendor,

            'vendor_id' => NULL

          );



          $this->db->insert('deliveryreceiptdetail', $dr_data);

        }

        else{

          

        }

      }

    }



    public function check_item_availability($branch_id, $item_code, $doc_entry){

      $this->db->where('branch_id', $branch_id);

      $branch = $this->db->get('branch')->row();



      $this->db->where('item_sapcode', $item_code);

      $item = $this->db->get('item')->row();



      if($item){

        $this->db->where('branch_id', $branch->branch_id);

        $this->db->where('item_id', $item->item_id);

        $this->db->where('deliveryreceiptdetail_refid', $doc_entry);

        $deliveryreceiptdetail = $this->db->get('deliveryreceiptdetail')->row();



        if($deliveryreceiptdetail){

          return TRUE;

        }

        return FALSE;

      }

      return FALSE;

    }



    public function get_dr_items($branch_id){

      if($branch_id != null){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        WHERE b.branch_id = $branch_id

        AND d.deliveryreceipt_status LIKE '%Waiting for delivery.%' ");

      }

      return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id");

    }



    public function get_dr($deliveryreceipt_id){

      return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status, br.brand_image, d.deliveryreceipt_no, d.deliveryreceipt_packed, d.deliveryreceipt_pickup

      FROM deliveryreceipt d 

      LEFT JOIN branch b ON d.branch_id = b.branch_id

      LEFT JOIN brand br ON br.brand_id = b.brand_id

      WHERE d.deliveryreceipt_id = $deliveryreceipt_id");

    }



    public function get_drd($deliveryreceipt_id){

      return $this->db->query("SELECT d.deliveryreceiptdetail_id, d.deliveryreceiptdetail_refid, i.item_code, i.item_description, d.deliveryreceiptdetail_quantity, d.deliveryreceiptdetail_receivequantity, d.deliveryreceiptdetail_sapuom, i.item_supplier

      FROM deliveryreceiptdetail d 

      LEFT JOIN item i ON d.item_code = i.item_code

      WHERE d.deliveryreceipt_id = $deliveryreceipt_id
      
      ORDER BY i.item_supplier ASC, i.item_description ASC");

    }



    public function delete_dr($deliveryreceipt_id){

      $this->db->where('deliveryreceipt_id', $deliveryreceipt_id);

      $this->db->delete('deliveryreceipt');



      $this->db->where('deliveryreceipt_id', $deliveryreceipt_id);

      $this->db->delete('deliveryreceiptdetail');

    }



    public function search_drs($data){

      $brand_id = $data['brand_id'];

      $branch_id = $data['branch_id'];

      $start = $data['start'];

      $end = $data['end'];



      if($brand_id && !$branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id");

      }



      if($brand_id && $branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id");

      }



      if($brand_id && !$branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND d.deliveryreceipt_date = '$start'");

      }

      

      if($brand_id && $branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND d.deliveryreceipt_date = '$start'");

      }



      if($brand_id && $branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND d.deliveryreceipt_date BETWEEN '$start' AND '$end'");

      }



      if($brand_id && !$branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND d.deliveryreceipt_date BETWEEN '$start' AND '$end'");

      }



      // End of 

      // BRAND filtering



      if($branch_id && !$brand_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id");

      }



      if($branch_id && !$brand_id && ($start ==  $end && $start == '1970-01-01') ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        WHERE b.branch_id = $branch_id

        AND d.deliveryreceipt_date = '$start'");

      }



      if($branch_id && !$brand_id && ($start !=  $end) ){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        WHERE b.branch_id = $branch_id

        AND d.deliveryreceipt_date BETWEEN '$start' AND '$end'");

      }



      // End of 

      // BRANCH filtering



      if(($end != '1970-01-01' && $end == $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE d.deliveryreceipt_date = '$start'");

      }



      if(($end != '1970-01-01' && $end != $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT d.deliveryreceipt_id, b.branch_name, d.deliveryreceipt_date, d.deliveryreceipt_remarks, d.deliveryreceipt_status 

        FROM deliveryreceipt d 

        LEFT JOIN branch b ON d.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE d.deliveryreceipt_date BETWEEN '$start' AND '$end'");

      }



      // End of 

      // DATE filtering

    }



    public function receive_dr($deliveryreceipt_receivedate, $user_id, $deliveryreceipt_id, $dr_items){

      $status = 'Complete';

      $branch_id = $this->session->userdata('user')->branch_id;

      for($i = 0; $i < count($dr_items); $i++){



        $this->db->where('deliveryreceiptdetail_id', $dr_items[$i]['id']);

        $drd = $this->db->get('deliveryreceiptdetail')->row();



        if(round($drd->deliveryreceiptdetail_quantity, 2) != round($dr_items[$i]['quantity'], 2)){

          $status = 'Incomplete';

        }

        $this->db->where('branch_id', $branch_id);
        $this->db->where('item_id', $drd->item_code);
        $currentinventory = $this->db->get('currentinventory')->row();

        $this->db->where('item_id', $drd->item_code);
        $item = $this->db->get('item')->row();


        $total_quantity = $currentinventory->currentinventory_quantity + ($item->item_uomsize * $dr_items[$i]['quantity']);  


        if($currentinventory){

          $this->db->where('currentinventory_id', $currentinventory->currentinventory_id);

          $this->db->update('currentinventory', array('currentinventory_quantity' => $total_quantity));

        }

        else{

          $data = array(

            'item_id' => $drd->item_code,

            'branch_id' => $this->session->userdata('user')->branch_id,

            'currentinventory_quantity' => ($item->item_uomsize * $dr_items[$i]['quantity']),

            'currentinventory_uom' => $item->item_uom,

          );



          $this->db->insert('currentinventory', $data);

        }



        $data = array(

          'item_id' => $drd->item_code,

          'physicalinventory_quantity' => $dr_items[$i]['quantity'],

          'physicalinventory_type' => 'Delivery',

          'branch_id' => $this->session->userdata('user')->branch_id,

          'physicalinventory_refid' => $deliveryreceipt_id,

          'physicalinventory_receivedate' => date('Y-m-d'),

        );



        $this->db->insert('physicalinventory', $data);



        $this->db->where('deliveryreceiptdetail_id', $dr_items[$i]['id']);

        $this->db->update('deliveryreceiptdetail', array('deliveryreceiptdetail_receivequantity' => $dr_items[$i]['quantity']));

      }



      $this->db->where('deliveryreceipt_id', $deliveryreceipt_id);

      $this->db->update('deliveryreceipt', array(
        'deliveryreceipt_status' => $status,
        'deliveryreceipt_receivedate' => date('Y-m-d', strtotime($deliveryreceipt_receivedate)),
        'user_id' => $user_id,
      ));

    }



    public function get_branch_by_branch_id($branch_id){

      $this->db->where('branch_id', $branch_id);

      return $this->db->get('branch');

    }

    public function update_currentinventory($currentinventory_id, $currentinventory_quantity){
      $this->db->where('currentinventory_id', $currentinventory_id);
      $this->db->update('currentinventory', array('currentinventory_quantity' => $currentinventory_quantity));
    }

    public function update_currentinventory_clone($currentinventory_id, $currentinventory_quantity){
      $this->db->where('currentinventory_id', $currentinventory_id);
      $this->db->update('currentinventory_copy', array('currentinventory_quantity' => $currentinventory_quantity));
    }

    public function get_brand_items_by_branch_id($branch_id){
      $this->db->where('branch_id', $branch_id);
      $brand_id = $this->db->get('branch')->row()->brand_id;

      return $this->db->query("SELECT i.item_code, i.item_description, i.item_supplier, i.item_sapuom
      FROM item i 
      LEFT JOIN category c ON c.category_id = i.category_id
      WHERE c.brand_id = $brand_id
      ORDER BY i.item_description ASC");
    }

    public function get_vendors_by_branch_id($branch_id){
      return $this->db->query("SELECT v.vendor_id, v.vendor_name
      FROM vendor v 
      LEFT JOIN company c ON v.vendor_id = c.vendor_id
      WHERE c.branch_id = $branch_id
      ORDER BY v.vendor_name ASC");
    }

    public function create_dr_manual($dr, $dr_items){

      $data = array(

        'branch_id' => $dr['branch_id'],

        'deliveryreceipt_date' => $dr['deliveryreceipt_date'],

        'deliveryreceipt_packed' => $dr['deliveryreceipt_packed'],

        'deliveryreceipt_pickup' => $dr['deliveryreceipt_pickup'],

        'deliveryreceipt_no' => $dr['deliveryreceipt_no'],

        'deliveryreceipt_remarks' => $dr['deliveryreceipt_remarks'],

        'deliveryreceipt_status' => $dr['deliveryreceipt_status'],

      );



      $this->db->insert('deliveryreceipt', $data);



      $deliveryreceipt_id = $this->db->insert_id();


      for($i = 0; $i < count($dr_items); $i++){

        $sap_uom = $dr_items[$i][3];

        if($dr_items[$i][3] == '' || $dr_items[$i][3] == NULL){
          $sap_uom = $dr_items[$i][3];
        }

        $dr_data = array(

          'deliveryreceipt_id' => $deliveryreceipt_id,

          'item_code' => $dr_items[$i][0],

          'deliveryreceiptdetail_quantity' => $dr_items[$i][2],

          'deliveryreceiptdetail_sapuom' => $sap_uom,

          'deliveryreceiptdetail_type' => 'Receive',

          'branch_id' => $dr['branch_id'],

          'deliveryreceiptdetail_refid' => NULL

        );


        $this->db->insert('deliveryreceiptdetail', $dr_data);

      }

    }

    public function get_branches(){
      return $this->db->query("SELECT * 
      FROM branch
      WHERE brand_id != 100007
      OR branch_id = 207
      ORDER BY branch_name ASC");
    }

  }