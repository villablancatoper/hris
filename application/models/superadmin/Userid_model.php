<?php

class Userid_model extends CI_Model{

    function __construct(){

    parent::__construct();

    }

    public function get_brands(){

        return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");

    }

    public function get_users(){
        return $this->db->query("SELECT emp_id, emp_number, emp_lastname, emp_firstname,emp_middlename,emp_company,emp_branch, emp_status
        FROM employee_id 
      
        ");
    }

    public function get_user($emp_id){

        $this->db->where('emp_id', $emp_id);
      
        return $this->db->get('employee_id');
    }

    public function get_branches(){

        return $this->db->query("SELECT * 
        FROM branch 
        -- WHERE branch_status = 1
        ORDER BY branch_name ASC");

    }
        public function fetch_branchess($brand_id){

        $this->db->select("branch_id,branch_name");

        $this->db->where("brand_id" , $brand_id);

        $query = $this->db->get("branch");

        $output = '<option value="">Select Branch</option>';

        

        foreach ($query->result() as $row) {

            $output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

        }

        return $output;

    }
    public function get_bname($emp_branch){

        return $this->db->query("SELECT branch_name 
        FROM branch 
        WHERE branch_id='$emp_branch'
        ");

    }

    public function add_user($data){
        $this->db->insert('employee_id', $data);
    }

    public function update_user($user_id, $data){
        $this->db->where('emp_id', $user_id);
        $this->db->update('employee_id', $data);
    }

    public function delete_user($user_id){
        $this->db->where('user_id', $user_id);
        $this->db->update('user', array('user_status' => 0));
    }

    public function check_branch_no_transaction_yesterday($yesterday){
        
        return $this->db->query("SELECT b.branch_name, u.user_name 
        FROM branch b
        LEFT JOIN user u ON b.branch_areamanager = u.user_id
        WHERE b.branch_id NOT IN (
            SELECT DISTINCT branch_id 
            FROM transaction 
            WHERE transaction_date = '$yesterday'
        )
        AND b.branch_status = 1
        AND b.brand_id NOT IN (100001, 100007)
        ORDER BY b.branch_name ASC
        ");
    }

    public function test(){
        
    }

}