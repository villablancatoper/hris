<?php

    class Service_model extends CI_Model{

        function __construct(){

            parent::__construct();

        }

        public function search_services($brand_id){
            return $this->db->query("SELECT ps.*, br.brand_name, br.brand_id
            FROM productservice ps
            LEFT JOIN brand br ON br.brand_id = ps.brand_id
            WHERE br.brand_id = $brand_id 
            AND ps.productservice_status = 1
            ORDER BY ps.productservice_description ASC");
        }

        public function get_servicemix($productservice_code){
            return $this->db->query("SELECT sm.servicemix_id, sm.productservice_code, i.item_description, sm.servicemix_quantity, i.item_uom
            FROM servicemix sm
            LEFT JOIN item i ON i.item_id = sm.item_id
            WHERE sm.productservice_code = $productservice_code 
            ORDER BY i.item_description ASC");
        }

        public function get_services_by_brand_id($brand_id){
            return $this->db->query("SELECT productservice_description, productservice_code FROM productservice WHERE brand_id = $brand_id ORDER BY productservice_description ASC");
        }

        public function get_items_by_brand_id($brand_id){
            return $this->db->query("SELECT item_id, item_description, item_uom
            FROM item 
            WHERE item.category_id IN ( 
                SELECT category.category_id 
                FROM category 
                WHERE category.brand_id = $brand_id
            )
            ORDER BY item_description ASC");
        }

        public function add_servicemix($productservice_id, $servicemix_data){

            for($i = 0; $i < count($servicemix_data); $i++){

                $data = array(
                    'productservice_code' => $productservice_id,
                    'item_id' => $servicemix_data[$i][0],
                    'servicemix_quantity' => $servicemix_data[$i][2],
                    'servicemix_uom' => $servicemix_data[$i][3]
                );

                $this->db->insert('servicemix', $data);

            }

        }

        public function get_brands(){
            return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");
        }

        public function update_servicemix_quantity($servicemix_id, $servicemix_quantity){
            $this->db->where('servicemix_id', $servicemix_id);
            $this->db->update('servicemix', array('servicemix_quantity' => $servicemix_quantity));
        }

        public function delete_servicemix_item($servicemix_id, $servicemix_quantity){
            $this->db->where('servicemix_id', $servicemix_id);
            $this->db->delete('servicemix');
        }

        public function add_servicemix_item($productservice_code, $item, $servicemix_quantity, $item_uom){
            $data = array(
                'productservice_code' => $productservice_code,
                'item_id' => $item,
                'servicemix_quantity' => $servicemix_quantity,
                'servicemix_uom' => $item_uom,
            );

            $this->db->insert('servicemix', $data);
        }
    }
