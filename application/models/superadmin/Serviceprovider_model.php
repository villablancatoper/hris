<?php

class Serviceprovider_model extends CI_Model{

    function __construct(){

    parent::__construct();

    }

    public function get_brands(){

        return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");

    }

    public function get_serviceproviders(){
        return $this->db->query("SELECT s.serviceprovider_id, b.branch_name,s.serviceprovider_name, s.serviceprovider_status
        FROM serviceprovider s
        LEFT JOIN branch b ON b.branch_id = s.branch_id
        WHERE s.serviceprovider_status = 1
        AND s.branch_id IN(
		  	SELECT branch_id FROM branch WHERE brand_id != 100007 AND branch_status = 1
		)");
    }

    public function get_serviceprovider($user_id){
        $this->db->where('serviceprovider_id', $user_id);
        $this->db->where('serviceprovider_status', 1);
        return $this->db->get('serviceprovider');
    }

    public function get_branches(){

        return $this->db->query("SELECT * 
        FROM branch 
        WHERE branch_status = 1
        AND brand_id != 100007
        ORDER BY branch_name ASC");

    }

    public function add_serviceprovider($data){
        $this->db->insert('serviceprovider', $data);
    }

    public function update_serviceprovider($serviceprovider_id, $data){
        $this->db->where('serviceprovider_id', $serviceprovider_id);
        $this->db->update('serviceprovider', $data);
    }

    public function delete_serviceprovider($serviceprovider_id){
        $this->db->where('serviceprovider_id', $serviceprovider_id);
        $this->db->update('serviceprovider', array('serviceprovider_status' => 0));
    }

    public function get_serviceprovider_positions(){
        return $this->db->query("SELECT p.*
        FROM position p
        ORDER BY p.position_name");
    }

}