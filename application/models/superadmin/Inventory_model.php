<?php

    class Inventory_model extends CI_Model{
        function __construct(){
            parent::__construct();
        }
        
        public function get_categories(){
            return $this->db->query("SELECT category_id, category_name 
            FROM category 
            WHERE category_status = 1 
            ORDER BY category_name");
        }

        public function get_brands(){
            return $this->db->get('brand');
        }
    }