<?php

class User_model extends CI_Model{

    function __construct(){

    parent::__construct();

    }

    public function get_brands(){

        return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");

    }

    public function get_users(){
        return $this->db->query("SELECT u.user_id, b.branch_name, u.user_name, u.user_username, u.user_password, u.user_lastlogin
        FROM user u
        LEFT JOIN branch b ON b.branch_id = u.branch_id
        WHERE user_status = 1");
    }

    public function get_user($user_id){
        $this->db->where('user_id', $user_id);
        $this->db->where('user_status', 1);
        return $this->db->get('user');
    }

    public function get_branches(){

        return $this->db->query("SELECT * 
        FROM branch 
        WHERE branch_status = 1
        ORDER BY branch_name ASC");

    }

    public function add_user($data){
        $this->db->insert('user', $data);
    }

    public function update_user($user_id, $data){
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
    }

    public function delete_user($user_id){
        $this->db->where('user_id', $user_id);
        $this->db->update('user', array('user_status' => 0));
    }

    public function check_branch_no_transaction_yesterday($yesterday){

        return $this->db->query("SELECT b.branch_name, u.user_name, COALESCE(MAX(t.transaction_date), 'NOT USING CFS') 'encoding_status'
        FROM branch b
        LEFT JOIN user u ON b.branch_areamanager = u.user_id
        LEFT JOIN transaction t ON t.branch_id = b.branch_id
        WHERE b.branch_id NOT IN (
            SELECT DISTINCT branch_id 
            FROM transaction 
            WHERE transaction_date = '$yesterday'
        )
        AND b.branch_status = 1
        AND b.brand_id NOT IN (100007)
        GROUP BY b.branch_name
        ORDER BY b.branch_name ASC
        ");
    }

    public function get_user_late_encoding_status($user_id){
        return $this->db->query("SELECT user_lateencode FROM user WHERE user_id = $user_id");
    }
}