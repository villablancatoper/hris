<?php

class Assorted_model extends CI_Model{

    function __construct(){

        parent::__construct();

    }

    

    public function get_budgetfolder_sc($month, $folder, $item_supplier_name, $category_name){

        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y');

        if(strpos($category_name, 'Consumables')){
            $branches = $this->db->query("SELECT DISTINCT b.branch_name, cat.category_name
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%'")->result();

            $materialorders = $this->db->query("SELECT i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, i.item_cost, i.item_sapuom, b.branch_name, bfm.budgetfoldermaterialorder_quantity
            FROM budgetfoldermaterialorder bfm
            LEFT JOIN budgetfolder bf ON bfm.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%';")->result();

            return array(
                'branches' => $branches,
                'materialorders' =>$materialorders,
                'month_and_year' => $month_and_year
            );


        }
        else{
            $branches = $this->db->query("SELECT DISTINCT b.branch_name, cat.category_name
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            LEFT JOIN category cat ON i.category_id = cat.category_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%'")->result();

            $otcorders = $this->db->query("SELECT i.item_supplier, i.item_name, i.item_shade, i.item_color, i.item_model, i.item_cost, i.item_sapuom, b.branch_name, bfo.budgetfolderotcorder_quantity
            FROM budgetfolderotcorder bfo
            LEFT JOIN budgetfolder bf ON bfo.budgetfolder_id = bf.budgetfolder_id
            LEFT JOIN branch b ON b.branch_id = bf.branch_id
            LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
            LEFT JOIN item i ON i.item_id = cc.item_id
            WHERE b.branch_budgetfolder LIKE '%$folder%'
            AND bf.budgetfolder_budgetdate = '$month_and_year'
            AND i.item_supplier LIKE '%$item_supplier_name%';")->result();

            return array(
                'branches' => $branches,
                'otcorders' => $otcorders,
                'month_and_year' => $month_and_year
            );
        }
        
    }

    public function get_folder_costs($month, $batch){

        $month_and_year = date('F', strtotime(date('Y').'-'.$month)).' '.date('Y');

        $data = array();

        $folders = array();

        $data['folders'] = $this->db->query("SELECT DISTINCT branch_budgetfolder 
        FROM branch 
        WHERE branch_budgetfolder IS NOT NULL
        AND branch_batchno = $batch
        AND branch_budgetfolder != 'ALL VS'
        ORDER BY branch_budgetfolder ASC")->result();

        foreach($data['folders'] as $value){
            array_push($folders, $value->branch_budgetfolder);
        }

        $data['suppliers'] = $this->db->query("SELECT DISTINCT i.item_supplier
        FROM item i
        LEFT JOIN category cat ON cat.category_id = i.category_id
        LEFT JOIN brand br ON br.brand_id = cat.brand_id
        LEFT JOIN branch b ON b.brand_id = br.brand_id
        WHERE b.branch_budgetfolder IN ('".implode("','", $folders)."')
        AND i.item_supplier IS NOT NULL
        ORDER BY i.item_supplier")->result();

        $total = 0;

        foreach($data['suppliers'] as $supplier){

            foreach($data['folders'] as $folder){

                $category_type = $this->db->query("SELECT category_name
                FROM category cat
                LEFT JOIN item i ON i.category_id = cat.category_id
                WHERE i.item_supplier LIKE '%$supplier->item_supplier%'")->row()->category_name;

                $order = null;
                $total_cost = null;

                if(strpos($category_type, 'Consumables')){

                    $order = $this->db->query("SELECT COALESCE(SUM(bfm.budgetfoldermaterialorder_quantity * i.item_cost), 0) 'total_cost'
                    FROM budgetfoldermaterialorder bfm
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfm.budgetfolder_id
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfm.currentinventory_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    LEFT JOIN branch b ON b.branch_id = cc.branch_id
                    WHERE b.branch_budgetfolder LIKE '%$folder->branch_budgetfolder%'
                    AND bf.budgetfolder_budgetdate = '$month_and_year'
                    AND i.item_supplier LIKE '%$supplier->item_supplier%'")->row();
    
                    if($order){
                        $total_cost = $order->total_cost;
                    }
    
                    $data['rows'][] = array(
                        'total_cost' => $total_cost,
                        'folder' => $folder->branch_budgetfolder,
                        'item_supplier' => $supplier->item_supplier
                    );

                }
                else{

                    $order = $this->db->query("SELECT COALESCE(SUM(bfo.budgetfolderotcorder_quantity * i.item_cost), 0) 'total_cost'
                    FROM budgetfolderotcorder bfo
                    LEFT JOIN budgetfolder bf ON bf.budgetfolder_id = bfo.budgetfolder_id
                    LEFT JOIN currentinventory_copy cc ON cc.currentinventory_id = bfo.currentinventory_id
                    LEFT JOIN item i ON i.item_id = cc.item_id
                    LEFT JOIN branch b ON b.branch_id = cc.branch_id
                    WHERE b.branch_budgetfolder LIKE '%$folder->branch_budgetfolder%'
                    AND bf.budgetfolder_budgetdate = '$month_and_year'
                    AND i.item_supplier LIKE '%$supplier->item_supplier%'")->row();
    
                    if($order){
                        $total_cost = $order->total_cost;
                    }
    
                    $data['rows'][] = array(
                        'total_cost' => $total_cost,
                        'folder' => $folder->branch_budgetfolder,
                        'item_supplier' => $supplier->item_supplier
                    );
                }
                $total = $total + $total_cost;
            }

        }

        $data['total'] = $total;

        return $data;
    }

    public function get_suppliers_by_folder($folder){
        return $this->db->query("SELECT DISTINCT i.item_supplier, cat.category_id, cat.category_name
        FROM item i
        LEFT JOIN category cat ON cat.category_id = i.category_id
        LEFT JOIN brand br ON br.brand_id = cat.brand_id
        LEFT JOIN branch b ON b.brand_id = br.brand_id
        WHERE b.branch_budgetfolder LIKE '%$folder%'
        AND i.item_supplier IS NOT NULL
        ORDER BY i.item_supplier");
    }
}
