<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Asmsales_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }

    public function get($start, $end){

    	
    		$query = $this->db->query("
    			SELECT * FROM(
                SELECT u.user_name as username, SUM(stt.storetarget_original) as storetarget_originals
                FROM storetarget stt
                LEFT JOIN branch br ON stt.branch_id = br.branch_id 
                LEFT JOIN brand b ON b.brand_id = br.brand_id 
                LEFT JOIN user u ON u.user_id=br.branch_areamanager
                WHERE b.brand_id != 100007 
                and storetarget_month BETWEEN '$start' AND '$end'
                GROUP BY username
                ORDER BY SUM(stt.storetarget_original) DESC) 
                AS A
        
          LEFT JOIN(
               SELECT u.user_name as username, SUM(t.transaction_totalsales) as transaction_totalsales
               FROM transaction t 
               LEFT JOIN branch br ON t.branch_id = br.branch_id 
               LEFT JOIN brand b ON b.brand_id = br.brand_id 
               LEFT JOIN user u ON u.user_id=br.branch_areamanager
               WHERE b.brand_id != 100007 
               and transaction_date BETWEEN '$start' AND '$end' 
               GROUP BY username ORDER BY SUM(t.transaction_totalsales) DESC) 
               AS B
               ON A.username=B.username");

    	

		return $query->result();

    }



    public function get_sum($branch_id, $start, $end){

		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 

									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc

									from transaction t

									WHERE t.branch_id = $branch_id AND 

									t.transaction_date BETWEEN '$start' and '$end'

								");

		return $query->result();

    }


  	public function get_branches(){

  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");

  		return $query->result();

  	}







  	// START



  	// END 

 



}