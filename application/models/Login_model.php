<?php

  class Login_model extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    public function check_user($username, $password){
      $sql = "SELECT b.branch_allowencode,u.user_access_level, u.user_id, u.user_department, u.user_name, u.user_username, b.branch_id 'branch_id', b.branch_name, br.brand_image, br.brand_id, br.brand_name, u.user_role, u.user_position, b.branch_batchno, u.user_lateencode FROM user u LEFT JOIN branch b ON u.branch_id = b.branch_id LEFT JOIN brand br ON br.brand_id = b.brand_id WHERE u.user_username=? AND u.user_password=? AND u.user_status = 1";

      $user = $this->db->query($sql, array($username, $password))->row();

      if($user){
        return $user;
      }
      return FALSE;
    }

    public function update_last_login($user_id){
      $this->db->where('user_id', $user_id);
      $this->db->update('user', array('user_lastlogin' => date('Y-m-d H:i:s')));
    }

    public function insert_user_log($data){
      $this->db->insert('userlog', $data);
    }

    public function update_last_logout($user_id){
      $this->db->where('user_id', $user_id);
      $this->db->update('user', array('user_lastlogout' => date('Y-m-d H:i:s')));
    }

    public function start_maintenance(){
      $this->db->set('user_maintenance', 1);
      $this->db->where('branch_id !=', 1);
      $this->db->update('user');

    }

    public function end_maintenance(){
      $this->db->set('user_maintenance', 0);
      $this->db->where('branch_id !=', 1);
      $this->db->update('user');

    }

    public function get_maintenance_status($user_id){
      return $query = $this->db->query("SELECT user_maintenance where user_id = $user_id");
    }


  }