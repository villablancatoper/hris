<?php



  class Report_model extends CI_Model{

    function __construct(){

      parent::__construct();

    }



    public function get_brands(){

      return $this->db->query("SELECT * FROM brand WHERE brand_id != 100007 ORDER BY brand_name");

    }

    public function get_dsrs_by_branch_id($branch_id){

      $date = date('Y-m-d');

      return $this->db->query("SELECT t.transaction_id, c.customer_id, c.customer_firstname, c.customer_lastname, t.transaction_docketno , t.transaction_orno, t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc

      FROM transaction t

      LEFT JOIN customer c ON t.customer_id = c.customer_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date = '$date'

      AND t.transaction_status = 1

      ORDER BY t.transaction_date DESC");

    }
    
//     public function get_converted_walk_in_sp($data){
//       $branch_id = $data['branch_id'];
//       $brand_id = $data['brand_id'];

//       $brand_branch = $this->db->query("SELECT br.branch_name, b.brand_name
//       FROM branch br
//       LEFT JOIN brand b ON b.brand_id = br.brand_id
//       WHERE br.branch_status = 1
//       AND br.branch_id = $branch_id")->result();

//       $serviceproviders = $this->db->query("SELECT DISTINCT serviceprovider_id, serviceprovider_name 
//       FROM serviceprovider
//       WHERE branch_id = $branch_id
//       AND serviceprovider_status = 1")->result();


//       foreach ($brand_branch as $row) {
//         $branch = $row->branch_name;
//         $brand = $row->brand_name;
//       }

//       if ($branch_id == 'All Branches') {
//         $branches = $this->db->query("SELECT branch_id, branch_name FROM branch where brand_id = $brand_id")->result();
//           $output  = array();
//           foreach ($branches as $key) {
//             foreach ($data['months'] as $month) {
//                 $start = $month['start'];
//                 $end = $month['end'];
               
//                 $walk_in = $this->db->query("SELECT DISTINCT t.transaction_docketno
//                     FROM transaction t
//                     LEFT JOIN branch b ON b.branch_id = t.branch_id
//                     WHERE b.branch_id = $key->branch_id
//                     AND t.transaction_date BETWEEN '$start' AND '$end'
//                     AND t.transaction_status = 1
//                     AND t.transaction_customerstatus = 'Walk-in'")->num_rows();
               
//             }

//           }
              
//       }else{
//           foreach ($serviceproviders as $serviceprovider) {
//             foreach ($data['months'] as $month) {

//               $start = $month['start'];
//               $end = $month['end'];

//               $three_months = strtotime("+3 months", strtotime($end));
//               $three_months = strftime ( '%Y-%m-%d' , $three_months );
             
          
//               $walk_in = $this->db->query("SELECT DISTINCT sp.serviceprovider_name, t.customer_id
//                   FROM transactiondetail td
//                   LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
//                   LEFT JOIN branch b ON b.branch_id = t.branch_id
//                   LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = t.serviceprovider_id
//                   WHERE b.branch_id = $branch_id
//                   AND t.transaction_date BETWEEN '$start' AND '$end'
//                   AND t.transaction_status = 1
//                   AND td.serviceprovider_id = $serviceprovider->serviceprovider_id
//                   AND t.transaction_customerstatus = 'Walk-in'")->result();
              
//               $count = count($walk_in);

//               $get_ids_three_months = $this->db->query("SELECT  sp.serviceprovider_name, t.customer_id, t.transaction_docketno
//                   FROM transactiondetail td
//                   LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
//                   LEFT JOIN branch b ON b.branch_id = t.branch_id
//                   LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = t.serviceprovider_id
//                   WHERE b.branch_id = $branch_id
//                   AND t.transaction_date BETWEEN '$end' AND '$three_months'
//                   AND td.serviceprovider_id = $serviceprovider->serviceprovider_id
//                   AND t.transaction_status = 1")->result();

//               $array_ids = [];

//               foreach ($get_ids_three_months as $row_id) {
//                 array_push($array_ids, $row_id->customer_id);
//               }

//               $converted = 0;

//               foreach ($walk_in as $first) {
//                  if (in_array($first->customer_id, $array_ids)) {
//                     $converted += 1;
//                  }
//               }
//              $output['data'][] = array('id' => $serviceprovider->serviceprovider_id, 'serviceprovider' => $serviceprovider->serviceprovider_name, 'date' => $start, 'walk_in' => $count, 'converted' => $converted);
//             }
             
//           }
//           $output['temp_month'] = array_values(array_unique(array_column($output['data'], 'date')));
//           $output['serviceproviders'] = $serviceproviders;
//           $output['brand'] = $brand;
//           $output['branch'] = $branch;
//           // $output['months'] = $temp_month;
        
//       }


    
//     return $output;
//   }

    public function get_converted_walk_in_sp($data){
      $branch_id = $data['branch_id'];
      $brand_id = $data['brand_id'];

      $start = $data['start'];
      $end = $data['end'];
      $three_months = $data['three_months'];

      $brand_branch = $this->db->query("SELECT br.branch_name, b.brand_name
      FROM branch br
      LEFT JOIN brand b ON b.brand_id = br.brand_id
      WHERE br.branch_status = 1
      AND br.branch_id = $branch_id")->result();

      $serviceproviders = $this->db->query("SELECT DISTINCT serviceprovider_id, serviceprovider_name 
      FROM serviceprovider
      WHERE branch_id = $branch_id
      AND serviceprovider_status = 1")->result();

      $sp_ids = [];

      foreach ($brand_branch as $row) {
        $branch = $row->branch_name;
        $brand = $row->brand_name;
      }

      if ($branch_id == 'All Branches') {
        $branches = $this->db->query("SELECT branch_id, branch_name FROM branch where brand_id = $brand_id")->result();
          $output  = array();
          foreach ($branches as $key) {
                $walk_in = $this->db->query("SELECT DISTINCT t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $key->branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->num_rows();
      
          }
              
      }else{
          foreach ($serviceproviders as $serviceprovider) {

              $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND sp.serviceprovider_id = $serviceprovider->serviceprovider_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();
              
              $count = count($walk_in);


              $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                FROM transaction t
                LEFT JOIN branch b ON b.branch_id = t.branch_id
                WHERE b.branch_id = $branch_id
                AND t.transaction_date BETWEEN '$end' AND '$three_months'
                AND t.transaction_status = 1")->result();

              $array_ids = [];

              foreach ($get_ids_three_months as $row_id) {
                array_push($array_ids, $row_id->customer_id);
              }

              $converted = 0;

              foreach ($walk_in as $first) {
                 if (in_array($first->customer_id, $array_ids)) {
                    $converted += 1;
                 }
              }
             $output['data'][] = array('id' => $serviceprovider->serviceprovider_id, 'serviceprovider' => $serviceprovider->serviceprovider_name, 'date' => $start, 'walk_in' => $count, 'converted' => $converted);
            
             
          }
          // $output['serviceproviders'] = $serviceproviders;
          $output['brand'] = $brand;
          $output['branch'] = $branch;
          // $output['months'] = $temp_month;
        
      }


      
      return $output;
  }

    public function get_converted_walk_in_annual($data){
      $branch_id = $data['branch_id'];
      $brand_id = $data['brand_id'];

      if ($branch_id == 'All Branches') {

         $branches = $this->db->query("SELECT br.branch_id, br.branch_name, b.brand_name
                            FROM branch br
                            LEFT JOIN brand b ON b.brand_id = br.brand_id
                            WHERE br.branch_status = 1
                            AND b.brand_id = $brand_id"
                  )->result();
          
          $output  = array();

          foreach ($branches as $key) {
            foreach ($data['months'] as $month) {
                $start = $month['start'];
                $end = $month['end'];

                $three_months = strtotime("+3 months", strtotime($end));
                $three_months = strftime ( '%Y-%m-%d' , $three_months );
               
                $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $key->branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();

                $count = count($walk_in);

                $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $key->branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

                $array_ids = [];

                foreach ($get_ids_three_months as $row_id) {
                  array_push($array_ids, $row_id->customer_id);
                }

                $converted = 0;

                foreach ($walk_in as $first) {
                   if (in_array($first->customer_id, $array_ids)) {
                      $converted += 1;
                   }
                }

                $output['data'][] = array('brand' => $key->brand_name, 'branch' => $key->branch_name, 'date' => date('F', strtotime($start)).' '. date('Y', strtotime($start)), 'walk_in' => $count, 'converted' => $converted);
               
            }

           
          }
              
      }else{
            foreach ($data['months'] as $month) {

              $get_branch = $this->db->query("SELECT br.branch_name, b.brand_name
                            FROM branch br
                            LEFT JOIN brand b ON b.brand_id = br.brand_id
                            WHERE br.branch_status = 1
                            AND br.branch_id = $branch_id"
                  )->row();

              $start = $month['start'];
              $end = $month['end'];

              $three_months = strtotime("+3 months", strtotime($end));
              $three_months = strftime ( '%Y-%m-%d' , $three_months );
             
              $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();

              $count = count($walk_in);

              $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

              $array_ids = [];

              foreach ($get_ids_three_months as $row_id) {
                array_push($array_ids, $row_id->customer_id);
              }

              $converted = 0;

              foreach ($walk_in as $first) {
                 if (in_array($first->customer_id, $array_ids)) {
                    $converted += 1;
                 }
              }

             $output['data'][] = array('brand' => $get_branch->brand_name, 'branch' => $get_branch->branch_name,
                                       'date' => date('F', strtotime($start)).' '. date('Y', strtotime($start)), 'walk_in' => $count, 'converted' => $converted);
            
            }
             
      }

    return $output;
  }
    
    
   public function get_converted_walk_in($brand_id, $branch_id, $month, $year){

      $start = $year.'-'.$month.'-01';
      $end = date('Y-m-t', strtotime($start));

      $three_months = strtotime("+3 months", strtotime($end));
      $three_months = strftime ( '%Y-%m-%d' , $three_months );

 
      if ($branch_id == 'All Branches') {
        $data = [];
        $get_branches = $this->db->query("SELECT br.branch_id, br.branch_name, b.brand_name
                    FROM branch br
                    LEFT JOIN brand b ON b.brand_id = br.brand_id
                    WHERE br.branch_status = 1
                    AND b.brand_id = $brand_id
                    ORDER BY br.branch_name ASC")->result();
        
        foreach ($get_branches as $row) {
          $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $row->branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();

          $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $row->branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

          $array_ids = [];

          foreach ($get_ids_three_months as $row_id) {
            array_push($array_ids, $row_id->customer_id);
          }

          $converted = 0;

          foreach ($walk_in as $first) {
             if (in_array($first->customer_id, $array_ids)) {
                $converted += 1;
             }
          }

          $data[] = array('brand' => $row->brand_name, 'branch' => $row->branch_name, 'walk_in' => count($walk_in), 'converted' => $converted);

        }
        
      } else {

        $brand_branch = $this->db->query("SELECT br.branch_name, b.brand_name
                    FROM branch br
                    LEFT JOIN brand b ON b.brand_id = br.brand_id
                    WHERE br.branch_status = 1
                    AND br.branch_id = $branch_id")->row();
                    
        $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();

        $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

        $array_ids = [];

        foreach ($get_ids_three_months as $row_id) {
          array_push($array_ids, $row_id->customer_id);
        }

        $converted = 0;

        foreach ($walk_in as $first) {
           if (in_array($first->customer_id, $array_ids)) {
              $converted += 1;
           }
        }
        $data['brand'] = $brand_branch->brand_name;
        $data['branch'] = $brand_branch->branch_name;
        $data['start'] = $start;
        $data['end'] = $end;
        $data['three_months'] = $three_months;
        $data['walk_in'] = count($walk_in);
        $data['converted'] = $converted;

      }
    
    return $data;
  }


  public function get_converted_regular($brand_id, $branch_id, $month, $year){

      $start = $year.'-'.$month.'-01';
      $end = date('Y-m-t', strtotime($start));

      $three_months = strtotime("+3 months", strtotime($end));
      $three_months = strftime ( '%Y-%m-%d' , $three_months );

 
      if ($branch_id == 'All Branches') {
        $data = [];
        $get_branches = $this->db->query("SELECT br.branch_id, br.branch_name, b.brand_name
                    FROM branch br
                    LEFT JOIN brand b ON b.brand_id = br.brand_id
                    WHERE br.branch_status = 1
                    AND b.brand_id = $brand_id
                    ORDER BY br.branch_name ASC")->result();
        
        foreach ($get_branches as $row) {
          $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $row->branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->result();

          $get_ids_three_months = $this->db->query("SELECT  t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $row->branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

          $array_ids = [];

          foreach ($get_ids_three_months as $row_id) {
            array_push($array_ids, $row_id->customer_id);
          }

          $converted = 0;

          foreach ($walk_in as $first) {
             if (in_array($first->customer_id, $array_ids)) {
                $converted += 1;
             }
          }

          $data[] = array('brand' => $row->brand_name, 'branch' => $row->branch_name, 'walk_in' => count($walk_in), 'converted' => $converted);

        }
        
      } else {

        $brand_branch = $this->db->query("SELECT br.branch_name, b.brand_name
                    FROM branch br
                    LEFT JOIN brand b ON b.brand_id = br.brand_id
                    WHERE br.branch_status = 1
                    AND br.branch_id = $branch_id")->row();
                    
        $regular = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE t.transaction_date BETWEEN '$start' AND '$end'
                    AND b.branch_id = $branch_id
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Regular'")->result();

        $get_ids_three_months = $this->db->query("SELECT t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$end' AND '$three_months'
                    AND t.transaction_status = 1")->result();

        $array_ids = [];

        foreach ($get_ids_three_months as $row_id) {
          array_push($array_ids, $row_id->customer_id);
        }

        $turned_walkin = 0;

        foreach ($regular as $first) {
           if (in_array($first->customer_id, $array_ids)) {
              $turned_walkin += 1;
           }
        }
        $data['brand'] = $brand_branch->brand_name;
        $data['branch'] = $brand_branch->branch_name;
        $data['start'] = $start;
        $data['end'] = $end;
        $data['three_months'] = $three_months;
        $data['regular'] = count($regular);
        $data['turned_walkin'] = $turned_walkin;

      }
    
    return $data;
  }






    
  public function search_annual_csr_otc($data){
    $branch_id = $data['branch_id'];
    $brand_id = $data['brand_id'];
    $otc = $data['otc'];

    if ($branch_id == 'All Branches') {
      foreach ($data['months'] as $month) {
        $start = $month['start'];
        $end = $month['end'];
        $query = $this->db->query("
         SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
          FROM productretail pr
          LEFT JOIN transactiondetail td  ON  td.productretail_id = pr.productretail_id
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN branch b ON t.branch_id = b.branch_id
          LEFT JOIN brand br ON br.brand_id = b.brand_id
          WHERE td.productretail_id = $otc
          AND b.brand_id = $brand_id
          AND t.transaction_date BETWEEN '$start' AND '$end'")->row();

        $output['infos'][] = array('date' => $start, 'otc' => $query->productretail_name, 'count' =>$query->count, 'sales' => $query->sales, 'brand' => $query->brand_name, 'branch' => $query->branch_name);
      }
    } else {
      foreach ($data['months'] as $month) {
        $start = $month['start'];
        $end = $month['end'];
        $query = $this->db->query("
         SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
          FROM productretail pr
          LEFT JOIN transactiondetail td  ON  td.productretail_id = pr.productretail_id
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN branch b ON t.branch_id = b.branch_id
          LEFT JOIN brand br ON br.brand_id = b.brand_id
          WHERE td.productretail_id = $otc
          AND b.branch_id = $branch_id
          AND t.transaction_date BETWEEN '$start' AND '$end'")->row();

        $output['infos'][] = array('date' => $start, 'otc' => $query->productretail_name, 'count' =>$query->count, 'sales' => $query->sales, 'brand' => $query->brand_name, 'branch' => $query->branch_name);
      }
    }

    $output['monthly_sales'] = array_column($output['infos'], 'sales');
    $output['brand'] = array_column($output['infos'], 'brand');
    $output['branch'] = array_column($output['infos'], 'branch');
    $output['item'] = array_column($output['infos'], 'otc');
    $output['count'] = array_column($output['infos'], 'count');
    return $output;
  }

  public function search_annual_csr($data){
     $branch_id = $data['branch_id'];
     $brand_id = $data['brand_id'];
     $category_id = $data['category_id'];
     $service_id = $data['service_id'];

     if ($category_id == 'All Categories') {
          if ($branch_id == 'All Branches') {
            foreach ($data['months'] as $month) {
              $start = $month['start'];
              $end = $month['end'];
              $query = $this->db->query("
              SELECT t.transaction_date, c.category_name, ps.productservice_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
                FROM productservice ps
                LEFT JOIN category c ON c.category_id = ps.category_id
                LEFT JOIN transactiondetail td  ON  td.productservice_id = ps.productservice_id
                LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
                LEFT JOIN branch b ON t.branch_id = b.branch_id
                LEFT JOIN brand br ON br.brand_id = b.brand_id
                WHERE br.brand_id = $brand_id
                AND t.transaction_date BETWEEN '$start' AND '$end'
                GROUP BY c.category_name")->result();

              $output['infos'][] = array('date' => $start, 'service' => $query->productservice_name, 'count' =>$query->count, 'sales' => $query->sales, 'brand' => $query->brand_name, 'branch' => $query->branch_name, 'category' => $query->category_name);
            }
            
          } else {
            $categories = $this->db->query("
              SELECT category_id, category_name
              FROM category
              WHERE category_status = 1
              AND brand_id = $brand_id")->result();

              foreach ($categories as $category) {
                foreach ($data['months'] as $month) {
                  $start = $month['start'];
                  $end = $month['end'];
                  $query = $this->db->query("
                    SELECT t.transaction_date, c.category_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
                    FROM productservice ps
                    LEFT JOIN category c ON c.category_id = ps.category_id
                    LEFT JOIN transactiondetail td  ON  td.productservice_id = ps.productservice_id
                    LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
                    LEFT JOIN branch b ON t.branch_id = b.branch_id
                    LEFT JOIN brand br ON br.brand_id = b.brand_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND c.category_id = $category->category_id")->result();

                    foreach ($query as $key) {
                        $output['infos'][] = array('date' => $start, 'count' =>$key->count, 'sales' => $key->sales, 'brand' => $key->brand_name, 'branch' => $key->branch_name, 'category' => $key->category_name);
                    }

                }
              }
         
          }
     } else {
        if ($branch_id == 'All Branches') {
          foreach ($data['months'] as $month) {
            $start = $month['start'];
            $end = $month['end'];
            $query = $this->db->query("
            SELECT t.transaction_date, c.category_name, ps.productservice_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
              FROM productservice ps
              LEFT JOIN category c ON c.category_id = ps.category_id
              LEFT JOIN transactiondetail td  ON  td.productservice_id = ps.productservice_id
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON t.branch_id = b.branch_id
              LEFT JOIN brand br ON br.brand_id = b.brand_id
              WHERE td.productservice_id = $service_id
              AND b.brand_id = $brand_id
              AND t.transaction_date BETWEEN '$start' AND '$end'")->row();

            $output['infos'][] = array('date' => $start, 'service' => $query->productservice_name, 'count' =>$query->count, 'sales' => $query->sales, 'brand' => $query->brand_name, 'branch' => $query->branch_name, 'category' => $query->category_name);
          }

        } else {
          foreach ($data['months'] as $month) {
            $start = $month['start'];
            $end = $month['end'];
            $query = $this->db->query("
            SELECT t.transaction_date,  c.category_name, ps.productservice_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count,  COALESCE(SUM(td.transactiondetails_totalsales), 0) as sales
              FROM productservice ps
              LEFT JOIN category c ON c.category_id = ps.category_id
              LEFT JOIN transactiondetail td  ON  td.productservice_id = ps.productservice_id
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON t.branch_id = b.branch_id
              LEFT JOIN brand br ON br.brand_id = b.brand_id
              WHERE td.productservice_id = $service_id
              AND b.branch_id = $branch_id
              AND t.transaction_date BETWEEN '$start' AND '$end'")->row();

            $output['infos'][] = array('date' => $start, 'service' => $query->productservice_name, 'count' =>$query->count, 'sales' => $query->sales, 'brand' => $query->brand_name, 'branch' => $query->branch_name, 'category' => $query->category_name);
          }
       
        }
     }




    $output['monthly_sales'] = array_column($output['infos'], 'sales');
    $output['brand'] = array_column($output['infos'], 'brand');
    $output['branch'] = array_column($output['infos'], 'branch');
    $output['service'] = array_column($output['infos'], 'service');
    $output['count'] = array_column($output['infos'], 'count');
    $output['category'] = array_values(array_unique(array_column($output['infos'], 'category')));

    return $output;


  }

  public function get_head_count($data){
      $branch_id = $data['branch_id'];
      $brand_id = $data['brand_id'];

      if ($branch_id == 'All Branches') {
          foreach ($data['months'] as $month) {
            $start = $month['start'];
            $end = $month['end'];
           
            $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.brand_id = $brand_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->num_rows();

            $regular = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.brand_id = $brand_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Regular'")->num_rows();

            $transfer = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.brand_id = $brand_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Transfer'")->num_rows();

            $turnaway = $this->db->query("SELECT ta.turnaway_id, SUM(ta.turnaway_quantity) 'total_turnaway'
                FROM turnaway ta
                LEFT JOIN branch br ON br.branch_id = ta.branch_id 
                WHERE ta.turnaway_date BETWEEN '$start' AND '$end'
                AND br.brand_id = $brand_id
                AND ta.turnaway_status = 1
                GROUP BY ta.turnaway_id")->num_rows();

            $output[] = array('date' => $start, 'walk_in' => $walk_in, 'regular' => $regular, 'transfer' => $transfer, 'turnaway' => $turnaway);
          }

      }else{
          foreach ($data['months'] as $month) {
            $start = $month['start'];
            $end = $month['end'];
           
            $walk_in = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                    FROM transactiondetail td
                    LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Walk-in'")->num_rows();

            $regular = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
                  FROM transactiondetail td
                  LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
                  LEFT JOIN branch b ON b.branch_id = t.branch_id
                  LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                  WHERE t.branch_id = $branch_id
                  AND t.transaction_date BETWEEN '$start' AND '$end'
                  AND t.transaction_status = 1
                  AND t.transaction_customerstatus = 'Regular'")->num_rows();

            $transfer = $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id,  t.transaction_docketno
        FROM transactiondetail td
        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        AND t.transaction_status = 1
        AND t.transaction_customerstatus = 'Transfer'")->num_rows();

            $turnaway = $this->db->query("SELECT ta.turnaway_id, SUM(ta.turnaway_quantity) 'total_turnaway'
                FROM turnaway ta
                WHERE ta.turnaway_date BETWEEN '$start' AND '$end'
                AND ta.branch_id = $branch_id 
                AND ta.turnaway_status = 1
                GROUP BY ta.turnaway_id")->num_rows();
            
            $output[] = array('date' => $start, 'walk_in' => $walk_in, 'regular' => $regular, 'transfer' => $transfer, 'turnaway' => $turnaway);
          }
      }
          
          return $output;
  
  }



    public function get_total_sales_by_branch_id($branch_id=null, $date=null){

      return $this->db->query("SELECT SUM(transaction_totalsales) AS 'total_sales' FROM transaction WHERE branch_id = $branch_id AND t.transaction_status = 1");

    }



    public function search_dsrs($data){

      $user_branch_id = $this->session->userdata('user')->branch_id;

      $user_id = $this->session->userdata('user')->user_id;

      $brand_id = $data['brand_id'];

      $branch_id = $data['branch_id'];

      $start = $data['start'];

      $end = $data['end'];

      $am_query = null;

      if($user_branch_id == 200){
        $am_query = "AND b.branch_areamanager = $user_id";
      }

      

      if($brand_id && !$branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && $branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && !$branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }

      

      if($brand_id && $branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && $branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && !$branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE br.brand_id = $brand_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      // End of 

      // BRAND filtering



      if($branch_id && !$brand_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE b.branch_id = $branch_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($branch_id && !$brand_id && ($start ==  $end && $start != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($branch_id && !$brand_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      // End of 

      // BRANCH filtering



      if(($end != '1970-01-01' && $end == $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if(($end != '1970-01-01' && $end != $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, s1.serviceprovider_name 'sp', s2.serviceprovider_name 'assister', t.transaction_totalsales, t.transaction_paymentcash, t.transaction_paymentcard, t.transaction_paymentgc 

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON c.customer_id = t.customer_id

        LEFT JOIN serviceprovider s1 ON s1.serviceprovider_id = t.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = t.transaction_assistid

        WHERE t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }

    }



    public function get_dsrs_detailed_by_branch_id($branch_id){

      $date = date('Y-m-d');

      return $this->db->query("SELECT c.customer_id, t.transaction_id, t.transaction_date, c.customer_firstname, c.customer_lastname, t.transaction_customerstatus, t.transaction_docketno , t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name as assister_name

      FROM transactiondetail td

      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

      LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

      LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

      LEFT JOIN customer c ON t.customer_id = c.customer_id

      LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

      LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date = '$date'

      AND t.transaction_status = 1

      ORDER BY t.transaction_date DESC");

    }



    public function search_dsrs_detailed($data){

      $user_branch_id = $this->session->userdata('user')->branch_id;

      $user_id = $this->session->userdata('user')->user_id;

      $am_query = null;

      if($user_branch_id == 200){
        $am_query = "AND b.branch_areamanager = $user_id";
      }


      $brand_id = $data['brand_id'];

      $branch_id = $data['branch_id'];

      $start = $data['start'];

      $end = $data['end'];

      

      if($brand_id && !$branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        WHERE br.brand_id = $brand_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && $branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name',  t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && !$branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id
        
        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }

      

      if($brand_id && $branch_id && ($start ==  $end && $end != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && $branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($brand_id && !$branch_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE br.brand_id = $brand_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      // End of 

      // BRAND filtering



      if($branch_id && !$brand_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE b.branch_id = $branch_id

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($branch_id && !$brand_id && ($start ==  $end && $start != '1970-01-01') ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if($branch_id && !$brand_id && ($start !=  $end) ){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_assistid, t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      // End of 

      // BRANCH filtering



      if(($end != '1970-01-01' && $end == $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE t.transaction_date = '$start'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }



      if(($end != '1970-01-01' && $end != $start)  && !$branch_id && !$brand_id){

        return $this->db->query("SELECT t.transaction_date, b.branch_name, c.customer_firstname, c.customer_lastname, t.transaction_docketno, t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, s2.serviceprovider_name 'assister_name', t.transaction_id, t.transaction_customerstatus

        FROM transactiondetail td

        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

        LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

        LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        LEFT JOIN customer c ON t.customer_id = c.customer_id

        LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

        LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

        LEFT JOIN item i ON pr.productretail_code = i.item_code

        WHERE t.transaction_date BETWEEN '$start' AND '$end'

        $am_query

        AND t.transaction_status = 1

        ORDER BY t.transaction_date DESC, b.branch_name ASC");

      }

    }



    public function get_overall_report_by_branch_id($branch_id){

      $date = date('Y-m-d');

      return $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

      FROM transaction t 

      WHERE t.branch_id = $branch_id 

      AND t.transaction_status = 1

      AND t.transaction_date = '$date'");

    }



    public function get_productservices_count($branch_id){

      $date = date('Y-m-d');

      return $this->db->query("SELECT t.transaction_id, td.productservice_id, t.transaction_date

      FROM transaction t 

      LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date = '$date'

      AND td.productservice_id IS NOT NULL
      
      AND t.transaction_status = 1
      ");

    }



    public function get_productretails_count($branch_id){

      $date = date('Y-m-d');

      return $this->db->query("SELECT t.transaction_id, td.productretail_id, t.transaction_date

      FROM transaction t 

      LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date = '$date'

      AND td.productretail_id IS NOT NULL
      
      AND t.transaction_status = 1
      
      ");

    }



    public function get_turnaways_by_branch_id_and_date($branch_id){

      $date = date('Y-m-d');

      $this->db->where('branch_id', $branch_id);

      $this->db->where('turnaway_date', $date);

      $this->db->where('turnaway_status', 1);


      return $this->db->get('turnaway');

    }



    public function search_overall_report($data){

      $user_branch_id = $this->session->userdata('user')->branch_id;

      $user_id = $this->session->userdata('user')->user_id;

      $am_query = null;

      if($user_branch_id == 200){
        $am_query = "AND b.branch_areamanager = $user_id";
      }


      $brand_id = $data['brand_id'];

      $branch_id = $data['branch_id'];

      $start = $data['start'];

      $end = $data['end'];

      

      if($brand_id && !$branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        $am_query 

        AND t.transaction_status = 1

        AND td.productservice_id IS NOT NULL")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        $am_query 

        AND t.transaction_status = 1

        AND td.productretail_id IS NOT NULL")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id
        
        $am_query 

        AND t.turnaway_status = 1

        ")->result();



        return $report_data;

      }



      if($brand_id && $branch_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t 

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND td.productservice_id IS NOT NULL
        
        $am_query
        
        AND t.transaction_status = 1

        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND td.productretail_id IS NOT NULL
        
        $am_query 
        
        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id
        
        $am_query 
        
        t.turnaway_status = 1

        ")->result();



        return $report_data;

      }



      if($brand_id && !$branch_id && ($start ==  $end && $end != '1970-01-01') ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date = '$start'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date = '$start'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1

        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date = '$start'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1

        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.turnaway_date = '$start'
        
        $am_query 

        AND t.turnaway_status = 1

        ")->result();



        return $report_data;

      }

      

      if($brand_id && $branch_id && ($start ==  $end && $end != '1970-01-01') ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1

        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.turnaway_date = '$start'
        
        AND t.turnaway_status = 1

        ")->result();



        return $report_data;

      }



      if($brand_id && $branch_id && ($start !=  $end) ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND b.branch_id = $branch_id

        AND t.turnaway_date BETWEEN '$start' AND '$end'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      if($brand_id && !$branch_id && ($start !=  $end) ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE br.brand_id = $brand_id

        AND t.turnaway_date BETWEEN '$start' AND '$end'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      // End of 

      // BRAND filtering



      if($branch_id && !$brand_id && $start == '1970-01-01' && $end == '1970-01-01' ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        $am_query 

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        $am_query 

        AND t.transaction_status = 1

        AND td.productservice_id IS NOT NULL")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      if($branch_id && !$brand_id && ($start ==  $end && $start != '1970-01-01') ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date = '$start'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.turnaway_date = '$start'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      if($branch_id && !$brand_id && ($start !=  $end) ){

        $report_data['overall_report'] = $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE b.branch_id = $branch_id

        AND t.turnaway_date BETWEEN '$start' AND '$end'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      // End of 

      // BRANCH filtering



      if(($end != '1970-01-01' && $end == $start)  && !$branch_id && !$brand_id){

        $report_data['overall_report'] =  $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date = '$start'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date = '$start'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date = '$start'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.turnaway_date = '$start'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }



      if(($end != '1970-01-01' && $end != $start)  && !$branch_id && !$brand_id){

        $report_data['overall_report'] =  $this->db->query("SELECT t.branch_id, t.transaction_date, b.branch_name, SUM(t.transaction_totalsales) 'total_sales', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_credit', SUM(t.transaction_paymentgc) 'total_gc'

        FROM transaction t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date BETWEEN '$start' AND '$end'

        $am_query 

        AND t.transaction_status = 1

        GROUP BY b.branch_name

        ORDER BY t.transaction_date DESC, b.branch_name ASC")->result();

        

        $report_data['productservices'] = $this->db->query("SELECT t.branch_id, td.productservice_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productservice_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['productretails'] = $this->db->query("SELECT t.branch_id, td.productretail_id, td.transactiondetails_totalsales

        FROM transactiondetail td

        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.transaction_date BETWEEN '$start' AND '$end'

        AND td.productretail_id IS NOT NULL
        
        $am_query 

        AND t.transaction_status = 1
        
        ")->result();



        $report_data['turnaways'] = $this->db->query("SELECT t.branch_id, t.turnaway_id

        FROM turnaway t

        LEFT JOIN branch b ON t.branch_id = b.branch_id

        LEFT JOIN brand br ON br.brand_id = b.brand_id

        WHERE t.turnaway_date BETWEEN '$start' AND '$end'
        
        $am_query 

        AND t.turnaway_status = 1
        
        ")->result();



        return $report_data;

      }

    }
    
    public function get_total_sales_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$date'
            AND t.transaction_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            ");
        }
    }
    
    public function get_total_cash_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(t.transaction_paymentcash) 'total_cash'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$date'
            AND t.transaction_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentcash) 'total_cash'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentcash) 'total_cash'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            ");
        }
    }
    
    public function get_total_card_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(t.transaction_paymentcard) 'total_card'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$date'
            AND t.transaction_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentcard) 'total_card'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentcard) 'total_card'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            ");
        }
    }
    
    public function get_total_gc_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(t.transaction_paymentgc) 'total_gc'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$date'
            AND t.transaction_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentgc) 'total_gc'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(t.transaction_paymentgc) 'total_gc'
            FROM transaction t
            WHERE t.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            ");
        }
    }
    
    public function get_total_sales_product_service_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            $total_sales = $this->db->query("SELECT SUM(transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE branch_id = $branch_id
            AND transaction_date ='$date'
            AND transaction_status = 1
            AND transaction_totalsales > 0
            ")->row()->total_sales;

            $otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            WHERE branch_id = $branch_id
            AND t.transaction_date ='$date'
            AND t.transaction_status = 1
            AND t.transaction_totalsales > 0
            AND td.productretail_id IS NOT NULL
            ")->row()->otc_sales;

            return $total_sales - $otc_sales;
        }
        
        if($start == $end && $end != null){
            $total_sales = $this->db->query("SELECT SUM(transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE branch_id = $branch_id
            AND transaction_date ='$start'
            AND transaction_status = 1
            AND transaction_totalsales > 0
            ")->row()->total_sales;

            $otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            WHERE branch_id = $branch_id
            AND t.transaction_date ='$start'
            AND t.transaction_status = 1
            AND t.transaction_totalsales > 0
            AND td.productretail_id IS NOT NULL
            ")->row()->otc_sales;

            return $total_sales - $otc_sales;
        }
        
        if($start != $end && $end != null){
            $total_sales = $this->db->query("SELECT SUM(transaction_totalsales) 'total_sales'
            FROM transaction t
            WHERE branch_id = $branch_id
            AND transaction_date BETWEEN '$start' AND '$end'
            AND transaction_status = 1
            AND transaction_totalsales > 0
            ")->row()->total_sales;

            $otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            WHERE branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            AND t.transaction_totalsales > 0
            AND td.productretail_id IS NOT NULL
            ")->row()->otc_sales;

            return $total_sales - $otc_sales;

        }
    }
    
    public function get_total_sales_product_retail_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_otc'
            FROM transactiondetail td
            LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$date'
            AND td.productretail_id IS NOT NULL
            AND t.transaction_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_otc'
            FROM transactiondetail td
            LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
            WHERE t.branch_id = $branch_id
            AND t.transaction_date = '$start'
            AND td.productretail_id IS NOT NULL
            AND t.transaction_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_otc'
            FROM transactiondetail td
            LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
            WHERE t.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND td.productretail_id IS NOT NULL
            AND t.transaction_status = 1
            ");
        }
    }
    
    public function get_total_turnaway_branch($branch_id = null, $start = null, $end = null){

        if(!$branch_id){
          $branch_id = $this->session->userdata('user')->branch_id;
        }
        
        if(!$start && !$end){
            $date = date('Y-m-d');
            
            return $this->db->query("SELECT SUM(t.turnaway_quantity) 'total_turnaway'
            FROM turnaway t
            WHERE t.branch_id = $branch_id
            AND t.turnaway_date = '$date'
            AND t.turnaway_status = 1
            ");
        }
        
        if($start == $end && $end != null){
            return $this->db->query("SELECT SUM(t.turnaway_quantity) 'total_turnaway'
            FROM turnaway t
            WHERE t.branch_id = $branch_id
            AND t.turnaway_date = '$start'
            AND t.turnaway_status = 1
            ");
        }
        
        if($start != $end && $end != null){
            return $this->db->query("SELECT SUM(t.turnaway_quantity) 'total_turnaway'
            FROM turnaway t
            WHERE t.branch_id = $branch_id
            AND t.turnaway_date BETWEEN '$start' AND '$end'
            AND t.turnaway_status = 1
            ");
        }
    }

    public function get_total_walkin_branch($branch_id = null, $start = null, $end = null){

      if(!$branch_id){
        $branch_id = $this->session->userdata('user')->branch_id;
      }
      
      if(!$start && !$end){
          $date = date('Y-m-d');
          
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$date'
                    AND t.transaction_status = 1
                    AND  t.transaction_customerstatus = 'Walk-in'
                    
          ");
      }
      
      if($start == $end && $end != null){
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$start'
                    AND t.transaction_status = 1
                    AND  t.transaction_customerstatus = 'Walk-in'
          ");
      }
      
      if($start != $end && $end != null){
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND  t.transaction_customerstatus = 'Walk-in'
          ");
      }
    }

    public function get_total_regular_branch($branch_id = null, $start = null, $end = null){

      if(!$branch_id){
        $branch_id = $this->session->userdata('user')->branch_id;
      }
      
      if(!$start && !$end){
          $date = date('Y-m-d');
          
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                      FROM transaction t
                      LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                      LEFT JOIN branch b ON b.branch_id = t.branch_id
                      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                      WHERE b.branch_id = $branch_id
                      AND t.transaction_date = '$date'
                      AND t.transaction_status = 1
                      AND t.transaction_customerstatus = 'Regular'
                      
                   
          ");
      }
      
      if($start == $end && $end != null){
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                      FROM transaction t
                      LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                      LEFT JOIN branch b ON b.branch_id = t.branch_id
                      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                      WHERE b.branch_id = $branch_id
                      AND t.transaction_date = '$start'
                      AND t.transaction_status = 1
                      AND t.transaction_customerstatus = 'Regular'
          ");
      }
    
      if($start != $end && $end != null){
          return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                      FROM transaction t
                      LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                      LEFT JOIN branch b ON b.branch_id = t.branch_id
                      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                      WHERE b.branch_id = $branch_id
                      AND t.transaction_date BETWEEN '$start' AND '$end'
                      AND t.transaction_status = 1
                      AND t.transaction_customerstatus = 'Regular'
          ");
      }
    }
  
  public function fetch_branches($brand_id){

      $this->db->select("branch_id,branch_name");

      $this->db->where("brand_id" , $brand_id);
      
      $this->db->where("branch_status" , 1);

      $query = $this->db->get("branch");

      $output = '<option value="" hidden selected>Select Branch</option>';

      $output .= '<option value = "All Branches">All Branches</option>';

      foreach ($query->result() as $row) {

        $output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

      }

      return $output;

  }

    public function fetch_branches_no_all($brand_id){

      $this->db->select("branch_id,branch_name");

      $this->db->where("brand_id" , $brand_id);
      
      $this->db->where("branch_status" , 1);

      $query = $this->db->get("branch");

      $output = '<option value="" hidden selected>Select Branch</option>';

      foreach ($query->result() as $row) {

        $output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

      }

      return $output;

  }

  public function fetch_otc($brand_id){

      $query = $this->db->query("SELECT * FROM productretail WHERE brand_id = $brand_id AND productretail_status = 1 ORDER BY productretail_name ASC")->result();

      $output = '<option value="" hidden selected>Select OTC</option>';

      $output .= '<option value = "All OTC">All OTC</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->productretail_id.'">'.$row->productretail_name.'</option>';

      }

      return $output;

  }

  public function fetch_items($brand_id){

      $query = $this->db->query("SELECT DISTINCT i.item_name, i.item_id
        FROM item i
        LEFT JOIN category c ON c.category_id = i.category_id
        LEFT JOIN brand br ON br.brand_id = c.brand_id
        WHERE br.brand_id = $brand_id
        ORDER BY i.item_name ASC
        ")->result();

      $output = '<option value="" hidden selected>Select Item</option>';

      // $output .= '<option value = "All Branches">All Branches</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->item_id.'">'.$row->item_name.'</option>';

      }

      return $output;

  }

  public function fetch_category($brand_id){

      $query = $this->db->query("SELECT * FROM category
      WHERE brand_id = $brand_id AND category_status = 1 AND category_name NOT LIKE '%OTC%'
      AND category_name NOT LIKE '%Consumables%'
      ORDER BY category_name ASC")->result();

      $output = '<option value="" hidden selected>Select Category</option>';

      // $output .= '<option value = "All Categories">All Categories</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->category_code.'">'.$row->category_name.'</option>';

      }

      return $output;

  }

    public function fetch_category_with_all($brand_id){

      $query = $this->db->query("SELECT * FROM category
      WHERE brand_id = $brand_id AND category_status = 1 AND category_name NOT LIKE '%OTC%'
      AND category_name NOT LIKE '%Consumables%'
      ORDER BY category_name ASC")->result();

      $output = '<option value="" hidden selected>Select Category</option>';

      $output .= '<option value = "All Categories">All Categories</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->category_code.'">'.$row->category_name.'</option>';

      }

      return $output;

  }

  public function fetch_services_with_all($category_id){

      $query = $this->db->query("SELECT * FROM productservice WHERE category_id = $category_id AND productservice_status = 1")->result();

      if (count($query) == 0) {
         $output = '<option value="" hidden selected>Select Service</option>';

      } else if (count($query) == 1) {
          $output = '<option value="" hidden selected>Select Service</option>';
          foreach ($query as $row) {

            $output .= '<option value = "'.$row->productservice_id.'">'.$row->productservice_name.'</option>';

          }
      } else {
          $output = '<option value="" hidden selected>Select Service</option>';

          $output .= '<option value = "All Services">All Services</option>';

          foreach ($query as $row) {

            $output .= '<option value = "'.$row->productservice_id.'">'.$row->productservice_name.'</option>';

          }
      }


      return $output;

  }

  public function fetch_services($category_id){

      $query = $this->db->query("SELECT * FROM productservice WHERE category_id = $category_id AND productservice_status = 1")->result();

      $output = '<option value="" hidden selected>Select Service</option>';

      // $output .= '<option value = "All Branches">All Branches</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->productservice_id.'">'.$row->productservice_name.'</option>';

      }

      return $output;

  }

  public function search_custom_csr_otc($brand_id, $branch_id, $otc, $start, $end){


    if ($otc == 'All OTC') {
      if ($branch_id== 'All Branches') {
          $query = $this->db->query("
            SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
            FROM transactiondetail td
            LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON t.branch_id = b.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE pr.brand_id = $brand_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            GROUP BY b.branch_name, pr.productretail_name
            ORDER BY b.branch_name
          ")->result();

      } else {
         $query = $this->db->query("
            SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
            FROM transactiondetail td
            LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON t.branch_id = b.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE b.branch_id = $branch_id
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            GROUP BY b.branch_name, pr.productretail_name
            ORDER BY b.branch_name
          ")->result();
      }
    } else {
      if ($branch_id== 'All Branches') {
          $query = $this->db->query("
            SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
            FROM transactiondetail td
            LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON t.branch_id = b.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE pr.brand_id = $brand_id
            AND td.productretail_id = $otc
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            GROUP BY b.branch_name
            ORDER BY b.branch_name
          ")->result();

      } else {
         $query = $this->db->query("
            SELECT t.transaction_date, pr.productretail_name, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
            FROM transactiondetail td
            LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON t.branch_id = b.branch_id
            LEFT JOIN brand br ON br.brand_id = b.brand_id
            WHERE b.branch_id = $branch_id
            AND td.productretail_id = $otc
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            GROUP BY b.branch_name
            ORDER BY b.branch_name
          ")->result();
      }

    }



      return $query;

  }

  public function search_custom_csr_items($brand_id, $branch_id, $item_id, $start, $end){


    if ($branch_id== 'All Branches') {
        $query = $this->db->query("
          SELECT t.transaction_date, i.item_name, i.item_id, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id
          LEFT JOIN branch b ON t.branch_id = b.branch_id
          LEFT JOIN brand br ON br.brand_id = b.brand_id
          LEFT JOIN servicemix sm ON sm.productservice_code = ps.productservice_code
          LEFT JOIN item i ON i.item_id = sm.item_id
          WHERE br.brand_id = $brand_id
          AND i.item_id = $item_id
          AND t.transaction_date BETWEEN '$start' AND '$end'
          AND t.transaction_status = 1
          GROUP BY b.branch_name
          ORDER BY b.branch_name
        ")->result();

    } else {
       $query = $this->db->query("
          SELECT t.transaction_date, i.item_name, i.item_id, br.brand_name, b.branch_name, COUNT(td.transactiondetails_totalsales) as count, SUM(td.transactiondetails_totalsales) as sales
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id
          LEFT JOIN branch b ON t.branch_id = b.branch_id
          LEFT JOIN brand br ON br.brand_id = b.brand_id
          LEFT JOIN servicemix sm ON sm.productservice_code = ps.productservice_code
          LEFT JOIN item i ON i.item_id = sm.item_id
          WHERE b.branch_id = $branch_id
          AND i.item_id = $item_id
          AND t.transaction_date BETWEEN '$start' AND '$end'
          AND t.transaction_status = 1
        ")->result();
    }
    return $query;

  }



  public function get_total_transfer_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }
    
    if(!$start && !$end){
        $date = date('Y-m-d');
        
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$date'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Transfer'
                    
                 
        ");
    }
    
    if($start == $end && $end != null){
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$start'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Transfer'
        ");
    }
    
    if($start != $end && $end != null){
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND t.transaction_customerstatus = 'Transfer'
        ");
    }
  }

    public function get_total_head_count_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }
    
    if(!$start && !$end){
        $date = date('Y-m-d');
        
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$date'
                    AND t.transaction_status = 1
                    AND ( t.transaction_customerstatus = 'Transfer' OR  t.transaction_customerstatus = 'Regular' OR  t.transaction_customerstatus = 'Walk-in')
                    
                 
                  
        ");
    }
    
    if($start == $end && $end != null){
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date = '$start'
                    AND t.transaction_status = 1
                    AND ( t.transaction_customerstatus = 'Transfer' OR  t.transaction_customerstatus = 'Regular' OR  t.transaction_customerstatus = 'Walk-in')
                    
                 
                  
        ");
    }
    
    if($start != $end && $end != null){
        return $this->db->query("SELECT DISTINCT t.transaction_date, t.customer_id, t.transaction_docketno
                    FROM transaction t
                    LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
                    LEFT JOIN branch b ON b.branch_id = t.branch_id
                    LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
                    WHERE b.branch_id = $branch_id
                    AND t.transaction_date BETWEEN '$start' AND '$end'
                    AND t.transaction_status = 1
                    AND ( t.transaction_customerstatus = 'Transfer' OR  t.transaction_customerstatus = 'Regular' OR  t.transaction_customerstatus = 'Walk-in')
                  
        ");
    }
  }

  public function get_todate_total_sales_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }

    $month = date('m');
    $year = date('Y');

    $todate_start = $year.'-'.$month.'-'.'01';
   
    if((!$start && !$end) || ($start == $end)){
      $date = date('Y-m-d');
      
      return $this->db->query("SELECT SUM(t.transaction_totalsales) 'todate_total_sales'
      FROM transaction t
      WHERE t.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$date'
      AND t.transaction_status = 1
      ");
    }
    
    if($start != $end){
        $month = date('m', strtotime($start));
        $year = date('Y', strtotime($start));
    
        $todate_start = $year.'-'.$month.'-'.'01';

        return $this->db->query("SELECT SUM(t.transaction_totalsales) 'todate_total_sales'
        FROM transaction t
        WHERE t.branch_id = $branch_id
        AND t.transaction_date BETWEEN '$todate_start' AND '$end'
        AND t.transaction_status = 1
        ");
    }

  }

  public function get_todate_tph_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }

    $month = date('m');
    $year = date('Y');

    $todate_start = $year.'-'.$month.'-'.'01';
    
    if((!$start && !$end) || ($start == $end)){
      $date = date('Y-m-d');
      
      $total_sales = $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales'
      FROM transaction t
      WHERE t.branch_id = $branch_id 
      AND t.transaction_date BETWEEN '$todate_start' AND '$date'
      AND t.transaction_status = 1")->row()->total_sales;

      $total_head_count = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$date'
      AND t.transaction_status = 1")->num_rows();

      $todate_tph = 0;

      if($total_sales){
        $todate_tph = ($total_sales/$total_head_count);   
      }

      return $todate_tph;
    }
    
    if($start != $end){
      $month = date('m', strtotime($start));
      $year = date('Y', strtotime($start));
  
      $todate_start = $year.'-'.$month.'-'.'01';

      $total_sales = $this->db->query("SELECT SUM(t.transaction_totalsales) 'total_sales'
      FROM transaction t
      WHERE t.branch_id = $branch_id 
      AND t.transaction_date BETWEEN '$todate_start' AND '$end'
      AND t.transaction_status = 1")->row()->total_sales;

      $total_head_count = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$end'
      AND t.transaction_status = 1")->num_rows();

      $todate_tph = 0;

      if($total_sales){
        $todate_tph = ($total_sales/$total_head_count);   
      }

      return $todate_tph;
    }
  }

  public function get_todate_otc_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }

    $month = date('m');
    $year = date('Y');

    $todate_start = $year.'-'.$month.'-'.'01';
    
    if((!$start && !$end) || ($start == $end)){
      $date = date('Y-m-d');
      
      return $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_otc'
      FROM transactiondetail td
      LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$date'
      AND td.productretail_id IS NOT NULL
      AND t.transaction_status = 1
      ");
    }
    
    if($start != $end){
      $month = date('m', strtotime($start));
      $year = date('Y', strtotime($start));
  
      $todate_start = $year.'-'.$month.'-'.'01';

      return $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_otc'
      FROM transactiondetail td
      LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$end'
      AND td.productretail_id IS NOT NULL
      AND t.transaction_status = 1
      ");
    }
  }

  public function get_todate_services_branch($branch_id = null, $start = null, $end = null){

    if(!$branch_id){
      $branch_id = $this->session->userdata('user')->branch_id;
    }

    $month = date('m');
    $year = date('Y');

    $todate_start = $year.'-'.$month.'-'.'01';
    
    if((!$start && !$end) || ($start == $end)){
      $date = date('Y-m-d');
      
      $total_sales = $this->db->query("SELECT SUM(transaction_totalsales) 'total_sales'
      FROM transaction t
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$todate_start' AND '$date'
      AND transaction_status = 1
      AND transaction_totalsales > 0
      ")->row()->total_sales;

      $otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$date'
      AND t.transaction_status = 1
      AND t.transaction_totalsales > 0
      AND td.productretail_id IS NOT NULL
      ")->row()->otc_sales;

      return $total_sales - $otc_sales;
    }
    
    if($start != $end){
      $month = date('m', strtotime($start));
      $year = date('Y', strtotime($start));
  
      $todate_start = $year.'-'.$month.'-'.'01';

      $total_sales = $this->db->query("SELECT SUM(transaction_totalsales) 'total_sales'
      FROM transaction t
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$todate_start' AND '$end'
      AND transaction_status = 1
      AND transaction_totalsales > 0
      ")->row()->total_sales;

      $otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE branch_id = $branch_id
      AND t.transaction_date BETWEEN '$todate_start' AND '$end'
      AND t.transaction_status = 1
      AND t.transaction_totalsales > 0
      AND td.productretail_id IS NOT NULL
      ")->row()->otc_sales;

      return $total_sales - $otc_sales;
    }
  }
    
  public function search_dsrs_by_branch_id($branch_id, $start, $end){

    if($start == $end){

      return $this->db->query("SELECT t.customer_id, t.transaction_id, t.transaction_date, c.customer_firstname, c.customer_lastname, t.transaction_customerstatus, t.transaction_docketno , t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, t.transaction_assistid, s2.serviceprovider_name as assister_name

      FROM transactiondetail td

      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

      LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

      LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

      LEFT JOIN customer c ON t.customer_id = c.customer_id

      LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

      LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date = '$start'

      AND t.transaction_status = 1

      ORDER BY t.transaction_date DESC, t.transaction_id DESC");
    }
    
    if($start != $end){

      return $this->db->query("SELECT t.customer_id, t.transaction_id, t.transaction_date, c.customer_firstname, c.customer_lastname, t.transaction_customerstatus,  t.transaction_docketno , t.transaction_orno, ps.productservice_description, pr.productretail_name, td.transactiondetails_totalsales, td.transactiondetails_extamount, td.transactiondetails_lesstax, s.serviceprovider_name, t.transaction_assistid, s2.serviceprovider_name as assister_name

      FROM transactiondetail td

      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

      LEFT JOIN serviceprovider s ON td.serviceprovider_id = s.serviceprovider_id

      LEFT JOIN serviceprovider s2 ON s2.serviceprovider_id = td.transactiondetails_assisterid

      LEFT JOIN customer c ON t.customer_id = c.customer_id

      LEFT JOIN productservice ps ON ps.productservice_id = td.productservice_id

      LEFT JOIN productretail pr ON pr.productretail_id = td.productretail_id

      WHERE t.branch_id = $branch_id

      AND t.transaction_date BETWEEN '$start' AND '$end'

      AND t.transaction_status = 1

      ORDER BY t.transaction_date DESC, t.transaction_id DESC");
    }

  }

  public function search_custom_dsr_ho($brand_id=null, $branch_id=null, $date){

    $user_branch_id = $this->session->userdata('user')->branch_id;

    $user_id = $this->session->userdata('user')->user_id;

    $am_query = null;

    if($user_branch_id == 200){
      $am_query = "AND b.branch_areamanager = $user_id";
    }


    $month = date('m', strtotime($date));
    $selected_day = date('d', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = 0;

    switch($month){
      case '01':
        $day = '31';
        break;
        
      case '02':
        $day = '28';
        break;

      case '03':
        $day = '31';
        break;
      
      case '04':
        $day = '30';
        break;

      case '05':
        $day = '31';
        break;

      case '06':
        $day = '30';
        break;
      
      case '07':
        $day = '31';
        break;

      case '08':
        $day = '31';
        break;

      case '09':
        $day = '30';
        break;

      case '10':
        $day = '31';
        break;

      case '11':
        $day = '30';
        break;

      case '12':
        $day = '31';
        break;
    }

    $start = $year.'-'.$month.'-'.'01';
    $end = $year.'-'.$month.'-'.$selected_day;

    if($branch_id && $date){
      $dsr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, SUM(t.transaction_totalsales) 'total_sales'
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      LEFT JOIN brand br ON br.brand_id = b.brand_id
      WHERE t.branch_id = $branch_id 
      $am_query
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      ")->row();

      $storetarget_original = 0;

      $storetarget = $this->db->query("SELECT st.storetarget_original
      FROM storetarget st
      WHERE st.storetarget_month = '$start'
      ")->row();

      if($storetarget){
        $storetarget_original = $storetarget->storetarget_original;
      }

      $sales_for_the_day = $this->db->query("SELECT SUM(t.transaction_totalsales) 'sales_for_the_day', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_card', SUM(t.transaction_paymentgc) 'total_gc'  
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      $am_query
      AND t.transaction_status = 1
      AND t.transaction_date = '$date'")->row();

      $trending_sales = ($dsr->total_sales / $selected_day) * $day;

      $percentage1 = 0;
        
      if($dsr->total_sales && $storetarget_original){
        $percentage1 = $storetarget_original == 0 ? 0 : ($dsr->total_sales / $storetarget_original) * 100;
      }

      $retail_target_sales = $storetarget_original == 0 ? 0 : $storetarget_original * .10;

      $retail_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'retail_sales_for_the_day'
      FROM transactiondetail td 
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date = '$date'
      $am_query
      AND t.transaction_status = 1
      AND td.productretail_id IS NOT NULL")->row()->retail_sales_for_the_day;
      
      $total_turnaway = $this->db->query("SELECT SUM(ta.turnaway_quantity) 'total_turnaway'
      FROM turnaway ta
      WHERE ta.turnaway_date BETWEEN '$start' AND '$end'
      AND ta.branch_id = $branch_id
      AND ta.turnaway_status = 1")->row()->total_turnaway;

      $retail_total_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_retail'
      FROM transactiondetail td 
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      $am_query
      AND t.transaction_status = 1
      AND td.productretail_id IS NOT NULL")->row()->total_sales_retail;
      
      $percentage2 = 0;
      
      if($retail_total_sales && $retail_target_sales){
          $percentage2 = ($retail_total_sales / $retail_target_sales) * 100;
      }

      $todate_walkin = $this->db->query("SELECT DISTINCT t.transaction_date, sp.serviceprovider_name, t.customer_id
      FROM transactiondetail td
      LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      $am_query
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Walk-in'")->num_rows();


      $todate_regular = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      $am_query
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Regular'")->num_rows();

      $todate_transfer = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      $am_query
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Transfer'")->num_rows();

      $total_head_count = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      $am_query
      AND t.transaction_status = 1")->num_rows();

      $todate_tph = 0;
        
      if($dsr->total_sales){
          $todate_tph = ($dsr->total_sales/$total_head_count);   
      }

      $todate_lost_sales = 0;
  
      if($total_turnaway){
        $todate_lost_sales = ($total_turnaway*$todate_tph);    
      }

      $data = array(
        'brand'                       =>          $dsr->brand_name,
        'branch'                      =>          $dsr->branch_name,
        'revenue_target'              =>          $storetarget_original,
        'sales_for_the_day'           =>          $sales_for_the_day->sales_for_the_day,
        'total_cash'                  =>          $sales_for_the_day->total_cash,
        'total_card'                  =>          $sales_for_the_day->total_card,
        'total_gc'                    =>          $sales_for_the_day->total_gc,
        'total_sales'                 =>          $dsr->total_sales,
        'trending_sales'              =>          number_format($trending_sales, 2),
        'percentage1'                 =>          number_format($percentage1, 2),
        'retail_target_sales'         =>          number_format($retail_target_sales, 2),
        'retail_sales_for_the_day'    =>          $retail_sales_for_the_day,
        'retail_total_sales'          =>          $retail_total_sales,
        'percentage2'                 =>          number_format($percentage2, 2),
        'todate_walkin'               =>          $todate_walkin,
        'todate_regular'              =>          $todate_regular,
        'todate_transfer'             =>          $todate_transfer,
        'total_head_count'            =>          $total_head_count,
        'total_turnaway'              =>          $total_turnaway,
        'todate_tph'                  =>          number_format($todate_tph, 2),
        'todate_lost_sales'           =>          number_format($todate_lost_sales, 2)
      );


      return $data;
      
    }

    if($brand_id && $date){
      $data = array();

      $branches = $this->db->query("SELECT b.branch_id
      FROM branch b
      LEFT JOIN brand br ON b.brand_id = br.brand_id
      WHERE b.brand_id = $brand_id
      AND b.branch_status != 0")->result();

      foreach($branches as $branch){
        $dsr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, SUM(t.transaction_totalsales) 'total_sales'
        FROM transaction t
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        WHERE t.branch_id = $branch->branch_id 
        $am_query
        AND t.transaction_status = 1
        AND t.transaction_date BETWEEN '$start' AND '$end'")->row();

        $storetarget_original = 0;

        $storetarget = $this->db->query("SELECT st.storetarget_original
        FROM storetarget st
        WHERE st.storetarget_month = '$start'
        ")->row();

        if($storetarget){
          $storetarget_original = $storetarget->storetarget_original;
        }

        $sales_for_the_day = $this->db->query("SELECT SUM(t.transaction_totalsales) 'sales_for_the_day', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_card', SUM(t.transaction_paymentgc) 'total_gc'  
        FROM transaction t
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        $am_query
        AND t.transaction_status = 1
        AND t.transaction_date = '$date'")->row();

        $trending_sales = ($dsr->total_sales / $selected_day) * $day;

        $percentage1 = 0;
        
        if($dsr->total_sales && $storetarget_original){
            $percentage1 = $storetarget_original == 0 ? 0 : ($dsr->total_sales / $storetarget_original) * 100;
        }

        $retail_target_sales = $storetarget_original == 0 ? 0 : $storetarget_original * .10;

        $retail_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'retail_sales_for_the_day'
        FROM transactiondetail td 
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date = '$date'
        $am_query
        AND t.transaction_status = 1
        AND td.productretail_id IS NOT NULL")->row()->retail_sales_for_the_day;
        
        $total_turnaway = $this->db->query("SELECT SUM(ta.turnaway_quantity) 'total_turnaway'
          FROM turnaway ta
          WHERE ta.turnaway_date BETWEEN '$start' AND '$end'
          AND ta.branch_id = $branch->branch_id 
          AND ta.turnaway_status = 1")->row()->total_turnaway;

        $retail_total_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_retail'
        FROM transactiondetail td 
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        $am_query
        AND t.transaction_status = 1
        AND td.productretail_id IS NOT NULL")->row()->total_sales_retail;

        $percentage2 = 0;
      
        if($retail_total_sales && $retail_target_sales){
          $percentage2 = ($retail_total_sales / $retail_target_sales) * 100;
        }



        $todate_walkin = $this->db->query("SELECT DISTINCT t.transaction_date, sp.serviceprovider_name, t.customer_id
        FROM transactiondetail td
        LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        $am_query
        AND t.transaction_status = 1
        AND t.transaction_customerstatus = 'Walk-in'")->num_rows();

        $todate_regular = $this->db->query("SELECT DISTINCT t.transaction_docketno
        FROM transaction t
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        $am_query
        AND t.transaction_status = 1
        AND t.transaction_customerstatus = 'Regular'")->num_rows();

        $todate_transfer = $this->db->query("SELECT DISTINCT t.transaction_docketno
        FROM transaction t
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        $am_query
        AND t.transaction_status = 1
        AND t.transaction_customerstatus = 'Transfer'")->num_rows();

        $total_head_count = $this->db->query("SELECT DISTINCT t.transaction_docketno
        FROM transaction t
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        WHERE b.branch_id = $branch->branch_id
        AND t.transaction_date BETWEEN '$start' AND '$end'
        $am_query
        AND t.transaction_status = 1")->num_rows();
        
        $todate_tph = 0;
        
        if($dsr->total_sales){
            $todate_tph = ($dsr->total_sales/$total_head_count);   
        }

        $todate_lost_sales = 0;
      
          if($total_turnaway){
            $todate_lost_sales = ($total_turnaway*$todate_tph);    
          }

        if($dsr->brand_name){
          $data[] = array(
            'brand'                       =>          $dsr->brand_name,
            'branch'                      =>          $dsr->branch_name,
            'revenue_target'              =>          $storetarget_original,
            'sales_for_the_day'           =>          $sales_for_the_day->sales_for_the_day,
            'total_cash'                  =>          $sales_for_the_day->total_cash,
            'total_card'                  =>          $sales_for_the_day->total_card,
            'total_gc'                    =>          $sales_for_the_day->total_gc,
            'total_sales'                 =>          $dsr->total_sales,
            'trending_sales'              =>          number_format($trending_sales, 2),
            'percentage1'                 =>          number_format($percentage1, 2),
            'retail_target_sales'         =>          number_format($retail_target_sales, 2),
            'retail_sales_for_the_day'    =>          $retail_sales_for_the_day,
            'retail_total_sales'          =>          $retail_total_sales,
            'percentage2'                 =>          number_format($percentage2, 2),
            'todate_walkin'               =>          $todate_walkin,
            'todate_regular'              =>          $todate_regular,
            'todate_transfer'             =>          $todate_transfer,
            'total_head_count'            =>          $total_head_count,
            'total_turnaway'              =>          $total_turnaway,
            'todate_tph'                  =>          number_format($todate_tph, 2),
            'todate_lost_sales'           =>          number_format($todate_lost_sales, 2)
          );
        }
      }

      return $data;
    }

  }

  public function search_custom_isr_ho($brand_id=null, $branch_id=null, $date){

    $user_branch_id = $this->session->userdata('user')->branch_id;

    $user_id = $this->session->userdata('user')->user_id;

    $am_query = null;

    if($user_branch_id == 200){
      $am_query = "AND b.branch_areamanager = $user_id";
    }


    $month = date('m', strtotime($date));
    $selected_day = date('d', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = date('t', strtotime($date));
    $start = $year.'-'.$month.'-'.'01';
    $end = $year.'-'.$month.'-'.$selected_day;


    if($branch_id && $date){

      $serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
      FROM serviceprovider sp
      LEFT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
      WHERE sp.branch_id = $branch_id
      GROUP BY sp.serviceprovider_id
      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
      OR SUM(td.transactiondetails_totalsales) > 0")->result();

      $data = array();

      foreach($serviceproviders as $serviceprovider){

        $serviceprovider_id = $serviceprovider->serviceprovider_id;

        $isr = $this->db->query("SELECT br.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name, p.position_name, p.position_id,  SUM(td.transactiondetails_totalsales) 'total_sales'
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        LEFT JOIN branch b ON b.branch_id = t.branch_id
        LEFT JOIN brand br ON br.brand_id = b.brand_id
        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
        LEFT JOIN position p ON p.position_id = sp.position_id
        WHERE t.transaction_date BETWEEN '$start' AND '$end'
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_totalsales > 0
        ")->row();

        // ISR Total GC
        $isr_total_paymentgc = 0;

        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
        FROM transaction t
        LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id
        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
        WHERE t.transaction_date BETWEEN '$start' AND '$end'
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_paymentgc > 0
        AND t.transaction_totalsales > 0
        ")->result();

        foreach($isr_paymentgc as $paymentgc){

          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

          if($sp_count == 1){
            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;
          }
          else{

            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id AND
            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

            $sales_percentage = $sp_total_sales / $total_sales;

            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

          }

        }

        // GRAND TOTAL SALES GC DEDUCTED
        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

        $sptarget_amount = $this->db->query("SELECT st.sptarget_amount
        FROM sptarget st
        LEFT JOIN branch b ON b.brand_id = st.brand_id
        WHERE b.branch_id = $branch_id
        ")->row()->sptarget_amount;

        $sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'sales_for_the_day'
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date = '$date'
        AND t.transaction_totalsales > 0")->row();

        // Sales for the day Total GC
        $sales_for_the_day_total_paymentgc = 0;

        $sales_for_the_day_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date = '$date'
        AND t.transaction_totalsales > 0
        AND t.transaction_paymentgc > 0")->result();

        foreach($sales_for_the_day_paymentgc as $paymentgc){

          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

          if($sp_count == 1){
            $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + $paymentgc->transaction_paymentgc;
          }
          else{

            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id AND
            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

            $sales_percentage = $sp_total_sales / $total_sales;

            $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

          }

        }

        $todate_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_sales'
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date BETWEEN '$start' AND  '$date'
        AND t.transaction_totalsales > 0")->row()->todate_sales;

        // To Date Total GC
        $todate_sales_total_paymentgc = 0;

        $todate_sales_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date BETWEEN '$start' AND  '$date'
        AND t.transaction_paymentgc > 0
        AND t.transaction_totalsales > 0")->result();

        foreach($todate_sales_paymentgc as $paymentgc){

          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

          if($sp_count == 1){
            $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + $paymentgc->transaction_paymentgc;
          }
          else{

            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id AND
            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
            FROM transactiondetail
            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

            $sales_percentage = $sp_total_sales / $total_sales;

            $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

          }

        }

        $trending_sales = 0;
        
        if($grand_total_sales > 0){
          $trending_sales = $grand_total_sales / $selected_day * $day;
        }

        $todate_otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_otc_sales'
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date BETWEEN '$start' AND  '$date'
        AND td.productretail_id IS NOT NULL")->row()->todate_otc_sales;

        $otc_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales_for_the_day'
        FROM transactiondetail td
        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_status = 1
        AND t.serviceprovider_id = $serviceprovider_id
        AND t.transaction_date = '$date'
        AND td.productretail_id IS NOT NULL")->row()->otc_sales_for_the_day;

        
        $todate_walkin = $this->db->query("SELECT DISTINCT transaction_docketno
        FROM transaction 
        LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
        WHERE branch_id = $branch_id
        AND transaction_date BETWEEN '$start' AND '$date'
        AND transaction_status = 1
        AND transactiondetail.serviceprovider_id = $serviceprovider_id
        AND transaction_customerstatus = 'Walk-in'")->num_rows();

        $todate_regular = $this->db->query("SELECT DISTINCT transaction_docketno
        FROM transaction 
        LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
        WHERE branch_id = $branch_id
        AND transaction_date BETWEEN '$start' AND '$date'
        AND transaction_status = 1
        AND transactiondetail.serviceprovider_id = $serviceprovider_id
        AND transaction_customerstatus = 'Regular'")->num_rows();

        $todate_transfer = $this->db->query("SELECT DISTINCT transaction_docketno
        FROM transaction 
        LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
        WHERE branch_id = $branch_id
        AND transaction_date BETWEEN '$start' AND '$date'
        AND transaction_status = 1
        AND transactiondetail.serviceprovider_id = $serviceprovider_id
        AND transaction_customerstatus = 'Transfer'")->num_rows();

        $total_head_count = $this->db->query("SELECT DISTINCT transaction_docketno
        FROM transaction 
        LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
        WHERE branch_id = $branch_id
        AND transaction_date BETWEEN '$start' AND '$date'
        AND transaction_status = 1
        AND transactiondetail.serviceprovider_id = $serviceprovider_id")->num_rows();
        
        $total_service = $this->db->query("SELECT  td.productservice_id
        FROM transaction t
        LEFT JOIN transactiondetail td ON td.transaction_id = t.transaction_id
        WHERE t.branch_id = $branch_id
        AND t.transaction_date BETWEEN '$start' AND '$date'
        AND t.transaction_status = 1
        AND td.serviceprovider_id = $serviceprovider_id
        AND td.productservice_id IS NOT NULL
        GROUP BY td.productservice_id")->num_rows();


        $todate_tph = 0;
        
        if($grand_total_sales  && $total_head_count){
          $todate_tph = $grand_total_sales / $total_head_count;
        }
        
        if($isr->brand_name){
          $data[] = array(
            'brand'                       =>          $isr->brand_name,
            'branch'                      =>          $isr->branch_name,
            'sp_target'                   =>          $isr->position_id == 1 /* SL */ || $isr->position_id == 7 /* MT */  ? 0 : $sptarget_amount,
            'serviceprovider_name'        =>          $isr->serviceprovider_name,
            'position_name'               =>          $isr->position_name,
            'todate_sales'                =>          round($todate_sales - $todate_sales_total_paymentgc, 2),
            'sales_for_the_day'           =>          round($sales_for_the_day->sales_for_the_day - $sales_for_the_day_total_paymentgc, 2),
            'total_sales'                 =>          $grand_total_sales,
            'trending_sales'              =>          number_format($trending_sales, 2),
            'todate_otc_sales'            =>          number_format($todate_otc_sales, 2),
            'otc_sales_for_the_day'       =>          number_format($otc_sales_for_the_day, 2),
            'todate_walkin'               =>          $todate_walkin,
            'todate_regular'              =>          $todate_regular,
            'todate_transfer'             =>          $todate_transfer,
            'total_head_count'            =>          $total_head_count,
            'todate_tph'                  =>          number_format($todate_tph, 2),
            'sales_for_the_day_total_paymentgc' =>    round($sales_for_the_day_total_paymentgc, 2),
            'isr_total_paymentgc' =>    round($isr_total_paymentgc, 2),
            'todate_sales_total_paymentgc' =>    round($todate_sales_total_paymentgc, 2),
            'total_service'               =>          $total_service,
            'day' => $day,
            'start' => $start,
            'end' => $end,
          );   
        }
      }
      
      return $data;
        
    }

    // if($brand_id && $date){

    //   $serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
    //   FROM serviceprovider sp
    //   LEFT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
    //   LEFT JOIN branch b ON b.branch_id = sp.branch_id
    //   LEFT JOIN brand br ON br.brand_id = b.brand_id
    //   WHERE br.brand_id = $brand_id
    //   GROUP BY sp.serviceprovider_id
    //   HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
    //   OR SUM(td.transactiondetails_totalsales) > 0")->result();

    //   $data = array();

    //   foreach($serviceproviders as $serviceprovider){

    //     $serviceprovider_id = $serviceprovider->serviceprovider_id;

    //     $isr = $this->db->query("SELECT br.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales'
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     LEFT JOIN branch b ON b.branch_id = t.branch_id
    //     LEFT JOIN brand br ON br.brand_id = b.brand_id
    //     LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
    //     WHERE t.transaction_date BETWEEN '$start' AND '$end'
    //     AND t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_totalsales > 0
    //     ")->row();

    //     // ISR Total GC
    //     $isr_total_paymentgc = 0;

    //     $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
    //     FROM transaction t
    //     LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id
    //     LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
    //     WHERE t.transaction_date BETWEEN '$start' AND '$end'
    //     AND t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_paymentgc > 0
    //     AND t.transaction_totalsales > 0
    //     ")->result();

    //     foreach($isr_paymentgc as $paymentgc){

    //       $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
    //       FROM transactiondetail
    //       WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

    //       if($sp_count == 1){
    //         $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;
    //       }
    //       else{

    //         $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id AND
    //         serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

    //         $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

    //         $sales_percentage = $sp_total_sales / $total_sales;

    //         $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

    //       }

    //     }

    //     // GRAND TOTAL SALES GC DEDUCTED
    //     $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

    //     $sptarget_amount = $this->db->query("SELECT st.sptarget_amount
    //     FROM sptarget st
    //     WHERE st.brand_id = $brand_id
    //     ")->row()->sptarget_amount;

    //     $sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'sales_for_the_day'
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date = '$date'
    //     AND t.transaction_totalsales > 0")->row();

    //     // Sales for the day Total GC
    //     $sales_for_the_day_total_paymentgc = 0;

    //     $sales_for_the_day_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date = '$date'
    //     AND t.transaction_totalsales > 0
    //     AND t.transaction_paymentgc > 0")->result();

    //     foreach($sales_for_the_day_paymentgc as $paymentgc){

    //       $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
    //       FROM transactiondetail
    //       WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

    //       if($sp_count == 1){
    //         $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + $paymentgc->transaction_paymentgc;
    //       }
    //       else{

    //         $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id AND
    //         serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

    //         $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

    //         $sales_percentage = $sp_total_sales / $total_sales;

    //         $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

    //       }

    //     }

    //     $todate_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_sales'
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date BETWEEN '$start' AND  '$date'
    //     AND t.transaction_totalsales > 0")->row()->todate_sales;

    //     // To Date Total GC
    //     $todate_sales_total_paymentgc = 0;

    //     $todate_sales_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date BETWEEN '$start' AND  '$date'
    //     AND t.transaction_paymentgc > 0
    //     AND t.transaction_totalsales > 0")->result();

    //     foreach($todate_sales_paymentgc as $paymentgc){

    //       $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
    //       FROM transactiondetail
    //       WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

    //       if($sp_count == 1){
    //         $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + $paymentgc->transaction_paymentgc;
    //       }
    //       else{

    //         $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id AND
    //         serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

    //         $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
    //         FROM transactiondetail
    //         WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

    //         $sales_percentage = $sp_total_sales / $total_sales;

    //         $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

    //       }

    //     }

    //     $trending_sales = 0;
        
    //     if($grand_total_sales > 0){
    //       $trending_sales = $grand_total_sales / $current_day * $day;
    //     }

    //     $todate_otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_otc_sales'
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND td.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date BETWEEN '$start' AND  '$date'
    //     AND td.productretail_id IS NOT NULL")->row()->todate_otc_sales;

    //     $otc_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales_for_the_day'
    //     FROM transactiondetail td
    //     LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
    //     WHERE t.transaction_status = 1
    //     AND t.serviceprovider_id = $serviceprovider_id
    //     AND t.transaction_date = '$date'
    //     AND td.productretail_id IS NOT NULL")->row()->otc_sales_for_the_day;

        
    //     $todate_walkin = $this->db->query("SELECT DISTINCT transaction_docketno
    //     FROM transaction 
    //     LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
    //     WHERE transaction_date BETWEEN '$start' AND '$date'
    //     AND transaction_status = 1
    //     AND transactiondetail.serviceprovider_id = $serviceprovider_id
    //     AND transaction_customerstatus = 'Walk-in'")->num_rows();

    //     $todate_regular = $this->db->query("SELECT DISTINCT transaction_docketno
    //     FROM transaction 
    //     LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
    //     WHERE transaction_date BETWEEN '$start' AND '$date'
    //     AND transaction_status = 1
    //     AND transactiondetail.serviceprovider_id = $serviceprovider_id
    //     AND transaction_customerstatus = 'Regular'")->num_rows();

    //     $todate_transfer = $this->db->query("SELECT DISTINCT transaction_docketno
    //     FROM transaction 
    //     LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
    //     WHERE transaction_date BETWEEN '$start' AND '$date'
    //     AND transaction_status = 1
    //     AND transactiondetail.serviceprovider_id = $serviceprovider_id
    //     AND transaction_customerstatus = 'Transfer'")->num_rows();

    //     $total_head_count = $this->db->query("SELECT DISTINCT transaction_docketno
    //     FROM transaction 
    //     LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
    //     WHERE transaction_date BETWEEN '$start' AND '$date'
    //     AND transaction_status = 1
    //     AND transactiondetail.serviceprovider_id = $serviceprovider_id")->num_rows();


    //     $todate_tph = 0;
        
    //     if($grand_total_sales  && $total_head_count){
    //       $todate_tph = $grand_total_sales / $total_head_count;
    //     }
        
    //     if($isr->brand_name){
    //       $data[] = array(
    //         'brand'                       =>          $isr->brand_name,
    //         'branch'                      =>          $isr->branch_name,
    //         'sp_target'                   =>          $sptarget_amount,
    //         'serviceprovider_name'        =>          $isr->serviceprovider_name,
    //         'todate_sales'                =>          round($todate_sales - $todate_sales_total_paymentgc, 2),
    //         'sales_for_the_day'           =>          round($sales_for_the_day->sales_for_the_day - $sales_for_the_day_total_paymentgc, 2),
    //         'total_sales'                 =>          $grand_total_sales,
    //         'trending_sales'              =>          number_format($trending_sales, 2),
    //         'todate_otc_sales'            =>          number_format($todate_otc_sales, 2),
    //         'otc_sales_for_the_day'       =>          number_format($otc_sales_for_the_day, 2),
    //         'todate_walkin'               =>          $todate_walkin,
    //         'todate_regular'              =>          $todate_regular,
    //         'todate_transfer'             =>          $todate_transfer,
    //         'total_head_count'            =>          $total_head_count,
    //         'todate_tph'                  =>          number_format($todate_tph, 2),
    //         'sales_for_the_day_total_paymentgc' =>    round($sales_for_the_day_total_paymentgc, 2),
    //         'isr_total_paymentgc' =>    round($isr_total_paymentgc, 2),
    //         'todate_sales_total_paymentgc' =>    round($todate_sales_total_paymentgc, 2),
    //       );   
    //     }
    //   }
      
    //   return $data;
        
    // }

  }

  public function search_custom_isr_branch($date){
    $branch_id = $this->session->userdata('user')->branch_id;
    $brand_id = $this->session->userdata('user')->brand_id;

    $month = date('m', strtotime($date));
    $selected_day = date('d', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = date('t', strtotime($date));

    $start = $year.'-'.$month.'-'.'01';
    $end = $year.'-'.$month.'-'.$selected_day;

    $serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
    FROM serviceprovider sp
    LEFT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
    WHERE sp.branch_id = $branch_id
    GROUP BY sp.serviceprovider_id
    HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
    OR SUM(td.transactiondetails_totalsales) > 0")->result();

    $data = array();

    foreach($serviceproviders as $serviceprovider){

      $serviceprovider_id = $serviceprovider->serviceprovider_id;

      $isr = $this->db->query('SELECT br.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name, p.position_name, p.position_id, SUM(td.transactiondetails_totalsales) AS total_sales
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      LEFT JOIN brand br ON br.brand_id = b.brand_id
      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
      LEFT JOIN position p ON p.position_id = sp.position_id
      WHERE t.branch_id = '.$branch_id .'
      AND t.transaction_date BETWEEN '."'".$start."'".' AND '. "'".$end."'".
      'AND t.transaction_status = 1
      AND td.serviceprovider_id = '.$serviceprovider_id.'
      AND t.transaction_totalsales > 0
      ')->row();

      // ISR Total GC
      $isr_total_paymentgc = 0;

      $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
      FROM transaction t
      LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id
      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
      WHERE t.branch_id = $branch_id 
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_paymentgc > 0
      AND t.transaction_totalsales > 0
      ")->result();

      foreach($isr_paymentgc as $paymentgc){

        $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
        FROM transactiondetail
        WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

        if($sp_count == 1){
          $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;
        }
        else{

          $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id AND
          serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

          $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

          $sales_percentage = $sp_total_sales / $total_sales;

          $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

        }

      }

      // GRAND TOTAL SALES GC DEDUCTED
      $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

      $sptarget_amount = $this->db->query("SELECT st.sptarget_amount
      FROM sptarget st
      WHERE st.brand_id = $brand_id
      ")->row()->sptarget_amount;

      $sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'sales_for_the_day'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date = '$date'
      AND t.transaction_totalsales > 0")->row();

      // Sales for the day Total GC
      $sales_for_the_day_total_paymentgc = 0;

      $sales_for_the_day_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date = '$date'
      AND t.transaction_totalsales > 0
      AND t.transaction_paymentgc > 0")->result();

      foreach($sales_for_the_day_paymentgc as $paymentgc){

        $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
        FROM transactiondetail
        WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

        if($sp_count == 1){
          $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + $paymentgc->transaction_paymentgc;
        }
        else{

          $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id AND
          serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

          $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

          $sales_percentage = $sp_total_sales / $total_sales;

          $sales_for_the_day_total_paymentgc = $sales_for_the_day_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

        }

      }

      $todate_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_sales'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date BETWEEN '$start' AND  '$date'
      AND t.transaction_totalsales > 0")->row()->todate_sales;

      // To Date Total GC
      $todate_sales_total_paymentgc = 0;

      $todate_sales_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date BETWEEN '$start' AND  '$date'
      AND t.transaction_paymentgc > 0
      AND t.transaction_totalsales > 0")->result();

      foreach($todate_sales_paymentgc as $paymentgc){

        $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
        FROM transactiondetail
        WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

        if($sp_count == 1){
          $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + $paymentgc->transaction_paymentgc;
        }
        else{

          $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id AND
          serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

          $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
          FROM transactiondetail
          WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

          $sales_percentage = $sp_total_sales / $total_sales;

          $todate_sales_total_paymentgc = $todate_sales_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

        }

      }

      $trending_sales = 0;
      
      if($grand_total_sales > 0){
        $trending_sales = $grand_total_sales / $selected_day * $day;
      }

      $todate_otc_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'todate_otc_sales'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND td.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date BETWEEN '$start' AND  '$date'
      AND td.productretail_id IS NOT NULL")->row()->todate_otc_sales;

      $otc_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'otc_sales_for_the_day'
      FROM transactiondetail td
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      WHERE t.branch_id = $branch_id
      AND t.transaction_status = 1
      AND t.serviceprovider_id = $serviceprovider_id
      AND t.transaction_date = '$date'
      AND td.productretail_id IS NOT NULL")->row()->otc_sales_for_the_day;

      
      $todate_walkin = $this->db->query("SELECT DISTINCT transaction_docketno
      FROM transaction 
      LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$start' AND '$date'
      AND transaction_status = 1
      AND transactiondetail.serviceprovider_id = $serviceprovider_id
      AND transaction_customerstatus = 'Walk-in'")->num_rows();

      $todate_regular = $this->db->query("SELECT DISTINCT transaction_docketno
      FROM transaction 
      LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$start' AND '$date'
      AND transaction_status = 1
      AND transactiondetail.serviceprovider_id = $serviceprovider_id
      AND transaction_customerstatus = 'Regular'")->num_rows();

      $todate_transfer = $this->db->query("SELECT DISTINCT transaction_docketno
      FROM transaction 
      LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$start' AND '$date'
      AND transaction_status = 1
      AND transactiondetail.serviceprovider_id = $serviceprovider_id
      AND transaction_customerstatus = 'Transfer'")->num_rows();

      $total_head_count = $this->db->query("SELECT DISTINCT transaction_docketno
      FROM transaction 
      LEFT JOIN transactiondetail ON transactiondetail.transaction_id = transaction.transaction_id
      WHERE branch_id = $branch_id
      AND transaction_date BETWEEN '$start' AND '$date'
      AND transaction_status = 1
      AND transactiondetail.serviceprovider_id = $serviceprovider_id")->num_rows();


      $todate_tph = 0;
      
      if($grand_total_sales  && $total_head_count){
        $todate_tph = $grand_total_sales / $total_head_count;
      }
      
      if($isr->brand_name){
        $data[] = array(
          'brand'                       =>          $isr->brand_name,
          'branch'                      =>          $isr->branch_name,
          'sp_target'                   =>          $isr->position_id == 1 /* SL */ || $isr->position_id == 7 /* MT */  ? 0 : $sptarget_amount,
          'position_name'               =>          $isr->position_name,
          'serviceprovider_name'        =>          $isr->serviceprovider_name,
          'todate_sales'                =>          round($todate_sales - $todate_sales_total_paymentgc, 2),
          'sales_for_the_day'           =>          round($sales_for_the_day->sales_for_the_day - $sales_for_the_day_total_paymentgc, 2),
          'total_sales'                 =>          $grand_total_sales,
          'trending_sales'              =>          number_format($trending_sales, 2),
          'todate_otc_sales'            =>          number_format($todate_otc_sales, 2),
          'otc_sales_for_the_day'       =>          number_format($otc_sales_for_the_day, 2),
          'todate_walkin'               =>          $todate_walkin,
          'todate_regular'              =>          $todate_regular,
          'todate_transfer'             =>          $todate_transfer,
          'total_head_count'            =>          $total_head_count,
          'todate_tph'                  =>          number_format($todate_tph, 2),
          'sales_for_the_day_total_paymentgc' =>    round($sales_for_the_day_total_paymentgc, 2),
          'isr_total_paymentgc' =>    round($isr_total_paymentgc, 2),
          'todate_sales_total_paymentgc' =>    round($todate_sales_total_paymentgc, 2),
        );   
      }
    }
    
    return $data;
  }

  public function get_categories_by_brand_id($brand_id){
    return $this->db->query("SELECT c.category_code, c.category_name
    FROM category c 
    WHERE c.category_code IN (
      SELECT DISTINCT category_id 
      FROM productservice
      WHERE brand_id = $brand_id
    )
    AND c.category_status = 1
    ORDER BY c.category_name");
  }

  public function search_custom_csr_branch($category_code, $productservice_code, $start, $end){

    $branch_id = $this->session->userdata('user')->branch_id;

    if($category_code){

      if($category_code == 'All Categories'){
        if($start != $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE t.transaction_date BETWEEN '$start' AND '$end' 
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          -- AND ps.category_id != 0
          GROUP BY ps.productservice_description
          ");
        }
    
        if($start == $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE t.transaction_date = '$start'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          -- AND ps.category_id != 0
          GROUP BY ps.productservice_description
          ");
        }
      }
      else{
        if($start != $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE ps.category_id = $category_code
          AND t.transaction_date BETWEEN '$start' AND '$end'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          GROUP BY ps.productservice_description
          ");
        }
    
        if($start == $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE ps.category_id = $category_code
          AND t.transaction_date = '$start'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          GROUP BY ps.productservice_description
          ");
        }
      }

    }

    if($productservice_code){
      if($productservice_code == 'All Services'){
        if($start != $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE t.transaction_date BETWEEN '$start' AND '$end'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          -- AND ps.category_id != 0
          GROUP BY ps.productservice_description
          ");
        }
    
        if($start == $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE t.transaction_date = '$start'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          -- AND ps.category_id != 0
          GROUP BY ps.productservice_description
          ");
        }
      }
      else{
        if($start != $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE ps.productservice_code = $productservice_code
          AND t.transaction_date BETWEEN '$start' AND '$end'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          GROUP BY ps.productservice_description
          ");
        }
    
        if($start == $end){
          return $this->db->query("SELECT c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
          FROM transactiondetail td
          LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
          LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
          LEFT JOIN category c ON c.category_code = ps.category_id
          WHERE ps.productservice_code = $productservice_code
          AND t.transaction_date = '$start'
          AND t.transaction_status = 1
          AND t.branch_id = $branch_id
          GROUP BY ps.productservice_description
          ");
        }
      }
    }

  }
  
    public function search_custom_csr_ho($brand_id, $branch_id, $category_code, $productservice_code, $start, $end){

    $user_branch_id = $this->session->userdata('user')->branch_id;

    $user_id = $this->session->userdata('user')->user_id;

    $am_query = null;

    if($user_branch_id == 200){
      $am_query = "AND b.branch_areamanager = $user_id";
    }

    if($branch_id == 'All Branches'){

      $branches = $this->db->query("SELECT * FROM branch WHERE branch_status = 1 AND brand_id = $brand_id")->result();

      $data = array();

      foreach($branches as $branch){

        if($category_code){

          if($category_code == 'All Categories'){
            if($start != $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE t.transaction_date BETWEEN '$start' AND '$end' 
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
        
            if($start == $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE t.transaction_date = '$start'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              $am_query
              -- AND ps.category_id != 0
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
          }
          else{
            if($start != $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE ps.category_id = $category_code
              AND t.transaction_date BETWEEN '$start' AND '$end'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
        
            if($start == $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE ps.category_id = $category_code
              AND t.transaction_date = '$start'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
          }
    
        }
    
        if($productservice_code){
          if($productservice_code == 'All Services'){
            if($start != $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE t.transaction_date BETWEEN '$start' AND '$end'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              -- AND ps.category_id != 0
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
        
            if($start == $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td        
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE t.transaction_date = '$start'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              -- AND ps.category_id != 0
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
          }
          else{
            if($start != $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td        
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE ps.productservice_code = $productservice_code
              AND t.transaction_date BETWEEN '$start' AND '$end'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
        
            if($start == $end){
              $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
              FROM transactiondetail td       
              LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
              LEFT JOIN branch b ON b.branch_id = t.branch_id
              LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
              LEFT JOIN category c ON c.category_code = ps.category_id
              WHERE ps.productservice_code = $productservice_code
              AND t.transaction_date = '$start'
              AND t.transaction_status = 1
              AND t.branch_id = $branch->branch_id
              $am_query
              GROUP BY ps.productservice_description
              ORDER BY c.category_name, b.branch_name
              ")->result();
            }
          }
        }
      }
      // ksort($data['category_name']);

      // $data = array('branches' => $branches, 'data' => $data);
      // ksort($data['data']->category_name);
      // $data['data'] = array_combine(keys, values)
      return $data;
    }else{
      $data = array();
      if($category_code){

        if($category_code == 'All Categories'){
          if($start != $end){
            $data[] =  $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE t.transaction_date BETWEEN '$start' AND '$end' 
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            -- AND ps.category_id != 0
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
      
          if($start == $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE t.transaction_date = '$start'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            -- AND ps.category_id != 0
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
        }
        else{
          if($start != $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE ps.category_id = $category_code
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
      
          if($start == $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE ps.category_id = $category_code
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
        }

      }

      if($productservice_code){
        if($productservice_code == 'All Services'){
          if($start != $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            -- AND ps.category_id != 0
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
      
          if($start == $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td        
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE t.transaction_date = '$start'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            -- AND ps.category_id != 0
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
        }
        else{
          if($start != $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td        
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE ps.productservice_code = $productservice_code
            AND t.transaction_date BETWEEN '$start' AND '$end'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
      
          if($start == $end){
            $data[] = $this->db->query("SELECT b.branch_name, c.category_name, ps.productservice_description, SUM(td.transactiondetails_totalsales) 'transactiondetails_totalsales', COUNT(td.transactiondetails_totalsales) 'service_count'
            FROM transactiondetail td       
            LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
            LEFT JOIN branch b ON b.branch_id = t.branch_id
            LEFT JOIN productservice ps ON td.productservice_id = ps.productservice_id
            LEFT JOIN category c ON c.category_code = ps.category_id
            WHERE ps.productservice_code = $productservice_code
            AND t.transaction_date = '$start'
            AND t.transaction_status = 1
            AND t.branch_id = $branch_id
            $am_query
            GROUP BY ps.productservice_description
            ORDER BY c.category_name
            ")->result();
          }
        }
      }
      return $data;
    }
    
  }


  public function get_productservices_by_brand_id($brand_id){
    return $this->db->query("SELECT * 
    FROM productservice
    WHERE productservice_status = 1
    AND brand_id = $brand_id
    ORDER BY productservice_description");
  }

  public function get_branches_by_branch_id($brand_id){
    
    $branch_id = $this->session->userdata('user')->branch_id;

    $user_id = $this->session->userdata('user')->user_id;

    if($branch_id == 200){

        return $this->db->query("SELECT * 
        FROM branch
        WHERE branch_areamanager = $user_id
        AND branch_status = 1
        AND brand_id = $brand_id
        ORDER BY branch_name");
    }

    return $this->db->query("SELECT * 
    FROM branch
    WHERE branch_status = 1
    AND brand_id = $brand_id
    ORDER BY branch_name");
  }

  public function get_variance($branch_id, $month, $year){
    $month_start = $year.'-'.$month.'-'.'01';
    $month_end = date('Y-m-t', strtotime($month_start));

    $batch_no = $this->db->query("SELECT branch_batchno
                                          FROM branch
                                          WHERE branch_id = $branch_id"
                                )->row();

    if ($batch_no->branch_batchno == 1) {
        $beginning_date = date('Y-m-t', strtotime('-1 months', strtotime($month_start)));
        $delivered_start =  date('Y-m-01', strtotime($month_start));
        $delivered_end = date('Y-m-t', strtotime($month_start));
        $ending_date = date('Y-m-t', strtotime($month_start));
    }elseif ($batch_no->branch_batchno == 2) {
        $beginning_date = date('Y-m-15', strtotime($month_start));
        $delivered_start =  date('Y-m-16', strtotime($month_start));
        $delivered_end = date('Y-m-14', strtotime('+1 months', strtotime($month_start)));
        $ending_date = date('Y-m-15', strtotime('+1 months', strtotime($month_start)));
    }

    $beginning_inventory = $this->db->query("SELECT br.branch_name, c.category_name, i.item_code, i.item_name, i.item_shade, i.item_color, i.item_model, i.item_uom, cc.currentinventory_quantity, i.item_uomsize ,SUM(ROUND(cc.currentinventory_quantity/ i.item_uomsize,2)) as beginning_quantity, i.item_uom,  cc.currentinventory_dateupdated
      FROM currentinventory_copy cc
      LEFT JOIN item i on cc.item_id = i.item_id
      LEFT JOIN category c on c.category_id = i.category_id
      LEFT JOIN branch br on br.branch_id = cc.branch_id
      WHERE br.branch_id = $branch_id
      AND cc.currentinventory_dateupdated = '$beginning_date'
      GROUP BY i.item_name
      ORDER BY i.item_name")->result();

    $ending_inventory = $this->db->query("SELECT i.item_code, i.item_name, i.item_uom, cc.currentinventory_quantity, i.item_uomsize,ROUND(cc.currentinventory_quantity/ i.item_uomsize,2) as ending_inventory, i.item_uom
       FROM item i
       LEFT JOIN category c on c.category_id = i.category_id
       LEFT JOIN currentinventory_copy cc on cc.item_id = i.item_id
       LEFT JOIN branch br on br.branch_id = cc.branch_id
       WHERE br.branch_id = $branch_id
       AND cc.currentinventory_dateupdated = '$ending_date'
       GROUP BY i.item_name
       ORDER BY i.item_name ASC")->result();

    $delivery = $this->db->query("SELECT SUM(drd.deliveryreceiptdetail_quantity) as temp , ROUND(SUM(drd.deliveryreceiptdetail_quantity) * i.item_uomsize, 2) as deliveryreceiptdetail_quantity , i.item_uomsize , cc.currentinventory_quantity , i.item_uom, i.item_name, br.branch_id, i.item_code 
       FROM item i
       LEFT JOIN category c on c.category_id = i.category_id
       LEFT JOIN currentinventory_copy cc on cc.item_id = i.item_id
       LEFT JOIN branch br on br.branch_id = cc.branch_id
       LEFT JOIN deliveryreceiptdetail drd on drd.item_code = i.item_code
       LEFT JOIN deliveryreceipt dr on dr.deliveryreceipt_id = drd.deliveryreceipt_id
       WHERE br.branch_id = $branch_id
       AND dr.deliveryreceipt_date BETWEEN '$delivered_start' AND '$delivered_end'
       AND cc.currentinventory_quantity > 0
       GROUP BY i.item_name
       ORDER BY i.item_name")->result();

    $theoretical = $this->db->query("SELECT i.item_name, i.item_code, t.transaction_date, i.item_uomsize, SUM(sm.servicemix_quantity) as used_quantity,
      ROUND(SUM(sm.servicemix_quantity) / i.item_uomsize, 2) as theoretical_used
      FROM transaction t
      LEFT JOIN transactiondetail td on td.transaction_id = t.transaction_id
      LEFT JOIN productservice ps on td.productservice_id = ps.productservice_id
      LEFT JOIN servicemix sm on sm.productservice_code = ps.productservice_code
      LEFT JOIN item i on i.item_id = sm.item_id
      WHERE t.transaction_date BETWEEN '$month_start' AND '$month_end'
      AND t.branch_id = $branch_id
      GROUP BY i.item_id
      ORDER by i.item_name ASC")->result();

      // foreach ($ending_inventory as $end) {
      //   foreach ($beginning_inventory as $item) {
      //     if ($item->item_code == $end->item_code) {
      //       $item->ending_inventory = number_format((float)$end->ending_inventory, 2, '.', '');
      //     }
      //   }
      // }

      // foreach ($delivery as $delivery_qty) {
      //   foreach ($items as $item) {
      //     if ($item->item_code == $delivery_qty->item_code) {
      //           $item->deliveryreceiptdetail_quantity = number_format((float)$delivery_qty->temp, 2, '.', '');
      //     }
      //   }
      // }

      // foreach ($theoretical as $theoretical_qty) {
      //     foreach ($items as $item) {
      //         if ($item->item_code == $theoretical_qty->item_code) {
      //             $item->theoretical = number_format((float)($theoretical_qty->used_quantity/ $item->item_uomsize), 2, '.', '');
      //         }
      //     }
      // }

      // foreach ($items as $row) {
      //     if ($row->deliveryreceiptdetail_quantity == 0) {
      //       $row->deliveryreceiptdetail_quantity = "";
      //     }
      // }  


    $data['delivered_start'] = $delivered_start;
    $data['delivered_end'] = $delivered_end;
    $data['beginning_inventory_date'] = $beginning_date;
    $data['ending_inventory_date'] = $ending_date;
    $data['beginning_inventory'] = $beginning_inventory;
    $data['ending_inventory'] = $ending_inventory;
    $data['delivery'] = $delivery;
    $data['theoretical'] = $theoretical;


    return $data;
  }

  public function fetch_asm_brands($user_id){

     $query = $this->db->query("SELECT DISTINCT br.brand_id, br.brand_name
      FROM branch b
      LEFT JOIN brand br ON br.brand_id = b.brand_id
      where branch_areamanager = $user_id
      ORDER BY br.brand_name ASC")->result();

      $output = '<option value="" hidden selected>Select Brand</option>';


      foreach ($query as $row) {

        $output .= '<option value = "'.$row->brand_id.'">'.$row->brand_name.'</option>';

      }

      return $output;

  }


  public function search_custom_dsr_asm_branches($date){

    $user_id = $this->session->userdata('user')->user_id;

    $month = date('m', strtotime($date));
    $current_day = date('d', strtotime($date));
    $year = date('Y', strtotime($date));
    $day = 0;

    switch($month){
      case '01':
        $day = '31';
        break;
        
      case '02':
        $day = '28';
        break;

      case '03':
        $day = '31';
        break;
      
      case '04':
        $day = '30';
        break;

      case '05':
        $day = '31';
        break;

      case '06':
        $day = '30';
        break;
      
      case '07':
        $day = '31';
        break;

      case '08':
        $day = '31';
        break;

      case '09':
        $day = '30';
        break;

      case '10':
        $day = '31';
        break;

      case '11':
        $day = '30';
        break;

      case '12':
        $day = '31';
        break;
    }

    $start = $year.'-'.$month.'-'.'01';
    $end = $year.'-'.$month.'-'.$day;

    $asm_branches = $this->db->query("SELECT * 
    FROM branch 
    WHERE branch_areamanager = $user_id
    AND branch_id IN (
      SELECT DISTINCT transaction.branch_id
      FROM transaction
      WHERE transaction_status = 1
      AND transaction_date BETWEEN '$start' AND '$end'
    )
    ORDER BY branch_name ASC")->result();

    $data = array();

    foreach($asm_branches as $asm_branch){

      $dsr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, st.storetarget_amount, SUM(t.transaction_totalsales) 'total_sales'
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      LEFT JOIN brand br ON br.brand_id = b.brand_id
      LEFT JOIN storetarget st ON st.branch_id = b.branch_id
      WHERE t.branch_id = $asm_branch->branch_id 
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      ")->row();

      $sales_for_the_day = $this->db->query("SELECT SUM(t.transaction_totalsales) 'sales_for_the_day', SUM(t.transaction_paymentcash) 'total_cash', SUM(t.transaction_paymentcard) 'total_card', SUM(t.transaction_paymentgc) 'total_gc'  
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_status = 1
      AND t.transaction_date = '$date'")->row();

      $trending_sales = ($dsr->total_sales / $current_day) * $day;

      $percentage1 = 0;
        
      if($dsr->total_sales && $dsr->storetarget_amount){
        $percentage1 = ($dsr->total_sales / $dsr->storetarget_amount) * 100;
      }

      $retail_target_sales = $dsr->storetarget_amount * .10;

      $retail_sales_for_the_day = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'retail_sales_for_the_day'
      FROM transactiondetail td 
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date = '$date'
      AND t.transaction_status = 1
      AND td.productretail_id IS NOT NULL")->row()->retail_sales_for_the_day;
      
      $total_turnaway = $this->db->query("SELECT SUM(ta.turnaway_quantity) 'total_turnaway'
      FROM turnaway ta
      WHERE ta.turnaway_date BETWEEN '$start' AND '$end'
      AND ta.branch_id = $asm_branch->branch_id
      AND ta.turnaway_status = 1")->row()->total_turnaway;

      $retail_total_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) 'total_sales_retail'
      FROM transactiondetail td 
      LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      AND td.productretail_id IS NOT NULL")->row()->total_sales_retail;
      
      $percentage2 = 0;
      
      if($retail_total_sales && $retail_target_sales){
          $percentage2 = ($retail_total_sales / $retail_target_sales) * 100;
      }

      $todate_walkin = $this->db->query("SELECT DISTINCT t.transaction_date, sp.serviceprovider_name, t.customer_id
      FROM transactiondetail td
      LEFT JOIN transaction t ON td.transaction_id = t.transaction_id
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      -- $am_query
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Walk-in'")->num_rows();

      $todate_regular = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Regular'")->num_rows();

      $todate_transfer = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1
      AND t.transaction_customerstatus = 'Transfer'")->num_rows();

      $total_head_count = $this->db->query("SELECT DISTINCT t.transaction_docketno
      FROM transaction t
      LEFT JOIN branch b ON b.branch_id = t.branch_id
      WHERE b.branch_id = $asm_branch->branch_id
      AND t.transaction_date BETWEEN '$start' AND '$end'
      AND t.transaction_status = 1")->num_rows();

      $todate_tph = 0;
        
        if($dsr->total_sales){
            $todate_tph = ($dsr->total_sales/$total_head_count);   
        }

      $todate_lost_sales = 0;
    
        if($total_turnaway){
          $todate_lost_sales = ($total_turnaway*$todate_tph);    
      }

      $data[] = array(
        'brand'                       =>          $dsr->brand_name,
        'branch'                      =>          $dsr->branch_name,
        'revenue_target'              =>          $dsr->storetarget_amount,
        'sales_for_the_day'           =>          $sales_for_the_day->sales_for_the_day,
        'total_cash'                  =>          $sales_for_the_day->total_cash,
        'total_card'                  =>          $sales_for_the_day->total_card,
        'total_gc'                    =>          $sales_for_the_day->total_gc,
        'total_sales'                 =>          $dsr->total_sales,
        'trending_sales'              =>          number_format($trending_sales, 2),
        'percentage1'                 =>          number_format($percentage1, 2),
        'retail_target_sales'         =>          number_format($retail_target_sales, 2),
        'retail_sales_for_the_day'    =>          $retail_sales_for_the_day,
        'retail_total_sales'          =>          $retail_total_sales,
        'percentage2'                 =>          number_format($percentage2, 2),
        'todate_walkin'               =>          $todate_walkin,
        'todate_regular'              =>          $todate_regular,
        'todate_transfer'             =>          $todate_transfer,
        'total_head_count'            =>          $total_head_count,
        'total_turnaway'              =>          $total_turnaway,
        'todate_tph'                  =>          number_format($todate_tph, 2),
        'todate_lost_sales'           =>          number_format($todate_lost_sales, 2)
      );

    }


    return $data;
      
  }

  public function get_per_brand_sales($start){

		$end = date('Y-m-t', strtotime($start));
		$data = array();

		$data['rows'] = $this->db->query("SELECT A.brand_name, A.total_storetarget_original, B.transaction_totalsales FROM(
				SELECT SUM(st.storetarget_original) AS total_storetarget_original, br.brand_name 
				FROM storetarget st
				LEFT JOIN branch b ON b.branch_id = st.branch_id
				LEFT JOIN brand br ON br.brand_id = b.brand_id 
				WHERE st.storetarget_month BETWEEN '$start' AND '$end'
				GROUP BY br.brand_name
			) AS A
	
			LEFT JOIN(
				SELECT br.brand_name, SUM(t.transaction_totalsales) AS transaction_totalsales
				FROM transaction t 
				LEFT JOIN branch b ON b.branch_id = t.branch_id 
				LEFT JOIN brand br ON br.brand_id = b.brand_id 
				WHERE b.brand_id != 100007 
				AND transaction_date BETWEEN '$start' AND '$end'
        AND transaction_status = 1
				GROUP BY br.brand_name
			) AS B
			ON A.brand_name = B.brand_name
			ORDER BY B.transaction_totalsales DESC
		")->result();

		// $data['days_count'] = date('t', strtotime($start));

		return $data;
  }

  public function get_per_branch_sales($start, $branch_id){

		$end = date('Y-m-t', strtotime($start));

		return $this->db->query("SELECT A.branch_name, A.storetarget_original, B.transaction_totalsales FROM(
				SELECT st.storetarget_original, b.branch_name 
				FROM storetarget st
				LEFT JOIN branch b ON b.branch_id = st.branch_id
				WHERE st.storetarget_month BETWEEN '$start' AND '$end'
        AND st.branch_id = $branch_id
			) AS A
	
			LEFT JOIN(
				SELECT b.branch_name, SUM(t.transaction_totalsales) AS transaction_totalsales
				FROM transaction t 
				LEFT JOIN branch b ON b.branch_id = t.branch_id 
				WHERE transaction_date BETWEEN '$start' AND '$end'
        AND transaction_status = 1
        AND t.branch_id = $branch_id
				GROUP BY b.branch_name
			) AS B
			ON A.branch_name = B.branch_name
		")->result();
  }


    public function fetch_services_by_brand_id($brand_id){

      $query = $this->db->query("SELECT * FROM productservice WHERE brand_id = $brand_id AND productservice_status = 1")->result();

      $output = '<option value="" hidden selected>Select Service</option>';

      // $output .= '<option value = "All Branches">All Branches</option>';

      foreach ($query as $row) {

        $output .= '<option value = "'.$row->productservice_id.'">'.$row->productservice_name.' - '.$row->productservice_amount;'</option>';

      }

      return $output;

  }


}