<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Commission_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }

    public function get(){

    	$query = $this->db->query("SELECT  br.branch_name, b.brand_name, sp.serviceprovider_name,

								FORMAT(SUM(td.transactiondetails_totalsales),2) as total_sales,

								FORMAT(SUM(ps.productservice_amount),2) as total_service,

								SUM(pr.productretail_amount) as total_retail,

								FORMAT(SUM(0),2) as service_incentives,

								FORMAT(SUM(0),2) as otc_commission,

								FORMAT(SUM(0),2) as total_commission

								from serviceprovider sp 

								INNER JOIN transactiondetail td on td.serviceprovider_id = sp.serviceprovider_id

								LEFT OUTER JOIN productservice ps on ps.productservice_id = td.productservice_id

								LEFT OUTER JOIN productretail pr on pr.productretail_id = td.productretail_id

								LEFT OUTER JOIN branch br on br.branch_id = sp.branch_id

								LEFT OUTER JOIN brand b on b.brand_id = br.brand_id

								LEFT OUTER JOIN transaction t on t.transaction_id = td.transaction_id

								GROUP BY sp.serviceprovider_name  

								ORDER BY `total_sales`  DESC");

		return $query->result();

    }

  

  	public function get_brands(){

  		$query = $this->db->query("SELECT brand_id, brand_name FROM brand WHERE brand_id != 100007");

  		return $query->result();

  	}



  	public function fetch_branches($brand_id){

  		$this->db->select("branch_id,branch_name");

  		$this->db->where("brand_id" , $brand_id);

  		$this->db->where("branch_status", 1);

  		$query = $this->db->get("branch");

  		$output = '<option value="">Select Branch</option>';

  		$output .= '<option value="'.$brand_id.'">All Branches</option>';

  		foreach ($query->result() as $row) {

  			$output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

  		}

  		return $output;

  	}

  	public function get_commission($brand_id, $branch_id, $start, $end, $commission_type, $start1, $end1){
  		if ($commission_type == 'otc') {
  			if ($branch_id == 100001 || $branch_id == 100002 || $branch_id == 100003 || $branch_id == 100004 || $branch_id == 100005 || $branch_id == 100006 || $branch_id == 100007 || $branch_id == 100008 || $branch_id == 100009) {
  				$data = $this->get_otc_total_allbranches($brand_id, $branch_id, $start1, $end1);
  			}else{
  				$data = $this->get_otc_total($brand_id, $branch_id, $start1, $end1);
  			}
  			return $data;
  		}elseif ($commission_type == 'service') {
  			if ($brand_id == 100003 || $branch_id == 75 || $branch_id == 76 || $branch_id == 77 || $branch_id == 78 || $branch_id == 79 || 
  				$branch_id == 80 || $branch_id == 74 || $branch_id == 81 || $branch_id == 82 || $branch_id == 83 || $branch_id == 95 ||    
  				$branch_id == 119 || $branch_id == 121 || $branch_id == 126 || $branch_id == 201 ) {
  				$data = $this->get_service_sb($brand_id, $branch_id, $start, $end);
  			}else{
  				$data = $this->get_service_total($brand_id, $branch_id, $start, $end);
  			}
  			return $data;
  		}elseif ($commission_type == 'all') {
  			if ($branch_id == 100001 || $branch_id == 100002 || $branch_id == 100004 || $branch_id == 100005 || $branch_id == 100006 || $branch_id == 100007 || $branch_id == 100008 || $branch_id == 100009 ) {
  				$data = $this->get_total_allbranches($brand_id, $branch_id, $start, $end, $start1, $end1);
  			}elseif ( $branch_id == 75 || $branch_id == 76 || $branch_id == 77 || $branch_id == 78 || 
  				$branch_id == 79 || $branch_id == 80 || $branch_id == 74 || $branch_id == 81 || $branch_id == 82 || $branch_id == 83 
  				|| $branch_id == 95 || $branch_id == 119 || $branch_id == 121 || $branch_id == 126 || $branch_id == 201) {
  				$data = $this->get_total_all_sb_per_branch($brand_id, $branch_id, $start, $end, $start1, $end1);
  			}elseif ($brand_id == 100003 ) {
  				$data = $this->get_total_all_sb_all_branch($brand_id, $branch_id, $start, $end, $start1, $end1);
  			}else{
  				$data = $this->get_total_all($brand_id, $branch_id, $start, $end, $start1, $end1);
  			}
  			return $data;
  		}
  	}
  	public function get_service_total($brand_id, $branch_id, $start, $end){
  		if ($branch_id == 100001 || $branch_id == 100002 || $branch_id == 100003 || $branch_id == 100004 || $branch_id == 100005 || 
  			$branch_id == 100006 || $branch_id == 100007 || $branch_id == 100008 || $branch_id == 100009 ) {
  			$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
		      FROM serviceprovider sp
		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
		      LEFT JOIN branch b ON b.branch_id = sp.branch_id 
		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id
		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
		      OR SUM(td.transactiondetails_totalsales) > 0
		      ")->result();
	    	  $data = array();
	    	  
	    	    foreach($serviceproviders as $serviceprovider){
			        $serviceprovider_id = $serviceprovider->serviceprovider_id;
			     
			        $isr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount)  'total_service', SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,
						SUM(0) as otc_commission,
						SUM(0) as total_commission
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        ")->row();
			        // ISR Total GC
			        $isr_total_paymentgc = 0;
			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
			        FROM transaction t
			        RIGHT JOIN transactiondetail td ON t.transaction_id = td.transaction_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id
					RIGHT  JOIN productretail pr on pr.productretail_id = td.productretail_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_paymentgc > 0
			        AND t.transaction_totalsales > 0
			        ")->result();
			        //payment gc
			        foreach($isr_paymentgc as $paymentgc){
			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
			          FROM transactiondetail
			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();
			          if($sp_count == 1){
			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;
			          }
			          else{
			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
			            FROM transactiondetail
			            WHERE transaction_id = $paymentgc->transaction_id AND
			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;
			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
			            FROM transactiondetail
			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;
			            $sales_percentage = $sp_total_sales / $total_sales;
			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);
			          }
			        }   // end payment gc
			        // GRAND TOTAL SALES GC DEDUCTED
			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);
			        if($isr->brand_name){
			            $data[] = (object)array(
			              'brand_name'                       =>          $isr->brand_name,
			              'branch_name'                      =>          $isr->branch_name,

			              'serviceprovider_name'        =>          $isr->serviceprovider_name,

			              'total_sales'                 =>          $grand_total_sales,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr->total_service,

			              'total_retail' =>    0,

			              'service_incentives' => $isr->service_incentives,

			              'otc_commission' => $isr->otc_commission,

			              'total_commission' => $isr->total_commission

			            );   

			        } //end if

			    } //end foreach serviceproviders

			   
  		}else{

	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
		      FROM serviceprovider sp
		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
		      WHERE sp.branch_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();

			      



	    	  $data = array();

			      



	    	  	// foreach serviceproviders

	    	    foreach($serviceproviders as $serviceprovider){



			        $serviceprovider_id = $serviceprovider->serviceprovider_id;



			        $isr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount)  'total_service', SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        INNER JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id

			        INNER JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();

			        



			        // ISR Total GC

			        $isr_total_paymentgc = 0;

			     



			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc

			        FROM transaction t

			        INNER JOIN transactiondetail td ON t.transaction_id = td.transaction_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_paymentgc > 0

			        AND t.transaction_totalsales > 0

			        ")->result();

			      



			        //payment gc

			        foreach($isr_paymentgc as $paymentgc){

			



			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 

			          FROM transactiondetail

			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();



			          if($sp_count == 1){

			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;

			          }

			          else{

			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id AND

			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

			            $sales_percentage = $sp_total_sales / $total_sales;

			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);



			          }

			    



			        }   // end payment gc





			        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

			   

			        if($isr->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr->brand_name,

			              'branch_name'                      =>          $isr->branch_name,

			              'serviceprovider_name'        =>          $isr->serviceprovider_name,

			              'total_sales'                 =>          $grand_total_sales,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr->total_service,

			              'total_retail' =>    0,

			              'service_incentives' => $isr->service_incentives,

			              'otc_commission' => $isr->otc_commission,

			              'total_commission' => $isr->total_commission

			            );   

			        } //end if

			    } //end foreach serviceproviders

			    

		}

		return $data;

  		// print_r($data);
  	}

  	public function test_model($brand_id, $branch_id, $start, $end){
  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level
		      FROM serviceprovider sp
		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
		      WHERE sp.branch_id = $branch_id
		      GROUP BY sp.serviceprovider_id
		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
		      OR SUM(td.transactiondetails_totalsales) > 0
		      ")->result();
	  			foreach ($serviceproviders as $serviceprovider) {
	  				$serviceprovider_id = $serviceprovider->serviceprovider_id;
	  				$isr = $this->db->query("SELECT  b.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount)  'total_service', SUM(pr.productretail_amount) as total_retail, SUM(0) as service_incentives, SUM(0) as otc_commission, SUM(0) as total_commission
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        ")->row();

			        $haircut_men = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_men',

						COUNT(t.transaction_id) as men_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id = 10298
			        ")->row();

			        $haircut_men_sp = $this->db->query("SELECT t.transaction_id, ps.productservice_amount 'technical_sales'
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id = 10298")->result();
			        $haircut_men_sp_deduc = 0;
			        foreach ($haircut_men_sp as $sp_haircut_men) {
			        	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				          FROM transactiondetail
				          WHERE transaction_id = $sp_haircut_men->transaction_id")->num_rows();
			        	if ($get_sp != 1) {
			        	  $haircut_men_sp_deduc += 1;
			        	}
			        }

			        $haircut_childWomen = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_childWomen',

						COUNT(t.transaction_id) as childWomen_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10372, 10373)
			        ")->row();

			        $haircut_childWomen_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10372, 10373)")->result();
			        $haircut_childWomen_sp_deduc = 0;
			        foreach ($haircut_childWomen_sp as $sp_haircut_childWomen) {
			        	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				          FROM transactiondetail
				          WHERE transaction_id = $sp_haircut_childWomen->transaction_id")->num_rows();
			        	if ($get_sp != 1) {
			        	  $haircut_childWomen_sp_deduc += 1;
			        	}
			        }


			        $technical_sales = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_name,  SUM(ps.productservice_amount) 'technical_sales',
	
						COUNT(t.transaction_id) as technical_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)
			        ")->row();

			        $technical_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			        $technical_sp_deduction = 0;
			        foreach ($technical_sp as $sp_technical) {
			        	
			        	$technical_sp_count  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				          FROM transactiondetail
				          WHERE transaction_id = $sp_technical->transaction_id")->num_rows();
			        	if ($technical_sp_count != 1) {
			        	  $technical_sp_deduction += 1;
			        	}
			        }



			        $other_sales = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) as 
			        	'other_sales',
	
						COUNT(t.transaction_id) as other_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id NOT IN (10298, 10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10372, 10373)
			        ")->row();

			        $other_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			        $other_sp_deduction = 0;
			        foreach ($other_sp as $sp_other) {
			        	
			        	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				          FROM transactiondetail
				          WHERE transaction_id = $sp_other->transaction_id")->num_rows();
			        	if ($get_sp != 1) {
			        	  $other_sp_deduction += 1;
			        	}
			        }

			        if($isr->brand_name){

			            $data[] = (object)array(
			            	
			            'brand_id' => $isr->brand_id,
			              'brand_name'                       =>          $isr->brand_name,

			              'branch_name'                      =>          $isr->branch_name,

			              'serviceprovider_name'        =>          $isr->serviceprovider_name,
			  
			              // 'total_sales'                 =>          $grand_total_sales,
			              // // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),
			              'total_service' =>    $isr->total_service,
			              'total_retail' =>    $isr->total_retail,
			              'service_incentives' => $isr->service_incentives,
			              'otc_commission' => $isr->otc_commission,
			              'total_commission' => $isr->total_commission,
			              'haircut_men' => $haircut_men->haircut_men,
			              'haircut_childWomen' => $haircut_childWomen->haircut_childWomen,
			              'technical_sales' => $technical_sales->technical_sales,
			              'men_count' => $haircut_men->men_count,
			              'childWomen_count' => $haircut_childWomen->childWomen_count,
			              'technical_count' => $technical_sales->technical_count,
			              'other_sales' => $other_sales->other_sales,
			              'other_count' => $other_sales->other_count,
			           	  'technical_deduc_count' => $technical_sp_deduction,
			           	  'haircut_childWomen_sp_deduc' => $haircut_childWomen_sp_deduc,
			           	  'haircut_men_sp_deduc' => $haircut_men_sp_deduc,
			           	  'other_sp_deduction' => $other_sp_deduction
			            );   

			        } //end if

	  			}
	  	return $data;

  	}



  	public function get_service_sb($brand_id, $branch_id, $start, $end){

  		
  		if ($branch_id == 100003 ) {
  			$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level
		      FROM serviceprovider sp
		      INNER JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
		      LEFT JOIN branch b ON b.branch_id = sp.branch_id 
		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id
		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
		      OR SUM(td.transactiondetails_totalsales) > 0
		      ")->result();
	    	  $data = array();
	    	  	// foreach serviceproviders

	    	    foreach($serviceproviders as $serviceprovider){
			        $serviceprovider_id = $serviceprovider->serviceprovider_id;
			     
			        $isr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount)  'total_service', SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						ps.productservice_code
			        FROM transactiondetail td
			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0")->row();

			        $haircut_sales = $this->db->query("SELECT SUM(td.transactiondetails_totalsales) as total, SUM(ps.productservice_amount) as service, ps.productservice_code
			        FROM transactiondetail td
			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND ps.productservice_code = 10298
			        OR ps.productservice_code = 10372
			        OR ps.productservice_code = 10373")->row();

			        // ISR Total GC
			        $isr_total_paymentgc = 0;
			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc
			        FROM transaction t
			        LEFT JOIN transactiondetail td ON t.transaction_id = td.transaction_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_paymentgc > 0
			        AND t.transaction_totalsales > 0
			        ")->result();
			        //payment gc
			        foreach($isr_paymentgc as $paymentgc){
			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 
			          FROM transactiondetail
			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();
			          if($sp_count == 1){
			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;
			          }
			          else{
			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 
			            FROM transactiondetail
			            WHERE transaction_id = $paymentgc->transaction_id AND
			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;
			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 
			            FROM transactiondetail
			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;
			            $sales_percentage = $sp_total_sales / $total_sales;
			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);

			          }
			     
			        }   // end payment gc

			        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

			        if($isr->brand_name){

			            $data[] = (object)array(
			              'brand_name'                       =>          $isr->brand_name,
			              'branch_name'                      =>          $isr->branch_name,
			              'serviceprovider_name'        =>          $isr->serviceprovider_name,
			              'total_sales'                 =>          $grand_total_sales,
			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),
			              'total_service' =>    $isr->total_service,
			              'total_retail' =>    0,
			              'service_incentives' => $isr->service_incentives,
			              'otc_commission' => $isr->otc_commission,
			              'total_commission' => $isr->total_commission,
			              'haircut_sales' => $haircut_sales->service
			            );   

			        } //end if

			    } //end foreach serviceproviders

  		}else{
	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level
		      FROM serviceprovider sp
		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
		      WHERE sp.branch_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id
		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
		      OR SUM(td.transactiondetails_totalsales) > 0
		      ")->result();
  		 
	  		 $isr = $this->db->query("SELECT  sp.serviceprovider_id, b.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount)  'total_service', 
	  		 	SUM(pr.productretail_amount) as total_retail, FORMAT(0,2) as service_incentives, FORMAT(0,2) as otc_commission, FORMAT(0,2) as total_commission, SUM(0) as haircut_men, SUM(0) as haircut_childWomen, SUM(0) as technical_sales , SUM(0) as other_sales , SUM(0) as haircut_men_sp_deduc, SUM(0) as haircut_childWomen_sp_deduc, SUM(0) as technical_deduc_count, FORMAT(0,0) as sp_level,
	  		 		SUM(0) as other_sp_deduction

				         FROM transactiondetail td
				         RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
				         LEFT JOIN branch b ON b.branch_id = t.branch_id
				         LEFT JOIN brand br ON br.brand_id = b.brand_id
				         LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
				 		 LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
				         RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
				         WHERE t.branch_id = $branch_id 
				         AND t.transaction_date BETWEEN '$start' AND '$end'
				         AND t.transaction_status = 1
				         GROUP BY sp.serviceprovider_id

				     ")->result();
  		 	
			        $haircut_childWomen = $this->db->query("SELECT sp.serviceprovider_id,  SUM(ps.productservice_amount) 
			        	'haircut_childWomen',COUNT(t.transaction_id) as childWomen_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10372, 10373)
                    GROUP by sp.serviceprovider_id
			        ")->result();
  		

					$haircut_men = $this->db->query("SELECT sp.serviceprovider_id, SUM(ps.productservice_amount) 'haircut_men'
					        FROM transactiondetail td
					        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
					        LEFT JOIN branch b ON b.branch_id = t.branch_id
					        LEFT JOIN brand br ON br.brand_id = b.brand_id
					        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
							LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
					        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
					        WHERE t.branch_id = $branch_id
					        AND t.transaction_date BETWEEN '$start' AND '$end'
					        AND t.transaction_status = 1
					        AND t.transaction_totalsales > 0
					        AND td.productservice_id = 10298
		                    GROUP by sp.serviceprovider_id
					        ")->result();

					$technical_sales = $this->db->query("SELECT sp.serviceprovider_id,  SUM(ps.productservice_amount) 'technical_sales',COUNT(t.transaction_id) as technical_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)
			        GROUP by sp.serviceprovider_id
			        ")->result();

			        $other_sales = $this->db->query("SELECT sp.serviceprovider_id,  SUM(ps.productservice_amount) as 
			        	'other_sales',COUNT(t.transaction_id) as other_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id NOT IN (10298, 10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10372, 10373)
			        GROUP by sp.serviceprovider_id
			        ")->result();
			        
			  //       $haircut_men_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.branch_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id = 10298")->result();


			   //      $get_sp = $this->db->query("SELECT serviceprovider_id,transaction_id, COUNT(serviceprovider_id) 
						// 						FROM transactiondetail
						// 						WHERE transaction_id IN (SELECT t.transaction_id
						// 									        FROM transactiondetail td
						// 									        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
							
						// 									        WHERE t.branch_id = $branch_id
						// 									        AND t.transaction_date BETWEEN '$start' AND '$end'
						// 									        AND t.transaction_status = 1
						// 									        AND t.transaction_totalsales > 0
						// 						                    AND td.productservice_id = 10298
						// 									        )
						// 						GROUP BY transaction_id
						// 						HAVING COUNT(DISTINCT serviceprovider_id) > 1
						// ")->result();

		// 	        $get_sp_childwoman = $this->db->query("SELECT serviceprovider_id,transaction_id, COUNT(serviceprovider_id) 
		// 										FROM transactiondetail
		// 										WHERE transaction_id IN (SELECT t.transaction_id
		// 													        FROM transactiondetail td
		// 													        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
															     
		// 													        WHERE t.branch_id = $branch_id
		// 													        AND t.transaction_date BETWEEN '$start' AND '$end'
		// 													        AND t.transaction_status = 1
		// 													        AND t.transaction_totalsales > 0
		// 										                    AND td.productservice_id IN (10372, 10373)
		// 													        )
		// 										GROUP BY transaction_id
		// 										HAVING COUNT(DISTINCT serviceprovider_id) > 1
		// 				")->result();
		// 	        $get_sp_technical = $this->db->query("SELECT serviceprovider_id,transaction_id, COUNT(serviceprovider_id) 
		// 										FROM transactiondetail
		// 										WHERE transaction_id IN (SELECT t.transaction_id
		// 													        FROM transactiondetail td
		// 													        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
															       
		// 													        WHERE t.branch_id = $branch_id
		// 													        AND t.transaction_date BETWEEN '$start' AND '$end'
		// 													        AND t.transaction_status = 1
		// 													        AND t.transaction_totalsales > 0
		// 										                    AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)
		// 													        )
		// 										GROUP BY transaction_id
		// 										HAVING COUNT(DISTINCT serviceprovider_id) > 1
		// 				")->result();

		// 	        $get_sp_other = $this->db->query("SELECT serviceprovider_id,transaction_id, COUNT(serviceprovider_id) 
		// 										FROM transactiondetail
		// 										WHERE transaction_id IN (SELECT t.transaction_id
		// 													        FROM transactiondetail td
		// 													        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
		// 													        WHERE t.branch_id = $branch_id
		// 													        AND t.transaction_date BETWEEN '$start' AND '$end'
		// 													        AND t.transaction_status = 1
		// 													        AND t.transaction_totalsales > 0
		// 										                    AND td.productservice_id NOT IN (10298, 10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10372, 10373)
		// 													        )
		// 										GROUP BY transaction_id
		// 										HAVING COUNT(DISTINCT serviceprovider_id) > 1
		// 				")->result();


					foreach ($isr as $sp_sales) {
						foreach ($haircut_men as $men) {
							if ($sp_sales->serviceprovider_id == $men->serviceprovider_id) {
								$sp_sales->haircut_men = $men->haircut_men;
							}
						}
						foreach ($haircut_childWomen as $childWomen) {
							if ($sp_sales->serviceprovider_id == $childWomen->serviceprovider_id) {
								$sp_sales->haircut_childWomen = $childWomen->haircut_childWomen;
							}
						}
						foreach ($technical_sales as $technical) {
							if ($sp_sales->serviceprovider_id == $technical->serviceprovider_id) {
								$sp_sales->technical_sales = $technical->technical_sales;
							}
						}
						foreach ($other_sales as $other) {
							if ($sp_sales->serviceprovider_id == $other->serviceprovider_id) {
								$sp_sales->other_sales = $other->other_sales;
							}
						}
		// 				foreach ($get_sp as $men_cut_sp_deduct) {
		// 					if ($sp_sales->serviceprovider_id == $men_cut_sp_deduct->serviceprovider_id) {
		// 						$sp_sales->haircut_men_sp_deduc += 1;
		// 					}
		// 				}
						
		// 				foreach ($get_sp_childwoman as $childwoman_sp_deduction_count) {
		// 					if ($sp_sales->serviceprovider_id == $childwoman_sp_deduction_count->serviceprovider_id) {
		// 						$sp_sales->haircut_childWomen_sp_deduc += 1;
		// 					}
		// 				}
						
		// 				foreach ($get_sp_technical as $technical_deduc_count) {
		// 					if ($sp_sales->serviceprovider_id == $technical_deduc_count->serviceprovider_id) {
		// 						$sp_sales->technical_deduc_count += 1;
		// 					}
		// 				}

		// 				foreach ($get_sp_other as $other_sp_deduction) {
		// 					if ($sp_sales->serviceprovider_id == $other_sp_deduction->serviceprovider_id) {
		// 						$sp_sales->other_sp_deduction += 1;
		// 					}
		// 				}
						foreach ($serviceproviders as $sp) {
							if ($sp_sales->serviceprovider_id == $sp->serviceprovider_id) {
								$sp_sales->sp_level = $sp->serviceprovider_level;
							}
						}
					
					}
					foreach ($isr as $row ) {
						if ($row->sp_level != 'BARBER' ) {
				        	$row->total_service = 0;
				        	$row->service_incentives = 0;
				        	$row->other_sales = 0;
				        	$row->technical_sales = 0;
				        	$row->haircut_men = 0;
				        	$row->haircut_childWomen = 0;
				        	$row->haircut_men_sp_deduc = 0;
				        	$row->technical_deduc_count = 0;
				        	$row->haircut_childWomen_sp_deduc = 0;
				        	$row->other_sp_deduction = 0;
						}
					}
					
					

		}
	
  		return $isr;

	  
	
  	}


  	public function get_otc_total($brand_id, $branch_id, $start1, $end1){

  		if ($branch_id == 100001 || $branch_id == 100002 || $branch_id == 100003 || $branch_id == 100004 || $branch_id == 100005 || $branch_id == 100006 || $branch_id == 100007 || $branch_id == 100008 || $branch_id == 100009) {
  			$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id
		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      LEFT JOIN branch b ON b.branch_id = sp.branch_id 

		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();



	    	  $data = array();

	  		foreach($serviceproviders as $serviceprovider){

			        $serviceprovider_id = $serviceprovider->serviceprovider_id;

			        $isr_retail = $this->db->query("SELECT br.brand_name,sp.serviceprovider_name,b.branch_id, b.branch_name, SUM(0) as total_service, SUM(pr.productretail_amount) as total_retail,

						SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start1' AND '$end1'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();



			        if($isr_retail->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr_retail->brand_name,

			              'branch_name'                      =>          $isr_retail->branch_name,

			              'serviceprovider_name'        =>          $isr_retail->serviceprovider_name,

			              'total_sales'                 =>          0,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr_retail->total_service,

			              'total_retail' =>    $isr_retail->total_retail,

			              'service_incentives' => $isr_retail->service_incentives,

			              'otc_commission' => $isr_retail->otc_commission,

			              'total_commission' => $isr_retail->total_commission

			            );   

			        } //end if

			}



  		}else{



	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level

		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      WHERE sp.branch_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();



	    	  $data = array();

	  		foreach($serviceproviders as $serviceprovider){

			        $serviceprovider_id = $serviceprovider->serviceprovider_id;

			        $isr_retail = $this->db->query("SELECT br.brand_name,sp.serviceprovider_name,b.branch_id, b.branch_name, SUM(0) as total_service, SUM(pr.productretail_amount) as total_retail,

						SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start1' AND '$end1'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();



			        if($isr_retail->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr_retail->brand_name,

			              'branch_name'                      =>          $isr_retail->branch_name,

			              'serviceprovider_name'        =>          $isr_retail->serviceprovider_name,
			              'sp_level'                   =>           $serviceprovider->serviceprovider_level,

			              'total_sales'                 =>          0,
			              'haircut_men_sp_deduc'        => 0,
			              'haircut_childWomen_sp_deduc'        => 0,
			              'technical_deduc_count'        => 0,
			              'other_sp_deduction'        => 0,
			              'technical_sales'        => 0,
			              'haircut_men'        => 0,
			              'haircut_childWomen'        => 0,
			              'technical_sales'        => 0,
			              'other_sales'        => 0,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    0,

			              'total_retail' =>    $isr_retail->total_retail,

			              'service_incentives' => $isr_retail->service_incentives,

			              'otc_commission' => $isr_retail->otc_commission,

			              'total_commission' => $isr_retail->total_commission

			            );   

			        } //end if

			}

		}

		return $data;

  	}

  	public function get_otc_total_allbranches($brand_id, $branch_id, $start1, $end1){



  		

  			$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id

		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      LEFT JOIN branch b ON b.branch_id = sp.branch_id 

		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();



	    	  $data = array();

	  		foreach($serviceproviders as $serviceprovider){



			        $serviceprovider_id = $serviceprovider->serviceprovider_id;

			        $isr_retail = $this->db->query("SELECT br.brand_name,sp.serviceprovider_name,b.branch_id, b.branch_name, SUM(0) as total_service, SUM(pr.productretail_amount) as total_retail,

						SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start1' AND '$end1'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();



			        if($isr_retail->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr_retail->brand_name,

			              'branch_name'                      =>          $isr_retail->branch_name,

			              'serviceprovider_name'        =>          $isr_retail->serviceprovider_name,

			              'total_sales'                 =>          0,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr_retail->total_service,

			              'total_retail' =>    $isr_retail->total_retail,

			              'service_incentives' => $isr_retail->service_incentives,

			              'otc_commission' => $isr_retail->otc_commission,

			              'total_commission' => $isr_retail->total_commission

			            );   

			        } //end if

			}

			return $data;

  	}



  	public function get_total_allbranches($brand_id, $branch_id, $start, $end, $start1, $end1){

  			  $serviceproviders = $this->db->query("SELECT sp.serviceprovider_id

		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      LEFT JOIN branch b ON b.branch_id = sp.branch_id

		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();



	    	  $data = array();



	    	  	// foreach serviceproviders

	    	    foreach($serviceproviders as $serviceprovider){



			        $serviceprovider_id = $serviceprovider->serviceprovider_id;



			        $isr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount) as total_service, SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();


			        // ISR Total GC

			        $isr_total_paymentgc = 0;



			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc

			        FROM transaction t

			        RIGHT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					RIGHT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_paymentgc > 0

			        AND t.transaction_totalsales > 0

			        ")->result();



			        //payment gc

			        foreach($isr_paymentgc as $paymentgc){



			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 

			          FROM transactiondetail

			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();



			          if($sp_count == 1){

			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;

			          }

			          else{

			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id AND

			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

			            $sales_percentage = $sp_total_sales / $total_sales;

			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);



			          }



			        }   // end payment gc







			        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

			   

			        if($isr->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr->brand_name,

			              'branch_name'                      =>          $isr->branch_name,

			              'serviceprovider_name'        =>          $isr->serviceprovider_name,

			              'total_sales'                 =>          $grand_total_sales,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr->total_service,

			              'total_retail' =>    $isr->total_retail,

			              'service_incentives' => $isr->service_incentives,

			              'otc_commission' => $isr->otc_commission,

			              'total_commission' => $isr->total_commission

			            );   

			        } //end if

			    } //end foreach serviceproviders

		return $data;
  	}

  	public function get_total_all($brand_id, $branch_id, $start, $end, $start1, $end1){

	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id

		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      WHERE sp.branch_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();



	    	  $data = array();



	    	  	// foreach serviceproviders

	    	    foreach($serviceproviders as $serviceprovider){



			        $serviceprovider_id = $serviceprovider->serviceprovider_id;



			        $isr = $this->db->query("SELECT br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount) as total_service, SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();



			        $isr_retail = $this->db->query("SELECT sp.serviceprovider_name,  SUM(pr.productretail_amount) as total_retail,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start1' AND '$end1'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();



			        // ISR Total GC

			        $isr_total_paymentgc = 0;



			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc

			        FROM transaction t

			        RIGHT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					RIGHT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_paymentgc > 0

			        AND t.transaction_totalsales > 0

			        ")->result();



			        //payment gc

			        foreach($isr_paymentgc as $paymentgc){



			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 

			          FROM transactiondetail

			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();



			          if($sp_count == 1){

			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;

			          }

			          else{

			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id AND

			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

			            $sales_percentage = $sp_total_sales / $total_sales;

			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);



			          }



			        }   // end payment gc







			        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);

			   

			        if($isr->brand_name){

			            $data[] = (object)array(

			              'brand_name'                       =>          $isr->brand_name,

			              'branch_name'                      =>          $isr->branch_name,

			              'serviceprovider_name'        =>          $isr->serviceprovider_name,

			              'total_sales'                 =>          $grand_total_sales,

			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),

			              'total_service' =>    $isr->total_service,

			              'total_retail' =>    $isr_retail->total_retail,

			              'service_incentives' => $isr->service_incentives,

			              'otc_commission' => $isr->otc_commission,

			              'total_commission' => $isr->total_commission

			            );   

			        } //end if

			    } //end foreach serviceproviders
		return $data;
  	}// END

  	public function get_total_all_sb_per_branch($brand_id, $branch_id, $start, $end, $start1, $end1){

	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level

		      FROM serviceprovider sp

		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id

		      WHERE sp.branch_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id

		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL

		      OR SUM(td.transactiondetails_totalsales) > 0

		      ")->result();

	    	  $data = array();
	    	  	// foreach serviceproviders
	    	    foreach($serviceproviders as $serviceprovider){

			        $serviceprovider_id = $serviceprovider->serviceprovider_id;

			        $isr = $this->db->query("SELECT br.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  ROUND(SUM(td.transactiondetails_totalsales),2) 'total_sales', ROUND(SUM(ps.productservice_amount),2) as total_service, ROUND(SUM(pr.productretail_amount),2) as total_retail,SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();

			        $haircut_men = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_men',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as men_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id = 10298
			        ")->row();

			  //       $haircut_men_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.branch_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id = 10298")->result();
			  //       $haircut_men_sp_deduc = 0;
			  //       foreach ($haircut_men_sp as $sp_haircut_men) {
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_haircut_men->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $haircut_men_sp_deduc += 1;
			  //       	}
			  //       }

			        $haircut_childWomen = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_childWomen',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as childWomen_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10372, 10373)
			        ")->row();

			  //       $haircut_childWomen_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.branch_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10372, 10373)")->result();
			  //       $haircut_childWomen_sp_deduc = 0;
			  //       foreach ($haircut_childWomen_sp as $sp_haircut_childWomen) {
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_haircut_childWomen->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $haircut_childWomen_sp_deduc += 1;
			  //       	}
			  //       }

			        $technical_sales = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_name,  SUM(ps.productservice_amount) 'technical_sales',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as technical_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10381)
			        ")->row();

			  //       $technical_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.branch_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			  //       $technical_sp_deduction = 0;
			  //       foreach ($technical_sp as $sp_technical) {
			        	
			  //       	$technical_sp_count  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_technical->transaction_id")->num_rows();
			  //       	if ($technical_sp_count != 1) {
			  //       	  $technical_sp_deduction += 1;
			  //       	}
			  //       }

			        $other_sales = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) as 
			        	'other_sales',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as other_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.branch_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id NOT IN (10298, 10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10372, 10373, 10381)
			        ")->row();

			  //       $other_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.branch_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			  //       $other_sp_deduction = 0;
			  //       foreach ($other_sp as $sp_other) {
			        	
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_other->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $other_sp_deduction += 1;
			  //       	}
			  //       }


        

			        $isr_total_paymentgc = 0;

			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc

			        FROM transaction t

			        RIGHT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					RIGHT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        WHERE t.branch_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_paymentgc > 0

			        AND t.transaction_totalsales > 0

			        ")->result();

			        foreach($isr_paymentgc as $paymentgc){

			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 

			          FROM transactiondetail

			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

			          if($sp_count == 1){
			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;

			          }
			          else{

			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id AND

			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

			            $sales_percentage = $sp_total_sales / $total_sales;

			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);


			          }



			        }   // end payment gc

		        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);
                    if ($serviceprovider->serviceprovider_level != 'BARBER' ) {
			        	$isr->total_service = 0;
	
			        }
	
			        if($isr->brand_name){

			            $data[] = (object)array(

			              'brand_id' => $isr->brand_id,
			              'brand_name'                       =>          $isr->brand_name,
			              'branch_name'                      =>          $isr->branch_name,
			              'serviceprovider_name'        =>          $isr->serviceprovider_name,
			              'sp_level'                    =>          $serviceprovider->serviceprovider_level,
			             // 'total_sales'                 =>          $grand_total_sales,
			              'total_paymentgc' =>    round($isr_total_paymentgc, 2),
			              'total_service' =>    $isr->total_service,
			              'total_retail' =>    $isr->total_retail,
			              'service_incentives' => $isr->service_incentives,
			              'otc_commission' => $isr->otc_commission,
			              'total_commission' => $isr->total_commission,
			              'haircut_men' => $haircut_men->haircut_men,
			              'haircut_childWomen' => $haircut_childWomen->haircut_childWomen,
			              'technical_sales' => $technical_sales->technical_sales,
			              // 'men_count' => $haircut_men->men_count,
			              // 'childWomen_count' => $haircut_childWomen->childWomen_count,
			              // 'technical_count' => $technical_sales->technical_count,
			              'other_sales' => $other_sales->other_sales
			              // 'other_count' => $other_sales->other_count,
			           	  // 'technical_deduc_count' => $technical_sp_deduction,
			           	  // 'haircut_childWomen_sp_deduc' => $haircut_childWomen_sp_deduc,
			           	  // 'haircut_men_sp_deduc' => $haircut_men_sp_deduc,
			           	  // 'other_sp_deduction' => $other_sp_deduction

			            );   

			        } //end if

			    } //end foreach serviceproviders
		return $data;
  	}// END

  	public function get_total_all_sb_all_branch($brand_id, $branch_id, $start, $end, $start1, $end1){

	  		$serviceproviders = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_level

		      FROM serviceprovider sp
              
		      RIGHT JOIN transactiondetail td ON td.serviceprovider_id = sp.serviceprovider_id
              LEFT JOIN branch b ON b.branch_id = sp.branch_id
		      WHERE b.brand_id = $branch_id
		      AND sp.serviceprovider_status = 1
		      GROUP BY sp.serviceprovider_id
		      HAVING SUM(td.transactiondetails_totalsales) IS NOT NULL
		      OR SUM(td.transactiondetails_totalsales) > 0
		      ")->result();


	    	  $data = array();

	    	  	// foreach serviceproviders

	    	    foreach($serviceproviders as $serviceprovider){

			        $serviceprovider_id = $serviceprovider->serviceprovider_id;

			        $isr = $this->db->query("SELECT br.brand_id, br.brand_name, b.branch_id, b.branch_name, sp.serviceprovider_name,  SUM(td.transactiondetails_totalsales) 'total_sales', SUM(ps.productservice_amount) as total_service, SUM(pr.productretail_amount) as total_retail,SUM(0) as service_incentives,

						SUM(0) as otc_commission,

						SUM(0) as total_commission

			        FROM transactiondetail td

			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id

			        LEFT JOIN branch b ON b.branch_id = t.branch_id

			        LEFT JOIN brand br ON br.brand_id = b.brand_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					LEFT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_totalsales > 0

			        ")->row();

			        $haircut_men = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_men',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as men_count
			        FROM transactiondetail td
			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id = 10298
			        ")->row();

			  //       $haircut_men_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.brand_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id = 10298")->result();
			  //       $haircut_men_sp_deduc = 0;
			  //       foreach ($haircut_men_sp as $sp_haircut_men) {
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_haircut_men->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $haircut_men_sp_deduc += 1;
			  //       	}
			  //       }

			        $haircut_childWomen = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) 'haircut_childWomen',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as childWomen_count
			        FROM transactiondetail td
			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10372, 10373)
			        ")->row();

			  //       $haircut_childWomen_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.brand_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10372, 10373)")->result();
			  //       $haircut_childWomen_sp_deduc = 0;
			  //       foreach ($haircut_childWomen_sp as $sp_haircut_childWomen) {
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_haircut_childWomen->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $haircut_childWomen_sp_deduc += 1;
			  //       	}
			  //       }

			        $technical_sales = $this->db->query("SELECT sp.serviceprovider_id, sp.serviceprovider_name,  SUM(ps.productservice_amount) 'technical_sales',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as technical_count
			        FROM transactiondetail td
			        LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10381)
			        ")->row();

			  //       $technical_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       LEFT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.brand_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			  //       $technical_sp_deduction = 0;
			  //       foreach ($technical_sp as $sp_technical) {
			        	
			  //       	$technical_sp_count  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_technical->transaction_id")->num_rows();
			  //       	if ($technical_sp_count != 1) {
			  //       	  $technical_sp_deduction += 1;
			  //       	}
			  //       }

			        $other_sales = $this->db->query("SELECT sp.serviceprovider_name,  SUM(ps.productservice_amount) as 
			        	'other_sales',
						SUM(0) as otc_commission,
						SUM(0) as total_commission,
						COUNT(t.transaction_id) as other_count
			        FROM transactiondetail td
			        RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			        LEFT JOIN branch b ON b.branch_id = t.branch_id
			        LEFT JOIN brand br ON br.brand_id = b.brand_id
			        LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			        RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			        WHERE t.brand_id = $branch_id 
			        AND t.transaction_date BETWEEN '$start' AND '$end'
			        AND t.transaction_status = 1
			        AND td.serviceprovider_id = $serviceprovider_id
			        AND t.transaction_totalsales > 0
			        AND td.productservice_id NOT IN (10298, 10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429, 10372, 10373, 10381)
			        ")->row();

			  //       $other_sp = $this->db->query("SELECT t.transaction_id, sp.serviceprovider_id, sp.serviceprovider_name,  ps.productservice_amount 'technical_sales'
			  //       FROM transactiondetail td
			  //       RIGHT JOIN transaction t ON t.transaction_id = td.transaction_id
			  //       LEFT JOIN branch b ON b.branch_id = t.branch_id
			  //       LEFT JOIN brand br ON br.brand_id = b.brand_id
			  //       LEFT JOIN productservice ps on ps.productservice_id = td.productservice_id
					// LEFT JOIN productretail pr on pr.productretail_id = td.productretail_id
			  //       RIGHT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id
			  //       WHERE t.brand_id = $branch_id
			  //       AND t.transaction_date BETWEEN '$start' AND '$end'
			  //       AND t.transaction_status = 1
			  //       AND td.serviceprovider_id = $serviceprovider_id
			  //       AND t.transaction_totalsales > 0
			  //       AND td.productservice_id IN (10301, 10302, 10303, 10304, 10307, 10308, 10375, 10376, 10377, 10378, 10379, 10380, 10382, 10429)")->result();
			  //       $other_sp_deduction = 0;
			  //       foreach ($other_sp as $sp_other) {
			        	
			  //       	$get_sp  = $this->db->query("SELECT DISTINCT serviceprovider_id 
				 //          FROM transactiondetail
				 //          WHERE transaction_id = $sp_other->transaction_id")->num_rows();
			  //       	if ($get_sp != 1) {
			  //       	  $other_sp_deduction += 1;
			  //       	}
			  //       }


        

			        $isr_total_paymentgc = 0;

			        $isr_paymentgc = $this->db->query("SELECT DISTINCT t.transaction_id, t.transaction_paymentgc

			        FROM transaction t

			        RIGHT JOIN transactiondetail td ON t.transaction_id = td.transaction_id

			        LEFT JOIN serviceprovider sp ON sp.serviceprovider_id = td.serviceprovider_id

			        LEFT  JOIN productservice ps on ps.productservice_id = td.productservice_id

					RIGHT  JOIN productretail pr on pr.productretail_id = td.productretail_id

			        WHERE t.brand_id = $branch_id 

			        AND t.transaction_date BETWEEN '$start' AND '$end'

			        AND t.transaction_status = 1

			        AND td.serviceprovider_id = $serviceprovider_id

			        AND t.transaction_paymentgc > 0

			        AND t.transaction_totalsales > 0

			        ")->result();

			        foreach($isr_paymentgc as $paymentgc){

			          $sp_count = $this->db->query("SELECT DISTINCT serviceprovider_id 

			          FROM transactiondetail

			          WHERE transaction_id = $paymentgc->transaction_id")->num_rows();

			          if($sp_count == 1){
			            $isr_total_paymentgc = $isr_total_paymentgc + $paymentgc->transaction_paymentgc;

			          }
			          else{

			            $sp_total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'sp_total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id AND

			            serviceprovider_id = $serviceprovider_id")->row()->sp_total_sales;

			            $total_sales = $this->db->query("SELECT SUM(transactiondetails_totalsales) 'total_sales' 

			            FROM transactiondetail

			            WHERE transaction_id = $paymentgc->transaction_id")->row()->total_sales;

			            $sales_percentage = $sp_total_sales / $total_sales;

			            $isr_total_paymentgc = $isr_total_paymentgc + ($sales_percentage * $paymentgc->transaction_paymentgc);


			          }



			        }   // end payment gc
			        
			        

		        // GRAND TOTAL SALES GC DEDUCTED

			        $grand_total_sales = round($isr->total_sales - $isr_total_paymentgc, 2);
                    
                    if ($serviceprovider->serviceprovider_level != 'BARBER' ) {
			        	$isr->total_service = 0;
			        }
                    
			        if($isr->brand_name){

			            $data[] = (object)array(

			              'brand_id' => $isr->brand_id,
			              'brand_name'                       =>          $isr->brand_name,
			              'branch_name'                      =>          $isr->branch_name,
			              'serviceprovider_name'        =>          $isr->serviceprovider_name,
			              'sp_level'                    =>          $serviceprovider->serviceprovider_level,
			              'total_sales'                 =>          $grand_total_sales,
			              // 'total_paymentgc' =>    round($isr_total_paymentgc, 2),
			              'total_service' =>    $isr->total_service,
			              'total_retail' =>    $isr->total_retail,
			              'service_incentives' => $isr->service_incentives,
			              'otc_commission' => $isr->otc_commission,
			              'total_commission' => $isr->total_commission,
			              'haircut_men' => $haircut_men->haircut_men,
			              'haircut_childWomen' => $haircut_childWomen->haircut_childWomen,
			              'technical_sales' => $technical_sales->technical_sales,
			             // 'men_count' => $haircut_men->men_count,
			             // 'childWomen_count' => $haircut_childWomen->childWomen_count,
			             // 'technical_count' => $technical_sales->technical_count,
			              'other_sales' => $other_sales->other_sales
			             // 'other_count' => $other_sales->other_count,
			           	 // 'technical_deduc_count' => 0,
			           	 // 'haircut_childWomen_sp_deduc' => 0,
			           	 // 'haircut_men_sp_deduc' => 0,
			           	 // 'other_sp_deduction' => 0

			            );   

			        } //end if

			    } //end foreach serviceproviders
		return $data;
  	}// END


} //END MODEL 

