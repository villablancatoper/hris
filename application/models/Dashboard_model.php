<?php



  class Dashboard_model extends CI_Model{

    public function __construct(){

      parent::__construct();

    }



    public function get_changelogs(){

      $this->db->order_by('changelog_id', 'DESC');

      // $this->db->limit(10);

      return $this->db->get('changelog')->result();

    }

    public function get_productretails_by_brand_id($brand_id){
      return $this->db->query("SELECT * 
      FROM productretail
      WHERE productretail_status != 0
      AND brand_id = $brand_id
      ORDER BY productretail_name ASC");
    }

    public function get_productservices_by_brand_id($brand_id){
      return $this->db->query("SELECT * 
      FROM productservice
      WHERE productservice_status != 0
      AND brand_id = $brand_id
      ORDER BY productservice_name ASC");
    }

    public function change_password($old_password, $new_password){
      $user_username = $this->session->userdata('user')->user_username;
      $check_password = $this->is_password_correct($user_username, $old_password);

      if($check_password == TRUE){
        $this->db->where('user_username', $user_username);
        $this->db->update('user', array('user_password' => $new_password));

        return TRUE;
      }
      else{
        return FALSE;
      }
    }
    
    
    
     public function get_branches(){

  		$this->db->select("branch_id,branch_name");

  		$this->db->where("brand_id !=" , 100007);

  		$this->db->where("branch_status", 1);
  		$this->db->order_by("branch_name", "ASC");

  		$query = $this->db->get("branch");

  		$output = '<option value="">Select Branch</option>';

  		foreach ($query->result() as $row) {

  			$output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

  		}

  		return $output;

  	}

    public function transfer_branch($user_id, $branch_id, $date){

      // $query = $this->db->query("SELECT user_lasttransfer
      //   FROM user
      //   WHERE user_id = $user_id")->row();

      // if ($query->user_lasttransfer == $date) {
      //     return "failed";
      // } else {
          $this->db->set('branch_id', $branch_id);
          $this->db->set('user_lasttransfer', $date);
          $this->db->where('user_id', $user_id);
          $this->db->update('user');
          return "success";
      // }

    }

    public function is_password_correct($user_username, $old_password){

      $user = $this->db->query("SELECT user_username, user_password
              FROM user
              WHERE user_username = '$user_username'
              AND user_password = '$old_password'")->row();      

      if($user){
        return TRUE;
      }
      else{
        return FALSE;
      }
    }

    public function get_user_notice_status($user_id){
      $this->db->where('user_id', $user_id);
      return $this->db->get('user')->row()->user_noticestatus;
    }

    public function update_user_notice_status($user_id){
      $this->db->where('user_id', $user_id);
      $this->db->update('user', array('user_noticestatus' => 1));
    }

    public function allow_lateencode($branch_id, $lateencode_date, $requestor, $reason){
      $date_now = date('Y-m-d');
      $date_expiry = strtotime("+1 day", strtotime($date_now));
      $date_expiry = strftime ( '%Y-%m-%d' , $date_expiry );
     
      $insert_data = array('branch_id' => $branch_id,
                           'branch_allowencode' => date('Y-m-d', strtotime($lateencode_date)),
                           'user_id' => $requestor,
                           'date_requested' => $date_now,
                           'request_expiry' => $date_expiry,
                           'reason' => $reason
                );
      $this->db->insert('late_encoding', $insert_data);

      $formatted_date = date('Y-m-d', strtotime($lateencode_date));
      $this->db->query("UPDATE branch SET branch_allowencode = '$formatted_date'
        WHERE branch_id = $branch_id");

    }

    public function get_standard_users(){
      return $this->db->query("
        SELECT user_id, user_name
        FROM user
        WHERE user_position = 'Standard User'
        AND user_status = 1
        ORDER BY user_name")->result();
    }


    public function get_lateencoding(){

      return $this->db->query("
        SELECT b.branch_name, u.user_name, le.date_requested, le.branch_allowencode, le.request_expiry, le.reason
        FROM late_encoding le 
        LEFT JOIN branch b ON b.branch_id = le.branch_id
        LEFT JOIN user u ON u.user_id = le.user_id
        ORDER BY le.date_requested DESC")->result();

    }

    public function get_lateencoding_by_id($user_id, $branch_id){
      
       return $this->db->query("
        SELECT le.request_expiry
        FROM late_encoding le 
        LEFT JOIN branch b ON b.branch_id = le.branch_id
        LEFT JOIN user u ON u.user_id = le.user_id
        WHERE u.user_id = $user_id
        AND b.branch_id = $branch_id
        ORDER BY le.date_requested DESC")->row();
    }
    

  }