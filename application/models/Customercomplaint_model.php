<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customercomplaint_model extends CI_Model {
    
    function __construct(){
      parent::__construct();
      

    }

 
    // public function store_mis_ticket($branch_name){
    //     $result = $this->ticket_db->query("SELECT ticket_id, ticket_submit_date_time, ticket_brand_branch, ticket_category, ticket_title,ticket_assignee, ticket_status, ticket_aging, ticket_last_update_date, ticket_last_update_details, ticket_date_completed
    //         FROM tbl_ticket t
    //         LEFT JOIN branch b ON b.branch_name = t.ticket_brand_branch
    //         WHERE t.ticket_brand_branch = '$branch_name'
    //         AND (t.ticket_category = 'CCTV'
    //         OR t.ticket_category = 'Telephone/Landline'
    //         OR t.ticket_category = 'Card Terminal'
    //         OR t.ticket_category = 'Biometrics'
    //         OR t.ticket_category = 'Internet'
    //         OR t.ticket_category = 'Computer(Desktop or Laptop)'
    //         OR t.ticket_category = 'Other IT concern')
    //         ORDER BY t.ticket_submit_date_time DESC")->result();
    //     return $result;
    // }


    public function get_complaint_branches($user_id, $user_position, $user_role){
        
        if ($user_role == 'Marketing') {
        //Marketing & Training

            return $this->db->query("SELECT branch_id, branch_name FROM branch WHERE branch_status = 1 AND brand_id != 100007 ORDER BY branch_name ASC")->result();

        } else if ($user_role == 'Standard User' && $user_position == 'Area Manager') {
        //Area Manager

            return $this->db->query("SELECT b.branch_name, b.branch_id
            FROM branch b
            LEFT JOIN user u on u.user_id = b.branch_areamanager
            WHERE u.user_id = $user_id
            AND b.branch_status = 1
            ORDER BY b.branch_name ASC")->result();
            
        } else {

            return $this->db->query("SELECT branch_id, branch_name FROM branch WHERE branch_status = 1 AND brand_id != 100007 ORDER BY branch_name ASC")->result();

        }
    }

    





   

    


}