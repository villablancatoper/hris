<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Salesforecast_model extends CI_Model {

	

    function __construct(){

        parent::__construct();

  

    }
    public function get_lastmonth_performance($branch_id, $start, $end , $branch, $brand_id){

    

    
    		$query = $this->db->query("SELECT COALESCE(SUM(transaction_totalsales),0) as transaction_totalsales
          	FROM transaction
            
                
    			WHERE brand_id =$brand_id and branch_id =$branch_id
    			AND transaction_date BETWEEN '$start' AND '$end' 
    	
    			") ;
          
                

    	

		      return $query->row();

    }
    public function get_target_walkin($brand_id){

    

    
      $query = $this->db->query("SELECT tph_target
          FROM tph
          
              
        WHERE brand_id =$brand_id 
        
    
        ") ;
        
              

    

        return $query->row();

  }
    public function get($branch_id, $start, $end , $branch, $brand_id){

    	if ($branch == 'All Branches') {
            $query = $this->db->query("

                SELECT b.brand_name as brandname,
                 stt.storetarget_original as storetarget_originals,
                 stt.branch_id as bid, b.brand_id as brid, 
                 stt.storetarget_id as store_id, 
                 br.branch_name as branch_name,
                 stt.storetarget_month as storetargetmonth,
                 stt.storetarget_updatedsalesforecast as salesforecast,
                 stt.storetarget_averagesalesperformance as averagesales,

                 stt.storetarget_lastmonth_performance as storetarget_lastmonth_performance,
                 stt.last_year_sales as last_year_sales,
                 stt.last_year_walkin as last_year_walkin,
                 stt.last_year_regular as last_year_regular,
                 stt.target_walkin as target_walkin,
                 stt.target_regular as target_regular

                FROM storetarget stt
                LEFT JOIN branch br ON stt.branch_id = br.branch_id 
                LEFT JOIN brand b ON b.brand_id = br.brand_id 
                
                WHERE b.brand_id='$brand_id' AND storetarget_month BETWEEN '$start' AND '$end' 
                GROUP BY branch_name
                ORDER BY SUM(stt.storetarget_original) DESC") ;



        }

        else{
    		$query = $this->db->query("
    			
                SELECT b.brand_name as brandname,
                 stt.storetarget_original as storetarget_originals,
                 stt.branch_id as bid, stt.storetarget_id as store_id,
                 br.branch_name as branch_name, 
                 stt.storetarget_month as storetargetmonth, 
                 stt.storetarget_updatedsalesforecast as salesforecast, 
                 stt.storetarget_averagesalesperformance as averagesales,

                 stt.storetarget_lastmonth_performance as storetarget_lastmonth_performance,
                 stt.last_year_sales as last_year_sales,
                 stt.last_year_walkin as last_year_walkin,
                 stt.last_year_regular as last_year_regular,
                 stt.target_walkin as target_walkin,
                 stt.target_regular as target_regular

    			FROM storetarget stt
                LEFT JOIN branch br ON stt.branch_id = br.branch_id 
    			LEFT JOIN brand b ON b.brand_id = br.brand_id 
                
    			WHERE b.brand_id != 100007 and stt.branch_id =$branch_id
    			and storetarget_month BETWEEN '$start' AND '$end' 
    			GROUP BY branch_name
    			ORDER BY SUM(stt.storetarget_original) DESC") ;
               
                }

    	

		return $query->result();

    }
 public function delete_user($storetarget_id){
      $this->db->where('storetarget_id', $storetarget_id);
      $this->db->delete('storetarget');
    }

  public function update_storetarget($storetarget_id, $storetarget_month,$storetarget_original, $Storetarget_forecast,$Storetarget_average,$st_last_sales,$st_last_walkin,$st_last_regular,$st_target_walkin,$st_target_regular,$st_last_performance){
      $this->db->where('storetarget_id', $storetarget_id);
      $this->db->set('storetarget_month',  $storetarget_month);
      $this->db->set('storetarget_original',  $storetarget_original);
      $this->db->set('storetarget_updatedsalesforecast',  $Storetarget_forecast);
      $this->db->set('storetarget_averagesalesperformance',  $Storetarget_average);

      $this->db->set('last_year_sales',  $st_last_sales);
      $this->db->set('last_year_walkin',  $st_last_walkin);
      $this->db->set('last_year_regular',  $st_last_regular);
      $this->db->set('target_walkin',  $st_target_walkin);
      $this->db->set('target_regular',  $st_target_regular);
      $this->db->set('storetarget_lastmonth_performance',  $st_last_performance);
      $this->db->update('storetarget');
      
    }

    public function get_sum($branch_id, $start, $end){

		$query = $this->db->query("SELECT SUM(t.transaction_totalsales) as total_sales, SUM(t.transaction_paymentcash) as total_cash, 

									SUM(t.transaction_paymentcard) as total_card, SUM(t.transaction_paymentgc) as total_gc

									from transaction t

									WHERE t.branch_id = $branch_id AND 

									t.transaction_date BETWEEN '$start' and '$end'

								");

		return $query->result();

    }
public function add_storetarget($data){
       $branch_id=$data['branch_id'];
       $storetarget_month=$data['storetarget_month'];
       $storetarget_original=$data['storetarget_original'];
       $storetarget_updatedsalesforecast=$data['storetarget_updatedsalesforecast'];
       $storetarget_averagesalesperformance=$data['storetarget_averagesalesperformance'];

       $storetarget_lastmonth_performance=$data['storetarget_lastmonth_performance'];
       $last_year_sales=$data['last_year_sales'];
       $last_year_walkin=$data['last_year_walkin'];
       $last_year_regular=$data['last_year_regular'];
       $target_walkin=$data['target_walkin'];
       $target_regular=$data['target_regular'];


       $query=$this->db->select('branch_id')
            ->from('storetarget')
            ->where('branch_id',$branch_id)
            ->where('storetarget_month',$storetarget_month)
            ->get()->num_rows();

            
        if($query>0)
        {
              $this->db->set('storetarget_month',  $storetarget_month);
              $this->db->set('storetarget_original',  $storetarget_original);
              $this->db->set('storetarget_updatedsalesforecast',  $storetarget_updatedsalesforecast);
              $this->db->set('storetarget_averagesalesperformance',  $storetarget_averagesalesperformance);
              
              $this->db->set('storetarget_lastmonth_performance',  $storetarget_lastmonth_performance);
              $this->db->set('last_year_sales',  $last_year_sales);
              $this->db->set('last_year_walkin',  $last_year_walkin);
              $this->db->set('last_year_regular',  $last_year_regular);
              $this->db->set('target_walkin',  $target_walkin);
              $this->db->set('target_regular',  $target_regular);


              $this->db->where('branch_id', $branch_id);
              $this->db->where('storetarget_month', $storetarget_month);
              $this->db->update('storetarget');
        }
        else
        {
          $this->db->insert('storetarget', $data);

        }


         
        
        
    }
    public function fetch_branchess($brand_id){

      $this->db->select("branch_id,branch_name");

      $this->db->where("brand_id" , $brand_id);
      
      $this->db->where("branch_status" , 1);

      $query = $this->db->get("branch");

      $output = '<option value="">Select Branch</option>';

      

      foreach ($query->result() as $row) {

        $output .= '<option value = "'.$row->branch_id.'">'.$row->branch_name.'</option>';

      }

      return $output;

    }

  	public function get_branches(){

  		$query = $this->db->query("SELECT branch_id, branch_name FROM branch WHERE brand_id != 100007 ORDER BY branch_name ASC");

  		return $query->result();

  	}







  	// START



  	// END 

 



}