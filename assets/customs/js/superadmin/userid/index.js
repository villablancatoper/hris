$(function(){

  var users_table = $('#users_table').DataTable({
    'processing': true,
  
      'bAutoWidth': false,
  
      'bSort': false,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      'ordering': true
  });

  // $('#add_turnaway_date').datepicker({

  //   autoclose: true

  // });



  // $('#turnaway_date').datepicker({

  //   autoclose: true

  // });



  // $('#daterange_btn').daterangepicker(

  //   {

  //     ranges: {

  //       'Today': [moment(), moment()],

  //       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

  //       'Last 7 Days': [moment().subtract(6, 'days'), moment()],

  //       'Last 30 Days': [moment().subtract(29, 'days'), moment()],

  //       'This Month': [moment().startOf('month'), moment().endOf('month')],

  //       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

  //     },

  //     startDate: moment().subtract(29, 'days'),

  //     endDate: moment()

  //   },

  //   function (start, end) {

  //     $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

  //     $('#start').val(start.format('MM/DD/YYYY'));

  //     $('#end').val(end.format('MM/DD/YYYY'));

  //   }

  // )



  $('.select2').select2();

  $('.dataTables_length').parent().remove();

            
  var add_button = '<div class="col-sm-6"><button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_user_modal">Add Employee</button><div>';

  $('#users_table_wrapper').children(":first").append(add_button)

  get_users()

})



$('#add_user_modal').on('show.bs.modal', function(){

  $('#add_user_modal').find('[class="modal-footer"]').show();

  $('#add_user_alert_success').hide();

  $('#add_user_form').show();

  $('#add_user_number').val('');

  $('#add_user_lastname').val('');

  $('#add_user_firstname').val('');
  $('#add_user_middlename').val('');

});



$('#user_modal').on('show.bs.modal', function () {

  $('#user_type').empty()

  $('#user_modal').find('[class="modal-footer"]').show();

  $('#user_alert_success').hide();

  $('#user_form').show();

  $('#user_id').val('')

  $('#user_name').val('')

  $('#user_username').val('')

  $('#user_password').val('')

  disable_editing()

});



function get_users(){

  var users_table = $('#users_table').DataTable()

  $('.dataTables_processing', $('#users_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'get_users',

    method: 'GET',

    success: function(data){

      var users = JSON.parse(data);

      users_table.clear().draw();

      for(let i = 0; i < users.length; i++){
        users_table.row.add([
  
          users[i].emp_number,

          biometricID(users[i].emp_number),
          users[i].emp_lastname,
  
          users[i].emp_firstname,

         

          users[i].emp_middlename,
          users[i].emp_company,

          getBranch(users[i].emp_branch),
          statusText( parseFloat(users[i].emp_status)),
           '<button class="btn btn-info btn-sm" onclick="get_user('+users[i].emp_id+')"><i class="fa fa-search"></i></button>'

         
        
        ])
      }

      $('.dataTables_processing', $('#users_table').closest('.dataTables_wrapper')).hide();

      users_table.draw()
    }

  })

}

function getBranch(x)
{
  var l="asdfasdf";
  if (x==900)
  {
    var NK="NK Commisary"
      return NK;

  }
  
  else
  {
    
       var update_branches = $('#test_branches').find('option[value="' + x + '"]');

      $('#test_branches').val(update_branches.attr('value')).trigger('change');
      l= $('#test_branches option:selected').html();
      
      return l;
  }

      
   


}



function biometricID(x)
{

  first3Chars = x.substring(0, 3);
  if(first3Chars==="KCG")
  {
     return "----";
  }
  if(first3Chars==="R2G")
  {
     return x.substring(3);

  }
  else{
    return " ";
  }
 


}

function statusText(x)
{
  var status="";
  if(x==1)
  {
    status="Active";
    return status;

  }
  if(x==0)
  {
    status="Active";
    return status;
  }


}

function paddId(x)
{
  var y="";
  if(x<10)
  {
    y='R2G000'+x;
    return y;
  }
  
   if (x<100&&x>=10)
   {

    y='R2G00'+x;
    return y;

   }
   if (x<1000&&x>=100) 
   {
    y='R2G0'+x;
    return y;


   }  
   if (x>=1000)
  {

    y='R2G'+x;
    return y;

  }
  
      


}


function add_user(){

  $('#add_user_button').attr('disabled', 'disabled');

  $('#add_user_button').text('Adding...');

  // var loading = '<div class="loading small"></div>';

  // $('#add_user_button').append(loading);

  var emp_branch = $('#add_branches').val();
  var emp_company = $('#add_company').val();

  var emp_number = $('#add_user_number').val()

  var emp_lastname = $('#add_user_lastname').val()

  var emp_firstname = $('#add_user_firstname').val();
  var emp_middlename = $('#add_user_middlename').val();
  var emp_status=1;

  if(!emp_branch || !emp_company || !emp_number || !emp_lastname || !emp_firstname || !emp_middlename ){

    alert('All fields is required!');

    $('#add_user_button').removeAttr('disabled');

    $('#add_user_button').text('Add');

    // $('.loading').remove();

  }

  else{

    $.ajax({

      url: 'add_user',

      method: 'POST',
      data: {emp_branch: emp_branch, emp_company: emp_company, emp_number:emp_number,emp_lastname:emp_lastname, emp_firstname:emp_firstname, emp_middlename:emp_middlename, emp_status:emp_status },
     

      success: function(data){

        $('#add_user_button').removeAttr('disabled');

        $('#add_user_button').text('Add');

        // $('.loading').remove();

        $('#add_user_alert_success').show(400);

        $('#add_user_form').hide()

        $('#add_user_modal').find('[class="modal-footer"]').hide();

        get_users();

      }

    })

  }

}



function get_user(id){

  $('#user_modal').modal('show');


  $.ajax({

    url: 'get_user/'+id,

    success: function(data){

      var user = JSON.parse(data);

     // $('#update_company').val(user.emp_company);

      var update_company = $('#update_company').find('option[value="' + user.emp_company + '"]');

      $('#update_company').val(update_company.attr('value')).trigger('change');
      var update_branches = $('#update_branches').find('option[value="' + user.emp_branch + '"]');

      $('#update_branches').val(update_branches.attr('value')).trigger('change');

      $('#update_user_lastname').val(user.emp_lastname);
      $('#emp_num').val(id);

      $('#update_user_firstname').val(user.emp_firstname);

      $('#update_user_middlename').val(user.emp_middlename);

      var update_status = $('#update_status').find('option[value="' + user.emp_status + '"]');

      $('#update_status').val(update_status.attr('value')).trigger('change');



      


    }

  })

}



function enable_editing(){

  $('#update_company').removeAttr('disabled');

  $('#update_branches').removeAttr('disabled');

  $('#update_user_lastname').removeAttr('disabled');

  $('#update_user_firstname').removeAttr('disabled');

  $('#update_user_middlename').removeAttr('disabled');
   $('#update_status').removeAttr('disabled');
 $('#user_update_button').removeAttr('disabled');
}



function disable_editing(){

  $('#update_company').attr('disabled', 'disabled');

  $('#update_branches').attr('disabled', 'disabled');

  $('#update_user_lastname').attr('disabled', 'disabled');

  $('#update_user_firstname').attr('disabled', 'disabled');

  $('#update_user_middlename').attr('disabled', 'disabled');

  $('#update_status').attr('disabled', 'disabled');
  $('#user_update_button').attr('disabled', 'disabled');

}



function delete_user(){

  if (confirm('Are you sure you want to delete this user?')) {

    $.ajax({

      url: 'delete_user/' + $('#user_id').val(),

      success: function () {

        $('#user_function').text('deleted');

        $('#user_alert_success').show(400);

        $('#user_form').hide();

        $('#user_modal').find('[class="modal-footer"]').hide();

        get_users();

      }

    })

  }

}



function update_user(){

  var emp_branch = $('#update_branches').val();
  var emp_company = $('#update_company').val();

  

  var emp_lastname = $('#update_user_lastname').val();

  var emp_firstname = $('#update_user_firstname').val();
  var emp_middlename = $('#update_user_middlename').val();
   var emp_status=$('#update_status').val();
  if(!emp_branch || !emp_company || !emp_lastname || !emp_firstname||!emp_middlename||!emp_status){

    alert('All fields is required!');

  }

  else {

    $.ajax({

      url: 'update_user/'+$('#emp_num').val(),

      method: 'POST',

      data: {emp_branch: emp_branch, emp_company: emp_company, emp_lastname:emp_lastname, emp_firstname:emp_firstname, emp_middlename:emp_middlename, emp_status:emp_status},

      success: function () {

          $('#user_function').text('updated');

          $('#user_alert_success').show(400);

          $('#user_form').hide()

          $('#user_modal').find('[class="modal-footer"]').hide();

          get_users();
          disable_editing();
      }

    })

  }

}



function daterange_search(){

  var date = $.trim($('#daterange_btn').find('span').text());

  var start = $('#start').val();

  var end = $('#end').val();

  var branch_id = $('#branches').select2('val');

  var error = '';



  if (date == 'Select date' || (typeof $('#branches').val() != 'undefined' && !$('#branches').val())){

    if(typeof $('#branches').val() != 'undefined' && !$('#branches').val()){

      error += 'Branch is required.\n';

    }



    if(date == 'Select date'){

      error += 'Date is required.';

    }



    alert(error)

  }

  else{



    if(start == end){

      $('#turnaways_table').dataTable().fnDestroy();



      var turnaways_table = $('#turnaways_table').DataTable({

        'bAutoWidth': true,

        'processing': true,

        'ajax': {

          'url': 'get_turnaways_by_date',

          'method': 'POST',

          'data': {'date_start': start, 'branch_id': branch_id}

        },

        'createdRow': function (row, data) {

          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');

        }

      })



      turnaways_table.columns(0).visible(false);



      turnaways_table.order([1, 'desc']).draw();



    }

    else{

      $('#turnaways_table').dataTable().fnDestroy();



      var turnaways_table = $('#turnaways_table').DataTable({

        'bAutoWidth': true,

        'processing': true,

        'ajax': {

          'url': 'get_turnaways_by_date',

          'method': 'POST',

          'data': { 'date_start': start, 'date_end': end, 'branch_id': branch_id }

        },

        'createdRow': function (row, data) {

          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');

        }

      })



      turnaways_table.columns(0).visible(false);



      turnaways_table.order([1, 'desc']).draw();

    }

  }

}

function check_branch_no_transaction_yesterday(){

  $("#check_branch_no_transaction_yesterday").text('Extracting...');

  $("#check_branch_no_transaction_yesterday").text('Extracted.');

  $("#check_branch_no_transaction_yesterday").text('Not Updated CRM');

  $.ajax({
    url: 'check_branch_no_transaction_yesterday',
    method: 'GET',
    success: function(data){
      
    }
  })
  
}

