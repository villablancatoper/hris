$(function(){
    var services_table = $('#services_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'ordering': true
    });

    services_table.order([1, 'asc'])

    services_table.columns(0).visible(false);

    var servicemix_datatable = $('#servicemix_datatable').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'ordering': true
    });

    $('.select2').select2();

    servicemix_datatable.columns(3).visible(false);

    var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'ordering': true
    });

    $('#servicemix_datatable_filter').remove();
    $('#servicemix_datatable_length').remove();

    $('#add_servicemix_datatable_filter').remove();
    $('#add_servicemix_datatable_length').remove();
});

$('.items').on('change', function (){
    setTimeout(function() { $('#add_item_quantity').focus() }, 1);
});

$('#service_modal').on('show.bs.modal', function(){
    var servicemix_datatable = $('#servicemix_datatable').DataTable();

    servicemix_datatable.clear().draw();

    $('#service_alert_success').hide();
    $('#service_form').show();
    $('#service_modal').find('.modal-footer').hide()

    $('.item-selection').attr('style', 'display: none;');
})

$('#add_servicemix_modal').on('show.bs.modal', function(){
    var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable();

    add_servicemix_datatable.clear().draw();

    $('#add_servicemix_alert_success').hide();
    $('#add_servicemix_form').show();
    $('#add_servicemix_modal').find('.modal-footer').show()

    $('#productservice_selection').attr('style', 'display: none;');
    $('#item_selection').attr('style', 'display: none;');

})

// $('#add_servicemix_modal').on('hidden.bs.modal', function(){

//     $('#add_brands').val('').trigger('change');

// })

$('#add_brands').on('change', function(){

    $('#loading_modal').modal('show');

    var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable();

    add_servicemix_datatable.clear().draw();
    
    var brand_id = $('#add_brands').val();

    $('#productservice_selection').attr('style', 'display: none;');
    $('#item_selection').attr('style', 'display: none;');

    $.ajax({
        url: 'get_services_by_brand_id',
        method: 'GET',
        data: {'brand_id': brand_id},
        success: function(data){
            var services = JSON.parse(data);

            $('#add_productservice').empty();

            var default_option = '<option value="">Select service</option>';

            var options = '';

            for(let i = 0; i < services.length; i++){
                options += '<option value="'+services[i].productservice_code+'">'+services[i].productservice_description+'</option>';
            }

            $('#add_productservice').append(default_option);
            $('#add_productservice').append(options);

            $.ajax({
                url: 'get_items_by_brand_id',
                method: 'GET',
                data: {'brand_id': brand_id},
                success: function(data){

                    var items = JSON.parse(data);

                    $('#items').empty();

                    default_option = '<option value="">Select item</option>';

                    options = '';

                    for(let i = 0; i < items.length; i++){
                        options += '<option data-item-uom="'+items[i].item_uom+'" value="'+items[i].item_id+'">'+items[i].item_description+'</option>';
                    }

                    $('#items').append(default_option);
                    $('#items').append(options);

                    $('#productservice_selection').removeAttr('style');
                    $('#item_selection').removeAttr('style');

                    $('#loading_modal').modal('hide');
                }
            
            });

        }
    })

});

function search_services(){
    var brand_id = $('#brands').val();

    var services_table = $('#services_table').DataTable();

    services_table.clear().draw();

    $('.dataTables_processing', $('#services_table').closest('.dataTables_wrapper')).show();

    if(!brand_id){
        alert('Brand is required!');

        $('.dataTables_processing', $('#services_table').closest('.dataTables_wrapper')).hide();
    }
    else{
        $.ajax({
            url: 'search_services',
            method: 'GET',
            data: {'brand_id': brand_id},
            success: function(data){

                var services = JSON.parse(data);
                console.log(services)

                for(let i = 0; i < services.length; i++){

                    services_table.row.add([

                        services[i].brand_id,

                        services[i].brand_name,

                        services[i].productservice_description,

                        services[i].productservice_amount,

                        '<button class="btn btn-info btn-sm" onclick="view_servicemix('+services[i].productservice_code+', this)" data-productservice-description="'+services[i].productservice_description+'" data-brand-name="'+services[i].brand_name+'" data-brand-id="'+services[i].brand_id+'"><i class="fa fa-search"></i></button>'
    
                    ]).draw(false)

                }

                services_table.draw();

                $('.dataTables_processing', $('#services_table').closest('.dataTables_wrapper')).hide();
            }
        })
    }
}

function view_servicemix(productservice_code, button){

    var last_brand_id = $('#brand_id').val();

    $('#service_modal').modal('show');
    
    $('#brand').val($(button).attr('data-brand-name'));

    $('#brand_id').val($(button).attr('data-brand-id'));

    var new_brand_id = $('#brand_id').val();

    var search = false;

    if(last_brand_id != new_brand_id){
        search = true;
    }

    $('#servicemix_datatable').parents('div.dataTables_wrapper').first().show();

    $('.dataTables_processing', $('#servicemix_datatable').closest('.dataTables_wrapper')).show();
    
    $('#servicemix_tabledit_wrapper').empty();
    
    $('#productservice_code').val(productservice_code);
    
    $('#service_description').val($(button).attr('data-productservice-description'));
    
    get_servicemix(productservice_code, button, search);
    
}

function test(){
    alert('test')
}

function get_servicemix(productservice_code, button = null, search = false){

    var brand_id = $(button).attr('data-brand-id');

    if(button.length == 6){
        brand_id = button;
    }
    
    var servicemix_datatable = $('#servicemix_datatable').DataTable();
    
    $.ajax({
        url: 'get_servicemix',
        method: 'GET',
        data: {'productservice_code': productservice_code},
        success: function(data){

            var servicemix = JSON.parse(data);

            var str = '<table id="servicemix_tabledit" class="table table-bordered table-hover" style="width: 100% !important;"><thead><tr><th>ID</th><th>Item</th><th>Quantity</th><th>UOM</th></tr></thead><tbody>';

            for(let i = 0; i < servicemix.length; i++){

                str += '<tr>'

                str += '<td>'+servicemix[i].servicemix_id+'</td>'

                str += '<td>'+servicemix[i].item_description+'</td>'

                str += '<td>'+servicemix[i].servicemix_quantity+'</td>'

                str += '<td>'+servicemix[i].item_uom+'</td></tr>'

            }

            if(servicemix.length == 0){
                str += '<tr><td class="text-center" colspan="4">No data available in table.</td></tr>'
            }

            str += '</tbody></table>';

            console.log($('#brand_id').val());

            if(search == true){
                $.ajax({
                    url: 'get_items_by_brand_id',
                    method: 'GET',
                    data: {'brand_id': brand_id},
                    success: function(data){
    
                        var items = JSON.parse(data);
                        
                        var default_option = '<option value="">Select item</option>';
                        
                        var options = '';
                        
                        $('.items').empty();
    
                        for(let i = 0; i < items.length; i++){
                            options += '<option data-item-uom="'+items[i].item_uom+'" value="'+items[i].item_id+'">'+items[i].item_description+'</option>';
                        }
    
                        $('.items').append(default_option);
                        $('.items').append(options);
    
                        $('.items').select2();
    
                        $('.item-selection').removeAttr('style');
    
                        $('#servicemix_tabledit_wrapper').append(str);
    
                        if(servicemix.length != 0){
                            $('#servicemix_tabledit').Tabledit({
                                url: 'process_servicemix',
                                columns: {
                                    identifier: [0, 'servicemix_id'],                   
                                    editable: [[2, 'servicemix_quantity']]
                                },
                                editmethod: 'post',
                                deletemethod: 'post',
                                buttons: {
                                    edit: {
                                        class: 'btn btn-sm btn-warning',
                                        html: '<span class="fa fa-edit"></span>',
                                        action: 'edit'
                                    },
                                    delete: {
                                        class: 'btn btn-sm btn-danger',
                                        html: '<span class="fa fa-trash"></span>',
                                        action: 'delete'
                                    },
                                    save: {
                                        class: 'btn btn-sm btn-success',
                                        html: 'Save'
                                    },
                                    confirm: {
                                        class: 'btn btn-sm btn-default',
                                        html: 'Are you sure?'
                                    }
                                },
                                hideIdentifier: true,
                                restoreButton: false,
                                onSuccess: function(data, textStatus, jqXHR){
                                    if(data.action == 'delete'){
                                        $('#'+data.servicemix_id).remove();
                                    }
    
                                    if(data.action == 'edit'){
                                        $('#'+data.servicemix_id).removeAttr('class');
                                        $('#'+data.servicemix_id).attr('class', 'info');
                                    }
                                }
                            });
                        }
    
    
                        servicemix_datatable.draw();
    
                        $('.dataTables_processing', $('#servicemix_datatable').closest('.dataTables_wrapper')).hide();
    
                        $('#servicemix_datatable').parents('div.dataTables_wrapper').first().hide();
                    }
                
                });
            }
            else{

                $('.items').select2();

                $('.item-selection').removeAttr('style');

                $('#servicemix_tabledit_wrapper').append(str);

                if(servicemix.length != 0){
                    $('#servicemix_tabledit').Tabledit({
                        url: 'process_servicemix',
                        columns: {
                            identifier: [0, 'servicemix_id'],                   
                            editable: [[2, 'servicemix_quantity']]
                        },
                        editmethod: 'post',
                        deletemethod: 'post',
                        buttons: {
                            edit: {
                                class: 'btn btn-sm btn-warning',
                                html: '<span class="fa fa-edit"></span>',
                                action: 'edit'
                            },
                            delete: {
                                class: 'btn btn-sm btn-danger',
                                html: '<span class="fa fa-trash"></span>',
                                action: 'delete'
                            },
                            save: {
                                class: 'btn btn-sm btn-success',
                                html: 'Save'
                            },
                            confirm: {
                                class: 'btn btn-sm btn-default',
                                html: 'Are you sure?'
                            }
                        },
                        hideIdentifier: true,
                        restoreButton: false,
                        onSuccess: function(data, textStatus, jqXHR){
                            if(data.action == 'delete'){
                                $('#'+data.servicemix_id).remove();
                            }

                            if(data.action == 'edit'){
                                $('#'+data.servicemix_id).removeAttr('class');
                                $('#'+data.servicemix_id).attr('class', 'info');
                            }
                        }
                    });
                }


                servicemix_datatable.draw();

                $('.dataTables_processing', $('#servicemix_datatable').closest('.dataTables_wrapper')).hide();

                $('#servicemix_datatable').parents('div.dataTables_wrapper').first().hide();
            } 
        }
    })
}

function add_item_on_servicemix(){

    var item = $('#items option:selected');

    var error = '';

    if(!item.text() || !$('#item_quantity').val()){

        if(!item.text()){
            error += 'Item is required!\n';
        }

        if(!$('#item_quantity').val()){
            error += 'Quantity is required!\n';
        }

        alert(error)

    }

    else{

        var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable();

        add_servicemix_datatable.row.add([
            item.val(),
            item.text(),
            $('#item_quantity').val(),
            item.attr('data-item-uom'),
            '<button class="btn btn-danger btn-xs" onclick="remove_item_on_servicemix(this)"><i class="fa fa-remove"></i></button>'
        ]).draw();

        $('#item_quantity').val('');

        $('#items').val('').trigger('change');

        $('#items').click();

    }

}

function remove_item_on_servicemix(button){

    var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable();
    var tr = button.parentNode.parentNode;
  
    add_servicemix_datatable.row(tr).remove().draw();
}

function add_servicemix(){
    var brand_id = $('#add_brands').val();

    var productservice_id = $('#add_productservice').val();

    var add_servicemix_datatable = $('#add_servicemix_datatable').DataTable();

    var servicemix_data = [];

    var servicemix = add_servicemix_datatable.rows().data();

    var error = '';

    if(!brand_id || !productservice_id || servicemix.rows().data().length < 1){

        if(!brand_id){
            error += 'Brand is required!\n';
        }

        if(!productservice_id){
            error += 'Product Service is required!\n';
        }

        if(servicemix.rows().data().length < 1){
            error += 'Service Mix seems to be empty!\n';
        }

        alert(error);
    }

    else{

        for(let i = 0; i < servicemix.length; i++){
            servicemix_data.push(servicemix[i]);
        }

        $.ajax({
            url: 'add_servicemix',
            method: 'POST',
            data: {'productservice_id': productservice_id, 'servicemix_data': servicemix_data},
            success: function(){

                $('#add_servicemix_alert_success').show(400);
                $('#add_servicemix_form').hide();
                $('#add_servicemix_modal').find('.modal-footer').hide();

            }
        })

    }
}

function add_servicemix_item(){
    var item = $('.items').val();
    var item_uom = $('.items option:selected').attr('data-item-uom');
    var productservice_code = $('#productservice_code').val();
    var quantity = $('#add_item_quantity').val();
    var error = '';

    $('#servicemix_datatable').parents('div.dataTables_wrapper').first().show();

    $('.dataTables_processing', $('#servicemix_datatable').closest('.dataTables_wrapper')).show();
    
    $('#servicemix_tabledit_wrapper').empty();

    if(!item || !quantity){

        if(!item){
            error += 'Item is required.\n'
        }

        if(!quantity){
            error += 'Quantity is required.\n'
        }

        alert(error)
    }
    else{
        $.ajax({
            url: 'add_servicemix_item',
            method: 'POST',
            data: {'productservice_code': productservice_code, 'item_id': item, 'servicemix_quantity': quantity, 'item_uom': item_uom},
            success: function(){
                $('#add_item_quantity').val('');
                $('.items').val('').trigger('change');
                get_servicemix($('#productservice_code').val(), $('#brand_id').val());
            }
        })
    }
}