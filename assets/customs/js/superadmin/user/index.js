$(function(){

  var users_table = $('#users_table').DataTable({
    'processing': true,
  
      'bAutoWidth': false,
  
      'bSort': false,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      'ordering': true
  });

  // $('#add_turnaway_date').datepicker({

  //   autoclose: true

  // });


  // $('#turnaway_date').datepicker({

  //   autoclose: true

  // });



  // $('#daterange_btn').daterangepicker(

  //   {

  //     ranges: {

  //       'Today': [moment(), moment()],

  //       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

  //       'Last 7 Days': [moment().subtract(6, 'days'), moment()],

  //       'Last 30 Days': [moment().subtract(29, 'days'), moment()],

  //       'This Month': [moment().startOf('month'), moment().endOf('month')],

  //       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

  //     },

  //     startDate: moment().subtract(29, 'days'),

  //     endDate: moment()

  //   },

  //   function (start, end) {

  //     $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

  //     $('#start').val(start.format('MM/DD/YYYY'));

  //     $('#end').val(end.format('MM/DD/YYYY'));

  //   }

  // )



  $('.select2').select2();

  $('.dataTables_length').parent().remove();

  var add_button = '<div class="col-sm-6"><button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_user_modal">Add User</button><form method="POST" action="'+$('#base_url').val()+'superadmin/user/check_branch_no_transaction_yesterday"><input id="check_branch_no_transaction_yesterday" class="btn btn-primary pull-right" type="submit" name="export_excel" style="margin-right: 5px;" value="Not Updated CRM"></form><div>';

  $('#users_table_wrapper').children(":first").append(add_button)

  get_users()

})



$('#add_user_modal').on('show.bs.modal', function(){

  $('#add_user_modal').find('[class="modal-footer"]').show();

  $('#add_user_alert_success').hide();

  $('#add_user_form').show();

  $('#add_user_name').val('')

  $('#add_user_username').val('')

});



$('#user_modal').on('show.bs.modal', function () {

  $('#user_type').empty()

  $('#user_modal').find('[class="modal-footer"]').show();

  $('#user_alert_success').hide();

  $('#user_form').show();

  $('#user_id').val('')

  $('#user_name').val('')

  $('#user_username').val('')

  disable_editing()

});



function get_users(){

  var users_table = $('#users_table').DataTable()


  $('.dataTables_processing', $('#users_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'get_users',

    method: 'GET',

    success: function(data){

      var users = JSON.parse(data);
      console.log(users)
      users_table.clear().draw();

      for(let i = 0; i < users.length; i++){
        users_table.row.add([
  
          users[i].branch_name,

          users[i].user_name,
  
          users[i].user_username,

          users[i].user_lastlogin,

          '<button class="btn btn-info btn-sm" onclick="get_user('+users[i].user_id+')"><i class="fa fa-search"></i></button>'
  
        ])
      }

      $('.dataTables_processing', $('#users_table').closest('.dataTables_wrapper')).hide();

      users_table.draw()
    }

  })

}



function add_user(){

  $('#add_user_button').attr('disabled', 'disabled');

  $('#add_user_button').text('Adding...');

  // var loading = '<div class="loading small"></div>';

  // $('#add_user_button').append(loading);

  var branch_id = $('#add_branches').val();

  var user_name = $('#add_user_name').val()

  var user_username = $('#add_user_username').val()

  var user_password = $('#add_user_password').val()


  if(!branch_id || !user_name || !user_username || !user_password){

    alert('All fields is required!');

    $('#add_user_button').removeAttr('disabled');

    $('#add_user_button').text('Add');

    // $('.loading').remove();

  }

  else{

    $.ajax({

      url: 'add_user',

      method: 'POST',

      data: $('#add_user_form').serialize(),

      success: function(data){

        $('#add_user_button').removeAttr('disabled');

        $('#add_user_button').text('Add');

        // $('.loading').remove();

        $('#add_user_alert_success').show(400);

        $('#add_user_form').hide()

        $('#add_user_modal').find('[class="modal-footer"]').hide();

        get_users();

      }

    })

  }

}



function get_user(id){

  $('#user_modal').modal('show');


  $.ajax({

    url: 'get_user/'+id,

    success: function(data){

      var user = JSON.parse(data);

      $('#user_id').val(user.user_id);

      var selected_branch = $('#branches').find('option[value="' + user.branch_id + '"]');

      $('#branches').val(selected_branch.attr('value')).trigger('change');

      $('#user_name').val(user.user_name);

      $('#user_username').val(user.user_username);

    }

  })

}



function enable_editing(){

  $('#branches').removeAttr('disabled');

  $('#user_name').removeAttr('disabled');

  $('#user_username').removeAttr('disabled');

  $('#user_update_button').removeAttr('disabled');

}



function disable_editing(){

  $('#user_id').attr('disabled', 'disabled');

  $('#branches').attr('disabled', 'disabled');

  $('#user_name').attr('disabled', 'disabled');

  $('#user_username').attr('disabled', 'disabled');

  $('#user_update_button').attr('disabled', 'disabled');

}



function delete_user(){

  if (confirm('Are you sure you want to delete this user?')) {

    $.ajax({

      url: 'delete_user/' + $('#user_id').val(),

      success: function () {

        $('#user_function').text('deleted');

        $('#user_alert_success').show(400);

        $('#user_form').hide();

        $('#user_modal').find('[class="modal-footer"]').hide();

        get_users();

      }

    })

  }

}



function update_user(){

  var branch_id = $('#branches').val();

  var user_name = $('#user_name').val()

  var user_username = $('#user_username').val()

  if(!branch_id || !user_name || !user_username){

    alert('All fields is required!');

  }

  else {

    $.ajax({

      url: 'update_user/'+$('#user_id').val(),

      method: 'POST',

      data: $('#user_form').serialize(),

      success: function () {

          $('#user_function').text('updated');

          $('#user_alert_success').show(400);

          $('#user_form').hide()

          $('#user_modal').find('[class="modal-footer"]').hide();

          get_users();

      }

    })

  }

}



function daterange_search(){

  var date = $.trim($('#daterange_btn').find('span').text());

  var start = $('#start').val();

  var end = $('#end').val();

  var branch_id = $('#branches').select2('val');

  var error = '';



  if (date == 'Select date' || (typeof $('#branches').val() != 'undefined' && !$('#branches').val())){

    if(typeof $('#branches').val() != 'undefined' && !$('#branches').val()){

      error += 'Branch is required.\n';

    }



    if(date == 'Select date'){

      error += 'Date is required.';

    }



    alert(error)

  }

  else{



    if(start == end){

      $('#turnaways_table').dataTable().fnDestroy();



      var turnaways_table = $('#turnaways_table').DataTable({

        'bAutoWidth': true,

        'processing': true,

        'ajax': {

          'url': 'get_turnaways_by_date',

          'method': 'POST',

          'data': {'date_start': start, 'branch_id': branch_id}

        },

        'createdRow': function (row, data) {

          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');

        }

      })



      turnaways_table.columns(0).visible(false);



      turnaways_table.order([1, 'desc']).draw();



    }

    else{

      $('#turnaways_table').dataTable().fnDestroy();



      var turnaways_table = $('#turnaways_table').DataTable({

        'bAutoWidth': true,

        'processing': true,

        'ajax': {

          'url': 'get_turnaways_by_date',

          'method': 'POST',

          'data': { 'date_start': start, 'date_end': end, 'branch_id': branch_id }

        },

        'createdRow': function (row, data) {

          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');

        }

      })



      turnaways_table.columns(0).visible(false);



      turnaways_table.order([1, 'desc']).draw();

    }

  }

}

function check_branch_no_transaction_yesterday(){

  $("#check_branch_no_transaction_yesterday").text('Extracting...');

  $("#check_branch_no_transaction_yesterday").text('Extracted.');

  $("#check_branch_no_transaction_yesterday").text('Not Updated CRM');

  $.ajax({
    url: 'check_branch_no_transaction_yesterday',
    method: 'GET',
    success: function(data){
      
    }
  })
  
}

