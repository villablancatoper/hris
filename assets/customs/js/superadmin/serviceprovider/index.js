$(function(){

  var serviceproviders_table = $('#serviceproviders_table').DataTable({
    'processing': true,
  
      'bAutoWidth': false,
  
      'bSort': false,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      'ordering': true
  });

  // $('#add_turnaway_date').datepicker({

  //   autoclose: true

  // });



  // $('#turnaway_date').datepicker({

  //   autoclose: true

  // });



  // $('#daterange_btn').daterangepicker(

  //   {

  //     ranges: {

  //       'Today': [moment(), moment()],

  //       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

  //       'Last 7 Days': [moment().subtract(6, 'days'), moment()],

  //       'Last 30 Days': [moment().subtract(29, 'days'), moment()],

  //       'This Month': [moment().startOf('month'), moment().endOf('month')],

  //       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

  //     },

  //     startDate: moment().subtract(29, 'days'),

  //     endDate: moment()

  //   },

  //   function (start, end) {

  //     $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

  //     $('#start').val(start.format('MM/DD/YYYY'));

  //     $('#end').val(end.format('MM/DD/YYYY'));

  //   }

  // )



  $('.select2').select2();

  $('.dataTables_length').parent().remove();

  var add_button = '<div class="col-sm-6"><button class="btn btn-primary pull-right" data-toggle="modal" data-target="#add_serviceprovider_modal">Add SP</button><div>';

  $('#serviceproviders_table_wrapper').children(":first").append(add_button)

  get_serviceproviders()

})



$('#add_serviceprovider_modal').on('show.bs.modal', function(){

  $('#add_serviceprovider_modal').find('[class="modal-footer"]').show();

  $('#add_serviceprovider_alert_success').hide();

  $('#add_serviceprovider_form').show();

  $('#add_serviceprovider_name').val('')

});



$('#serviceprovider_modal').on('show.bs.modal', function () {

  $('#serviceprovider_type').empty()

  $('#serviceprovider_modal').find('[class="modal-footer"]').show();

  $('#serviceprovider_alert_success').hide();

  $('#serviceprovider_form').show();

  $('#serviceprovider_id').val('')

  $('#serviceprovider_name').val('')

  disable_editing()

});



function get_serviceproviders(){

  var serviceproviders_table = $('#serviceproviders_table').DataTable()

  $('.dataTables_processing', $('#serviceproviders_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'get_serviceproviders',

    method: 'GET',

    success: function(data){

      var serviceproviders = JSON.parse(data);

      serviceproviders_table.clear().draw();

      for(let i = 0; i < serviceproviders.length; i++){
        serviceproviders_table.row.add([
  
          serviceproviders[i].branch_name,

          serviceproviders[i].serviceprovider_name,

          '<button class="btn btn-info btn-sm" onclick="get_serviceprovider('+serviceproviders[i].serviceprovider_id+')"><i class="fa fa-search"></i></button>'
  
        ])
      }

      $('.dataTables_processing', $('#serviceproviders_table').closest('.dataTables_wrapper')).hide();

      serviceproviders_table.draw()
    }

  })

}



function add_serviceprovider(){

  var branch_id = $('#add_branches').val();

  var serviceprovider_name = $('#add_serviceprovider_name').val();

  var serviceprovider_position = $('#add_serviceprovider_level').val();

  var error = '';

  if(!branch_id || !serviceprovider_name || !serviceprovider_position){

    if(!branch_id){
      error += "Branch is required\n";
    }

    if(!serviceprovider_name){
      error += "Name is required\n";
    }

    if(!serviceprovider_position){
      error += "Position is required\n";
    }

    alert(error);

  }

  else{

    $.ajax({

      url: 'add_serviceprovider',

      method: 'POST',

      data: $('#add_serviceprovider_form').serialize(),

      success: function(){

        $('#add_serviceprovider_alert_success').show(400);

        $('#add_serviceprovider_form').hide()

        $('#add_serviceprovider_modal').find('[class="modal-footer"]').hide();

        get_serviceproviders();

      }

    })

  }

}



function get_serviceprovider(id){

  $('#serviceprovider_modal').modal('show');


  $.ajax({

    url: 'get_serviceprovider/'+id,

    success: function(data){

      var serviceprovider = JSON.parse(data);

      $('#serviceprovider_id').val(serviceprovider.serviceprovider_id);

      var selected_branch = $('#branches').find('option[value="' + serviceprovider.branch_id + '"]');

      $('#branches').val(selected_branch.attr('value')).trigger('change');

      var selected_position = $('#serviceprovider_level').find('option[value="' + serviceprovider.position_id + '"]');

      $('#serviceprovider_level').val(selected_position.attr('value')).trigger('change');

      $('#serviceprovider_name').val(serviceprovider.serviceprovider_name);

      // $('#serviceprovider_level').val(serviceprovider.serviceprovider_level);

      $('#serviceprovider_empid').val(serviceprovider.serviceprovider_empid);

    }

  })

}



function enable_editing(){

  $('#branches').removeAttr('disabled');

  $('#serviceprovider_name').removeAttr('disabled');

  $('#serviceprovider_level').removeAttr('disabled');

  $('#serviceprovider_empid').removeAttr('disabled');  

  $('#serviceprovider_update_button').removeAttr('disabled');

}



function disable_editing(){

  $('#serviceprovider_id').attr('disabled', 'disabled');

  $('#branches').attr('disabled', 'disabled');

  $('#serviceprovider_name').attr('disabled', 'disabled');

  $('#serviceprovider_level').attr('disabled', 'disabled');

  $('#serviceprovider_empid').attr('disabled', 'disabled');  

  $('#serviceprovider_update_button').attr('disabled', 'disabled');

}



function delete_serviceprovider(){

  if (confirm('Are you sure you want to delete this serviceprovider?')) {

    $.ajax({

      url: 'delete_serviceprovider/' + $('#serviceprovider_id').val(),

      success: function () {

        $('#serviceprovider_function').text('deleted');

        $('#serviceprovider_alert_success').show(400);

        $('#serviceprovider_form').hide();

        $('#serviceprovider_modal').find('[class="modal-footer"]').hide();

        get_serviceproviders();

      }

    })

  }

}



function update_serviceprovider(){

  var branch_id = $('#branches').val();

  var serviceprovider_name = $('#serviceprovider_name').val()

  if(!branch_id || !serviceprovider_name){

    alert('All fields is required!');

  }

  else {

    $.ajax({

      url: 'update_serviceprovider/'+$('#serviceprovider_id').val(),

      method: 'POST',

      data: $('#serviceprovider_form').serialize(),

      success: function () {

          $('#serviceprovider_function').text('updated');

          $('#serviceprovider_alert_success').show(400);

          $('#serviceprovider_form').hide()

          $('#serviceprovider_modal').find('[class="modal-footer"]').hide();

          get_serviceproviders();

      }

    })

  }

}

