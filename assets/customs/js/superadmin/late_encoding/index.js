$(document).ready(function() {

  var lateencode_table = $('#lateencode_table').DataTable({

    "dom" : 'rtip',

    "bDestroy" : true,

    buttons: [{

      extend: 'excel',

    }],

    'bAutoWidth': false,

    'columnDefs': [

        // { targets: [3,4,5,6,7],

        //   className: "align-right" }

    ]

    })


  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

  get_lateencoding();

});


function get_lateencoding(){

  var lateencode_table = $('#lateencode_table').DataTable()

  var global_date_today = $('#global_date_today').val();




  $('.dataTables_processing', $('#lateencode_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'get_lateencoding',

    method: 'GET',

    success: function(data){

      var result = JSON.parse(data);
      console.log(result)
      lateencode_table.clear().draw();

      for(let i = 0; i < result.length; i++){

        var status = '';

        if (result[i].request_expiry > global_date_today) {
            status = '<small class="label label-success" style = "font-size: 14px;">Active&nbsp</small>';
        } else {
            status = '<small class="label label-danger" style = "font-size: 13px;">Expired</small>';
        }

                        
        lateencode_table.row.add([
  
          result[i].branch_name,

          result[i].user_name,
  
          result[i].date_requested,

          result[i].request_expiry,

          result[i].branch_allowencode,

          result[i].reason,

          status

          
  
        ])
      }

      $('.dataTables_processing', $('#lateencode_table').closest('.dataTables_wrapper')).hide();

      lateencode_table.draw()
    }

  })

}


function allow_lateencode_button(){
  
  var branch_id = $('#lateencode_branch').val();
  var lateencode_date = $('#lateencode_date').val();
  var requestor = $('#requestor').val();
  var reason = $('#reason').val();

  console.log(branch_id)
  console.log(lateencode_date)
  console.log(requestor)


  if (!branch_id || !lateencode_date || !requestor) {
      alert("Please fill all fields");
  } else {
      $.ajax({
        method: 'POST',
        url: $('#base_url').val()+'dashboard/allow_lateencode',
        data: {branch_id: branch_id, lateencode_date: lateencode_date, requestor: requestor, reason: reason},
        success: function(){
            $('#lateencode_message').html("<div class = 'alert alert-success' role = 'alert'>Success!</h5>")
            .hide()
            .fadeIn(100);
            setTimeout(function(){
            $('#allow_lateencode_modal').modal('hide')
              }, 2000);
            $("#allow_lateencode_form")[0].reset();
            get_lateencoding();


        }
      })  
      
  }

}



