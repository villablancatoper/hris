$(function(){

    var gcs_table = $('#gcs_table').DataTable({
      'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,
  
        'language': {
  
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      
        },
        'ordering': true
    });

    $('.dataTables_filter').addClass('pull-right')
  
  })
  
  
  
  $('#add_user_modal').on('show.bs.modal', function(){
  
    $('#add_user_modal').find('[class="modal-footer"]').show();
  
    $('#add_user_alert_success').hide();
  
    $('#add_user_form').show();
  
    $('#add_user_name').val('')
  
    $('#add_user_username').val('')
  
    $('#add_user_password').val('')
  
  });
  
  
  
  $('#user_modal').on('show.bs.modal', function () {
  
    $('#user_type').empty()
  
    $('#user_modal').find('[class="modal-footer"]').show();
  
    $('#user_alert_success').hide();
  
    $('#user_form').show();
  
    $('#user_id').val('')
  
    $('#user_name').val('')
  
    $('#user_username').val('')
  
    $('#user_password').val('')
  
    disable_editing()
  
  });
  
  
  
  function search_gcs(){
  
    var gcs_table = $('#gcs_table').DataTable()

    var search_item = $('#search_item').val();

    $('.dataTables_processing', $('#gcs_table').closest('.dataTables_wrapper')).show();

    if(!search_item){
        alert('Enter value first!');

        $('.dataTables_processing', $('#gcs_table').closest('.dataTables_wrapper')).hide();
    }
    else{

        $.ajax({
        
            url: 'search_gcs',
        
            method: 'GET',

            data: {'search_item': search_item},
        
            success: function(data){
        
                var gcs = JSON.parse(data);
        
                gcs_table.clear().draw();
        
                for(let i = 0; i < gcs.length; i++){
                gcs_table.row.add([
            
                    gcs[i].giftcheque_description,
        
                    gcs[i].giftcheque_barcode,
            
                    gcs[i].giftcheque_pin,
        
                    gcs[i].giftcheque_amount
                ])
                }
        
                $('.dataTables_processing', $('#gcs_table').closest('.dataTables_wrapper')).hide();
        
                gcs_table.draw()
            }
    
        })

    }
  
  }
  
  
  function get_user(id){
  
    $('#user_modal').modal('show');
  
  
    $.ajax({
  
      url: 'get_user/'+id,
  
      success: function(data){
  
        var user = JSON.parse(data);
  
        $('#user_id').val(user.user_id);
  
        var selected_branch = $('#branches').find('option[value="' + user.branch_id + '"]');
  
        $('#branches').val(selected_branch.attr('value')).trigger('change');
  
        $('#user_name').val(user.user_name);
  
        $('#user_username').val(user.user_username);
  
        $('#user_password').val(user.user_password);
  
      }
  
    })
  
  }
  
  
