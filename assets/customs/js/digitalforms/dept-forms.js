$(document).ready(function() {

    var form_list_table = $('#form_list_table').DataTable({
      "dom": '<"top">Bfrt<"bottom"lp><"clear">',
      "bDestroy" : true,
      "ordering": true,
      'order':[[0,"desc"]],
      'processing': true,
      'language': {
  
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
      
      },
      buttons: [{
        extend: 'excel',
      }],
      'bAutoWidth': false
    })
    
    $('.select2').select2();
  
    show_major_requests()
  
  });

  $('#selection').on('change', function() {

    if ($('#selection').val() == 'leave') {

      $('#leave_table').show();

      var form_list_table = $('#form_list_table').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].from_date,
              requests[i].to_date,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#leave_table').hide();
    }
    
    if ($('#selection').val() == 'Official Business') {

      $('#ob_table').show();

      var form_list_table = $('#form_list_table_1').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].time_in,
              requests[i].time_out,
              requests[i].covered_time,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#ob_table').hide();
    }
    
    if ($('#selection').val() == 'Overtime/Offsetting') {

      $('#ot_table').show();

      var form_list_table = $('#form_list_table_2').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].time_in,
              requests[i].time_out,
              requests[i].covered_time,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#ot_table').hide();
    }
    
    if ($('#selection').val() == 'Biometric No In/No Out') {

      $('#bio_table').show();

      var form_list_table = $('#form_list_table_3').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].time_in,
              requests[i].time_out,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#bio_table').hide();
    }

    if ($('#selection').val() == 'Change of Work Schedule') {

      $('#cow_table').show();

      var form_list_table = $('#form_list_table_4').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].time_in,
              requests[i].time_out,
              requests[i].new_time_in,
              requests[i].new_time_out,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#cow_table').hide();
    }

    if ($('#selection').val() == 'Branch Transfer') {

      $('#bt_table').show();

      var form_list_table = $('#form_list_table_5').DataTable()
      var req_type = $('#selection').val();
  
      $.ajax({
      
        url: 'specific_requests',
        method: 'POST',
  
        data: {
  
          type: req_type
  
        },
    
        success: function(data) {
    
          var requests = JSON.parse(data);
  
          form_list_table.clear().draw();
    
          for(let i = 0; i < requests.length; i++){
            form_list_table.row.add([  
              requests[i].form_id,
              requests[i].date_created,
              requests[i].user_name,
              requests[i].branch_name,
              requests[i].request_type,
              requests[i].transfer_branch,
              requests[i].request_status,
              requests[i].remarks,
              '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
            ])
          }
    
          $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
    
          form_list_table.draw();
          
        }
    
      })

    } else {
      $('#bt_table').hide();
    }

  });
  
  function sort_all(sort_index) {
  
    var form_list_table = $('#form_list_table').DataTable();
  
    if (sort_index == 'All') {
          
      form_list_table.column(3).search("", true, false ).draw();
  
    } else {
  
      form_list_table.column(3).search(""+sort_index+"", true, false ).draw();
  
    }
    
    
  }
  
  function show_major_requests() {
  
    var form_list_table = $('#form_list_table').DataTable()
  
    $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).show();
  
    $.ajax({
    
      url: 'get_major_request/',
  
      success: function(data) {
  
        var requests = JSON.parse(data);

        form_list_table.clear().draw();
        for(let i = 0; i < requests.length; i++){
          form_list_table.row.add([  
            requests[i].form_id,
            requests[i].date_created,
            requests[i].user_name,
            requests[i].branch_name,
            requests[i].request_type,
            requests[i].from_date,
            requests[i].to_date,
            requests[i].request_status,
            requests[i].remarks,
            '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
          ])
        }
  
        $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();
  
        form_list_table.draw();
        
      }
  
    })
  
  }
  
  function update_request() {
  
    var department = $('#department').val();
    var from = change_date($('#from_input_edit').datepicker('getDate'));
    var to = change_date($('#to_input_edit').datepicker('getDate'));
    var leave_type = $('#leave_type_edit').val();
    var remarks = $('#remarks').val();
    var today = new change_date(Date());
    var id = $('#form_id').val();
    console.log(id)
  
    if(!from){
  
      alert('All fields is required!');
  
    }
  
    else {
  
      $.ajax({
  
        url: 'digital_forms/update_request/' + id,
  
        method: 'POST',
  
        data: {
  
          department: department,
          from_date: from,
          to_date: to,
          leave_type: leave_type,
          remarks: remarks,
          date_modified: today
  
        },
  
        success: function() {
  
          $('#user_function').text('updated');
          $('#user_alert_success').show(400);
          $('#user_form').hide()
          $('#request_information').find('[class="modal-footer"]').hide();
  
          show_all_user_requests();
  
        }
  
      })
  
    }
  
  }
  
  function post_request(form_id) {
  
    $('#request_information').modal('show');
   
    $.ajax({
  
      url: 'post_request/',
      method: 'POST',
      data: {form_id: form_id},
      success: function(data) {
  
        var request_data = JSON.parse(data);
        
        $('#form_id').val(request_data['post_request'].form_id);
        $('#user_id').val(request_data['post_request'].user_id);
        $('#request_name').val(request_data['post_request'].user_name);
        $('#branch').val(request_data['post_request'].branch_name);
        $('#position_edit').val(request_data['post_request'].user_position);
        $('#from_input_edit').val(request_data['post_request'].from_date);
        $('#to_input_edit').val(request_data['post_request'].to_date);
        $('#leave_type_edit').val(request_data['post_request'].request_type);
        $('#info_time_in').val(request_data['post_request'].time_in);
        $('#info_time_out').val(request_data['post_request'].time_out);
        $('#cow_new_info_in').val(request_data['post_request'].new_time_in);
        $('#cow_new_info_out').val(request_data['post_request'].new_time_out);
        $('#info_transfer_branches').val(request_data['post_request'].transfer_branch);
        $('#ob_info_covered').val(request_data['post_request'].covered_time);
        $('#remarks').val(request_data['post_request'].remarks);
  
        if (request_data['post_request'].from_date && request_data['post_request'].to_date) {
          $('#dates').show();
        } else {
          $('#dates').hide();
        }
  
        if (request_data['post_request'].request_type == 'Vacation Leave' ||
            request_data['post_request'].request_type == 'Sick Leave' ||
            request_data['post_request'].request_type == 'Personal/Emergency Leave' ||
            request_data['post_request'].request_type == 'Maternity Leave' ||
            request_data['post_request'].request_type == 'Paternity Leave' ||
            request_data['post_request'].request_type == 'Others') {
          $('#reason_for_leave').show();
        } else {
          $('#reason_for_leave').hide();
        }
  
        if (request_data['post_request'].time_in && request_data['post_request'].time_out) {
          $('#bio_info').show();
        } else {
          $('#bio_info').hide();
        }
  
        if (request_data['post_request'].new_time_in && request_data['post_request'].new_time_out) {
          $('#cow_info').show();
        } else {
          $('#cow_info').hide();
        }
  
        if (request_data['post_request'].covered_time) {
          $('#ob_info').show();
        } else {
          $('#ob_info').hide();
        }

        var li = '';

        for(let i = 0; i < request_data['req_history'].length; i++) {
            li += '<li><h5 class="float-right">'+request_data['req_history'][i].date_created+'</h5><p>'+request_data['req_history'][i].form_history_remark+'</p></li>'
        }

        $('#history').html(li);

        if(request_data['post_request'].request_level == '1' || request_data['post_request'].request_level == '2' || request_data['post_request'].request_level == '3') {
          
          $("#user_edit_button").hide();
          $("#user_update_button").hide();
          $("#request_delete_button").hide();

        }
  
      }
  
    })
  
  }
  
  function enable_editing() {
  
    $('#from_input_edit').removeAttr('disabled');
    $('#to_input_edit').removeAttr('disabled');
    $('#leave_type_edit').removeAttr('disabled');
    $('#remarks').removeAttr('disabled');
    $('#user_update_button').removeAttr('disabled');
  
  }
  
  function disable_editing(){
  
    $('#user_id').attr('disabled', 'disabled');
    $('#branches').attr('disabled', 'disabled');
    $('#user_name').attr('disabled', 'disabled');
    $('#user_username').attr('disabled', 'disabled');
    $('#user_update_button').attr('disabled', 'disabled');
  
  }
  