$(document).ready(function() {

    var form_list_table = $('#form_list_table').DataTable({
      "dom": '<"top">Bfrt<"bottom"lp><"clear">',
      "bDestroy" : true,
      "ordering": true,
      'order':[[0,"desc"]],
      'processing': true,
      'language': {
  
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
      
      },
      buttons: [{
        extend: 'excel',
      }],
      'bAutoWidth': false
    })
    
    $('.select2').select2();
  
    show_all_pending_requests()
  
});

$('#request_approve').on('show.bs.modal', function() {

  $('#user_info').find('[class="modal-footer"]').show();

  $('#user_alert_success').hide();

  $('#user_form').show();

});


$('#request_disapprove_button').on('click', function() {
  
  $("#approval_reason_label").hide();
  $("#approval_reason").hide();
  $("#disapproval_reason_label").show();
  $("#disapproval_reason").show();

  $("#confirm_disapprove_button").show();
  $("#confirm_approve_button").hide();
  $("#request_cancel").show();

  $("#request_disapprove_button").hide();
  $("#request_approve_button").hide();

});

$('#request_approve_button').on('click', function() {
  
  $("#approval_reason_label").show();
  $("#approval_reason").show();
  $("#disapproval_reason_label").hide();
  $("#disapproval_reason").hide();

  $("#confirm_approve_button").show();
  $("#confirm_disapprove_button").hide();
  $("#request_cancel").show();

  $("#request_disapprove_button").hide();
  $("#request_approve_button").hide();

});


$('#request_cancel').on('click', function() {

  $("#approval_reason_label").hide();
  $("#approval_reason").hide();
  $("#disapproval_reason_label").hide();
  $("#disapproval_reason").hide();

  $("#confirm_disapprove_button").hide();
  $("#confirm_approve_button").hide();
  $("#request_cancel").hide();

  $("#request_disapprove_button").show();
  $("#request_approve_button").show();

});

  
function show_all_pending_requests() {

  var form_list_table = $('#form_list_table').DataTable()

  $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'get_all_pending_requests',
    method: 'GET',

    success: function(data) {

      var pendings = JSON.parse(data);
      form_list_table.clear().draw();

      for(let i = 0; i < pendings.length; i++){
        form_list_table.row.add([  
          pendings[i].form_id,
          pendings[i].date_created,
          pendings[i].user_name,
          pendings[i].request_type,
          pendings[i].request_status,
          '<button class="btn btn-info btn-sm" onclick="post_request('+pendings[i].form_id+')"><i class="fa fa-search"></i></button>'
        ])
      }

      $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();

      form_list_table.draw();
      
    }

  })

}


function post_request(form_id){

  $('#request_approve').modal('show');
 
  $.ajax({

    url: 'post_request/',
    method: 'POST',
    data: {form_id: form_id},
  
    success: function(data) {

      var request_data = JSON.parse(data);

      var src = request_data['post_request'].from_date;

      $('#form_id').val(request_data['post_request'].form_id);
      $('#user_id').val(request_data['post_request'].user_id);
      $('#request_name').val(request_data['post_request'].user_name);
      $('#branch').val(request_data['post_request'].branch_id);
      $('#position_edit').val(request_data['post_request'].user_position);
      $('#department_edit').val(request_data['post_request'].user_department);
      $('#from_input_edit').val(request_data['post_request'].from_date);
      $('#to_input_edit').val(request_data['post_request'].to_date);
      $('#leave_type_edit').val(request_data['post_request'].request_type);
      $('#remarks').val(request_data['post_request'].remarks);

      var li = '';
      for(let i = 0; i < request_data['req_history'].length; i++) {
          li += '<li><h5 class="float-right">'+request_data['req_history'][i].date_created+'</h5><p>'+request_data['req_history'][i].form_history_remark+'</p></li>'
      }

      $('#history').html(li);

      if(request_data['post_request'].request_level == '1' || request_data['post_request'].request_level == '2' || request_data['post_request'].request_level == '3') {
        
        $("#user_edit_button").hide();
        $("#user_update_button").hide();
        $("#request_delete_button").hide();

      }

    }

  })

}

function approve_request() {
  $('#confirm_approve_button').attr('disabled', 'disabled');
  $('#confirm_approve_button').text('Adding...');

  var id = $('#form_id').val();

  $.ajax({

    url: 'approve_request/' + id,
    method: 'PATCH',

    success: function(data) {

      $('#user_function').text('updated');
      $('#user_alert_success').show(400);
      $('#user_form').hide()
      $('#user_modal').find('[class="modal-footer"]').hide();

      show_all_pending_requests();

    }

  });

}


$('#confirm_approve_button').on('click', function() {

  var id = $('#form_id').val();
  var reason = $('#approval_reason').val();

  $.ajax({

    url: 'approve_request/' + id,
    method: 'POST',
    data: {approve_reason: reason },

    success: function(data) {

      $('#user_function').text('updated');
      $('#user_alert_success').show(400);
      $('#user_form').hide()
      $('#request_approve').find('[class="modal-footer"]').hide();

      show_all_pending_requests();

    }

  });

});

$('#confirm_disapprove_button').on('click', function() {

  var id = $('#form_id').val();
  var reason = $('#disapproval_reason').val();

  $.ajax({

    url: 'disapprove_request/' + id,
    data: {disapprove_reason: reason },
    method: 'POST',

    success: function(data) {

      $('#user_function').text('updated');
      $('#user_alert_success').show(400);
      $('#user_form').hide()
      $('#request_approve').find('[class="modal-footer"]').hide();

      show_all_pending_requests();

    }

  });

});
