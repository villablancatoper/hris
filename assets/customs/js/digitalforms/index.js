$(document).ready(function() {

  var form_list_table = $('#form_list_table').DataTable({
    "dom": '<"top">Bfrt<"bottom"lp><"clear">',
    "bDestroy" : true,
    "ordering": true,
    'order':[[0,"desc"]],
    'processing': true,
    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
    
    },
    buttons: [{
      extend: 'excel',
    }],
    'bAutoWidth': false
  })
  
  $('.select2').select2();

  show_all_user_requests()

});

$('#modal_new_form').on('show.bs.modal', function() {

  $('#modal_new_form').find('[class="modal-footer"]').show();

  $('#success_add_request').hide();

  $('#add_new_request_form').show();

});

$('#request_information').on('show.bs.modal', function() {

  $('#request_information').find('[class="modal-footer"]').show();

  $('#user_alert_success').hide();
  
  $('#user_form').show();

});

$('#request_information').on('hidden.bs.modal', function () {
  console.log('closing')

  $('#from_input_edit').val('');
  $('#to_input_edit').val('');
  $('#leave_type_edit').val('');
  $('#info_time_in').val('');
  $('#info_time_out').val('');
  $('#remarks').val('');

})



//from datepicker

$(document).ready(function(){

  $("#datetimepicker1, #datetimepicker2, #edit_from, #edit_to").datepicker({
    
    dateFormat: 'yyyy-mm-dd',

    minDate:new Date(),
    autoclose: true,
    changeMonth: false
  }).val();

});


$('#form_type').on('change', function() {

  if (this.value == 'leave_request') {

    $("#leave_form_selected").show();
    $("#schedule_form_selected").hide();
    $("#bio").hide();
    $("#branch_transfer").hide();
    $("#os").hide();
    $("#cow").hide();
    $("#ob").hide();
    $("#ot").hide();
    
  } else {

    $("#schedule_form_selected").show();
    $("#leave_form_selected").hide();
    $("#bio").hide();
    $("#branch_transfer").hide();
    $("#os").hide();
    $("#cow").hide();
    $("#ob").hide();
    $("#ot").hide();

  }

});

$('#schedule_type').on('change', function() {

    if (this.value == 'bio') {

      $("#bio").show();
      $("#branch_transfer").hide();
      $("#os").hide();
      $("#cow").hide();
      $("#ob").hide();
      $("#ot").hide();
      
    } else if (this.value == 'branch_transfer') {

      $("#branch_transfer").show();
      $("#bio").hide();
      $("#os").hide();
      $("#cow").hide();
      $("#ob").hide();
      $("#ot").hide();

    } else if (this.value == 'os') {

      $("#os").show();
      $("#bio").hide();
      $("#branch_transfer").hide();
      $("#cow").hide();
      $("#ob").hide();
      $("#ot").hide();

    } else if (this.value == 'cow') {

      $("#cow").show();
      $("#bio").hide();
      $("#branch_transfer").hide();
      $("#os").hide();
      $("#ob").hide();
      $("#ot").hide();

    } else if (this.value == 'ob') {

      $("#ob").show();
      $("#bio").hide();
      $("#branch_transfer").hide();
      $("#os").hide();
      $("#cow").hide();
      $("#ot").hide();

    } else if (this.value == 'ot') {

      $("#ot").show();
      $("#bio").hide();
      $("#branch_transfer").hide();
      $("#os").hide();
      $("#cow").hide();
      $("#ob").hide();

    }

});

$('#request_delete_button').on('click', function() {

  if (confirm('Are you sure you want to delete the request?')) {

    $.ajax({

      url: 'digital_forms/delete_request/' + $('#form_id').val(),

      success: function() {

        $('#user_function').text('deleted');

        $('#user_alert_success').show(400);

        $('#user_form').hide();

        $('#request_information').find('[class="modal-footer"]').hide();

        show_all_user_requests();

      }

    })

  }

});

function change_date(data) {

  var d = new Date(data),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return new_date = [year, month, day].join('-');

}

function sort_all(sort_index){

  var form_list_table = $('#form_list_table').DataTable();

  if (sort_index == 'All') {
        
    form_list_table.column(3).search("", true, false ).draw();

  } else {
    console.log(sort_index)

    form_list_table.column(3).search(""+sort_index+"", true, false ).draw();

  }
  
  
}

function new_request_form() {
  $('#add_request_button').attr('disabled', 'disabled');
  $('#add_request_button').text('Adding...');

  var form_type = $('#form_type').val()

  if (form_type == 'leave_request') {

    var department = $('#department').val();
    var from = $('#datetimepicker1').datepicker('getDate')
    var to = $('#datetimepicker2').datepicker('getDate')
    var format_from = change_date(from);
    var format_to = change_date(to);
    var leave_type = $('#leave_type').val();
    var comment = $('#comment').val();
  
  
    if(!comment) {
  
      $('#comment').css('border-color', 'red');
      $('#comment_error').show().delay(5000).fadeOut();
  
      $('#add_request_button').removeAttr('disabled');
      $('#add_request_button').text('Add');
  
    } else {
  
      $('#comment').css('border-color', '#d2d6de');
      $('#comment_error').hide()
  
    }
  
    
    if (!leave_type) {
  
      $('#leave_type').css('border-color', 'red');
      $('#leave_type_error').show().delay(5000).fadeOut();
  
      $('#add_request_button').removeAttr('disabled');
      $('#add_request_button').text('Add');
  
    } else {
  
      $('#leave_type').css('border-color', '#d2d6de');
      $('#leave_type_error').hide()
  
    }
  
  
    if (!from) {
  
      $('#from_input').css('border-color', 'red');
      $('#datetimepicker1').datepicker().val('');
      $('#from_error').show().delay(5000).fadeOut();
  
      $('#add_request_button').removeAttr('disabled');
      $('#add_request_button').text('Add');
  
    } else {
  
      $('#from_input').css('border-color', '#d2d6de');
      $('#from_error').hide()
  
    }
  
  
    if (!to) {
  
      $('#to_input').css('border-color', 'red');
      $('#datetimepicker2').val('');
      $('#to_error').show().delay(5000).fadeOut();
  
      $('#add_request_button').removeAttr('disabled');
      $('#add_request_button').text('Add');
  
    } else {
  
      $('#to_input').css('border-color', '#d2d6de');
      $('#to_error').hide()
  
    }
  
    
    if (comment && leave_type && format_from && format_to) {
  
      $.ajax({
  
        url: 'digital_forms/add_new_request_form',
        method: 'POST',
        data: {
  
          department: department,
          from_date: format_from,
          to_date: format_to,
          leave_type: leave_type,
          comment: comment,
          form_type: form_type,
  
        },
              
        success: function(data){
  
          $('#add_request_button').removeAttr('disabled');
          $('#add_request_button').text('Add');
  
          $('#success_add_request').show(400);
          $('#add_new_request_form').hide()
          $('#modal_new_form').find('[class="modal-footer"]').hide();
  
          $('#from_input').val('');
          $('#to_input').val('');
          $('#leave_type').val('');
          $('#comment').val('');
          show_all_user_requests();
        }
  
      })
  
    }

  } else if (form_type == 'schedule_request') {

    var schedule_type = $('#schedule_type').val();

    if (schedule_type == 'bio') {

      var bio_in = $('#bio_time_in').val();
      var bio_out = $('#bio_time_out').val();
      var bio_comment = $('#bio_comment').val();
      

      if (bio_in && bio_out && bio_comment) {
  
        $.ajax({
    
          url: 'digital_forms/add_new_request_form',
          method: 'POST',
          data: {
    
            time_in: bio_in,
            time_out: bio_out,
            comment: bio_comment,
            form_type: schedule_type,
    
          },
                
          success: function(data){
    
            $('#add_request_button').removeAttr('disabled');
            $('#add_request_button').text('Add');
    
            $('#success_add_request').show(400);
            $('#add_new_request_form').hide()
            $('#modal_new_form').find('[class="modal-footer"]').hide();
    
            $('#bio_time_in').val('');
            $('#bio_time_out').val('');
            $('#bio_comment').val('');
            show_all_user_requests();
          }
    
        })
    
      } else {

        alert('Please Fill up all the required fields');

      }

    } else if (schedule_type == 'cow') {

      var cow_in = $('#cow_time_in').val();
      var cow_out = $('#cow_time_out').val();
      var cow_new_in = $('#cow_new_time_in').val();
      var cow_new_out = $('#cow_new_time_out').val();
      var cow_comment = $('#cow_comment').val();
      

      if (cow_in && cow_out && cow_new_in && cow_new_out && cow_comment) {
  
        $.ajax({
    
          url: 'digital_forms/add_new_request_form',
          method: 'POST',
          data: {
    
            time_in: cow_in,
            time_out: cow_out,
            new_time_in: cow_new_in,
            new_time_out: cow_new_out,
            comment: cow_comment,
            form_type: schedule_type,
    
          },
                
          success: function(data) {
    
            $('#add_request_button').removeAttr('disabled');
            $('#add_request_button').text('Add');
    
            $('#success_add_request').show(400);
            $('#add_new_request_form').hide()
            $('#modal_new_form').find('[class="modal-footer"]').hide();
    
            $('#bio_time_in').val('');
            $('#bio_time_out').val('');
            $('#bio_comment').val('');
            show_all_user_requests();
          }
    
        })
    
      } else {

        alert('Please Fill up all the required fields');

      }

    } else if (schedule_type == 'branch_transfer') {

      var transfer_branch = $('#transfer_branches').val();
      var bt_comment = $('#bt_comment').val();
      

      if (transfer_branch && bt_comment) {
  
        $.ajax({
    
          url: 'digital_forms/add_new_request_form',
          method: 'POST',
          data: {
    
            transfer_branch: transfer_branch,
            comment: bt_comment,
            form_type: schedule_type,
    
          },
                
          success: function(data) {
    
            $('#add_request_button').removeAttr('disabled');
            $('#add_request_button').text('Add');
    
            $('#success_add_request').show(400);
            $('#add_new_request_form').hide()
            $('#modal_new_form').find('[class="modal-footer"]').hide();
    
            $('#cow_time_in').val();
            $('#cow_time_out').val();
            $('#cow_new_time_in').val();
            $('#cow_new_time_out').val();
            $('#cow_comment').val();

            show_all_user_requests();
          }
    
        })
    
      } else {

        alert('Please Fill up all the required fields');

      }

    } else if (schedule_type == 'ob') {

      var covered_time = $('#ob_covered').val();
      var from = $('#ob_time_in').val();
      var to = $('#ob_time_out').val();
      var comment = $('#ob_comment').val();
      

      if (covered_time && from && to && comment) {
  
        $.ajax({
    
          url: 'digital_forms/add_new_request_form',
          method: 'POST',
          data: {
    
            covered_time: covered_time,
            from: from,
            to: to,
            comment: comment,
            form_type: schedule_type,
    
          },
                
          success: function(data) {
    
            $('#add_request_button').removeAttr('disabled');
            $('#add_request_button').text('Add');
    
            $('#success_add_request').show(400);
            $('#add_new_request_form').hide()
            $('#modal_new_form').find('[class="modal-footer"]').hide();
    
            $('#ob_covered').val();
            $('#ob_time_in').val();
            $('#ob_time_out').val();
            $('#ob_comment').val();

            show_all_user_requests();
          }
    
        })
    
      } else {

        alert('Please Fill up all the required fields');

      }

    } else if (schedule_type == 'ot') {

      var covered_time = $('#ot_covered').val();
      var from = $('#ot_time_in').val();
      var to = $('#ot_time_out').val();
      var comment = $('#ot_comment').val();
      

      if (covered_time && from && to && comment) {
  
        $.ajax({
    
          url: 'digital_forms/add_new_request_form',
          method: 'POST',
          data: {
    
            covered_time: covered_time,
            from: from,
            to: to,
            comment: comment,
            form_type: schedule_type,
    
          },
                
          success: function(data) {
    
            $('#add_request_button').removeAttr('disabled');
            $('#add_request_button').text('Add');
    
            $('#success_add_request').show(400);
            $('#add_new_request_form').hide()
            $('#modal_new_form').find('[class="modal-footer"]').hide();
    
            $('#ot_covered').val();
            $('#ot_time_in').val();
            $('#ot_time_out').val();
            $('#ot_comment').val();

            show_all_user_requests();
          }
    
        })
    
      } else {

        alert('Please Fill up all the required fields');

      }

    }

  } 

}

function show_all_user_requests() {

  var form_list_table = $('#form_list_table').DataTable()

  $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'digital_forms/get_request/',

    success: function(data) {

      var requests = JSON.parse(data);
      form_list_table.clear().draw();
      for(let i = 0; i < requests.length; i++){
        form_list_table.row.add([  
          requests[i].form_id,
          requests[i].date_created,
          requests[i].request_type,
          requests[i].request_status,
          '<button class="btn btn-info btn-sm" onclick="post_request('+requests[i].form_id+')"><i class="fa fa-search"></i></button>'
        ])
      }

      $('.dataTables_processing', $('#form_list_table').closest('.dataTables_wrapper')).hide();

      form_list_table.draw();
      
    }

  })

}

function update_request() {

  var department = $('#department').val();
  var from = change_date($('#from_input_edit').datepicker('getDate'));
  var to = change_date($('#to_input_edit').datepicker('getDate'));
  var leave_type = $('#leave_type_edit').val();
  var remarks = $('#remarks').val();
  var today = new change_date(Date());
  var id = $('#form_id').val();
  console.log(id)

  if(!from){

    alert('All fields is required!');

  }

  else {

    $.ajax({

      url: 'digital_forms/update_request/' + id,

      method: 'POST',

      data: {

        department: department,
        from_date: from,
        to_date: to,
        leave_type: leave_type,
        remarks: remarks,
        date_modified: today

      },

      success: function() {

        $('#user_function').text('updated');
        $('#user_alert_success').show(400);
        $('#user_form').hide()
        $('#request_information').find('[class="modal-footer"]').hide();

        show_all_user_requests();

      }

    })

  }

}

function post_request(form_id) {

  $('#request_information').modal('show');
 
  $.ajax({

    url: 'digital_forms/post_request/',
    method: 'POST',
    data: {form_id: form_id},
    success: function(data) {

      var request_data = JSON.parse(data);

      $('#form_id').val(request_data['post_request'].form_id);
      $('#user_id').val(request_data['post_request'].user_id);
      $('#request_name').val(request_data['post_request'].user_name);
      $('#branch').val(request_data['post_request'].branch_name);
      $('#position_edit').val(request_data['post_request'].user_position);
      $('#from_input_edit').val(request_data['post_request'].from_date);
      $('#to_input_edit').val(request_data['post_request'].to_date);
      $('#leave_type_edit').val(request_data['post_request'].request_type);
      $('#info_time_in').val(request_data['post_request'].time_in);
      $('#info_time_out').val(request_data['post_request'].time_out);
      $('#cow_new_info_in').val(request_data['post_request'].new_time_in);
      $('#cow_new_info_out').val(request_data['post_request'].new_time_out);
      $('#info_transfer_branches').val(request_data['post_request'].transfer_branch);
      $('#ob_info_covered').val(request_data['post_request'].covered_time);
      $('#remarks').val(request_data['post_request'].remarks);

      if (request_data['post_request'].from_date && request_data['post_request'].to_date) {
        $('#dates').show();
      } else {
        $('#dates').hide();
      }

      if (request_data['post_request'].request_type == 'Vacation Leave' ||
          request_data['post_request'].request_type == 'Sick Leave' ||
          request_data['post_request'].request_type == 'Personal/Emergency Leave' ||
          request_data['post_request'].request_type == 'Maternity Leave' ||
          request_data['post_request'].request_type == 'Paternity Leave' ||
          request_data['post_request'].request_type == 'Others') {
        $('#reason_for_leave').show();
      } else {
        $('#reason_for_leave').hide();
      }

      if (request_data['post_request'].time_in && request_data['post_request'].time_out) {
        $('#bio_info').show();
      } else {
        $('#bio_info').hide();
      }

      if (request_data['post_request'].new_time_in && request_data['post_request'].new_time_out) {
        $('#cow_info').show();
      } else {
        $('#cow_info').hide();
      }

      if (request_data['post_request'].covered_time) {
        $('#ob_info').show();
      } else {
        $('#ob_info').hide();
      }
      
      var li = '';
      for(let i = 0; i < request_data['req_history'].length; i++) {
          li += '<li><h5 class="float-right">'+request_data['req_history'][i].converted_date+'</h5><p> Remarks: '+request_data['req_history'][i].form_history_remark+'</p></li>'
      }

      $('#history').html(li);

      if(request_data['post_request'].request_level == '1' || request_data['post_request'].request_level == '2' || request_data['post_request'].request_level == '3') {
        
        $("#user_edit_button").hide();
        $("#user_update_button").hide();
        $("#request_delete_button").hide();

      }

    }

  })

}

function enable_editing() {

  $('#from_input_edit').removeAttr('disabled');
  $('#to_input_edit').removeAttr('disabled');
  $('#leave_type_edit').removeAttr('disabled');
  $('#remarks').removeAttr('disabled');
  $('#user_update_button').removeAttr('disabled');

}

function disable_editing(){

  $('#user_id').attr('disabled', 'disabled');
  $('#branches').attr('disabled', 'disabled');
  $('#user_name').attr('disabled', 'disabled');
  $('#user_username').attr('disabled', 'disabled');
  $('#user_update_button').attr('disabled', 'disabled');

}
