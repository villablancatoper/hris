$(function(){

  $('.select2').select2();

  $('match-height').matchHeight();

  $('#dsrs_detailed_table').DataTable({

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    },

    'bAutoWidth': false,

    'columnDefs': [
        { targets: [3, 4, 7], className: "align-right" },
    ],
    dom: 'Bfrtip',
    buttons: [{
      extend: 'excel',
      title: 'Detailed Sales Report',
      filename: 'Detailed Sales Report'
    }]

  })

  $('#daterange_btn').daterangepicker(

    {

      ranges: {

        'Today': [moment(), moment()],

        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days': [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month': [moment().startOf('month'), moment().endOf('month')],

        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate: moment()

    },

    function (start, end) {

      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

      $('#start').val(start.format('MM/DD/YYYY'));

      $('#end').val(end.format('MM/DD/YYYY'));

    }

  )

  $('.dt-button').addClass('btn btn-default');

})



function search_dsrs_detailed(){

  $('.btn-flat').attr('disabled', 'disabled');

  var dsrs_detailed_table = $('#dsrs_detailed_table').DataTable();

  $('.dataTables_processing', $('#dsrs_detailed_table').closest('.dataTables_wrapper')).show();
  var branch = $('#branches').select2('val');

  var start = $('#start').val();

  var end = $('#end').val()

  var dsrs_detailed_table = $('#dsrs_detailed_table').DataTable();

  dsrs_detailed_table.clear().draw()

  if(!branch || !start || !end){

    if(!branch){
      alert('Branch is required.')
    }

    if(!start || !end){
      alert('Date is required.')
    }

    $('.dataTables_processing', $('#dsrs_detailed_table').closest('.dataTables_wrapper')).hide();

    $('.btn-flat').removeAttr('disabled');
  }

  else{

    dsrs_detailed_table.clear().draw();



    $.ajax({

      url: 'search_dsrs_detailed',

      method: 'GET',

      data: {'branch': branch, 'start': start, 'end': end},

      success: function(data){

        var dsrs_detailed = JSON.parse(data);



        for(let i = 0; i < dsrs_detailed.length; i++){

          dsrs_detailed_table.row.add([

            dsrs_detailed[i].transaction_id,

            dsrs_detailed[i].transaction_date,

            dsrs_detailed[i].branch_name,

            dsrs_detailed[i].customer_firstname + ' ' + dsrs_detailed[i].customer_lastname,

            dsrs_detailed[i].transaction_customerstatus,

            dsrs_detailed[i].serviceprovider_name,

            dsrs_detailed[i].assister_name,

            dsrs_detailed[i].productservice_description,

            dsrs_detailed[i].productretail_name,

            dsrs_detailed[i].transaction_docketno,

            dsrs_detailed[i].transaction_orno,

            dsrs_detailed[i].transactiondetails_totalsales

          ])

        }

        $.ajax({
          url: 'get_dsrs_detailed_branch',
          method: 'GET',
          data: {'branch': branch, 'start': start, 'end': end},
          success: function(data){
              var dsrs_detailed_total = JSON.parse(data);
              
              var total_sales = (eval(dsrs_detailed_total.total_sales.total_sales) + 0.00).toFixed(2);
              var total_cash = (eval(dsrs_detailed_total.total_cash.total_cash) + 0.00).toFixed(2);
              var total_card = (eval(dsrs_detailed_total.total_card.total_card) + 0.00).toFixed(2);
              var total_gc = (eval(dsrs_detailed_total.total_gc.total_gc) + 0.00).toFixed(2);
              var total_sales_services = (eval(dsrs_detailed_total.total_sales_services) + 0.00).toFixed(2);
              var total_sales_otc = (eval(dsrs_detailed_total.total_sales_otc.total_sales_otc) + 0.00).toFixed(2);

              var todate_total_sales = (eval(dsrs_detailed_total.todate_total_sales.todate_total_sales) + 0.00).toFixed(2);
              var todate_tph = (eval(dsrs_detailed_total.todate_tph) + 0.00).toFixed(2);
              var todate_otc = (eval(dsrs_detailed_total.todate_otc.todate_otc) + 0.00).toFixed(2);
              var todate_services = (eval(dsrs_detailed_total.todate_services) + 0.00).toFixed(2);
              
              $('#total_sales').text(numberWithCommas(numberWithCommas(total_sales)));
              $('#total_cash').text(numberWithCommas(total_cash));
              $('#total_card').text(numberWithCommas(total_card));
              $('#total_gc').text(numberWithCommas((eval(total_gc)+0.00).toFixed(2)));
              $('#total_sales_otc').text(numberWithCommas(total_sales_otc));
              $('#total_sales_services').text(numberWithCommas(total_sales_services));
              $('#total_turnaway').text(numberWithCommas(eval(dsrs_detailed_total.total_turnaway.total_turnaway) + 0));
              $('#total_regular').text(numberWithCommas(eval(dsrs_detailed_total.total_regular) + 0));
              $('#total_walkin').text(numberWithCommas(eval(dsrs_detailed_total.total_walkin) + 0));
              $('#total_transfer').text(numberWithCommas(eval(dsrs_detailed_total.total_transfer) + 0));
              $('#total_head_count').text(numberWithCommas(eval(dsrs_detailed_total.total_head_count) + 0));

              $('#todate_total_sales').text(numberWithCommas(numberWithCommas(todate_total_sales)));
              $('#todate_tph').text(numberWithCommas(numberWithCommas(todate_tph)));
              $('#todate_otc').text(numberWithCommas(numberWithCommas(todate_otc)));
              $('#todate_services').text(numberWithCommas(numberWithCommas(todate_services)));

              dsrs_detailed_table.draw()
      
              $('.dataTables_processing', $('#dsrs_detailed_table').closest('.dataTables_wrapper')).hide();
      
              $('.btn-flat').removeAttr('disabled');
          }
        })

      }

    })

  }

}