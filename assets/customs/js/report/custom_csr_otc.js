$(function(){

    $('.match-height').matchHeight();

    var csr_table = $('#csr_table').DataTable({

      'processing': true,

      'bAutoWidth': false,

      'bSort': false, 

      'columnDefs': [

          { targets: [4], className: "align-right" },
          { targets: [3], className: "align-center" },


      ],

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

      },

      dom: 'Bfrtip',

      buttons: [{

        extend: 'excel',

        title: 'Category Sales Report | '+$('#current_date').val(),

        filename: $('#user_branch_name').text()+' (Category Sales Report)'

      }],

      'ordering': false,

      "lengthChange": false,

      "paging": false,

      "bInfo" : false

    })


    $('#daterange_btn').daterangepicker(

      {

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],


          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

    $('.dt-button').addClass('btn btn-default');

    $('.select2').select2();


})

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);
                 $.ajax({
                  url: "fetch_otc",
                  method: "POST",
                  data: {brand_id:brand_id},
                  success:function(data){
                    $('#otc').html(data);
                  }

                 })

          }

      })


    }

})


function search_custom_csr_otc(){
    var brand_id = $('#brand').val();
    var branch_id = $('#branch').val();
    var otc = $('#otc').val();
    var start = $('#start').val();
    var end = $('#end').val();
    var csr_table = $('#csr_table').DataTable()
    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).show();
    if (!brand_id || !branch_id || !otc || !start || !end) {
        $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
        alert("Please select all fields");
        
    } else {
        csr_table.clear().draw();
         $.ajax({
              url: "search_custom_csr_otc",
              method: "POST",
              data: {brand_id: brand_id, branch_id: branch_id, otc: otc, start: start, end: end},
               beforeSend: function(){
                    $('#total_count').html('');
                    $('#total_sales').html('');
              },
              success:function(data){
                var result = JSON.parse(data);
                console.log(result);
                var total_count = 0;
                var total_sales = 0;
                for (let i = 0; i < result.length; i++) {
                    total_count = eval(total_count) + eval(result[i].count);
                    total_sales = eval(total_sales) + eval(result[i].sales);
                    csr_table.row.add([   
                    result[i].brand_name,
                    result[i].branch_name,
                    result[i].productretail_name,
                    numberWithCommas(result[i].count),
                    numberWithCommas(parseFloat(result[i].sales).toFixed(2))
                  ])
                }
                if (total_count === 0 || total_count === null || total_count === '') {
                    total_count = '';
                } else {
                    total_count = numberWithCommas(total_count);
                }

                if (total_sales === 0 || total_sales === null || total_sales === '') {
                    total_sales = '';
                } else {
                    total_sales = numberWithCommas(parseFloat(total_sales).toFixed(2));
                }

                $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
                $('#total_count').html(total_count);
                $('#total_sales').html(total_sales);
                csr_table.draw()
              }

         })

    }



}





  

  

  

  

  

  