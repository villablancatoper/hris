$(function(){

    

    $('.match-height').matchHeight();

  

    var csr_table = $('#csr_table').DataTable({

  

      'processing': true,

  

      'bAutoWidth': false,

  

      'bSort': false, 

  

      'columnDefs': [

          { targets: [3, 2], className: "align-right" },

      ],

      'language': {

  

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    

      },

      dom: 'Bfrtip',

      buttons: [{

        extend: 'excel',

        title: $('#user_branch_name').text()+' | Category Sales Report | '+$('#current_date').val(),

        filename: $('#user_branch_name').text()+' (Category Sales Report)'

      }],

      'ordering': false,



      "lengthChange": false,

  

      "paging": false,

      

      "bInfo" : false

    })

  

    csr_table.buttons().container()

          .appendTo( '#csr_table .col-sm-6:eq(0)' );

    

    $('#daterange_btn').daterangepicker(

  

      {

  

        ranges: {

  

          'Today': [moment(), moment()],

  

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

  

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

  

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

  

          'This Month': [moment().startOf('month'), moment().endOf('month')],

  

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

  

        },

  

        startDate: moment().subtract(29, 'days'),

  

        endDate: moment()

  

      },

  

      function (start, end) {

  

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

  

        $('#start').val(start.format('MM/DD/YYYY'));

  

        $('#end').val(end.format('MM/DD/YYYY'));

  

      }

  

    )

  

    $('.dt-button').addClass('btn btn-default');



    $('.select2').select2();

  



      var brand_id = $('#brand').val();

        $.ajax({
          url: "fetch_branches",
          method: "POST",
          data: {brand_id:brand_id},
          success:function(data){
            $('#branches').html(data);
                 $.ajax({
                  url: "fetch_category_with_all",
                  method: "POST",
                  data: {brand_id:brand_id},
                  success:function(data){
                    $('#categories').html(data);
                  }
                 })
          }
        })
})

$('#categories').change(function(){
      var category_id = $('#categories').val();
      // alert(category_id)
      if (category_id != 'All Categories') {
        $('#productservices').prop('disabled', false);
        $.ajax({

            url: "fetch_services_with_all",

            method: "POST",

            data: {category_id:category_id},

            success:function(data){
              $('#productservices').html(data);
            }
        })
      } else {
        $('#productservices').html('');
        $('#productservices').prop('disabled', 'disabled');
      }
  })

  

function search_custom_csr_branch(){

  

    var test = $('#csr_table').DataTable()



    test.destroy();



    var date;

    var category_code = $('#categories').val();

    var productservice_code = $('#productservices').val();



    var start = $('#start').val();

    var end = $('#end').val();



    if(start && start != end){

        date = start + ' to ' + end;

    }



    if(start && start == end){

        date = start;

    }



    var csr_table = $('#csr_table').DataTable({



        'processing': true,



        'bAutoWidth': false,



        'bSort': false, 



        'columnDefs': [

            { targets: [3, 2], className: "align-right" },

        ],

        'language': {



            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

        

        },

        dom: 'Bfrtip',

        buttons: [{

            extend: 'excel',

            title: $('#user_branch_name').text()+' | Category Sales Report | '+date,

            filename: $('#user_branch_name').text()+' (Category Sales Report)'

        }],

        'ordering': false,



        "lengthChange": false,

    

        "paging": false,

        

        "bInfo" : false

    })



    $('.dt-button').addClass('btn btn-default');



    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).show();

    

    csr_table.clear().draw();

    

    if(!start || !end || (!category_code && !productservice_code)){

        var error = '';



        if(!start || !end){

            error += 'Date is required.\n';

        }


        alert(error);



        $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();

    }

    else{

        $.ajax({

            

            url: 'search_custom_csr_branch',

            method: 'GET',

            data: {'start': start, 'end': end, 'category_code': category_code, 'productservice_code': productservice_code},

            success: function(data){

                var data = JSON.parse(data);
                var csr_data = [];
                csr_data = csr_data.concat(...data)
                csr_data.sort(alphabetically(true));
                var total_sales = 0;
                var category_total_sales = 0;
                var last_category = '';
                
                for(let i = 0; i < csr_data.length; i++){

                    var service = csr_data[i].productservice_description;
                    var category = csr_data[i].category_name;

                    
                    if(csr_data[i].productservice_description == null || csr_data[i].category_name == null){
                        service = 'OTC';
                        category = 'OTC';
                    }

                    if(i == 0){
                        last_category = category;
                    }

                    if(last_category != category){
                        csr_table.row.add([
                            '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',
                            '',
                            '',
                            '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                        ]).draw(false);

                        category_total_sales = 0;
                    }

                    csr_table.row.add([
                        category,
                        service,
                        csr_data[i].service_count,
                        numberWithCommas(csr_data[i].transactiondetails_totalsales)
                    ]).draw(false);

                    category_total_sales = eval(category_total_sales) + eval(parseFloat(csr_data[i].transactiondetails_totalsales).toFixed(2));

                    total_sales = eval(total_sales) + eval(parseFloat(csr_data[i].transactiondetails_totalsales).toFixed(2));

                    last_category = category;
                }

                if(csr_data.length > 1){

                    csr_table.row.add([
                        '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',
                        '',
                        '',
                        '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                    ]).draw(false);

                }



                if(csr_data.length > 1){

                    csr_table.row.add([

                        '<strong><h3 class="display-5">GRAND TOTAL</h3></strong>',

                        '',

                        '',

                        '<strong><h3 class="display-5">'+numberWithCommas(total_sales)+'</h3></strong>'

                    ]).draw(false);

                }



                $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();



                csr_table.draw();



            }

        })

    }

}


function alphabetically(ascending) {
  return function (a, b) {
    // equal items sort equally
    var a1= a.category_name, b1= b.category_name;
    if (a1 === b1) {
        return 0;
    }
    // nulls sort after anything else
    else if (a1 === null) {
        return 1;
    }
    else if (b1 === null) {
        return -1;
    }
    // otherwise, if we're ascending, lowest sorts first
    else if (ascending) {
        return a1 < b1 ? -1 : 1;
    }
    // if descending, highest sorts first
    else { 
        return a1 < b1 ? 1 : -1;
    }
  };
}
  

  

  

  

  

  