$(document).ready(function() {


   var converted_walkin_table = $('#converted_walkin_table').DataTable({
    "processing": true,
    "dom" : 'Brtip',
    buttons: [{
      extend: 'excel',
    }],
    "ordering": false,
    "bDestroy" : true,
    'columnDefs': [
        { targets: [2, 3, 4, 5, 6], className: "align-center" },

    ],
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      },
    })
    $('.dt-buttons').addClass('pull-right');
    $('.dt-buttons').addClass('position-relative');
    
    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
    $('.dt-button').addClass('btn btn-default');
    $('.select2').select2();
});

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})


function search(){
  // refresh();
  var converted_walkin_table = $('#converted_walkin_table').DataTable();
  var brand_id = $('#brand').val();
  var branch_id = $('#branch').val();
  var month = $('#month').val();
  var year = $('#year').val();
  var user_role = $('#user_role').val();
  var selected_month = $('#month option:selected').text();

  // converted_walkin_table.buttons(0, null).containers().appendTo('#button_here');
    $('.dataTables_processing', $('#converted_walkin_table').closest('.dataTables_wrapper')).show();


  if (!month && user_role === 'Super Admin') {
    converted_walkin_table.clear().draw()
    $.ajax({
      url: "get_converted_walk_in_annual",
      method: "POST",
      data: {brand_id: brand_id, branch_id: branch_id, year: year},
      success:function(data){
        var result = JSON.parse(data);

        // if (branch_id === 'All Branches') {

        //   console.log(result);

        // } else {

          for (let i = 0; i < result['data'].length; i++) {
           diff = result['data'][i].walk_in - result['data'][i].converted;
           percentage = result['data'][i].converted / result['data'][i].walk_in * 100;
            if (isNaN(percentage)) {
              percentage = "0.00"+"%";
            } else {
              percentage = percentage.toFixed(2)+"%";
            }

           // total_walkin = total_walkin + result['data'][i].walk_in;
           // total_converted = total_converted + result['data'][i].converted;
           // total_diff = total_diff + diff;
           // total_percentage = total_converted / total_walkin * 100;
           // total_percentage = total_percentage.toFixed(2)+'%';
           
            converted_walkin_table.row.add([   
              result['data'][i].brand,
              result['data'][i].branch,
              result['data'][i].date,
              result['data'][i].walk_in,
              result['data'][i].converted,
              diff,
              percentage
            ])
            
          }

        // }





        $('.dataTables_processing', $('#converted_walkin_table').closest('.dataTables_wrapper')).hide();
        converted_walkin_table.draw()

      }

    })
  } else if (!brand_id || !branch_id || !year || !month){
    alert("Please select required fields");
    $('.dataTables_processing', $('#converted_walkin_table').closest('.dataTables_wrapper')).hide();
  } else {
        converted_walkin_table.clear().draw()
         $.ajax({
              url: "get_converted_walk_in",
              method: "POST",
              data: {brand_id: brand_id, branch_id: branch_id, month: month, year: year},
              success:function(data){
                var result = JSON.parse(data);
                console.log(result);
                var total_count = 0;
                var total_sales = 0;
                var diff = 0;
                var percentage = 0;
                var total_walkin = 0;
                var total_converted = 0;
                var total_diff = 0;
                var total_percentage = 0;
                if (branch_id !== 'All Branches') {
                   diff = result.walk_in - result.converted;
                   percentage = result.converted / result.walk_in * 100;
                   total_walkin = result.walk_in;
                   total_converted = result.converted;
                   total_diff = diff;
                   total_percentage = percentage.toFixed(2)+'%';
                   percentage = percentage.toFixed(2)+'%';

                    converted_walkin_table.row.add([   
                      result.brand,
                      result.branch,
                      selected_month +' '+year,
                      result.walk_in,
                      result.converted,
                      diff,
                      percentage
                    ])
                } else {
                  for (let i = 0; i < result.length; i++) {
                     diff = result[i].walk_in - result[i].converted;
                     percentage = result[i].converted / result[i].walk_in * 100;
                      if (isNaN(percentage)) {
                        percentage = "0.00"+"%";
                      } else {
                        percentage = percentage.toFixed(2)+"%";
                      }
 
                     total_walkin = total_walkin + result[i].walk_in;
                     total_converted = total_converted + result[i].converted;
                     total_diff = total_diff + diff;
                     total_percentage = total_converted / total_walkin * 100;
                     total_percentage = total_percentage.toFixed(2)+'%';
                      converted_walkin_table.row.add([   
                        result[i].brand,
                        result[i].branch,
                        selected_month +' '+year,
                        result[i].walk_in,
                        result[i].converted,
                        diff,
                        percentage
                      ])

                      
                  }
                }

                $('#total_walkin').html(total_walkin);
                $('#total_converted').html(total_converted);
                $('#total_difference').html(total_diff);
                $('#total_percentage').html(total_percentage);


                $('.dataTables_processing', $('#converted_walkin_table').closest('.dataTables_wrapper')).hide();
                converted_walkin_table.draw()
              }

         })
  }
}

