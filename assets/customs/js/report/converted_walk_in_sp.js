$(document).ready(function() {


   var converted_walk_in_table = $('#converted_walk_in_table').DataTable({
    "processing": true,
    "dom" : 'rt',
    "ordering": false,
    "bDestroy" : true,
    'columnDefs': [
        { targets: [3, 4, 5, 6], className: "align-right" },
     
    ],
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      },
    })
    $('.dt-buttons').addClass('pull-right');
    $('.dt-buttons').addClass('position-fixed');
    
    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
    $('.dt-button').addClass('btn btn-default');
    $('.select2').select2();
});

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id !== '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})



function search(){
  // refresh();
  $('#total_walk_in').html('');
  $('#total_converted').html('');
  var converted_walk_in_table = $('#converted_walk_in_table').DataTable()
  var brand_id = $('#brand').val();
  var branch_id = $('#branch').val();
  var year = $('#year').val();

  var title_brand = $('#brand option:selected').text();

  var title_branch = $('#branch option:selected').text();
  // converted_walk_in_table.buttons(0, null).containers().appendTo('#button_here');
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).show();


  if (!brand_id || !branch_id || !year) {
    alert("Please select required fields");
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
  
  } else if (branch_id === 'All Branches') {
    alert("Not yet implemented. Try searching per branch only")
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
  } else {
    converted_walk_in_table.clear().draw();
    $.ajax({
          url: "get_converted_walk_in_sp",
          method: "POST",
          data:{
            brand_id: brand_id,branch_id: branch_id, year: year
          },
          success:function(data){
            var headcount_list = JSON.parse(data);
            var total_walk_in = 0;
            var total_converted = 0;
            var diff = 0;
            var total_diff = 0;
            var percentage = 0;
            var total_percentage = 0;
            console.log(headcount_list);
           
            for (let i = 0; i < headcount_list.length; i++) {
                diff = eval(headcount_list[i].walk_in) - eval(headcount_list[i].converted)
                percentage = eval(headcount_list[i].converted) / eval(headcount_list[i].walk_in) * 100
                total_walk_in = eval(total_walk_in) + eval(headcount_list[i].walk_in);
                total_converted = eval(total_converted) + eval(headcount_list[i].converted);
                total_diff = eval(total_diff) + eval(diff);
                if (isNaN(percentage)) {
                  percentage = 0.00+"%";
                } else {
                  percentage = percentage.toFixed(2)+"%";
                }
                converted_walk_in_table.row.add([   
                title_brand,
                title_branch,
                headcount_list[i].serviceprovider,
                numberWithCommas(headcount_list[i].walk_in),
                numberWithCommas(headcount_list[i].converted),
                numberWithCommas(diff),
                percentage
              ])
            }

            $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
            converted_walk_in_table.draw()
            $('#total_walk_in').html(total_walk_in);
            $('#total_converted').html(total_converted);
            $('#total_diff').html(total_diff);
            total_percentage = eval(total_converted) / eval(total_walk_in) * 100;
            if (isNaN(total_percentage)) {
              percentage = 0.00+"%";
            } else {
              total_percentage = total_percentage.toFixed(2)+"%";
            }
            $('#total_percentage').html(total_percentage);


          }
    })
  }


}
function refresh(){

  var converted_walk_in_table = $('#converted_walk_in_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,
    "ordering": false,

    'bAutoWidth': false,

    'columnDefs': [
        { targets: [1, 2, 3, 4, 6], className: "align-right" },
    ],
    });
   converted_walk_in_table.clear().draw();
   // converted_walk_in_table.draw();
   $("#table_body").empty();
   // $("#tr_body").empty();

   // $("#converted_walk_in_table").empty();

}


function get_month($index){
  if ($index === 0) {
    return "January";
  }else if ($index == 1) {
    return "February";
  }else if ($index == 2) {
    return "March";
  }else if ($index == 3) {
    return "April";
  }else if ($index == 4) {
    return "May";
  }else if ($index == 5) {
    return "June";
  }else if ($index == 6) {
    return "July";
  }else if ($index == 7) {
    return "August";
  }else if ($index == 8) {
    return "September";
  }else if ($index == 9) {
    return "October";
  }else if ($index == 10) {
    return "November";
  }else if ($index == 11) {
    return "December";
  }
    
}




function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');
}

