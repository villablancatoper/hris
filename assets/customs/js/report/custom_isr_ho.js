$(function(){

  $('.select2').select2();

  $('match-height').matchHeight();

  var custom_isr_table = $('#custom_isr_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false,

    'columnDefs': [
        { targets: [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], className: "align-right" },
    ],
    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    },
    dom: 'Bfrtip',
    buttons: [
      'excel'
    ],
    'ordering': true
  })

  custom_isr_table.order([5, 'DESC'])
  
  custom_isr_table.buttons().container()
      .appendTo( '#custom_isr_table .col-sm-6:eq(0)' );

  $('#datepicker').datepicker({
    'autoclose': true
  });

  $('#daterange_btn').daterangepicker(

    {

      ranges: {

        'Today': [moment(), moment()],

        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days': [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month': [moment().startOf('month'), moment().endOf('month')],

        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate: moment()

    },

    function (start, end) {

      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

      $('#start').val(start.format('MM/DD/YYYY'));

      $('#end').val(end.format('MM/DD/YYYY'));

    }

  )
  
  $('.dt-button').addClass('btn btn-default');
})

$('#brands').change(function(){

    var brand_id = $('#brands').val();

    if (brand_id != '') {
      $.ajax({
          url: "fetch_branches_no_all",
          method: "POST",
          data: {brand_id:brand_id},
          success:function(data){
            $('#branches').html(data);
            // console.log(data);

          }

      })


    }

})



function search_custom_isr_ho(){

  $('.btn-flat').attr('disabled', 'disabled');
    
  $('.dataTables_processing', $('#custom_isr_table').closest('.dataTables_wrapper')).show();

  var custom_isr_table = $('#custom_isr_table').DataTable();



  var brand = $('#brands').select2('val');

  var branch = $('#branches').select2('val');

  var date = $('#datepicker').val();



  if(!branch || !date){

    alert('Please select branch and date');
    
    $('.dataTables_processing', $('#custom_isr_table').closest('.dataTables_wrapper')).hide();

    $('.btn-flat').removeAttr('disabled');

  }

  else{

    custom_isr_table.clear().draw();



    $.ajax({

      url: 'search_custom_isr_ho',

      method: 'GET',

      data: {'brand': brand, 'branch': branch, 'date':date},

      success: function(data){

        var custom_isr = JSON.parse(data);

        console.log(custom_isr)

        if(typeof custom_isr.length == 'undefined'){
          $('.dataTables_processing', $('#custom_isr_table').closest('.dataTables_wrapper')).hide();
          
          custom_isr_table.row.add([

            custom_isr.brand,

            custom_isr.branch,

            custom_isr.serviceprovider_name,

            custom_isr.position_name,

            numberWithCommas(custom_isr.sp_target),

            numberWithCommas(custom_isr.sales_for_the_day),

            numberWithCommas(custom_isr.total_sales),

            numberWithCommas(custom_isr.trending_sales),

            numberWithCommas(custom_isr.todate_otc_sales),

            numberWithCommas(custom_isr.otc_sales_for_the_day),

            numberWithCommas(custom_isr.todate_walkin),

            numberWithCommas(custom_isr.todate_regular),

            numberWithCommas(custom_isr.todate_transfer),            

            numberWithCommas(custom_isr.total_head_count),

            numberWithCommas(custom_isr.todate_tph),
            
            numberWithCommas(custom_isr.total_service)

          ])

          custom_isr_table.order([5, 'desc']).draw()

          $('.btn-flat').removeAttr('disabled');
        
        }
        else{
          $('.dataTables_processing', $('#custom_isr_table').closest('.dataTables_wrapper')).hide();
          for(let i = 0; i < custom_isr.length; i++){
            custom_isr_table.row.add([

              custom_isr[i].brand,

              custom_isr[i].branch,

              custom_isr[i].serviceprovider_name,

              custom_isr[i].position_name,

              numberWithCommas(custom_isr[i].sp_target),

              numberWithCommas(custom_isr[i].sales_for_the_day),

              numberWithCommas(custom_isr[i].total_sales),

              numberWithCommas(custom_isr[i].trending_sales),

              numberWithCommas(custom_isr[i].todate_otc_sales),

              numberWithCommas(custom_isr[i].otc_sales_for_the_day),

              numberWithCommas(custom_isr[i].todate_walkin),

              numberWithCommas(custom_isr[i].todate_regular),

              numberWithCommas(custom_isr[i].todate_transfer),            

              numberWithCommas(custom_isr[i].total_head_count),

              numberWithCommas(custom_isr[i].todate_tph),
              
              numberWithCommas(custom_isr[i].total_service)
    
            ])
          }
          custom_isr_table.order([5, 'desc']).draw()
        }
        $('.btn-flat').removeAttr('disabled');
      }

    })

    // $('#brands').val('').trigger('change');

    // $('#branches').val('').trigger('change');

    // $('#daterange_btn span').html('<i class="fa fa-calendar"></i> Select date');

    // $('#start').val('');

    // $('#end').val('')

  }

}