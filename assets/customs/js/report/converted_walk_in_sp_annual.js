$(document).ready(function() {


   var converted_walk_in_table = $('#converted_walk_in_table').DataTable({
    "processing": true,
    "dom" : 'rt',
    "ordering": false,
    "bDestroy" : true,
    'columnDefs': [
        { targets: [1, 2, 3, 4, 6], className: "align-right" },
     
    ],
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      },
    })
    $('.dt-buttons').addClass('pull-right');
    $('.dt-buttons').addClass('position-fixed');
    
    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
    $('.dt-button').addClass('btn btn-default');
    $('.select2').select2();
});

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})



function search(){
  // refresh();
  var converted_walk_in_table = $('#converted_walk_in_table').DataTable()
  var brand_id = $('#brand').val();
  var branch_id = $('#branch').val();
  var year = $('#year').val();
  // converted_walk_in_table.buttons(0, null).containers().appendTo('#button_here');
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).show();


  if (!brand_id || !branch_id || !year) {
    alert("Please select required fields");
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
  
  } else if(branch_id === 'All Branches'){
    alert("Not yet implemented. Try searching per branch only")
    $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
  } else {

    converted_walk_in_table.clear().draw();
    $.ajax({
          url: "get_converted_walk_in_sp",
          method: "POST",
          data:{
            brand_id: brand_id,branch_id: branch_id, year: year
          },
          success:function(data){
            var headcount_list = JSON.parse(data);
            var total_walk_in = 0;
            var total_diff = 0;
            var total_regular = 0;
            var total_transfer = 0;
            var total_converted = 0;
            var grand_total_head_count = 0;
            var total_turnaway = 0;
            var total_head_count = 0;
            var th_months = '';
            var th_no = '';
            var tbody = '';
            var thead = '';
            var last_sp = '';
            var arr = [];
            var dates = [];
            var row_total_walk_in = 0;
            var row_total_converted = 0;
            var row_total_diff = 0;
            var diff = 0;
            for (let i = 0; i < headcount_list['temp_month'].length; i++) {
                arr.push(headcount_list['temp_month'][i]);
            }

            dates = [... new Set(arr)];
            var data = [];
            // for (let i = 0; i < dates.length; i++) {
            //    // for (let j =0; j < headcount_list.length; j++) {
            //    //    if (headcount_list[j].serviceprovider === dates[i]) {
                    
            //    //    }
            //    // }
            // }
            var temparray = [];
            var chunkSize = 12;
            for (var i=0,len=headcount_list['data'].length; i<len; i+=chunkSize)
              temparray.push(headcount_list['data'].slice(i,i+chunkSize));
         
            console.log(headcount_list);
            console.log(temparray);
         

            $("#converted_walk_in_table").empty();
            for (let i = 0; i < dates.length; i++) {
                const d = new Date(dates[i])
                const n = d.getMonth();
                th_months += "<th colspan = '3' style='text-align: center !important;'>"+get_month(n)+"</th>"
                th_no += "<th style='text-align: center !important; white-space: nowrap !important;'>Walk-in</th><th style='text-align: center !important;'>Converted</th><th style='text-align: center !important;'>Difference</th>"
            }
            thead += "<thead><tr><th colspan='3'></th>"+th_months+"<th colspan = '3' style='text-align: center !important;  white-space: nowrap !important;'>Total</th></tr><tr><th class =>Brand</th><th colspan='1'  style='text-align: center !important;'>Branch</th><th colspan='1'  style='text-align: center !important;'>Serviceprovider</th>"+th_no+"<th style='text-align: center !important;  white-space: nowrap !important;'>Walk-in</th><th style='text-align: center !important;'>Converted</th><th style='text-align: center !important;'>Difference</th></tr></thead>";
            tbody += "<tbody id = 'table_body'>";
            
            for (let i = 0; i < temparray.length; i++) {
              
              row_total_walk_in = 0;
              row_total_converted = 0;
              row_total_diff = 0;
              diff = 0;
              for (let y = 0; y < 12; y++) {
                   diff = eval(temparray[i][y].walk_in) - eval(temparray[i][y].converted);
                   row_total_walk_in = eval(row_total_walk_in) + temparray[i][y].walk_in;
                   row_total_converted = eval(row_total_converted) + temparray[i][y].converted;
                   row_total_diff = eval(row_total_diff) + diff;
                  
                  if (last_sp === temparray[i][y].serviceprovider) {
                     // if (temparray[i][y].walk_in === 0) {
                     //    temparray[i][y].walk_in = '';
                     // }
                     // if (temparray[i][y].converted === 0) {
                     //    temparray[i][y].converted = '';
                     // }
                     // if (diff === 0) {
                     //    diff = '';
                     // }
                     tbody+= "<td>"+ temparray[i][y].walk_in+"</td><td>"+ temparray[i][y].converted+"</td><td>"+diff+"</td>"
                  } else {
                     // if (temparray[i][y].walk_in === 0) {
                     //    temparray[i][y].walk_in = '';
                     // }
                     // if (temparray[i][y].converted === 0) {
                     //    temparray[i][y].converted = '';
                     // }
                     // if (diff === 0) {
                     //    diff = '';
                     // }
                     tbody += "<tr><td style='text-align: center !important; white-space: nowrap !important;'>"+headcount_list['brand']+"</td><td style='text-align: center !important; white-space: nowrap !important;'>"+headcount_list['branch']+"</td><td style='text-align: center !important; white-space: nowrap !important;'>"+temparray[i][y].serviceprovider+"</td><td>"+ temparray[i][y].walk_in+"</td><td>"+ temparray[i][y].converted+"</td><td>"+diff+"</td>"
                     
                  }

                  last_sp =  temparray[i][y].serviceprovider

              }
              // if (row_total_walk_in === 0) {
              //     row_total_walk_in = '';
              // }
              // if (row_total_converted === 0) {
              //     row_total_converted = '';
              // }
              // if (row_total_diff === 0) {
              //     row_total_diff = '';
              // }
              tbody+= "<td>"+row_total_walk_in+"</td><td>"+row_total_converted+"</td><td>"+row_total_diff+"</td>"
              tbody += "</tr>";
              
            }
            


            // tbody += "<td style='text-align: center !important;'>"+total_walk_in+"</td><td style='text-align: center !important;'>"+total_converted+"</td><td style='text-align: center !important;'>"+total_diff+"</td>"
            tbody += "</tr>";
              
            
           
            tbody += "</tbody>";
    
            $("#converted_walk_in_table").append(thead);
    
            $("#converted_walk_in_table").append(tbody);

            $('.dataTables_processing', $('#converted_walk_in_table').closest('.dataTables_wrapper')).hide();
            // converted_walk_in_table.draw()
          
          }
    })
  }


}
function refresh(){

  var converted_walk_in_table = $('#converted_walk_in_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,
    "ordering": false,

    'bAutoWidth': false,

    'columnDefs': [
        { targets: [1, 2, 3, 4, 6], className: "align-right" },
    ],
    });
   converted_walk_in_table.clear().draw();
   // converted_walk_in_table.draw();
   $("#table_body").empty();
   // $("#tr_body").empty();

   // $("#converted_walk_in_table").empty();

}


function get_month($index){
  if ($index == 0) {
    return "January";
  }else if ($index == 1) {
    return "February";
  }else if ($index == 2) {
    return "March";
  }else if ($index == 3) {
    return "April";
  }else if ($index == 4) {
    return "May";
  }else if ($index == 5) {
    return "June";
  }else if ($index == 6) {
    return "July";
  }else if ($index == 7) {
    return "August";
  }else if ($index == 8) {
    return "September";
  }else if ($index == 9) {
    return "October";
  }else if ($index == 10) {
    return "November";
  }else if ($index == 11) {
    return "December";
  }
    
}




function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');
}

