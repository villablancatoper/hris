$(document).ready(function() {

  var variance_table = $('#variance_table').DataTable({
    "dom" : 'Brtip',
    "bDestroy" : true,
    "ordering": false,
    buttons: [{
      extend: 'excel',
      title: 'Variance Analysis | '+$('#start').val() + ' to '+$('#end').val(),
      filename: 'Variance Analysis | '+$('#start').val() + ' to '+$('#end').val(),
    }],
    'columnDefs': [
        { targets: [0,1,2,3,4], className: "align-left" },
        { targets: [5, 6, 7, 8, 9, 10 , 11], className: "align-right" },
        {targets: [1,2,3,4], width: "12%"},
        {targets: [0], width: "11%"},
        {targets: [6], width: "9%"}

    ]
    })
});

$('.select2').select2();
 



function select(){
    var variance_table = $('#variance_table').DataTable({
    "dom" : 'Brtip',
    "bDestroy" : true,
    "processing": true,
    "ordering": false,
    'columnDefs': [
        { targets: [0,1,2,3,4], className: "align-left" },
        { targets: [5, 6, 7, 8, 9, 10 , 11], className: "align-right" },
        {targets: [1,2,3,4], width: "12%"},
        {targets: [0], width: "11%"},
        {targets: [6], width: "9%"}


    ],
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
      },
    })
    $('.dataTables_processing', $('#variance_table').closest('.dataTables_wrapper')).show();
    var branch_id = $('#branch').val();
    var month = $('#month').val();
    var temp = month.split(".");
    var beginning = temp[0];
    var ending = temp[1];
    var month_delivered = $( "#month option:selected" ).text();
    var delivered_start = '';
    var delivered_end = '';

    if (month_delivered == 'January 2019') {
        delivered_start = '2019-01-01';
        delivered_end = '2019-01-31';
    }else if (month_delivered == 'February 2019') {
        delivered_start = '2019-02-01';
        delivered_end = '2019-02-28';
    }else if (month_delivered == 'March 2019') {
        delivered_start = '2019-03-01';
        delivered_end = '2019-03-31';
    }else if (month_delivered == 'April 2019') {
        delivered_start = '2019-04-01';
        delivered_end = '2019-04-30';
    }else if (month_delivered == 'May 2019') {
        delivered_start = '2019-05-01';
        delivered_end = '2019-05-31';
    }else if (month_delivered == 'June 2019') {
        delivered_start = '2019-06-01';
        delivered_end = '2019-06-30';
    }else if (month_delivered == 'July 2019') {
        delivered_start = '2019-07-01';
        delivered_end = '2019-07-31';
    }else if (month_delivered == 'August 2019') {
        delivered_start = '2019-08-01';
        delivered_end = '2019-08-31';
    }else if (month_delivered == 'September 2019') {
        delivered_start = '2019-09-01';
        delivered_end = '2019-09-30';
    }else if (month_delivered == 'October 2019') {
        delivered_start = '2019-10-01';
        delivered_end = '2019-10-31';
    }else if (month_delivered == 'November 2019') {
        delivered_start = '2019-11-01';
        delivered_end = '2019-11-30';
    }else if (month_delivered == 'December 2019') {
        delivered_start = '2019-12-01';
        delivered_end = '2019-12-31';
    }
    // console.log(branch_id)
    // console.log(beginning)
    // console.log(ending)
    // console.log(delivered_start)
    // console.log(delivered_end)
    $.ajax({
        "url": 'variance/get_variance',
        "method": 'POST',
        "data" : {
            branch_id:branch_id,
            beginning:beginning,
            delivered_start:delivered_start,
            delivered_end:delivered_end,
            ending:ending
        },
        success:function(data){
            var variance_items = JSON.parse(data);
            console.log(variance_items);

            variance_table.clear().draw();
            for(let i = 0; i < variance_items['items'].length; i++){
                if (variance_items['items'][i].deliveryreceiptdetail_quantity === '' || variance_items['items'][i].deliveryreceiptdetail_quantity === null) {
                    var actual_usage = eval(variance_items['items'][i].beginning_quantity) - eval(variance_items['items'][i].ending_inventory);
                 
                }else{
                    var actual_usage = eval(variance_items['items'][i].beginning_quantity) + eval(variance_items['items'][i].deliveryreceiptdetail_quantity) - eval(variance_items['items'][i].ending_inventory);

                }
                
                if (variance_items['items'][i].item_model === null || variance_items['items'][i].item_model === "") {
                    var item_name = variance_items['items'][i].item_name
                }else{
                    var item_name = variance_items['items'][i].item_model
                }
               
                var variance = numberWithCommas((eval(actual_usage) - eval(variance_items['items'][i].theoretical)).toFixed(2));

                // for (let j = 0 ; j < variance_items['items'].length; j++) {
                //     variance_items['items'][i].beginning_quantity = numberWithCommas(eval(variance_items['items'][i].beginning_quantity).toFixed(2) / eval(variance_items['items'][i].item_uomsize).toFixed(2))
                //     variance_items['items'][i].deliveryreceiptdetail_quantity = numberWithCommas(eval(variance_items['items'][i].deliveryreceiptdetail_quantity) / eval(variance_items['items'][i].item_uomsize))
                //     variance_items['items'][i].ending_inventory = numberWithCommas(eval(variance_items['items'][i].ending_inventory) / eval(variance_items['items'][i].item_uomsize))
                //     variance_items['items'][i].theoretical = numberWithCommas(eval(variance_items['items'][i].theoretical) / eval(variance_items['items'][i].item_uomsize))
                // }


                
              variance_table.row.add([   
                variance_items['items'][i].category_name,
                item_name,        
                variance_items['items'][i].item_model,   
                variance_items['items'][i].item_color,    
                variance_items['items'][i].item_shade,       
                variance_items['items'][i].item_uom, 
                variance_items['items'][i].beginning_quantity,
                variance_items['items'][i].deliveryreceiptdetail_quantity,
                variance_items['items'][i].ending_inventory,      
                actual_usage.toFixed(2),
                variance_items['items'][i].theoretical,
                variance
              ])
            $('.dataTables_processing', $('#variance_table').closest('.dataTables_wrapper')).hide();
            variance_table.draw()
            }
        }
    });




}