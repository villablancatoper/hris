$(function(){
    
  $('.match-height').matchHeight();

  var date = $('#current_date').val();
  var start = $('#start').val();
  var end = $('#end').val();

  if(start && start != end){
    date = start + ' - ' + end;
  }

  if(start && start == end){
    date = start;
  }

  var dsr_detailed_table = $('#dsr_detailed_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false, 

    'columnDefs': [
        { targets: [8, 9, 11], className: "align-right" },
    ],
    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    },
    dom: 'Bfrtip',
    buttons: [{
      extend: 'excel',
      title: $('#user_branch_name').text()+' | Detailed Sales Report | '+$('#current_date').val(),
      filename: $('#user_branch_name').text()+' (Detailed Sales Report)'
    }],
    'ordering': true
  })

  dsr_detailed_table.buttons().container()
        .appendTo( '#dsr_detailed_table .col-sm-6:eq(0)' );
  
  $('#daterange_btn').daterangepicker(

    {

      ranges: {

        'Today': [moment(), moment()],

        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days': [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month': [moment().startOf('month'), moment().endOf('month')],

        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate: moment()

    },

    function (start, end) {

      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

      $('#start').val(start.format('MM/DD/YYYY'));

      $('#end').val(end.format('MM/DD/YYYY'));

    }

  )

  $('.dt-button').addClass('btn btn-default');

  $('#dsr_detailed_table tbody').on('dblclick', 'tr', function () {

    $(this).attr('style', 'background-color: #cce8ff !important; color: black;').siblings().removeAttr("style");
    
    window.location.href = $('#base_url').val()+'joborder/entry/'+$(this).find('td:eq(0)').text()+'/'+$(this).find('td:eq(1)').text();

  });

  $('#dsr_detailed_table tbody').on('click', 'tr', function () {

    $(this).attr('style', 'background-color: #cce8ff !important; color: black;').siblings().removeAttr("style");

  });

  // dsr_detailed_table.columns(0).visible(false);
  // dsr_detailed_table.columns(1).visible(false);

})

function search_dsrs_detailed_branch(){

    var test = $('#dsr_detailed_table').DataTable()

    test.destroy();

    var date = $('#current_date').val();
    var start = $('#start').val();
    var end = $('#end').val();

    if(start && start != end){
      date = start + ' to ' + end;
    }

    if(start && start == end){
      date = start;
    }

    var dsr_detailed_table = $('#dsr_detailed_table').DataTable({

      'processing': true,

      'bAutoWidth': false,

      'bSort': false, 

      'columnDefs': [
          { targets: [8, 9, 11], className: "align-right" },
      ],
      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      dom: 'Bfrtip',
      buttons: [{
        extend: 'excel',
        title: $('#user_branch_name').text()+' | Detailed Sales Report | '+date,
        filename: $('#user_branch_name').text()+' (Detailed Sales Report)'
      }],
      'ordering': true
    })

    $('.dt-button').addClass('btn btn-default');

    $('.dataTables_processing', $('#dsr_detailed_table').closest('.dataTables_wrapper')).show();
    
    dsr_detailed_table.clear().draw();
    
    if(!start && !end){
        alert('Select date to search');
    }
    else{
        $.ajax({
            url: 'search_dsrs_detailed_branch',
            method: 'GET',
            data: {'start': start, 'end': end},
            success: function(data){
                
                var dsrs_detailed = JSON.parse(data);
                console.log(dsrs_detailed)
                for(let i = 0; i < dsrs_detailed.length; i++){
                    
                    dsr_detailed_table.row.add([
                        dsrs_detailed[i].customer_id,
                        dsrs_detailed[i].transaction_id,
                        dsrs_detailed[i].transaction_date,
                        dsrs_detailed[i].customer_firstname + ' ' + dsrs_detailed[i].customer_lastname,
                        dsrs_detailed[i].transaction_customerstatus,
                        dsrs_detailed[i].serviceprovider_name,
                        dsrs_detailed[i].assister_name,
                        dsrs_detailed[i].transaction_docketno,
                        dsrs_detailed[i].transaction_orno,
                        dsrs_detailed[i].productservice_description,
                        dsrs_detailed[i].productretail_name,
                        numberWithCommas(dsrs_detailed[i].transactiondetails_totalsales)
                    ]).draw(false);
                    
                }
                $('.dataTables_processing', $('#dsr_detailed_table').closest('.dataTables_wrapper')).hide();
                // dsr_detailed_table.columns(0).visible(false);
                // dsr_detailed_table.columns(1).visible(false);
                $('#time_info').removeClass('hide')
                $('#time_info').addClass('shown')
                $('#compute_summary').removeClass('hide')
                $('#compute_summary').addClass('shown')
            }
        })
        

    }
}

function compute_summary(){
    var start = $('#start').val();
    var end = $('#end').val();
      $('#loading_modal').modal('show');
    
  if (!start || !end) {
    alert("Please select date")
      $('#loading_modal').modal('hide');

  }else{
              $.ajax({
                      url: 'get_dsrs_detailed_branch',
                      method: 'GET',
                      data: {'start': start, 'end': end},
                      success: function(data){
                      var dsrs_detailed_total = JSON.parse(data);
                      console.log(dsrs_detailed_total)
                      var total_sales = (eval(dsrs_detailed_total.total_sales.total_sales) + 0.00).toFixed(2);
                      var total_cash = (eval(dsrs_detailed_total.total_cash.total_cash) + 0.00).toFixed(2);
                      var total_card = (eval(dsrs_detailed_total.total_card.total_card) + 0.00).toFixed(2);
                      var total_gc = (eval(dsrs_detailed_total.total_gc.total_gc) + 0.00).toFixed(2);
                      var total_sales_services = (eval(dsrs_detailed_total.total_sales_services) + 0.00).toFixed(2);
                      var total_sales_otc = (eval(dsrs_detailed_total.total_sales_otc.total_sales_otc) + 0.00).toFixed(2);

                      var todate_total_sales = (eval(dsrs_detailed_total.todate_total_sales.todate_total_sales) + 0.00).toFixed(2);
                      var todate_tph = (eval(dsrs_detailed_total.todate_tph) + 0.00).toFixed(2);
                      var todate_otc = (eval(dsrs_detailed_total.todate_otc.todate_otc) + 0.00).toFixed(2);
                      var todate_services = (eval(dsrs_detailed_total.todate_services) + 0.00).toFixed(2);
                      
                      $('#total_sales').text(numberWithCommas(numberWithCommas(total_sales)));
                      $('#total_cash').text(numberWithCommas(total_cash));
                      $('#total_card').text(numberWithCommas(total_card));
                      $('#total_gc').text(numberWithCommas((eval(total_gc)+0.00).toFixed(2)));
                      $('#total_sales_otc').text(numberWithCommas(total_sales_otc));
                      $('#total_sales_services').text(numberWithCommas(total_sales_services));
                      $('#total_turnaway').html(numberWithCommas(eval(dsrs_detailed_total.total_turnaway.total_turnaway) + 0));
                      $('#total_regular').html(numberWithCommas(eval(dsrs_detailed_total.total_regular) + 0));
                      $('#total_walkin').html(numberWithCommas(eval(dsrs_detailed_total.total_walkin) + 0));
                      $('#total_transfer').html(numberWithCommas(eval(dsrs_detailed_total.total_transfer) + 0));
                      $('#total_head_count').html(numberWithCommas(eval(dsrs_detailed_total.total_head_count) + 0));

                      $('#todate_total_sales').text(numberWithCommas(numberWithCommas(todate_total_sales)));
                      $('#todate_tph').text(numberWithCommas(numberWithCommas(todate_tph)));
                      $('#todate_otc').text(numberWithCommas(numberWithCommas(todate_otc)));
                      $('#todate_services').text(numberWithCommas(numberWithCommas(todate_services)));

                       $('#loading_modal').modal('hide');
                      
                  }
              })
  }

}





