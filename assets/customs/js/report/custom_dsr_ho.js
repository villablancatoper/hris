$(function(){

    $('.select2').select2();
  
    $('match-height').matchHeight();
  
    var custom_dsr_table = $('#custom_dsr_table').DataTable({
  
      'processing': true,
  
      'bAutoWidth': false,
  
      'bSort': false,
  
      'columnDefs': [
          { targets: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], className: "align-right" },
      ],
      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      dom: 'Bfrtip',
      buttons: [
        'csv', 'excel', 'print'
      ],
      'ordering': true
    })
    
    custom_dsr_table.buttons().container()
        .appendTo( '#custom_dsr_table .col-sm-6:eq(0)' );

    $('#datepicker').datepicker({
      'autoclose': true
    });
  
    $('#daterange_btn').daterangepicker(
  
      {
  
        ranges: {
  
          'Today': [moment(), moment()],
  
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
  
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
  
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
  
          'This Month': [moment().startOf('month'), moment().endOf('month')],
  
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  
        },
  
        startDate: moment().subtract(29, 'days'),
  
        endDate: moment()
  
      },
  
      function (start, end) {
  
        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
  
        $('#start').val(start.format('MM/DD/YYYY'));
  
        $('#end').val(end.format('MM/DD/YYYY'));
  
      }
  
    )
    
    $('.dt-button').addClass('btn btn-default');
})
  

  $('#brands').change(function(){

    var brand_id = $('#brands').val();

    if (brand_id != '') {

      $.ajax({
          url: "fetch_branches_no_all",

          method: "POST",
          data: {brand_id:brand_id},

          success:function(data){
            $('#branches').html(data);

          }

      })


    }

})
  
  
  function search_custom_dsr_ho(){

    $('.btn-flat').attr('disabled', 'disabled');
      
    $('.dataTables_processing', $('#custom_dsr_table').closest('.dataTables_wrapper')).show();
  
    var custom_dsr_table = $('#custom_dsr_table').DataTable();
  
  
  
    var brand = $('#brands').select2('val');
  
    var branch = $('#branches').select2('val');
  
    var date = $('#datepicker').val();


  
    if(!branch || !date ){
  
      alert('Please select branch and date.')
      
      $('.dataTables_processing', $('#custom_dsr_table').closest('.dataTables_wrapper')).hide();

      $('.btn-flat').removeAttr('disabled');
  
    }
  
    else{
  
      custom_dsr_table.clear().draw();
  
  
  
      $.ajax({
  
        url: 'search_custom_dsr_ho',
  
        method: 'GET',
  
        data: {'brand': brand, 'branch': branch, 'date':date},
  
        success: function(data){
  
          var custom_dsr = JSON.parse(data);

          if(typeof custom_dsr.length == 'undefined'){
            $('.dataTables_processing', $('#custom_dsr_table').closest('.dataTables_wrapper')).hide();
            
            custom_dsr_table.row.add([
  
              custom_dsr.brand,
  
              custom_dsr.branch,
  
              numberWithCommas(custom_dsr.revenue_target),

              numberWithCommas(custom_dsr.sales_for_the_day),

              numberWithCommas(custom_dsr.total_cash),

              numberWithCommas(custom_dsr.total_card),

              numberWithCommas(custom_dsr.total_gc),              
  
              numberWithCommas(custom_dsr.total_sales),
              
              numberWithCommas(custom_dsr.trending_sales),

              numberWithCommas(custom_dsr.percentage1)+'%',

              numberWithCommas(custom_dsr.retail_target_sales),

              numberWithCommas(custom_dsr.retail_sales_for_the_day),

              numberWithCommas(custom_dsr.retail_total_sales),

              numberWithCommas(custom_dsr.percentage2)+'%',

              numberWithCommas(custom_dsr.todate_walkin),

              numberWithCommas(custom_dsr.todate_regular),

              numberWithCommas(custom_dsr.todate_transfer),
  
              numberWithCommas(custom_dsr.total_head_count),
  
              numberWithCommas(custom_dsr.total_turnaway),

              numberWithCommas(custom_dsr.todate_tph),

              numberWithCommas(custom_dsr.todate_lost_sales)
  
            ])

            custom_dsr_table.order([1, 'desc']).draw()
            
            $('.btn-flat').removeAttr('disabled');
          
          }
          else{
            $('.dataTables_processing', $('#custom_dsr_table').closest('.dataTables_wrapper')).hide();
            for(let i = 0; i < custom_dsr.length; i++){
              custom_dsr_table.row.add([
  
                custom_dsr[i].brand,
    
                custom_dsr[i].branch,
    
                numberWithCommas(custom_dsr[i].revenue_target),
  
                numberWithCommas(custom_dsr[i].sales_for_the_day),
  
                numberWithCommas(custom_dsr[i].total_cash),
  
                numberWithCommas(custom_dsr[i].total_card),
  
                numberWithCommas(custom_dsr[i].total_gc),              
    
                numberWithCommas(custom_dsr[i].total_sales),
                
                numberWithCommas(custom_dsr[i].trending_sales),
  
                numberWithCommas(custom_dsr[i].percentage1)+'%',
  
                numberWithCommas(custom_dsr[i].retail_target_sales),
  
                numberWithCommas(custom_dsr[i].retail_sales_for_the_day),
  
                numberWithCommas(custom_dsr[i].retail_total_sales),
  
                numberWithCommas(custom_dsr[i].percentage2)+'%',
  
                numberWithCommas(custom_dsr[i].todate_walkin),
  
                numberWithCommas(custom_dsr[i].todate_regular),
  
                numberWithCommas(custom_dsr[i].todate_transfer),
    
                numberWithCommas(custom_dsr[i].total_head_count),
    
                numberWithCommas(custom_dsr[i].total_turnaway),
  
                numberWithCommas(custom_dsr[i].todate_tph),
  
                numberWithCommas(custom_dsr[i].todate_lost_sales)
    
              ])
  
              custom_dsr_table.order([1, 'desc']).draw()

              $('.btn-flat').removeAttr('disabled');
            }
          }
          $('.btn-flat').removeAttr('disabled');
        }
  
      })
  
      // $('#brands').val('').trigger('change');
  
      // $('#branches').val('').trigger('change');
  
      // $('#daterange_btn span').html('<i class="fa fa-calendar"></i> Select date');
  
      // $('#start').val('');
  
      // $('#end').val('')
  
    }
  
  }