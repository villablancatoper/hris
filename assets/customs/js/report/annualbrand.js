$(document).ready(function() {



  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,
     "processing": true,

    buttons: [{

      extend: 'excel',footer: true,

      title: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      filename: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

    }],
 'columnDefs': [

        { targets: [1,2,3,4,5,6,7,8,9,10,11,12,13], className: "align-right" },
         { "width": "6%", "targets": [1,2,3,4,5,6,7,8,9,10,11,12,13] },
         { "width": "9%", "targets": [0] },
         {"targets": [0],"orderable": false},
          {"targets": [1,2,3,4,5,6,7,8,9,10,11,12,13],"orderable": true},
    
    ]

    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});

// end of function 

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})



$('#daterange_btn').daterangepicker({

        ranges: {

          

          'This Month': [moment().startOf('month'), moment().endOf('month')]

        },

       alwaysShowCalendars:false,
        startDate: moment().subtract(29, 'days'),

        endDate: moment(),
        alwaysShowCalendars:false,
        showCustomRangeLabel:false,
        autoUpdateInput:true,
        autoApply:true

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

// end of function 



function compute(){
    refresh();
    var totalsales_table = $('#totalsales_table').DataTable({

      "dom" : 'Brtip',

      "aaData": null,

      "bPaginate": false,

      "bDestroy" : true,
        "ordering" :true,
 "processing": true,
      buttons: [{

        extend: 'excel',footer: true,

        title: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

        filename: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      }],

     'columnDefs': [

        { targets: [1,2,3,4,5,6,7,8,9,10,11,12,13], className: "align-right" },
         { "width": "6%", "targets": [1,2,3,4,5,6,7,8,9,10,11,12,13] },
         { "width": "9%", "targets": [0] },
          {"targets": [0],"orderable": false},
          {"targets": [1,2,3,4,5,6,7,8,9,10,11,12,13],"orderable": true},
    ]



      

    })

    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');


    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();

    var start = $('#month_and_year').val();
     var year = $('#month_and_year').val();

    var end = getendmonth($('#month_and_year').val());

    // alert(start)

    // alert(end)

    // alert(brand_id)

    // alert(branch)

    // var branch_name = $('#branch_name').val();

    if (!start || !end ) {

       alert("Selection of Month and Year is required")

       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

        totalsales_table.clear().draw()

    }
    else 

      {
         $.ajax({

          url: "annualbrand/compute",

          method: "POST",

          data: {start: start, end: end, year:year},

          success: function(data){

            var daily_sales = JSON.parse(data);
            var totaljan=0,totalfeb=0,totalmar=0,totalapr=0,toralmay=0,totaljun=0,totaljul=0,totalaug=0,totalsep=0,totaloct=0,totalnov=0,totaldec=0;
            var totalsalesjan=0,totalsalesfeb=0,totalsalesmar=0,totalsalesapr=0,toralsalesmay=0,totalsalesjun=0,totalsalesjul=0,totalsalesaug=0,totalsalessep=0,totalsalesoct=0,totalsalesnov=0,totalsalesdec=0;
            var brand="";
            var total="<b><td>TOTAL</td></b>";
            var totalbrandsales=0;
            var count=-1;
            var totalss=[];
            console.log(daily_sales)        
            


               for (let i = 0; i < daily_sales['list'].length; i++) {

                  totalbrandsales+=parseFloat(daily_sales['list'][i]['sales']);
                  
                  if (i % 12 === 0 && i !== 0)
                  {
                    totalsales_table.row.add([

                            daily_sales['list'][i-1 ]['brand'],
                            numberWithCommas( daily_sales['list'][i-12]['sales']),  
                            numberWithCommas( daily_sales['list'][i-11]['sales']),  
                            numberWithCommas( daily_sales['list'][i-10]['sales']),  
                            numberWithCommas( daily_sales['list'][i-9]['sales']),  
                            numberWithCommas( daily_sales['list'][i-8]['sales']),  
                            numberWithCommas( daily_sales['list'][i-7]['sales']),  
                            numberWithCommas( daily_sales['list'][i-6]['sales']),  
                            numberWithCommas( daily_sales['list'][i-5]['sales']),  
                            numberWithCommas( daily_sales['list'][i-4]['sales']),  
                            numberWithCommas( daily_sales['list'][i-3]['sales']),  
                            numberWithCommas( daily_sales['list'][i-2]['sales']), 
                            numberWithCommas( daily_sales['list'][i-1]['sales']),                            
                            numberWithCommas(totalbrandsales.toFixed(2))

                      ])
                    totalbrandsales=0;
                    totalsalesjan+=parseFloat(daily_sales['list'][i-12]['sales']);
                    totalsalesfeb+=parseFloat(daily_sales['list'][i-11]['sales']);
                    totalsalesmar+=parseFloat( daily_sales['list'][i-10]['sales']);
                    totalsalesapr+=parseFloat( daily_sales['list'][i-9]['sales']);
                    toralsalesmay+=parseFloat( daily_sales['list'][i-8]['sales']);
                    totalsalesjun+=parseFloat( daily_sales['list'][i-7]['sales']);
                    totalsalesjul+=parseFloat( daily_sales['list'][i-6]['sales']);
                    totalsalesaug+=parseFloat( daily_sales['list'][i-5]['sales']);
                    totalsalessep+=parseFloat( daily_sales['list'][i-4]['sales']);
                    totalsalesoct+=parseFloat( daily_sales['list'][i-3]['sales']);
                    totalsalesnov+=parseFloat( daily_sales['list'][i-2]['sales']);
                    totalsalesdec+=parseFloat( daily_sales['list'][i-1]['sales']);


                  }
                  var totalannualsales=(parseFloat(totalsalesjan)+parseFloat(totalsalesfeb)+parseFloat(totalsalesmar)+parseFloat(totalsalesapr)+parseFloat(toralsalesmay)+parseFloat(totalsalesjun)+parseFloat(totalsalesjul)+parseFloat(totalsalesaug)+parseFloat(totalsalessep)+parseFloat(totalsalesoct)+parseFloat(totalsalesnov)+parseFloat(totalsalesdec)).toFixed(2);
                  $('#jan').text((numberWithCommas(parseFloat(totalsalesjan).toFixed(2))).toString());
                  $('#feb').text((numberWithCommas(parseFloat(totalsalesfeb).toFixed(2))).toString());
                   $('#mar').text((numberWithCommas(parseFloat(totalsalesmar).toFixed(2))).toString());
                   $('#apr').text((numberWithCommas(parseFloat(totalsalesapr).toFixed(2))).toString());
                   $('#may').text((numberWithCommas(parseFloat(toralsalesmay).toFixed(2))).toString());
                   $('#jun').text((numberWithCommas(parseFloat(totalsalesjun).toFixed(2))).toString());
                   $('#jul').text((numberWithCommas(parseFloat(totalsalesjul).toFixed(2))).toString());
                  $('#aug').text((numberWithCommas(parseFloat(totalsalesaug).toFixed(2))).toString());
                  $('#sep').text((numberWithCommas(parseFloat(totalsalessep).toFixed(2))).toString());
                  $('#oct').text((numberWithCommas(parseFloat(totalsalesoct).toFixed(2))).toString());
                  $('#nov').text((numberWithCommas(parseFloat(totalsalesnov).toFixed(2))).toString());
                  $('#dec').text((numberWithCommas(parseFloat(totalsalesdec).toFixed(2))).toString());
                  $('#tot').text((numberWithCommas(parseFloat(totalannualsales).toFixed(2))).toString());

                  


                

                  

                 }
              
              

         
        
            
             $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();


            totalsales_table.draw()



          }



      })



      }






      



}// end of function 
function thismonth(x)
{

var today = new Date();
  let d = new Date(today);
  d.setMonth(today.getMonth() +1)
  d.setDate(0);
  d.setHours(23);
  d.setMinutes(59);
  d.setSeconds(59);
let start_date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" +01

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let selected_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" +01
  
  
  if (start_date==selected_date)
  {
    return true;
  }
  else if(start_date>selected_date)
  {
      return false;

  }

}
function storetargetsum(x)
{
var sum=sum+x;
return sum;

}
function getendmonth(x)
{

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let formatted_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate()
  return formatted_date;

}
function monthlytrendingpercent(x,y)
{
  var monthlypercent=(x/y)*100;

  
  return Number(monthlypercent).toFixed(2).concat("%");


}
function trendingpercent(x,y)
{
  var trendingpercent=(y/x)*100;

  
  return Number(trendingpercent).toFixed(2).concat("%");


}
function trending(x)
{
  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/daytodayint)*endday ;
  return trending.toFixed(2);


}
function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}// end of function



function refresh(){

  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    "buttons" : ['pdf'],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })



   totalsales_table.clear();

   totalsales_table.draw();

  
   $('#jan').text("");
   $('#feb').text("");
   $('#mar').text("");
   $('#apr').text("");
   $('#may').text("");
   $('#jun').text("");
   $('#jul').text("");
   $('#aug').text("");
   $('#sep').text("");
   $('#oct').text("");
   $('#nov').text("");
   $('#dec').text("");
   $('#tot').text("");

   $('#storetarget_payment').text("");
$('#totalsales_payment').text("");
   $('#total_cash').text("");

   $('#total_card').text("");

   $('#total_gc').text("");
$('#total_cards').text("");
}



