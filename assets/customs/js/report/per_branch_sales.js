$(function() {
    $('#totalsales_table').DataTable({
          'processing': true,
      'dom' : 'Brtip',
      'bDestroy' : true,
      'aaData': null,
      buttons: [{
        extend: 'excel',
        title: 'Brand Sales Report',
        filename: 'Brand Sales Report',
          }],
          'language': {
  
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      'columnDefs': [
              { targets: [1,2,3,4,5], className: "align-right" },
      ]
    })
  
    $('.select2').select2();
    $('.dt-buttons').addClass('pull-right');
    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
    $('.dt-button').addClass('btn btn-default');
  });
  
  function get_per_branch_sales(){
      
      $('#totalsales_table').DataTable().destroy();
      
      let totalsales_table = $('#totalsales_table').DataTable({
          'processing': true,
          'dom' : 'Brtip',
          'aaData': null,
          'bPaginate': false,
          'bDestroy' : true,
          'ordering' :false,
          buttons: [{
              extend: 'excel',
              title: 'Brand Sales Report | '+$('#month_and_year option:selected').text(),
              filename: 'Brand Sales Report | '+$('#month_and_year option:selected').text(),
          }],
          'columnDefs': [
              { targets: [1,2,3,4,5], className: 'align-right' },
          ],
          'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      },
    });
  
      $('.dt-buttons').addClass('pull-right');
      $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
      $('.dt-button').addClass('btn btn-default');
  
      $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();
  
      $('#totalsales_table').DataTable().clear().draw();
  
      var start = $('#month_and_year').val();
  
      if (!start) {
  
          totalsales_table.clear().draw();
          $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();
  
          alert("Selection of Month and Year is required");
      }
      else{
  
          $.ajax({
              url: "get_per_branch_sales",
              method: "GET",
              data: {start: start, 'branch_id': $('#branches').val()},
              success: function(data){
                  var daily_sales = JSON.parse(data);
                  var total_storetarget_original = 0, total_transaction_totalsales = 0;
                  var total = "<b><td>TOTAL</td></b>";    
                  var total_sales_ly = 0;
  
                  for(let i = 0; i < daily_sales.length; i++){
                      let mtd = get_mtd_percentage(daily_sales[i].transaction_totalsales, daily_sales[i].storetarget_original);
                      let sales_ly = 0;
  
                      totalsales_table.row.add([
                          daily_sales[i].branch_name,        
                          sales_ly,                     
                          numberWithCommas(parseFloat(daily_sales[i].transaction_totalsales).toFixed(2)),
                          numberWithCommas(parseFloat(daily_sales[i].storetarget_original).toFixed(2)),
                          mtd,
                          get_growth_percentage(daily_sales[i].transaction_totalsales, sales_ly),
                      ]);
  
                      total_storetarget_original = parseFloat(total_storetarget_original) + parseFloat(daily_sales[i].storetarget_original);
                      total_transaction_totalsales = parseFloat(total_transaction_totalsales) + parseFloat(daily_sales[i].transaction_totalsales); 
                      total_sales_ly = parseFloat(total_sales_ly) + parseFloat(sales_ly);
                  }
  
                  totalsales_table.row.add([
                      total,                             
                      total_sales_ly,
                      numberWithCommas(parseFloat(total_transaction_totalsales).toFixed(2)),
                      numberWithCommas(parseFloat(total_storetarget_original).toFixed(2)),
                      get_mtd_percentage(total_transaction_totalsales, total_storetarget_original),
                      get_growth_percentage((total_transaction_totalsales, total_sales_ly))
                  ])
                  
                  totalsales_table.draw();
                  $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();
              }
          });
      }
  }
  
  function get_mtd_percentage(total_sales, target){
      return (safeDivision(parseFloat(total_sales), parseFloat(target)) * 100).toFixed(2).concat("%");
  }
  
  function get_growth_percentage(sales_ly, total_sales){
      return (safeDivision(parseFloat(sales_ly), parseFloat(total_sales) * 100)).toFixed(2).concat("%");
  }
  
  function safeDivision(first, second){
      if(!first || first == 0 || !second || second == 0){
          return 0;
      }
  
      return parseFloat(first) / parseFloat(second);
  }
  