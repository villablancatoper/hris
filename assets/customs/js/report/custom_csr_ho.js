$(function(){

    

    $('.match-height').matchHeight();

  

    var csr_table = $('#csr_table').DataTable({

  

      'processing': true,

  

      'bAutoWidth': false,

  

      'bSort': false, 

  

      'columnDefs': [

          { targets: [3, 4], className: "align-right" },

      ],

      'language': {

  

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    

      },

      dom: 'Bfrtip',

      buttons: [{

        extend: 'excel',

        title: 'Category Sales Report | '+$('#current_date').val(),

        filename: $('#user_branch_name').text()+' (Category Sales Report)'

      }],

      'ordering': false,



      "lengthChange": false,

  

      "paging": false,

      

      "bInfo" : false

    })

  

    csr_table.buttons().container()

          .appendTo( '#csr_table .col-sm-6:eq(0)' );

    

    $('#daterange_btn').daterangepicker(

  

      {

  

        ranges: {

  

          'Today': [moment(), moment()],

  

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

  

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

  

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

  

          'This Month': [moment().startOf('month'), moment().endOf('month')],

  

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

  

        },

  

        startDate: moment().subtract(29, 'days'),

  

        endDate: moment()

  

      },

  

      function (start, end) {

  

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

  

        $('#start').val(start.format('MM/DD/YYYY'));

  

        $('#end').val(end.format('MM/DD/YYYY'));

  

      }

  

    )

  

    $('.dt-button').addClass('btn btn-default');



    $('.select2').select2();



    $('#branches').next("span").hide();



    $('#categories').next("span").hide();



    $('#productservices').next("span").hide();

  


    var user_id = $('#user_id').val();
    var user_position = $('#user_position').val();

    if (user_position === 'Area Manager') {
        $.ajax({
          url: "fetch_asm_brands",
          method: "POST",
          data: {user_id: user_id},
          success:function(data){
            $('#brands').html(data);
          }
        })
    }

})

if (user_position !== 'Area Manager') {


    $('#brands').change(function(){
        var brand_id = $('#brands').val();
        if (brand_id != '') {
          $.ajax({
              url: "fetch_branches",
              method: "POST",
              data: {brand_id:brand_id},
              success:function(data){
                $('#branches').html(data);
                     $.ajax({
                      url: "fetch_category_with_all",
                      method: "POST",
                      data: {brand_id:brand_id},
                      success:function(data){
                        $('#categories').html(data);


                      }
                     })
              }
          })

        }
    })
}

$('#categories').change(function(){
      var category_id = $('#categories').val();
      // alert(category_id)
      if (category_id != 'All Categories') {
        $('#productservices').prop('disabled', false);
        $.ajax({

            url: "fetch_services_with_all",

            method: "POST",

            data: {category_id:category_id},

            success:function(data){
              $('#productservices').html(data);
            }
        })
      } else {
        $('#productservices').html('');
        $('#productservices').prop('disabled', 'disabled');
      }
  })



// $('#brands').on("change", function(e) { 



//     var brand_id = $("#brands option:selected").val();


//     $.ajax({

//         url: 'get_categories_ho',

//         method: 'GET',

//         data: {'brand_id': brand_id},

//         success: function(data){

//             var categories = JSON.parse(data);
//             // console.log(categories)


//             $('#categories').empty();



//             if(categories.length > 0){

//                 var default_option = new Option('Select category', '', true, true);

//                 var all_categories = new Option('ALL CATEGORIES', 'all_categories', false, false);



//                 $("#categories").append(default_option).trigger('change');

//                 $("#categories").append(all_categories)

//             }



//             for(let i = 0; i < categories.length; i++){

//                 var option = new Option(categories[i].category_name, categories[i].category_code, false, false);

//                 $("#categories").append(option);

//             }



//             $('#categories').next("span").show();



//             $.ajax({

//                 url: 'get_productservices_ho',

//                 method: 'GET',

//                 data: {'brand_id': brand_id},

//                 success: function(data){

//                     var productservices = JSON.parse(data);

        

//                     $('#productservices').empty();

        

//                     if(productservices.length > 0){

//                         var default_option = new Option('Select service', '', true, true);

//                         var all_services = new Option('ALL SERVICES', 'all_services', false, false);

        

//                         $("#productservices").append(default_option).trigger('change');

//                         $("#productservices").append(all_services)

//                     }

        

//                     for(let i = 0; i < productservices.length; i++){

//                         var option = new Option(productservices[i].productservice_description, productservices[i].productservice_code, false, false);

//                         $("#productservices").append(option);

//                     }

        

//                     $('#productservices').next("span").show();



//                     $.ajax({

//                         url: 'get_branches_ho',

//                         method: 'GET',

//                         data: {'brand_id': brand_id},

//                         success: function(data){

//                             var branches = JSON.parse(data);

                

//                             $('#branches').empty();

                

//                             if(branches.length > 0){

//                                 var default_option = new Option('Select branch', '', true, true);

//                                 var all_branches = new Option('ALL BRANCHES', 'all_branches', false, false);

                

//                                 $("#branches").append(default_option).trigger('change');

//                                 if($('#user_branch_id').val() != 200){
                                    
//                                     $("#branches").append(all_branches);

//                                 }

//                             }

                

//                             for(let i = 0; i < branches.length; i++){

//                                 var option = new Option(branches[i].branch_name, branches[i].branch_id, false, false);

//                                 $("#branches").append(option);

//                             }

                

//                             $('#branches').next("span").show();

//                         }

//                     });

//                 }

//             });

//         }

//     })



// });

  

function search_custom_csr_ho(){

  

    var test = $('#csr_table').DataTable()



    test.destroy();



    var date;

    var category_code = $('#categories').val();

    var branch = $('#branches').select2('data');

    var productservice_code = $('#productservices').val();

    var branch_id = $('#branches').val();

    var brand_id = $('#brands').val();

    console.log(category_code)
    console.log(productservice_code)
    console.log(branch_id)
    console.log(brand_id)


    var start = $('#start').val();

    var end = $('#end').val();



    if(start && start != end){

        date = start + ' to ' + end;

    }



    if(start && start == end){

        date = start;

    }



    // console.log(branch)



    var csr_table = $('#csr_table').DataTable({



        'processing': true,



        'bAutoWidth': false,



        'bSort': false, 



        'columnDefs': [

            { targets: [3, 4], className: "align-right" },

        ],

        'language': {



            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

        

        },

        dom: 'Bfrtip',

        buttons: [{

            extend: 'excel',

            title: branch[0].text+' | Category Sales Report | '+date,

            filename: branch[0].text+' (Category Sales Report)'

        }],

        

        'ordering': false,



        "lengthChange": false,

    

        "paging": false,

        

        "bInfo" : false

    })



    $('.dt-button').addClass('btn btn-default');
    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).show();
    csr_table.clear().draw();
    if(!start || !end || (!category_code && !productservice_code) || !branch_id){
        var error = '';
        if(!start || !end){
            error += 'Date is required.\n';
        }
        // if(!category_code && !productservice_code){
        //     error += 'Either select a CATEGORY with a DATE, or select a SERVICE with a DATE.\n';
        // }
        // if(category_code && productservice_code){
        //     error += 'Either select a CATEGORY with a DATE, or select a SERVICE with a DATE.\n';
        // }
        if(!branch_id){
            error += 'Branch is required.\n';
        }
        alert(error);
        $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();

    }

    else{
        if(branch_id == 'All Branches'){
            $.ajax({

                url: 'search_custom_csr_ho',

                method: 'GET',

                data: {'start': start, 'end': end, 'category_code': category_code, 'productservice_code': productservice_code, 'branch_id': branch_id, 'brand_id': brand_id},

                success: function(data){
                    var csr_data = JSON.parse(data);
                    console.log(csr_data)
                    var data = [];
                    data = data.concat(...csr_data)
                    data.sort(alphabetically(true));
                    console.log(data)
                    var new_data = [];
                    sortObjArray(data, "productservice_description");
                    new_data = removeDuplicatesFromObjArray(data, "productservice_description");
                    console.log(new_data)
                    var total_sales = 0;
                    var category_total_sales = 0;
                    var category_total_qty = 0;
                    var last_category = '';
                    var counter = 0;
                    // data.category_name
                    for(let j = 0; j < new_data.length; j++){
                        var service = new_data[j].productservice_description; 
                        var category = new_data[j].category_name;
                        if(new_data[j].productservice_description == null || new_data[j].category_name == null){
                                service = 'OTC';
                                category = 'OTC';
                        }
                        if(j == 0 || counter == j){
                                last_category = category;
                        }
                        if(last_category != category){
                                csr_table.row.add([
                                    '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',
                                    '',
                                    '',
                                    '<strong><h4 class="display-5">'+numberWithCommas(category_total_qty)+'</h4></strong>',
                                    '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                                ]).draw(false);
                                category_total_sales = 0;
                                category_total_qty = 0;
                        }
                        csr_table.row.add([
                                new_data[j].branch_name,
                                category,
                                service,
                                new_data[j].service_count,
                                numberWithCommas(new_data[j].transactiondetails_totalsales)
                        ]).draw(false);
                            category_total_sales = eval(category_total_sales) + eval(parseFloat(new_data[j].transactiondetails_totalsales).toFixed(2));
                            category_total_qty = eval(category_total_qty) + eval(parseFloat(new_data[j].service_count).toFixed(2));
                            total_sales = eval(total_sales) + eval(parseFloat(new_data[j].transactiondetails_totalsales).toFixed(2));
                            last_category = category;
                    }
                    csr_table.row.add([
                        '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',

                        '',

                        '',

                        '<strong><h4 class="display-5">'+numberWithCommas(category_total_qty)+'</h4></strong>',

                        '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                    ]).draw(false);
                    csr_table.row.add([
                        '<strong><h3 class="display-5">GRAND TOTAL</h3></strong>',
                        '',
                        '',
                        '',
                        '<strong><h3 class="display-5">'+numberWithCommas(total_sales)+'</h3></strong>'
                    ]).draw(false);
                    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
                    csr_table.draw();

                }

            })

        }

        else{



            $.ajax({

                

                url: 'search_custom_csr_ho',

                method: 'GET',

                data: {'start': start, 'end': end, 'category_code': category_code, 'productservice_code': productservice_code, 'branch_id': branch_id, 'brand_id': brand_id},

               success: function(data){
                    var csr_data = JSON.parse(data);
                    console.log(csr_data)
                    var data = [];
                    data = data.concat(...csr_data)
                    data.sort(alphabetically(true));
                    console.log(data)
                    var new_data = [];
                    sortObjArray(data, "productservice_description");
                    new_data = removeDuplicatesFromObjArray(data, "productservice_description");
                    console.log(new_data)
                    var total_sales = 0;
                    var category_total_sales = 0;
                    var category_total_qty = 0;
                    var last_category = '';
                    var counter = 0;
                    // data.category_name
                    for(let j = 0; j < new_data.length; j++){
                        var service = new_data[j].productservice_description; 
                        var category = new_data[j].category_name;
                        if(new_data[j].productservice_description == null || new_data[j].category_name == null){
                                service = 'OTC';
                                category = 'OTC';
                        }
                        if(j == 0 || counter == j){
                                last_category = category;
                        }
                        if(last_category != category){
                                csr_table.row.add([
                                    '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',
                                    '',
                                    '',
                                    '<strong><h4 class="display-5">'+numberWithCommas(category_total_qty)+'</h4></strong>',
                                    '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                                ]).draw(false);
                                category_total_sales = 0;
                                category_total_qty = 0;
                        }
                        csr_table.row.add([
                                new_data[j].branch_name,
                                category,
                                service,
                                new_data[j].service_count,
                                numberWithCommas(new_data[j].transactiondetails_totalsales)
                        ]).draw(false);
                            category_total_sales = eval(category_total_sales) + eval(parseFloat(new_data[j].transactiondetails_totalsales).toFixed(2));
                            category_total_qty = eval(category_total_qty) + eval(parseFloat(new_data[j].service_count).toFixed(2));
                            total_sales = eval(total_sales) + eval(parseFloat(new_data[j].transactiondetails_totalsales).toFixed(2));
                            last_category = category;
                    }
                    csr_table.row.add([
                        '<strong><h4 class="display-5">SUB-TOTAL</h4></strong>',

                        '',

                        '',

                        '<strong><h4 class="display-5">'+numberWithCommas(category_total_qty)+'</h4></strong>',

                        '<strong><h4 class="display-5">'+numberWithCommas(category_total_sales)+'</h4></strong>'
                    ]).draw(false);
                    csr_table.row.add([
                        '<strong><h3 class="display-5">GRAND TOTAL</h3></strong>',
                        '',
                        '',
                        '',
                        '<strong><h3 class="display-5">'+numberWithCommas(total_sales)+'</h3></strong>'
                    ]).draw(false);
                    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
                    csr_table.draw();

                }

            })

        }

    }


}

// sorts an array of objects according to one field
// call like this: sortObjArray(myArray, "name" );
// it will modify the input array
sortObjArray = function(arr, field) {
    arr.sort(
        function compare(a,b) {
            if (a[field] < b[field])
                return -1;
            if (a[field] > b[field])
                return 1;
            return 0;
        }
    );
}

// call like this: uniqueDishes = removeDuplicatesFromObjArray(dishes, "dishName");
// it will NOT modify the input array
// input array MUST be sorted by the same field (asc or desc doesn't matter)
removeDuplicatesFromObjArray = function(arr, field) {
    var u = [];
    arr.reduce(function (a, b) {
        if (a[field] !== b[field]) u.push(b);
        return b;
    }, []);
    return u;
}

function alphabetically(ascending) {
  return function (a, b) {
    
    // equal items sort equally
    var a1= a.category_name, b1= b.category_name;
    if (a1 === b1) {
        return 0;
    }
    // nulls sort after anything else
    else if (a1 === null) {
        return 1;
    }
    else if (b1 === null) {
        return -1;
    }
    // otherwise, if we're ascending, lowest sorts first
    else if (ascending) {
        return a1 < b1 ? -1 : 1;
    }
    // if descending, highest sorts first
    else { 
        return a1 < b1 ? 1 : -1;
    }
  };
}


  

  

  

  

  

  