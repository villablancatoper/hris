$(function(){

  $('.select2').select2();

  $('match-height').matchHeight();

  $('#dsrs_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false,

    'columnDefs': [
        { targets: [3, 4, 7, 8, 9, 10], className: "align-right" },
    ],

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    },
    dom: 'Bfrtip',
    buttons: [
      'csv', 'excel', 'print'
    ],

  })

  $('#daterange_btn').daterangepicker(

    {

      ranges: {

        'Today': [moment(), moment()],

        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days': [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month': [moment().startOf('month'), moment().endOf('month')],

        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate: moment()

    },

    function (start, end) {

      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

      $('#start').val(start.format('MM/DD/YYYY'));

      $('#end').val(end.format('MM/DD/YYYY'));

    }

  )

  $('.dt-button').addClass('btn btn-default');

})



function search_dsrs(){

  $('.btn-flat').attr('disabled', 'disabled');

  var dsrs_table = $('#dsrs_table').DataTable();

  $('.dataTables_processing', $('#dsrs_table').closest('.dataTables_wrapper')).show();


  var brand = $('#brands').select2('val');

  var branch = $('#branches').select2('val');

  var start = $('#start').val();

  var end = $('#end').val();



  if(!brand && !branch && !start && !end){

    alert('You must atleast select a brand or a branch, or select a date to search.')
    $('.dataTables_processing', $('#dsrs_table').closest('.dataTables_wrapper')).hide();

    $('.btn-flat').removeAttr('disabled');
  }

  else{

    dsrs_table.clear().draw();



    $.ajax({

      url: 'search_dsrs',

      method: 'GET',

      data: {'brand': brand, 'branch': branch, 'start': start, 'end': end},

      success: function(data){

        var dsrs = JSON.parse(data);



        for(let i = 0; i < dsrs.length; i++){

          dsrs_table.row.add([

            dsrs[i].transaction_date,

            dsrs[i].branch_name,

            dsrs[i].customer_firstname + ' ' + dsrs[i].customer_lastname,

            dsrs[i].transaction_docketno,

            dsrs[i].transaction_orno,

            dsrs[i].sp,

            dsrs[i].assister,

            '&#8369;'+dsrs[i].transaction_totalsales,

            '&#8369;'+dsrs[i].transaction_paymentcash,

            '&#8369;'+dsrs[i].transaction_paymentcard,

            '&#8369;'+dsrs[i].transaction_paymentgc

          ])

        }



        dsrs_table.draw()

        $('.dataTables_processing', $('#dsrs_table').closest('.dataTables_wrapper')).hide();

        $('.btn-flat').removeAttr('disabled');

      }

    })

    // $('#brands').val('').trigger('change');

    // $('#branches').val('').trigger('change');

    // $('#daterange_btn span').html('<i class="fa fa-calendar"></i> Select date');

    // $('#start').val('');

    // $('#end').val('')

  }

}