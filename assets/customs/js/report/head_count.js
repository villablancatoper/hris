$(document).ready(function() {


  var headcount_table = $('#headcount_table').DataTable({
    "dom" : 'Brtip',
    "bDestroy" : true,
    "paging": false,
    buttons: [{
      extend: 'excel',
    }],
    'bAutoWidth': false,
    'columnDefs': [
        { targets: [1,2,3,4,5,6,7,8],className: "align-center" },
      
        
    ]
    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});


$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})


function search(){
  refresh();
  var brand_id = $('#brand').val();
  var branch_id = $('#branch').val();
  var year = $('#year').val();
  var headcount_table = $('#headcount_table').DataTable({
    "processing": true,
    "dom" : 'Brtip',
    "ordering": false,
    "paging": false,
    "bDestroy" : true,
     buttons: [{
        extend: 'excel',
        title: 'Head Count Report | Year: '+ year,
        filename: 'Head Count Report '+ year
      },
      ],
    'columnDefs': [
        { targets: [1,2,3,4,5,6,7,8],className: "align-center" },
        
    ],
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      },
    })
    $('.dataTables_processing', $('#headcount_table').closest('.dataTables_wrapper')).show();
    $('.dt-buttons').addClass('pull-right');
    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
    $('.dt-button').addClass('btn btn-default');

  if (!brand_id || !branch_id || !year) {
    alert("Please select required fields");
    $('.dataTables_processing', $('#headcount_table').closest('.dataTables_wrapper')).hide();
    headcount_table.clear().draw()
  }else{
    $.ajax({
          url: "get_head_count",
          method: "POST",
          data:{
            brand_id: brand_id,branch_id: branch_id, year: year
          },
          success:function(data){
            var headcount_list = JSON.parse(data);
            var walk_in_percentage = 0;
            var regular_percentage = 0;
            var transfer_percentage = 0;
            var grand_walk_in_percentage = 0;
            var grand_regular_percentage = 0;
            var grand_transfer_percentage = 0;
            var total_walk_in = 0;
            var total_regular = 0;
            var total_transfer = 0;
            var grand_total_head_count = 0;
            var total_turnaway = 0;
            var total_head_count = 0;
            console.log(headcount_list);
            for (var i = 0; i < headcount_list.length; i++) {
               total_head_count = eval(headcount_list[i].walk_in) + eval(headcount_list[i].regular) + eval(headcount_list[i].transfer);
               walk_in_percentage = eval(headcount_list[i].walk_in) / total_head_count * 100;
               regular_percentage = eval(headcount_list[i].regular) / total_head_count * 100;
               transfer_percentage = eval(headcount_list[i].transfer) / total_head_count * 100;
               total_walk_in = eval(total_walk_in) + eval(headcount_list[i].walk_in);
               total_regular = eval(total_regular) + eval(headcount_list[i].regular);
               total_transfer = eval(total_transfer) + eval(headcount_list[i].transfer);
               grand_total_head_count = eval(grand_total_head_count) + total_head_count;
               grand_walk_in_percentage = eval(total_walk_in) / grand_total_head_count * 100;
               grand_regular_percentage = eval(total_regular) / grand_total_head_count * 100;
               grand_transfer_percentage = eval(total_transfer) / grand_total_head_count * 100;
               total_turnaway = eval(total_turnaway) + eval(headcount_list[i].turnaway);
              const d = new Date(headcount_list[i].date)
              const n = d.getMonth();
              
              if (isNaN(walk_in_percentage)) {
                 walk_in_percentage = 0 + '%';
              } else {
                 walk_in_percentage = walk_in_percentage.toFixed(2) + '%';
              }
              if (isNaN(regular_percentage)) {
                 regular_percentage = 0 + '%';
              } else {
                 regular_percentage = regular_percentage.toFixed(2) + '%';
              }
              if (isNaN(transfer_percentage)) {
                 transfer_percentage = 0 + '%';
              } else {
                 transfer_percentage = transfer_percentage.toFixed(2) + '%';
              }

              headcount_table.row.add([
                get_month(n),   
                numberWithCommas(headcount_list[i].walk_in),
                walk_in_percentage,
                numberWithCommas(headcount_list[i].regular),
                regular_percentage,             
                numberWithCommas(headcount_list[i].transfer),
                transfer_percentage,
                numberWithCommas(total_head_count),
                numberWithCommas(headcount_list[i].turnaway)
              ])
            }
            if (isNaN(grand_walk_in_percentage)) {
                 grand_walk_in_percentage = 0 + '%';
            } else {
                 grand_walk_in_percentage = grand_walk_in_percentage.toFixed(2) + '%';
            }
            if (isNaN(grand_regular_percentage)) {
                 grand_regular_percentage = 0 + '%';
            } else {
                 grand_regular_percentage = grand_regular_percentage.toFixed(2) + '%';
            }
            if (isNaN(grand_transfer_percentage)) {
                 grand_transfer_percentage = 0 + '%';
            } else {
                 grand_transfer_percentage = grand_transfer_percentage.toFixed(2) + '%';
            }

            $('#total_walk_in').html(numberWithCommas(total_walk_in));
            $('#total_regular').html(numberWithCommas(total_regular));
            $('#total_transfer').html(numberWithCommas(total_transfer));
            $('#grand_total').html(numberWithCommas(grand_total_head_count));
            $('#total_turnaway').html(numberWithCommas(total_turnaway));
            $('#total_percent_walk_in').html(numberWithCommas(grand_walk_in_percentage));
            $('#total_percent_transfer').html(numberWithCommas(grand_transfer_percentage));
            $('#total_percent_regular').html(numberWithCommas(grand_regular_percentage));



          
            // headcount_table.row.add([
            //     "<td class = 'bold_th'>Total</td>",
            //     "<td class = 'bold_th'>"+numberWithCommas(total_walk_in)+"</td>",
            //     "<td class = 'bold_th'>"+numberWithCommas(total_regular)+"</td>",
            //     "<td class = 'bold_th'>"+numberWithCommas(total_transfer)+"</td>",
            //     "<td class = 'bold_th'>"+numberWithCommas(grand_total_head_count)+"</td>",
            //     "<td class = 'blackout'></td>",
            //     "<td class = 'bold_th'>"+numberWithCommas(total_turnaway)+"</td>"

            // ])
            $('.dataTables_processing', $('#headcount_table').closest('.dataTables_wrapper')).hide();
            headcount_table.draw()
            


          }
    })
  }


}
function refresh(){

  var headcount_table = $('#headcount_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,


    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })

   headcount_table.clear();

   headcount_table.draw();

}


function get_month($index){
  if ($index == 0) {
    return "January";
  }else if ($index == 1) {
    return "February";
  }else if ($index == 2) {
    return "March";
  }else if ($index == 3) {
    return "April";
  }else if ($index == 4) {
    return "May";
  }else if ($index == 5) {
    return "June";
  }else if ($index == 6) {
    return "July";
  }else if ($index == 7) {
    return "August";
  }else if ($index == 8) {
    return "September";
  }else if ($index == 9) {
    return "October";
  }else if ($index == 10) {
    return "November";
  }else if ($index == 11) {}{
    return "December";
  }
    
}




function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}

