$(function(){

    $('.match-height').matchHeight();

    var csr_table = $('#csr_table').DataTable({

      'processing': true,

      'bAutoWidth': false,

      'bSort': false, 

      'columnDefs': [

          { targets: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26], className: "align-right" },
          { targets: [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25], className: "align-center"},
          {targets: [0], className: "align-left"},

      ],

      'scrollX': true,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

      },

      dom: 'Bfrtip',

      buttons: [{

        extend: 'excel',

        title: 'Category Sales Report | '+$('#current_date').val(),

        filename: $('#user_branch_name').text()+' (Category Sales Report)'

      }],

      'ordering': false,

      "lengthChange": false,

      "paging": false,

      "bInfo" : false

    })


    $('#daterange_btn').daterangepicker(

      {

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],


          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

    $('.dt-button').addClass('btn btn-default');

    $('.select2').select2();


})

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);
                 $.ajax({
                  url: "fetch_category",
                  method: "POST",
                  data: {brand_id:brand_id},
                  success:function(data){
                    $('#category').html(data);

                  }

                 })
          }

      })


    }

})

$('#category').change(function(){
      var category_id = $('#category').val();
      // alert(category_id)
      if (category_id != 'All Categories') {
        $('#service').prop('disabled', false);
        $.ajax({

            url: "fetch_services",

            method: "POST",

            data: {category_id:category_id},

            success:function(data){
              $('#service').html(data);
            }
        })
      } else {
        $('#service').html('');
        $('#service').prop('disabled', 'disabled');
      }
  })






function search_custom_csr(){
    var brand_id = $('#brand').val();
    var branch_id = $('#branch').val();
    var category_id = $('#category').val();
    var service_id = $('#service').val();
    var year = $('#year').val();


    var csr_table = $('#csr_table').DataTable()
    $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).show();
     if (!brand_id || !branch_id || !category_id || !year) {
        $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
        alert("Please select all fields");
        
    } else if (category_id !== 'All Categories') {
        csr_table.clear().draw();
         $.ajax({
              url: "search_annual_csr",
              method: "POST",
              data: {brand_id: brand_id, branch_id: branch_id, category_id: category_id, service_id: service_id, year: year},
               beforeSend: function(){
                    // $('#total_count').html('');
                    // $('#total_sales').html('');
              },
              success:function(data){
                var result = JSON.parse(data);
                console.log(result);

                var total_count = eval(result['count'][0]) + eval(result['count'][1]) + eval(result['count'][2]) +
                                  eval(result['count'][3]) + eval(result['count'][4]) + eval(result['count'][5]) +
                                  eval(result['count'][6]) + eval(result['count'][7]) + eval(result['count'][8]) +
                                  eval(result['count'][9]) + eval(result['count'][10]) + eval(result['count'][11]) 

                var total_sales = eval(result['monthly_sales'][0]) + eval(result['monthly_sales'][1]) + eval(result['monthly_sales'][2]) +
                                  eval(result['monthly_sales'][3]) + eval(result['monthly_sales'][4]) + eval(result['monthly_sales'][5]) +
                                  eval(result['monthly_sales'][6]) + eval(result['monthly_sales'][7]) + eval(result['monthly_sales'][8]) +
                                  eval(result['monthly_sales'][9]) + eval(result['monthly_sales'][10]) + eval(result['monthly_sales'][11]) 
                if (parseFloat(result['monthly_sales'][0]) === 0) {
                  result['monthly_sales'][0] = result['monthly_sales'][0];
                } else {
                  result['monthly_sales'][0] = numberWithCommas(result['monthly_sales'][0])
                }
                if (parseFloat(result['monthly_sales'][1]) === 0) {
                  result['monthly_sales'][1] = result['monthly_sales'][1];
                } else {
                  result['monthly_sales'][1] = numberWithCommas(result['monthly_sales'][1])
                }
                if (parseFloat(result['monthly_sales'][2]) === 0) {
                  result['monthly_sales'][2] = result['monthly_sales'][2];
                } else {
                  result['monthly_sales'][2] = numberWithCommas(result['monthly_sales'][2])
                }
                if (parseFloat(result['monthly_sales'][3]) === 0) {
                  result['monthly_sales'][3] = result['monthly_sales'][3];
                } else {
                  result['monthly_sales'][3] = numberWithCommas(result['monthly_sales'][3])
                }
                if (parseFloat(result['monthly_sales'][4]) === 0) {
                  result['monthly_sales'][4] = result['monthly_sales'][4];
                } else {
                  result['monthly_sales'][4] = numberWithCommas(result['monthly_sales'][4])
                }
                if (parseFloat(result['monthly_sales'][5]) === 0) {
                  result['monthly_sales'][5] = result['monthly_sales'][5];
                } else {
                  result['monthly_sales'][5] = numberWithCommas(result['monthly_sales'][5])
                }
                if (parseFloat(result['monthly_sales'][6]) === 0) {
                  result['monthly_sales'][6] = result['monthly_sales'][6];
                } else {
                  result['monthly_sales'][6] = numberWithCommas(result['monthly_sales'][6])
                }
                if (parseFloat(result['monthly_sales'][7]) === 0) {
                  result['monthly_sales'][7] = result['monthly_sales'][7];
                } else {
                  result['monthly_sales'][7] = numberWithCommas(result['monthly_sales'][7])
                }
                if (parseFloat(result['monthly_sales'][8]) === 0) {
                  result['monthly_sales'][8] = result['monthly_sales'][8];
                } else {
                  result['monthly_sales'][8] = numberWithCommas(result['monthly_sales'][8])
                }
                if (parseFloat(result['monthly_sales'][9]) === 0) {
                  result['monthly_sales'][9] = result['monthly_sales'][9];
                } else {
                  result['monthly_sales'][9] = numberWithCommas(result['monthly_sales'][9])
                }
                if (parseFloat(result['monthly_sales'][10]) === 0) {
                  result['monthly_sales'][10] = result['monthly_sales'][10];
                } else {
                  result['monthly_sales'][10] = numberWithCommas(result['monthly_sales'][10])
                }
                if (parseFloat(result['monthly_sales'][11]) === 0) {
                  result['monthly_sales'][11] = result['monthly_sales'][11];
                } else {
                  result['monthly_sales'][11] = numberWithCommas(result['monthly_sales'][11])
                }

                if (total_sales === 0) {
                   total_sales = total_sales + 0.00;
                } else {
                  total_sales = numberWithCommas(parseFloat(total_sales).toFixed(2));
                }
                    csr_table.row.add([   
                    result['category'][0],
                    result['service'][0],
                    result['count'][0],
                    result['monthly_sales'][0],
                    result['count'][1],
                    result['monthly_sales'][1],
                    result['count'][2],
                    result['monthly_sales'][2],
                    result['count'][3],
                    result['monthly_sales'][3],
                    result['count'][4],
                    result['monthly_sales'][4],
                    result['count'][5],
                    result['monthly_sales'][5],
                    result['count'][6],
                    result['monthly_sales'][6],
                    result['count'][7],
                    result['monthly_sales'][7],
                    result['count'][8],
                    result['monthly_sales'][8],
                    result['count'][9],
                    result['monthly_sales'][9],
                    result['count'][10],
                    result['monthly_sales'][10],
                    result['count'][11],
                    result['monthly_sales'][11],
                    total_count,
                    total_sales
                  ])
                
                // if (total_count === 0 || total_count === null || total_count === '') {
                //     total_count = '';
                // } else {
                //     total_count = numberWithCommas(total_count);
                // }

                // if (total_sales === 0 || total_sales === null || total_sales === '') {
                //     total_sales = '';
                // } else {
                //     total_sales = numberWithCommas(parseFloat(total_sales).toFixed(2));
                // }

                $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
    //             $('#total_count').html(total_count);
    //             $('#total_sales').html(total_sales);
                csr_table.draw()
              }

         })

    } else {
         $.ajax({
              url: "search_annual_csr",
              method: "POST",
              data: {brand_id: brand_id, branch_id: branch_id, category_id: category_id, service_id: service_id, year: year},
              success:function(data){
                var result = JSON.parse(data);
                console.log(result);
                $('.dataTables_processing', $('#csr_table').closest('.dataTables_wrapper')).hide();
              }
         })
    }



}





  

  

  

  

  

  