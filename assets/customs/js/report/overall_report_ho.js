$(function(){

  $('.select2').select2();

  $('match-height').matchHeight();

  $('#overall_report_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false,

    'columnDefs': [
        { targets: [1, 2, 3, 4, 5, 6, 7], className: "align-right" },
    ],

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    },
    dom: 'Bfrtip',
    buttons: [
      'csv', 'excel', 'print'
    ]

  })

  $('#daterange_btn').daterangepicker(

    {

      ranges: {

        'Today': [moment(), moment()],

        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

        'Last 7 Days': [moment().subtract(6, 'days'), moment()],

        'Last 30 Days': [moment().subtract(29, 'days'), moment()],

        'This Month': [moment().startOf('month'), moment().endOf('month')],

        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

      },

      startDate: moment().subtract(29, 'days'),

      endDate: moment()

    },

    function (start, end) {

      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

      $('#start').val(start.format('MM/DD/YYYY'));

      $('#end').val(end.format('MM/DD/YYYY'));

    }

  )

  $('.dt-button').addClass('btn btn-default');

})



function search_overall_report(){

  $('.btn-flat').attr('disabled', 'disabled');

  var overall_report_table = $('#overall_report_table').DataTable();

  $('.dataTables_processing', $('#overall_report_table').closest('.dataTables_wrapper')).show();

  var brand = $('#brands').select2('val');

  var branch = $('#branches').select2('val');

  var start = $('#start').val();

  var end = $('#end').val();



  if(!brand && !branch && !start && !end){

    alert('You must atleast select a brand or a branch, or select a date to search.')
    $('.dataTables_processing', $('#overall_report_table').closest('.dataTables_wrapper')).hide();

    $('.btn-flat').removeAttr('disabled');
  }

  else{

    overall_report_table.clear().draw();



    $.ajax({

      url: 'search_overall_report',

      method: 'GET',

      data: {'brand': brand, 'branch': branch, 'start': start, 'end': end},

      success: function(data){

        var overall_report_data = JSON.parse(data);

        var overall_reports = overall_report_data['overall_report'];

        var productservices = overall_report_data['productservices'];

        var productretails = overall_report_data['productretails'];

        var turnaways = overall_report_data['turnaways'];



        for(let i = 0; i < overall_reports.length; i++){

          var productservices_totalsales = 0.00;

          var productretails_totalsales = 0.00;

          var turnaways_count = 0;

          

          for(let j = 0; j < productservices.length; j++){

            if(overall_reports[i].branch_id == productservices[j].branch_id){

              productservices_totalsales = eval(productservices_totalsales) + eval(productservices[j].transactiondetails_totalsales);

            }

          }



          for(let j = 0; j < productretails.length; j++){

            if(overall_reports[i].branch_id == productretails[j].branch_id){

              productretails_totalsales = eval(productretails_totalsales) + eval(productretails[j].transactiondetails_totalsales);

            }

          }



          for(let j = 0; j < turnaways.length; j++){

            if(overall_reports[i].branch_id == turnaways[j].branch_id){

              turnaways_count++;

            }

          }



          overall_report_table.row.add([

            // overall_reports[i].transaction_date,

            overall_reports[i].branch_name,

            '&#8369;'+overall_reports[i].total_sales,

            '&#8369;'+(+0.00 + productservices_totalsales).toFixed(2),

            '&#8369;'+(+0.00 + productretails_totalsales).toFixed(2),

            '&#8369;'+overall_reports[i].total_cash,

            '&#8369;'+overall_reports[i].total_credit,

            '&#8369;'+overall_reports[i].total_gc,

            turnaways_count

          ])

        }

        overall_report_table.draw()

        $('.dataTables_processing', $('#overall_report_table').closest('.dataTables_wrapper')).hide();

        $('.btn-flat').removeAttr('disabled');

      }

    })



    // $('#brands').val('').trigger('change');

    // $('#branches').val('').trigger('change');

    // $('#daterange_btn span').html('<i class="fa fa-calendar"></i> Select date');

    // $('#start').val('');

    // $('#end').val('')

  }

}