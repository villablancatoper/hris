$(document).ready(function() {

  var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtp',
    "bDestroy" : true,
    "aaData": null,
    "scrollX": false,
    "buttons" : ['pdf'],
    'columnDefs': [
        { targets: [3, 4, 7, 8], className: "align-right" },
        { targets: [0, 1, 2, 5, 6, 9, 10], className: "align-center" },

        // {targets: 9, width: "15%"},
        // {targets: 10, width: "15%"},
        // {targets: [3,4,5,6,7,8], width: "10%"},
        // {targets: [2], width: "11%"},
        // {targets: [0, 1], width: "10%"}

    ]
    })
  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');


});
// end of function 


$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id !== '') {

      $.ajax({

          url: "z_index/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})


$('#daterange_btn').daterangepicker({
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
      },
      function (start, end) {
        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
        $('#start').val(start.format('YYYY-MM-D'));
        $('#end').val(end.format('YYYY-MM-D'));
      }
)
// end of function 

function compute(){

  var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtp',
    "aaData": null,
    "scrollX": false,
    "bPaginate": false,
    "bDestroy" : true,
    buttons: [{
        extend: 'pdf',
        title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),
        footer: true
        // filename: $('#user_branch_name').text()+' (Category Sales Report)'
      }],
    'columnDefs': [
        { targets: [3, 4, 7, 8], className: "align-right" },
        { targets: [0, 1, 2, 5, 6, 9, 10], className: "align-center" },
        {targets: [0,1, 3, 4, 5, 6, 7, 8], width: "6%"},
        {targets: [2], width: "9%"},
        {targets: [9, 10], width: "13%"},

        // {targets: 9, width: "15%"},
        // {targets: 10, width: "15%"},
        
        // {targets: [2], width: "11%"},
        // {targets: [0, 1], width: "10%"}
    ]
  })
    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');
    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();
    var branch = $('#branch option:selected').text();
    var branch_id = $('#branch').val();
    var start = $('#start').val();
    var end = $('#end').val();
    var brand_id = $('#brand').val();

    // alert(start)
    // alert(end)
    // alert(brand_id)
    // alert(branch_id)
    // var branch_name = $('#branch_name').val();
    if (!start || !end || !branch_id) {
      alert("Selection of branch and date are required")
       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();
        totalsales_table.clear().draw()
    }else{
      $.ajax({
          url: "z_index/compute",
          method: "POST",
          data: {branch_id: branch_id, start: start, end: end, brand_id: brand_id},
          success: function(data){
            var sales_list = JSON.parse(data);
            console.log(sales_list)   
            var total_sales = 0;
            var total_cash = 0;
            var total_deposit = 0;
            var total_card = 0;
            var total_gc = 0;      
            for(let i = 0; i < sales_list.length; i++){
                total_sales = eval(total_sales) + eval(sales_list[i].transaction_totalsales);
                total_cash = eval(total_cash) + eval(sales_list[i].transaction_paymentcash);
                total_deposit = eval(total_deposit) + eval(sales_list[i].cash_deposit);
                total_card = eval(total_card) + eval(sales_list[i].transaction_paymentcard);
                total_gc = eval(total_gc) + eval(sales_list[i].transaction_paymentgc);


                if (sales_list[i].cash_status == 'Approved') {
                    var cash_status = '<span class="badge badge-success" style = "background-color: #28a745 !important; color: white !important; width: 140px !important;">Approved - Cash</span>'
                }else if (sales_list[i].cash_status == 'For Approval' || sales_list[i].cash_status == ''){
                    var cash_status = '<span class="badge badge-danger" style = "background-color: #ffc107 !important; color: black !important; width: 140px !important;">For Approval - Cash</span>'
                }else{
                    var cash_status = '<span class="badge badge-danger" style = "background-color: #dc3545 !important; color: white !important; width: 140px !important;">With Variance - Cash</span>'
                }

                if (sales_list[i].card_status == 'Approved') {
                    var card_status = '<span class="badge badge-success" style = "background-color: #28a745 !important; color: white !important; width: 140px !important;">Approved - Card</span>'
                }else if (sales_list[i].card_status == 'For Approval' || sales_list[i].card_status == ''){
                    var card_status = '<span class="badge badge-danger" style = "background-color: #ffc107 !important; color: black !important; width: 140px !important;">For Approval - Card</span>'
                }else{
                    var card_status = '<span class="badge badge-danger" style = "background-color: #dc3545 !important; color: white !important; width: 140px !important;">With Variance - Card</span>'
                }

                totalsales_table.row.add([
                sales_list[i].transaction_date,
                sales_list[i].brand_name,
                sales_list[i].branch_name,
                numberWithCommas(sales_list[i].transaction_totalsales),
                numberWithCommas(sales_list[i].transaction_paymentcash),                            
                numberWithCommas(sales_list[i].cash_deposit),
                sales_list[i].deposit_date,  
                numberWithCommas(sales_list[i].transaction_paymentcard),          
                numberWithCommas(sales_list[i].transaction_paymentgc),
                '<div class ="row" style = "margin-top: -5px;">'+cash_status+'</div>'+'<div class ="row" style = "margin-top: 5px;">'+card_status+'</div>',
                '<div class = "row" style = "margin-top: -5px !important;"><button value = "'+[branch_id,sales_list[i].branch_id,sales_list[i].brand_id,branch,sales_list[i].transaction_date,sales_list[i].transaction_totalsales,sales_list[i].transaction_paymentcash,sales_list[i].transaction_paymentcard]+'" onclick = "showCashTally(value);"class="btn btn-info btn-sm" style = "font-size: 10px !important; display: inline-block !important; width: 100px !important; height: 20px !important; padding: 0px 0px 0px 0px !important;">Cash Tally</button>'+
                '<button value = "'+[branch_id,sales_list[i].branch_id,sales_list[i].brand_id,sales_list[i].branch_name,branch,sales_list[i].transaction_date,sales_list[i].transaction_totalsales,sales_list[i].transaction_paymentcash,sales_list[i].transaction_paymentcard]+'" onclick = "cashVariance(value);" class="btn btn-info btn-sm" style = "font-size: 10px !important; display: inline-block !important; margin-left: 5px !important; width: 100px !important; height: 20px !important; padding: 0px 0px 0px 0px !important;">Cash Variance</button></div>'+
                '<div class = "row" style = "margin-top: 5px !important; margin-bottom: -5px !important;"><button value = "'+[branch_id,sales_list[i].branch_id,sales_list[i].brand_id,branch,sales_list[i].transaction_date,sales_list[i].transaction_totalsales,sales_list[i].transaction_paymentcash,sales_list[i].transaction_paymentcard]+'" onclick = "showCardTally(value);"class="btn btn-primary btn-sm" style = "font-size: 10px !important; display: inline-block !important; width: 100px !important; height: 20px !important; padding: 0px 0px 0px 0px !important;">Card Tally</button>'+
                '<button value = "'+[branch_id,sales_list[i].branch_id,sales_list[i].brand_id,sales_list[i].branch_name,branch,sales_list[i].transaction_date,sales_list[i].transaction_totalsales,sales_list[i].transaction_paymentcash,sales_list[i].transaction_paymentcard]+'" onclick = "cardVariance(value);" class="btn btn-primary btn-sm" style = "font-size: 10px !important; display: inline-block !important; margin-left: 5px !important; width: 100px !important; height: 20px !important; padding: 0px 0px 0px 0px !important;">Card Variance</button></div>'
                ,
            ])

            }
            $('#total_sales').html(numberWithCommas(total_sales.toFixed(2)));
            $('#total_cash').html(numberWithCommas(total_cash.toFixed(2)));
            $('#total_deposit').html(numberWithCommas(total_deposit.toFixed(2)));
            $('#total_card').html(numberWithCommas(total_card.toFixed(2)));
            $('#total_gc').html(numberWithCommas(total_gc.toFixed(2)));




            $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();
            totalsales_table.draw()

          }

      })

    } 

}// end of function 

function numberWithCommas(x) {
    var parts = x.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
    
}// end of function

function refresh(){
  var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtip',
    "bDestroy" : true,
    "aaData": null,
    "buttons" : ['pdf'],
    'bAutoWidth': false,
    'columnDefs': [
        { targets: [1,2,3,4],
          className: "align-right" }
    ]
    })

   totalsales_table.clear();
   totalsales_table.draw();
  
   $('#totalsales_payment').text("");
   $('#total_cash').text("");
   $('#total_card').text("");
   $('#total_gc').text("");
   $('#cash_deposit').text("");

}

function cashVariance(temp){
    var temp = JSON.stringify(temp)
    var data = temp.split(","); 
    console.log(data)
    $('#brand_id').val(data[2]);
    var branch_name = data[4];
    var transaction_date = data[5];
    var transaction_totalsales = data[6];
    var transaction_paymentcash = data[7];
    var transaction_paymentcard = data[8].replace('"','');
    $('.label').html('Total Cash Payment :' + numberWithCommas(transaction_paymentcash))
    $('#cashVarianceModal').modal('show');
    $('#branch_modal').text(branch_name)
    $('#date_modal').text(transaction_date)
    $('#totalsales').text(transaction_totalsales)
    $('#cash_card').val('cash');
    $('#branch_id').val(data[0].replace('"',''));
    if ($('#branch_id').val() === 'All') {
         $('#branch_id').val(data[1]);
    }
    $('#brand_id').val(data[2]);
    $('#branch_name').val(data[3]);
    $('#transaction_date').val(data[5]);
    $('#transaction_totalsales').val(data[6]);
    $('#transaction_paymentcash').val(data[7]);
    $('#transaction_paymentcard').val(data[8].replace('"',''));
    $('#expected_cash').keyup(function(){
        var expected_cash = $('#expected_cash').val();
        $('#variance').val(expected_cash - transaction_paymentcash);
    });

}

function cardVariance(temp){
    var temp = JSON.stringify(temp)
    var data = temp.split(","); 
    console.log(data)
    $('#brand_id').val(data[2]);
    var branch_name = data[4];
    var transaction_date = data[5];
    var transaction_totalsales = data[6];
    var transaction_paymentcash = data[7];
    var transaction_paymentcard = data[8].replace('"','');
    $('.label-card').html('Total Card Payment :' + numberWithCommas(transaction_paymentcard))
    $('#cardVarianceModal').modal('show');
    $('#cardbranch_modal').text(branch_name)
    $('#carddate_modal').text(transaction_date)
    $('#cardtotalsales').text(transaction_totalsales)
    $('#cash_card').val('card');
    $('#branch_id').val(data[0].replace('"',''));
    if ($('#branch_id').val() === 'All') {
         $('#branch_id').val(data[1]);
    }
    $('#brand_id').val(data[2]);
    $('#branch_name').val(data[3]);
    $('#transaction_date').val(data[5]);
    $('#transaction_totalsales').val(data[6]);
    $('#transaction_paymentcash').val(data[7]);
    $('#transaction_paymentcard').val(data[8].replace('"',''));
    $('#expected_card').keyup(function(){
        var expected_card = $('#expected_card').val();
        $('#card_variance').val(expected_card - transaction_paymentcard);
    });

}

function showCashTally(temp){
    var temp = JSON.stringify(temp)
    var data = temp.split(","); 
    console.log(data)
    $('#cash_card').val('cash');
    $('#branch_id').val(data[0].replace('"',''));
    if ($('#branch_id').val() === 'All') {
         $('#branch_id').val(data[1]);
    }
    $('#brand_id').val(data[2]);
    $('#branch_name').val(data[3]);
    $('#transaction_date').val(data[4]);
    $('#transaction_totalsales').val(data[5]);
    $('#transaction_paymentcash').val(data[6]);
    $('#transaction_paymentcard').val(data[7].replace('"',''));
    $('#tallyModal').modal('show');
}

function showCardTally(temp){
    var temp = JSON.stringify(temp)
    var data = temp.split(","); 
    console.log(data)
    $('#cash_card').val('card');
    $('#branch_id').val(data[0].replace('"',''));
    if ($('#branch_id').val() === 'All') {
         $('#branch_id').val(data[1]);
    }
    $('#brand_id').val(data[2]);
    $('#branch_name').val(data[3]);
    $('#transaction_date').val(data[4]);
    $('#transaction_totalsales').val(data[5]);
    $('#transaction_paymentcash').val(data[6]);
    $('#transaction_paymentcard').val(data[7].replace('"',''));
    $('#tallyModal').modal('show');
}

function submitTally(){
  refresh();
  var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtip',
    "aaData": null,
    "bPaginate": false,
    "bDestroy" : true,
    buttons: [{
        extend: 'pdf',
        title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),
        footer: true
        // filename: $('#user_branch_name').text()+' (Category Sales Report)'
      }],
  })
  var brand_id = $('#brand_id').val();
  var branch_id = $('#branch_id').val();
  var branch_name = $('#branch_name').val();
  var transaction_date = $('#transaction_date').val();
  var transaction_totalsales = $('#transaction_totalsales').val();
  var transaction_paymentcash = $('#transaction_paymentcash').val();
  var transaction_paymentcard = $('#transaction_paymentcard').val();
  var cash_card = $('#cash_card').val();
  // alert(branch_id)
  // alert(transaction_paymentcard)
  // alert(transaction_date)
  // alert(transaction_totalsales)
  // alert(transaction_paymentcash)
  // alert(transaction_paymentcard)

  $.ajax({
      url: 'z_index/submitTally',
      method: 'POST',
      data: {
        brand_id:brand_id,
        branch_id:branch_id,
        branch_name:branch_name,
        transaction_date:transaction_date,
        transaction_totalsales:transaction_totalsales,
        transaction_paymentcash:transaction_paymentcash,
        transaction_paymentcard:transaction_paymentcard,
        cash_card:cash_card
      },
      success: function(data){
           $('#message').html("<div class = 'alert alert-success' style = 'text-align: center;' role = 'alert'>Success!</h5>")
              .hide()
              .fadeIn(100);
              // totalsales_table.rows().invalidate().draw()
              compute();// Add new data
              setTimeout(function(){
              $('#tallyModal').modal('hide')
                }, 1000);
              setTimeout(function(){
              $('#message').html('')
                }, 1000);
                     
      }
  });

}

function cashVarianceSubmit(){
     refresh();
    var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtip',
    "aaData": null,
    "bPaginate": false,
    "bDestroy" : true,
    buttons: [{
        extend: 'pdf',
        title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),
        footer: true
        // filename: $('#user_branch_name').text()+' (Category Sales Report)'
      }],
  })
    var brand_id = $('#brand_id').val();
    var branch_id = $('#branch_id').val();
    var branch_name = $('#branch_name').val();
    var transaction_date = $('#transaction_date').val();
    var transaction_totalsales = $('#transaction_totalsales').val();
    var transaction_paymentcash = $('#transaction_paymentcash').val();
    var transaction_paymentcard = $('#transaction_paymentcard').val();
    var cash_card = $('#cash_card').val();
    var expected_cash = $('#expected_cash').val();
    var variance = $('#variance').val();

    if (expected_cash == '') {
        alert("Please input expected cash payment")
    }else{
        $.ajax({
          url: 'z_index/submitCashVariance',
          method: 'POST',
          data: {
            branch_id:branch_id,
            brand_id:brand_id,
            branch_name:branch_name,
            transaction_date:transaction_date,
            transaction_totalsales:transaction_totalsales,
            transaction_paymentcash:transaction_paymentcash,
            transaction_paymentcard:transaction_paymentcard,
            cash_card:cash_card,
            expected_cash:expected_cash,
            variance:variance
          },
          success: function(data){
              $('#message').html("<div class = 'alert alert-success' style = 'text-align: center;' role = 'alert'>Success!</h5>")
                  .hide()
                  .fadeIn(100);
                  compute();
                  setTimeout(function(){
                  $('#cashVarianceModal').modal('hide')
                    }, 1000);
                  setTimeout(function(){
                  $('#message').html('')
                    }, 1000);
                  $("#form_cash_variance")[0].reset();          
          }
      });
    }
}

function cardVarianceSubmit(){
   refresh();
    var totalsales_table = $('#totalsales_table').DataTable({
    "dom" : 'Brtip',
    "aaData": null,
    "bPaginate": false,
    "bDestroy" : true,
    buttons: [{
        extend: 'pdf',
        title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),
        footer: true
        // filename: $('#user_branch_name').text()+' (Category Sales Report)'
      }],
  })
    var branch_id = $('#branch_id').val();
    var branch_name = $('#branch_name').val();
    var transaction_date = $('#transaction_date').val();
    var transaction_totalsales = $('#transaction_totalsales').val();
    var transaction_paymentcash = $('#transaction_paymentcash').val();
    var transaction_paymentcard = $('#transaction_paymentcard').val();
    var cash_card = $('#cash_card').val();
    var expected_card = $('#expected_card').val();
    var card_variance = $('#card_variance').val();
    console.log(branch_id)

    if (expected_card == '') {
      alert('Please input expected card payment')
    }else{
      $.ajax({                                                             
        url: 'z_index/submitCardVariance',
        method: 'POST',
        data: {
          branch_id:branch_id,
          branch_name:branch_name,
          transaction_date:transaction_date,
          transaction_totalsales:transaction_totalsales,
          transaction_paymentcash:transaction_paymentcash,
          transaction_paymentcard:transaction_paymentcard,
          cash_card:cash_card,
          expected_card:expected_card,
          card_variance:card_variance
        },
        success: function(data){
            $('#message').html("<div class = 'alert alert-success' style = 'text-align: center;' role = 'alert'>Success!</h5>")
                .hide()
                .fadeIn(100);
                compute();// Add new data
                setTimeout(function(){
                $('#cardVarianceModal').modal('hide')
                  }, 100);
                setTimeout(function(){
                $('#message').html('')
                  }, 1000);
                $("#form_card_variance")[0].reset();          
        }
     });
   }

}
