$(document).ready(function() {



  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    buttons: [{

      extend: 'excel',

      title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),

      filename: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),

    }],

    'columnDefs': [

        { targets: [3,4,5,6], className: "align-right" },

    ]

    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});

// end of function 

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})


$('#daterange_btn').daterangepicker({

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-D'));

        $('#end').val(end.format('YYYY-MM-D'));

      }

)

// end of function 



function compute(){
    refresh();
    var totalsales_table = $('#totalsales_table').DataTable({

      "dom" : 'Brtip',

      "aaData": null,

      "bPaginate": false,

      "bDestroy" : true,

      buttons: [{

        extend: 'excel',

        title: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),

        filename: 'Total Sales | '+$('#start').val() + ' to '+$('#end').val(),

      }],

      'columnDefs': [

        { targets: [3,4,5,6], className: "align-right"},

      ],



      "footerCallback": function ( row, data, start, end, display ) {

      

        var api = this.api();

          nb_cols = api.columns().nodes().length;

            var j = 3;

              while(j < nb_cols){

                var intVal = function ( i ) {

                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;

              };

                var pageTotal = api

                      .column( j, { page: 'current'} )

                      .data()

                      .reduce( function (a, b) {

                          return intVal(a) + intVal(b);

                      }, 0 );

                // Update footer



                $( api.column( j ).footer() ).html(parseFloat(pageTotal).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

                j++;

            } 

      }

    })

    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');


    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();

    var brand_id = $('#brand').val();

    var branch = $('#branch option:selected').text();

    var branch_id = $('#branch').val();

    var start = $('#start').val();

    var end = $('#end').val();

    // alert(start)

    // alert(end)

    // alert(branch_id)

    // alert(branch)

    // var branch_name = $('#branch_name').val();

    if (!start || !end || !branch_id || !brand_id) {

      alert("Selection of brand, branch and date are required")

       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

        totalsales_table.clear().draw()
        
    }else{

      $.ajax({

          url: "totalsales/compute",

          method: "POST",

          data: {brand_id:brand_id, branch:branch, branch_id: branch_id, start: start, end: end},

          success: function(data){

            var daily_sales = JSON.parse(data);

            // console.log(daily_sales)            

            for(let i = 0; i < daily_sales.length; i++){

              totalsales_table.row.add([

                daily_sales[i].transaction_date,

                daily_sales[i].brand_name,

                daily_sales[i].branch_name,

                numberWithCommas(daily_sales[i].transaction_totalsales),               

                numberWithCommas(daily_sales[i].transaction_paymentcash),

                numberWithCommas(daily_sales[i].transaction_paymentcard),          

                numberWithCommas(daily_sales[i].transaction_paymentgc)

              ])

            }



            $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

            totalsales_table.draw()



          }



      })



    } 



}// end of function 



function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}// end of function



function refresh(){

  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    "buttons" : ['pdf'],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })



   totalsales_table.clear();

   totalsales_table.draw();

  

   $('#totalsales_payment').text("");

   $('#total_cash').text("");

   $('#total_card').text("");

   $('#total_gc').text("");

}



