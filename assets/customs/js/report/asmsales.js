$(document).ready(function() {



  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    buttons: [{

      extend: 'excel',

      title: 'ASM Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      filename: 'ASM Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

    }],

    'columnDefs': [

        { targets: [1,2,3,4,5], className: "align-right" },

    ]

    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});

// end of function 

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})



$('#daterange_btn').daterangepicker({


        ranges: {

          

          'This Month': [moment().startOf('month'), moment().endOf('month')]

        },
        alwaysShowCalendars:false,
        startDate: moment().subtract(29, 'days'),

        endDate: moment(),
        alwaysShowCalendars:false,
        showCustomRangeLabel:false,
        autoUpdateInput:true,
        autoApply:true
      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

// end of function 



function compute(){
    refresh();



    var totalsales_table = $('#totalsales_table').DataTable({

      "dom" : 'Brtip',

      "aaData": null,

      "bPaginate": false,

      "bDestroy" : true,
      "ordering" :false,
      "ConvertNumericData":false,

      buttons: [{

        extend: 'excel',

        title: 'ASM Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

        filename: 'ASM Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      }],

      'columnDefs': [

        { targets: [1,2,3,4,5], className: "align-right" },

      ],



      

    })

    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');


    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();

    

    var start = $('#month_and_year').val();

    var end = getendmonth($('#month_and_year').val());


     //alert(start)

    // alert(end)

    // alert(brand_id)

    // alert(branch)

    // var branch_name = $('#branch_name').val();

    if (!start || !end ) {

      alert("Selection of Month and Year is required")

       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

        totalsales_table.clear().draw()

    }
      else if(thismonth(start)==false)

      {
         $.ajax({

          url: "asmsales/compute",

          method: "POST",

          data: {start: start, end: end},

          success: function(data){

            var daily_sales = JSON.parse(data);
            var totalstoretarget=0,totaltransaction_totalsales=0,totalmtd=0,totaltrending=0,totaltrendingpercent=0;
            var total="<b><td>TOTAL</td></b>";
            console.log(daily_sales)            

            for(let i = 0; i < daily_sales.length; i++){

              totalsales_table.row.add([

                daily_sales[i].username,                             
                numberWithCommas(daily_sales[i].storetarget_originals),
                numberWithCommas(daily_sales[i].transaction_totalsales),
                (monthlytrendingpercent(daily_sales[i].transaction_totalsales,daily_sales[i].storetarget_originals)).toString(),
                numberWithCommas(trending(daily_sales[i].transaction_totalsales)),
                trendingpercent(daily_sales[i].storetarget_originals,trending(daily_sales[i].transaction_totalsales))
              ])
                 totalstoretarget=parseFloat(totalstoretarget)+parseFloat(daily_sales[i].storetarget_originals);
                 totaltransaction_totalsales=parseFloat(totaltransaction_totalsales)+parseFloat(daily_sales[i].transaction_totalsales);
                 

              
            }
            totalsales_table.row.add([

                total,                             
                numberWithCommas(totalstoretarget.toFixed(2)),
                numberWithCommas(totaltransaction_totalsales.toFixed(2)),
                monthlytrendingpercent(totaltransaction_totalsales,totalstoretarget),
               numberWithCommas((trending(totaltransaction_totalsales))),
                trendingpercent(totalstoretarget,trending(totaltransaction_totalsales))
              ])
            
             $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();


            totalsales_table.draw()



          }



      })



      }



    else{

      $.ajax({

          url: "asmsales/compute",

          method: "POST",

          data: {start: start, end: end},

          success: function(data){

            var daily_sales = JSON.parse(data);
            var totalstoretarget=0,totaltransaction_totalsales=0,totalmtd=0,totaltrending=0,totaltrendingpercent=0;
            var total="<b><td>TOTAL</td></b>";
            console.log(daily_sales)            

            for(let i = 0; i < daily_sales.length; i++){

              totalsales_table.row.add([

                daily_sales[i].username,                             
                numberWithCommas(daily_sales[i].storetarget_originals),
                numberWithCommas(daily_sales[i].transaction_totalsales),
                (monthlytrendingpercent(daily_sales[i].transaction_totalsales,daily_sales[i].storetarget_originals)).toString(),
                numberWithCommas(trending(daily_sales[i].transaction_totalsales)),
                trendingpercent(daily_sales[i].storetarget_originals,trending(daily_sales[i].transaction_totalsales))
              ])
                 totalstoretarget=parseFloat(totalstoretarget)+parseFloat(daily_sales[i].storetarget_originals);
                 totaltransaction_totalsales=parseFloat(totaltransaction_totalsales)+parseFloat(daily_sales[i].transaction_totalsales);
                 

              
            }
            totalsales_table.row.add([

                total,                             
                numberWithCommas(totalstoretarget.toFixed(2)),
                numberWithCommas(totaltransaction_totalsales.toFixed(2)),
                monthlytrendingpercent(totaltransaction_totalsales,totalstoretarget),
               numberWithCommas((trending(totaltransaction_totalsales))),
                trendingpercent(totalstoretarget,trending(totaltransaction_totalsales))
              ])
            
             $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();


            totalsales_table.draw()



          }



      })



    } 



}// end of function 
function thismonth(x)
{

var today = new Date();
  let d = new Date(today);
  d.setMonth(today.getMonth() +1)
  d.setDate(0);
  d.setHours(23);
  d.setMinutes(59);
  d.setSeconds(59);
let start_date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" +01 

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let selected_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" +01
  
  
  if (start_date==selected_date)
  {
    return true;
  }
  else if(start_date>selected_date)
  {
      return false;

  }

}
function storetargetsum(x)
{
var sum=sum+x;
return sum;

}

function monthlytrendingpercent(x,y)
{
  var monthlypercent=(x/y)*100;

  
  return Number(monthlypercent).toFixed(2).concat("%");


}
function trendingpercent(x,y)
{
  var trendingpercent=(y/x)*100;

  
  return Number(trendingpercent).toFixed(2).concat("%");


}
function trending(x)
{

  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/daytodayint)*endday ;
  return trending.toFixed(2);


}
function startmonth(x)
{
    var start='2019-10-01';
    return start;

}

function getendmonth(x)
{

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let formatted_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate()
  return formatted_date;

}
function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}// end of function



function refresh(){

  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    "buttons" : ['pdf'],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })



   totalsales_table.clear();

   totalsales_table.draw();

  

   $('#storetarget_payment').text("");
$('#totalsales_payment').text("");
   $('#total_cash').text("");

   $('#total_card').text("");

   $('#total_gc').text("");
$('#total_cards').text("");
}


