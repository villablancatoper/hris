$(document).ready(function() {



  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,
    "autoWidth": false,
 "processing": true,

    buttons: [{

      extend: 'excel', footer: true,

      title: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      filename: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

    }],

    'columnDefs': [

        { targets: [2,3,4,5,6,7,8,9,10,11,12,13,14], className: "align-right" },
         { "width": "6%", "targets": [2,3,4,5,6,7,8,9,10,11,12,13] },
         { "width": "9%", "targets": [0] },
         { "width": "11%", "targets": [1] },
         {"targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13],"orderable": false},
    ],
'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});

// end of function 

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})
$('#add_brand').change(function(){

    var brand_id = $('#add_brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "annualstore/fetch_branchess",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#add_branches').html(data);

          }

      })

    }

})


$('#daterange_btn').daterangepicker({

        ranges: {

          

          'This Month': [moment().startOf('month'), moment().endOf('month')]

        },

       alwaysShowCalendars:false,
        startDate: moment().subtract(29, 'days'),

        endDate: moment(),
        alwaysShowCalendars:false,
        showCustomRangeLabel:false,
        autoUpdateInput:true,
        autoApply:true

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

// end of function 



function compute(){
    refresh();
    var totalsales_table = $('#totalsales_table').DataTable({

      "dom" : 'Brtip',

      "aaData": null,

      "bPaginate": false,

      "bDestroy" : true,
      "ordering" :false,
 "processing": true,
      buttons: [{

        extend: 'excel',footer: true,

        title: 'Sales Forecast | '+$('#start').val() + ' to '+$('#end').val(),

        filename: 'Sales Forecast | '+$('#start').val() + ' to '+$('#end').val(),

      }],

      'columnDefs': [

         { targets: [2,3,4,5,6,7,8,9,10,11,12,13,14], className: "align-right" },
         { "width": "6%", "targets": [2,3,4,5,6,7,8,9,10,11,12,13] },
         { "width": "9%", "targets": [0] },
         { "width": "11%", "targets": [1] },
         {"targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13],"orderable": false},

      ],

'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
   



      

    })

    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');


    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();

    var start = $('#month_and_year').val();

    var end = getendmonth($('#month_and_year').val());

    

    var branch = $('#branch option:selected').text();
    var brand_id = $('#brand').val();

    var branch_id = $('#branch').val();


    // alert(start)

    // alert(end)

    // alert(brand_id)

    // alert(branch)

    // var branch_name = $('#branch_name').val();

    if (!start || !end ||!branch_id||!brand_id ) {

       alert("Selection of Brand, Branch, and Month is required")

       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

        totalsales_table.clear().draw()

    }

    else 

      {
         $.ajax({

          url: "annualstore/compute",

          method: "POST",

          data: {branch_id: branch_id, start: start, end: end, branch:branch, brand_id:brand_id},

          success: function(data){

            var daily_sales = JSON.parse(data);
           var totaljan=0,totalfeb=0,totalmar=0,totalapr=0,toralmay=0,totaljun=0,totaljul=0,totalaug=0,totalsep=0,totaloct=0,totalnov=0,totaldec=0;
            var totalsalesjan=0,totalsalesfeb=0,totalsalesmar=0,totalsalesapr=0,toralsalesmay=0,totalsalesjun=0,totalsalesjul=0,totalsalesaug=0,totalsalessep=0,totalsalesoct=0,totalsalesnov=0,totalsalesdec=0;
           var branch="";
            
           var total="<b><td>TOTAL</td></b>";
            var count=0;
            var totalbrandsales=0;
            console.log(daily_sales)  

              for (let i = 0; i < daily_sales['list'].length; i++) {
         
                  if(count==0)
                  {
                    totaljan=daily_sales['list'][i]['sales'];
                    totalbrandsales+=parseFloat(totaljan);
                    totalsalesjan+=parseFloat(totaljan);
                  }
                  if(count==1)
                                    {
                  totalfeb=daily_sales['list'][i]['sales'];

                  totalbrandsales+=parseFloat(totalfeb);
                  totalsalesfeb+=parseFloat(totalfeb);
                  }
                  if(count==2)
                  {
                  totalmar=daily_sales['list'][i]['sales'];
                          totalbrandsales+=parseFloat(totalmar);
                  totalsalesmar+=parseFloat(totalmar);
                  }
                  if(count==3)
                  {
                          totalapr=daily_sales['list'][i]['sales'];
                   totalbrandsales+=parseFloat(totalapr);
                  totalsalesapr+=parseFloat(totalapr);
                  }
                  if(count==4)
                  {
                        toralmay=daily_sales['list'][i]['sales'];
                        totalbrandsales+=parseFloat(toralmay);
                        toralsalesmay+=parseFloat(toralmay);
                  }
                  if(count==5)
                  {
                              totaljun=daily_sales['list'][i]['sales'];
                             totalbrandsales+=parseFloat(totaljun);
                            totalsalesjun+=parseFloat(totaljun);
                  }
                  if(count==6)
                  {
                      totaljul=daily_sales['list'][i]['sales'];
                       totalbrandsales+=parseFloat(totaljul);
                        totalsalesjul+=parseFloat(totaljul);
                  }
                  if(count==7)
                                            {
                          totalaug=daily_sales['list'][i]['sales'];
                           totalbrandsales+=parseFloat(totalaug);
                                totalsalesaug+=parseFloat(totalaug);
                  }
                  if(count==8)
                  {
                      totalsep=daily_sales['list'][i]['sales'];
                          totalbrandsales+=parseFloat(totalsep);
                        totalsalessep+=parseFloat(totalsep);
                  }
                  if(count==9)
                  {
                  totaloct=daily_sales['list'][i]['sales'];
                      totalbrandsales+=parseFloat(totaloct);
                              totalsalesoct+=parseFloat(totaloct);
                                    }
                  if(count==10)
                  {
                    totalnov=daily_sales['list'][i]['sales'];
                          totalbrandsales+=parseFloat(totalnov);
                            totalsalesnov+=parseFloat(totalnov);
                  }
                  if(count==11)
                  {
                      totaldec=daily_sales['list'][i]['sales'];
                       totalbrandsales+=parseFloat(totaldec);
                              totalsalesdec+=parseFloat(totaldec);
                  }

                             count++;
                    if (count==12) {
                    brand=daily_sales['list'][i]['brand']; 
                    brandname=daily_sales['list'][i]['brand_name']; 
                     totalsales_table.row.add([
                       brandname,
                      brand,

                     numberWithCommas( totaljan),
                   numberWithCommas( totalfeb),
                   numberWithCommas( totalmar),
                   numberWithCommas( totalapr),
                  numberWithCommas(  toralmay),
                  numberWithCommas(  totaljun),
                  numberWithCommas(  totaljul),
                  numberWithCommas(  totalaug),
                  numberWithCommas(  totalsep),
                  numberWithCommas(  totaloct),
                  numberWithCommas(  totalnov),
                  numberWithCommas(  totaldec),
                    numberWithCommas(totalbrandsales.toFixed(2))

                    ])

                   
                  count=0;
                  totalbrandsales=0;
                    }


                

                  

                 }
                 var totalannualsales=(parseFloat(totalsalesjan)+parseFloat(totalsalesfeb)+parseFloat(totalsalesmar)+parseFloat(totalsalesapr)+parseFloat(toralsalesmay)+parseFloat(totalsalesjun)+parseFloat(totalsalesjul)+parseFloat(totalsalesaug)+parseFloat(totalsalessep)+parseFloat(totalsalesoct)+parseFloat(totalsalesnov)+parseFloat(totalsalesdec)).toFixed(2);
                  $('#jan').text((numberWithCommas(parseFloat(totalsalesjan).toFixed(2))).toString());
                  $('#feb').text((numberWithCommas(parseFloat(totalsalesfeb).toFixed(2))).toString());
                  $('#mar').text((numberWithCommas(parseFloat(totalsalesmar).toFixed(2))).toString());
                  $('#apr').text((numberWithCommas(parseFloat(totalsalesapr).toFixed(2))).toString());
                  $('#may').text((numberWithCommas(parseFloat(toralsalesmay).toFixed(2))).toString());
                  $('#jun').text((numberWithCommas(parseFloat(totalsalesjun).toFixed(2))).toString());
                  $('#jul').text((numberWithCommas(parseFloat(totalsalesjul).toFixed(2))).toString());
                  $('#aug').text((numberWithCommas(parseFloat(totalsalesaug).toFixed(2))).toString());
                  $('#sep').text((numberWithCommas(parseFloat(totalsalessep).toFixed(2))).toString());
                  $('#oct').text((numberWithCommas(parseFloat(totalsalesoct).toFixed(2))).toString());
                  $('#nov').text((numberWithCommas(parseFloat(totalsalesnov).toFixed(2))).toString());
                  $('#dec').text((numberWithCommas(parseFloat(totalsalesdec).toFixed(2))).toString());
                  $('#tot').text((numberWithCommas(parseFloat(totalannualsales).toFixed(2))).toString());
            

             
            
             $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();


            totalsales_table.draw()



          }



      })



      }



    



}// end of function 
 $('#currentinventory_modal').on('show.bs.modal', function(){
    disable_editing();
    $('#currentinventory_alert_success').hide();
    $('#currentinventory_modal').find('.modal-footer').show();
    $('#currentinventory_form').show();
  })
 $('#add_user_modal').on('show.bs.modal', function(){
    disable_editing();
    $('#add_user_alert_success').hide();
    $('#add_user_modal').find('.modal-footer').show();
    $('#add_user_form').show();
  })
function get_item(button){
    $('#currentinventory_modal').modal('show');
    
    $('#storetarget_id').val($(button).attr('data-action-storeid'));
    
    $('#Storetarget_branch').val($(button).attr('data-action-id'));
    $('#Storetarget_month').val($(button).attr('data-action-month'));
    $('#Storetarget_original').val($(button).attr('data-action-orig'));
    $('#Storetarget_forecast').val($(button).attr('data-action-forecast'));
    $('#Storetarget_average').val($(button).attr('data-action-average'));
  }
   function enable_editing(){
    $('#Storetarget_month').removeAttr('disabled');
    $('#Storetarget_original').removeAttr('disabled');
    $('#Storetarget_forecast').removeAttr('disabled');
    $('#Storetarget_average').removeAttr('disabled');
    $("#currentinventory_update_button").removeAttr('disabled');
    $("#currentinventory_delete_button").attr('disabled', 'disabled');
    $("#currentinventory_edit_button").attr('disabled', 'disabled');
  }
  
  function disable_editing(){
    $("#Storetarget_month").attr('disabled', 'disabled');
    $("#Storetarget_original").attr('disabled', 'disabled');
    $("#Storetarget_forecast").attr('disabled', 'disabled');
    $("#Storetarget_average").attr('disabled', 'disabled');
    $("#Storetarget_month").attr('disabled', 'disabled');
    $("#currentinventory_update_button").attr('disabled', 'disabled');
  }
  function delete_record(){

  if (confirm('Are you sure you want to delete this user?')) {

    $.ajax({

      url: 'salesforecast/delete_user/' + $('#storetarget_id').val(),

      success: function () {

        $('#currentinventory_function').text('deleted');

        $('#currentinventory_alert_success').show(400);

        $('#currentinventory_form').hide();

        $('#currentinventory_modal').find('[class="modal-footer"]').hide();

        compute();

      }

    })

  }

}
  function update_currentinventory(){
  
    var storetarget_id = $('#storetarget_id').val();
  
    var storetarget_month = $('#Storetarget_month').val();
    var storetarget_original = $('#Storetarget_original').val();
    var Storetarget_forecast = $('#Storetarget_forecast').val();

    var Storetarget_average = $('#Storetarget_average').val();


  
   
      $.ajax({
        url: 'salesforecast/update_currentinventory_clone',
        method: 'POST',
        data: {'storetarget_id': storetarget_id, 'storetarget_month': storetarget_month, 'storetarget_original': storetarget_original, 'Storetarget_forecast': Storetarget_forecast, 'Storetarget_average': Storetarget_average},
        success: function(){
          $('#currentinventory_alert_success').show(400);
          $('#currentinventory_modal').find('.modal-footer').hide();
          $('#currentinventory_function').text('updated');
          $('#currentinventory_form').hide();
          compute();
        }
      })
    
  }
function thismonth(x)
{

var today = new Date();
  let d = new Date(today);
  d.setMonth(today.getMonth() +1)
  d.setDate(0);
  d.setHours(23);
  d.setMinutes(59);
  d.setSeconds(59);
let start_date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" +01 

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let selected_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" +01
  
  
  if (start_date==selected_date)
  {
    return true;
  }
  else if(start_date>selected_date)
  {
      return false;

  }

}
function storetargetsum(x)
{
var sum=sum+x;
return sum;

}
function getendmonth(x)
{

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let formatted_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate()
  return formatted_date;

}
function monthlytrendingpercent(x,y)
{
  var monthlypercent=(x/y)*100;

  
  return Number(monthlypercent).toFixed(2).concat("%");


}
function trendingpercent(x,y)
{
  var trendingpercent=(y/x)*100;

  
  return Number(trendingpercent).toFixed(2).concat("%");


}
function trending(x,y)
{
  if(thismonth(y)==true)
  {
  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/daytodayint)*endday ;
  return trending.toFixed(2);
  }
else
{
  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/endday)*endday ;
  return trending.toFixed(2);

}
function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}// end of function



}
function refresh(){

  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    "buttons" : ['pdf'],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })



   totalsales_table.clear();

   totalsales_table.draw();

  

   $('#totalsales_payment').text("");

   $('#total_cash').text("");

   $('#total_card').text("");

   $('#jan').text("");
   $('#feb').text("");
   $('#mar').text("");
   $('#apr').text("");
   $('#may').text("");
   $('#jun').text("");
   $('#jul').text("");
   $('#aug').text("");
   $('#sep').text("");
   $('#oct').text("");
   $('#nov').text("");
   $('#dec').text("");
   $('#tot').text("");
  

}

function add_user(){

  $('#add_user_button').attr('disabled', 'disabled');

  $('#add_user_button').text('Adding...');

  // var loading = '<div class="loading small"></div>';

  // $('#add_user_button').append(loading);

  var brand_id = $('#add_brand').val();

  var branch_id = $('#add_branches').val();

  
  var storetarget_month = $('#storetarget_month_and_year').val()

  var storetarget_original = $('#Storetarget_originals').val();
  var storetarget_updatedsalesforecast = $('#Storetarget_forecasts').val();
  var storetarget_averagesalesperformance = $('#Storetarget_averages').val();
  

    


  if(!branch_id || !storetarget_month_and_year || !Storetarget_original || !Storetarget_forecast||!Storetarget_average){

    alert('All fields are required!');

    $('#add_user_button').removeAttr('disabled');

    $('#add_user_button').text('Add');

    // $('.loading').remove();

  }

  else{

    $.ajax({

      url: 'annualstore/add_user',

      method: 'POST',

      data: {branch_id: branch_id, storetarget_month:storetarget_month, storetarget_original:storetarget_original,storetarget_updatedsalesforecast:storetarget_updatedsalesforecast,storetarget_averagesalesperformance:storetarget_averagesalesperformance},

      success: function(data){

        $('#add_user_button').removeAttr('disabled');

        $('#add_user_button').text('Add');

        // $('.loading').remove();

        $('#add_user_alert_success').show(400);

        $('#add_user_form').hide()

        $('#add_user_modal').find('[class="modal-footer"]').hide();
        

           
        
        }

    })

  }

}

