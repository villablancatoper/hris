$(document).ready(function() {



  var commission_table = $('#commission_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    buttons: [{

      extend: 'excel',

    }],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [3,4,5,6,7],

          className: "align-right" }

    ]

    })

  // $('#category_date').text('Select Date')

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});





$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})

$('#commission_type').change(function(){

    var commission_type = $('#commission_type').val();

    if (commission_type == 'service') {

        $('#daterange_btn').html("<span id='category_date'><i class='fa fa-calendar' ></i> Select Date (Service Incentives)</span><i class='fa fa-caret-down'></i>")

        $('#daterange_btn1').addClass('hide')

    }else if (commission_type == 'otc') {

        $('#daterange_btn').html("<span id='category_date'><i class='fa fa-calendar' ></i> Select Date (OTC Commission)</span><i class='fa fa-caret-down'></i>")

        $('#daterange_btn1').addClass('hide')

    }else if (commission_type == 'all') {

        $('#daterange_btn').html("<span id='category_date'><i class='fa fa-calendar' ></i> Select Date (Service Incentives)</span><i class='fa fa-caret-down'></i>")

        $('#daterange_btn1').removeClass('hide')

        $('#daterange_btn1').addClass('shown')

  

    }

})



$('#daterange_btn').daterangepicker({

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

$('#daterange_btn1').daterangepicker({

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start1, end1) {

        $('#daterange_btn1 span').html(start1.format('MMMM D, YYYY') + ' - ' + end1.format('MMMM D, YYYY'))

        $('#start1').val(start1.format('YYYY-MM-DD'));

        $('#end1').val(end1.format('YYYY-MM-DD'));

      }

    )



function compute(){

    var title_brand = $('#brand option:selected').text();

    var title_branch = $('#branch option:selected').text();

    var title_commission_type = $('#commission_type option:selected').text();

    if (title_commission_type == 'All') {

        title_commission_type = 'OTC and Service Incentives';

    }

    var title_start = $('#start').val();

    var title_end = $('#end').val();

    if (!title_start || !title_end) {

        title_start = $('#start1').val();

        title_end = $('#end1').val();

    }else{

        title_start = $('#start').val();

        title_end = $('#end').val();

    }

        if (title_brand === 'Sports Barbers') {
            $("#sb_table").removeClass("hide");
            $("#sb_table").addClass("shown");
            $("#non_sb_table").addClass("hide");

            var commission_table_sb = $('#commission_table_sb').DataTable({
            "processing": true,
            "dom" : 'Brtip',
            "bDestroy" : true,
            "scrollX": true,
            "ordering": true,
             buttons: [{
                extend: 'excel',
                title: 'Commission | '+ title_commission_type,
                filename: 'Commision '+ title_commission_type
              },
              ],
            'columnDefs': [
                { targets: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12], className: "align-right" },
                { targets: [0, 2], width: "11%" },
                { targets: [1], width: "7%" },
                { targets: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12], width: "9%" },
            ],
            'language': {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
              },
            })
             $('.dataTables_processing', $('#commission_table_sb').closest('.dataTables_wrapper')).show();

              $('.select2').select2();

              $('.dt-buttons').addClass('pull-right');

              $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

              $('.dt-button').addClass('btn btn-default');
              var brand_id = $('#brand').val();
              var branch_id = $('#branch').val();
              var commission_type = $('#commission_type').val();
              if (commission_type == 'service') {
                  var start = $('#start').val();
                  var end = $('#end').val();
                  var start1 = '';
                  var end1 = '';
              }else if (commission_type == 'otc') {
                  var start1 = $('#start').val();
                  var end1 = $('#end').val();
                  var start = '';
                  var end = '';
              }else if (commission_type == 'all') {
                  var start = $('#start').val();
                  var end = $('#end').val();
                  var start1 = $('#start1').val();
                  var end1 = $('#end1').val();
              }
              if (!brand_id || !branch_id || !commission_type) {
                alert("Selection of brand, branch, and commission type is required")
                 $('.dataTables_processing', $('#commission_table_sb').closest('.dataTables_wrapper')).hide();
                  commission_table_sb.clear().draw()
              }else{
                $.ajax({
                    url: "commission/compute",
                    method: "POST",
                    data: {brand_id:brand_id, branch_id: branch_id, start: start, end: end, commission_type: commission_type, start1:start1, end1:end1},
                    beforeSend: function(){
                      $("#compute").attr("disabled", true);
                      $("#daterange_btn").attr("disabled", true);
                      $("#daterange_btn1").attr("disabled", true);
                      $("#brand").attr("disabled", true);
                      $("#branch").attr("disabled", true);
                      $("#commission_type").attr("disabled", true);
                    },

                    success: function(data){

                      $("#compute").attr("disabled", false);

                      $("#daterange_btn").attr("disabled", false);

                      $("#daterange_btn1").attr("disabled", false);

                      $("#brand").attr("disabled", false);

                      $("#branch").attr("disabled", false);

                      $("#commission_type").attr("disabled", false);

                      var commission_list = JSON.parse(data);

                      console.log(commission_list)
                      var service_total = 0;
                      var retail_total = 0;
                      var sales_total = 0;
                      var vat_diff_total = 0;
                      var prod_cost_total = 0;
                      var chair_rent_total = 0;
                      var due_lessee_total = 0;
                      var total_sales = 0;
                      var vat = 0;
                      var vat_diff = 0;
                      var prod_cost = 0;
                      var chair_rent = 0;
                      var non_op_penalty = 0;
                      var inc_op_penalty = 0;
                      var due_lessee = 0;
                      var id = '';
                      var otc_commmi = 0;
                      var total_men = 0;
                      var total_woman = 0;
                      var total_technical = 0;
                      var total_other = 0;
                      var net_less_product_cost = 0;
                      commission_table_sb.clear().draw();
   
                      for(let i = 0; i < commission_list.length; i++){
                        net_less_product_cost = eval(eval(commission_list[i].technical_sales / 1.12) * 0.15);
                        total_sales = eval(commission_list[i].total_service) + eval(commission_list[i].total_retail)
                        vat = eval(total_sales) / 1.12;
                        vat_diff = eval(total_sales) - eval(vat);
                        if (parseFloat(commission_list[i].total_retail) === 0){
                            prod_cost = eval(eval(eval(commission_list[i].technical_sales) / 1.12) * 0.15)
                        } else {
                            prod_cost = eval(eval(eval(commission_list[i].technical_sales) / 1.12) * 0.15) + eval(eval(commission_list[i].total_retail / 1.12) * 0.90);
                        }
                        
                        if (parseFloat(commission_list[i].total_service) === 0){
                            due_lessee = eval(commission_list[i].total_commission);
                            chair_rent = 0;
                        } else {
                            due_lessee = eval(commission_list[i].total_commission);
                            chair_rent = eval(eval(eval(commission_list[i].total_service) / 1.12) - eval(eval(commission_list[i].technical_sales) / 1.12) * 0.15) * 0.70
                        }
                        
                        service_total = eval(service_total) + eval(commission_list[i].total_service);
                        retail_total = eval(retail_total) + eval(commission_list[i].total_retail);
                        sales_total = eval(sales_total) + eval(commission_list[i].total_retail) +  eval(commission_list[i].total_service);
                        vat_diff_total = eval(vat_diff_total) + eval(eval(commission_list[i].total_service) - eval(vat))
                        prod_cost_total = eval(prod_cost_total) + eval(prod_cost);
                        chair_rent_total = eval(chair_rent_total) + eval(chair_rent);
                        due_lessee_total = eval(due_lessee_total) + eval(due_lessee);


                        commission_table_sb.row.add([   

                          commission_list[i].branch_name,

                          id,               

                          commission_list[i].serviceprovider_name,       

                          numberWithCommas(commission_list[i].total_service),

                          numberWithCommas(commission_list[i].total_retail),

                          numberWithCommas(total_sales.toFixed(2)),

                          numberWithCommas(vat_diff.toFixed(2)),

                          '',
                          numberWithCommas(prod_cost.toFixed(2)),
                          numberWithCommas(chair_rent.toFixed(2)),
                          '',
                          '',
                          numberWithCommas(due_lessee.toFixed(2))

                        ])

                      }

                      commission_table_sb.row.add([   

                          '<th colspan="3" style = "font-weight: bold;">TOTAL</th>',

                          '',               

                          '',       

                          numberWithCommas(service_total.toFixed(2)),

                          numberWithCommas(retail_total.toFixed(2)),

                          numberWithCommas(sales_total.toFixed(2)),

                          numberWithCommas(vat_diff_total.toFixed(2)),

                          '',
                          numberWithCommas(prod_cost_total.toFixed(2)),
                          numberWithCommas(chair_rent_total.toFixed(2)),
                          '',
                          '',
                          numberWithCommas(due_lessee_total.toFixed(2))

                      ])


                      $('.dataTables_processing', $('#commission_table_sb').closest('.dataTables_wrapper')).hide();

                      commission_table_sb.draw()
                      
                
                    }

                })

              }  


        } else {
            $("#non_sb_table").removeClass("hide");
            $("#non_sb_table").addClass("shown");
            $("#sb_table").addClass("hide");
            var commission_table = $('#commission_table').DataTable({
            "processing": true,
            "dom" : 'Brtip',
            "bDestroy" : true,
            "ordering": true,
             buttons: [{
                extend: 'excel',
                title: 'Commission | '+ title_commission_type,
                filename: 'Commision '+ title_commission_type
              },
              ],
            'columnDefs': [
                { targets: [3, 4, 5, 6, 7], className: "align-right" },
            ],
            'language': {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
              },
            })

        $('.dataTables_processing', $('#commission_table').closest('.dataTables_wrapper')).show();
        $('.dt-buttons').addClass('pull-right');
        $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');
        $('.dt-button').addClass('btn btn-default');
        var brand_id = $('#brand').val();
        var branch_id = $('#branch').val();
        var commission_type = $('#commission_type').val();
        if (commission_type == 'service') {
            var start = $('#start').val();
            var end = $('#end').val();
            var start1 = '';
            var end1 = '';
        }else if (commission_type == 'otc') {
            var start1 = $('#start').val();
            var end1 = $('#end').val();
            var start = '';
            var end = '';
        }else if (commission_type == 'all') {
            var start = $('#start').val();
            var end = $('#end').val();
            var start1 = $('#start1').val();
            var end1 = $('#end1').val();
        }
        if (!brand_id || !branch_id || !commission_type) {
          alert("Selection of brand, branch, and commission type is required")
           $('.dataTables_processing', $('#commission_table').closest('.dataTables_wrapper')).hide();
            commission_table.clear().draw()
        }else{
          $.ajax({
              url: "commission/compute",
              method: "POST",
              data: {brand_id:brand_id, branch_id: branch_id, start: start, end: end, commission_type: commission_type, start1:start1, end1:end1},
              beforeSend: function(){
                $("#compute").attr("disabled", true);
                $("#daterange_btn").attr("disabled", true);
                $("#daterange_btn1").attr("disabled", true);
                $("#brand").attr("disabled", true);
                $("#branch").attr("disabled", true);
                $("#commission_type").attr("disabled", true);
              },

              success: function(data){

                $("#compute").attr("disabled", false);

                $("#daterange_btn").attr("disabled", false);

                $("#daterange_btn1").attr("disabled", false);

                $("#brand").attr("disabled", false);

                $("#branch").attr("disabled", false);

                $("#commission_type").attr("disabled", false);

                var commission_list = JSON.parse(data);

                console.log(commission_list)

                commission_table.clear().draw();

                for(let i = 0; i < commission_list.length; i++){

                  commission_table.row.add([   

                    commission_list[i].brand_name,

                    commission_list[i].branch_name,               

                    commission_list[i].serviceprovider_name,       

                    numberWithCommas(commission_list[i].total_service),

                    numberWithCommas(commission_list[i].total_retail),

                    numberWithCommas(commission_list[i].service_incentives),

                    numberWithCommas(commission_list[i].otc_commission),

                    numberWithCommas(commission_list[i].total_commission)       

                  ])

                }



                $('.dataTables_processing', $('#commission_table').closest('.dataTables_wrapper')).hide();

                commission_table.draw()

              }

          })

        }  



        }

    



}



function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}

