$(function(){

  $('#dsr_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'columnDefs': [
        { targets: [1, 2, 5, 6, 7, 8], className: "align-right" },
    ]

  });

})

function edit_transaction(joborder_id, customer_id){
    window.location.href = $('#base_url').val() + 'joborder/entry/'+customer_id+'/'+joborder_id;
}