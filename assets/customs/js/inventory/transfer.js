$(function(){

  var transferrequest_table = $('#transferrequest_table').DataTable({
    
    'processing': true,

    'lengthChange': false,

    'bAutoWidth': true,

    'language': {
  
      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    }

  });



  transferrequest_table.columns(0).visible(false);

  transferrequest_table.order([1, 'desc']);



  var add_transfer_item_table = $('#add_transfer_item_table').DataTable({

    'lengthChange': false,

    'bAutoWidth': true  

  });



  add_transfer_item_table.columns(0).visible(false);



  var transfer_item_table = $('#transfer_item_table').DataTable({

    'lengthChange': false,

    'bAutoWidth': true  

  });
  

  transfer_item_table.columns(0).visible(false);



  $('.select2').select2();



  $('#add_request_from').val($('#user_branch_id').val()).trigger('change');
  $('#add_request_to').val(207).trigger('change');


  $('#add_transfer_item_table').on( 'click', 'tbody tr button', function () {

    var parent = $(this).parent();

    var grandparent = parent.parent();



    add_transfer_item_table.row(grandparent).remove().draw();

  });

  

  $('#search_box').click(function(){

    $(document).on('keypress', function(event){

      if(event.which == 13){

        if(!$('#search_box').val()){

          alert('Insert value first!')

        }

        else{

          search_transfer_requests();

        }

      }

    })

  });



  jQuery(function($){

    $('.match-height').matchHeight();

  });



  get_transfer_requests();

})





$('#add_request_modal').on('show.bs.modal', function(){

  $('#add_request_alert_success').hide();

  $('#add_request_from').val($('#user_branch_id').val()).trigger('change');

  $('#add_request_to').val(207).trigger('change');

  $('#add_request_items').val('').trigger('change');

  $('#add_request_quantity').val('');



  $('#add_request_form').show();

  $('#add_request_alert_success').hide();

  $('#add_request_modal').find('[class="modal-footer"]').show();



  $('#add_transfer_item_table').DataTable().clear().draw();

});



$('#transfer_request_modal').on('show.bs.modal', function(){

  $('#transfer_request_alert_success').hide();

  $('#transfer_request_remarks').val('');



  $('#transfer_request_form').show();

  $('#transfer_request_alert_success').hide();

});



function add_item(){

  var add_transfer_item_table = $('#add_transfer_item_table').DataTable();



  var request_item = $('#add_request_items').select2('val');

  var request_quantity = $('#add_request_quantity').val();

  var error = '';



  if(!request_item || !request_quantity){

    if(!request_item){

      error += 'Item is required.\n';

    }

    if(!request_quantity){

      error += 'Quantity is required.';

    }



    alert(error);

  }



  $.ajax({

    url: 'get_item_by_item_id',

    method: 'GET',

    data: {'search_item':request_item},

    success: function(data){



      var item = JSON.parse(data);



      var transfer_items = [];



      add_transfer_item_table.rows().every( function(rowIdx, tableLoop, rowLoop) {

        transfer_items.push(this.data()[0]);

      });



      if(transfer_items.includes(item.item_id)){

        alert("Item is already in the table!");

      }

      else{

        add_transfer_item_table.row.add([

          item.item_id,

          item.item_description,

          item.item_uom,

          request_quantity,

          '<button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>',

        ]).draw();

      }

    }

  })



  $('#add_request_items').val('').trigger('change')

  $('#add_request_quantity').val('');

}



function add_request(){

  var to = $('#add_request_to').select2('val');

  var from = $('#add_request_from').select2('val');

  var add_transfer_item_table = $('#add_transfer_item_table').DataTable();

  var error = '';



  if(add_transfer_item_table.rows().data().length == 0 || !to){

    if(add_transfer_item_table.rows().data().length == 0){

      error += 'No item/s found in the table.\n'

    }

    if(!to){

      error += 'Select branch on "To" selection.'

    }



    alert(error);

  }

  else{

    var transfer_items = [];



    add_transfer_item_table.rows().every(function(rowIdx, tableLoop, rowLoop){

      data = {'id': this.data()[0], 'quantity': this.data()[3]}

      transfer_items.push(data);

    })



    $.ajax({

      url: 'transfer_items',

      method: 'POST',

      data: {'transfer_items': transfer_items, 'from': from, 'to': to},

      success: function(){

        $('#add_request_form').hide();

        $('#add_request_alert_success').show(400);

        $('#add_request_modal').find('[class="modal-footer"]').hide();

        $('#add_request_function').text('request a transfer');

        $('#add_request_function_name').text($('#add_request_to').select2('data')[0].text);

        get_transfer_requests()

      }

    })

  }



}



function get_transfer_requests(){

  // $('#loading_modal').modal('show');

  var transferrequest_table = $('#transferrequest_table').DataTable();

  transferrequest_table.clear().draw()

  $('.dataTables_processing', $('#transferrequest_table').closest('.dataTables_wrapper')).show();

  $.ajax({

    url: 'get_transfer_requests',

    method: 'GET',

    success: function(data){

      var transfer_request = JSON.parse(data);



      for(let i = 0; i < transfer_request.length; i++){



        transferrequest_table.row.add([

          transfer_request[i].transferinventory_id,

          transfer_request[i].transferinventory_requestdate,

          transfer_request[i].sender,

          transfer_request[i].receiver,

          transfer_request[i].transferinventory_status,

          '<button class="btn btn-info btn-sm" onclick="get_transfer_request('+ transfer_request[i].transferinventory_id +')"><i class="fa fa-search"></i></button>'

        ])

      }
      transferrequest_table.draw();

      $('.dataTables_processing', $('#transferrequest_table').closest('.dataTables_wrapper')).hide();

      // $('#loading_modal').modal('hide');

    }

  })

}



function get_branch_name(branch_id){

  var branch_name = null;

  $.ajax({

    url: 'get_branch_name',

    method: 'GET',

    data: {'branch_id': branch_id},

    success: function(data){

      var branch = JSON.parse(data);

      branch_name = branch.branch_name;

    }

  })



  return branch_name;

}



function get_transfer_request(id){

  $('#response').empty();

  $('#loading_modal').modal('show');

  $.ajax({

    url: 'get_transfer_request',

    method: 'GET',

    data: {'search_item': id},

    success: function(data){

      var transfer_request = JSON.parse(data);



      $('#transfer_request_from').val(transfer_request.sender);
      
      $('#from').val(transfer_request.from);

      $('#transfer_request_to').val(transfer_request.receiver);

      $('#to').val(transfer_request.to);

      $('#transfer_request_id').val(transfer_request.transferinventory_id);

      

      $.ajax({

        url: 'get_transfer_request_items',

        method: 'GET',

        data: {'search_item': id},

        success: function(data){

          var transfer_request_items = JSON.parse(data);

          

          var transfer_item_table = $('#transfer_item_table').DataTable();



          transfer_item_table.clear().draw()



          for(let i = 0; i < transfer_request_items.length; i++){

            transfer_item_table.row.add([

              transfer_request_items[i].item_id,

              transfer_request_items[i].item_description,

              transfer_request_items[i].transferinventoryitem_quantity,

              transfer_request_items[i].transferinventoryitem_uom

            ])

          }

          transfer_item_table.draw()

        }

      });



      $.ajax({

        url: 'check_respond_availability',

        method: 'GET',

        data: {'status':transfer_request.transferinventory_status, 'branch_id': transfer_request.branch_id},

        success: function(data){

          $('#response').append(data);

        }

      })



      $('#request_history').empty();



      $.ajax({

        url: 'get_transferinventoryhistory/'+transfer_request.transferinventory_id,

        method: 'GET',

        success: function(data){

          var request_history = JSON.parse(data);

          var str = '';



          if(request_history.length == 0){

            str += '<p class="text-center">No history available.</p>'

          }

          else{

            for(let i = 0; i < request_history.length; i++){

              if(i == 0){



                str += '<div class="row"><div class="col-md-6"><div class="row"><div class="col-md-3"><p>Date:</p></div><div class="col-md-9"><p><strong>'+ request_history[i].transferinventoryhistory_date +'</strong></p></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-3"><p>Status:</p></div><div class="col-md-9"><p><strong>'+ request_history[i].transferinventoryhistory_status +'</strong></p></div></div></div></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-3"><p>By:</p></div><div class="col-md-9"><p><strong>'+ request_history[i].user_name +'</strong></p></div></div></div></div><div class="row"><div class="col-md-12"><label for=""></label><textarea rows="3" class="form-control" disabled="disabled">'+ request_history[i].transferinventoryhistory_remarks +'</textarea></div></div><hr>';

              }

              else{



                str+='<div class="row"><div class="col-md-6"><div class="row"><div class="col-md-3"><p>Date:</p></div><div class="col-md-9"><p><strong>'+ request_history[i].transferinventoryhistory_date +'</strong></p></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-3"><p>By:</p></div><div class="col-md-9"><p><strong>'+ request_history[i].user_name +'</strong></p></div></div></div></div><div class="row"><div class="col-md-12"><label for=""></label><textarea rows="3" class="form-control" disabled="disabled">'+ request_history[i].transferinventoryhistory_remarks +'</textarea></div></div><hr>';

              }

            }

          }

          $('#request_history').append(str);
    
          $('#transfer_request_modal').modal('show');

          $('#loading_modal').modal('hide');

        }

      })



    }

  })

}



function search_transfer_requests(){

  var search_item = $('#search_box').val();



  var transferrequest_table = $('#transferrequest_table').DataTable();

  var error = '';



  if(typeof $('#branches').val() != 'undefined' && !$('#branches').val()){

    error += 'Branch is required.'



    alert(error)

  }

  else if((typeof $('#branches').val() == 'undefined') && !search_item){

    error += 'Insert value first.'



    alert(error)

  }

  else{

    transferrequest_table.clear().draw()



    if(search_item){

      $.ajax({

        url: 'search_transferinventory',

        method: 'GET',

        data: {'branch_id': $('#branches').select2('val'), 'search_item': search_item},

        success: function(data){

          var transfer_requests = JSON.parse(data);

          if(transfer_requests.length == 0){

            alert('No request found.')

          }

          else{

            for(let i = 0; i < transfer_requests.length; i++){

    

              transferrequest_table.row.add([

                transfer_requests[i].transferinventory_id,

                transfer_requests[i].transferinventory_requestdate,

                transfer_requests[i].branch_name,

                get_branch_name(transfer_requests[i].transferinventory_receiverid),

                transfer_requests[i].transferinventory_status,

                '<button class="btn btn-info btn-sm" onclick="get_transfer_request('+ transfer_requests[i].transferinventory_id +')"><i class="fa fa-search"></i></button>'

              ]).draw();

            }

          }

        }

      })

    }

    else{

      $.ajax({

        url: 'search_transferinventory',

        method: 'GET',

        data: {'branch_id': $('#branches').select2('val')},

        success: function(data){

          var transfer_requests = JSON.parse(data);



          if(transfer_requests.length == 0){

            alert('No request found.')

          }

          else{

            for(let i = 0; i < transfer_requests.length; i++){

  

              transferrequest_table.row.add([

                transfer_requests[i].transferinventory_id,

                transfer_requests[i].transferinventory_requestdate,

                transfer_requests[i].branch_name,

                get_branch_name(transfer_requests[i].transferinventory_receiverid),

                transfer_requests[i].transferinventory_status,

                '<button class="btn btn-info btn-sm" onclick="get_transfer_request('+ transfer_requests[i].transferinventory_id +')"><i class="fa fa-search"></i></button>'

              ]).draw();

            }

          }

        }

      })

    }

    $('#search_box').val('')

  }

}



function decline_request(){

  var transferinventory_id = $('#transfer_request_id').val()

  var remarks = $('#transfer_request_remarks').val();



  if(!remarks){

    alert('Remarks is required.');

  }

  else{

    $.ajax({

      url: 'decline_request',

      method: 'POST',

      data: {'transferinventory_id': transferinventory_id, 'remarks': remarks},

      success: function(){

        $('#transfer_request_form').hide();

        $('#transfer_request_alert_success').show(400);

        $('#transfer_request_function').text('declined a request');

        $('#transfer_request_function_name').text($('#transfer_request_from').val());

        get_transfer_requests()

      }

    });

  }

}



function approve_request(){

  const transferinventory_id = $('#transfer_request_id').val()

  const remarks = $('#transfer_request_remarks').val();

  const transfer_item_table = $('#transfer_item_table').DataTable();

  let transfer_data = [];

  const from = $('#from').val();

  const to = $('#to').val();

  const data = transfer_item_table.rows().data();

  for(var i = 0; i < data.length; i++){

    transfer_data.push(data[i]);

  }


  if(!remarks){

    alert('Remarks is required.');

  }

  else{

    $.ajax({

      url: 'approve_request',

      method: 'POST',

      data: {'transferinventory_id': transferinventory_id, 'remarks': remarks, 'transfer_items': transfer_data, 'from': from, 'to': to},

      success: function(){

        $('#transfer_request_form').hide();

        $('#transfer_request_alert_success').show(400);

        $('#transfer_request_function').text('approved a request');

        $('#transfer_request_function_name').text($('#transfer_request_from').val());

        get_transfer_requests()

      }

    });

    

  }



}



