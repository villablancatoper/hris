$(function (){
  $('#deliveryreceipt_table').DataTable({
    'processing': true,
    'bAutoWidth': false
  })
  get_dr_items()
})

function get_dr_items(){
  var deliveryreceipt_table = $('#deliveryreceipt_table').DataTable();
  $.ajax({
    url: 'get_dr_items',
    method: "GET",
    success: function(data){
      var deliveryreceipts = JSON.parse(data);

      deliveryreceipt_table.clear().draw();

      for(let i = 0; i < deliveryreceipts.length; i++){
        deliveryreceipt_table.row.add([
          deliveryreceipts[i].deliveryreceipt_date,
          deliveryreceipts[i].branch_name,
          deliveryreceipts[i].deliveryreceipt_remarks,
          deliveryreceipts[i].deliveryreceipt_status,
          '<button onclick="receive_dr('+deliveryreceipts[i].deliveryreceipt_id+')" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>',
        ]);
      }
      deliveryreceipt_table.draw();
    }
  });
}


function receive_dr(deliveryreceipt_id){
  window.location.href = 'receive_dr/'+deliveryreceipt_id;
}