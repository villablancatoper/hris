$(function(){

  
  $('.datepicker').datepicker({
    'autoclose': true
  });

})



function receive_dr(){

  if(confirm("Are you sure you have entered the correct values? \n You cannot undone this if you wish to proceed.")){

    var ids = $("input[id^=drd]");

    var deliveryreceipt_id = $('#deliveryreceipt_id').val();

    const entered_date_received = $('#dr_date_received').val();

    var items = {};

    let error = ''



    for(let i = 0; i < ids.length; i++){

      var quantity = $(ids[i]).val()

      var txt = $(ids[i]).attr('id');

      var id = txt.match(/\d/g);



      id = id.join("");

      items[i] = {id, quantity}

    }

    if(!entered_date_received){

      if(!entered_date_received){
        error += 'Date Received id required!';
      }

      alert(error);

    }
    else{

      $.ajax({
  
        url: 'receive_dr',
  
        method: 'POST',
  
        data: {'items': items, 'deliveryreceipt_id': deliveryreceipt_id, 'deliveryreceipt_receivedate' : entered_date_received},
  
        success: function(){
  
          alert('You have successfully received a DR!')
  
          // window.location.href = $('#base_url').val()+'inventory/receive';
  
        }
  
      })

    }


  }

}



$('input[id^=drd]').on('keydown keyup', function(e){



  var txt = $(this).attr('id');

  var id = txt.match(/\d/g);

  id = id.join("");

  dr_id = 'dr_id['+id+']'



  dr = document.getElementById(dr_id);



  dr_value = parseFloat(dr.value.replace(/\,/g,'')).toFixed(0);

  this_value = parseFloat($(this).val().replace(/\,/g,''));



  if (this_value > dr_value && e.keyCode != 46 && e.keyCode != 8){

      e.preventDefault();     

      $(this).val(dr_value);

  }

  

});