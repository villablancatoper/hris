$(function(){

    var currentinventory_table = $('#currentinventory_table').DataTable({
  
      'bAutoWidth': true,
  
      'processing': true,
  
      'language': {
  
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
      
      }
  
    });
    
    currentinventory_table.columns(0).visible(false);
  
    if($('#user_branch_id').val() != 1){
      currentinventory_table.columns(10).visible(false);
    }
  
    currentinventory_table.order([3, 'asc'])
  
    $('#branches').select2();
  
  
  
    jQuery(function($){
  
      $('.match-height').matchHeight();
  
    });
  
    var buttonCommon = {
      init: function (dt, node, config) {
        var table = dt.table().context[0].nTable;
        if (table) config.title = $(table).data('export-title')
      },
      title: 'default title'
    };
  
    $.extend( $.fn.dataTable.defaults, {
      "buttons": [
          $.extend( true, {}, buttonCommon, {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: ':visible'
              }
          } ),
          $.extend( true, {}, buttonCommon, {
              extend: 'pdfHtml5',
              orientation: 'landscape',
              exportOptions: {
                  columns: ':visible'
              }
          } ),
          $.extend( true, {}, buttonCommon, {
              extend: 'print',
              exportOptions: {
                  columns: ':visible'
              },
              orientation: 'landscape'
          } )
        ]
    } );
  
    // refresh();
  
  })
  
  
  
  function refresh(){
  
    var currentinventory_table = $('#currentinventory_table').DataTable();
  
    currentinventory_table.clear().draw();
  
    $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).show();
  
  
    $.ajax({  
  
      url: 'inventory/get_currentinventory',
  
      method: 'GET',
  
      success: function(data){
  
        var currentinventory = JSON.parse(data);
  
  
  
        for(let i = 0; i < currentinventory.length; i++){
          var conversion_quantity = currentinventory[i].currentinventory_quantity / currentinventory[i].item_uomsize;
          
          conversion_quantity = (+0.00 + conversion_quantity).toFixed(2);
            
  
          currentinventory_table.row.add([
  
            currentinventory[i].currentinventory_id,
  
            currentinventory[i].category_name,
  
            currentinventory[i].item_code,
  
            currentinventory[i].branch_name,
  
            currentinventory[i].item_name,
  
            currentinventory[i].item_description,
  
            currentinventory[i].currentinventory_quantity,
  
            currentinventory[i].item_uom,
            
            conversion_quantity,
            
            currentinventory[i].item_sapuom
  
          ]);
  
        }
  
        currentinventory_table.draw();
  
        $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).hide();
  
      }
  
    });
  
  }
  
  
  
  $(document).on('keypress', function(event){
  
    if(event.which == '13'){
  
      search_currentinventory();
  
    }
  
  });
  
  $('#currentinventory_modal').on('show.bs.modal', function(){
    disable_editing();
    $('#currentinventory_alert_success').hide();
    $('#currentinventory_modal').find('.modal-footer').show();
    $('#currentinventory_form').show();
  })
  
  
  
  function search_currentinventory(){
  
    var search_item = $('#search_box');
  
    var error = '';
  
  
  
    if(typeof $('#branches').val() != 'undefined' && !$('#branches').val()){
  
      error += 'Branch is required.'
  
  
  
      alert(error)
  
    }
  
    else if((typeof $('#branches').val() == 'undefined') && !search_item.val()){
  
      error += 'Insert value first.'
  
      alert(error)
  
    }
  
    else{
  
      var currentinventory_table = $('#currentinventory_table').DataTable();
  
      currentinventory_table.columns(0).visible(false);
  
      currentinventory_table.order([3, 'asc'])
      
      currentinventory_table.clear();
  
      $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).show();
  
      if(search_item.val()){
  
        $.ajax({
  
          url: $('#base_url').val()+'inventory/search_currentinventory_clone',
  
          method: 'GET',
  
          data: {'branch_id': $('#branches').select2('val'), 'search_item': search_item.val()},
  
          success: function(data){
  
            var currentinventory = JSON.parse(data);
  
            if(currentinventory.length == 0){
  
              alert('No item found.')
              $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).hide();
              currentinventory_table.clear().draw()
            }
  
            else{
  
              for(let i = 0; i < currentinventory.length; i++){
                  var conversion_quantity = currentinventory[i].currentinventory_quantity / currentinventory[i].item_uomsize;
                          
                  conversion_quantity = (+0.00 + conversion_quantity).toFixed(2);
                    
          
                  currentinventory_table.row.add([
          
                    currentinventory[i].currentinventory_id,
          
                    currentinventory[i].category_name,
          
                    currentinventory[i].item_code,
          
                    currentinventory[i].branch_name,
          
                    currentinventory[i].item_name,
          
                    currentinventory[i].item_description,
          
                    currentinventory[i].currentinventory_quantity,
          
                    currentinventory[i].item_uom,
                    
                    conversion_quantity,
                    
                    currentinventory[i].item_sapuom,

                    currentinventory[i].currentinventory_dateupdated,
  
                    '<button class="btn btn-info btn-sm" data-currentinventory-id="'+currentinventory[i].currentinventory_id+'" data-currentinventory-crmquantity="'+currentinventory[i].currentinventory_quantity+'" data-currentinventory-name="'+currentinventory[i].item_name+'" data-currentinventory-branch="'+currentinventory[i].branch_name+'" onclick="get_item(this)"><i class="fa fa-search"></i></button>'
          
                  ]);
  
              }
  
              currentinventory_table.draw();
              $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).hide();
  
            }
  
          }
  
        })
  
      }
  
      else{
  
        $.ajax({
  
          url: $('#base_url').val()+'inventory/search_currentinventory_clone',
  
          method: 'GET',
  
          data: {'branch_id': $('#branches').select2('val')},
  
          success: function(data){
  
            var currentinventory = JSON.parse(data);
  
            if(currentinventory.length == 0){
  
              alert('No item found.')
              $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).hide();
              currentinventory_table.clear().draw()
            }
  
            else{
  
              for(let i = 0; i < currentinventory.length; i++){
  
                var conversion_quantity = currentinventory[i].currentinventory_quantity / currentinventory[i].item_uomsize;
                          
                  conversion_quantity = (+0.00 + conversion_quantity).toFixed(2);
                    
          
                  currentinventory_table.row.add([
          
                    currentinventory[i].currentinventory_id,
          
                    currentinventory[i].category_name,
          
                    currentinventory[i].item_code,
          
                    currentinventory[i].branch_name,
          
                    currentinventory[i].item_name,
          
                    currentinventory[i].item_description,
          
                    currentinventory[i].currentinventory_quantity,
          
                    currentinventory[i].item_uom,
                    
                    conversion_quantity,
                    
                    currentinventory[i].item_sapuom,

                    currentinventory[i].currentinventory_dateupdated,
  
                    '<button class="btn btn-info btn-sm" data-currentinventory-id="'+currentinventory[i].currentinventory_id+'" data-currentinventory-crmquantity="'+currentinventory[i].currentinventory_quantity+'" data-currentinventory-name="'+currentinventory[i].item_name+'" data-currentinventory-branch="'+currentinventory[i].branch_name+'" onclick="get_item(this)"><i class="fa fa-search"></i></button>'
          
                  ]);
  
              }
  
              currentinventory_table.draw();
              $('.dataTables_processing', $('#currentinventory_table').closest('.dataTables_wrapper')).hide();
  
            }
  
          }
  
        })
  
      }
  
      search_item.val('');
  
    }
  
  }
  
  function get_item(button){
    $('#currentinventory_modal').modal('show');
    $('#currentinventory_id').val($(button).attr('data-currentinventory-id'));
    $('#currentinventory_branch').val($(button).attr('data-currentinventory-branch'));
    $('#currentinventory_name').val($(button).attr('data-currentinventory-name'));
    $('#currentinventory_crmquantity').val($(button).attr('data-currentinventory-crmquantity'));
  }
  
  function enable_editing(){
    $("#currentinventory_crmquantity").removeAttr('disabled');
    $("#currentinventory_update_button").removeAttr('disabled');
  }
  
  function disable_editing(){
    $("#currentinventory_crmquantity").attr('disabled', 'disabled');
    $("#currentinventory_update_button").attr('disabled', 'disabled');
  }
  
  function update_currentinventory(){
  
    var currentinventory_id = $('#currentinventory_id').val();
  
    var crm_quantity = $('#currentinventory_crmquantity').val();
  
    if(!crm_quantity){
      alert('CRM Quantity is required!');
    }
    else{
      $.ajax({
        url: 'update_currentinventory_clone',
        method: 'POST',
        data: {'currentinventory_id': currentinventory_id, 'currentinventory_quantity': crm_quantity},
        success: function(){
          $('#currentinventory_alert_success').show(400);
          $('#currentinventory_modal').find('.modal-footer').hide();
          $('#currentinventory_function').text('updated');
          $('#currentinventory_form').hide();
          search_currentinventory();
        }
      })
    }
  }