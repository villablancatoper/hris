$(function (){
  $('#add_dr_date').datepicker({
    'autoclose' : true
  });

  var deliveryreceipt_table = $('#deliveryreceipt_table').DataTable({
    'processing': true,
    'bAutoWidth': false,
    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    }
  });

  var drd_items_table = $('#drd_items_table').DataTable({
    'processing': true,
    'bAutoWidth': false,

    "lengthChange": false,

    "paging": false,
    
    "bInfo" : false,

    'bSort' : false
  }).order([1, 'asc']);

  var print_table = $('#print_table').DataTable({
    'processing': true,
    'bAutoWidth': false,

    "lengthChange": false,

    "paging": false,
    
    "bInfo" : false,

    'bSort' : false
  }).order([0, 'asc']);

  deliveryreceipt_table.columns(0).visible(false);
  deliveryreceipt_table.order([1, 'desc']);
  

  $('.select2').select2();
  $('.match-height').matchHeight();
  
  $('#daterange_btn').daterangepicker(
    {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment()
    },
    function (start, end) {
      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      $('#start').val(start.format('MM/DD/YYYY'));
      $('#end').val(end.format('MM/DD/YYYY'));
    }
  )
  get_dr_items();
})

$('#add_dr_modal').on('show.bs.modal', function(){

  $("#add_dr_date").datepicker().on('show.bs.modal', function(event) {
      // prevent datepicker from firing bootstrap modal "show.bs.modal"
    event.stopPropagation(); 
  });

  $('#add_dr_date').datepicker('setDate', null);

  $('#add_dr_branch').val('').trigger('change');
  $('#add_dr_items').val('').trigger('change');
  $('#add_dr_quantity').val('');

  $('#add_dr_form').show();
  $('#add_dr_alert_success').hide();
  $('#add_dr_modal').find('[class="modal-footer"]').show();

  $('#add_transfer_item_table').DataTable().clear().draw();
});


function get_dr_items(){
  $('.dataTables_processing', $('#deliveryreceipt_table').closest('.dataTables_wrapper')).show();
  var deliveryreceipt_table = $('#deliveryreceipt_table').DataTable();
  $.ajax({
    url: 'get_dr_items',
    method: "GET",
    success: function(data){
      var deliveryreceipts = JSON.parse(data);

      deliveryreceipt_table.clear().draw();

      for(let i = 0; i < deliveryreceipts.length; i++){
        var action = '<button onclick="get_dr('+deliveryreceipts[i].deliveryreceipt_id+')" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>';

        if(deliveryreceipts[i].deliveryreceipt_status == 'Waiting for delivery.'){
          action += '<button onclick="delete_dr('+deliveryreceipts[i].deliveryreceipt_id+')" class="btn btn-danger btn-sm" style="margin-left: 3px;"><i class="fa fa-trash"></i></button>';
        }

        deliveryreceipt_table.row.add([
          deliveryreceipts[i].deliveryreceipt_id,
          deliveryreceipts[i].deliveryreceipt_date,
          deliveryreceipts[i].branch_name,
          deliveryreceipts[i].deliveryreceipt_remarks,
          deliveryreceipts[i].deliveryreceipt_status,
          action,
        ]);
      }

      $('.dataTables_processing', $('#deliveryreceipt_table').closest('.dataTables_wrapper')).hide();
      deliveryreceipt_table.draw();
    }
  });
}

function get_dr(deliveryreceipt_id){
  $('#loading_modal').modal('show');

  var drd_items_table = $('#drd_items_table').DataTable();
  $.ajax({
    url: 'get_dr',
    method: 'GET',
    data: {'deliveryreceipt_id': deliveryreceipt_id},
    success: function(data){
      var deliveryreceipt = JSON.parse(data);

      $('#dr_branch').val(deliveryreceipt.branch_name);
      $('.print-branch').val(deliveryreceipt.branch_name);
      
      $('.print-img').attr('src', $('#base_url').val()+'assets/images/brands/'+deliveryreceipt.brand_image);

      $('#dr_no').val(deliveryreceipt.deliveryreceipt_no);
      $('.print-no').val(deliveryreceipt.deliveryreceipt_no);

      $('#dr_packed').val(deliveryreceipt.deliveryreceipt_packed);
      $('.print-packeddate').val(deliveryreceipt.deliveryreceipt_packed);

      $('#dr_pickup').val(deliveryreceipt.deliveryreceipt_pickup);
      $('.print-pickup').val(deliveryreceipt.deliveryreceipt_pickup);

      $('#dr_date').val(deliveryreceipt.deliveryreceipt_date);
      $('.print-date').val(deliveryreceipt.deliveryreceipt_date);

      $('#dr_remarks').val(deliveryreceipt.deliveryreceipt_remarks);

      $.ajax({
        url: 'get_drd',
        method: 'GET',
        data: {'deliveryreceipt_id': deliveryreceipt_id},
        success: function(data){
          var deliveryreceiptdetails = JSON.parse(data);

          drd_items_table.clear().draw();

          $('#print_drd_table_1_tbody').empty();
          $('#print_drd_table_2_tbody').empty();

          $('.print-user').val($('#user_user_name').text());
          $('.print-position').val($('#user_branch_name').text());

          var str = '';
          var sap_uom = '';

          for(let i = 0; i < deliveryreceiptdetails.length; i++){

            var quantity = parseFloat(deliveryreceiptdetails[i].deliveryreceiptdetail_quantity).toFixed(0);
            var receive_quantity = 0;

            sap_uom = deliveryreceiptdetails[i].deliveryreceiptdetail_sapuom;

            if(deliveryreceiptdetails[i].deliveryreceiptdetail_sapuom == null){
              sap_uom = '';
            }

            if(deliveryreceiptdetails[i].deliveryreceiptdetail_receivequantity){
              receive_quantity = parseFloat(deliveryreceiptdetails[i].deliveryreceiptdetail_receivequantity).toFixed(2)
            }

            var variant = 0.00;

            if(quantity != receive_quantity && receive_quantity != 0){
              variant = quantity - receive_quantity;
            }
  
            drd_items_table.row.add([
              deliveryreceiptdetails[i].item_code,
              deliveryreceiptdetails[i].item_description,
              sap_uom,
              quantity,
              receive_quantity,
              variant.toFixed(2)
            ]);

            str += '<tr><td>'+deliveryreceiptdetails[i].vendor_name+'</td>'
            str += '<td>'+deliveryreceiptdetails[i].item_description+'</td>'
            str += '<td>'+sap_uom+'</td>'
            str += '<td>'+parseFloat(deliveryreceiptdetails[i].deliveryreceiptdetail_quantity).toFixed(2)+'</td><tr>'
          }

          drd_items_table.draw();

          $('#loading_modal').modal('hide');

          $('#print_drd_table_1_tbody').append(str)
          $('#print_drd_table_2_tbody').append(str)

          // if(deliveryreceipt.deliveryreceipt_status != 'Waiting for delivery.'){
            
          //   $('#btnPrintRC').removeAttr('style');
          //   $('#btnPrintDR').attr('style', 'margin-left: 5px;'); 

          // }
          // else{
          //   $('#btnPrintRC').attr('style', 'display: none;');
          // }

          $('#dr_modal').modal('show');
        }
      })
    }
  })
}

function delete_dr(deliveryreceipt_id){
  if(confirm('Are you sure you want to delete this DR?')){
    $.ajax({
      url: 'delete_dr',
      method: 'POST',
      data: {'deliveryreceipt_id': deliveryreceipt_id},
      success: function(){
        get_dr_items();
      }
    })
  }
}

function search_drs(){
  var deliveryreceipt_table = $('#deliveryreceipt_table').DataTable();

  $('.dataTables_processing', $('#deliveryreceipt_table').closest('.dataTables_wrapper')).show();
  
  var brand = $('#brands').select2('val');
  var branch = $('#branches').select2('val');
  var start = $('#start').val();
  var end = $('#end').val();
  var error = null;

  if(!brand && !branch && !start && !end){
    alert('You must atleast select a brand or branch, or pick a date to search.')
  }
  else{
    $.ajax({
      url: 'search_drs',
      method: 'GET',
      data: {
        'brand': brand,
        'branch': branch,
        'start': start,
        'end': end
      },
      success: function(data){
        var deliveryreceipts = JSON.parse(data);
  
        deliveryreceipt_table.clear().draw();
  
        for(let i = 0; i < deliveryreceipts.length; i++){
          var action = '<button onclick="get_dr('+deliveryreceipts[i].deliveryreceipt_id+')" class="btn btn-info btn-sm"><i class="fa fa-search"></i></button>';

          if(deliveryreceipts[i].deliveryreceipt_status == 'Waiting for delivery.'){
            action += '<button onclick="delete_dr('+deliveryreceipts[i].deliveryreceipt_id+')" class="btn btn-danger btn-sm" style="margin-left: 3px;"><i class="fa fa-trash"></i></button>';
          }

          deliveryreceipt_table.row.add([
            deliveryreceipts[i].deliveryreceipt_id,
            deliveryreceipts[i].deliveryreceipt_date,
            deliveryreceipts[i].branch_name,
            deliveryreceipts[i].deliveryreceipt_remarks,
            deliveryreceipts[i].deliveryreceipt_status,
            action,
          ]);
        }
        deliveryreceipt_table.draw();

        $('.dataTables_processing', $('#deliveryreceipt_table').closest('.dataTables_wrapper')).hide();
      }
    })
  }
}

function setModalMaxHeight(element) {
  this.$element     = $(element);  
  this.$content     = this.$element.find('.modal-content');
  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight     = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
      'overflow': 'hidden'
  });

  this.$element
      .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
      });
  }

  $('#loading_modal').on('show.bs.modal', function() {
      $(this).show();
      setModalMaxHeight(this);
  });

  $(window).resize(function() {
  if ($('.modal.in').length != 0) {
    setModalMaxHeight($('.modal.in'));
  }
});

document.getElementById("btnPrintDR").onclick = function () {

  
  printElement(document.getElementById("printThis"));

  $('.print-title').removeAttr('style');

  $('.print-hide').attr('style', 'display: none;');
  
   window.print();

  $('.print-title').attr('style', 'display: none;');

  $('.print-hide').removeAttr('style');
}

function printElement(elem) {
  var domClone = elem.cloneNode(true);
  
  var $printSection = document.getElementById("printSection");
  
  if (!$printSection) {
      var $printSection = document.createElement("div");
      $printSection.id = "printSection";
      document.body.appendChild($printSection);
  }
  
  $printSection.innerHTML = "";
  
  $printSection.appendChild(domClone);
}