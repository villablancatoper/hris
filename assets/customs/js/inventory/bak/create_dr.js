$(function(){
  var add_dr_table = $('#add_dr_table').DataTable({
    'bAutoWidth': false,
    'processing': true
  })

  var pdn1_table = $('#pdn1_table').DataTable({
    'bAutoWidth': false,
    'processing': true
  })

  $('.select2').select2();
  $('.match-height').matchHeight();

  $('#add_dr_date').datepicker({
    'autoclose' : true
  });

  // $('.dr-item').hide();

  $('#add_dr_items').on('change', function(){

  })

  $('#add_dr_branch').on('change', function(){
    $('.dr-item').removeAttr('hidden');

    var branch_id = $('#add_dr_branch').select2('val');

    $.ajax({
      url: 'get_pdn1_items_by_whs_code',
      method: 'GET',
      data: {'branch_id': branch_id, 'table': $('#type_of_dr').val()},
      success: function(data){
        var pdn1_items = JSON.parse(data);

        pdn1_table.clear().draw();

        for(let i = 0; i < pdn1_items.length; i++){
          var quantity = pdn1_items[i].Quantity
          quantity = parseInt(quantity).toFixed(2);

          if(!check_item_availability(pdn1_items[i].DocEntry, pdn1_items[i].ItemCode, $('#add_dr_branch').select2('val'))){
            pdn1_table.row.add([
              pdn1_items[i].DocEntry,
              pdn1_items[i].ItemCode,
              pdn1_items[i].Dscription,
              quantity,
              pdn1_items[i].unitMsr,
              '<button onclick="get_pdn_item_by_doc_entry('+ pdn1_items[i].DocEntry +', this); return false;" class="btn btn-xs btn-info" style="margin-left: 3px;"><i class="fa fa-plus"></i></button>',
            ])
          }
        }
        pdn1_table.draw();
      }
    });
  })
})

$('#type_of_dr').on('change', function(){
  var pdn1_table = $('#pdn1_table').DataTable();
  var branch_id = $('#add_dr_branch').select2('val');
  var add_dr_table = $('#add_dr_table').DataTable();

  add_dr_table.clear().draw();

  $.ajax({
    url: 'get_pdn1_items_by_whs_code',
    method: 'GET',
    data: {'branch_id': branch_id, 'table': $('#type_of_dr').val()},
    success: function(data){
      var pdn1_items = JSON.parse(data);

      pdn1_table.clear().draw();

      for(let i = 0; i < pdn1_items.length; i++){
        var quantity = pdn1_items[i].Quantity
        quantity = parseInt(quantity).toFixed(2);

        if(!check_item_availability(pdn1_items[i].DocEntry, pdn1_items[i].ItemCode, $('#add_dr_branch').select2('val'))){
          pdn1_table.row.add([
            pdn1_items[i].DocEntry,
            pdn1_items[i].ItemCode,
            pdn1_items[i].Dscription,
            quantity,
            pdn1_items[i].unitMsr,
            '<button onclick="get_pdn_item_by_doc_entry('+ pdn1_items[i].DocEntry +', this); return false;" class="btn btn-xs btn-info" style="margin-left: 3px;"><i class="fa fa-plus"></i></button>',
          ])
        }
      }
      pdn1_table.draw();
    }
  });
});

function get_pdn_item_by_doc_entry(doc_entry, button){
  var tr = button.parentNode.parentNode;
  var add_dr_table = $('#add_dr_table').DataTable();
  var pdn1_table = $('#pdn1_table').DataTable()
  var url = 'get_pdn_item_by_doc_entry_on_pdn1';

  if($('#type_of_dr').val() == "WTR1"){
    url = 'get_pdn_item_by_doc_entry_on_wtr1';
  }

  $.ajax({
    url: url,
    method: 'GET',
    data: {'doc_entry':doc_entry},
    success: function(data){
      var pdn1_item = JSON.parse(data);
      var quantity = pdn1_item.Quantity
      quantity = parseInt(quantity).toFixed(2);

      add_dr_table.row.add([
        pdn1_item.DocEntry,
        pdn1_item.ItemCode,
        pdn1_item.Dscription,
        quantity,
        pdn1_item.unitMsr,
        '<button class="btn btn-xs btn-danger" onclick="remove_row(this); return false;"><i class="fa fa-remove"></i></button>',
      ])

      add_dr_table.draw();
    }
  })

  pdn1_table.row(tr).remove().draw();
}

function remove_row(button){
  var add_dr_table = $('#add_dr_table').DataTable();
  var tr = button.parentNode.parentNode;
  var td = button.parentNode;
  var first_sibling = $(td).siblings(0)[0];
  var doc_entry = $(first_sibling).text();
  var pdn1_table = $('#pdn1_table').DataTable()
  var url = 'get_pdn_item_by_doc_entry_on_pdn1';

  if($('#type_of_dr').val() == "WTR1"){
    url = 'get_pdn_item_by_doc_entry_on_wtr1';
  }

  $.ajax({
    url: url,
    method: 'GET',
    data: {'doc_entry':doc_entry},
    success: function(data){
      var pdn1_item = JSON.parse(data);

      var quantity = pdn1_item.Quantity
      quantity = parseInt(quantity).toFixed(2);

      pdn1_table.row.add([
        pdn1_item.DocEntry,
        pdn1_item.ItemCode,
        pdn1_item.Dscription,
        quantity,
        pdn1_item.unitMsr,
        '<button onclick="get_pdn_item_by_doc_entry('+ pdn1_item.DocEntry +', this); return false;" class="btn btn-xs btn-info" style="margin-left: 3px;"><i class="fa fa-plus"></i></button>',
      ])
      pdn1_table.draw();
    }
  })

  add_dr_table.row(tr).remove().draw();
}

function create_dr(){
  var error = '';
  var branch = $('#add_dr_branch').select2('val');
  var date = $('#add_dr_date').val();
  var remarks = $('#add_dr_remarks').val();
  var dr_table = $('#add_dr_table').DataTable();

  var dr_table_items = dr_table.rows().data()

  var dr_items = [];

  for(let i = 0; i < dr_table_items.length; i++){
    dr_items[i] = [dr_table_items[i][0], dr_table_items[i][1], dr_table_items[i][2], dr_table_items[i][3],dr_table_items[i][4]];
  }

  if(!branch || !date || dr_table_items.length == 0){
    if(!branch){
      error += 'Branch is required.\n';
    }
    if(!date){
      error += 'Date is required.\n';
    }
    if(dr_table_items.length == 0){
      error += 'Delivery Items table seems to be empty.';
    }

    alert(error);
  }
  else{
    $.ajax({
      url: 'create_dr',
      method: 'POST',
      data:{
        'branch_id': branch,
        'date': date,
        'remarks': remarks,
        'dr_type': $('#type_of_dr').val(),
        'dr_items': dr_items
      },
      success: function(){
        alert('DR has been successfully created.');
        window.location.reload();
      }
    })
  }
}

function check_item_availability(doc_entry, item_code, branch_id){
  var bool = null;
  $.ajax({
    url: 'check_item_availability',
    method: 'GET',
    async: false,
    data: {
      'branch_id': branch_id,
      'doc_entry': doc_entry,
      'item_code': item_code
    },
    success: function(data){
      bool = data;
    }
  });
  return bool;
}