$(function(){

    var add_dr_table = $('#add_dr_table').DataTable({

      'bAutoWidth': false,

      'processing': true,

      'bSort': false

    })



    add_dr_table.columns(0).visible(false);


    $('.select2').select2();

    $('.match-height').matchHeight();

  

    $('#add_dr_date').datepicker({

      'autoclose' : true

    });



    $('#add_dr_packed').datepicker({

        'autoclose' : true

    });

    

    $('#add_dr_pickup').datepicker({

        'autoclose' : true

    });

})



$('#add_dr_branch').on("change", function(e) { 


    var branch_id = $("#add_dr_branch option:selected").val();


    $.ajax({

        url: 'get_brand_items_by_branch_id',

        method: 'GET',

        data: {'branch_id': branch_id},

        success: function(data){



            var items = JSON.parse(data);



            $("#add_item").empty();



            var default_option = new Option('Select item', '', true, true);

            $("#add_item").append(default_option).trigger('change');



            for(let i = 0; i < items.length; i++){



                var sap_uom = '';
                var vendor = '';

                if(items[i].item_sapuom){

                    sap_uom = items[i].item_sapuom;

                }

                if(items[i].item_supplier){

                    vendor = items[i].item_supplier;

                }



                var option = new Option(items[i].item_description, items[i].item_code, false, false);

                $("#add_item").append(option);


                $("#add_item option:last").attr('sap-uom', sap_uom);
                
                $("#add_item option:last").attr('vendor', vendor);

            }



        }

    });



});



function add_item(){



    var add_dr_table = $('#add_dr_table').DataTable();



    var item = $('#add_item').val();



    var quantity = $('#add_quantity').val();



    var error = '';



    var checker = null;

    

    $('#add_item_button').attr('disabled', 'disabled');



    if(!item || !quantity){



        if(!item){

            error += 'Item is required.\n';

        }



        if(!quantity){

            error += 'Quantity is required.\n';

        }



        alert(error);



        $('#add_item_button').removeAttr('disabled');



    }



    else{



        add_dr_table.rows().every(function (){



            var data = this.data();



            if(data[0] == $('#add_item option:selected').val()){



              checker = true;



            }



        });



        if(checker == true){

            alert('Item is already at the table.');

            $('#add_item_button').removeAttr('disabled');

        }

        else{

            add_dr_table.row.add([

                $('#add_item option:selected').val(),

                $('#add_item option:selected').text(),

                quantity,

                $('#add_item option:selected').attr('sap-uom'),

                $('#add_item option:selected').attr('vendor'),

                '<button class="btn btn-xs btn-danger" onclick="remove_row_items_table(this); return false;"><i class="fa fa-remove"></i></button>'

            ]);

    

            add_dr_table.draw();

    

            $('#add_item_button').removeAttr('disabled');



            $('#add_item').val('').trigger('change');

            $('#add_quantity').val('');

        }



    }

}



function remove_row_items_table(button){



    var add_dr_table = $('#add_dr_table').DataTable();

    var tr = button.parentNode.parentNode;

  

    add_dr_table.row(tr).remove().draw();

}



function remove_row_dr_table(button){



    var add_dr_table = $('#add_dr_table').DataTable();

    var tr = button.parentNode.parentNode;

  

    add_dr_table.row(tr).remove().draw();

}



function create_dr_manual(){

    var error = '';

    var branch = $('#add_dr_branch').select2('val');

    var date = $('#add_dr_date').val();

    var packed = $('#add_dr_packed').val();

    var pickup = $('#add_dr_pickup').val();

    var no = $('#add_dr_no').val();

    var remarks = $('#add_dr_remarks').val();

    var dr_table = $('#add_dr_table').DataTable();



    var dr_items = [];



    if(!branch || !date || dr_table.rows().data() == 0){

        if(!branch){

            error += 'Branch is required.\n';

        }

        if(!date){

            error += 'Date is required.\n';

        }

        if(dr_table.rows().data() == 0){

            error += 'Delivery Items table seems to be empty.';

        }



        alert(error);

    }

    else{



        dr_table.rows().every( function () {

            dr_items.push(this.data());

        });



        $.ajax({

            url: 'create_dr_manual',

            method: 'POST',

            data:{

                'branch_id': branch,

                'date': date,

                'packed': packed,

                'pickup': pickup,

                'no': no,

                'remarks': remarks,

                'dr_items': dr_items

            },

            success: function(){

                alert('DR has been successfully created.');

                window.location.reload();

            }

        })

    }

}



function add_to_dr(){



    var items_table = $('#items_table').DataTable();



    var add_dr_table = $('#add_dr_table').DataTable();



    var items_data = items_table;



    var add_vendor_text = $('#add_vendor option:selected').text();



    var add_vendor_id = $('#add_vendor option:selected').val();



    var error = ''



    if(!add_vendor_id || items_data.rows().count() < 1){



        if(!add_vendor_id){

            error += 'Vendor is required.\n';

        }



        if(items_data.rows().count() < 1){

            error += 'Items Table seems to be empty.\n';

        }



        alert(error);



    }



    else{



        for(let i = 0; i < items_data.rows().count(); i++){

        

            var item = items_data.row(i).data()

    

            add_dr_table.row.add([

                item[0],

                item[1],

                item[2],

                item[3],

                add_vendor_id,

                add_vendor_text,

                '<button class="btn btn-xs btn-danger" onclick="remove_row_dr_table(this); return false;"><i class="fa fa-remove"></i></button>',

            ]).draw(false)

        }

        

    

        add_dr_table.draw();

    

        items_table.clear().draw();



        $('#add_vendor').val('').trigger('change');



        $('#loading_modal').modal('hide');



    }



}



function setModalMaxHeight(element) {

    this.$element     = $(element);  

    this.$content     = this.$element.find('.modal-content');

    var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();

    var dialogMargin  = $(window).width() < 768 ? 20 : 60;

    var contentHeight = $(window).height() - (dialogMargin + borderWidth);

    var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;

    var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;

    var maxHeight     = contentHeight - (headerHeight + footerHeight);

  

    this.$content.css({

        'overflow': 'hidden'

    });

  

    this.$element

        .find('.modal-body').css({

        'max-height': maxHeight,

        'overflow-y': 'auto'

        });

    }

  

    $('#loading_modal').on('show.bs.modal', function() {

        $(this).show();

        setModalMaxHeight(this);

    });

  

    $(window).resize(function() {

    if ($('.modal.in').length != 0) {

      setModalMaxHeight($('.modal.in'));

    }

});

