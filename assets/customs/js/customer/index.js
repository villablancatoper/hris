$(function(){

  // get_customers();  

  var customers_table_new = $('#customers_table_new').DataTable({

    'bAutoWidth': true,

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    }

  });

  var customers_table_old = $('#customers_table_old').DataTable({
    'bAutoWidth': true,

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    }
  });

    customers_table_old.columns(0).visible(false);



  $('#customer_birthday').datepicker({

    autoclose: true

  });

  $('#add_customer_customer_birthday').datepicker({

    autoclose: true

  });

  $('#transfer_customer_birthday').datepicker({

    autoclose: true

  });



  $('.select2').select2();



  $("#transfer_customer_birthday").datepicker().on('hide.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });

})



function search_customer(customer_firstname){

  var search_item = $('#search_box').val();
  
  if(customer_firstname){
      search_item = customer_firstname;
  }

  if(!search_item){

    alert('Enter value first on search box!');

  }

  else{

    var customers_table_new = $('#customers_table_new').DataTable();

    var customers_table_old = $('#customers_table_old').DataTable();
    
    if(customer_firstname){
        $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).show();
    
        $.ajax({
    
          url: $('#base_url').val()+'customer/'+'search_customers_new',
    
          method: 'GET',
    
          data: {'search_item': search_item},
    
          success: function(data){
    
            var customers_new = JSON.parse(data);
    
            if(customers_new.length == 0){
    
              customers_table_new.clear().draw();
              $('#search_box').val('');
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
            else{
    
              for(var i = 0; i < customers_new.length; i++){
    
                customers_table_new.row.add([
    
                  customers_new[i].customer_id,
    
                  customers_new[i].customer_firstname,
    
                  customers_new[i].customer_lastname,
    
                  customers_new[i].customer_email,
    
                  '<button class="btn btn-info btn-sm" onclick="get_customer_new('+customers_new[i].customer_id+')"><i class="fa fa-search"></i></button>'
    
                ]).draw(false)
              }
              customers_table_new.clear();
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
          }
    
        });
    }
    else{
        $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).show();
        $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).show();
    
        $.ajax({
    
          url: $('#base_url').val()+'customer/'+'search_customers_new',
    
          method: 'GET',
    
          data: {'search_item': search_item},
    
          success: function(data){
    
            var customers_new = JSON.parse(data);
    
            if(customers_new.length == 0){
    
              customers_table_new.clear().draw();
              $('#search_box').val('');
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
            else{
    
              for(var i = 0; i < customers_new.length; i++){
    
                customers_table_new.row.add([
    
                  customers_new[i].customer_id,
    
                  customers_new[i].customer_firstname,
    
                  customers_new[i].customer_lastname,
    
                  customers_new[i].customer_email,
    
                  '<button class="btn btn-info btn-sm" onclick="get_customer_new('+customers_new[i].customer_id+')"><i class="fa fa-search"></i></button>'
    
                ]).draw(false)
              }
              customers_table_new.clear();
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
          }
    
        });
    
      
        customers_table_new.order([
    
          1, 'asc'
    
        ])
    
        $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).show();
    
        $.ajax({
    
          url: $('#base_url').val()+'customer/'+'search_customers_old',
    
          method: 'GET',
    
          data: {'search_item': search_item},
    
          success: function(data){
    
            var customers_old = JSON.parse(data);
    
            if(customers_old.length == 0){
    
              customers_table_old.clear().draw();
              $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).hide();
            }
    
            else{
    
              for(var i = 0; i < customers_old.length; i++){
    
                customers_table_old.row.add([
    
                  customers_old[i].C_IDNO,
                  
                  customers_old[i].branch_name,
    
                  customers_old[i].C_FNAME,
    
                  customers_old[i].C_LNAME,
    
                  customers_old[i].C_MOBILENO,
    
                  customers_old[i].C_SEX,
    
                  customers_old[i].age_range,
    
                  customers_old[i].location_name,
    
                  '<button class="btn btn-warning btn-sm" onclick="get_customer_old('+customers_old[i].C_IDNO+')"><i class="fa fa-copy"></i></button>'
    
                ]).draw(false)
    
              }
              customers_table_old.clear()
              $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).hide();
            }
    
          }
    
        });
    
    
    
        customers_table_old.order([
    
          1, 'asc'
    
        ])
    }

  }

}



$('#customer_modal').on('show.bs.modal', function(){

  $('#customer_modal').find('[class="modal-footer"]').show();

  $('#customer_alert_success').hide();

  $('#customer_form').show();

  disable_editing();

  $("#customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });

});



$('#transfer_customer_modal').on('show.bs.modal', function(){

  $('#transfer_customer_modal').find('[class="modal-footer"]').show();

  $('#transfer_customer_alert_success').hide();

  $('#transfer_customer_form').show();

  $("#transfer_customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });

});



$('#transfer_customer_modal').on('hide.bs.modal', function(){

  $('#transfer_customer_firstname').val('');

  $('#transfer_customer_lastname').val('');

  $('#transfer_customer_gender').val('');

  $('#transfer_customer_phoneno').val('');

  $('#transfer_customer_email').val('');

  $('#transfer_customer_mobileno').val('');

  $('#transfer_customer_location_id').val('');

  $('#transfer_customer_age_id').val('');

  $('#transfer_customer_occupation_id').val('');

  $('#transfer_customer_birthday').val('');

});



$('#add_customer_modal').on('show.bs.modal', function(){

  $('#add_customer_error').empty();

  $('#add_customer_modal').find('[class="modal-footer"]').show();

  $('#add_customer_alert_success').hide();

  $('#add_customer_form').show();

  $("#add_customer_customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });



  $('#add_customer_firstname').val('');

  $('#add_customer_lastname').val('');

  $('#add_customer_gender').empty('');



  $('#add_customer_gender').append('<option value="">Select gender</option><option value="Male">Male</option><option value="Female">Female</option>');



  $('#add_customer_phoneno').val('');

  $('#add_customer_mobileno').val('');

  $('#add_customer_email').val('');

  $('#add_customer_location_name').val('');

  $('#add_customer_occupation_name').val('');

  $('#add_customer_customer_birthday').val('');

  $('#add_customer_agerange_range').val('');

});



function get_customer_new(id){

  $('#customer_modal').modal('show');

  $('#customer_gender').empty();



  $.ajax({

    url: $('#base_url').val()+'customer/'+'get_customer/'+id,

    success: function(data){

      var customer = JSON.parse(data);



      console.log(customer)



      $('#customer_id').val(customer.customer_id)

      $('#customer_firstname').val(customer.customer_firstname)

      $('#customer_lastname').val(customer.customer_lastname)



      var customer_gender = '';

      

      if(customer.customer_gender != null){

        customer_gender = customer.customer_gender ;

      }

      

      var customer_gender_option = '<option value="'+ customer_gender+'" hidden selected>'+ customer_gender+'</option><option value="Male">Male</option><option value="Female">Female</option>'



      $('#customer_gender').append(customer_gender_option)

      $('#customer_phoneno').val(customer.customer_phoneno)

      $('#customer_mobileno').val(customer.customer_mobileno)



      var selected_location = $('#location_id').find('option[value="' + customer.location_id + '"]');

      $('#location_id').val(selected_location.attr('value')).trigger('change');



      var selected_occupation = $('#occupation_id').find('option[value="' + customer.occupation_id + '"]');

      $('#occupation_id').val(selected_occupation.attr('value')).trigger('change');

      

      $('#customer_birthday').val(customer.customer_birthday)



      var selected_age = $('#age_id').find('option[value="' + customer.age_id + '"]');

      $('#age_id').val(selected_age.attr('value')).trigger('change');



      $('#customer_email').val(customer.customer_email)

      $('#customer_foottraffic').val(customer.customer_foottraffic)

    }

  })

}



$(document).on('keypress', function (e) {

  if (e.which == 13) {

    search_customer();

  }

});



function update_customer(){

  $.ajax({

    url: $('#base_url').val()+'customer/'+'update_customer/'+$('#customer_id').val(),

    method: 'POST',

    data: $('#customer_form').serialize(),

    success: function(data){

      if(data == true){

        $('#function').text('updated');

        $('#function_id').text($('#customer_id').val());

        $('#customer_alert_success').show(400);

        $('#customer_form').hide();

        $('#customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#customer_firstname').val())

      }

    }

  })

}



function delete_customer(){

  if(confirm('Are you sure you want to delete Customer ID: '+$('#customer_id').val())){

    $.ajax({

      url: $('#base_url').val()+'customer/'+'delete_customer/'+$('#customer_id').val(),

      success: function(data){

        $('#function').text('deleted');

        $('#function_id').text($('#customer_id').val());

        $('#customer_alert_success').show(400);

        $('#customer_form').hide();

        $('#customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#customer_firstname').val());

      }

    })

  }

}



function enable_editing(){

  $('#customer_firstname').removeAttr('disabled')

  $('#customer_lastname').removeAttr('disabled')

  $('#customer_gender').removeAttr('disabled')

  $('#customer_phoneno').removeAttr('disabled')

  $('#customer_mobileno').removeAttr('disabled')

  $('#location_id').removeAttr('disabled')

  $('#occupation_id').removeAttr('disabled')

  $('#customer_birthday').removeAttr('disabled')

  $('#age_id').removeAttr('disabled')

  $('#customer_email').removeAttr('disabled')

  $('#update_customer_button').removeAttr('disabled');

}



function disable_editing(){

  $('#customer_firstname').attr('disabled', 'disabled')

  $('#customer_lastname').attr('disabled', 'disabled')

  $('#customer_gender').attr('disabled', 'disabled')

  $('#customer_phoneno').attr('disabled', 'disabled')

  $('#customer_mobileno').attr('disabled', 'disabled')

  $('#location_id').attr('disabled', 'disabled')

  $('#occupation_id').attr('disabled', 'disabled')

  $('#customer_birthday').attr('disabled', 'disabled')

  $('#age_id').attr('disabled', 'disabled')

  $('#customer_email').attr('disabled', 'disabled');

  $('#update_customer_button').attr('disabled', 'disabled');

}



function test(){

  $('#add_customer_modal').modal('show');

}



function add_customer(){

  $('#add_customer_error').empty();



  var firstname = $('#add_customer_firstname').val();



  if(!firstname){

    $('#add_customer_error').append('<p class="text-danger">Firstname is required.</p>');

  }

  else{

    $.ajax({

      url: $('#base_url').val()+'customer/'+'add_customer',

      method: 'POST',

      data: $('#add_customer_form').serialize(),

      success: function(data){

        if(data == true){

          $('#add_customer_function').text('added');

          $('#add_customer_function_name').text($('#add_customer_firstname').val() + ' ' +$('#add_customer_lastname').val() );

          $('#add_customer_alert_success').show(400);

          $('#add_customer_form').hide();

          $('#add_customer_modal').find('[class="modal-footer"]').hide();

            search_customer($('#add_customer_firstname').val());

        }

      }

    })

  }

}



function get_customer_old(customerolddata_id){

  if(confirm('Are you sure you want transfer this data to the new database?')){

    $.ajax({

      url: $('#base_url').val()+'customer/'+'is_customer_data_transferred/'+customerolddata_id,

      success: function(data){

        if(data == true){

          alert("This customer has already been transferred to the new database");

        }

        else{

          $.ajax({

            url: $('#base_url').val()+'customer/'+'get_customer_old/'+customerolddata_id,

            success: function(data){

              var customer_old = JSON.parse(data);

      

              $('#transfer_customer_customerolddata_id').val(customerolddata_id)

              $('#transfer_customer_firstname').val(customer_old.customerolddata_firstname);

              $('#transfer_customer_lastname').val(customer_old.customerolddata_lastname);

              $('#transfer_customer_gender').val(customer_old.customerolddata_gender);

              $('#transfer_customer_mobileno').val(customer_old.customerolddata_contactno);

      

              var transfer_selected_location = $('#transfer_customer_location_id').find('option[value="' + customer_old.location_id + '"]');

              $('#transfer_customer_location_id').val(transfer_selected_location.attr('value')).trigger('change');

      

              var transfer_selected_age = $('#transfer_customer_age_id').find('option[value="' + customer_old.age_id + '"]');

              $('#transfer_customer_age_id').val(transfer_selected_age.attr('value')).trigger('change');

            }

          })

          $('#transfer_customer_modal').modal('show');

        }

      }

    })

  }

}



function transfer_customer_old_data(){

  $('#transfer_customer_error').empty()

  var error = null;



  // var firstname = $('#transfer_customer_firstname').val();

  // var lastname = $('#transfer_customer_lastname').val();

  // var gender = $('#transfer_customer_gender').val();

  // var phoneno = $('#transfer_customer_phoneno').val();

  // var mobileno = $('#transfer_customer_mobileno').val();

  // var email = $('#transfer_customer_email').val();



  if(!$('#transfer_customer_firstname').val()){

    error += '<p class="text-danger">Firstname is required.</p>'

    $('#transfer_customer_error').append(error);

  }

  else{

    $.ajax({

      url: 'transfer_customer_old_data/'+$('#transfer_customer_customerolddata_id').val(),

      method: 'POST',

      data: $('#transfer_customer_form').serialize(),

      success: function(){

        $('#transfer_customer_function').text('transferred');

        $('#transfer_customer_function_name').text($('#transfer_customer_firstname').val() + ' ' +$('#transfer_customer_lastname').val());

        $('#transfer_customer_alert_success').show(400);

        $('#transfer_customer_form').hide();

        $('#transfer_customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#transfer_customer_firstname').val());

      }

    })

  }

}