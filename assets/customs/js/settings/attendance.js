$(function(){

  var attendance_table = $('#attendance_table').DataTable({
     'processing': true,
      "dom": 'Brtip',
      'bAutoWidth': false,
      'buttons': [{
        extend: 'excel',
        exportOptions: {
          columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
        },
        title: ''
      }],
  
      'bSort': false,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
      },
      'ordering': true,
      'columnDefs': [
        { targets: [5, 6, 7, 8, 9, 10, 11, 13],
          className: "none" }
      ]
  });

 
  


 $('.select2').select2();
 

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

})

$('#daterange_btn').daterangepicker({

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],

          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )



function search_name(){

  var attendance_table = $('#attendance_table').DataTable()

  $('.dataTables_processing', $('#attendance_table').closest('.dataTables_wrapper')).show();
  
  var name_query = $('#name_query').val();
  var start = $('#start').val();
  var end = $('#end').val();

  if (!start || !end) {
    alert("Date is require");
    $('.dataTables_processing', $('#attendance_table').closest('.dataTables_wrapper')).hide();
  } else if (!name_query) {
    alert("Please input in textbox");
    $('.dataTables_processing', $('#attendance_table').closest('.dataTables_wrapper')).hide();
  } else {
    $.ajax({

      url: "search_name",

      method: "POST",

      data: {name_query: name_query, start: start, end: end},

      success:function(data){
        var result = JSON.parse(data)
        console.log(result)
        attendance_table.clear().draw();
        for(let i = 0; i < result.length; i++){
          attendance_table.row.add([
            result[i].department,
            result[i].user_no,
            result[i].user_id,
            result[i].name,
            result[i].date_time,
            result[i].status,
            result[i].operation,
            result[i].exception_description,
            result[i].timetable,
            result[i].identification_code,
            result[i].identification,
            result[i].work_code,
            result[i].device_no,
            result[i].checked,
            result[i].location,
          ])
        }
        $('.dataTables_processing', $('#attendance_table').closest('.dataTables_wrapper')).hide();

        attendance_table.draw()
      }

    })
  }

}






