$(document).ready(function() {



  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    buttons: [{

      extend: 'excel',

      title: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

      filename: 'Brand Sales Report | '+$('#start').val() + ' to '+$('#end').val(),

    }],

    'columnDefs': [

        { targets: [1,2,3,4,5], className: "align-right" },

    ]

    })

  $('.select2').select2();

  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

});


function changedate(x)
  {
      var today = new Date(x);
      var dd = today.getDate();
      var mm = today.getMonth()-1; 
      var yyyy = today.getFullYear();
      if(dd<10) 
      {
          dd='0'+dd;
      } 
      if(mm==0||mm==(-1)){
        if(mm==0){
          yyyy= yyyy-1;
          mm=12;
        }
        if(mm==(-1)){
          yyyy= yyyy-1;
          mm=11;
        }
      }
      if(mm<10) 
      {
          mm='0'+mm;
      } 
      
     
      today = yyyy+'-'+mm+'-'+dd;
      console.log(today);
      return today;

  }
  function changeend(x)
  {
      var today = new Date(x);
      var dd = today.getDate();
      var mm = today.getMonth()-1; 
      var yyyy = today.getFullYear();
     
      if(dd<10) 
      {
          dd='0'+dd;
      } 
      if(mm==0||mm==(-1)){
        if(mm==0){
          yyyy= yyyy-1;
          mm=12;
        }
        if(mm==(-1)){
          yyyy= yyyy-1;
          mm=11;
        }
      }
      if(mm<10) 
      {
          mm='0'+mm;
      } 
      
     
      
      var lastDay = new Date(yyyy, mm , 0);
      lastDay=lastDay.getDate();
      today = yyyy+'-'+mm+'-'+lastDay;
      console.log(today);
      return today;

  }
 
  $('#add_branches').change(function(){
    console.log('success');
    
    var selected_branch = $('#branch').find('option[value="' + $('#add_brand').val() + '"]');
  
    $('#branch').val(selected_branch.attr('value')).trigger('change');
    get_performance_forecast();
  
  
  })
  String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
  }
$('#Storetarget_last_performance').change(function(){
  console.log('success');
  var last_month_sales = $('#Storetarget_last_performance').val();
  $('#Storetarget_fore').val('asd')  

})
String.prototype.replaceAt=function(index, replacement) {
  return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}
function get_performance_forecast(){
  
  var elements = document.getElementsByTagName("input");
  for (var ii=0; ii < elements.length; ii++) {
  if (elements[ii].type == "text"||elements[ii].type == "number") {
    elements[ii].value = "0";
  }
}
  var brand_id = $('#add_brand').val();
  var branch_id = $('#add_branches').val();
  var branch = $('#add_branches option:selected').text();
  var start = $('#storetarget_month_and_year').val();
  start=changedate(start);
 
  var end = getendmonth($('#storetarget_month_and_year').val());
  end=changeend(end);
  if (brand_id != ''&&branch_id != ''&&start != '') {

    $.ajax({

        url: "salesforecast/get_lastmonth_performance",

        method: "POST",

        data: {branch_id: branch_id, start: start, end: end, branch:branch, brand_id:brand_id},

        success:function(data){

          var total=JSON.parse(data);
          console.log('success');
        let forecast=  (total.transaction_totalsales*1.25).toFixed(0);
        console.log('before'+forecast);
        let x=forecast.length;
        var count=0;
        var xy=0;
        if(x-1>=0){
          xy= forecast.charAt(x-1);
          if(xy>0){
            count++;
            forecast= forecast.replaceAt(x-1, "0")
          }
        }
        if(x-2>=0){
          xy= forecast.charAt(x-2);
          if(xy>0){
            count++;
            forecast= forecast.replaceAt(x-2, "0")
          }
        }
        if(x-3>=0){
          xy= forecast.charAt(x-3);
          if(xy>0){
            count++;
            forecast= forecast.replaceAt(x-3, "0")
          }
        }

        if(x-4>=0){
          xy= forecast.charAt(x-4);
          if(xy<5){
            
              forecast= forecast.replaceAt(x-4, "5")
            
            
          }
          if(xy>=5){
            
              forecast= forecast.replaceAt(x-4, "0")
              forecast=parseFloat(forecast)+10000
          }
        }
      
        
     
        
          
          
        
      
        
        // forecast= forecast.charAt(x-2);
        // forecast= forecast.charAt(x-3);
        // forecast= forecast.charAt(x-4);

      
        console.log( ('after'+forecast));
        
          
          $('#Storetarget_last_performance').val( numberWithCommas( total.transaction_totalsales));
          $('#Storetarget_fore').val(numberWithCommas(forecast));
        }

    })

  }
}
function get_target_walkin(){
 
  var original_target = $('#Storetarget_originals').val();
  var brand_id = $('#add_brand').val();
  var branch_id = $('#add_branches').val();
  
 
 

  if (original_target != ''&&brand_id!=''&&branch_id!='') {

    $.ajax({

        url: "salesforecast/get_target_walkin",

        method: "POST",

        data: {brand_id:brand_id},

        success:function(data){

          var tph=JSON.parse(data);
          var target_walkin=   numberWithCommas( ((original_target/tph.tph_target)/2).toFixed(2));
          $('#Storetarget_target_walkin').val( target_walkin );
          $('#Storetarget_target_regular').val( numberWithCommas( ((original_target/tph.tph_target)/2).toFixed(2)) );
        }

    })

  }
}
function formatStoretarget_originals(num) {
  $('#commaseparated').val(numberWithCommas($('#Storetarget_originals').val()));
 
};
function formatStoretarget_averages(num) {
  $('#commaseparatedaverage').val(numberWithCommas($('#Storetarget_averages').val()));
 
};
function formatlast_sales(num) {
  $('#commaseparated_last_year').val(numberWithCommas($('#Storetarget_last_sales').val()));
 
};
function formatlast_walkin(num) {
  $('#commaseparated_last_walkin').val(numberWithCommas($('#Storetarget_last_walkin').val()));
 
};
function formatlast_regular(num) {
  $('#commaseparated_last_regular').val(numberWithCommas($('#Storetarget_last_regular').val()));
 
};
$('#Storetarget_last_regular').keyup(function(){
  formatlast_regular($(this).val());
})
$('#Storetarget_last_walkin').keyup(function(){
  formatlast_walkin($(this).val());
})
$('#Storetarget_originals').keyup(function(){
  formatStoretarget_originals($(this).val());

  get_target_walkin();
 
})
$('#Storetarget_averages').keyup(function(){
  formatStoretarget_averages($(this).val());

  
 
})
$('#Storetarget_last_sales').keyup(function(){
  formatlast_sales($(this).val());
})
$('#storetarget_month_and_year').change(function(){
  
  get_performance_forecast();
})
// end of function 
$('#branch').change(function(){

  var branch_id = $('#branch').val();

  console.log(branch_id);

})
$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "commission/fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})
$('#add_brand').change(function(){

    var brand_id = $('#add_brand').val();

    if (brand_id != '') {

      $.ajax({

          url: "salesforecast/fetch_branchess",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){
            var selected_brand = $('#brand').find('option[value="' + $('#add_brand').val() + '"]');

            $('#brand').val(selected_brand.attr('value')).trigger('change');
            $('#add_branches').html(data);
            get_performance_forecast();
          }

      })

    }

})


$('#daterange_btn').daterangepicker({

        ranges: {

          

          'This Month': [moment().startOf('month'), moment().endOf('month')]

        },

       alwaysShowCalendars:false,
        startDate: moment().subtract(29, 'days'),

        endDate: moment(),
        alwaysShowCalendars:false,
        showCustomRangeLabel:false,
        autoUpdateInput:true,
        autoApply:true

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

// end of function 



function compute(){
    refresh();
    var totalsales_table = $('#totalsales_table').DataTable({

      "dom" : 'Brtip',

      "aaData": null,

      "bPaginate": false,

      "bDestroy" : true,
      "ordering" :false,

      buttons: [{

        extend: 'excel',

        title: 'Sales Forecast | '+$('#start').val() + ' to '+$('#end').val(),

        filename: 'Sales Forecast | '+$('#start').val() + ' to '+$('#end').val(),

      }],

      'columnDefs': [

        { targets: [1,2,3,4,5,6,7,8,9,10,11], className: "align-right" },

      ],



      

    })

    $('.dt-buttons').addClass('pull-right');

    $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

    $('.dt-button').addClass('btn btn-default');


    $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).show();

    
    var start = $('#month_and_year').val();

    var end = getendmonth($('#month_and_year').val());

    

    var branch = $('#branch option:selected').text();
    var brand_id = $('#brand').val();

    var branch_id = $('#branch').val();


    // alert(start)

    // alert(end)

    // alert(brand_id)

    // alert(branch)

    // var branch_name = $('#branch_name').val();

    if (!start || !end ||!branch_id||!brand_id ) {

       alert("Selection of Brand, Branch, and Month is required")

       $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();

        totalsales_table.clear().draw()

    }else

      {
         $.ajax({

          url: "salesforecast/compute",

          method: "POST",

          data: {branch_id: branch_id, start: start, end: end, branch:branch, brand_id:brand_id},

          success: function(data){

            var daily_sales = JSON.parse(data);
            var totalstoretarget=0,totaltransaction_totalsales=0,totalmtd=0,totaltrending=0,totaltrendingpercent=0;
            var total="<b><td>TOTAL</td></b>";
            console.log(daily_sales)            

            for(let i = 0; i < daily_sales.length; i++){

              totalsales_table.row.add([

                daily_sales[i].brandname, 
                daily_sales[i].branch_name, 
                 daily_sales[i].storetargetmonth,                             
                numberWithCommas(daily_sales[i].storetarget_originals),
                numberWithCommas(daily_sales[i].salesforecast),
                numberWithCommas(daily_sales[i].averagesales),

                numberWithCommas(daily_sales[i].storetarget_lastmonth_performance),
                numberWithCommas(daily_sales[i].last_year_sales),
                numberWithCommas(daily_sales[i].last_year_walkin),
                numberWithCommas(daily_sales[i].last_year_regular),
                numberWithCommas(daily_sales[i].target_walkin),
                numberWithCommas(daily_sales[i].target_regular),
               '<button class="btn btn-info btn-sm" data-action-target_regular="'+daily_sales[i].target_regular+'" data-action-target_walkin="'+daily_sales[i].target_walkin+'" data-action-last_year_regular="'+daily_sales[i].last_year_regular+'" data-action-last_year_walkin="'+daily_sales[i].last_year_walkin+'" data-action-last_year_sales="'+daily_sales[i].last_year_sales+'" data-action-storetarget_lastmonth_performance="'+daily_sales[i].storetarget_lastmonth_performance+'" data-action-storeid="'+daily_sales[i].store_id+'" data-action-storeid="'+daily_sales[i].store_id+'" data-action-id="'+daily_sales[i].bid+'" data-action-month="'+daily_sales[i].storetargetmonth+'" data-action-orig="'+daily_sales[i].storetarget_originals+'" data-action-fore="'+daily_sales[i].salesforecast+'"  data-action-average="'+daily_sales[i].averagesales+'" onclick="get_item(this)"><i class="fa fa-search"></i></button>'
          
              ])
                 

              
            }
            
            
             $('.dataTables_processing', $('#totalsales_table').closest('.dataTables_wrapper')).hide();


            totalsales_table.draw()



          }



      })



      }



    



}// end of function 
 $('#currentinventory_modal').on('show.bs.modal', function(){
    disable_editing();
    $('#currentinventory_alert_success').hide();
    $('#currentinventory_modal').find('.modal-footer').show();
    $('#currentinventory_form').show();
  })
 $('#add_user_modal').on('show.bs.modal', function(){
    disable_editing();
    $('#add_user_form').show();
    $('#add_user_alert_success').hide();
    $('#add_user_modal').find('.modal-footer').show();
    
  })
function get_item(button){

    $('#currentinventory_modal').modal('show');
    
    $('#storetarget_id').val($(button).attr('data-action-storeid'));
    
    $('#Storetarget_branch').val($(button).attr('data-action-id'));
    $('#Storetarget_month').val($(button).attr('data-action-month'));
    $('#Storetarget_original').val($(button).attr('data-action-orig'));
    $('#Storetarget_fores').val($(button).attr('data-action-fore'));
    $('#Storetarget_average').val($(button).attr('data-action-average'));

    $('#edit_Storetarget_last_sales').val($(button).attr('data-action-last_year_sales'));
    $('#edit_Storetarget_last_walkin').val($(button).attr('data-action-last_year_walkin'));
    $('#edit_Storetarget_last_regular').val($(button).attr('data-action-last_year_regular'));
    $('#edit_Storetarget_target_walkin').val($(button).attr('data-action-target_walkin'));
    $('#edit_Storetarget_target_regular').val($(button).attr('data-action-target_regular'));
    $('#edit_Storetarget_last_performance').val($(button).attr('data-action-storetarget_lastmonth_performance'));

    $("#currentinventory_update_button").attr('disabled', 'disabled');
    $("#currentinventory_delete_button").attr('disabled', 'disabled');
    $("#currentinventory_edit_button").removeAttr('disabled', 'disabled');
  }
   function enable_editing(){
    // $('#Storetarget_month').removeAttr('disabled');
    $('#Storetarget_original').removeAttr('disabled');
    $('#Storetarget_fores').removeAttr('disabled');
    $('#Storetarget_average').removeAttr('disabled');
    $("#currentinventory_update_button").removeAttr('disabled');
    $("#currentinventory_delete_button").removeAttr('disabled');
    $("#currentinventory_edit_button").attr('disabled', 'disabled');

    $("#edit_Storetarget_last_sales").removeAttr('disabled');
    $("#edit_Storetarget_last_walkin").removeAttr('disabled');
    $("#edit_Storetarget_last_regular").removeAttr('disabled');
    $("#edit_Storetarget_target_walkin").removeAttr('disabled');
    $("#edit_Storetarget_target_regular").removeAttr('disabled');
    $("#edit_Storetarget_last_performance").removeAttr('disabled');
  }
  
  function disable_editing(){
    $("#Storetarget_month").attr('disabled', 'disabled');
    $("#Storetarget_original").attr('disabled', 'disabled');
    $("#Storetarget_fore").attr('disabled', 'disabled');
    $("#Storetarget_average").attr('disabled', 'disabled');
    $("#Storetarget_month").attr('disabled', 'disabled');
    $("#currentinventory_update_button").attr('disabled', 'disabled');

    $("#edit_Storetarget_last_sales").attr('disabled', 'disabled');
    $("#edit_Storetarget_last_walkin").attr('disabled', 'disabled');
    $("#edit_Storetarget_last_regular").attr('disabled', 'disabled');
    $("#edit_Storetarget_target_walkin").attr('disabled', 'disabled');
    $("#edit_Storetarget_target_regular").attr('disabled', 'disabled');
    $("#edit_Storetarget_last_performance").attr('disabled', 'disabled');
  }
  function delete_record(){

  if (confirm('Are you sure you want to delete this user?')) {

    $.ajax({

      url: 'salesforecast/delete_user/' + $('#storetarget_id').val(),

      success: function () {

        $('#currentinventory_function').text('deleted');

        $('#currentinventory_alert_success').show(400);

        $('#currentinventory_form').hide();

        $('#currentinventory_modal').find('[class="modal-footer"]').hide();

        compute();

      }

    })

  }

}
function removeCommas(x){
  var myStr = x;
  var newStr = myStr.replace(/,/g, '');
  
  return newStr;  
}

  function update_currentinventory(){
  
    var storetarget_id = $('#storetarget_id').val();
  
    var storetarget_month = $('#Storetarget_month').val();
    var storetarget_original = $('#Storetarget_original').val();
    var Storetarget_fore = $('#Storetarget_fores').val();

    var Storetarget_average = $('#Storetarget_average').val();

    var st_last_sales = $('#edit_Storetarget_last_sales').val();
    var st_last_walkin = $('#edit_Storetarget_last_walkin').val();
    var st_last_regular = $('#edit_Storetarget_last_regular').val();
    var st_target_walkin = $('#edit_Storetarget_target_walkin').val();
    var st_target_regular = $('#edit_Storetarget_target_regular').val();
    var st_last_performance = $('#edit_Storetarget_last_performance').val();
  
   
      $.ajax({
        url: 'salesforecast/update_storetarget',
        method: 'POST',
        data: {'storetarget_id': storetarget_id, 
        'storetarget_month': storetarget_month, 
        'storetarget_original': storetarget_original,
         'Storetarget_forecast': Storetarget_fore, 
         'Storetarget_average': Storetarget_average,
        
         'st_last_sales': st_last_sales,
         'st_last_walkin': st_last_walkin,
         'st_last_regular': st_last_regular,
         'st_target_walkin': st_target_walkin,
         'st_target_regular': st_target_regular,
         'st_last_performance': st_last_performance,
        },
        success: function(){
          $('#currentinventory_alert_success').show(400);
          $('#currentinventory_modal').find('.modal-footer').hide();
          $('#currentinventory_function').text('updated');
          $('#currentinventory_form').hide();
          compute();
        }
      })
    
  }
function thismonth(x)
{

var today = new Date();
  let d = new Date(today);
  d.setMonth(today.getMonth() +1)
  d.setDate(0);
  d.setHours(23);
  d.setMinutes(59);
  d.setSeconds(59);
let start_date = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" +01 

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let selected_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" +01
  
  
  if (start_date==selected_date)
  {
    return true;
  }
  else if(start_date>selected_date)
  {
      return false;

  }

}
function storetargetsum(x)
{
var sum=sum+x;
return sum;

}
function getendmonth(x)
{

  var endday=new Date (x);
  let end = new Date(x);
  end.setMonth(endday.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  let formatted_date = end.getFullYear() + "-" + (end.getMonth() + 1) + "-" + end.getDate()
  return formatted_date;

}
function monthlytrendingpercent(x,y)
{
  var monthlypercent=(x/y)*100;

  
  return Number(monthlypercent).toFixed(2).concat("%");


}
function trendingpercent(x,y)
{
  var trendingpercent=(y/x)*100;

  
  return Number(trendingpercent).toFixed(2).concat("%");


}
function trending(x,y)
{
  if(thismonth(y)==true)
  {
  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/daytodayint)*endday ;
  return trending.toFixed(2);
  }
else
{
  var today = new Date();
  let end = new Date(today);
  end.setMonth(today.getMonth() +1)
  end.setDate(0);
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  
  var daytoday = String(today.getDate()).padStart(2, '0');
  var endday = String(end.getDate()).padStart(2, '0');
  var daytodayint=parseFloat(daytoday);
  var trending=(x/endday)*endday ;
  return trending.toFixed(2);

}
function numberWithCommas(x) {

    var parts = x.toString().split('.');

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

    return parts.join('.');

    

}// end of function



}
function refresh(){

  var totalsales_table = $('#totalsales_table').DataTable({

    "dom" : 'Brtip',

    "bDestroy" : true,

    "aaData": null,

    "buttons" : ['pdf'],

    'bAutoWidth': false,

    'columnDefs': [

        { targets: [1,2,3,4],

          className: "align-right" }

    ]

    })



   totalsales_table.clear();

   totalsales_table.draw();

  

   $('#totalsales_payment').text("");

   $('#total_cash').text("");

   $('#total_card').text("");

   $('#total_gc').text("");

}
$( "#Storetarget_originals" ).focus(function() {
  $( "#commaseparated" ).attr('style','display:block; text-align: right;')
 
});
$("#Storetarget_originals").focusout(function(){
  $( "#commaseparated" ).attr('style','display:none; text-align: right;')
});
$( "#Storetarget_averages" ).focus(function() {
  $( "#commaseparatedaverage" ).attr('style','display:block; text-align: right;')
 
});
$("#Storetarget_averages").focusout(function(){
  $( "#commaseparatedaverage" ).attr('style','display:none; text-align: right;')
});
$( "#Storetarget_last_sales" ).focus(function() {
  $( "#commaseparated_last_year" ).attr('style','display:block; text-align: right;')
 
});
$("#Storetarget_last_sales").focusout(function(){
  $( "#commaseparated_last_year" ).attr('style','display:none; text-align: right;')
});
$( "#Storetarget_last_walkin" ).focus(function() {
  $( "#commaseparated_last_walkin" ).attr('style','display:block; text-align: right;')
 
});
$("#Storetarget_last_walkin").focusout(function(){
  $( "#commaseparated_last_walkin" ).attr('style','display:none; text-align: right;')
});
$( "#Storetarget_last_regular" ).focus(function() {
  $( "#commaseparated_last_regular" ).attr('style','display:block; text-align: right;')
 
});
$("#Storetarget_last_regular").focusout(function(){
  $( "#commaseparated_last_regular" ).attr('style','display:none; text-align: right;')
});
function add_storetarget(){

  $('#add_user_button').attr('disabled', 'disabled');

  $('#add_user_button').text('Adding...');

   var loading = '<div class="loading small"></div>';

  // $('#add_user_button').append(loading);

  var brand_id = $('#add_brand').val();

  var branch_id = $('#add_branches').val();

  
  var storetarget_month = $('#storetarget_month_and_year').val()

  var storetarget_original = $('#Storetarget_originals').val();
  var storetarget_updatedsalesforecast = $('#Storetarget_fore').val();

  var storetarget_averagesalesperformance = $('#Storetarget_averages').val();
  var storetarget_lastmonth_performance = $('#Storetarget_last_performance').val();
  var last_year_sales = $('#Storetarget_last_sales').val();
  var last_year_walkin = $('#Storetarget_last_walkin').val();
  var last_year_regular = $('#Storetarget_last_regular').val();
  var target_walkin = $('#Storetarget_target_walkin').val();
  var target_regular = $('#Storetarget_target_regular').val();
  var target_walkin = target_walkin.replace(/,/g, "");
  var target_regular = target_regular.replace(/,/g, "");
  var storetarget_updatedsalesforecast = storetarget_updatedsalesforecast.replace(/,/g, "");
  var storetarget_lastmonth_performance = storetarget_lastmonth_performance.replace(/,/g, "");
  var storetarget_averagesalesperformance = storetarget_averagesalesperformance.replace(/,/g, "");
 
  if(!branch_id || !storetarget_month ){
    
    alert('All fields are required!');

    $('#add_user_button').removeAttr('disabled');

    $('#add_user_button').text('Add');

    
          
     $('.loading').remove();

  }

  else{

    $.ajax({

      url: 'salesforecast/add_storetarget',

      method: 'POST',

      data: {
          branch_id: branch_id,
          storetarget_month:storetarget_month,
          storetarget_original:storetarget_original,
          storetarget_updatedsalesforecast:storetarget_updatedsalesforecast,
          storetarget_averagesalesperformance:storetarget_averagesalesperformance,

          storetarget_lastmonth_performance:storetarget_lastmonth_performance,
          last_year_sales:last_year_sales,          
          last_year_walkin:last_year_walkin,
          last_year_regular:last_year_regular,
          target_walkin:target_walkin,
          target_regular:target_regular

        
        
        },

      success: function(data){

        $('#add_user_button').removeAttr('disabled');

        $('#add_user_button').text('Add');
        
        $('#add_user_alert_success').show(400);

     
       
        console.log($('#add_brand').val());
       

        var selected_date = $('#month_and_year').find('option[value="' + $('#storetarget_month_and_year').val() + '"]');

        $('#month_and_year').val(selected_date.attr('value')).trigger('change');
       

        $('#add_user_alert_success').show(400);
        $('#add_user_modal').find('.modal-footer').hide();
        $(".modal-backdrop.in").hide();
        $('#add_user_form').hide();
        $('#add_user_modal').hide();

        $('.loading').remove();
        compute();
        }

    })

  }

}

