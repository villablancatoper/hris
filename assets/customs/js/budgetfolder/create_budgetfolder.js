$(function(){
    var consumables = $('#consumables_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1, 2, 3, 4, 5, 6], className: "text-center" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    var otcs_table = $('#otcs_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1, 2, 3, 4, 5, 6], className: "text-center" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    $('.select2').select2();
})

$('input[id^="consumables_current_qty"]').keyup(function(event) {

    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault(); //stop character from entering input
    }
    if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8)){

        var $order_sum = 0;
        
        $('input[class^="consumables-order-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $order_sum+=parseFloat($value);              
            }
        });
    
        $('#consumables_order_qty_total').val($order_sum.toFixed(2));

        // Current Inventory

        var $current_sum = 0;

         var multiplier = $('#multiplier').val();
        
        var id = this.id;
        
        var id_number = id.replace(/[^0-9]/gi, '');
        
        var total_amount = (this.value * $(this).attr('data-item-cost')).toFixed(2);
        
        $('.consumables-total-amount-'+id_number).val(total_amount);
        
        $('input[class^="consumables-total-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $current_sum+=parseFloat($value);              
            }
        });
    
        $('#consumables_current_qty_total').val($current_sum.toFixed(2));

        var max_allowance = ((($('#sales_forecast').val()*0.94)*multiplier) - $current_sum).toFixed(2);

        $('#consumables_maximum_allowance').val(max_allowance);

        $('#consumables_over_under').val(($('#consumables_maximum_allowance').val() - $order_sum).toFixed(2));

    }
}); 

$('input[id^="consumables_order_qty"]').keyup(function(event) {

    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault(); //stop character from entering input
    }
    if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8)){

        // Order Quantity

        var $order_sum = 0;
        
        var id = this.id;
        
        var id_number = id.replace(/[^0-9]/gi, '');
        
        var total_amount = (this.value * $(this).attr('data-item-cost')).toFixed(2);
        
        $('.consumables-order-amount-'+id_number).val(total_amount);
        
        $('input[class^="consumables-order-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $order_sum+=parseFloat($value);              
            }
        });
    
        $('#consumables_order_qty_total').val($order_sum.toFixed(2));

        $('#consumables_over_under').val(($('#consumables_maximum_allowance').val() - $order_sum).toFixed(2));

    }
}); 

$('input[id^="otcs_current_qty"]').keyup(function(event) {

    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault(); //stop character from entering input
    }
    if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8)){

        var $order_sum = 0;
        
        $('input[class^="otcs-order-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $order_sum+=parseFloat($value);              
            }
        });
    
        $('#otcs_order_qty_total').val($order_sum.toFixed(2));

        // Current Inventory

        var $current_sum = 0;

         var multiplier = $('#multiplier').val();
        
        var id = this.id;
        
        var id_number = id.replace(/[^0-9]/gi, '');
        
        var total_amount = (this.value * $(this).attr('data-item-cost')).toFixed(2);
        
        $('.otcs-total-amount-'+id_number).val(total_amount);
        
        $('input[class^="otcs-total-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $current_sum+=parseFloat($value);              
            }
        });
    
        $('#otcs_current_qty_total').val($current_sum.toFixed(2));

        var max_allowance = ((($('#sales_forecast').val()*0.94)*multiplier) - $current_sum).toFixed(2);

        $('#otcs_maximum_allowance').val(max_allowance);

        $('#otcs_over_under').val(($('#otcs_maximum_allowance').val() - $order_sum).toFixed(2));

    }
}); 

$('input[id^="otcs_order_qty"]').keyup(function(event) {

    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
        event.preventDefault(); //stop character from entering input
    }
    if((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || (event.keyCode == 8)){

        // Order Quantity

        var $order_sum = 0;
        
        var id = this.id;
        
        var id_number = id.replace(/[^0-9]/gi, '');
        
        var total_amount = (this.value * $(this).attr('data-item-cost')).toFixed(2);
        
        $('.otcs-order-amount-'+id_number).val(total_amount);
        
        $('input[class^="otcs-order-amount"]').each(function(){
            var $value = this.value; 
            if($value){
                $order_sum+=parseFloat($value);              
            }
        });
    
        $('#otcs_order_qty_total').val($order_sum.toFixed(2));

        $('#otcs_over_under').val(($('#otcs_maximum_allowance').val() - $order_sum).toFixed(2));

    }
}); 

function create_budgetfolder(){

    var otcs_order_qty_total = eval($('#otcs_order_qty_total').val());
    var otcs_maximum_allowance = parseFloat($('#otcs_maximum_allowance').val());

    var consumables_order_qty_total = eval($('#consumables_order_qty_total').val());
    var consumables_maximum_allowance = parseFloat($('#consumables_maximum_allowance').val());
    var auto_order = parseFloat($('#auto_order').val())
    var consumables_total = consumables_order_qty_total + auto_order;

    if (consumables_order_qty_total > consumables_maximum_allowance && otcs_order_qty_total > otcs_maximum_allowance) {
        alert("Manual order of consumables and OTC must not exceed their respective allocated allowance");
    } else if (otcs_order_qty_total > otcs_maximum_allowance){
        alert("Manual order of OTC must not exceed the allocated allowance");
    } else if (consumables_order_qty_total > consumables_maximum_allowance){
        alert("Manual order of consumables must not exceed the allocated allowance");

    } else {
        
        if(confirm('Are you sure you want to submit?')){
            
            $('#loading_modal').modal('show');
    
            var consumables = [];
            $("#consumables_table").find("input[id^='consumables_order_qty[']").each(function(){ 
                if(eval($(this).val() > 0)){
                    var element_id = this.id.replace(/[^0-9]/gi, '');
                    var item_supplier = $(this).attr("data-item-supplier");
                    var item_cost = $(this).attr("data-item-cost");
                    var id = parseInt(element_id, 10);
                   
                    var temp = {
                        'currentinventory_id':id, 
                        'order_quantity':$(this).val(),
                        'item_cost': item_cost,
                        'item_supplier' : item_supplier
                    };
                    consumables.push(temp);
                }
            });
    
            var otcs = [];
            $("#otcs_table").find("input[id^='otcs_order_qty[']").each(function(){ 
                if(eval($(this).val() > 0)){
                    var element_id = this.id.replace(/[^0-9]/gi, '');
                    var id = parseInt(element_id, 10);
    
                    var temp = {
                        'currentinventory_id':id, 
                        'order_quantity':$(this).val(),
                    };
                    otcs.push(temp);
                }
            });
                    $('#loading_modal').modal('hide');
        
            $.ajax({
                url: 'create_budgetfolder',
                method: 'POST',
                data: {
                    'user_id': $('#user_id').val(),
                    'branch_id': $('#branch_id').val(),
                    'budgetfolder_budgetdate': $('#budget_date').val(),
                    'budgetfolder_materialbeginningtotal': $('#materials_beginning').val(),
                    'budgetfolder_materialordertotal': consumables_total,
                    'budgetfolder_materialmaxallowance': $('#material_allowance').val(),
                    'budgetfolder_otcbeginningtotal': $('#otcs_current_qty_total').val(),
                    'budgetfolder_otcordertotal': $('#otcs_order_qty_total').val(),
                    'budgetfolder_otcmaxallowance': $('#otc_allowance').val(),
                    'consumables': consumables,
                    'otcs': otcs
                },
                success: function(){
                    $('#loading_modal').modal('hide');
                    alert($('#branch_name').text()+' Budget Folder has been successfully submitted!');
                    history.back();
                }
            })
        }
    }




}
