$(function(){

    var items_per_supplier = $('#items_per_supplier').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });
    
    $('.select2').select2();

    var table_name = "'excel_table'";
    var book_name = "'"+$('#folders').val()+"'";

    // var excel_button = '<div class="row"><div class="col-md-12"><button class="btn btn-default pull-right" onclick="export_excel('+table_name+', '+book_name+')">Excel</button></div></div>';

    // $('#items_per_supplier_wrapper').prepend(excel_button);

})


// function get_test(){

//     var month = $('#month_and_year').val();
//     var folder = $('#folders').val();
//     var item_supplier = $('#item_suppliers').val()
//     var error = '';

//     $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).show();

//     if((!month || !folder) || !item_supplier){
//         if(!month){
//             error += 'Month and Year is required!\n';
//         }
//         if(!folder){
//             error += 'Budget Folder Name is required!\n';
//         }
//         if(!item_supplier){
//             error += 'Supplier is required!\n';
//         }

//         alert(error);

//         $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();
//     }
//     else{
//         var item_supplier_name = $('#item_suppliers').val();
//         var categoy_name = $('#item_suppliers option:selected').attr('data-category');

//         $('#items_per_supplier').removeAttr('style');
//         $('#excel_table').remove();

//         $.ajax({
//             url: 'get_test',
//             method: 'GET',
//             data:{'month': month, 'folder': folder, 'item_supplier_name': item_supplier_name, 'category_name': categoy_name,},
//             success: function(data){
//                 var items_per_supplier = JSON.parse(data);

//                 var table = '<table id="excel_table" class="table-hover" style="width: 100%; max-width: 100%; margin-bottom: 20px; border-spacing: 0; border-collapse: collapse; box-sizing: border-box; display: table; border-color: black;">'

//                 var tr = '<thead><tr><th><p style="text-align: left !important;">Budget Folder Month: </p> </th><th><p style="text-align: left !important;">'+items_per_supplier['month_and_year']+'</p></th></tr><tr><th><p style="text-align: left !important;">Supplier Name: </p> </th><th><p style="text-align: left !important;">'+item_supplier+'</p></th></tr><tr><th><p style="text-align: left !important;">Budget Folder Name: </p> </th><th><p style="text-align: left !important;">'+folder+'</p></th></tr> <tr><th>&nbsp;</th></tr><tr><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Supplier</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Item Name</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Shade</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Color</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Model</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Unit Cost</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">UOM</th>';

//                 var tbody = '<tbody>'

//                 var total_quantity = null;

//                 var total_amount = null;

//                 for(let i = 0; i < items_per_supplier['branches'].length; i++){
//                     tr += '<th colspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['branches'][i].branch_name+'</th>';
//                 }

//                 tr += '<th colspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">TOTAL</th></tr>';
                
//                 tr += '<tr>';

//                 for(let i = 0; i < items_per_supplier['branches'].length; i++){
//                     tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Qty</th>';
//                     tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Amount</th>';
//                 }

//                 tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Qty</th><th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Amount</th>'

//                 tr += '</tr></thead>';

//                 if(typeof items_per_supplier['materialorders'] != 'undefined'){

//                     var totals = {};
//                     var grand_total = 0;

//                     if(items_per_supplier['materialorders'].length > 0){


//                         for(let i = 0; i < items_per_supplier['materialorders'].length; i++){
//                             total_quantity = 0;
//                             total_amount = 0;

//                             var branch = items_per_supplier['materialorders'][i].branch_name;
                            
//                             tbody += '<tr>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_supplier ? items_per_supplier['materialorders'][i].item_supplier : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_name ? items_per_supplier['materialorders'][i].item_name : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_shade ? items_per_supplier['materialorders'][i].item_shade : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_color ? items_per_supplier['materialorders'][i].item_color : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_model ? items_per_supplier['materialorders'][i].item_model : '' )+'</td>';
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_cost ? items_per_supplier['materialorders'][i].item_cost : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_sapuom ? items_per_supplier['materialorders'][i].item_sapuom : '' )+'</td>';

//                             for(let t = 0; t < items_per_supplier['branches'].length; t++ ){
//                                 totals[branch] = [];
//                             }
    
//                             for(let j = 0; j < items_per_supplier['branches'].length; j++){
    
//                                 var amount = eval(items_per_supplier['materialorders'][i].item_cost) * eval(items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity);
                                
//                                 if(items_per_supplier['branches'][j].branch_name == items_per_supplier['materialorders'][i].branch_name){
    
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity+'</td>';
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(amount.toFixed(2))+'</td>';
//                                     total_quantity = eval(total_quantity) + eval(items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity);
//                                     total_amount = eval(total_amount) + eval(amount);

//                                     grand_total = grand_total + amount;

//                                 }
//                                 else{
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                                 }
//                                 // totals[branch].push(amount)
//                                 console.log(amount)
//                             }
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+total_quantity.toFixed(2)+'</td>';
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(total_amount.toFixed(2))+'</td>';
//                             tbody += '</tr>';
//                         }
    
//                         tbody += '<tr>';
//                         tbody += '<td colspan="7" style="background: yellow !important; text-align: center; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">TOTAL</td>';

//                         for(let j = 0; j < items_per_supplier['branches'].length; j++){
//                             tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                             tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;"></td>';
//                         }

//                         tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                         tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">'+numberWithCommas(grand_total.toFixed(2))+'</td>';

//                         tbody += '</tr>';

//                         // totals[branch].push(12)

//                         console.log(totals)
//                     }
//                     else{
//                         tbody += '<tr><td colspan="9" class="text-center">No data available.</td></tr>';
//                     }

//                 }

//                 if(typeof items_per_supplier['otcorders'] != 'undefined'){

//                     var totals = {};
//                     var grand_total = 0;

//                     if(items_per_supplier['otcorders'].length > 0){


//                         for(let i = 0; i < items_per_supplier['otcorders'].length; i++){
//                             total_quantity = 0;
//                             total_amount = 0;

//                             var branch = items_per_supplier['otcorders'][i].branch_name;
                            
//                             tbody += '<tr>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_supplier ? items_per_supplier['otcorders'][i].item_supplier : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_name ? items_per_supplier['otcorders'][i].item_name : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_shade ? items_per_supplier['otcorders'][i].item_shade : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_color ? items_per_supplier['otcorders'][i].item_color : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_model ? items_per_supplier['otcorders'][i].item_model : '' )+'</td>';
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_cost ? items_per_supplier['otcorders'][i].item_cost : '' )+'</td>';
//                             tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_sapuom ? items_per_supplier['otcorders'][i].item_sapuom : '' )+'</td>';

//                             for(let t = 0; t < items_per_supplier['branches'].length; t++ ){
//                                 totals[branch] = 0;
//                             }
    
//                             for(let j = 0; j < items_per_supplier['branches'].length; j++){
    
//                                 if(items_per_supplier['branches'][j].branch_name == items_per_supplier['otcorders'][i].branch_name){
//                                     var amount = eval(items_per_supplier['otcorders'][i].item_cost) * eval(items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity);
    
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity+'</td>';
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(amount.toFixed(2))+'</td>';
//                                     total_quantity = eval(total_quantity) + eval(items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity);
//                                     total_amount = eval(total_amount) + eval(amount);

//                                     grand_total = grand_total + amount;
//                                 }
//                                 else{
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                                     tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                                 }
//                             }
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+total_quantity.toFixed(2)+'</td>';
//                             tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(total_amount.toFixed(2))+'</td>';
//                             tbody += '</tr>';
//                         }
    
//                         tbody += '<tr>';
//                         tbody += '<td colspan="7" style="background: yellow !important; text-align: center; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">TOTAL</td>';

//                         for(let j = 0; j < items_per_supplier['branches'].length; j++){
//                             tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                             tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;"></td>';
//                         }

//                         tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
//                         tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">'+numberWithCommas(grand_total.toFixed(2))+'</td>';

//                         tbody += '</tr>';
//                     }
//                     else{
//                         tbody += '<tr><td colspan="9" class="text-center">No data available.</td></tr>';
//                     }

//                 }

//                 tbody += '</tbody>'

//                 table += (tr + tbody) + '</table>';

//                 $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();

//                 $('#items_per_supplier').attr('style', 'display: none;');
                
//                 $('#items_per_supplier_wrapper').append(table);

//             }
//         })
//     }

// }

function get_budgetfolder_sc(){

    var month = $('#month_and_year').val();
    var folder = $('#folders').val();
    var item_supplier = $('#item_suppliers').val()
    var error = '';
    
    $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).show();

    if(!month){
        if(!month){
            error += 'Month and Year is required!\n';
        }
        alert(error);

        $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();
    }
    else{
        var item_supplier_name = $('#item_suppliers').val();
        var categoy_name = $('#item_suppliers option:selected').attr('data-category');

        $('#items_per_supplier').removeAttr('style');
        $('#excel_table').remove();

        $.ajax({
            url: 'get_budgetfolder_sc',
            method: 'GET',
            data:{'month': month, 'folder': folder, 'item_supplier_name': item_supplier_name, 'category_name': categoy_name,},
            success: function(data){
                var items_per_supplier = JSON.parse(data);

                var table = '<table id="excel_table" class="table-hover" style="width: 100%; max-width: 100%; margin-bottom: 20px; border-spacing: 0; border-collapse: collapse; box-sizing: border-box; display: table; border-color: black;">'

                var tr = '<thead><tr><th><p style="text-align: left !important;">Budget Folder Month: </p> </th><th><p style="text-align: left !important;">'+items_per_supplier['month_and_year']+'</p></th></tr><tr><th><p style="text-align: left !important;">Supplier Name: </p> </th><th><p style="text-align: left !important;">'+item_supplier+'</p></th></tr><tr><th><p style="text-align: left !important;">Budget Folder Name: </p> </th><th><p style="text-align: left !important;">'+folder+'</p></th></tr> <tr><th>&nbsp;</th></tr><tr><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Supplier</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Item Name</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Shade</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Color</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Model</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">Unit Cost</th><th rowspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">UOM</th>';

                var tbody = '<tbody>'

                var total_quantity = null;

                var total_amount = null;

                for(let i = 0; i < items_per_supplier['branches'].length; i++){
                    tr += '<th colspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['branches'][i].branch_name+'</th>';
                }

                tr += '<th colspan="2" style="padding: 0px 5px 0px 5px; border: 1px solid black;">TOTAL</th></tr>';
                
                tr += '<tr>';

                for(let i = 0; i < items_per_supplier['branches'].length; i++){
                    tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Qty</th>';
                    tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Amount</th>';
                }

                tr += '<th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Qty</th><th style="padding: 0px 5px 0px 5px; border: 1px solid black;">Amount</th>'

                tr += '</tr></thead>';

                if(typeof items_per_supplier['materialorders'] != 'undefined'){

                    var totals = {};
                    var grand_total = 0;

                    if(items_per_supplier['materialorders'].length > 0){


                        for(let i = 0; i < items_per_supplier['materialorders'].length; i++){
                            total_quantity = 0;
                            total_amount = 0;

                            var branch = items_per_supplier['materialorders'][i].branch_name;
                            
                            tbody += '<tr>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_supplier ? items_per_supplier['materialorders'][i].item_supplier : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_name ? items_per_supplier['materialorders'][i].item_name : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_shade ? items_per_supplier['materialorders'][i].item_shade : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_color ? items_per_supplier['materialorders'][i].item_color : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_model ? items_per_supplier['materialorders'][i].item_model : '' )+'</td>';
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_cost ? items_per_supplier['materialorders'][i].item_cost : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['materialorders'][i].item_sapuom ? items_per_supplier['materialorders'][i].item_sapuom : '' )+'</td>';

                            for(let t = 0; t < items_per_supplier['branches'].length; t++ ){
                                totals[branch] = [];
                            }
    
                            for(let j = 0; j < items_per_supplier['branches'].length; j++){
    
                                var amount = eval(items_per_supplier['materialorders'][i].item_cost) * eval(items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity);
                                
                                if(items_per_supplier['branches'][j].branch_name == items_per_supplier['materialorders'][i].branch_name){
    
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity+'</td>';
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(amount.toFixed(2))+'</td>';
                                    total_quantity = eval(total_quantity) + eval(items_per_supplier['materialorders'][i].budgetfoldermaterialorder_quantity);
                                    total_amount = eval(total_amount) + eval(amount);

                                    grand_total = grand_total + amount;

                                }
                                else{
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                                }
                                // totals[branch].push(amount)
                                console.log(amount)
                            }
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+total_quantity.toFixed(2)+'</td>';
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(total_amount.toFixed(2))+'</td>';
                            tbody += '</tr>';
                        }
    
                        tbody += '<tr>';
                        tbody += '<td colspan="7" style="background: yellow !important; text-align: center; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">TOTAL</td>';

                        for(let j = 0; j < items_per_supplier['branches'].length; j++){
                            tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                            tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;"></td>';
                        }

                        tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                        tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">'+numberWithCommas(grand_total.toFixed(2))+'</td>';

                        tbody += '</tr>';

                        // totals[branch].push(12)

                        console.log(totals)
                    }
                    else{
                        tbody += '<tr><td colspan="9" class="text-center">No data available.</td></tr>';
                    }

                }

                if(typeof items_per_supplier['otcorders'] != 'undefined'){

                    var totals = {};
                    var grand_total = 0;

                    if(items_per_supplier['otcorders'].length > 0){


                        for(let i = 0; i < items_per_supplier['otcorders'].length; i++){
                            total_quantity = 0;
                            total_amount = 0;

                            var branch = items_per_supplier['otcorders'][i].branch_name;
                            
                            tbody += '<tr>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_supplier ? items_per_supplier['otcorders'][i].item_supplier : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_name ? items_per_supplier['otcorders'][i].item_name : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_shade ? items_per_supplier['otcorders'][i].item_shade : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_color ? items_per_supplier['otcorders'][i].item_color : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_model ? items_per_supplier['otcorders'][i].item_model : '' )+'</td>';
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_cost ? items_per_supplier['otcorders'][i].item_cost : '' )+'</td>';
                            tbody += '<td style="padding: 0px 5px 0px 5px; border: 1px solid black;">'+(items_per_supplier['otcorders'][i].item_sapuom ? items_per_supplier['otcorders'][i].item_sapuom : '' )+'</td>';

                            for(let t = 0; t < items_per_supplier['branches'].length; t++ ){
                                totals[branch] = 0;
                            }
    
                            for(let j = 0; j < items_per_supplier['branches'].length; j++){
    
                                if(items_per_supplier['branches'][j].branch_name == items_per_supplier['otcorders'][i].branch_name){
                                    var amount = eval(items_per_supplier['otcorders'][i].item_cost) * eval(items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity);
    
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity+'</td>';
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(amount.toFixed(2))+'</td>';
                                    total_quantity = eval(total_quantity) + eval(items_per_supplier['otcorders'][i].budgetfolderotcorder_quantity);
                                    total_amount = eval(total_amount) + eval(amount);

                                    grand_total = grand_total + amount;
                                }
                                else{
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                                    tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                                }
                            }
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+total_quantity.toFixed(2)+'</td>';
                            tbody += '<td style="text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;">'+numberWithCommas(total_amount.toFixed(2))+'</td>';
                            tbody += '</tr>';
                        }
    
                        tbody += '<tr>';
                        tbody += '<td colspan="7" style="background: yellow !important; text-align: center; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">TOTAL</td>';

                        for(let j = 0; j < items_per_supplier['branches'].length; j++){
                            tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                            tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;"></td>';
                        }

                        tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black;"></td>';
                        tbody += '<td style="background: yellow !important; text-align: right; padding: 0px 5px 0px 5px; border: 1px solid black; font-weight: bold;">'+numberWithCommas(grand_total.toFixed(2))+'</td>';

                        tbody += '</tr>';
                    }
                    else{
                        tbody += '<tr><td colspan="9" class="text-center">No data available.</td></tr>';
                    }

                }

                tbody += '</tbody>'

                table += (tr + tbody) + '</table>';

                $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();

                $('#items_per_supplier').attr('style', 'display: none;');
                
                $('#items_per_supplier_wrapper').append(table);

            }
        })
    }

}

// $('#folders').on('change', function(){

//     var folder = $('#folders').val();

//     $('#item_suppliers').empty();

//     var loading_data = new Option('Loading data...', '', true, true);
//     $("#item_suppliers").append(loading_data).trigger('change');

//     $.ajax({
//         url: 'get_suppliers_by_folder',
//         method: 'GET',
//         data:{'folder': folder},
//         success: function(data){
//             var suppliers = JSON.parse(data);

//             $('#item_suppliers').empty();
//             var default_option = new Option('Select Supplier', '', true, true);
//             $("#item_suppliers").append(default_option).trigger('change');

//             for(let i = 0; i < suppliers.length; i++){
//                 var option = new Option(suppliers[i].item_supplier, suppliers[i].item_supplier, true, true);
//                 // var option = new Option('<option value="'+suppliers[i].item_supplier+'" data-category="'+suppliers[i].category_name+'"></option>');
//                 $("#item_suppliers").append(option).trigger('change');
//                 $("#item_suppliers option:selected").attr('data-category', suppliers[i].category_name);
//             }

//             $('.select2').select2();

//             $("#item_suppliers").val('').trigger('change');
//         }
//     })
// })

function get_folder_costs(){
    $('#suppliers_folders').DataTable().destroy();

    var month = $('#month_and_year').val();
    var year = $('#month_and_year option:selected').attr('data-year');
    
    var batch = $('#month_and_year option:selected').attr('data-batch');

    $('#suppliers_folders_wrapper').empty();

    $('#items_per_supplier_wrapper').removeAttr('style');
    
    $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).show();

    if(!month){
        alert('Month & Year is required.');
        $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();
    }
    else{
        $.ajax({
            url: 'get_folder_costs',
            method: 'GET',
            data: {'month': month, 'batch': batch, 'year': year},
            success: function(data){

                var data  = JSON.parse(data);
                console.log(data)
                var pos = {'ALL FT' : 'FT'+ year + month, 'ALL MX' : 'MX'+ year + month, 'ALL VS' : 'VS'+ year + month, 'ALL FT' : 'FT'+ year + month, 'HS SOUTH' : 'HSS'+ year + month, 'NH NORTH CENTRAL' : 'NHNC'+ year + month, 'NH NORTH EAST' : 'NHNE'+ year + month, 'NH NORTH PROVINCIAL' : 'NHNP'+ year + month, 'NH SOUTH LUZON' : 'NHSL'+ year + month, 'NH SOUTH METRO' : 'NHSM'+ year + month, 'ALL OLL' : 'OLL'+ year + month, 'HB LUZON' : 'HBL'+ year + month, 'HB VISMIN' : 'HBV'+ year + month, 'HS NORTH' : 'HSN'+ year + month, 'HS VISMIN' : 'HSV'+ year + month, 'NH VISMIN' : 'NHV'+ year + month, 'SB LUZON' : 'SBL'+ year + month, 'SB VISAYAS' : 'SBV'+ year + month};

                var table = '<table id="suppliers_folders" class="table table-bordered table-hover">';

                var thead = '<thead><tr><th>Supplier</th>';

                var tbody = '<tbody>'

                // start
                var folders = {};

                for(let i = 0; i < data['folders'].length; i++){
                    thead += '<th>'+data['folders'][i].branch_budgetfolder+'</th>';
                    folders[data['folders'][i].branch_budgetfolder] = {'po': 1, 'total': 0};
                }
                console.log(folders)
                tbody += '<tr>';
                tbody += '<td></td>'
                
                for(let x = 0; x < data['folders'].length; x++, counter++){
                    tbody += '<td><div style="display: block !important;" class="text-center"><button style="margin-bottom: 5px" class="btn btn-primary btn-sm btn-flat" data-folder="'+data['folders'][x].branch_budgetfolder+'" data-month="'+month+'" data-batch="'+batch+'" data-year="'+year+'" onclick="view_budgetfolder_summary(this)">Top Sheet</button></div><div style="display: block !important;" class="text-center"><button class="btn btn-primary btn-sm btn-flat" data-folder="'+data['folders'][x].branch_budgetfolder+'" data-month="'+month+'" data-batch="'+batch+'" data-year="'+year+'" onclick="view_budgetfolder_inventory_quantity(this)">Inventory</button></div></td>';
                }

                tbody += '</tr>'
                
                var counter = 0;

                for(let i = 0; i < data['suppliers'].length; i++){
                    tbody += '<tr>';
                    tbody += '<td>'+data['suppliers'][i].item_supplier+'</td>';


                    for(let j = 0; j < data['folders'].length; j++, counter++){

                        tbody += '<td style="text-align: right;" data-folder="'+data['folders'][j].branch_budgetfolder+'" data-po="'+(pos[data['folders'][j].branch_budgetfolder] + '-' + ( folders[data['folders'][j].branch_budgetfolder]['po'] < 10 ? '0'+folders[data['folders'][j].branch_budgetfolder]['po'] : folders[data['folders'][j].branch_budgetfolder]['po'] ))+'" data-supplier="'+data['rows'][counter].item_supplier+'" data-month="'+month+'" data-batch="'+batch+'" data-year="'+year+'">'+(data['rows'][counter].total_cost > 0 ? '<div class="td-hack">'+numberWithCommas(data['rows'][counter].total_cost)+'</div>' : '')+'</td>';

                        if(data['rows'][counter].total_cost > 0){
                            folders[data['folders'][j].branch_budgetfolder]['po']++;
                            folders[data['folders'][j].branch_budgetfolder]['total'] += eval(data['rows'][counter].total_cost);
                        }
                    }
                }

                tbody += '</tr>';
                // end

                tbody += '<tr>';
                tbody += '<td style="font-size: 20px; font-weight: bold;">TOTAL</td>';

                for(let j = 0; j < data['folders'].length; j++){
                    tbody += '<td style="text-align: right; font-weight: bold; font-size: 20px;">'+numberWithCommas(folders[data['folders'][j].branch_budgetfolder]['total'].toFixed(2))+'</td>';
                }

                tbody += '</tr>';

                tbody += '</tbody>';

                table += thead;

                table += tbody;

                table += '</table>';

                $('#suppliers_folders_wrapper').append(table);

                $('#items_per_supplier_wrapper').attr('style', 'display: none;');
                $('.dataTables_processing', $('#items_per_supplier').closest('.dataTables_wrapper')).hide();

                $('#suppliers_folders').DataTable({
                    'processing': true,
                
                    'bAutoWidth': false,
                
                    'bSort': false,
            
                    'language': {
            
                        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                
                    },
                    'ordering': false,
            
                    "lengthChange": false,
            
                    "paging": false,
                    
                    "bInfo" : false
                });

                $('td').hover(
                    function() {
                        var overlay = '<div class="overlay">'
                        + '<div class="excel"><button class="btn btn-primary btn-sm btn-flat" onclick="excel(this)" data-folder="'+$(this).attr('data-folder')+'" data-po="'+$(this).attr('data-po')+'" data-supplier="'+$(this).attr('data-supplier')+'" data-month="'+$(this).attr('data-month')+'" data-batch="'+$(this).attr('data-batch')+'"'+' data-year="'+$(this).attr('data-year')+'">Excel</button></div>'
                        + '</div>';

                        $(this).find('.td-hack').append(overlay);
                        $(this).find('.td-hack').attr('style', 'padding-bottom: 10px;');
                    },
                    function() {
                        $(this).find('.overlay').remove();
                    }
                );
                $('td').mouseleave(function(){
                    $('.td-hack').removeAttr('style');
                });
            }
        })
    }
}

function excel(button) {
    var folder = $(button).attr('data-folder');
    var po = $(button).attr('data-po');
    var supplier = encodeURIComponent($(button).attr('data-supplier')).replace("(", "%28").replace(")", "%29");
    var month = $(button).attr('data-month');
    var batch = $(button).attr('data-batch');

    var win = window.open($('#base_url').val()+'budgetfolder/generate_excel/'+folder+'/'+po+'/'+supplier+'/'+month+'/'+batch, '_blank');
    win.focus();
}

function view_budgetfolder_summary(button){
    var folder = $(button).attr('data-folder');
    var month = $(button).attr('data-month');
    var year = $(button).attr('data-year');
    var batch = $(button).attr('data-batch');
    $('#loading_modal').modal('show');

    $('.summary').empty();

    $.ajax({
        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',
        method: 'GET',
        success: function(data){
    
          document.getElementById('current_time_and_date').value = data;
          
          $.ajax({
              url: 'view_budgetfolder_summary',
              method: 'GET',
              data: {'folder': folder, 'month': month, 'batch': batch, 'year': year},
              success: function(data){
                  var summary = JSON.parse(data);
      
                  var header = '<div class="row"><div class="col-md-12"><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month: <span><strong class="budget-folder-month"></strong></span></p></div></div>';
                  var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';
                  var thead = '<thead><tr><th style="border-right: 1px white !important; border-left: 1px white !important; border-top: 1px white !important;"></th>';
                  var tbody = '<tbody>';
                  var original_total = null;
                  var sales_forecast_total = null;
                  var projected_sales_material_total = null;
                  var projected_sales_otc_total = null;
                  var beginning_total = null;
                  var max_allowance_total = null;
                  var material_order_total = null;
                  var material_over_under_total = null;
                  var otc_beginning_total = null;
                  var otc_max_allowance_total = null;
                  var otc_order_total = null;
                  var otc_over_under_total = null;
                  var material_grand_total = null;
                  var otc_grand_total = null;
                  var total_material_cost = null;
                  var total_otc_cost = null;
                  var grand_total_otc_material_cost = null;
                  var grand_total_otc_material_over_under = null;
                  var average_sales_performance_total = null;
                  var material_cost_total = null;

                  var material_multiplier = .05;
                  var otc_multiplier = .06;

                  var projected_sales_multiplier = .94;
        
                  if(folder == 'ALL MX'){
        
                      material_multiplier = .08;
        
                  }
        
                  if(folder == 'SB LUZON' || folder == 'SB VISAYAS'){
        
                      otc_multiplier = .04;
                      projected_sales_multiplier = .96;
        
                  }
      
                  var pos = {'ALL FT' : 'FT'+ year + month, 'ALL MX' : 'MX'+ year + month, 'ALL VS' : 'VS'+ year + month, 'ALL FT' : 'FT'+ year + month, 'HS SOUTH' : 'HSS'+ year + month, 'NH NORTH CENTRAL' : 'NHNC'+ year + month, 'NH NORTH EAST' : 'NHNE'+ year + month, 'NH NORTH PROVINCIAL' : 'NHNP'+ year + month, 'NH SOUTH LUZON' : 'NHSL'+ year + month, 'NH SOUTH METRO' : 'NHSM'+ year + month, 'ALL OLL' : 'OLL'+ year + month, 'HB LUZON' : 'HBL'+ year + month, 'HB VISMIN' : 'HBV'+ year + month, 'HS NORTH' : 'HSN'+ year + month, 'HS VISMIN' : 'HSV'+ year + month, 'NH VISMIN' : 'NHV'+ year + month, 'SB LUZON' : 'SBL'+ year + month, 'SB VISAYAS' : 'SBV'+ year + month};
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      thead += '<th>'+summary['columns'][i].branch_name+'</th>';
                  }
      
                  thead += '<th>TOTAL</th>';
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Original Target</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].original)+'</td>'
                      original_total = eval(original_total) + eval(summary['columns'][i].original);
                  }
      
                  tbody += '<td '+detectNegative(original_total)+'>'+numberWithCommasConvertToPositive(original_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Sales Forecast</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].sales_forecast)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].sales_forecast)+'</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                  }
      
                  tbody += '<td '+detectNegative(sales_forecast_total)+'>'+numberWithCommasConvertToPositive(sales_forecast_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Average Sales Performance</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].average_sales_performance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].average_sales_performance)+'</td>'
                      average_sales_performance_total = eval(average_sales_performance_total) + eval(summary['columns'][i].average_sales_performance);
                  }
      
                  tbody += '<td '+detectNegative(average_sales_performance_total)+'>'+numberWithCommasConvertToPositive(average_sales_performance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month ('+(projected_sales_multiplier*100)+'%) - Materials</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier).toFixed(2))+'</td>'
                      projected_sales_material_total = eval(projected_sales_material_total) + (eval(summary['columns'][i].sales_forecast)  * projected_sales_multiplier);
                  }
      
                  tbody += '<td '+detectNegative(projected_sales_material_total)+'>'+numberWithCommasConvertToPositive(projected_sales_material_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">'+(material_multiplier*100)+'% of Material Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(( (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier) * material_multiplier).toFixed(2))+'</td>'
                      material_cost_total = eval(material_cost_total) + ((eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier) * material_multiplier);
                  }
      
                  tbody += '<td '+detectNegative(material_cost_total)+'>'+numberWithCommasConvertToPositive(material_cost_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Beginning Total</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].beginning)+'</td>'
                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);
                  }
      
                  tbody += '<td '+detectNegative(beginning_total)+'>'+numberWithCommasConvertToPositive(beginning_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Maximum Allowance to Purchase</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_max_allowance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_max_allowance)+'</td>'
                      max_allowance_total = eval(max_allowance_total) + eval(summary['columns'][i].material_max_allowance);
                  }
      
                  tbody += '<td '+detectNegative(max_allowance_total)+'>'+numberWithCommasConvertToPositive(max_allowance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered Material Total Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_order_total)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_order_total)+'</td>'
                      material_order_total = eval(material_order_total) + eval(summary['columns'][i].material_order_total);
                  }
      
                  tbody += '<td '+detectNegative(material_order_total)+'>'+numberWithCommasConvertToPositive(material_order_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_over_under)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_over_under)+'</td>'
                      material_over_under_total = eval(material_over_under_total) + eval(summary['columns'][i].material_over_under);
                  }
      
                  tbody += '<td '+detectNegative(material_over_under_total)+'>'+numberWithCommasConvertToPositive(material_over_under_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important;">'+Math.round((summary['columns'][i].beginning/summary['columns'][i].sales_forecast)*100)+'%</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);
                  }
      
                  tbody += '<td style="text-align: center !important;">'+Math.round((beginning_total/sales_forecast_total)*100)+'%</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month ('+(otc_multiplier*100)+'%) - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * otc_multiplier).toFixed(2))+'</td>'
                      projected_sales_otc_total = eval(projected_sales_otc_total) + (eval(summary['columns'][i].sales_forecast) * otc_multiplier);
                  }
      
                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">60% of OTC Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6).toFixed(2))+'</td>'
                    //   projected_sales_otc_total = eval(projected_sales_otc_total) + ((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6);
                  }
                  projected_sales_otc_total = eval(projected_sales_otc_total) * .6;
                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Beginning Total</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_beginning)+'</td>'
                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);
                  }
      
                  tbody += '<td '+detectNegative(otc_beginning_total)+'>'+numberWithCommasConvertToPositive(otc_beginning_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Maximum Allowance to Purchase</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_max_allowance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_max_allowance)+'</td>'
                      otc_max_allowance_total = eval(otc_max_allowance_total) + eval(summary['columns'][i].otc_max_allowance);
                  }
      
                  tbody += '<td '+detectNegative(otc_max_allowance_total)+'>'+numberWithCommasConvertToPositive(otc_max_allowance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered OTC Total Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_order_total)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_order_total)+'</td>'
                      otc_order_total = eval(otc_order_total) + eval(summary['columns'][i].otc_order_total);
                  }
      
                  tbody += '<td '+detectNegative(otc_order_total)+'>'+numberWithCommasConvertToPositive(otc_order_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(temp);
      
                      otc_over_under_total = eval(otc_over_under_total) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(otc_over_under_total)+'>'+numberWithCommasConvertToPositive(otc_over_under_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important;">'+Math.round((summary['columns'][i].otc_beginning/summary['columns'][i].sales_forecast)*100)+'%</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);
                  }
      
                  tbody += '<td style="text-align: center !important;">'+Math.round((otc_beginning_total/sales_forecast_total)*100)+'%</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - MATERIALS</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'
                  }
      
                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'
                  tbody += '<td style="font-weight: bold; width: 80px !important; text-align: center !important;">PO NUMBER</td>'
                  tbody += '</tr>'
      
                  var series = 1;
      
                  for(let i = 0; i < summary['side_infos']['material_suppliers'].length; i++){
      
                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['material_suppliers'][i].item_supplier+'</td>'
      
                      material_grand_total = 0;
          
                      for(let j = 0; j < summary['columns'].length; j++){
                          tbody += '<td '+detectNegative(summary['columns'][j]['material_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['material_cost'][i])+'</td>'
                          material_grand_total = eval(material_grand_total) + eval(summary['columns'][j]['material_cost'][i]);
                      }
          
                      tbody += '<td '+detectNegative(material_grand_total)+'>'+numberWithCommasConvertToPositive(material_grand_total.toFixed(2))+'</td>'
      
                      if(material_grand_total > 0){
                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   
                          series++;                
                      }
                      else{
                          tbody += '<td></td>' 
                      }
      
                      tbody += '</tr>'
      
                  }
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL MATERIAL COST</td>'
      
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                          
                      total_material_cost = eval(total_material_cost) + eval(temp);
      
                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(total_material_cost)+'>'+numberWithCommasConvertToPositive(total_material_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'
      
                  var total_over_under = 0;
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['material_max_allowance']) - eval(temp);
      
                      total_over_under = eval(total_over_under) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  if(max_allowance_total > 0 && total_over_under > 0){
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
                  else{
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
                  
                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'
                  }
      
                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'
                  tbody += '<td style="text-align: center !important; font-weight: bold;">PO NUMBER</td>'
                  tbody += '</tr>'
      
                  for(let i = 0; i < summary['side_infos']['otc_suppliers'].length; i++){
      
                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['otc_suppliers'][i].item_supplier+'</td>'
      
                      otc_grand_total = 0;
          
                      for(let j = 0; j < summary['columns'].length; j++){
                          tbody += '<td '+detectNegative(summary['columns'][j]['otc_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['otc_cost'][i])+'</td>'
                          otc_grand_total = eval(otc_grand_total) + eval(summary['columns'][j]['otc_cost'][i]);
                      }
          
                      tbody += '<td '+detectNegative(otc_grand_total)+'>'+numberWithCommasConvertToPositive(otc_grand_total.toFixed(2))+'</td>'
      
                      if(otc_grand_total > 0){
                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   
                          series++;                
                      }
                      else{
                          tbody += '<td></td>' 
                      }
      
                      tbody += '</tr>'
      
                  }
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL OTC COST</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
                          
                      total_otc_cost = eval(total_otc_cost) + eval(temp);
      
                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(total_otc_cost)+'>'+numberWithCommasConvertToPositive(total_otc_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'
      
                  total_over_under = 0;
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(temp);
      
                      total_over_under = eval(total_over_under) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  if(otc_max_allowance_total > 0 && total_over_under > 0){
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
                  else{
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL (MATERIAL & OTC)</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var otc_temp = 0;
                      var material_temp = 0;
                      var total_otc_material_cost = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          otc_temp = eval(otc_temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          material_temp = eval(material_temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                          
                      total_otc_material_cost = eval(material_temp) + eval(otc_temp);
      
                      grand_total_otc_material_cost = eval(grand_total_otc_material_cost) + eval(total_otc_material_cost);
      
                      tbody += '<td '+detectNegative(total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(total_otc_material_cost.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(grand_total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(grand_total_otc_material_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL OVER / UNDER FOR THIS MONTH</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var otc_temp = 0;
                      var material_temp = 0;
                      var otc_over_under = 0;
                      var material_over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          otc_temp = eval(otc_temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          material_temp = eval(material_temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                       
                      otc_over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(otc_temp);
      
                      material_over_under = eval(summary['columns'][i]['material_max_allowance']) - eval(material_temp);
      
                      total_otc_material_over_under = eval(otc_over_under) + eval(material_over_under)
      
                      grand_total_otc_material_over_under = eval(grand_total_otc_material_over_under) + eval(total_otc_material_over_under);
      
                      tbody += '<td '+detectNegative(total_otc_material_over_under)+'>'+numberWithCommasConvertToPositive(total_otc_material_over_under.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(grand_total_otc_material_over_under)+'>'+numberWithCommasConvertToPositive(grand_total_otc_material_over_under.toFixed(2))+'</td>'
                  tbody += '</tr>'
                  
      
      
      
                  thead += '</tr></thead>';
                  tbody += '</tr></tbody>'
      
                  $('.summary').append(header);
                  $('.summary').append(table);
                  $('.summary-table').append(thead);
                  $('.summary-table').append(tbody);
      
                  $('.budget-folder-name').text(summary['side_infos']['budgetfolder'].toUpperCase());
                  $('.budget-folder-month').text(summary['side_infos']['budgetfolder_month'].toUpperCase());
      
                  $('#loading_modal').modal('hide');
      
                  printThis();
              }
          })

        }
    })
    
}

function view_budgetfolder_inventory_quantity(button){
    var folder = $(button).attr('data-folder');
    var month = $(button).attr('data-month');
    var year = $(button).attr('data-year');
    var batch = $(button).attr('data-batch');
    $('#loading_modal').modal('show');

    $('.summary').empty();

    $.ajax({
        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',
        method: 'GET',
        success: function(data){

            document.getElementById('current_time_and_date').value = data;

            $.ajax({
                url: 'view_budgetfolder_inventory',
                method: 'GET',
                data: {'folder': folder, 'month': month, 'batch': batch, 'year': year},
                success: function(data){
        
                    var orders = JSON.parse(data);
                    console.log(orders)
                    orders['branches'].sort(function(a,b) {
                        a = a.toLowerCase();
                        b = b.toLowerCase();
                        if( a == b) return 0;
                        return a < b ? -1 : 1;
                    });
                    var header = '<div class="row"><div class="col-md-12"><p class="top-content header"><span style="padding-left: 88px;"><strong>Beginning Inventory</strong></span></p><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month:<span><strong class="budget-folder-month"></strong></span></p></div></div>';
                    var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';
                    var thead = '<thead><tr><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th>';
                    var tbody = '<tbody>';
        
                    var total_quantity = null;
                    var total_amount = null;
                    var column_total = {};
        
                    for(let i = 0; i < orders['branches'].length; i++){
                        thead += '<th>'+orders['branches'][i]+'</th>'
        
                        column_total[orders['branches'][i]] = [];
        
                        for(let j = 0; j < orders['material_suppliers'].length; j++){
        
                            column_total[orders['branches'][i]][orders['material_suppliers'][j]] = [];
                        }
                    }
        
                    thead += '<th>TOTAL</th>'
        
                    thead += '</tr><tr><th>Item Name</th><th>Shade</th><th>Color</th><th>Model</th><th>UOM</th>';
        
                    for(let i = 0; i < orders['branches'].length; i++){
                        thead += '<th>Quantity</th>'
                    }
        
                    thead += '<th>Quantity</th>'
        
                    var counter = 0;
        
  
                    
                    for(let i = 0; i < orders['materials'].length; i++){
                        total_quantity = null;
                        total_amount = null;
                        tbody += '<tr>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_name+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_shade != null ? orders['materials'][i].item_shade : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_color != null ? orders['materials'][i].item_color : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_model != null ? orders['materials'][i].item_model : '')+'</td>';
                   
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_sapuom != null ? orders['materials'][i].item_sapuom : '')+'</td>';
                            for(let l = 0; l < orders['branches'].length; l++, counter++){   
                                if(orders['columns']['materials'][counter].length > 0){
                                    // column_total[orders['columns']['materials'][counter][0].branch_name][orders['columns']['materials'][counter][0].item_supplier].push(eval(orders['columns']['materials'][counter][0].amount));
                                    tbody += '<td style="padding-left: 3px; text-align: center;">'+orders['columns']['materials'][counter][0].quantity+'</td>';
                                    
                                    total_quantity = eval(total_quantity) + eval(orders['columns']['materials'][counter][0].quantity);
                                   
        
                                }
                                else{
                                    tbody += '<td style="padding-left: 3px;"></td>';
                                   
                                }
        
                            }    
                        var quantity = parseFloat(total_quantity).toFixed(2);
                        if(isNaN(quantity)){
                            tbody += '<td style = "text-align: center;"></td>';
                        }else{
                            tbody += '<td style = "text-align: center;">'+numberWithCommas(quantity)+'</td>';
                        }
                        
                        
        
                        tbody += '</tr>'
        
                        // last_supplier = orders['materials'][i].item_supplier;
                    }
        
        
                    thead += '</tr></thead>';
                    tbody += '</tbody>'
        
        
                    $('.summary').append(header);
                    $('.summary').append(table);
                    $('.summary-table').append(thead);
                    $('.summary-table').append(tbody);
        
                    $('.budget-folder-name').text(orders['folder'].toUpperCase());
                    $('.budget-folder-month').text(orders['month_and_year'].toUpperCase());
        
                    $('#loading_modal').modal('hide');
        
                    printThis();
        
                    $('#loading_modal').modal('hide');
        
                }
            })
        }

    });

}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    
    $printSection.appendChild(domClone);
}

function printThis(){
    printElement(document.getElementById("printThis"));

    window.print();
}