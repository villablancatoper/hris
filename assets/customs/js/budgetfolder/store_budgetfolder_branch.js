$(function(){

    // $('#items_table').Tabledit({
    //     url: 'process_budgetfolder_inventory',
    //     columns: {
    //         identifier: [0, 'currentinventory_id'],                   
    //         editable: [[8, 'currentinventory_quantity']]
    //     },
    //     editmethod: 'post',
    //     // deletemethod: 'post',    
    //     buttons: {
    //         edit: {
    //             class: 'btn btn-sm btn-warning',
    //             html: '<span class="fa fa-edit"></span>',
    //             action: 'edit'
    //         },
    //         // delete: {
    //         //     class: 'btn btn-sm btn-danger',
    //         //     html: '<span class="fa fa-trash"></span>',
    //         //     action: 'delete'
    //         // },
    //         save: {
    //             class: 'btn btn-sm btn-success',
    //             html: 'Save'
    //         },
    //         // confirm: {
    //         //     class: 'btn btn-sm btn-default',
    //         //     html: 'Are you sure?'
    //         // }
    //     },
    //     hideIdentifier: true,
    //     restoreButton: false,
    //     deleteButton: false,
    //     onSuccess: function(data, textStatus, jqXHR){
    //         if(data.action == 'delete'){
    //             $('#'+data.currentinventory_id).remove();
    //         }
    
    //         if(data.action == 'edit'){
    //             $('#'+data.currentinventory_id).removeAttr('class');
    //             $('#'+data.currentinventory_id).attr('class', 'info');
    //         }
    //     }
    // });

    var tbody = $('#items_table tbody').children();

    if(tbody.length == 0){
        $('#items_table tbody').append('<tr><td colspan="9" style="text-align: center;">No data available</td></tr>')
    }

    $('.select2').select2();

})

function submit_budgetfolder(){

    $('#loading_modal').modal('show');
    
    var ids = [];
    $("#items_table").find("input").each(function(){ 
        var temp = {'id':this.id, 'value':$(this).val()};
        ids.push(temp);
    });

    var dateupdated = $('#date_as_of').val();

    if(!dateupdated){
        alert('Date is required!');
    }
    else{

        $.ajax({
            url: 'submit_budgetfolder',
            method: 'POST',
            data: {'ids': ids, 'dateupdated': dateupdated},
            success: function(){
                $('#loading_modal').modal('hide');
                alert('Store Budget Folder Inventory has been successfully submitted!');
                window.location.reload();
            }
        })

    }
}

$('#date_as_of').on('change', function(){

    var date_as_of = $('#date_as_of').val();

    if(date_as_of){

        $.ajax({
            url: 'check_encoding_availability',
            method: 'GET',
            data: {'date_as_of': date_as_of},
            success: function(data){
                console.log(data)
                if(data == 1){
                    $('#content').removeAttr('hidden');
                }
                else{
                    alert('Editing has been locked! Contact MIS for further assistance.');
                }
            }
        })

    }

    
})

  

  
  