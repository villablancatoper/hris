$(function(){

  var items_table = $('#items_table').DataTable({
    'processing': true,
  
    'bAutoWidth': false,

    'bSort': false,

    'columnDefs': [
      { targets: [1, 3, 5, 6, 7], className: "align-right" },
    ],

    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    },
    dom: 'Bfrtip',

    buttons: [{
      extend: 'pdf',
      title: $('#user_branch_name').text()+' | Budget Folder Inventory Sales Report',
      filename: $('#user_branch_name').text()+' (Budget Folder Inventory Sales Report)'
    }],

    "lengthChange": false,

    "paging": false,
    
    "bInfo" : false
  });

  $('.dt-button').addClass('btn btn-default');

  $('.dt-button').text('Save as PDF')

  $('.select2').select2();

})



function generate_budgetfolder(){

  var branch_id = $('#branches').val();

  var branches = $('#branches').select2('data')

  var test = $('#items_table').DataTable();

  test.destroy();

  var items_table = $('#items_table').DataTable({
    'processing': true,
  
    'bAutoWidth': false,

    'bSort': false,

    'columnDefs': [
      { targets: [1, 3, 5, 6, 7], className: "align-right" },
    ],

    'language': {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
  
    },
    dom: 'Bfrtip',

    buttons: [{
      extend: 'pdf',
      title: 'Budget Folder Inventory of '+branches[0].text,
      filename: branches[0].text
    }],

    "lengthChange": false,

    "paging": false,
    
    "bInfo" : false
  });

  $('.dt-button').addClass('btn btn-default');

  $('.dt-button').text('Save as PDF')

  $('.dataTables_processing', $('#items_table').closest('.dataTables_wrapper')).show();

  if(!branch_id){
    alert('Branch is required!')
    $('.dataTables_processing', $('#items_table').closest('.dataTables_wrapper')).hide();
  }
  else{
    $.ajax({
  
      url: 'generate_budgetfolder',
  
      method: 'GET',
  
      data: {'branch_id': branch_id},
  
      success: function(data){
  
        var items = JSON.parse(data);
  
        items_table.clear().draw();
        console.log(items.length);
  
        for(let i = 0; i < items['data'].length; i++){
          items_table.row.add([
  
            items['data'][i].item_name,
  
            numberWithCommas(items['data'][i].currentinventory_quantity),

            items['data'][i].currentinventory_uom,

            numberWithCommas(items['data'][i].item_uomsize),

            items['data'][i].item_sapuom,

            numberWithCommas(items['data'][i].sap_conversion),

            '&#8369;'+numberWithCommas(items['data'][i].item_cost),

            '&#8369;'+numberWithCommas(items['data'][i].total_cost)
    
          ])

        }

        items_table.row.add([
  
          '<h4 class="display-4">Total OTC</h4>',

          '',

          '',

          '',

          '',

          '',

          '',

          '<h4 class="display-4">₱'+numberWithCommas(items['total_otc'])+'</h4>'
  
        ])

        items_table.row.add([
  
          '<h4 class="display-4">Total Consumables</h4>',

          '',

          '',

          '',

          '',

          '',

          '',

          '<h4 class="display-4">₱'+numberWithCommas(items['total_consumables'])+'</h4>'
  
        ])

        items_table.row.add([
  
          '<h4 class="display-4">Grand Total</h4>',

          '',

          '',

          '',

          '',

          '',

          '',

          '<h4 class="display-4">₱'+numberWithCommas(items['grand_total'])+'</h4>'
  
        ])
  
        $('.dataTables_processing', $('#items_table').closest('.dataTables_wrapper')).hide();
        
        // $.ajax({
  
        //   url: 'generate_budgetfolder_total',
      
        //   method: 'GET',
      
        //   data: {'branch_id': branch_id},
      
        //   success: function(data){

        //     var budgetfolder_total = JSON.parse(data);

        //     $('#total_cost').text(budgetfolder_total);

        //     items_table.row.add([
  
        //       '<h4 class="display-4">Grand Total</h4>',
    
        //       '',
  
        //       '',
  
        //       '',
  
        //       '',
  
        //       '',
  
        //       '',
  
        //       '<h4 class="display-4">₱'+numberWithCommas(budgetfolder_total)+'</h4>'
      
        //     ])

        //   }
        
        // });
        
        items_table.draw()
      }
  
    })
  }
}


