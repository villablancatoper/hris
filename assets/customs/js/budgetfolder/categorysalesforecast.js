$(function(){


    var categorysalesforecast_table = $('#categorysalesforecast_table').DataTable({

      'processing': true,

      'bAutoWidth': false,

      'bSort': false, 

      'columnDefs': [

          // { targets: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26], className: "align-right" },
          { targets: [1,2,3, 4, 5], className: "align-center"},
          {targets: [0], className: "align-left"},

      ],

      'scrollX': true,

      'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

      },

      dom: 'Brtip',

      buttons: [{

        extend: 'excel',

        title: 'Category Sales Report | '+$('#current_date').val(),

        filename: $('#user_branch_name').text()+' (Category Sales Report)'

      }],

      'ordering': false,

      "lengthChange": false,

      "paging": false,

      "bInfo" : false

    })


    $('#daterange_btn').daterangepicker(

      {

        ranges: {

          'Today': [moment(), moment()],

          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

          'Last 7 Days': [moment().subtract(6, 'days'), moment()],

          'Last 30 Days': [moment().subtract(29, 'days'), moment()],

          'This Month': [moment().startOf('month'), moment().endOf('month')],


          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        startDate: moment().subtract(29, 'days'),

        endDate: moment()

      },

      function (start, end) {

        $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

        $('#start').val(start.format('YYYY-MM-DD'));

        $('#end').val(end.format('YYYY-MM-DD'));

      }

    )

    $('.dt-button').addClass('btn btn-default');
    $('.dt-button').addClass('pull-right');


    $('.select2').select2();


})

$('#brand').change(function(){

    var brand_id = $('#brand').val();
    var base_url = $('#base_url').val();
    if (brand_id != '') {
      
      $.ajax({
      url: base_url+'report/fetch_services_by_brand_id',
      method: "POST",
      data: {brand_id:brand_id},
      success:function(data){
        $('#service').html(data);
      }

     })


    }

})

$('#modal_brand').change(function(){

    var brand_id = $('#modal_brand').val();
    var base_url = $('#base_url').val();
    if (brand_id != '') {
      
      $.ajax({
      url: base_url+'report/fetch_services_by_brand_id',
      method: "POST",
      data: {brand_id:brand_id},
      success:function(data){
        $('#modal_service').html(data);
        $('#modal_service').append("<option value='OTC'>OTC</option>")

      }

     })


    }

})

function submit_forecast(){
  var brand_id = $('#modal_brand').val();
  var service_id = $('#modal_service').val();
  var percentage = $('#modal_percentage').val();
  var month = $('#modal_month_and_year option:selected').text();
  var year = $('#modal_month_and_year option:selected').attr('data-year');


  if (!brand_id || !service_id || !percentage || !month) {
    alert('Please fill all fields.')
  } else {
    $.ajax({
      url: "submit_forecast",
      method: "POST",
      data: {
        brand_id: brand_id,
        service_id: service_id,
        percentage: percentage,
        month: month,
        year: year
      },
      success: function(data){
        var result = JSON.parse(data)
        console.log(result)
        $('#submit_success').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>You have successfully added category sales forecast.</div>')
          .hide()
          .fadeIn(100);
          setTimeout(function() {
              $("#submit_success").fadeOut('fast');
              // $("#modal_form")[0].reset();
              // $("#modal_brand").val(null).trigger("change")
              $("#modal_service").val(null).trigger("change")
              // $("#modal_month_and_year").val(null).trigger("change")
          }, 2500);
          

      }

    })
  }

}



function compute(){
    var brand_id = $('#brand').val();
    var month_and_year = $('#month_and_year option:selected').text();


    var categorysalesforecast_table = $('#categorysalesforecast_table').DataTable()
    $('.dataTables_processing', $('#categorysalesforecast_table').closest('.dataTables_wrapper')).show();
     if (!brand_id || !month_and_year) {
        $('.dataTables_processing', $('#categorysalesforecast_table').closest('.dataTables_wrapper')).hide();
        alert("Please select all fields");
    } else {
        categorysalesforecast_table.clear().draw();
         $.ajax({
              url: "get_forecast",
              method: "POST",
              data: {brand_id: brand_id, month_and_year: month_and_year},
               beforeSend: function(){
              },
              success:function(data){
                var result = JSON.parse(data);
                console.log(result);
                var total_percentage = 0;

                for (let i = 0; i < result.length; i++) {
                    var productservice_name = '';

                    total_percentage += parseFloat(result[i].percentage);
                    
                    if (result[i].productservice_name === null) {
                       
                        productservice_name = 'OTC';
                   
                    } else {
                     
                        productservice_name = result[i].productservice_name;
                   
                    }
                    
                    categorysalesforecast_table.row.add([   
                        result[i].brand_name,
                        month_and_year,
                        result[i].category_name,
                        productservice_name,
                        result[i].percentage+'%',
                        '<button onclick = "edit_modal(this);" data-budgetforecast_id="'+result[i].budgetforecast_id+'" data-productservice_name="'+productservice_name+'" data-brand="'+result[i].brand_name+'" data-percentage="'+result[i].percentage+'"  style = "margin-left: -1px !important;" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>'
                    ])

                }

  
                $('.total_percentage').html(total_percentage.toFixed(2)+'%');


                $('.dataTables_processing', $('#categorysalesforecast_table').closest('.dataTables_wrapper')).hide();

                categorysalesforecast_table.draw()
                


              }

         })

    }

}

function edit_modal(button){
  var productservice_name = $(button).attr('data-productservice_name');
 
  var percentage = $(button).attr('data-percentage');
 
  var budgetforecast_id = $(button).attr('data-budgetforecast_id');


  $('#service_name').val(productservice_name);
  
  $('#service_percentage').val(percentage);
  
  $('#budgetforecast_id').val(budgetforecast_id);


  
  $('#edit_modal').modal('show');

}

function edit_percentage(){

  var budgetforecast_id = $('#budgetforecast_id').val();
  var percentage = $('#service_percentage').val();


  if (!percentage) {
    
    alert("Please enter percentage");
  
  } else {

    $.ajax({

        url: "edit_percentage",
        method: "POST",
        data: {budgetforecast_id: budgetforecast_id, percentage: percentage},
        success:function(){
        

          $('#edit_success').html('<div class="alert alert-success alert-dismissible"><h4><i class="icon fa fa-check"></i> Success!</h4>You have successfully edited a category sales forecast.</div>')
          .hide()
          .fadeIn(100);
          setTimeout(function() {
              $("#edit_success").fadeOut('fast');
              $("#edit_form")[0].reset();
          
              $('#edit_modal').modal('toggle');
              // $("#modal_brand").val(null).trigger("change")
              // $("#modal_service").val(null).trigger("change")
              // $("#modal_month_and_year").val(null).trigger("change")
          }, 2000);

          compute();

        }

    })

  }

}





  

  

  

  

  

  