$(function(){
    var budgetfolders_table = $('#budgetfolders_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1], className: "text-center" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    

    $('.select2').select2();
})


function get_budgetfolders(){

    var month = $('#month_and_year').val();
    var batch_no = $('#month_and_year option:selected').attr('data-batch');
    var budgetfolders_table = $('#budgetfolders_table').DataTable();


    $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).show();

    if(!month){
        alert("Select Month & Year first to proceed.");
        $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).hide();
        budgetfolders_table.clear().draw()
    }
    else{
        $.ajax({
            url: 'get_budgetfolders',
            method: 'GET',
            data: {'month': month, 'batch_no': batch_no},
            success: function(data){

                var budgetfolder = JSON.parse(data);

                budgetfolders_table.clear().draw()

                for(let i = 0 ; i < budgetfolder.length; i++){
                    budgetfolders_table.row.add([
                        budgetfolder[i].branch_budgetfolder,
                        '<button class="btn btn-info btn-sm btn-flat" data-folder="'+budgetfolder[i].branch_budgetfolder+'" data-month="'+month+'" data-batch="'+batch_no+'" onclick="view_budgetfolder_summary(this)">Top Sheet</button>'+
                        '<button style="margin-left: 3px;" class="btn btn-info btn-sm btn-flat" data-folder="'+budgetfolder[i].branch_budgetfolder+'" data-month="'+month+'" data-batch="'+batch_no+'" onclick="view_budgetfolder_orders(this)">Orders</button>',
                    ]);
                }
    
                budgetfolders_table.draw();
                $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).hide();
            }
        })
    }
}

function view_budgetfolder_summary(button){
    var folder = $(button).attr('data-folder');
    var month = $(button).attr('data-month');
    var batch = $(button).attr('data-batch');
    $('#loading_modal').modal('show');

    $('.summary').empty();

    $.ajax({
        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',
        method: 'GET',
        success: function(data){
    
          document.getElementById('current_time_and_date').value = data;
          
          $.ajax({
              url: 'view_budgetfolder_summary',
              method: 'GET',
              data: {'folder': folder, 'month': month, 'batch': batch},
              success: function(data){
                  var summary = JSON.parse(data);
      
                  var header = '<div class="row"><div class="col-md-12"><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month: <span><strong class="budget-folder-month"></strong></span></p></div></div>';
                  var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';
                  var thead = '<thead><tr><th style="border-right: 1px white !important; border-left: 1px white !important; border-top: 1px white !important;"></th>';
                  var tbody = '<tbody>';
                  var original_total = null;
                  var sales_forecast_total = null;
                  var projected_sales_material_total = null;
                  var projected_sales_otc_total = null;
                  var beginning_total = null;
                  var max_allowance_total = null;
                  var material_order_total = null;
                  var material_over_under_total = null;
                  var otc_beginning_total = null;
                  var otc_max_allowance_total = null;
                  var otc_order_total = null;
                  var otc_over_under_total = null;
                  var material_grand_total = null;
                  var otc_grand_total = null;
                  var total_material_cost = null;
                  var total_otc_cost = null;
                  var grand_total_otc_material_cost = null;
                  var grand_total_otc_material_over_under = null;
                  var average_sales_performance_total = null;
                  var material_cost_total = null;

                  var material_multiplier = .05;
                  var otc_multiplier = .06;

                  if(folder == 'ALL MX'){
                    material_multiplier = .08;
                  }

                  if(folder == 'SB LUZON' || folder == 'SB VISAYAS'){
                    otc_multiplier = .04;
                  }
      
                  var pos = {'ALL FT' : 'FT'+ new Date().getFullYear() + month, 'ALL MX' : 'MX'+ new Date().getFullYear() + month, 'ALL VS' : 'VS'+ new Date().getFullYear() + month, 'ALL FT' : 'FT'+ new Date().getFullYear() + month, 'HS SOUTH' : 'HSS'+ new Date().getFullYear() + month, 'NH NORTH CENTRAL' : 'NHNC'+ new Date().getFullYear() + month, 'NH NORTH EAST' : 'NHNE'+ new Date().getFullYear() + month, 'NH NORTH PROVINCIAL' : 'NHNP'+ new Date().getFullYear() + month, 'NH SOUTH LUZON' : 'NHSL'+ new Date().getFullYear() + month, 'NH SOUTH METRO' : 'NHSM'+ new Date().getFullYear() + month, 'ALL OLL' : 'OLL'+ new Date().getFullYear() + month, 'HB LUZON' : 'HBL'+ new Date().getFullYear() + month, 'HB VISMIN' : 'HBV'+ new Date().getFullYear() + month, 'HS NORTH' : 'HSN'+ new Date().getFullYear() + month, 'HS VISMIN' : 'HSV'+ new Date().getFullYear() + month, 'NH VISMIN' : 'NHV'+ new Date().getFullYear() + month, 'SB LUZON' : 'SBL'+ new Date().getFullYear() + month, 'SB VISAYAS' : 'SBV'+ new Date().getFullYear() + month};
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      thead += '<th>'+summary['columns'][i].branch_name+'</th>';
                  }
      
                  thead += '<th>TOTAL</th>';
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Original Target</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].original)+'</td>'
                      original_total = eval(original_total) + eval(summary['columns'][i].original);
                  }
      
                  tbody += '<td '+detectNegative(original_total)+'>'+numberWithCommasConvertToPositive(original_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Sales Forecast</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].sales_forecast)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].sales_forecast)+'</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                  }
      
                  tbody += '<td '+detectNegative(sales_forecast_total)+'>'+numberWithCommasConvertToPositive(sales_forecast_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Average Sales Performance</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].average_sales_performance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].average_sales_performance)+'</td>'
                      average_sales_performance_total = eval(average_sales_performance_total) + eval(summary['columns'][i].average_sales_performance);
                  }
      
                  tbody += '<td '+detectNegative(average_sales_performance_total)+'>'+numberWithCommasConvertToPositive(average_sales_performance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month (94%) - Materials</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * .94).toFixed(2))+'</td>'
                      projected_sales_material_total = eval(projected_sales_material_total) + (eval(summary['columns'][i].sales_forecast)  * .94);
                  }
      
                  tbody += '<td '+detectNegative(projected_sales_material_total)+'>'+numberWithCommasConvertToPositive(projected_sales_material_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">'+(material_multiplier*100)+'% of Material Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(( (eval(summary['columns'][i].sales_forecast) * .94) * material_multiplier).toFixed(2))+'</td>'
                      material_cost_total = eval(material_cost_total) + ((eval(summary['columns'][i].sales_forecast) * .94) * material_multiplier);
                  }
      
                  tbody += '<td '+detectNegative(material_cost_total)+'>'+numberWithCommasConvertToPositive(material_cost_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Beginning Total</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].beginning)+'</td>'
                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);
                  }
      
                  tbody += '<td '+detectNegative(beginning_total)+'>'+numberWithCommasConvertToPositive(beginning_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Maximum Allowance to Purchase</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_max_allowance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_max_allowance)+'</td>'
                      max_allowance_total = eval(max_allowance_total) + eval(summary['columns'][i].material_max_allowance);
                  }
      
                  tbody += '<td '+detectNegative(max_allowance_total)+'>'+numberWithCommasConvertToPositive(max_allowance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered Material Total Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_order_total)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_order_total)+'</td>'
                      material_order_total = eval(material_order_total) + eval(summary['columns'][i].material_order_total);
                  }
      
                  tbody += '<td '+detectNegative(material_order_total)+'>'+numberWithCommasConvertToPositive(material_order_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].material_over_under)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].material_over_under)+'</td>'
                      material_over_under_total = eval(material_over_under_total) + eval(summary['columns'][i].material_over_under);
                  }
      
                  tbody += '<td '+detectNegative(material_over_under_total)+'>'+numberWithCommasConvertToPositive(material_over_under_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important;">'+Math.round((summary['columns'][i].beginning/summary['columns'][i].sales_forecast)*100)+'%</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);
                  }
      
                  tbody += '<td style="text-align: center !important;">'+Math.round((beginning_total/sales_forecast_total)*100)+'%</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month ('+(otc_multiplier*100)+'%) - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * otc_multiplier).toFixed(2))+'</td>'
                      projected_sales_otc_total = eval(projected_sales_otc_total) + (eval(summary['columns'][i].sales_forecast) * otc_multiplier);
                  }
      
                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'
                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">60% of OTC Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6).toFixed(2))+'</td>'
                      projected_sales_otc_total = eval(projected_sales_otc_total) + ((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6);
                  }
      
                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Beginning Total</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_beginning)+'</td>'
                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);
                  }
      
                  tbody += '<td '+detectNegative(otc_beginning_total)+'>'+numberWithCommasConvertToPositive(otc_beginning_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Maximum Allowance to Purchase</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_max_allowance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_max_allowance)+'</td>'
                      otc_max_allowance_total = eval(otc_max_allowance_total) + eval(summary['columns'][i].otc_max_allowance);
                  }
      
                  tbody += '<td '+detectNegative(otc_max_allowance_total)+'>'+numberWithCommasConvertToPositive(otc_max_allowance_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered OTC Total Cost</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td '+detectNegative(summary['columns'][i].otc_order_total)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_order_total)+'</td>'
                      otc_order_total = eval(otc_order_total) + eval(summary['columns'][i].otc_order_total);
                  }
      
                  tbody += '<td '+detectNegative(otc_order_total)+'>'+numberWithCommasConvertToPositive(otc_order_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(temp);
      
                      otc_over_under_total = eval(otc_over_under_total) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(otc_over_under_total)+'>'+numberWithCommasConvertToPositive(otc_over_under_total.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important;">'+Math.round((summary['columns'][i].otc_beginning/summary['columns'][i].sales_forecast)*100)+'%</td>'
                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);
                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);
                  }
      
                  tbody += '<td style="text-align: center !important;">'+Math.round((otc_beginning_total/sales_forecast_total)*100)+'%</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - MATERIALS</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'
                  }
      
                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'
                  tbody += '<td style="font-weight: bold; width: 80px !important; text-align: center !important;">PO NUMBER</td>'
                  tbody += '</tr>'
      
                  var series = 1;
      
                  for(let i = 0; i < summary['side_infos']['material_suppliers'].length; i++){
      
                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['material_suppliers'][i].item_supplier+'</td>'
      
                      material_grand_total = 0;
          
                      for(let j = 0; j < summary['columns'].length; j++){
                          tbody += '<td '+detectNegative(summary['columns'][j]['material_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['material_cost'][i])+'</td>'
                          material_grand_total = eval(material_grand_total) + eval(summary['columns'][j]['material_cost'][i]);
                      }
          
                      tbody += '<td '+detectNegative(material_grand_total)+'>'+numberWithCommasConvertToPositive(material_grand_total.toFixed(2))+'</td>'
      
                      if(material_grand_total > 0){
                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   
                          series++;                
                      }
                      else{
                          tbody += '<td></td>' 
                      }
      
                      tbody += '</tr>'
      
                  }
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL MATERIAL COST</td>'
      
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                          
                      total_material_cost = eval(total_material_cost) + eval(temp);
      
                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(total_material_cost)+'>'+numberWithCommasConvertToPositive(total_material_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'
      
                  var total_over_under = 0;
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['material_max_allowance']) - eval(temp);
      
                      total_over_under = eval(total_over_under) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  if(max_allowance_total > 0 && total_over_under > 0){
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
                  else{
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
                  
                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - OTC</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'
                  }
      
                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'
                  tbody += '<td style="text-align: center !important; font-weight: bold;">PO NUMBER</td>'
                  tbody += '</tr>'
      
                  for(let i = 0; i < summary['side_infos']['otc_suppliers'].length; i++){
      
                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['otc_suppliers'][i].item_supplier+'</td>'
      
                      otc_grand_total = 0;
          
                      for(let j = 0; j < summary['columns'].length; j++){
                          tbody += '<td '+detectNegative(summary['columns'][j]['otc_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['otc_cost'][i])+'</td>'
                          otc_grand_total = eval(otc_grand_total) + eval(summary['columns'][j]['otc_cost'][i]);
                      }
          
                      tbody += '<td '+detectNegative(otc_grand_total)+'>'+numberWithCommasConvertToPositive(otc_grand_total.toFixed(2))+'</td>'
      
                      if(otc_grand_total > 0){
                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   
                          series++;                
                      }
                      else{
                          tbody += '<td></td>' 
                      }
      
                      tbody += '</tr>'
      
                  }
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL OTC COST</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
                          
                      total_otc_cost = eval(total_otc_cost) + eval(temp);
      
                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(total_otc_cost)+'>'+numberWithCommasConvertToPositive(total_otc_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'
      
                  total_over_under = 0;
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var temp = 0;
                      var over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(temp);
      
                      total_over_under = eval(total_over_under) + eval(over_under);
      
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                  }
      
                  if(otc_max_allowance_total > 0 && total_over_under > 0){
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
                  else{
                      tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive((eval(total_over_under)).toFixed(2))+'</td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL (MATERIAL & OTC)</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var otc_temp = 0;
                      var material_temp = 0;
                      var total_otc_material_cost = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          otc_temp = eval(otc_temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          material_temp = eval(material_temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                          
                      total_otc_material_cost = eval(material_temp) + eval(otc_temp);
      
                      grand_total_otc_material_cost = eval(grand_total_otc_material_cost) + eval(total_otc_material_cost);
      
                      tbody += '<td '+detectNegative(total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(total_otc_material_cost.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(grand_total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(grand_total_otc_material_cost.toFixed(2))+'</td>'
                  tbody += '</tr>'
      
                  tbody += '<tr>'
      
                  for(let i = 0; i < summary['columns'].length+2; i++){
                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'
                  }
      
                  tbody += '</tr>'
      
                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL OVER / UNDER FOR THIS MONTH</td>'
      
                  for(let i = 0; i < summary['columns'].length; i++){
      
                      var otc_temp = 0;
                      var material_temp = 0;
                      var otc_over_under = 0;
                      var material_over_under = 0;
      
                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){
      
                          otc_temp = eval(otc_temp) + eval(summary['columns'][i]['otc_cost'][j]);
      
                      }
      
                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){
      
                          material_temp = eval(material_temp) + eval(summary['columns'][i]['material_cost'][j]);
      
                      }
                       
                      otc_over_under = eval(summary['columns'][i]['otc_max_allowance']) - eval(otc_temp);
      
                      material_over_under = eval(summary['columns'][i]['material_max_allowance']) - eval(material_temp);
      
                      total_otc_material_over_under = eval(otc_over_under) + eval(material_over_under)
      
                      grand_total_otc_material_over_under = eval(grand_total_otc_material_over_under) + eval(total_otc_material_over_under);
      
                      tbody += '<td '+detectNegative(total_otc_material_over_under)+'>'+numberWithCommasConvertToPositive(total_otc_material_over_under.toFixed(2))+'</td>';
                  }
      
                  tbody += '<td '+detectNegative(grand_total_otc_material_over_under)+'>'+numberWithCommasConvertToPositive(grand_total_otc_material_over_under.toFixed(2))+'</td>'
                  tbody += '</tr>'
                  
      
      
      
                  thead += '</tr></thead>';
                  tbody += '</tr></tbody>'
      
                  $('.summary').append(header);
                  $('.summary').append(table);
                  $('.summary-table').append(thead);
                  $('.summary-table').append(tbody);
      
                  $('.budget-folder-name').text(summary['side_infos']['budgetfolder'].toUpperCase());
                  $('.budget-folder-month').text(summary['side_infos']['budgetfolder_month'].toUpperCase());
      
                  $('#loading_modal').modal('hide');
      
                  printThis();
              }
          })

        }
    })
    
}

function printThis(){
    printElement(document.getElementById("printThis"));

    // $('#summary_table').removeAttr('style');

    // $('.print-title').removeAttr('style');

    // $('.print-hide').attr('style', 'display: none;');

    window.print();

    // $('.print-title').attr('style', 'display: none;');

    // $('.print-hide').removeAttr('style');

    // $('#summary_table').attr('style', 'display: none;');
}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    
    $printSection.appendChild(domClone);

    // var doc = new jsPDF('l');
    // var specialElementHandlers = {
    //     '#editor': function (element, renderer) {
    //         return true;
    //     }
    // };

// $('#cmd').click(function () {
    // doc.fromHTML($('#printThis').html(), 15, 15, {
    //     'width': 170
    // });
    // doc.save('sample-file.pdf');
// });
}

function view_budgetfolder_orders(button){
    var folder = $(button).attr('data-folder');
    var month = $(button).attr('data-month');
    var batch = $(button).attr('data-batch');
    $('#loading_modal').modal('show');

    $('.summary').empty();

    $.ajax({
        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',
        method: 'GET',
        success: function(data){

            document.getElementById('current_time_and_date').value = data;

            $.ajax({
                url: 'view_budgetfolder_orders',
                method: 'GET',
                data: {'folder': folder, 'month': month, 'batch': batch},
                success: function(data){
        
                    var orders = JSON.parse(data);
        
                    var header = '<div class="row"><div class="col-md-12"><p class="top-content header"><span style="padding-left: 88px;"><strong>Summary of Requisition</strong></span></p><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month:<span><strong class="budget-folder-month"></strong></span></p></div></div>';
                    var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';
                    var thead = '<thead><tr><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th>';
                    var tbody = '<tbody>';
        
                    var total_quantity = null;
                    var total_amount = null;
                    var column_total_material = {};
                    var column_total_otc = {};

                    // Material
        
                    for(let i = 0; i < orders['branches'].length; i++){
                        thead += '<th colspan="2">'+orders['branches'][i]+'</th>'
        
                        column_total_material[orders['branches'][i]] = [];
        
                        for(let j = 0; j < orders['material_suppliers'].length; j++){
        
                            column_total_material[orders['branches'][i]][orders['material_suppliers'][j]] = [];
                        }
                    }

                    // OTC

                    for(let i = 0; i < orders['branches'].length; i++){
        
                        column_total_otc[orders['branches'][i]] = [];
        
                        for(let j = 0; j < orders['otc_suppliers'].length; j++){
        
                            column_total_otc[orders['branches'][i]][orders['otc_suppliers'][j]] = [];
                        }
                    }
        
                    thead += '<th colspan="2">TOTAL</th>'
        
                    thead += '</tr><tr><th>Supplier</th><th>Item Name</th><th>Shade</th><th>Color</th><th>Model</th><th>Unit Cost</th><th>UOM</th>';
        
                    for(let i = 0; i < orders['branches'].length; i++){
                        thead += '<th>QTY</th><th>Amount</th>'
                    }
        
                    thead += '<th>QTY</th><th>Amount</th>'

                    // Material
        
                    var counter = 0;
        
                    var last_supplier;
                    
                    var supplier_column_grand_total = 0;
                    
                    for(let i = 0; i < orders['materials'].length; i++){
                        total_quantity = null;
                        total_amount = null;
        
                        if(i == 0){
                            last_supplier = orders['materials'][i].item_supplier;
                        }
        
                        if(last_supplier != orders['materials'][i].item_supplier){
                            tbody += '<tr>';
                            tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';
        
                            var supplier_column_total = null;
        
                            for(let l = 0; l < orders['branches'].length; l++){
                                tbody += '<td style="background: yellow !important;"></td>';
                                tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(column_total_material[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';
        
                                supplier_column_total += column_total_material[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);
                            }
                            
                            tbody += '<td style="background: yellow !important;"></td>';
                            tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';
        
                            tbody += '</tr>';
        
                            supplier_column_grand_total += supplier_column_total;
                        }
        
                        tbody += '<tr>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_supplier+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_name+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_shade != null ? orders['materials'][i].item_shade : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_color != null ? orders['materials'][i].item_color : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_model != null ? orders['materials'][i].item_model : '')+'</td>';
                        tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['materials'][i].item_cost)+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_sapuom != null ? orders['materials'][i].item_sapuom : '')+'</td>';
        
                            
                            for(let l = 0; l < orders['branches'].length; l++, counter++){
                                
                                
                                if(orders['columns']['materials'][counter].length > 0){
        
                                    column_total_material[orders['columns']['materials'][counter][0].branch_name][orders['columns']['materials'][counter][0].item_supplier].push(eval(orders['columns']['materials'][counter][0].amount));
        
                                    tbody += '<td style="padding-left: 3px;">'+orders['columns']['materials'][counter][0].budgetfoldermaterialorder_quantity+'</td>';
                                    tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['columns']['materials'][counter][0].amount)+'</td>';
            
                                    total_quantity = eval(total_quantity) + eval(orders['columns']['materials'][counter][0].budgetfoldermaterialorder_quantity);
                                    total_amount = eval(total_amount) + eval(orders['columns']['materials'][counter][0].amount);
        
                                }
                                else{
                                    tbody += '<td style="padding-left: 3px;"></td>';
                                    tbody += '<td style="padding-left: 3px;"></td>';
                                }
        
                            }    
        
                        tbody += '<td>'+total_quantity+'</td>';
                        tbody += '<td>'+numberWithCommas(total_amount.toFixed(2))+'</td>';
        
                        tbody += '</tr>'
        
                        last_supplier = orders['materials'][i].item_supplier;
                    }
        
                    tbody += '<tr>';
                    tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';
        
                    var supplier_column_total = 0;
        
                    for(let l = 0; l < orders['branches'].length; l++){
                        tbody += '<td style="background: yellow !important;"></td>';
                        tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(column_total_material[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';
        
                        supplier_column_total += column_total_material[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);
                    }
                    
                    tbody += '<td style="background: yellow !important;"></td>';
                    tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';
        
                    tbody += '</tr>';
        
                    supplier_column_grand_total += supplier_column_total;

                    // OTC

                    counter = 0;
                    
                    for(let i = 0; i < orders['otcs'].length; i++){
                        total_quantity = null;
                        total_amount = null;
        
                        if(i == 0){
                            last_supplier = orders['otcs'][i].item_supplier;
                        }
        
                        if(last_supplier != orders['otcs'][i].item_supplier){
                            tbody += '<tr>';
                            tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';
        
                            var supplier_column_total = 0;
        
                            for(let l = 0; l < orders['branches'].length; l++){
                                tbody += '<td style="background: yellow !important;"></td>';
                                tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(column_total_otc[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';
        
                                supplier_column_total += column_total_otc[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);
                            }
                            
                            tbody += '<td style="background: yellow !important;"></td>';
                            tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';
        
                            tbody += '</tr>';
        
                            supplier_column_grand_total += supplier_column_total;
                        }
        
                        tbody += '<tr>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['otcs'][i].item_supplier+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['otcs'][i].item_name+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_shade != null ? orders['otcs'][i].item_shade : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_color != null ? orders['otcs'][i].item_color : '')+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_model != null ? orders['otcs'][i].item_model : '')+'</td>';
                        tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['otcs'][i].item_cost)+'</td>';
                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_sapuom != null ? orders['otcs'][i].item_sapuom : '')+'</td>';
        
                            
                            for(let l = 0; l < orders['branches'].length; l++, counter++){
                                
                                
                                if(orders['columns']['otcs'][counter].length > 0){
        
                                    column_total_otc[orders['columns']['otcs'][counter][0].branch_name][orders['columns']['otcs'][counter][0].item_supplier].push(eval(orders['columns']['otcs'][counter][0].amount));
        
                                    tbody += '<td style="padding-left: 3px;">'+orders['columns']['otcs'][counter][0].budgetfolderotcorder_quantity+'</td>';
                                    tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['columns']['otcs'][counter][0].amount)+'</td>';
            
                                    total_quantity = eval(total_quantity) + eval(orders['columns']['otcs'][counter][0].budgetfolderotcorder_quantity);
                                    total_amount = eval(total_amount) + eval(orders['columns']['otcs'][counter][0].amount);
        
                                }
                                else{
                                    tbody += '<td style="padding-left: 3px;"></td>';
                                    tbody += '<td style="padding-left: 3px;"></td>';
                                }
        
                            }    
        
                        tbody += '<td>'+total_quantity+'</td>';
                        tbody += '<td>'+numberWithCommas(total_amount.toFixed(2))+'</td>';
        
                        tbody += '</tr>'
        
                        last_supplier = orders['otcs'][i].item_supplier;
                    }
        
                    tbody += '<tr>';
                    tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';
        
                    supplier_column_total = 0;

                    console.log(column_total_otc)
                    console.log(column_total_material)
        
                    for(let l = 0; l < orders['branches'].length; l++){
                        tbody += '<td style="background: yellow !important;"></td>';
                        tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(column_total_otc[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';
        
                        supplier_column_total += column_total_otc[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);
                    }
                    
                    tbody += '<td style="background: yellow !important;"></td>';
                    tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';
        
                    tbody += '</tr>';
        
                    supplier_column_grand_total += supplier_column_total;
        
                    tbody += '<tr>';
                    tbody += '<td style="text-align: center !important; background: green !important; vertical-align: middle !important;" colspan="7"><strong style="color: white !important;">GRAND - TOTAL'+'</strong></td>';
        
                    
                    for(let l = 0; l < orders['branches'].length; l++){
                        
                        var supplier_subtotal_material_total = 0;
                        var supplier_subtotal_otc_total = 0;
                        var supplier_subtotal_grand_total = 0;
        
                        for(let m = 0; m < orders['material_suppliers'].length; m++){
                            supplier_subtotal_material_total += column_total_material[orders['branches'][l]][orders['material_suppliers'][m]].reduce(function(acc, val) { return acc + val; }, 0)
                        }

                        for(let m = 0; m < orders['otc_suppliers'].length; m++){
                            supplier_subtotal_otc_total += column_total_otc[orders['branches'][l]][orders['otc_suppliers'][m]].reduce(function(acc, val) { return acc + val; }, 0)
                        }

                        // console.log(supplier_subtotal_material_total);
                        // console.log(supplier_subtotal_otc_total);
                        // console.log(supplier_subtotal_grand_total);

                        supplier_subtotal_grand_total = supplier_subtotal_material_total + supplier_subtotal_otc_total;
        
                        tbody += '<td style="background: green !important;"></td>';
                        tbody += '<td style="background: green !important;"><strong style="color: white !important;">'+numberWithCommas(supplier_subtotal_grand_total.toFixed(2))+'</strong></td>';
                    }
                    
                    tbody += '<td style="background: green !important;"></td>';
                    tbody += '<td style="background: green !important;"><strong style="color: white !important;">'+numberWithCommas(supplier_column_grand_total.toFixed(2))+'</strong></td>';
        
                    tbody += '</tr>';
        
                    thead += '</tr></thead>';
                    tbody += '</tbody>'
        
        
                    $('.summary').append(header);
                    $('.summary').append(table);
                    $('.summary-table').append(thead);
                    $('.summary-table').append(tbody);
        
                    $('.budget-folder-name').text(orders['folder'].toUpperCase());
                    $('.budget-folder-month').text(orders['month_and_year'].toUpperCase());
        
                    $('#loading_modal').modal('hide');
        
                    printThis();
        
                    $('#loading_modal').modal('hide');
        
                }
            })
        }

    });

}
