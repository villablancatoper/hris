$(function(){

    var budgetfolders_table = $('#budgetfolders_table').DataTable({

        'processing': true,

    

        'bAutoWidth': false,

        'bDestroy': true,

        'bSort': false,



        'language': {



            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    

        },

        'columnDefs': [

            { targets: [0, 1], className: "text-center" },



        ],

        'ordering': false,



        "paging": false,

        

        "bInfo" : false,

        scrollX: true



    });

    $('.select2').select2();

    $('thead').remove();

})

function show_topsheet(){

    var budgetfolders_table = $('#budgetfolders_table').DataTable();

    var month = $('#month_and_year').val();

    var year = $('#month_and_year option:selected').attr('data-year');

    var batch_no = $('#month_and_year option:selected').attr('data-batch');

    var folders = $('#folders').val();

    var error = null;

    $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).show();

    if(!month || !folders){
        if(!month){
            error += 'Month and Year is required.\n';
        }
        if(!folders){
            error += 'Month and Year is required.\n';
        }

        alert(error);

        $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).hide();
    }
    else{

        budgetfolders_table.clear().draw();

        $.ajax({
    
            url: "show_topsheet",
    
            method: "POST",
    
            data: {month: month, year: year, batch_no: batch_no, folders: folders},
    
            success:function(data){
    
              $("#topsheet").removeClass("hidden");
    
              $("#orders").removeClass("hidden");
    
              $("#inventory").removeClass("hidden");

              $("#print_test").removeClass("hidden");
    
              var months = {'01': 'January', '02': 'February', '03': 'March', '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', '09': 'September', '10': 'October', '11': 'November', '12': 'December'};
    
              var budgetfolder = JSON.parse(data)
    
              console.log(budgetfolder)
    
              var th_branches = '';
    
              var tbody = '';
    
              var thead = '';
    
              var total_original = 0;
    
              var total_sales_forecast = 0;
    
              var total_average_sales_performance = 0;
    
              var total_projected_sales = 0;
    
              var total_material_cost = 0;
    
              var total_beginning = 0;
    
              var total_max_allowance = 0;
    
              var total_max_allowance_otc = 0;
    
              var total_material_order = 0;
    
              var total_material_over_under = 0;
    
              var total_otc_over_under = 0;
    
              var total_projected_sales_otc = 0;
    
              var total_otc_cost = 0;
    
              var total_otc_order = 0;
    
              var total_otc_beginning = 0;
    
              var grand_total_material_otc = 0;
    
              var grand_total_over_under = 0;
    
              var grand_total_material_otc_over_under = 0;
    
              var material_multiplier = .05;
    
              var otc_multiplier = .06;

              var projected_sales_multiplier = .94;
    
              if(folders == 'ALL MX'){
    
                  material_multiplier = .08;
    
              }

              if(folders == 'ALL VS'){
                material_multiplier = .08;
                otc_multiplier = 0.05;
                projected_sales_multiplier = .95;
      
              }
    
              if(folders == 'SB LUZON' || folders == 'SB VISAYAS'){
    
                otc_multiplier = .04;
                projected_sales_multiplier = .96;
    
              }

              if(folders == 'ALL FT' /*&&  (months[month]+' '+year == 'February 2020')*/){
    
                material_multiplier = .08;
    
              }
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  th_branches += "<th style = 'width: 300px !important;'>"+budgetfolder['branches'][i].branch_name+"</th>";
    
              }
    
              $("#budgetfolders_table").empty();
    
              // $("#budgetfolders_table").append("<thead><tr><th style='border-right: 1px white !important; border-top: none !important; width: 400px !important;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>"+th_branches+"<th style = 'width: 300px !important;'>TOTAL</th></tr></thead>");
    
              // $("#budgetfolders_table").append("<tbody>");
    
              thead += "<thead><tr><th style='border-right: 1px white !important; border-top: none !important; width: 400px !important;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>"+th_branches+"<th style = 'width: 300px !important;'>TOTAL</th></tr></thead>"
    
              tbody += "<tbody style = 'font-size: 11px;'>";
    
              tbody += "<tr><td style = 'font-weight:bold !important;'> Original Target </td>";
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].original)+"</td>";
    
                  total_original = eval(total_original) + eval(budgetfolder['columns'][i].original);
    
              }
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_original.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
              tbody += "<tr><td style = 'font-weight:bold !important;'> Sales Forecast </td>";
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].sales_forecast)+"</td>";
    
                  total_sales_forecast = eval(total_sales_forecast) + eval(budgetfolder['columns'][i].sales_forecast);
    
              }
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_sales_forecast.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
              tbody += "<tr><td style = 'font-weight:bold !important;'> Average Sales Performance </td>";
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].average_sales_performance)+"</td>";
    
                  total_average_sales_performance = eval(total_average_sales_performance) + eval(budgetfolder['columns'][i].average_sales_performance);
    
              }
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_average_sales_performance.toFixed(2))+"</td>"
    
              tbody += "</tr>";

              tbody += "<tr><td style = 'font-weight:bold !important;'> Last Month Sales </td>";

              var total_lastmonthsales = 0;
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].last_month_sales)+"</td>";
    
                  total_lastmonthsales = eval(total_lastmonthsales) + eval(budgetfolder['columns'][i].last_month_sales);
    
              }
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_lastmonthsales.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
            

              //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'> Projected sales for the Month ("+(projected_sales_multiplier * 100)+"%) - Materials </td>";
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(budgetfolder['columns'][i].sales_forecast) * projected_sales_multiplier).toFixed(2))+"</td>";
    
                  
    
              }
    
              total_projected_sales = total_sales_forecast * projected_sales_multiplier;
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_projected_sales.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
    
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >"+(material_multiplier*100)+"% of Material Cost</td>" 
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(budgetfolder['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier)).toFixed(2))+"</td>";

              }
    
              total_material_cost = total_projected_sales * eval(material_multiplier);
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_material_cost.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >Materials Beginning Total</td>" 
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
    
                  tbody += "<td "+detectNegative(budgetfolder['columns'][i].beginning)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].beginning)+"</td>"
    
                  total_beginning = eval(total_beginning) + eval(budgetfolder['columns'][i].beginning);
    
              }
    
              tbody += "<td "+detectNegative(total_beginning)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_beginning.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
    
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>Materials Maximum Allowance to Purchase</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var max_allowance_purchase = (eval(budgetfolder['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier))
                  tbody += "<td "+detectNegative((max_allowance_purchase - eval(budgetfolder['columns'][i].beginning) + 0.00).toFixed(2))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(max_allowance_purchase - budgetfolder['columns'][i].beginning)).toFixed(2))+"</td>"
    
                  total_max_allowance = eval(total_max_allowance) + max_allowance_purchase - eval(budgetfolder['columns'][i].beginning) ;
    
              }
    
              tbody += "<td "+detectNegative(total_max_allowance)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_max_allowance.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
    
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >Ordered Material Total Cost</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_material_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(budgetfolder['columns'][i]['material_cost'][y])
                    }

                  tbody += "<td "+detectNegative(ordered_material_total_cost)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(ordered_material_total_cost.toFixed(2))+"</td>"
                
                  total_material_order = eval(total_material_order) + eval(ordered_material_total_cost);
                
              }
              
              
              tbody += "<td "+detectNegative(total_material_order)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_material_order.toFixed(2))+"</td>"
    
              tbody += "</tr>";
    
    
    
              tbody += "<tr><td style='font-weight: bold; padding-left: 8px;'><span style='color: red !important;'>(Over)</span> / Under - Materials</td>"
              console.log(ordered_material_total_cost);
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_material_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(budgetfolder['columns'][i]['material_cost'][y])
                    }
                  var max_deduct = (eval(budgetfolder['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier) - budgetfolder['columns'][i].beginning)
                  tbody += "<td "+detectNegative(max_deduct - eval(ordered_material_total_cost))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((max_deduct - ordered_material_total_cost).toFixed(2))+"</td>"
    
                  total_material_over_under = eval(total_material_over_under) + eval(max_deduct - eval(ordered_material_total_cost));
    
              }
    
              tbody += "<td "+detectNegative(total_material_over_under)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_material_over_under.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
              //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>Total Percentage of Beginning Supplies</td>"
    
              
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  var percentage = ((budgetfolder['columns'][i].beginning/budgetfolder['columns'][i].sales_forecast)*100).toFixed(2);
    
                  if (isNaN(percentage)) {
    
                     percentage = 0;
    
                  }
    
                  tbody += "<td style = 'text-align: center !important;'>"+percentage+"%"+"</td>";
    
              }
    
              tbody += "<td style = 'text-align: center !important;'> "+((total_beginning / total_sales_forecast)*100).toFixed(2)+"%"+"</td>"
    
              tbody += "</tr>"
    
               //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style='font-weight: bold; font-size: 11px !important;'>Projected Sales for the Month ("+(otc_multiplier*100)+"%) - OTC</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(budgetfolder['columns'][i].sales_forecast) * otc_multiplier).toFixed(2))+"</td>";
    
              }
    
              total_projected_sales_otc = total_sales_forecast * eval(otc_multiplier);
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_projected_sales_otc.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >60% of OTC Cost</td>" 
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(budgetfolder['columns'][i].sales_forecast) * otc_multiplier * .6).toFixed(2))+"</td>";
    
              }
    
              total_otc_cost = total_projected_sales_otc * .6;
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_otc_cost.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
    
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >OTC Beginning Total</td>" 
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
    
                  tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(budgetfolder['columns'][i].otc_beginning)+"</td>";
    
                  total_otc_beginning = eval(total_otc_beginning) + eval(budgetfolder['columns'][i].otc_beginning);
    
              }
    
              tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_otc_beginning.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
    
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>OTC Maximum Allowance to Purchase</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var otc_max_purchase = (eval(budgetfolder['columns'][i].sales_forecast) * otc_multiplier * .6)
                  tbody += "<td "+detectNegative((otc_max_purchase - eval(budgetfolder['columns'][i].otc_beginning)).toFixed(2))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((otc_max_purchase - budgetfolder['columns'][i].otc_beginning).toFixed(2))+"</td>"
    
                  total_max_allowance_otc = eval(total_max_allowance_otc) + otc_max_purchase - eval(budgetfolder['columns'][i].otc_beginning);
    
              }
    
              tbody += "<td "+detectNegative(total_max_allowance_otc)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(parseFloat(total_max_allowance_otc + 0.00).toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
              tbody += "<tr><td style = 'font-weight:bold !important;' >Ordered OTC Total Cost</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_otc_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(budgetfolder['columns'][i]['otc_cost'][y])
                    }
                  tbody += "<td "+detectNegative(budgetfolder['columns'][i].ordered_otc_total_cost)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(ordered_otc_total_cost)).toFixed(2))+"</td>"
    
                  total_otc_order = eval(total_otc_order) + eval(ordered_otc_total_cost);
    
              }
    
              tbody += "<td "+detectNegative(total_otc_order)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_otc_order.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
    
    
              tbody += "<tr><td style='font-weight: bold; padding-left: 8px;'><span style='color: red !important;'>(Over)</span> / Under - OTC</td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_otc_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(budgetfolder['columns'][i]['otc_cost'][y])
                    }
                  var otc_deduct = eval(eval(budgetfolder['columns'][i].sales_forecast) * otc_multiplier * .6) - budgetfolder['columns'][i].otc_beginning
    
                  tbody += "<td "+detectNegative((otc_deduct - eval(ordered_otc_total_cost)).toFixed(2))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((otc_deduct - eval(ordered_otc_total_cost)).toFixed(2))+"</td>"
    
                  total_otc_over_under = eval(total_otc_over_under) +  eval(otc_deduct - eval(ordered_otc_total_cost));
    
              }
    
              tbody += "<td "+detectNegative(total_otc_over_under)+" style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_otc_over_under.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
              //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>Total Percentage of Beginning Supplies</td>"
    
              for (let i = 0; i < budgetfolder['branches'].length; i++) {
    
                  var percentage = ((budgetfolder['columns'][i].otc_beginning/budgetfolder['columns'][i].sales_forecast)*100).toFixed(2);
    
                  if (isNaN(percentage)) {
    
                      percentage = 0;
    
                  }
    
                  tbody += "<td style = 'text-align: center !important;'>"+percentage+"%"+"</td>";
    
              }
    
              tbody += "<td style = 'text-align: center !important;'> "+((total_otc_beginning / total_sales_forecast)*100).toFixed(2)+"%"+"</td>"
    
              tbody += "</tr>"
    
               //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>GRAND TOTAL (MATERIAL & OTC) </td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_otc_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(budgetfolder['columns'][i]['otc_cost'][y])
                    }
                  var ordered_material_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(budgetfolder['columns'][i]['material_cost'][y])
                    }

                  tbody += "<td "+detectNegative((eval(ordered_otc_total_cost) + eval(ordered_material_total_cost)).toFixed(2))+ "style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((eval(ordered_otc_total_cost) + eval(ordered_material_total_cost)).toFixed(2))+"</td>";
    
                   grand_total_material_otc = eval(grand_total_material_otc) + eval(ordered_otc_total_cost) + eval(ordered_material_total_cost);
    
              }
    
              tbody += "<td "+detectNegative(grand_total_material_otc)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(grand_total_material_otc.toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
               //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "<tr><td style = 'font-weight:bold !important; font-size: 11px !important;'>GRAND TOTAL OVER/UNDER FOR THIS MONTH </td>"
    
              for(let i = 0; i < budgetfolder['branches'].length; i++){
                  var ordered_otc_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(budgetfolder['columns'][i]['otc_cost'][y])
                    }
                  var ordered_material_total_cost = 0;
                    for (let  y = 0; y < budgetfolder['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(budgetfolder['columns'][i]['material_cost'][y])
                    }

                  var otc =  eval(eval(budgetfolder['columns'][i].sales_forecast) * otc_multiplier * .6 - budgetfolder['columns'][i].otc_beginning) - eval(ordered_otc_total_cost)
                  var mats =  (eval(budgetfolder['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier) - budgetfolder['columns'][i].beginning) - eval(ordered_material_total_cost)
                  var grand_over_under = eval(otc) + eval(mats);
    
                  tbody += "<td "+detectNegative(grand_over_under)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(grand_over_under.toFixed(2))+"</td>";
    
                  grand_total_material_otc_over_under = eval(grand_total_material_otc_over_under) + eval(grand_over_under);
    
              }
    
              tbody += "<td "+detectNegative(grand_total_material_otc_over_under)+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(eval(grand_total_material_otc_over_under).toFixed(2))+"</td>"
    
              tbody += "</tr>"
    
               //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              // //
    
              tbody += "<td colspan = "+budgetfolder['branches'].length+2+"></td>";
    
              //
    
              tbody += "</tbody>";
    
              $("#budgetfolders_table").append(thead);
    
              $("#budgetfolders_table").append(tbody);

              $('.dataTables_processing', $('#budgetfolders_table').closest('.dataTables_wrapper')).hide();
    
            }
    
        });
    
    }

}



$('#month_and_year').change(function(){

    var month = $('#month_and_year').val();

    var batch_no = $('#month_and_year option:selected').attr('data-batch');

    // alert(batch_no)

    // alert(month)

    if (batch_no != '') {



      $.ajax({



          url: "fetch_folders",



          method: "GET",



          data: {'month': month, 'batch_no': batch_no},



          success:function(data){

            // var budgetfolder = JSON.parse(data);

            // console.log(data)

            // $('#folders').html(budgetfolder);

            $('#folders').html(data);

          }

      })

    }

})



function view_budgetfolder_summary(){

    // var folder = $(button).attr('data-folder');

    var month = $('#month_and_year').val();

    var year = $('#month_and_year option:selected').attr('data-year');

    var batch = $('#month_and_year option:selected').attr('data-batch');

    var folder = $('#folders').val();



    $('#loading_modal').modal('show');



    $('.summary').empty();



    $.ajax({

        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',

        method: 'GET',

        success: function(data){

    

          document.getElementById('current_time_and_date').value = data;

          

          $.ajax({

              url: 'view_budgetfolder_summary',

              method: 'GET',

              data: {'folder': folder, 'month': month, 'year': year, 'batch': batch},

              success: function(data){

                  var summary = JSON.parse(data);
                  console.log(summary)

                  var months = {'01': 'January', '02': 'February', '03': 'March', '04': 'April', '05': 'May', '06': 'June', '07': 'July', '08': 'August', '09': 'September', '10': 'October', '11': 'November', '12': 'December'};

                  var header = '<div class="row"><div class="col-md-12"><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month: <span><strong class="budget-folder-month"></strong></span></p></div></div>';

                  var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';

                  var thead = '<thead><tr><th style="border-right: 1px white !important; border-left: 1px white !important; border-top: 1px white !important;"></th>';

                  var tbody = '<tbody>';

                  var original_total = null;

                  var sales_forecast_total = null;

                  var projected_sales_material_total = null;

                  var projected_sales_otc_total = null;

                  var beginning_total = null;

                  var max_allowance_total = null;

                  var material_order_total = null;

                  var material_over_under_total = null;

                  var otc_beginning_total = null;

                  var otc_max_allowance_total = null;

                  var otc_order_total = null;

                  var otc_over_under_total = null;

                  var material_grand_total = null;

                  var otc_grand_total = null;

                  var total_material_cost = null;

                  var total_otc_cost = null;

                  var grand_total_otc_material_cost = null;

                  var grand_total_otc_material_over_under = null;

                  var average_sales_performance_total = null;

                  var material_cost_total = null;
                  var total_otc_over_under = 0;


                  var material_multiplier = .05;

                  var otc_multiplier = .06;

                  var projected_sales_multiplier = .94;
        
                  if(folder == 'ALL MX'){
        
                    material_multiplier = .08;
        
                  }
        
                  if(folder == 'SB LUZON' || folder == 'SB VISAYAS'){
        
                    otc_multiplier = .04;
                    projected_sales_multiplier = .96;
        
                  }

                  if(folder == 'ALL VS'){
                    material_multiplier = .08;
                    otc_multiplier = 0.05;
                    projected_sales_multiplier = .95;
                  }

                  if(folder == 'ALL FT' /* && (months[month]+' '+year == 'February 2020' )*/){
    
                    material_multiplier = .08;
        
                  }

                  console.log(material_multiplier);

                  var pos = {'ALL FT' : 'FT'+ new Date().getFullYear() + month, 'ALL MX' : 'MX'+ new Date().getFullYear() + month, 'ALL VS' : 'VS'+ new Date().getFullYear() + month, 'ALL FT' : 'FT'+ new Date().getFullYear() + month, 'HS SOUTH' : 'HSS'+ new Date().getFullYear() + month, 'NH NORTH CENTRAL' : 'NHNC'+ new Date().getFullYear() + month, 'NH NORTH EAST' : 'NHNE'+ new Date().getFullYear() + month, 'NH NORTH PROVINCIAL' : 'NHNP'+ new Date().getFullYear() + month, 'NH SOUTH LUZON' : 'NHSL'+ new Date().getFullYear() + month, 'NH SOUTH METRO' : 'NHSM'+ new Date().getFullYear() + month, 'ALL OLL' : 'OLL'+ new Date().getFullYear() + month, 'HB LUZON' : 'HBL'+ new Date().getFullYear() + month, 'HB VISMIN' : 'HBV'+ new Date().getFullYear() + month, 'HS NORTH' : 'HSN'+ new Date().getFullYear() + month, 'HS VISMIN' : 'HSV'+ new Date().getFullYear() + month, 'NH VISMIN' : 'NHV'+ new Date().getFullYear() + month, 'SB LUZON' : 'SBL'+ new Date().getFullYear() + month, 'SB VISAYAS' : 'SBV'+ new Date().getFullYear() + month};

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      thead += '<th>'+summary['columns'][i].branch_name+'</th>';

                  }

      

                  thead += '<th>TOTAL</th>';

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Original Target</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].original)+'</td>'

                      original_total = eval(original_total) + eval(summary['columns'][i].original);

                  }

      

                  tbody += '<td '+detectNegative(original_total)+'>'+numberWithCommasConvertToPositive(original_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Sales Forecast</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].sales_forecast)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].sales_forecast)+'</td>'

                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);

                  }

      

                  tbody += '<td '+detectNegative(sales_forecast_total)+'>'+numberWithCommasConvertToPositive(sales_forecast_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Average Sales Performance</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].average_sales_performance)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].average_sales_performance)+'</td>'

                      average_sales_performance_total = eval(average_sales_performance_total) + eval(summary['columns'][i].average_sales_performance);

                  }

      

                  tbody += '<td '+detectNegative(average_sales_performance_total)+'>'+numberWithCommasConvertToPositive(average_sales_performance_total.toFixed(2))+'</td>'

                  tbody += '</tr>'


                  tbody += "<tr><td style = 'font-weight:bold !important;'> Last Month Sales </td>";

                  var total_lastmonthsales = 0;
        
                  for (let i = 0; i < summary['columns'].length; i++) {
        
                      tbody += "<td style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive(summary['columns'][i].last_month_sales)+"</td>";
        
                      total_lastmonthsales = eval(total_lastmonthsales) + eval(summary['columns'][i].last_month_sales);
        
                  }
        
                  tbody += "<td style = 'text-align: right !important;'> "+numberWithCommasConvertToPositive(total_lastmonthsales.toFixed(2))+"</td>"
        
                  tbody += "</tr>";



                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'



                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month ('+(projected_sales_multiplier*100)+'%) - Materials</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier).toFixed(2))+'</td>'

                      projected_sales_material_total = eval(projected_sales_material_total) + (eval(summary['columns'][i].sales_forecast)  * projected_sales_multiplier);

                  }

      

                  tbody += '<td '+detectNegative(projected_sales_material_total)+'>'+numberWithCommasConvertToPositive(projected_sales_material_total.toFixed(2))+'</td>'

                  tbody += '</tr>'



                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">'+(material_multiplier*100)+'% of Material Cost</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(( (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier) * material_multiplier).toFixed(2))+'</td>'

                      material_cost_total = eval(material_cost_total) + ((eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier) * material_multiplier);

                  }

      

                  tbody += '<td '+detectNegative(material_cost_total)+'>'+numberWithCommasConvertToPositive(material_cost_total.toFixed(2))+'</td>'

                  tbody += '</tr>'



      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Beginning Total</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].beginning)+'</td>'

                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);

                  }

      

                  tbody += '<td '+detectNegative(beginning_total)+'>'+numberWithCommasConvertToPositive(beginning_total.toFixed(2))+'</td>'

                  tbody += '<td rowspan="2" style = "text-align: center;">Beginning Inventory <br> Plus Ordered</td>'


                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Materials Maximum Allowance to Purchase</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){
                      var max_allowance_purchase = (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier))
                      tbody += "<td "+detectNegative((max_allowance_purchase - eval(summary['columns'][i].beginning) + 0.00).toFixed(2))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((max_allowance_purchase - eval(summary['columns'][i].beginning)).toFixed(2))+"</td>"

                      max_allowance_total = eval(max_allowance_total) +  max_allowance_purchase - eval(summary['columns'][i].beginning);

                  }

      

                  tbody += '<td '+detectNegative(max_allowance_total)+'>'+numberWithCommasConvertToPositive(max_allowance_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered Material Total Cost</td>'


                  
                  for(let i = 0; i < summary['columns'].length; i++){
                      var ordered_material_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(summary['columns'][i]['material_cost'][y])
                      }

                      tbody += '<td '+detectNegative(ordered_material_total_cost)+'>'+numberWithCommasConvertToPositive(ordered_material_total_cost.toFixed(2))+'</td>'

                      material_order_total = eval(material_order_total) + eval(ordered_material_total_cost);


                  }

      

                  tbody += '<td '+detectNegative(material_order_total)+'>'+numberWithCommasConvertToPositive(material_order_total.toFixed(2))+'</td>'

                  tbody += '<td>'+numberWithCommasConvertToPositive((material_order_total +  parseFloat(beginning_total)).toFixed(2))+'</td'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 8px;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      var ordered_material_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(summary['columns'][i]['material_cost'][y])
                      }

                      var over_under = 0;
                      
                      over_under = (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier) - summary['columns'][i].beginning) - ordered_material_total_cost;
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';

                      material_over_under_total = eval(material_over_under_total) + eval(over_under);

                  }

      

                  tbody += '<td '+detectNegative(material_over_under_total)+'>'+numberWithCommasConvertToPositive(material_over_under_total.toFixed(2))+'</td>'

                  tbody += '<td>'+numberWithCommasConvertToPositive(((material_order_total + beginning_total) / projected_sales_material_total * 100).toFixed(2))+'%'+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      var percentage = ((summary['columns'][i].beginning/summary['columns'][i].sales_forecast)*100).toFixed(2);

                      if (isNaN(percentage)) {

                        percentage = 0;

                      }

                      tbody += '<td style="text-align: center !important;">'+percentage+'%</td>'

                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);

                      beginning_total = eval(beginning_total) + eval(summary['columns'][i].beginning);

                  }

      

                  tbody += '<td style="text-align: center !important;">'+((beginning_total/sales_forecast_total)*100).toFixed(2)+'%</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'



                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">Projected Sales for the Month ('+(otc_multiplier*100)+'%) - OTC</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive((eval(summary['columns'][i].sales_forecast) * otc_multiplier).toFixed(2))+'</td>'

                      projected_sales_otc_total = eval(projected_sales_otc_total) + (eval(summary['columns'][i].sales_forecast) * otc_multiplier);

            

                  }

      

                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'

                  tbody += '</tr>'



                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px; width: 180px !important;">60% of OTC Cost</td>'

                  console.log(projected_sales_otc_total)

                  

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].original)+'>'+numberWithCommasConvertToPositive(((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6).toFixed(2))+'</td>'

                      // projected_sales_otc_total = eval(projected_sales_otc_total) + ((eval(summary['columns'][i].sales_forecast) * otc_multiplier) * .6);

        

                  }

                  projected_sales_otc_total = eval(projected_sales_otc_total) * .6;

                  tbody += '<td '+detectNegative(projected_sales_otc_total)+'>'+numberWithCommasConvertToPositive(projected_sales_otc_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Beginning Total</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td '+detectNegative(summary['columns'][i].otc_beginning)+'>'+numberWithCommasConvertToPositive(summary['columns'][i].otc_beginning)+'</td>'

                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);

                  }

      

                  tbody += '<td '+detectNegative(otc_beginning_total)+'>'+numberWithCommasConvertToPositive(otc_beginning_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">OTC Maximum Allowance to Purchase</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){
                      var otc_max_purchase = (eval(summary['columns'][i].sales_forecast) * otc_multiplier * .6)
                      tbody += "<td "+detectNegative((otc_max_purchase - eval(summary['columns'][i].otc_beginning)).toFixed(2))+" style = 'text-align: right !important;'>"+numberWithCommasConvertToPositive((otc_max_purchase - summary['columns'][i].otc_beginning).toFixed(2))+"</td>"
        
                      otc_max_allowance_total = eval(otc_max_allowance_total) + otc_max_purchase - eval(summary['columns'][i].otc_beginning);

                    //   otc_max_allowance_total = eval(otc_max_allowance_total) + eval(eval(summary['columns'][i].beginning) - max_allowance_purchase);

                  }

      

                  tbody += '<td '+detectNegative(otc_max_allowance_total)+'>'+numberWithCommasConvertToPositive(otc_max_allowance_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Ordered OTC Total Cost</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){
                      var ordered_otc_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(summary['columns'][i]['otc_cost'][y])
                      }

                      tbody += '<td '+detectNegative(ordered_otc_total_cost)+'>'+numberWithCommasConvertToPositive(ordered_otc_total_cost.toFixed(2))+'</td>'

                      otc_order_total = eval(otc_order_total) + eval(ordered_otc_total_cost);

                  }

      

                  tbody += '<td '+detectNegative(otc_order_total)+'>'+numberWithCommasConvertToPositive(otc_order_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 8px;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'


                  for(let i = 0; i < summary['columns'].length; i++){

                      var ordered_otc_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['otc_cost'].length; y++) {
                         ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(summary['columns'][i]['otc_cost'][y])
                      }

                      var over_under = 0;
                      
                      var otc_max_purchase = eval(eval(summary['columns'][i].sales_forecast) * otc_multiplier * .6) - eval(summary['columns'][i].otc_beginning);
                      over_under = eval(otc_max_purchase) - eval(ordered_otc_total_cost);
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';

                      total_otc_over_under = eval(total_otc_over_under) + eval(over_under);

                  }

    
      

                  tbody += '<td '+detectNegative(total_otc_over_under)+'>'+numberWithCommasConvertToPositive(total_otc_over_under.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">Total percentage of beginning supplies</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      var percentage = ((summary['columns'][i].otc_beginning/summary['columns'][i].sales_forecast)*100).toFixed(2);

                      if (isNaN(percentage)) {

                          percentage = 0;

                      }

                      tbody += '<td style="text-align: center !important;">'+percentage+'%</td>'

                      sales_forecast_total = eval(sales_forecast_total) + eval(summary['columns'][i].sales_forecast);

                      otc_beginning_total = eval(otc_beginning_total) + eval(summary['columns'][i].otc_beginning);

                  }

      

                  tbody += '<td style="text-align: center !important;">'+((otc_beginning_total/sales_forecast_total)*100).toFixed(2)+'%</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - MATERIALS</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'

                  }

      

                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'

                  tbody += '<td style="font-weight: bold; width: 80px !important; text-align: center !important;">PO NUMBER</td>'

                  tbody += '</tr>'

      

                  var series = 1;

      

                  for(let i = 0; i < summary['side_infos']['material_suppliers'].length; i++){

      

                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['material_suppliers'][i].item_supplier+'</td>'

      

                      material_grand_total = 0;

          

                      for(let j = 0; j < summary['columns'].length; j++){

                          tbody += '<td '+detectNegative(summary['columns'][j]['material_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['material_cost'][i])+'</td>'

                          material_grand_total = eval(material_grand_total) + eval(summary['columns'][j]['material_cost'][i]);

                      }

          

                      tbody += '<td '+detectNegative(material_grand_total)+'>'+numberWithCommasConvertToPositive(material_grand_total.toFixed(2))+'</td>'

      

                      if(material_grand_total > 0){

                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   

                          series++;                

                      }

                      else{

                          tbody += '<td></td>' 

                      }

      

                      tbody += '</tr>'

      

                  }

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL MATERIAL COST</td>'

      

      

                  for(let i = 0; i < summary['columns'].length; i++){

      

                      var temp = 0;

      

                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){

      

                          temp = eval(temp) + eval(summary['columns'][i]['material_cost'][j]);

      

                      }

                          

                      total_material_cost = eval(total_material_cost) + eval(temp);

      

                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';

                  }

      

                  tbody += '<td '+detectNegative(total_material_cost)+'>'+numberWithCommasConvertToPositive(total_material_cost.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important; padding-left: 8px;"><span style="color: red !important;">(Over)</span> / Under - Materials</td>'

      

                  var total_over_under = 0;

      

                  for(let i = 0; i < summary['columns'].length; i++){

      
                      var ordered_material_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['material_cost'].length; y++) {
                         ordered_material_total_cost = eval(ordered_material_total_cost) + eval(summary['columns'][i]['material_cost'][y])
                      }

                      var temp = 0;

                      var over_under = 0;
                      
                      over_under = (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier) - summary['columns'][i].beginning) - ordered_material_total_cost;
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';
                      temp = eval(temp) + eval(over_under);

                      total_over_under = eval(total_over_under) + eval(temp);

                  }
                  
                  
                  tbody += '<td '+detectNegative(total_over_under)+'>'+numberWithCommasConvertToPositive(total_over_under.toFixed(2))+'</td>';
                 

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

                  

                  tbody += '<tr>'+'<td style="text-align: center !important; font-weight: bold;">SUPPLIER - OTC</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

                      tbody += '<td style="text-align: center !important; font-weight: bold; padding-left: 3px;">'+summary['columns'][i].branch_name+'</td>'

                  }

      

                  tbody += '<td style="text-align: center !important; font-weight: bold;">GRAND TOTAL</td>'

                  tbody += '<td style="text-align: center !important; font-weight: bold;">PO NUMBER</td>'

                  tbody += '</tr>'

      

                  for(let i = 0; i < summary['side_infos']['otc_suppliers'].length; i++){

      

                      tbody += '<tr>'+'<td style="font-weight: bold; padding-left: 3px;">'+summary['side_infos']['otc_suppliers'][i].item_supplier+'</td>'

      

                      otc_grand_total = 0;

          

                      for(let j = 0; j < summary['columns'].length; j++){

                          tbody += '<td '+detectNegative(summary['columns'][j]['otc_cost'][i])+'>'+numberWithCommasConvertToPositive(summary['columns'][j]['otc_cost'][i])+'</td>'

                          otc_grand_total = eval(otc_grand_total) + eval(summary['columns'][j]['otc_cost'][i]);

                      }

          

                      tbody += '<td '+detectNegative(otc_grand_total)+'>'+numberWithCommasConvertToPositive(otc_grand_total.toFixed(2))+'</td>'

      

                      if(otc_grand_total > 0){

                          tbody += '<td style="text-align: center !important;">'+ pos[folder] + '-' + ( series < 10 ? '0'+series : series ) +'</td>'   

                          series++;                

                      }

                      else{

                          tbody += '<td></td>' 

                      }

      

                      tbody += '</tr>'

      

                  }

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">TOTAL OTC COST</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

      

                      var temp = 0;

      

                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){

      

                          temp = eval(temp) + eval(summary['columns'][i]['otc_cost'][j]);

      

                      }

                          

                      total_otc_cost = eval(total_otc_cost) + eval(temp);

      

                      tbody += '<td '+detectNegative(temp)+'>'+numberWithCommasConvertToPositive(temp.toFixed(2))+'</td>';

                  }

      

                  tbody += '<td '+detectNegative(total_otc_cost)+'>'+numberWithCommasConvertToPositive(total_otc_cost.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important; padding-left: 8px;"><span style="color: red !important;">(Over)</span> / Under - OTC</td>'

      

                  total_over_under = 0;

                  over_under = 0; 

                  for(let i = 0; i < summary['columns'].length; i++){

                      over_under =(eval(eval(summary['columns'][i].sales_forecast) * otc_multiplier * .6) - summary['columns'][i].otc_beginning) -  eval(summary['columns'][i].otc_order_total);
                      tbody += '<td '+detectNegative(over_under)+'>'+numberWithCommasConvertToPositive(over_under.toFixed(2))+'</td>';

                  }

      
                  tbody += '<td '+detectNegative(total_otc_over_under)+'>'+numberWithCommasConvertToPositive(total_otc_over_under.toFixed(2))+'</td>';
                  

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL (MATERIAL & OTC)</td>'

      

                  for(let i = 0; i < summary['columns'].length; i++){

      

                      var otc_temp = 0;

                      var material_temp = 0;

                      var total_otc_material_cost = 0;

      

                      for(let j = 0; j < summary['columns'][i]['otc_cost'].length; j++){

      

                          otc_temp = eval(otc_temp) + eval(summary['columns'][i]['otc_cost'][j]);

      

                      }

      

                      for(let j = 0; j < summary['columns'][i]['material_cost'].length; j++){

      

                          material_temp = eval(material_temp) + eval(summary['columns'][i]['material_cost'][j]);

      

                      }

                          

                      total_otc_material_cost = eval(material_temp) + eval(otc_temp);

      

                      grand_total_otc_material_cost = eval(grand_total_otc_material_cost) + eval(total_otc_material_cost);

      

                      tbody += '<td '+detectNegative(total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(total_otc_material_cost.toFixed(2))+'</td>';

                  }

      

                  tbody += '<td '+detectNegative(grand_total_otc_material_cost)+'>'+numberWithCommasConvertToPositive(grand_total_otc_material_cost.toFixed(2))+'</td>'

                  tbody += '</tr>'

      

                  tbody += '<tr>'

      

                  for(let i = 0; i < summary['columns'].length+2; i++){

                      tbody += '<td style="height: 10px !important; border-right: 1px white !important; border-left: 1px white !important;"></td>'

                  }

      

                  tbody += '</tr>'

      

                  tbody += '<tr>'+'<td style="font-weight: bold; text-align: center !important;">GRAND TOTAL OVER / UNDER FOR THIS MONTH</td>'

      
                  var grand_over_under_total = 0;
                  for(let i = 0; i < summary['columns'].length; i++){
                    var ordered_otc_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['otc_cost'].length; y++) {
                           ordered_otc_total_cost = eval(ordered_otc_total_cost) + eval(summary['columns'][i]['otc_cost'][y])
                      }
                    var ordered_material_total_cost = 0;
                      for (let  y = 0; y < summary['columns'][i]['material_cost'].length; y++) {
                           ordered_material_total_cost = eval(ordered_material_total_cost) + eval(summary['columns'][i]['material_cost'][y])
                      }


                      var otc = eval(eval(summary['columns'][i].sales_forecast) * otc_multiplier * .6 - summary['columns'][i].otc_beginning) - eval(ordered_otc_total_cost);
                      var mats = (eval(summary['columns'][i].sales_forecast) * projected_sales_multiplier * eval(material_multiplier) - summary['columns'][i].beginning) - eval(ordered_material_total_cost);
                      var grand_over_under = eval(otc) + eval(mats);
                      tbody += '<td '+detectNegative(grand_over_under)+'>'+numberWithCommasConvertToPositive(grand_over_under.toFixed(2))+'</td>';
                      grand_over_under_total = eval(grand_over_under_total) + grand_over_under
                  }

      

                  tbody += '<td '+detectNegative(grand_over_under_total)+'>'+numberWithCommasConvertToPositive(grand_over_under_total.toFixed(2))+'</td>'

                  tbody += '</tr>'

                  

      

      

      

                  thead += '</tr></thead>';

                  tbody += '</tr></tbody>'

      

                  $('.summary').append(header);

                  $('.summary').append(table);

                  $('.summary-table').append(thead);

                  $('.summary-table').append(tbody);

      

                  $('.budget-folder-name').text(summary['side_infos']['budgetfolder'].toUpperCase());

                  $('.budget-folder-month').text(summary['side_infos']['budgetfolder_month'].toUpperCase());

      

                  $('#loading_modal').modal('hide');

      

                  printThis();

              }

          })



        }

    })

    
}


function printThis(){

    printElement(document.getElementById("printThis"));



    // $('#summary_table').removeAttr('style');



    // $('.print-title').removeAttr('style');



    // $('.print-hide').attr('style', 'display: none;');



    window.print();



    // $('.print-title').attr('style', 'display: none;');



    // $('.print-hide').removeAttr('style');



    // $('#summary_table').attr('style', 'display: none;');

}

function printElement(elem) {

    var domClone = elem.cloneNode(true);

    

    var $printSection = document.getElementById("printSection");

    

    if (!$printSection) {

        var $printSection = document.createElement("div");

        $printSection.id = "printSection";

        document.body.appendChild($printSection);

    }

    

    $printSection.innerHTML = "";

    

    $printSection.appendChild(domClone);



    // var doc = new jsPDF('l');

    // var specialElementHandlers = {

    //     '#editor': function (element, renderer) {

    //         return true;

    //     }

    // };

// $('#cmd').click(function () {

    // doc.fromHTML($('#printThis').html(), 15, 15, {

    //     'width': 170

    // });

    // doc.save('sample-file.pdf');

// });

}


function view_budgetfolder_orders(button){

    var month = $('#month_and_year').val();

    var batch = $('#month_and_year option:selected').attr('data-batch');

    var folder = $('#folders').val();

    $('#loading_modal').modal('show');



    $('.summary').empty();



    $.ajax({

        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',

        method: 'GET',

        success: function(data){



            document.getElementById('current_time_and_date').value = data;



            $.ajax({

                url: 'view_budgetfolder_orders',

                method: 'GET',

                data: {'folder': folder, 'month': month, 'batch': batch},

                success: function(data){

        

                    var orders = JSON.parse(data);

                    var header = '<div class="row"><div class="col-md-12"><p class="top-content header"><span style="padding-left: 88px;"><strong>Summary of Requisition</strong></span></p><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month:<span><strong class="budget-folder-month"></strong></span></p></div></div>';

                    var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';

                    var thead = '<thead><tr><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th>';

                    var tbody = '<tbody>';

        

                    var total_quantity = null;

                    var total_amount = null;

                    var material_column_total = {};


                    for(let i = 0; i < orders['branches'].length; i++){

                        thead += '<th colspan="2">'+orders['branches'][i]+'</th>'

        

                        material_column_total[orders['branches'][i]] = [];

        

                        for(let j = 0; j < orders['material_suppliers'].length; j++){

        

                            material_column_total[orders['branches'][i]][orders['material_suppliers'][j]] = [];

                        }

                    }

        

                    thead += '<th colspan="2">TOTAL</th>'

        

                    thead += '</tr><tr><th>Supplier</th><th>Item Name</th><th>Shade</th><th>Color</th><th>Model</th><th>Unit Cost</th><th>UOM</th>';

        

                    for(let i = 0; i < orders['branches'].length; i++){

                        thead += '<th>QTY</th><th>Amount</th>'

                    }

        

                    thead += '<th>QTY</th><th>Amount</th>'

                    

                    var counter = 0;

        

                    var last_supplier;

                    

                    var supplier_column_grand_total = 0;

                    
                    if(orders['material_suppliers'].length > 0){
                        

                        for(let i = 0; i < orders['materials'].length; i++){

                            total_quantity = null;

                            total_amount = null;

            

                            if(i == 0){

                                last_supplier = orders['materials'][i].item_supplier;

                            }

            

                            if(last_supplier != orders['materials'][i].item_supplier){

                                tbody += '<tr>';

                                tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';

            

                                var supplier_column_total = null;

            

                                for(let l = 0; l < orders['branches'].length; l++){

                                    tbody += '<td style="background: yellow !important;"></td>';

                                    tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(material_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';

            

                                    supplier_column_total += material_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);

                                }

                                

                                tbody += '<td style="background: yellow !important;"></td>';

                                tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';

            

                                tbody += '</tr>';

            

                                supplier_column_grand_total += supplier_column_total;

                            }

            

                            tbody += '<tr>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_supplier+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_name+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_shade != null ? orders['materials'][i].item_shade : '')+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_color != null ? orders['materials'][i].item_color : '')+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_model != null ? orders['materials'][i].item_model : '')+'</td>';

                            tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['materials'][i].item_cost)+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_sapuom != null ? orders['materials'][i].item_sapuom : '')+'</td>';

            

                                

                                for(let l = 0; l < orders['branches'].length; l++, counter++){

                                    

                                    

                                    if(orders['columns']['materials'][counter].length > 0){

            

                                        material_column_total[orders['columns']['materials'][counter][0].branch_name][orders['columns']['materials'][counter][0].item_supplier].push(eval(orders['columns']['materials'][counter][0].amount));

            

                                        tbody += '<td style="padding-left: 3px;">'+orders['columns']['materials'][counter][0].budgetfoldermaterialorder_quantity+'</td>';

                                        tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['columns']['materials'][counter][0].amount)+'</td>';

                

                                        total_quantity = eval(total_quantity) + eval(orders['columns']['materials'][counter][0].budgetfoldermaterialorder_quantity);

                                        total_amount = eval(total_amount) + eval(orders['columns']['materials'][counter][0].amount);

            

                                    }

                                    else{

                                        tbody += '<td style="padding-left: 3px;"></td>';

                                        tbody += '<td style="padding-left: 3px;"></td>';

                                    }

            

                                }    

            

                            tbody += '<td>'+total_quantity+'</td>';

                            tbody += '<td>'+numberWithCommas(total_amount.toFixed(2))+'</td>';

            

                            tbody += '</tr>'

            

                            last_supplier = orders['materials'][i].item_supplier;

                        }

            

                        tbody += '<tr>';

                        tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';

            

                        var supplier_column_total = null;

            

                        for(let l = 0; l < orders['branches'].length; l++){

                            tbody += '<td style="background: yellow !important;"></td>';
                            
                            tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(material_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';

            

                            supplier_column_total += material_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);

                        }

                        

                        tbody += '<td style="background: yellow !important;"></td>';
                        
                        tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';

            

                        tbody += '</tr>';

                    }

        

                    supplier_column_grand_total += supplier_column_total;

                    /*
                        OTC
                            */

                    if(orders['otcs'].length > 0){

                        var total_quantity = null;

                        var total_amount = null;

                        var otc_column_total = {};

            
                        for(let i = 0; i < orders['branches'].length; i++){
            

                            otc_column_total[orders['branches'][i]] = [];

            

                            for(let j = 0; j < orders['otc_suppliers'].length; j++){

            

                                otc_column_total[orders['branches'][i]][orders['otc_suppliers'][j]] = [];

                            }

                        }

                        var counter = 0;

            

                        var last_supplier = null;

                        var count = 0;

                        for(let i = 0; i < orders['otcs'].length; i++){

                            total_quantity = null;

                            total_amount = null;

            

                            if(i == 0){

                                last_supplier = orders['otcs'][i].item_supplier;

                            }

                            if(last_supplier != orders['otcs'][i].item_supplier){

                                console.log(count++);

                                tbody += '<tr>';

                                tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';

            

                                var supplier_column_total = null;

                                
                                for(let l = 0; l < orders['branches'].length; l++){
                                    
                                    console.log(orders['branches'][l]);
                                    console.log(last_supplier);
                                    
                                    tbody += '<td style="background: yellow !important;"></td>';

                                    tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(otc_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';

            

                                    supplier_column_total += otc_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);

                                }

                                

                                tbody += '<td style="background: yellow !important;"></td>';

                                tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';

            

                                tbody += '</tr>';

            

                                supplier_column_grand_total += supplier_column_total;

                            }

            

                            tbody += '<tr>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['otcs'][i].item_supplier+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['otcs'][i].item_name+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_shade != null ? orders['otcs'][i].item_shade : '')+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_color != null ? orders['otcs'][i].item_color : '')+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_model != null ? orders['otcs'][i].item_model : '')+'</td>';

                            tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['otcs'][i].item_cost)+'</td>';

                            tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['otcs'][i].item_sapuom != null ? orders['otcs'][i].item_sapuom : '')+'</td>';

            

                                

                            for(let l = 0; l < orders['branches'].length; l++, counter++){

                                

                                

                                if(orders['columns']['otcs'][counter].length > 0){

        

                                    otc_column_total[orders['columns']['otcs'][counter][0].branch_name][orders['columns']['otcs'][counter][0].item_supplier].push(eval(orders['columns']['otcs'][counter][0].amount));

        

                                    tbody += '<td style="padding-left: 3px;">'+orders['columns']['otcs'][counter][0].budgetfolderotcorder_quantity+'</td>';

                                    tbody += '<td style="padding-left: 3px;">'+numberWithCommas(orders['columns']['otcs'][counter][0].amount)+'</td>';

            

                                    total_quantity = eval(total_quantity) + eval(orders['columns']['otcs'][counter][0].budgetfolderotcorder_quantity);

                                    total_amount = eval(total_amount) + eval(orders['columns']['otcs'][counter][0].amount);

        

                                }

                                else{

                                    tbody += '<td style="padding-left: 3px;"></td>';

                                    tbody += '<td style="padding-left: 3px;"></td>';

                                }

        

                            }    

            

                            tbody += '<td>'+total_quantity+'</td>';

                            tbody += '<td>'+numberWithCommas(total_amount.toFixed(2))+'</td>';

            

                            tbody += '</tr>'

            

                            last_supplier = orders['otcs'][i].item_supplier;

                        }

            

                        tbody += '<tr>';

                        tbody += '<td style="text-align: center !important; background: yellow !important; vertical-align: middle !important;" colspan="7"><strong>'+last_supplier+' - TOTAL'+'</strong></td>';

                    

        

                        var supplier_column_total = null;

            

                        for(let l = 0; l < orders['branches'].length; l++){

                            console.log(otc_column_total[orders['branches'][l]]);
                            console.log(last_supplier);

                            tbody += '<td style="background: yellow !important;"></td>';

                            tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(otc_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0).toFixed(2))+'</strong></td>';

            

                            supplier_column_total += otc_column_total[orders['branches'][l]][last_supplier].reduce(function(acc, val) { return acc + val; }, 0);

                        }

                        

                        tbody += '<td style="background: yellow !important;"></td>';

                        tbody += '<td style="background: yellow !important;"><strong>'+numberWithCommas(supplier_column_total.toFixed(2))+'</strong></td>';

            

                        tbody += '</tr>';

                    }

        

                    supplier_column_grand_total += supplier_column_total;


                    tbody += '<tr>';

                    tbody += '<td style="text-align: center !important; background: green !important; vertical-align: middle !important;" colspan="7"><strong style="color: white !important;">GRAND - TOTAL'+'</strong></td>';

        

                    var grand_total = 0;

                    for(let l = 0; l < orders['branches'].length; l++){

                        

                        var material_subtotal_total = 0;

                        var otc_subtotal_total = 0;

                        var otc_material_subtotal_total = 0;

        

                        for(let m = 0; m < orders['material_suppliers'].length; m++){

                            material_subtotal_total += material_column_total[orders['branches'][l]][orders['material_suppliers'][m]].reduce(function(acc, val) { return acc + val; }, 0)

                        }

                        if(orders['otcs'].length > 0){

                            for(let m = 0; m < orders['otc_suppliers'].length; m++){
    
                                otc_subtotal_total += otc_column_total[orders['branches'][l]][orders['otc_suppliers'][m]].reduce(function(acc, val) { return acc + val; }, 0)
    
                            }

                        }


                        otc_material_subtotal_total = otc_subtotal_total + material_subtotal_total

                        tbody += '<td style="background: green !important;"></td>';

                        tbody += '<td style="background: green !important;"><strong style="color: white !important;">'+numberWithCommas(otc_material_subtotal_total.toFixed(2))+'</strong></td>';

                        grand_total = grand_total + otc_material_subtotal_total;

                    }

                    

                    tbody += '<td style="background: green !important;"></td>';

                    tbody += '<td style="background: green !important;"><strong style="color: white !important;">'+numberWithCommas(grand_total.toFixed(2))+'</strong></td>';

        

                    tbody += '</tr>';

        

                    thead += '</tr></thead>';

                    tbody += '</tbody>'

        

        

                    $('.summary').append(header);

                    $('.summary').append(table);

                    $('.summary-table').append(thead);

                    $('.summary-table').append(tbody);

        

                    $('.budget-folder-name').text(orders['folder'].toUpperCase());

                    $('.budget-folder-month').text(orders['month_and_year'].toUpperCase());

        

                    $('#loading_modal').modal('hide');

        

                    printThis();

        

                    $('#loading_modal').modal('hide');

        

                }

            })

        }



    });

}


function view_budgetfolder_inventory_quantity(button){

    var year = $('#month_and_year option:selected').attr('data-year');

    var month = $('#month_and_year').val();

    var batch = $('#month_and_year option:selected').attr('data-batch');

    var folder = $('#folders').val();

    $('#loading_modal').modal('show');



    $('.summary').empty();



    $.ajax({

        url: $('#base_url').val()+'dashboard/get_current_server_time_and_date',

        method: 'GET',

        success: function(data){



            document.getElementById('current_time_and_date').value = data;



            $.ajax({

                url: 'view_budgetfolder_inventory',

                method: 'GET',

                data: {'folder': folder, 'month': month, 'batch': batch, 'year': year},

                success: function(data){

        

                    var orders = JSON.parse(data);

                    console.log(orders)

                    // orders['branches'].sort(function(a,b) {

                    //     a = a.toLowerCase();

                    //     b = b.toLowerCase();

                    //     if( a == b) return 0;

                    //     return a < b ? -1 : 1;

                    // });

                    var header = '<div class="row"><div class="col-md-12"><p class="top-content header"><span style="padding-left: 88px;"><strong>Beginning Inventory</strong></span></p><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month:<span><strong class="budget-folder-month"></strong></span></p></div></div>';

                    var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div><div class="row"><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px;"><strong>Conforme:</strong><p><strong>Aileen Gonzales</strong></p><p>Head of Operations</p><p>Date/Time Printed: '+$('#current_time_and_date').val()+'</p></div><div class="col-md-5"><p style="margin-bottom: 15px"></div></div></div><div class="col-md-6"><div class="row"><div class="col-md-7"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Ramon de Ubago III</strong><p>President & CEO</div><div class="col-md-5"><p style="margin-bottom: 15px"><strong>Approved by:</strong><p><strong>Roberto Carlos</strong><p>EVP & CFO</div></div></div></div>';

                    var thead = '<thead><tr><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th><th style=" border-right: 1px white !important; border-left: 1px white !important; border-top: 1px solid white !important;"></th>';

                    var tbody = '<tbody>';

        

                    var total_quantity = null;

                    var total_amount = null;

                    var column_total = {};

        

                    for(let i = 0; i < orders['branches'].length; i++){

                        thead += '<th>'+orders['branches'][i]+'</th>'

        

                        column_total[orders['branches'][i]] = [];

        

                        for(let j = 0; j < orders['material_suppliers'].length; j++){

        

                            column_total[orders['branches'][i]][orders['material_suppliers'][j]] = [];

                        }

                    }

                    thead += '<th>TOTAL</th>'

                    thead += '</tr><tr><th>Item Name</th><th>Shade</th><th>Color</th><th>Model</th><th>UOM</th>';

                    for(let i = 0; i < orders['branches'].length; i++){

                        thead += '<th>Quantity</th>'

                    }

                    thead += '<th>Quantity</th>'

                    var counter = 0;

                    for(let i = 0; i < orders['materials'].length; i++){

                        total_quantity = null;

                        total_amount = null;

                        tbody += '<tr>';

                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+orders['materials'][i].item_name+'</td>';

                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_shade != null ? orders['materials'][i].item_shade : '')+'</td>';

                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_color != null ? orders['materials'][i].item_color : '')+'</td>';

                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_model != null ? orders['materials'][i].item_model : '')+'</td>';


                        tbody += '<td style="text-align: left !important; padding-left: 3px;">'+(orders['materials'][i].item_sapuom != null ? orders['materials'][i].item_sapuom : '')+'</td>';

                            for(let l = 0; l < orders['branches'].length; l++, counter++){   

                                if(orders['columns']['materials'][counter].length > 0){

                                    tbody += '<td style="padding-left: 3px; text-align: center;">'+orders['columns']['materials'][counter][0].quantity+'</td>';

                                    total_quantity = eval(total_quantity) + eval(orders['columns']['materials'][counter][0].quantity);

                                }

                                else{

                                    tbody += '<td style="padding-left: 3px;"></td>';

                                }

                            }    

                        var quantity = parseFloat(total_quantity).toFixed(2);

                        if(isNaN(quantity)){

                            tbody += '<td style = "text-align: center;"></td>';

                        }else{

                            tbody += '<td style = "text-align: center;">'+numberWithCommas(quantity)+'</td>';

                        }

                        tbody += '</tr>'

                    }

        

        

                    thead += '</tr></thead>';

                    tbody += '</tbody>'

        

        

                    $('.summary').append(header);

                    $('.summary').append(table);

                    $('.summary-table').append(thead);

                    $('.summary-table').append(tbody);

        

                    $('.budget-folder-name').text(orders['folder'].toUpperCase());

                    $('.budget-folder-month').text(orders['month_and_year'].toUpperCase());

        

                    $('#loading_modal').modal('hide');

        

                    printThis();

        

                    $('#loading_modal').modal('hide');

        

                }

            })

        }



    });

}


function print_test(){

    var month = $('#month_and_year').val();

    var year = $('#month_and_year option:selected').attr('data-year');

    var batch = $('#month_and_year option:selected').attr('data-batch');

    var folder = $('#folders').val();

    $('#loading_modal').modal('show');

    $('.summary').empty();


    console.log(month+year+batch+folder)

    $.ajax({
              url: 'print_test',

              method: 'GET',

              data: {'folder': folder, 'month': month, 'year': year, 'batch': batch},
              success: function(data){
                var result = JSON.parse(data)
                console.log(result)

                  var qty = 0;

                  var amount = 0;

                  var header = '<div class="row"><div class="col-md-12"><p class="top-content header">Budget Folder Name: <span><strong class="budget-folder-name"></strong></span></p><p class="top-content">Budget Folder Month: <span><strong class="budget-folder-month"></strong></span></p></div></div>';

                  var table = '<div class="table-responsive"><table class="table table-bordered table-hover summary-table general-content" style="width: 100% !important;"></table></div>';

                  var thead = '<thead>';

                  var tbody = '<tbody>';

                  thead += '<tr><th>Category</th><th style = "width: 30px !important;">Service</th><th>Price</th><th style = "width: 10px !important;">Percentage</th><th style = "width: 10px !important;">QTY</th><th>Amount</th></tr>'

                  
                 
                  for (let i = 0; i < result['category_forecast'].length; i++) {
                      qty = Math.ceil(parseFloat(parseFloat(result['category_forecast'][i].percentage / 100) * result['total_forecast']) / result['category_forecast'][i].productservice_amount);
                      amount = Math.ceil(parseFloat(parseFloat(result['category_forecast'][i].percentage / 100) * result['total_forecast']));

                      if (isNaN(qty)) {
                        qty = 'N/A';
                      }

                      tbody += '<tr>';
                      tbody += '<td>'+result['category_forecast'][i].category_name+'</td>';
                      tbody += '<td style = "width: 30px !important;">'+result['category_forecast'][i].productservice_name+'</td>';
                      tbody += '<td>'+result['category_forecast'][i].productservice_amount+'</td>';
                      tbody += '<td style = "width: 10px !important;">'+result['category_forecast'][i].percentage+'%'+'</td>';
                      tbody += '<td style = "width: 10px !important;">'+qty+'</td>';
                      tbody += '<td>'+amount+'</td>';
                      tbody += '</tr>';

                  }

                  





                  thead += '</thead>';

                  tbody += '</tbody>'


                  $('.summary').append(header);

                  $('.summary').append(table);

                  $('.summary-table').append(thead);

                  $('.summary-table').append(tbody);

  
                  $('.budget-folder-name').text(folder.toUpperCase());

                  $('.budget-folder-month').text(result['month_and_year'].toUpperCase());

                  $('#loading_modal').modal('hide');

                  printThis();

              }
   

    })

 
              


}





