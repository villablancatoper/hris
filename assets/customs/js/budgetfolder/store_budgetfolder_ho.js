$(function(){
    var checklist_table = $('#checklist_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1, 2, 3, 4, 5, 6], className: "text-center" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    var budgetfolder_table = $('#budgetfolder_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1, 2, 3, 4, 5, 6], className: "text-center" },
            { targets: [8], className: "align-right" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    var consumables_table = $('#consumables_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [1, 2, 3, 4, 5, 6], className: "text-center" },
            { targets: [7, 10], className: "align-right" }
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    consumables_table.columns(7).visible(false);

    var otcs_table = $('#otcs_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [1, 2, 3, 4, 5, 6], className: "text-center" },
            { targets: [7, 10], className: "align-right" }
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    otcs_table.columns(7).visible(false);

    var forecast_table = $('#forecast_table').DataTable({
        'processing': true,
    
        'bAutoWidth': false,
    
        'bSort': false,

        'language': {

            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
        },
        'columnDefs': [
            { targets: [0, 1], className: "align-left" },
            { targets: [3, 4], className: "text-center" },
            { targets: [2, 5], className: "align-right" },
        ],
        'ordering': false,

        "lengthChange": false,

        "paging": false,
        
        "bInfo" : false
    });

    $('.select2').select2();
})

function check_brand_budgetfolder_availability(){

    $('.dataTables_processing', $('#checklist_table').closest('.dataTables_wrapper')).show();

    var checklist_table = $('#checklist_table').DataTable();

    var brand_id = $('#brands').val();

    var batch_no = $('#month_and_year option:selected').attr('data-batch');

    var year = $('#month_and_year option:selected').attr('data-year');

    var month = $('#month_and_year').val();


    if(!brand_id || !month){

        var error = '';

        if(!brand_id){
            error += 'Brand is required.\n';
        }

        if(!month){
            error += 'Month & Year is required.\n';
        }

        alert(error);
        
        $('.dataTables_processing', $('#checklist_table').closest('.dataTables_wrapper')).hide();
    }
    else{

        checklist_table.clear().draw();

        $.ajax({
            url: 'check_brand_budgetfolder_availability',
            method: 'GET',
            data: {'brand_id': brand_id, 'month': month, 'batch_no': batch_no, 'year': year},
            success: function(data){
                var branches = JSON.parse(data);
                console.log(branches)
                console.log($('#user_branch_id').val())
                var no_branches = branches.length;
                var wall_to_wall_done = 0;
                var budgetfolder_ready = 0;
                for(let i = 0; i < branches.length; i++){

                    var disable_create = 'disabled="disabled"';
                    // var disable_create = '';
                    // var wall_to_wall_label = '';
                    // var sales_forecast_label = '';
                    // var budgetfolder_label = '';

                    // console.log(branches[i].branch_id);

                    if((branches[i].budgetfolder_status == 'READY FOR CREATION') && ($('#user_branch_id').val() == 1 || $('#user_branch_id').val() == 200)){
                        disable_create = '';
                        console.log('TEst')

                    }

                    // if(branches[i].branch_id == 45){
                    //     disable_create = '';
                    // }

                    var disable_view = 'disabled="disabled"';

                    if(branches[i].wall_to_wall_status == 'DONE' || branches[i].wall_to_wall_status == 'SUBMITTED'){
                        disable_view = '';
                        
                    }

                    var wall_to_wall_date = '';
                    var clickable_label = branches[i].wall_to_wall_status;
                    var budgetfolder_status_clickable = branches[i].budgetfolder_status;
                    var budgetfolder_status_label = '';
                    var categoryforecast_status_label = '';
                    var order_clickable = '';

                    if (branches[i].autoorder_status === 'DONE') {
                        categoryforecast_status_label = 'DONE';
                        order_clickable = 'DONE'; 
                    } else if (branches[i].wall_to_wall_status === 'DONE' && branches[i].sales_forecast_status === 'DONE') {
                        categoryforecast_status_label = 'READY FOR CREATION';
                        order_clickable = '<a onclick="view_categoryforecast('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-wall-to-wall-date="'+wall_to_wall_date+'" data-sales-forecast="'+branches[i].sales_forecast+'" data-budget-date="'+branches[i].month_and_year+'" style="color: white !important;" >'+categoryforecast_status_label+'</a>';
                    } else {
                        categoryforecast_status_label = 'PENDING';
                        order_clickable = 'PENDING'; 
                    }


                    if(branches[i].wall_to_wall_date){
                        wall_to_wall_date = branches[i].wall_to_wall_date.currentinventory_dateupdated;
                    }

                    if(branches[i].wall_to_wall_status == 'DONE'){
                        // wall_to_wall_label = 'info';
                        wall_to_wall_done += 1;
                        clickable_label = '<a onclick="view_store_budgetfolder('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-wall-to-wall-date="'+wall_to_wall_date+'" style="color: white !important;" >'+branches[i].wall_to_wall_status+'</a>';
                    }

                    if(branches[i].wall_to_wall_status == 'VIEW SUBMITTED'){
                        // wall_to_wall_label = 'success';
                        wall_to_wall_done += 1;
                        clickable_label = '<a onclick="view_store_budgetfolder('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-wall-to-wall-date="'+wall_to_wall_date+'" style="color: white !important;" >'+branches[i].wall_to_wall_status+'</a>';
                    }

                    if(branches[i].budgetfolder_status == 'READY FOR CREATION'){
                        budgetfolder_status_label = 'success';
                    }

                    if(branches[i].budgetfolder_status == 'NOT READY' || branches[i].budgetfolder_status == 'DISAPPROVED'){
                        budgetfolder_status_label = 'danger';
                    }

                    if(branches[i].budgetfolder_status == 'VIEW CREATED'){
                        budgetfolder_ready += 1;
                        budgetfolder_status_label = 'success';
                        budgetfolder_status_clickable = '<a onclick="view_submitted_budgetfolder('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-budget-date="'+branches[i].budget_date+'" style="color: white !important;" data-company="'+branches[i].company+'" data-folder="'+branches[i].folder+'" data-original-target="'+branches[i].original_target+'" data-sales-forecast="'+branches[i].sales_forecast+'" data-wall-to-wall-date="'+wall_to_wall_date+'">'+branches[i].budgetfolder_status+'</a>';
                    }

                    if(branches[i].budgetfolder_status == 'APPROVED'){
                        budgetfolder_ready += 1;
                        budgetfolder_status_label = 'info';
                        budgetfolder_status_clickable = '<a onclick="view_submitted_budgetfolder('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-budget-date="'+branches[i].budget_date+'" style="color: white !important;" data-company="'+branches[i].company+'" data-folder="'+branches[i].folder+'" data-original-target="'+branches[i].original_target+'" data-sales-forecast="'+branches[i].sales_forecast+'" >'+branches[i].budgetfolder_status+'</a>';
                    }


                    checklist_table.row.add([
    
                        branches[i].month_and_year,
                        branches[i].brand,
                        branches[i].branch,
                        '<small class="label label-'+(branches[i].wall_to_wall_status == 'DONE' ? 'info' : branches[i].wall_to_wall_status == 'VIEW SUBMITTED' ? 'success' : 'danger')+'">'+clickable_label+'</small>',
                        '<small class="label label-'+(branches[i].sales_forecast_status == 'DONE' ? 'info' : 'danger')+'">'+(branches[i].sales_forecast_status == 'DONE' ? numberWithCommas(branches[i].sales_forecast_value) : branches[i].sales_forecast_status)+'</small>',
                       
                        '<small class="label label-'+(categoryforecast_status_label == 'DONE' ? 'info' : categoryforecast_status_label == 'READY FOR CREATION' ? 'success' : 'danger')+'">'+order_clickable+'</small>',
                        
                        '<small class="label label-'+budgetfolder_status_label+'">'+budgetfolder_status_clickable+'</small>',
                        // '<button class="btn btn-info btn-sm btn-flat" onclick="view_store_budgetfolder('+branches[i].branch_id+', this)" data-branch-name="'+branches[i].branch+'" data-wall-to-wall-date="'+wall_to_wall_date+'" '+disable_view+'>View</button>'
                        '<button class="btn btn-success btn-sm btn-flat" data-wall-to-wall-date="'+wall_to_wall_date+'" data-month-and-year="'+branches[i].month_and_year+'" onclick="create_store_budgetfolder_ho('+branches[i].branch_id+', this)" '+disable_create+'>Create</button>'
                        // '<button class="btn btn-warning btn-sm btn-flat" onclick="update_store_budgetfolder('+branches[i].branch_id+')">Update</button>'
    
                    ]).draw(false);

                }

                $('#no_branches').html("("+no_branches+")");
                $('#wall_to_wall_done').html("("+wall_to_wall_done+")");
                $('#budgetfolder_ready').html("("+budgetfolder_ready+")");
                checklist_table.draw();

                $('.dataTables_processing', $('#checklist_table').closest('.dataTables_wrapper')).hide();
            }
        })

    }

}

var csf_data = [];

var add_csf = function(budgetmonth, sales_forecast, branch_id, productservices) {
    csf_data.push({ budgetmonth: budgetmonth, sales_forecast: sales_forecast, branch_id: branch_id, productservices: productservices });
};

function view_categoryforecast (branch_id, button){
    $('#forecast_branch').val($(button).attr('data-branch-name'));
    $('#forecast_salesforecast').val($(button).attr('data-sales-forecast'));
    var budgetforecast_month = $(button).attr('data-budget-date');
    var sales_forecast = $(button).attr('data-sales-forecast');
    var forecast_table = $('#forecast_table').DataTable();
   
    forecast_table.clear().draw();
 
    $('#categoryforecast').modal('show');

    $('.dataTables_processing', $('#forecast_table').closest('.dataTables_wrapper')).show();

    $.ajax({
        url: 'get_categoryforecast',
        method: 'POST',
        data: { 'branch_id': branch_id, 
                'budgetforecast_month': budgetforecast_month
        },
        success: function(data){
            var result = JSON.parse(data);
            console.log(result)
            csf_data.push(budgetforecast_month, sales_forecast, branch_id, result);
            for (let i = 0; i < result.length; i++) {
                var qty = '';
                var amount = '';
                if (result[i].productservice_name === 'OTC') {
                    qty = 'N/A';
                    amount = numberWithCommas(parseFloat((result[i].percentage) / 100 * sales_forecast).toFixed(2));
                } else {
                    qty = Math.ceil(eval(eval(result[i].percentage / 100) * sales_forecast) / result[i].productservice_amount);
                    amount = numberWithCommas((qty * parseFloat(result[i].productservice_amount)).toFixed(2));
                }


                 forecast_table.row.add([
                        result[i].category_name,
                        result[i].productservice_name,
                        result[i].productservice_amount,
                        result[i].percentage+'%',
                        qty,
                        amount
                        
                    ]).draw(false);
            }
            $('.dataTables_processing', $('#forecast_table').closest('.dataTables_wrapper')).hide();
        }
    })
   
}

function generate_order(){
    var branch_id = csf_data[2];
    var budgetmonth = csf_data[0];
    $('#loading_modal').modal('show');
    window.location.href = $('#base_url').val()+'budgetfolder/auto_order/'+branch_id+'/'+budgetmonth;
}

function view_store_budgetfolder(branch_id, button){

    var wall_to_wall_date = $(button).attr('data-wall-to-wall-date') == 'null' ? '' : $(button).attr('data-wall-to-wall-date');

    $('#branch_name').val($(button).attr('data-branch-name'));
    $('#branch_id').val(branch_id);
    $('#wall_to_wall_date').val(wall_to_wall_date);

    var budgetfolder_table = $('#budgetfolder_table').DataTable();

    $('#store_budgetfolder').modal('show');

    budgetfolder_table.clear().draw();

    $('.dataTables_processing', $('#budgetfolder_table').closest('.dataTables_wrapper')).show();

    $.ajax({
        url: 'get_budgetfolder_inventory',
        method: 'GET',
        data: {'branch_id': branch_id, 'wall_to_wall_date': wall_to_wall_date},
        success: function(data){
            var items = JSON.parse(data);
            console.log(items)
            var sum = 0;
            var total_cost = 0;
            var otcs = 0;
            var consumables = 0;

            for(let i = 0; i < items.length; i++){

                var current_quantity = eval(items[i].current_quantity);

                if(current_quantity < 0){
                    current_quantity = 0;
                }

                total_cost = (eval(items[i].item_cost) * eval(current_quantity)).toFixed(2);
                sum = eval(sum) + eval(total_cost);

                var category_name = items[i].category_name;


                if(category_name.indexOf('Consumables') != -1){
                    consumables = eval(consumables) + eval(total_cost);
                }
                else{
                    otcs = eval(otcs) + eval(total_cost);
                }


                budgetfolder_table.row.add([

                    items[i].category_name,
                    items[i].item_description,
                    items[i].item_shade,
                    items[i].item_model,
                    items[i].item_color,
                    current_quantity,
                    items[i].item_sapuom,
                    items[i].item_cost,
                    numberWithCommas((eval(items[i].item_cost) * eval(current_quantity)).toFixed(2))

                ]).draw(false);

            }

            budgetfolder_table.row.add([

                '<h4 class="display-4">OTCs</h4>',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '<h4 class="display-4">'+numberWithCommas(otcs.toFixed(2))+'</h4>'

            ]).draw(false);

            budgetfolder_table.row.add([

                '<h4 class="display-4">Consumables</h4>',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '<h4 class="display-4">'+numberWithCommas(consumables.toFixed(2))+'</h4>'

            ]).draw(false);

            budgetfolder_table.row.add([

                '<h4 class="display-4">GRAND TOTAL</h4>',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '<h4 class="display-4">'+numberWithCommas(sum.toFixed(2))+'</h4>'

            ]).draw(false);

            $('#otc_up').html(numberWithCommas(otcs.toFixed(2)));
            $('#consumables_up').html(numberWithCommas(consumables.toFixed(2)));
            $('#total_up').html(numberWithCommas(sum.toFixed(2)));
            

            budgetfolder_table.draw();

            $('.dataTables_processing', $('#budgetfolder_table').closest('.dataTables_wrapper')).hide();
        }
    })
}

function create_store_budgetfolder_ho(branch_id, button){

    var wall_to_wall_date = $(button).attr('data-wall-to-wall-date') == 'null' ? '' : $(button).attr('data-wall-to-wall-date');
    var month_and_year = $(button).attr('data-month-and-year') == 'null' ? '' : $(button).attr('data-month-and-year');

    window.location.href = $('#base_url').val()+'budgetfolder/create_budgetfolder/'+branch_id+'/'+wall_to_wall_date+'/'+month_and_year;

    // $.ajax({
    //     url: $('#base_url').val()+'budgetfolder/create_budgetfolder',
    //     method: 'GET',
    //     data: {'branch_id': branch_id, 'wall_to_wall_date': wall_to_wall_date}
    // })
}  

function confirm_budgetfolder_inventory(){
    var branch_id = $('#branch_id').val();
    var wall_to_wall_date = $('#wall_to_wall_date').val();

    $.ajax({
        url: 'confirm_budgetfolder_inventory',
        method: 'POST',
        data: {'branch_id': branch_id, 'wall_to_wall_date': wall_to_wall_date},
        success: function(){
            check_brand_budgetfolder_availability();
            $('#store_budgetfolder').modal('hide');
            alert($('#branch_name').val()+' budget folder inventory has been approved.');
        }
    })
}

function view_submitted_budgetfolder(branch_id, button){

    var budget_date = $(button).attr('data-budget-date') == 'null' ? '' : $(button).attr('data-budget-date');
    var sales_forecast = $(button).attr('data-sales-forecast');
    var wall_to_wall_date = $(button).attr('data-wall-to-wall-date') == 'null' ? '' : $(button).attr('data-wall-to-wall-date');

    $('#submitted_branch_name').val($(button).attr('data-branch-name'));
    $('#submitted_budget_date').val(budget_date);
    $('#submitted_company').val($(button).attr('data-company'));
    $('#submitted_folder').val($(button).attr('data-folder'));
    $('#submitted_original_target').val($(button).attr('data-original-target'));
    $('#submitted_sales_forecast').val(sales_forecast);

    $('#submitted_budgetfolder').modal('show');
    
    var consumables_table = $('#consumables_table').DataTable();
    
    var otcs_table = $('#otcs_table').DataTable();

    consumables_table.clear().draw();

    otcs_table.clear().draw();

    $('.dataTables_processing', $('#consumables_table').closest('.dataTables_wrapper')).show();

    $('.dataTables_processing', $('#otcs_table').closest('.dataTables_wrapper')).show();

    $.ajax({
        url: 'view_submitted_budgetfolder_consumables',
        method: 'GET',
        data: {'branch_id': branch_id, 'budget_date': budget_date, 'wall_to_wall_date': wall_to_wall_date},
        success: function(data){
            var consumables = JSON.parse(data);
            var beginning_total = consumables['beginning_total'];
            var order_total = consumables['order_total'];
            console.log(consumables)
            var consumables_maximum_allowance = parseFloat(consumables['auto_order']) + parseFloat(consumables['manual_budget']) + parseFloat(consumables['non_consumable']);


            // var MXs = [111, 31, 84, 199]; // Hard coded branches of MXs

            // var multiplier = 0.05;

            // if(MXs.includes(branch_id)){
            //     multiplier = 0.08;
            // }

            for(let i = 0; i < consumables['items'].length; i++){


                var current_quantity_total = (eval(consumables['items'][i].item_cost) * eval(consumables['items'][i].current_quantity)).toFixed(2);

                var current_order_total = (eval(consumables['items'][i].item_cost) * eval(consumables['items'][i].budgetfoldermaterialorder_quantity)).toFixed(2);

                consumables_table.row.add([

                    consumables['items'][i].item_description,

                    consumables['items'][i].item_shade,

                    consumables['items'][i].item_color,

                    consumables['items'][i].item_model,

                    numberWithCommas(consumables['items'][i].item_cost),

                    consumables['items'][i].current_quantity,

                    consumables['items'][i].item_sapuom,

                    numberWithCommas(current_quantity_total),

                    consumables['items'][i].budgetfoldermaterialorder_quantity,

                    consumables['items'][i].item_sapuom,

                    numberWithCommas(current_order_total)
                
                ]).draw(false);

                // beginning_total = eval(beginning_total) + eval(current_quantity_total);
                
                // order_total = eval(order_total) + eval(current_order_total);

            }


            var consumables_over_under = (eval(consumables_maximum_allowance) - eval(order_total)).toFixed(2);

            $('.dataTables_processing', $('#consumables_table').closest('.dataTables_wrapper')).hide();

            consumables_table.row.add([

                '<h4 class="display-4">Beginning Inventory Total</h4>',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '<h4 class="display-4">'+numberWithCommas(beginning_total)+'</h4>'

            ]).draw(false);

            consumables_table.row.add([

                '<h4 class="display-4">Total Ordered Amount</h4>',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '<h4 class="display-4">'+numberWithCommas(order_total)+'</h4>'

            ]).draw(false);

            consumables_table.row.add([

                '<h4 class="display-4">Max. Allowance to Purchase</h4>',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '<h4 class="display-4">'+numberWithCommas(consumables_maximum_allowance.toFixed(2))+'</h4>'

            ]).draw(false);

            consumables_table.row.add([

                '<h4 class="display-4">Over / Under For this Month</h4>',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '',

                '<h4 class="display-4">'+numberWithCommas(consumables_over_under)+'</h4>'

            ]).draw(false);


            consumables_table.draw();


            $.ajax({
                url: 'view_submitted_budgetfolder_otcs',
                method: 'GET',
                data: {'branch_id': branch_id, 'budget_date': budget_date, 'wall_to_wall_date': wall_to_wall_date},
                success: function(data){
                    var otcs = JSON.parse(data);

                    beginning_total = otcs['beginning_total'];

                    order_total = otcs['order_total'];
        
                    for(let i = 0; i < otcs['items'].length; i++){
        
                        var current_quantity_total = (eval(otcs['items'][i].item_cost) * eval(otcs['items'][i].current_quantity)).toFixed(2);
        
                        var current_order_total = (eval(otcs['items'][i].item_cost) * eval(otcs['items'][i].budgetfolderotcorder_quantity)).toFixed(2);
        
                        otcs_table.row.add([
        
                            otcs['items'][i].item_description,
        
                            otcs['items'][i].item_shade,
        
                            otcs['items'][i].item_color,
        
                            otcs['items'][i].item_model,
        
                            numberWithCommas(otcs['items'][i].item_cost),
        
                            otcs['items'][i].current_quantity,
        
                            otcs['items'][i].item_sapuom,
        
                            numberWithCommas(current_quantity_total),
        
                            otcs['items'][i].budgetfolderotcorder_quantity,
        
                            otcs['items'][i].item_sapuom,
        
                            numberWithCommas(current_order_total)
                        
                        ]).draw(false);

                        // beginning_total = eval(beginning_total) + eval(current_quantity_total);
                
                        // order_total = eval(order_total) + eval(current_order_total);
        
                    }

                    var otcs_maximum_allowance = otcs['max_allowance'];

                    var otcs_over_under = (eval(otcs_maximum_allowance) - eval(order_total)).toFixed(2);
        
                    $('.dataTables_processing', $('#otcs_table').closest('.dataTables_wrapper')).hide();

                    otcs_table.row.add([

                        '<h4 class="display-4">Beginning Inventory Total</h4>',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '<h4 class="display-4">'+numberWithCommas(beginning_total)+'</h4>'
        
                    ]).draw(false);

                    otcs_table.row.add([

                        '<h4 class="display-4">Total Ordered Amount</h4>',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '<h4 class="display-4">'+numberWithCommas(order_total)+'</h4>'
        
                    ]).draw(false);
        
                    otcs_table.row.add([
        
                        '<h4 class="display-4">Max. Allowance to Purchase</h4>',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '<h4 class="display-4">'+numberWithCommas(otcs_maximum_allowance)+'</h4>'
        
                    ]).draw(false);
        
                    otcs_table.row.add([
        
                        '<h4 class="display-4">Over / Under For this Month</h4>',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '',
        
                        '<h4 class="display-4">'+numberWithCommas(otcs_over_under)+'</h4>'
        
                    ]).draw(false);
        
                    otcs_table.draw();

                }
            });
        }
    });
    
}
