$(function(){
  // get_turnaways()

  var turnaways_table = $('#turnaways_table').DataTable();
  turnaways_table.columns(0).visible(false);

  $('#add_turnaway_date').datepicker({
    autoclose: true
  });

  $('#turnaway_date').datepicker({
    autoclose: true
  });

  $('#daterange_btn').daterangepicker(
    {
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment()
    },
    function (start, end) {
      $('#daterange_btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      $('#start').val(start.format('MM/DD/YYYY'));
      $('#end').val(end.format('MM/DD/YYYY'));
    }
  )

  $('.select2').select2();
})

$('#add_turnaway_modal').on('show.bs.modal', function(){
  $('#add_turnaway_error').empty()
  $('#add_turnaway_modal').find('[class="modal-footer"]').show();
  $('#add_turnaway_alert_success').hide();
  $('#add_turnaway_form').show();
  $("#add_turnaway_date").datepicker().on('show.bs.modal', function(event) {
      // prevent datepicker from firing bootstrap modal "show.bs.modal"
    event.stopPropagation(); 
  });
  $('#add_turnaway_date').val('')
  $('#add_turnaway_type').val('')
  $('#add_turnaway_turnaway').val('')
  $('#add_turnaway_remarks').val('')
});

$('#turnaway_modal').on('show.bs.modal', function () {
  $('#turnaway_type').empty()
  $('#turnaway_error').empty()
  $('#turnaway_modal').find('[class="modal-footer"]').show();
  $('#turnaway_alert_success').hide();
  $('#turnaway_form').show();
  $("#turnaway_date").datepicker().on('show.bs.modal', function (event) {
    // prevent datepicker from firing bootstrap modal "show.bs.modal"
    event.stopPropagation();
  });
  $('#turnaway_date').val('')
  $('#turnaway_turnaway').val('')
  $('#turnaway_remarks').val('')
  disable_editing()
});

function get_turnaways(){
  $('#turnaways_table').dataTable().fnDestroy();

  var turnaways_table = $('#turnaways_table').DataTable({
    'bAutoWidth' : true,
    'processing': true,
    'ajax' : { 
    'url': 'get_turnaways',
    'method' : 'GET'
    },
    'createdRow': function( row, data) {
      $(row).attr('onclick', 'get_turnaway("'+ data[0] +'")');
    }
  })

  // var joborders_table = $('#joborders_table').dataTable({
  //     'bAutoWidth' : true,
  //     'processing': true,
      // 'ajax' : { 
      // 'url': 'get_customers',
      // 'method' : 'GET'
      // },
      // 'createdRow': function( row, data) {
      // $(row).attr('onclick', 'get_customer("'+ data[0] +'")');
      // }
  // });
}

function add_turnaway(){
  $('#add_turnaway_error').empty()

  var date = $('#add_turnaway_date').val()
  var turnaway = $('#add_turnaway_turnaway').val()
  var date_button = $.trim($('#daterange_btn').find('span').text());

  if(!date || !turnaway){
    if(!date){
      $('#add_turnaway_error').append('<p class="text-danger">Date is required.</p>')
    }
    if(!turnaway){
      $('#add_turnaway_error').append('<p class="text-danger">Turn Away is required.</p>')
    }
  }
  else{
    $.ajax({
      url: 'add_turnaway',
      method: 'POST',
      data: $('#add_turnaway_form').serialize(),
      success: function(data){
        if(data==true){
          $('#add_turnaway_alert_success').show(400);
          $('#add_turnaway_form').hide()
          $('#add_turnaway_modal').find('[class="modal-footer"]').hide();
          // get_turnaways();
          if(date_button != 'Select date'){
            daterange_search()
          }
        }
      }
    })
  }
}

function get_turnaway(id){
  $('#turnaway_modal').modal('show');

  $.ajax({
    url: 'get_turnaway/'+id,
    async: false,
    success: function(data){
      var turnaway = JSON.parse(data);
      console.log(turnaway)

      var type_options = '<option value="'+turnaway.turnaway_type+'" hidden selected>'+turnaway.turnaway_type+'</option>'+'<option value="No Card Terminal">No Card Terminal</option><option value="No Service Provider">No Service Provider</option><option value="Service Not Available">Service Not Available</option><option value="Others">Others</option>';

      $('#turnaway_id').val(turnaway.turnaway_id);
      $('#turnaway_date').val(turnaway.turnaway_date);
      $('#turnaway_type').append(type_options);
      $('#turnaway_turnaway').val(turnaway.turnaway_quantity);
      $('#turnaway_remarks').val(turnaway.turnaway_remarks);
    }
  })
}

function enable_editing(){
  $('#turnaway_date').removeAttr('disabled');
  $('#turnaway_type').removeAttr('disabled');
  $('#turnaway_turnaway').removeAttr('disabled');
  $('#turnaway_remarks').removeAttr('disabled');
  $('#turnaway_update_button').removeAttr('disabled');
}

function disable_editing(){
  $('#turnaway_date').attr('disabled', 'disabled');
  $('#turnaway_type').attr('disabled', 'disabled');
  $('#turnaway_turnaway').attr('disabled', 'disabled');
  $('#turnaway_remarks').attr('disabled', 'disabled');
  $('#turnaway_update_button').attr('disabled', 'disabled');
}

function delete_turnaway(){
  if (confirm('Are you sure you want to delete Turnaway ID: ' + $('#turnaway_id').val())) {
    $.ajax({
      url: 'delete_turnaway/' + $('#turnaway_id').val(),
      async: false,
      success: function (data) {
        $('#turnaway_function').text('deleted');
        $('#turnaway_function_id').text($('#turnaway_id').val());
        $('#function_id').text($('#turnaway_id').val());
        $('#turnaway_alert_success').show(400);
        $('#turnaway_form').hide();
        $('#turnaway_modal').find('[class="modal-footer"]').hide();
        // get_turnaways();
        daterange_search()
      }
    })
  }
}

function update_turnaway(){
  var date = $('#turnaway_date').val()
  var turnaway = $('#turnaway_turnaway').val()

  if (!date || !turnaway) {
    if (!date) {
      $('#turnaway_error').append('<p class="text-danger">Date is required.</p>')
    }
    if (!turnaway) {
      $('#turnaway_error').append('<p class="text-danger">Turn Away is required.</p>')
    }
  }
  else {
    $.ajax({
      url: 'update_turnaway/' + $('#turnaway_id').val(),
      method: 'POST',
      data: $('#turnaway_form').serialize(),
      success: function (data) {
        if (data == true) {
          $('#turnaway_function').text('updated');
          $('#turnaway_function_id').text($('#turnaway_id').val());
          $('#turnaway_alert_success').show(400);
          $('#turnaway_form').hide()
          $('#turnaway_modal').find('[class="modal-footer"]').hide();
          // get_turnaways();
          daterange_search();
        }
      }
    })
  }
}

function daterange_search(){
  var date = $.trim($('#daterange_btn').find('span').text());
  var start = $('#start').val();
  var end = $('#end').val();
  var branch_id = $('#branches').select2('val');
  var error = '';

  if (date == 'Select date' || (typeof $('#branches').val() != 'undefined' && !$('#branches').val())){
    if(typeof $('#branches').val() != 'undefined' && !$('#branches').val()){
      error += 'Branch is required.\n';
    }

    if(date == 'Select date'){
      error += 'Date is required.';
    }

    alert(error)
  }
  else{

    if(start == end){
      $('#turnaways_table').dataTable().fnDestroy();

      var turnaways_table = $('#turnaways_table').DataTable({
        'bAutoWidth': true,
        'processing': true,
        'ajax': {
          'url': 'get_turnaways_by_date',
          'method': 'POST',
          'data': {'date_start': start, 'branch_id': branch_id}
        },
        'createdRow': function (row, data) {
          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');
        }
      })

      turnaways_table.columns(0).visible(false);

      turnaways_table.order([1, 'desc']).draw();

    }
    else{
      $('#turnaways_table').dataTable().fnDestroy();

      var turnaways_table = $('#turnaways_table').DataTable({
        'bAutoWidth': true,
        'processing': true,
        'ajax': {
          'url': 'get_turnaways_by_date',
          'method': 'POST',
          'data': { 'date_start': start, 'date_end': end, 'branch_id': branch_id }
        },
        'createdRow': function (row, data) {
          $(row).attr('onclick', 'get_turnaway("' + data[0] + '")');
        }
      })

      turnaways_table.columns(0).visible(false);

      turnaways_table.order([1, 'desc']).draw();
    }
  }
}
