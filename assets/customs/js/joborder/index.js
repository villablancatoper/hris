$(function(){


  var customers_table_new = $('#customers_table_new').DataTable({

    'bAutoWidth': true,

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    }

  });

  var customers_table_old = $('#customers_table_old').DataTable({

    'bAutoWidth': true,

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    }

  });



  $('#customer_birthday').datepicker({

    autoclose: true

  });

  $('#add_customer_customer_birthday').datepicker({

    autoclose: true

  });

  $('#transfer_customer_birthday').datepicker({

    autoclose: true

  });



  $('.select2').select2();



  $("#transfer_customer_birthday").datepicker().on('hide.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });





  // customers_table_new.columns(0).visible(false);

  customers_table_new.order([1, 'asc'])



//   customers_table_old.columns(0).visible(false);
//   customers_table_old.columns(1).visible(false);

  customers_table_old.order([1, 'asc'])



  var joborders_table = $('#joborders_table').DataTable({

    'bAutoWidth': true,

    'processing': true,

    "language": {

      processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '

    }

  });

  

  joborders_table.columns(0).visible(false);



  $('#customers_table_new tbody').on('dblclick', 'tr', function () {

    $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).show();

    var search_item = $(this).children('td:eq(0)').text();



    joborders_table.clear()



    $.ajax({

      url: 'joborder/get_joborders_by_customer_id',

      method: 'GET',

      data: {'search_item': search_item},

      success: function(data){

        var joborders = JSON.parse(data);
        console.log(joborders)
        if(joborders.length == 0){

          joborders_table.clear().draw();

          alert('No job orders found.')

          $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).hide();

        }

        else{



          for(var i = 0; i < joborders.length; i++){

            var transaction_id = null

            if(joborders[i].branch_id == $('#user_branch_id').val()){
              transaction_id = joborders[i].transaction_id;
            }

            joborders_table.row.add([

              transaction_id,

              joborders[i].transaction_date,

              joborders[i].brand_name,

              joborders[i].branch_name,

              joborders[i].serviceprovider_name,

              joborders[i].services_performed

            ]).draw(false)

          }



          joborders_table.order([1, 'desc']).draw();

        }

        $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).hide();

      }

    })

  });



  $('#customers_table_old tbody').on('dblclick', 'tr', function () {
      
      $(this).attr('style', 'background-color: #cce8ff !important; color: black;').siblings().removeAttr("style");

    var customer_id = $(this).children('td:eq(1)').text();

    joborders_table.clear().draw();

    $('#workaround').val(customer_id);

    $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).show();

    $.ajax({

      url: 'joborder/get_joborders_on_job_trans_e',

      method: 'GET',

      data: {'customer_id': customer_id},

      success: function(data){

        var joborders = JSON.parse(data);

        if(joborders.length == 0){

          joborders_table.clear().draw;

          alert('No job order found.')
          $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).hide();
        }

        else{



          for(var i = 0; i < joborders.length; i++){

            joborders_table.row.add([

              '',

              joborders[i].JT_DATESERVE,

              joborders[i].brand_name,

              joborders[i].branch_name,

              joborders[i].serviceprovider_name,

            //   joborders[i].services_performed

               ''

            ]).draw(false)

          }

        // console.log(joborders)

          joborders_table.order([1, 'desc']).draw();
          $('.dataTables_processing', $('#joborders_table').closest('.dataTables_wrapper')).hide();
        }

      }

    })

  });



  $('#customers_table_new tbody').on('click', 'tr', function () {

    $(this).attr('style', 'background-color: #cce8ff !important; color: black;').siblings().removeAttr("style");

    var search_item = $(this).children('td:eq(0)').text();

    $('#new_job_entry_id').val(search_item);

    $('#new_job_entry_button').removeAttr('disabled');

  });



  // $('#customers_table_new tbody').on('dblclick', 'tr', function () {

  //   alert('test')

  // });



  $('#joborders_table tbody').on('dblclick', 'tr', function () {

    $(this).attr('style', 'background-color: #cce8ff !important; color: black;').siblings().removeAttr("style");

    var data = joborders_table.row(this).data();

    console.log(data[0]);
    
    if(data[0]){

        if($('#workaround').val()){
    
          window.location.href = 'joborder/entry/69'+'/'+data[0]+'/'+$('#workaround').val()
    
        }
    
        else{
    
          window.location.href = 'joborder/entry/'+$('#new_job_entry_id').val()+'/'+data[0]
    
        }
    }
  });



  $('.select2').select2();

})



function new_job_entry(){

  var new_job_entry_id = $('#new_job_entry_id').val();

  if(new_job_entry_id){

    window.location.href = 'joborder/entry/'+new_job_entry_id;

  }

  else{

    alert('Select customer to create new job order!');

  }

}



function search_customer(customer_firstname){
  var joborders_table = $('#joborders_table').DataTable();
  joborders_table.clear().draw();
  
  var search_item = $('#search_box').val();
  
  if(customer_firstname){
      search_item = customer_firstname;
  }

  if(!search_item){

    alert('Enter value first on search box!');

  }
  else if(search_item.length < 3){
      alert('Minimum characters to search is 3.');
  }

  else{

    var customers_table_new = $('#customers_table_new').DataTable();

    var customers_table_old = $('#customers_table_old').DataTable();
    
    if(customer_firstname){
        $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).show();
    
        $.ajax({
    
          url: $('#base_url').val()+'customer/'+'search_customers_new',
    
          method: 'GET',
    
          data: {'search_item': search_item},
    
          success: function(data){
    
            var customers_new = JSON.parse(data);
    
            if(customers_new.length == 0){
    
              customers_table_new.clear().draw();
              $('#search_box').val('');
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
            else{
                customers_table_new.clear()
    
                for(var i = 0; i < customers_new.length; i++){

                        let str_first = customers_new[i].customer_firstname.split(" ");
                        let str_last = customers_new[i].customer_lastname.split(" ");

                        let first_name = '';
                        let last_name = '';

                        let lower_first = '';
                        let lower_last = '';

                        for(let j = 0; j < str_first.length; j++){
                          let lower_first = (str_first[j]+' ').toLowerCase()
                          first_name += lower_first.charAt(0).toUpperCase() + lower_first.slice(1);
                        }

                        for(let j = 0; j < str_last.length; j++){
                          let lower_last = (str_last[j]+' ').toLowerCase()
                          last_name += lower_last.charAt(0).toUpperCase() + lower_last.slice(1);
                        }
    
                        customers_table_new.row.add([
        
                        customers_new[i].customer_id,
        
                        first_name.charAt(0).toUpperCase() + first_name.slice(1),
        
                        last_name.charAt(0).toUpperCase() + last_name.slice(1),
        
                        customers_new[i].customer_email,
        
                        '<button class="btn btn-info btn-sm" onclick="get_customer_new('+customers_new[i].customer_id+')"><i class="fa fa-search"></i></button>'
    
                    ]).draw(false)
                }
                $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
          }
    
        });
    }
    else{
        $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).show();
        $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).show();
    
        $.ajax({
    
          url: $('#base_url').val()+'customer/'+'search_customers_new',
    
          method: 'GET',
    
          data: {'search_item': search_item},
    
          success: function(data){
    
            var customers_new = JSON.parse(data);
    
            if(customers_new.length == 0){
    
              customers_table_new.clear().draw();
              $('#search_box').val('');
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
            else{
                customers_table_new.clear();
                for(var i = 0; i < customers_new.length; i++){

                  let str_first = customers_new[i].customer_firstname.split(" ");
                  let str_last = customers_new[i].customer_lastname.split(" ");

                  let first_name = '';
                  let last_name = '';

                  let lower_first = '';
                  let lower_last = '';

                  for(let j = 0; j < str_first.length; j++){
                    let lower_first = (str_first[j]+' ').toLowerCase()
                    first_name += lower_first.charAt(0).toUpperCase() + lower_first.slice(1);
                  }

                  for(let j = 0; j < str_last.length; j++){
                    let lower_last = (str_last[j]+' ').toLowerCase()
                    last_name += lower_last.charAt(0).toUpperCase() + lower_last.slice(1);
                  }

                  customers_table_new.row.add([
  
                    customers_new[i].customer_id,
    
                    first_name,
    
                    last_name,
    
                    customers_new[i].customer_email,
    
                    '<button class="btn btn-info btn-sm" onclick="get_customer_new('+customers_new[i].customer_id+')"><i class="fa fa-search"></i></button>'

                  ]).draw(false)
                }
              $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
            }
    
          }
    
        });
    
      
        customers_table_new.order([
    
          1, 'asc'
    
        ])
    
        $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).show();
    
        // $.ajax({
    
        //   url: $('#base_url').val()+'customer/'+'search_customers_old',
    
        //   method: 'GET',
    
        //   data: {'search_item': search_item},
    
        //   success: function(data){
    
        //     var customers_old = JSON.parse(data);
    
        //     if(customers_old.length == 0){
    
        //       customers_table_old.clear().draw();
        //       $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).hide();
        //     }
    
        //     else{
                
        //         customers_table_old.clear()
    
        //       for(var i = 0; i < customers_old.length; i++){
    
        //         customers_table_old.row.add([

        //           '<button class="btn btn-primary btn-sm" onclick="get_customer_old('+customers_old[i].C_IDNO+')">Transfer</button>',
    
        //           customers_old[i].C_IDNO,
                  
        //           customers_old[i].branch_name,
    
        //           customers_old[i].C_FNAME,
    
        //           customers_old[i].C_LNAME,
    
        //           customers_old[i].C_MOBILENO,
    
        //           customers_old[i].C_SEX,
    
        //           customers_old[i].age_range,
    
        //           customers_old[i].location_name
    
        //         ]).draw(false)
    
        //       }
        //       $('.dataTables_processing', $('#customers_table_old').closest('.dataTables_wrapper')).hide();
        //     }
    
        //   }
    
        // });
    
    
    
        // customers_table_old.order([
    
        //   1, 'asc'
    
        // ])
    }

  }

}



$('#customer_modal').on('show.bs.modal', function(){

  $('#customer_modal').find('[class="modal-footer"]').show();

  $('#customer_alert_success').hide();

  $('#customer_form').show();

  disable_editing();

  $("#customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });

});



$('#transfer_customer_modal').on('show.bs.modal', function(){

  $('#transfer_customer_modal').find('[class="modal-footer"]').show();

  $('#transfer_customer_alert_success').hide();

  $('#transfer_customer_form').show();

  $("#transfer_customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });

});



$('#transfer_customer_modal').on('hide.bs.modal', function(){

  $('#transfer_customer_firstname').val('');

  $('#transfer_customer_lastname').val('');

  $('#transfer_customer_gender').val('');

  $('#transfer_customer_phoneno').val('');

  $('#transfer_customer_email').val('');

  $('#transfer_customer_mobileno').val('');

  $('#transfer_customer_location_id').val('');

  $('#transfer_customer_age_id').val('');

  $('#transfer_customer_occupation_id').val('');

  $('#transfer_customer_birthday').val('');

});



$('#add_customer_modal').on('show.bs.modal', function(){

  $('#add_customer_error').empty();

  $('#specify').empty();

  $('#add_customer_modal').find('[class="modal-footer"]').show();

  $('#add_customer_alert_success').hide();

  $('#add_customer_form').show();

  $("#add_customer_customer_birthday").datepicker().on('show.bs.modal', function(event) {

      // prevent datepicker from firing bootstrap modal "show.bs.modal"

    event.stopPropagation(); 

  });



  $('#add_customer_firstname').val('');

  $('#add_customer_lastname').val('');

  $('#add_customer_gender').empty('');



  $('#add_customer_gender').append('<option value="">Select gender</option><option value="Male">Male</option><option value="Female">Female</option>');



  $('#add_customer_phoneno').val('');

  $('#add_customer_mobileno').val('');

  $('#add_customer_email').val('');

  $('#add_customer_location_name').val('').trigger('change');

  $('#add_customer_occupation_name').val('').trigger('change');

  $('#add_customer_customer_birthday').val('');

  $('#add_customer_agerange_range').val('').trigger('change');

  $('#add_hear_about_us').val('').trigger('change');

});

$('#add_hear_about_us').on('change', function(){
  let specify = '<label for="add_specify">Please specify:</label><input id="add_specify" name="add_specify" class="form-control" type="text">';

  if($('#add_hear_about_us').val() == 'Others'){
    $('#specify').append(specify);
  }
  else{
    $('#specify').empty();
  }
}); 

$('#customer_hear_about_us').on('change', function(){
  let customer_specified = '<label for="customer_specified">Please specify:</label><input id="customer_specified" name="customer_specified" class="form-control" type="text">';

  if($('#customer_hear_about_us').val() == 'Others'){

    if(!$('#customer_specified').val()){
      $('#specified').append(customer_specified);
    }
  }
  else{
    $('#specified').empty();
  }
}); 



function get_customer_new(id){

  $('#customer_modal').modal('show');

  $('#customer_gender').empty();



  $.ajax({

    url: $('#base_url').val()+'customer/'+'get_customer/'+id,

    success: function(data){

      var customer = JSON.parse(data);

      $('#customer_id').val(customer.customer_id)

      $('#customer_firstname').val(customer.customer_firstname)

      $('#customer_lastname').val(customer.customer_lastname)



      var customer_gender = '';

      

      if(customer.customer_gender != null){

        customer_gender = customer.customer_gender ;

      }

      

      var customer_gender_option = '<option value="'+ customer_gender+'" hidden selected>'+ customer_gender+'</option><option value="Male">Male</option><option value="Female">Female</option>'



      $('#customer_gender').append(customer_gender_option)

      $('#customer_phoneno').val(customer.customer_phoneno)

      $('#customer_mobileno').val(customer.customer_mobileno)



      var selected_location = $('#location_id').find('option[value="' + customer.location_id + '"]');

      $('#location_id').val(selected_location.attr('value')).trigger('change');



      var selected_occupation = $('#occupation_id').find('option[value="' + customer.occupation_id + '"]');

      $('#occupation_id').val(selected_occupation.attr('value')).trigger('change');

      

      $('#customer_birthday').val(customer.customer_birthday)



      var selected_age = $('#age_id').find('option[value="' + customer.age_id + '"]');

      $('#age_id').val(selected_age.attr('value')).trigger('change');


      var selected_about_us = $('#customer_hear_about_us').find('option[value="' + customer.customer_aboutus + '"]');
      let hear_about_us = ['Facebook', 'Instagram', 'Events', 'Flyers', 'Mail Ads', 'Magazine', 'Blog/Articles', 'Referred'];

      if(hear_about_us.indexOf(customer.customer_aboutus) != -1){
        $('#customer_hear_about_us').val(customer.customer_aboutus).trigger('change');
      }
      else{

        if(!customer.customer_aboutus){
          $('#customer_hear_about_us').val('').trigger('change')
        }
        else{
          let customer_specified = '<label for="customer_specified">Please specify:</label><input id="customer_specified" disabled="disabled" name="customer_specified" class="form-control" type="text">';
  
          $('#specified').append(customer_specified);
          $('#customer_specified').val(customer.customer_aboutus);
          $('#customer_hear_about_us').val('Others').trigger('change')
        }

      }




      $('#customer_email').val(customer.customer_email)

      $('#customer_foottraffic').val(customer.customer_foottraffic)

    }

  })

}



$(document).on('keypress', function (e) {

  if (e.which == 13) {

    search_customer();

  }

});



function update_customer(){

  $.ajax({

    url: $('#base_url').val()+'customer/'+'update_customer/'+$('#customer_id').val(),

    method: 'POST',

    data: $('#customer_form').serialize(),

    success: function(data){

      if(data == true){

        $('#function').text('updated');

        $('#function_id').text($('#customer_id').val());

        $('#customer_alert_success').show(400);

        $('#customer_form').hide();

        $('#customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#customer_firstname').val())

        $('#specified').empty();

      }

    }

  })

}



function delete_customer(){

  if(confirm('Are you sure you want to delete Customer ID: '+$('#customer_id').val())){

    $.ajax({

      url: $('#base_url').val()+'customer/'+'delete_customer/'+$('#customer_id').val(),

      success: function(){

        $('#function').text('deleted');

        $('#function_id').text($('#customer_id').val());

        $('#customer_alert_success').show(400);

        $('#customer_form').hide();

        $('#customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#customer_firstname').val());

      }

    })

  }

}



function enable_editing(){

  $('#customer_firstname').removeAttr('disabled')

  $('#customer_lastname').removeAttr('disabled')

  $('#customer_gender').removeAttr('disabled')

  $('#customer_phoneno').removeAttr('disabled')

  $('#customer_mobileno').removeAttr('disabled')

  $('#location_id').removeAttr('disabled')

  $('#occupation_id').removeAttr('disabled')

  $('#customer_birthday').removeAttr('disabled')

  $('#age_id').removeAttr('disabled')

  $('#customer_email').removeAttr('disabled');

  $('#customer_hear_about_us').removeAttr('disabled');

  $('#update_customer_button').removeAttr('disabled');

  $('#customer_specified').removeAttr('disabled');

}



function disable_editing(){

  $('#customer_firstname').attr('disabled', 'disabled')

  $('#customer_lastname').attr('disabled', 'disabled')

  $('#customer_gender').attr('disabled', 'disabled')

  $('#customer_phoneno').attr('disabled', 'disabled')

  $('#customer_mobileno').attr('disabled', 'disabled')

  $('#location_id').attr('disabled', 'disabled')

  $('#occupation_id').attr('disabled', 'disabled')

  $('#customer_birthday').attr('disabled', 'disabled')

  $('#age_id').attr('disabled', 'disabled')

  $('#customer_email').attr('disabled', 'disabled');
  
  $('#customer_hear_about_us').attr('disabled', 'disabled');

  $('#update_customer_button').attr('disabled', 'disabled');

}



function test(){

  $('#add_customer_modal').modal('show');

}



function add_customer(){
  $('#add_customer_button').attr('disabled', 'disabled');

  $('#add_customer_error').empty();



  var firstname = $('#add_customer_firstname').val();

  var hear_about_us = $('#add_hear_about_us').val();



  if(!firstname  || !hear_about_us ){

    if(!firstname){
      $('#add_customer_error').append('<p class="text-danger">Firstname is required.</p>');
    }

    if(!hear_about_us){
      $('#add_customer_error').append('<p class="text-danger">"How did the client hear about us?" is required.</p>');
    }
    
    $('#add_customer_button').removeAttr('disabled');
  }

  else{

    $.ajax({

      url: $('#base_url').val()+'customer/'+'add_customer',

      method: 'POST',

      data: $('#add_customer_form').serialize(),

      success: function(data){

        if(data == true){
          $('#add_customer_button').removeAttr('disabled');

          $('#add_customer_function').text('added');

          $('#add_customer_function_name').text($('#add_customer_firstname').val() + ' ' +$('#add_customer_lastname').val() );

          $('#add_customer_alert_success').show(400);

          $('#add_customer_form').hide();

          $('#add_customer_modal').find('[class="modal-footer"]').hide();

          search_customer($('#add_customer_firstname').val());

        }
        else{
          $('#add_customer_button').removeAttr('disabled');

          $('#add_customer_function').text('added');

          $('#add_customer_function_name').text($('#add_customer_firstname').val() + ' ' +$('#add_customer_lastname').val() );

          $('#add_customer_alert_success').show(400);

          $('#add_customer_form').hide();

          $('#add_customer_modal').find('[class="modal-footer"]').hide();

          search_customer($('#add_customer_firstname').val());
        }

      }

    })

  }

}



function get_customer_old(C_IDNO){

  if(confirm('Are you sure you want transfer this data to the new database?')){
      $('#loading_modal').modal('show');

    $.ajax({

      url: $('#base_url').val()+'customer/'+'is_customer_data_transferred/'+C_IDNO,

      success: function(data){

        if(data == true){

          alert("This customer has already been transferred to the new database");
          $('#loading_modal').modal('hide');
        }

        else{

          $.ajax({

            url: $('#base_url').val()+'customer/'+'get_customer_old/'+C_IDNO,

            success: function(data){

              var customer_old = JSON.parse(data);

      

              $('#transfer_customer_C_IDNO').val(C_IDNO)

              $('#transfer_customer_firstname').val(customer_old.C_FNAME);

              $('#transfer_customer_lastname').val(customer_old.C_LNAME);

              $('#transfer_customer_gender').append('<option value="'+customer_old.C_SEX+'" hidden selected>'+customer_old.C_SEX+'</option>');

              $('#transfer_customer_mobileno').val(customer_old.C_MOBILENO);

      

              var transfer_selected_location = $('#transfer_customer_location_id').find('option[value="' + customer_old.location_id + '"]');

              $('#transfer_customer_location_id').val(transfer_selected_location.attr('value')).trigger('change');

      

              var transfer_selected_age = $('#transfer_customer_age_id').find('option[value="' + customer_old.age_id + '"]');

              $('#transfer_customer_age_id').val(transfer_selected_age.attr('value')).trigger('change');

              $('#transfer_customer_modal').modal('show');

            }

          })
        }
        $('#loading_modal').modal('hide');
      }

    })

  }
  else{
      $('#loading_modal').modal('hide');
  }
    // $('#loading_modal').removeAttr('class');
    // $('#loading_modal').attr('class', 'modal fade centered-modal');
}



function transfer_customer_old_data(){

  $('#transfer_customer_error').empty()

  $('#transfer_customer_button').attr('disabled', 'disabled');

  var error = null;



  // var firstname = $('#transfer_customer_firstname').val();

  // var lastname = $('#transfer_customer_lastname').val();

  // var gender = $('#transfer_customer_gender').val();

  // var phoneno = $('#transfer_customer_phoneno').val();

  // var mobileno = $('#transfer_customer_mobileno').val();

  // var email = $('#transfer_customer_email').val();



  if(!$('#transfer_customer_firstname').val()){

    error += '<p class="text-danger">Firstname is required.</p>'

    $('#transfer_customer_error').append(error);
    $('#transfer_customer_button').removeAttr('disabled');

  }

  else{

    $.ajax({

      url: $('#base_url').val()+'customer/'+'transfer_customer_old_data/'+$('#transfer_customer_C_IDNO').val(),

      method: 'POST',

      data: $('#transfer_customer_form').serialize(),

      success: function(){

        $('#transfer_customer_function').text('transferred');

        $('#transfer_customer_function_name').text($('#transfer_customer_firstname').val() + ' ' +$('#transfer_customer_lastname').val());

        $('#transfer_customer_alert_success').show(400);

        $('#transfer_customer_form').hide();

        $('#transfer_customer_modal').find('[class="modal-footer"]').hide();

        search_customer($('#transfer_customer_firstname').val());

        $('#transfer_customer_button').removeAttr('disabled');

      }

    })

  }

}

function get_all_customers(){
  var customers_table_new = $('#customers_table_new').DataTable();

  $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).show();

  $.ajax({
    url: 'joborder/get_all_customers',
    method: 'GET',
    success: function(data){
      var customers_new = JSON.parse(data);

      customers_table_new.clear();
    
      for(var i = 0; i < customers_new.length; i++){

          customers_table_new.row.add([

            customers_new[i].customer_id,

            customers_new[i].customer_firstname,

            customers_new[i].customer_lastname,

            customers_new[i].customer_mobileno,

            '<button class="btn btn-info btn-sm" onclick="get_customer_new('+customers_new[i].customer_id+')"><i class="fa fa-search"></i></button>'

        ]).draw(false)
      }
      $('.dataTables_processing', $('#customers_table_new').closest('.dataTables_wrapper')).hide();
    }
  })
}
