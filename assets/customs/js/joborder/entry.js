$(function(){


  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({

    checkboxClass: 'icheckbox_minimal-blue',

    radioClass   : 'iradio_minimal-blue'

  })



  $('#date_rendered').datepicker({

    autoclose: true,

    changeMonth: false

  })



  $('.gc-details').hide();



  $('ins[class="iCheck-helper"]').click(function(){

    var parent = $(this).parent().attr('aria-checked');

    var checkbox = $(this).siblings(0).attr('id');



    if(checkbox == 'checkbox_cash'){

        if(parent=='true'){

            $('#input_cash').removeAttr('disabled');

        }

        else{

            $('#input_cash').attr('disabled', 'disabled');

        }

    }



    if(checkbox == 'checkbox_credit'){

        if(parent=='true'){

            $('#input_credit').removeAttr('disabled');

        }

        else{

            $('#input_credit').attr('disabled', 'disabled');

        }

    }



    if(checkbox == 'checkbox_gc'){

      if(parent=='true'){

        $('.gc-details').show(400);

      }

      else{

          $('.gc-details').hide(400);

      }

  }

  });
  

  $('.select2').select2();



  var gc_details = $('#gc_details').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false, 

    'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
    },

    "lengthChange": false,
    
    "bInfo" : false

  })



  gc_details.columns(0).visible(false);

  

  var product_consumables_table = $('#product_consumables_table').DataTable({

    'lengthChange': false,
    "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]]

  })



  product_consumables_table.columns(0).visible(false);

  

  $(document).on('keypress', function (e) {

    if (e.which == 13) { // Enter

      if($('#search_gc').is(':focus')){

        search_gc();

      }

      if($('#search_retail_service').is(':focus')){

        search_product_service();

      }

      if($('#search_product_service').is(':focus')){

        search_product_service();

      }

      if($('#gc_pin').is(':focus')){

        validate_gc();

      }

    }

  });

  

  var product_services_table = $('#product_services_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false, 

    'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
    },

    "lengthChange": false,
    
    "bInfo" : false
  });

  $('#product_services_table').on( 'click', 'tbody tr button', function () {

    var parent = $(this).parent();

    var grandparent = parent.parent();

    var input_total_sales = parseFloat($('#total_sales').val());

    var product_amount = parseFloat($(grandparent).find('td:nth-child(3)').text());

    var search = parseFloat($(grandparent).find('td:nth-child(1)').text());

    var total_sales = input_total_sales - product_amount;



    var nodes = [];



    product_consumables_table.rows().every( function(rowIdx, tableLoop, rowLoop) {

      if ( this.data()[1] == search) nodes.push(this.node())

    })

    

    nodes.forEach(function(node) {

      product_consumables_table.row(node).remove().search('').draw()

    })



    $('#total_sales').val((+'0.00'+total_sales).toFixed(2));

    product_services_table.row(grandparent).remove().draw();

  });



  product_services_table.columns(0).visible(false);



  var retail_services_table = $('#retail_services_table').DataTable({

    'processing': true,

    'bAutoWidth': false,

    'bSort': false, 

    'language': {

        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    
    },

    "lengthChange": false,
    
    "bInfo" : false

  });

  $('#retail_services_table').on( 'click', 'tbody tr button', function () {

    var parent = $(this).parent();

    var grandparent = parent.parent();

    var input_total_sales = parseFloat($('#total_sales').val());

    var product_amount = parseFloat($(grandparent).find('td:nth-child(3)').text());

    var search = parseFloat($(grandparent).find('td:nth-child(1)').text());

    var total_sales = input_total_sales - product_amount;

    var nodes = [];

    product_consumables_table.rows().every( function(rowIdx, tableLoop, rowLoop) {

      if ( this.data()[1] == search) nodes.push(this.node())

    })

    

    nodes.forEach(function(node) {

      product_consumables_table.row(node).remove().search('').draw()

    })

    $('#total_sales').val((+'0.00'+total_sales).toFixed(2));

    retail_services_table.row(grandparent).remove().draw();

  });



  retail_services_table.columns(0).visible(false);



  $('#gc_details').on( 'click', 'tbody tr button', function () {

    var parent = $(this).parent();

    var grandparent = parent.parent();

    var total_payment_old = parseFloat($('#total_payment').val()).toFixed(2);

    var gc_amount = parseFloat($(grandparent).find('td:nth-child(3)').text());

    var total_payment_new = total_payment_old - gc_amount;

    var input_gc_old = parseFloat($('#input_gc').val()).toFixed(2);

    var input_gc_new = input_gc_old - gc_amount;





    $('#total_payment').val((+'0.00'+total_payment_new).toFixed(2));

    $('#input_gc').val((+'0.00'+input_gc_new).toFixed(2));

    gc_details.row(grandparent).remove().draw();

  }); 

  var tmp = '0.00';



  if($('#transaction_id').val()){

    $.ajax({

      url: $('#base_url').val() + 'joborder/get_joborder_details_products/' + $('#transaction_id').val(),

      success: function(data){

        var product_services = JSON.parse(data);

        var input_total_sales = parseFloat($('#total_sales').val());



        for(var i = 0; i < product_services.length; i++){

          product_services[i].productservice_code

          product_services_table.row.add([

            product_services[i].productservice_id,

            product_services[i].productservice_code,

            product_services[i].productservice_description,

            product_services[i].transactiondetails_totalsales,

            product_services[i].serviceprovider_id,

            product_services[i].serviceprovider_name,

            product_services[i].transactiondetails_assisterid,

            product_services[i].assister_name,

            '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'

          ]).draw(false)

          input_total_sales += parseFloat(product_services[i].transactiondetails_totalsales);

          $('#total_sales').val((+tmp + input_total_sales).toFixed(2));



          $.ajax({

            url: $('#base_url').val() + 'joborder/get_servicemix_by_productservice_code/' + product_services[i].productservice_code,

            success: function(data){

              var productservice_servicemix = JSON.parse(data);

              for(let j = 0; j < productservice_servicemix.length; j++){

                product_consumables_table.row.add([

                  productservice_servicemix[j].servicemix_id,

                  productservice_servicemix[j].productservice_code,

                  productservice_servicemix[j].item_code,

                  productservice_servicemix[j].item_description,

                  productservice_servicemix[j].item_uom,

                  productservice_servicemix[j].servicemix_quantity

                ]).draw(false);

              }

            }

          })

        }

      }

    });



    $.ajax({

      url: $('#base_url').val() + 'joborder/get_joborder_details_retails/' + $('#transaction_id').val(),

      success: function(data){

        var product_retails = JSON.parse(data);

        var input_total_sales = parseFloat($('#total_sales').val());



        for(var i = 0; i < product_retails.length; i++){

          retail_services_table.row.add([

            product_retails[i].productretail_id,

            product_retails[i].productretail_code,

            product_retails[i].productretail_name,

            product_retails[i].transactiondetails_totalsales,

            product_retails[i].serviceprovider_id,

            product_retails[i].serviceprovider_name,

            product_retails[i].transactiondetails_assisterid,

            product_retails[i].assister_name,

            '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'
            // ''
          ]).draw(false)

          input_total_sales += parseFloat(product_retails[i].transactiondetails_totalsales);

          $('#total_sales').val((+tmp + input_total_sales).toFixed(2));

          $.ajax({

            url: $('#base_url').val() + 'joborder/get_servicemix_by_productservice_code/' + product_retails[i].productretail_code,
      
            success: function(data){
      
              var productservice_servicemix = JSON.parse(data);
      
              for(let j = 0; j < productservice_servicemix.length; j++){
      
                product_consumables_table.row.add([
      
                  productservice_servicemix[j].servicemix_id,
      
                  productservice_servicemix[j].productservice_code,
      
                  productservice_servicemix[j].item_code,
      
                  productservice_servicemix[j].item_description,
      
                  productservice_servicemix[j].item_uom,
      
                  productservice_servicemix[j].servicemix_quantity
      
                ]).draw(false);
      
              }
      
            }
      
          })

        }
      }

    });

  }



  calculate_total_payment();



  $("#dialog").dialog({

      autoOpen: false,

      modal: true,

      show: {

          effect: "blind",

          duration: 300

      },

      hide: {

          effect: "explode",

          duration: 400

      }

  });



  $("#dialog1").dialog({

      autoOpen: false,

      modal: true,

      show: {

          effect: "blind",

          duration: 300

      },

      hide: {

          effect: "explode",

          duration: 400

      }

  });



  $("#gc_validation_dialog").dialog({

      autoOpen: false,

      modal: true,

      show: {

          effect: "blind",

          duration: 300

      },

      hide: {

          effect: "explode",

          duration: 400

      }

  });

  $('.inputContainer').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
      e.preventDefault()
    })
  })
  $('.inputContainer').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
  })

})

function setModalMaxHeight(element) {
  this.$element     = $(element);  
  this.$content     = this.$element.find('.modal-content');
  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight     = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
      'overflow': 'hidden'
  });

  this.$element
    .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
  });
}

$('#loading_modal').on('show.bs.modal', function() {
  $(this).show();
  setModalMaxHeight(this);
});

$(window).resize(function() {
  if ($('.modal.in').length != 0) {
    setModalMaxHeight($('.modal.in'));
  }
});



function calculate_total_payment(){

  var tmp = '0.00';

  var cash = parseFloat($('#input_cash').val());

  var gc = parseFloat($('#input_gc').val());

  var credit = parseFloat($('#input_credit').val());



  if(!cash){

    cash = 0.00;

  }

  if(!credit){

    credit = 0.00;

  }

  if(!gc){

    gc = 0.00;

  }



  var total_payment = cash + gc + credit;



  $('#total_payment').val((+tmp + total_payment).toFixed(2));

}



$('#input_cash').keyup(function(){

  calculate_total_payment()

});



$('#input_gc').keyup(function(){

  calculate_total_payment();

});



$('#input_credit').keyup(function(){

  calculate_total_payment();  

});





function search_product_service(){

  var date_rendered = $('#date_rendered').val();

   

  $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).show();

  $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).show();

  var product_services_table = $('#product_services_table').DataTable();

  var retail_services_table = $('#retail_services_table').DataTable(); 

  var product_consumables_table = $('#product_consumables_table').DataTable();

  var search_product_service = $('#search_product_service');

  var search_retail_service = $('#search_retail_service');

  var input_total_sales = parseFloat($('#total_sales').val());

  var serviceprovider_id = $('#service_providers').select2('val');

  var serviceprovider_name = $('#service_providers').select2('data');

  var assister_id = '';
  
  if(typeof $('#service_assisters').select2('val') != 'undefined'){
    assister_id = $('#service_assisters').select2('val');
  }

  var assister_name = '';

  if(typeof $('#service_assisters').select2('data') != 'undefined'){
    assister_name = $('#service_assisters').select2('data');
    assister_name = assister_name[0].text;
  }

  var tmp = '0.00';

  if(!serviceprovider_id){
    alert('SP is required!')

    $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

    $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();


  } else {

      if (search_product_service.val()){



        if(product_services_table.rows().data().length > 0){
    
    
    
          var product_services = [];
    
    
    
          product_services_table.rows().every( function(rowIdx, tableLoop, rowLoop) {
    
            product_services.push(this.data()[1]);
    
          });
    
    
            $.ajax({
    
              url: $('#base_url').val() + 'joborder/get_product_service/' + search_product_service.val() + '/' +serviceprovider_id,
    
              success: function(data){
    
                var product_service = JSON.parse(data);
         
                if(product_service.product_service){
                  
                  product_services_table.row.add([
      
                    product_service.product_service.productservice_id,
    
                    product_service.product_service.productservice_code,
    
                    product_service.product_service.productservice_description,
    
                    product_service.product_service.productservice_amount,
    
                    serviceprovider_id,
    
                    serviceprovider_name[0].text,

                    assister_id,

                    assister_name,
    
                    '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'
    
                  ]).draw(false)
    
                  input_total_sales += parseFloat(product_service.product_service.productservice_amount);
    
                  $('#total_sales').val((+tmp + input_total_sales).toFixed(2));
        
    
                  if(product_service.servicemix){
    
        
    
                    for(let i = 0; i < product_service.servicemix.length; i++){
    
                      product_consumables_table.row.add([
    
                        product_service.servicemix[i].servicemix_id,
    
                        product_service.servicemix[i].productservice_code,
    
                        product_service.servicemix[i].item_code,
    
                        product_service.servicemix[i].item_description,
    
                        product_service.servicemix[i].item_uom,
    
                        product_service.servicemix[i].servicemix_quantity
    
                      ]).draw(false)
    
                    }
    
                  }

                  $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

                  $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
    
                }
    
                else{

                  $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

                  $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
    
                  alert('No service found!');
    
                }
    
              }
    
            });
    
            search_product_service.val('');
    
        }
    
        else{
    
          $.ajax({
    
            url: $('#base_url').val() + 'joborder/get_product_service/' + search_product_service.val() + '/' +serviceprovider_id,
    
            success: function(data){
    
              var product_service = JSON.parse(data);
              
              if(product_service.product_service){
    
                $.ajax({
    
                  url: $('#base_url').val() + 'joborder/get_serviceprovider/' + serviceprovider_id,
        
                  success: function(data){
        
                    var serviceprovider = JSON.parse(data);
                    console.log(serviceprovider)
                    product_services_table.row.add([
    
                      product_service.product_service.productservice_id,
      
                      product_service.product_service.productservice_code,
      
                      product_service.product_service.productservice_description,
      
                      product_service.product_service.productservice_amount,
      
                      serviceprovider_id,
      
                      serviceprovider_name[0].text,

                      assister_id,

                      assister_name,
      
                      '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'
      
                    ]).draw(false)
      
                    input_total_sales += parseFloat(product_service.product_service.productservice_amount);
      
                    $('#total_sales').val((+tmp + input_total_sales).toFixed(2));
          
      
                    if(product_service.servicemix){
      
          
      
                      for(let i = 0; i < product_service.servicemix.length; i++){
      
                        product_consumables_table.row.add([
      
                          product_service.servicemix[i].servicemix_id,
      
                          product_service.servicemix[i].productservice_code,
      
                          product_service.servicemix[i].item_code,
      
                          product_service.servicemix[i].item_description,
      
                          product_service.servicemix[i].item_uom,
      
                          product_service.servicemix[i].servicemix_quantity
      
                        ]).draw(false)
      
                      }
      
                    }

                    $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

                    $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
                  }
                });
    
              }
    
              else{

                $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

                $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
    
                alert('No service found!');
    
              }
    
            }
    
          });
    
          search_product_service.val('');
      
        }
    
      }

    
      if (search_retail_service.val()){
    
        console.log('retail');
    
        $.ajax({
    
          url: $('#base_url').val() + 'joborder/get_retail_service/' + search_retail_service.val(),
    
          success: function(data){
    
            var retail_service = JSON.parse(data);
    
            if(retail_service.product_retail){

              $.ajax({
    
                url: $('#base_url').val() + 'joborder/get_serviceprovider/' + serviceprovider_id,
      
                success: function(data){
      
                  var serviceprovider = JSON.parse(data);

                  if(retail_service.product_retail){
              
                    retail_services_table.row.add([
        
                      retail_service.product_retail.productretail_id,
        
                      retail_service.product_retail.productretail_code,
        
                      retail_service.product_retail.productretail_name,
        
                      retail_service.product_retail.productretail_amount,
        
                      serviceprovider_id,
      
                      serviceprovider_name[0].text,

                      assister_id,

                      assister_name,
        
                      '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'
        
                    ]).draw(false)
        
                    input_total_sales += parseFloat(retail_service.product_retail.productretail_amount);
        
                    $('#total_sales').val((+tmp + input_total_sales).toFixed(2));
        
                  }
                  
                  if(retail_service.servicemix){
        
                    for(let i = 0; i < retail_service.servicemix.length; i++){
        
                      product_consumables_table.row.add([
        
                        retail_service.servicemix[i].servicemix_id,
        
                        retail_service.servicemix[i].productservice_code,
        
                        retail_service.servicemix[i].item_code,
        
                        retail_service.servicemix[i].item_description,
        
                        retail_service.servicemix[i].item_uom,
        
                        retail_service.servicemix[i].servicemix_quantity
        
                      ]).draw(false)
        
                    }
        
                  }

                  $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

                  $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
                }
              });

            }
    
            else{

              $('.dataTables_processing', $('#product_services_table').closest('.dataTables_wrapper')).hide();

              $('.dataTables_processing', $('#retail_services_table').closest('.dataTables_wrapper')).hide();
    
              alert('No service found!');
    
            }
    
    
    
          }
    
        });
    
        search_retail_service.val('');
    
      }

    $('#service_providers').val('').trigger('change');
    $('#service_assisters').val('').trigger('change');
  }

}



function insert_transaction(){

  $('#button_save_transaction').attr('disabled', 'disabled');

  $('#loading_modal').modal('show')

  var customer_id = $('#customer_id').val()



  var date_rendered = $('#date_rendered').val();

  var docket_no = $('#docket_no').val();

  var or_no = $('#or_no').val();


  var input_cash = $('#input_cash').val();

  var input_credit = $('#input_credit').val();

  var input_gc = $('#input_gc').val();

  var payment = input_cash + input_credit;



  var total_sales = $('#total_sales').val();
 

  var product_services_table = $('#product_services_table').DataTable();

  var retail_services_table = $('#retail_services_table').DataTable(); 

  var product_consumables_table = $('#product_consumables_table').DataTable(); 

  var gc_details = $('#gc_details').DataTable(); 

 

  var retail_services_data = [];

  var product_services_data = [];

  var product_consumables_data = [];

  var gcs_data = [];



  var tmp = '0.00';

  var total_payment = $('#total_payment').val()



  var retail = retail_services_table.rows().data();

  

  for(var i = 0; i < retail.length; i++){

    retail_services_data.push(retail[i]);

  }

  

  var product = product_services_table.rows().data();



  for(var i = 0; i < product.length; i++){

    product_services_data.push(product[i]);

  }



  var consumables = product_consumables_table.rows().data();


  for(var i = 0; i < consumables.length; i++){

    product_consumables_data.push(consumables[i]);

  }



  var gcs = gc_details.rows().data();



  for(var i = 0; i < gcs.length; i++){

    gcs_data.push(gcs[i]);

  }



  if(!input_cash){

    payment = input_credit;

  }

  if(!payment){

    payment = input_gc;

  }



  if(eval(input_gc) > 0){

    if(eval(total_sales) > eval(input_gc)){

      total_sales = eval(total_sales) - eval(input_gc);

    }

    else if(eval(total_sales) <= eval(input_gc)){

      total_sales = 0.00;

    }

  }



  if (!date_rendered || !docket_no || !payment || (product_services_data.length == 0 && retail_services_data.length == 0) ||  (!input_gc && ( eval(total_payment) < eval($('#total_sales').val()) ) ) || ( input_gc && ( eval(total_payment) < eval( $('#total_sales').val()) ) ) || (!input_gc && ( eval(total_payment) > eval($('#total_sales').val()) ) )){

    var error = '';

    if(!date_rendered){

      error += 'Date Rendered is required.\n';

    }

    if (!docket_no) {

      error += 'Docket No. is required.\n';

    }

    if(!payment){

      error += 'Type of Payment is required.\n';

    }



    if(product_services_data.length == 0 && retail_services_data.length == 0){

      error += 'Both Product Services and Retail Services seems to be empty.\n';

    }

    if(!input_gc && ( eval(total_payment) > eval($('#total_sales').val()) ) ){
      error += 'Total Payment must not be greater than Total Sales.\n';
    }


    if( (!input_gc && ( eval(total_payment) < eval($('#total_sales').val()) ) ) || ( input_gc && ( eval(total_payment) < eval( $('#total_sales').val()) ) ) ){

      error += 'Total Payment must not be less than Total Sales.';

    }



    alert(error);

    $('#button_save_transaction').removeAttr('disabled');

    $('#loading_modal').modal('hide')

  }

  else{

    $.ajax({

      url: $('#base_url').val() +'joborder/insert_transaction',

      method: 'POST',

      data: { 

        'customer_id': customer_id,

        'brand_id': $('#user_brand_id').val(),

        'branch_id': $('#user_branch_id').val(),

        'transaction_date': date_rendered, 

        'transaction_docketno': docket_no, 

        'transaction_orno': or_no,

        'transaction_totalsales': total_sales, 

        'transaction_paymentcash': input_cash, 

        'transaction_paymentcard': input_credit, 

        'transaction_paymentgc': input_gc,

        'transaction_customerstatus': $('#foottraffic').val(),

        'retail_services_data': retail_services_data,

        'product_services_data': product_services_data,

        'product_consumables_data': product_consumables_data,

        'gcs_data': gcs_data

      },

      success: function(){

        alert('Transaction saved successfully!');

        window.location.href = $('#base_url').val()+'joborder';

        $('#loading_modal').modal('hide')

      }
      
    })

  }

}



function update_joborder(){

  $('#update_joborder_button').attr('disabled', 'disabled');

  $('#loading_modal').modal('show')

  var customer_id = $('#customer_id').val()

  var transaction_id = $('#transaction_id').val()



  var date_rendered = $('#date_rendered').val();

  var docket_no = $('#docket_no').val();  

  var or_no = $('#or_no').val();  



  var input_cash = $('#input_cash').val();

  var input_credit = $('#input_credit').val();

  var input_gc = $('#input_gc').val();

  var payment = input_cash + input_credit;

  var total_payment = $('#total_payment').val()



  var total_sales = $('#total_sales').val();


  var product_services_table = $('#product_services_table').DataTable();

  var retail_services_table = $('#retail_services_table').DataTable(); 

  var product_consumables_table = $('#product_consumables_table').DataTable();

 

  var retail_services_data = [];

  var product_services_data = [];

  var product_consumables_data = [];



  var retail = retail_services_table.rows().data();

  

  for(var i = 0; i < retail.length; i++){

    retail_services_data.push(retail[i]);

  }

  

  var product = product_services_table.rows().data();



  for(var i = 0; i < product.length; i++){

    product_services_data.push(product[i]);

  }



  var consumables = product_consumables_table.rows().data();



  for(var i = 0; i < consumables.length; i++){

    product_consumables_data.push(consumables[i]);

  }



  if(!input_cash){

    payment = input_credit;

  }

  if(!payment){

    payment = input_gc;

  }



  if(eval(input_gc) > 0){

    if(eval(total_sales) > eval(input_gc)){

      total_sales = eval(total_sales) - eval(input_gc);

    }

    else if(eval(total_sales) <= eval(input_gc)){

      total_sales = 0.00;

    }

  }



  if (!date_rendered || !docket_no || !payment || (product_services_data.length == 0 && retail_services_data.length == 0) ||  (!input_gc && ( eval(total_payment) < eval($('#total_sales').val()) ) ) || ( input_gc && ( eval(total_payment) < eval( $('#total_sales').val()) ) ) || (!input_gc && ( eval(total_payment) > eval($('#total_sales').val()) ) ) || ( (input_gc < eval(total_payment)) && ( eval(total_payment) > eval($('#total_sales').val()) ) )){

    var error = '';

    if(!date_rendered){

      error += 'Date Rendered is required.\n';

    }

    if (!docket_no) {

      error += 'Docket No. is required.\n';

    }

    if(!payment){

      error += 'Type of Payment is required.\n';

    }



    if(product_services_data.length == 0 && retail_services_data.length == 0){

      error += 'Both Product Services and Retail Services seems to be empty.\n';

    }

    if(!input_gc && ( eval(total_payment) > eval($('#total_sales').val()) ) ){
      error += 'Total Payment must not be greater than Total Sales.\n';
    }
    

    if(( (input_gc < eval(total_payment)) && ( eval(total_payment) > eval($('#total_sales').val()) ) )){
      error += 'Total Payment must not be greater than Total Sales.\n';
    }


    if( (!input_gc && ( eval(total_payment) < eval($('#total_sales').val()) ) ) || ( input_gc && ( eval(total_payment) < eval( $('#total_sales').val()) ) ) ){

      error += 'Total Payment must not be less than Total Sales.';

    }



    alert(error);

    $('#button_save_transaction').removeAttr('disabled');

    $('#loading_modal').modal('hide')

  }

  else{

    $.ajax({

      url: $('#base_url').val() +'joborder/update_transaction',

      method: 'POST',

      data: { 

        'transaction_id': transaction_id,

        'customer_id': customer_id,

        'brand_id': $('#user_brand_id').val(),

        'branch_id': $('#user_branch_id').val(),

        'transaction_date': date_rendered, 

        'transaction_docketno': docket_no, 

        'transaction_orno': or_no, 

        'transaction_totalsales': total_sales, 

        'transaction_paymentcash': input_cash, 

        'transaction_paymentcard': input_credit, 

        'transaction_paymentgc': input_gc,

        'retail_services_data': retail_services_data,

        'product_services_data': product_services_data,

        'product_consumables_data': product_consumables_data,

      },

      success: function(){

        alert('Transaction updated successfully!');

        window.location.reload();

        $('#loading_modal').modal('hide')

      }

    })

  }

}



function enable_editing(){

  $('#date_rendered').removeAttr('disabled');

  $('#docket_no').removeAttr('disabled');

  $('#or_no').removeAttr('disabled');

  $('#service_providers').removeAttr('disabled');

  $('#service_assisters').removeAttr('disabled');

  $('#search_product_service').removeAttr('disabled');

  $('#search_product_service_button').removeAttr('disabled');

  $('#search_retail_service').removeAttr('disabled');

  $('#search_retail_service_button').removeAttr('disabled');

  $('#date_rendered').removeAttr('disabled');

  $('#update_joborder_button').removeAttr('disabled');

  $('#search_gc').removeAttr('disabled');

  $('#search_gc_button').removeAttr('disabled');

}



function delete_joborder(){

  if(confirm('Are you sure you want to delete Transaction ID: '+$('#transaction_id').val())){

    $.ajax({
      url: $('#base_url').val()+'dashboard/get_current_server_date',
      method: 'GET',
      success: function(time){
        if(time <= 20190628){
          alert('You cannot delete transaction dated before or equal to June 28, 2019!');
        }
        else{
          $.ajax({

            url: $('#base_url').val() + 'joborder/delete_transaction/'+$('#transaction_id').val(),
      
            success: function(){
      
              alert('Successfully deleted Transaction ID: '+$('#transaction_id').val());
      
              window.location.href = $('#base_url').val()+'joborder';
      
            }
      
          })    
        }
      }
    })

  }

}



function search_gc(){

  var search_item = $('#search_gc').val();

  $('.dataTables_processing', $('#gc_details').closest('.dataTables_wrapper')).show();



  if(!search_item){

    alert('Insert value first!');

    return false;

  }

  else{

    $.ajax({

      url: $('#base_url').val()+'joborder/check_gc_availability',

      method: 'GET',

      data: {'search_item': search_item},

      success: function(data){

        var gc = JSON.parse(data);

        var message = '';



        if(gc.giftcheque_dateconsumed != null){

          message += '<p class="text-center text-danger">Gift Cheque has been consumed.</p> <br>';

          message += '<p>Barcode: <span class="pull-right">'+ gc.giftcheque_barcode + '</span></p>';

          message += '<p>Branch Used: <span class="pull-right">'+ gc.branch_name + '</span></p>';

          message += '<p>Date Consumed: <span class="pull-right">'+ gc.giftcheque_dateconsumed + '</span></p>';

          $('#dialog').empty();

          $('#dialog').append(message);

          $("#dialog").dialog("open");

        }

        if(data == 1){

          $('#gc_barcode').val(search_item);

          $("#gc_validation_dialog").dialog("open");

        }

        if(data == 0){

          message = '';

          message += "<p class='text-center text-danger' style='margin-top: 30px;'>Didn't match any existing GC.</p> <br>";

          $('#dialog1').empty();

          $('#dialog1').append(message);

          $("#dialog1").dialog("open");

        }

        $('.dataTables_processing', $('#gc_details').closest('.dataTables_wrapper')).hide();

      }

    })

  }



  $('#search_gc').val('')

}



function validate_gc(){

  $('#error').empty();

  var gc_pin = $('#gc_pin').val();

  var gc_details = $('#gc_details').DataTable();

  

  if(!gc_pin){

    alert('Insert value first!');

  }

  else{

    $.ajax({

      url: $('#base_url').val()+'joborder/validate_gc',

      method: 'GET', 

      data:{'pin': gc_pin, 'barcode': $('#gc_barcode').val()},

      success: function(data){

        var gc = JSON.parse(data);



        if(gc != false){

          var checker = null;

          $("#gc_validation_dialog").dialog("close");



          gc_details.rows().every(function (rowIdx, tableLoop, rowLoop ){

              var data = this.data();

              if(data[1] == gc.giftcheque_barcode){

                checker = true;

              }

          });



          if(checker == true){

            alert('GC is already at the table!');

          }

          else{

            var input_gc_val = parseFloat($('#input_gc').val()).toFixed(2);



            if(input_gc_val == 'NaN'){

              input_gc_val = 0.00;

            }



            var gc_amount = parseFloat(gc.giftcheque_amount).toFixed(2);

            var total_gc_val = eval(input_gc_val) + eval(gc_amount);

            var total_payment = parseFloat($('#total_payment').val()).toFixed(2);



            $('#input_gc').val((+0.00 +total_gc_val).toFixed(2));

            $('#total_payment').val((+0.00 + (eval(gc_amount) + eval(total_payment) )).toFixed(2))



            console.log(gc_details.row().data())



            gc_details.row.add([

              gc.giftcheque_id,

              gc.giftcheque_barcode,

              gc.giftcheque_name,

              gc.giftcheque_amount,

              '<button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'

            ]).draw();

          }

        }

        else{

          $('#error').empty();

          $('#error').append('<p class="text-danger text-center">Incorrect pin.</p>');

        }

      }

    })

  }

  $('#gc_pin').val('');

}





