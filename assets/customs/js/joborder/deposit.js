
$(document).ready(function() {

  var deposit_table = $('#deposit_table').DataTable({
    "dom" : 'Brtp',
    "bDestroy" : true,
    "aaData": null,
    "scrollX": false,
    "processing": true,
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    },
    "buttons" : ['excel'],
   'columnDefs': [
        { targets: [0, 1, 2], className: "align-left" },
        { targets: [3], className: "align-right" },
        { targets: [5, 4], className: "align-center" },

    ]
    })
  $('.select2').select2();
  $('#datepicker').datepicker({
    'autoclose': true
  });
  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

  $('.dataTables_processing', $('#deposit_table').closest('.dataTables_wrapper')).show();

     $.ajax({
          url: "get_all_deposit",
          method: "GET",
          success: function(data){
            deposit_list = JSON.parse(data);
            console.log(deposit_list);
            deposit_table.clear().draw();
              for(let i = 0; i < deposit_list.length; i++){
                deposit_table.row.add([
          
                  deposit_list[i].brand_name,

                  deposit_list[i].brand_name,
                 
                  deposit_list[i].transaction_date,

                  numberWithCommas(deposit_list[i].cash_deposit),
                  
                  deposit_list[i].deposit_date,

                  '<button class="btn btn-info btn-sm" onclick="show_status('+deposit_list[i].id+');"><i class="fa fa-search"></i></button>' +
                  '<button style = "margin-left: 5px !important; background-color: red; border-color: red;" class="btn btn-info btn-sm" onclick="show_status('+deposit_list[i].id+');"><i class="fa fa-trash"></i></button>',

          
                ])
              }
              $('.dataTables_processing', $('#deposit_table').closest('.dataTables_wrapper')).hide();
                deposit_table.draw()
            
          }

      })



});

$('#brand').change(function(){

    var brand_id = $('#brand').val();

    if (brand_id !== '') {

      $.ajax({

          url: "fetch_branches",

          method: "POST",

          data: {brand_id:brand_id},

          success:function(data){

            $('#branch').html(data);

          }

      })

    }

})

function add_deposit(){
  var deposit_table = $('#deposit_table').DataTable({
    "dom" : 'Brtp',
    "bDestroy" : true,
    "aaData": null,
    "scrollX": false,
    "processing": true,
    'language': {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    },
    "buttons" : ['excel'],
    'columnDefs': [
        { targets: [0, 1, 2], className: "align-left" },
        { targets: [3], className: "align-right" },
        { targets: [5, 4], className: "align-center" },

    ]
  })
  var datepicker = $('#datepicker').val();
  var amount = $('#amount').val();
  var remarks = $('#remarks').val();
  var brand_id = $('#brand').val();
  var branch_id = $('#branch').val();

  if (!datepicker || !amount) {
  	alert("Date and amount fields are required")
  }else{
  	      
      $.ajax({
          url: "add_deposit",
          method: "POST",
          data: {datepicker: datepicker, amount: amount, remarks:remarks, brand_id:brand_id, branch_id: branch_id},
          success: function(data){

      			$('#message').html("<div class = 'alert alert-success' style = 'text-align: center;' role = 'alert'>Success!</h5>")
      			.hide()
            .fadeIn(100);

            setTimeout(function(){
              	$('#add_deposit_modal').modal('hide')
            }, 1500);
            setTimeout(function(){
              $('#message').html('')
              $('#datepicker').val('')
              $('#amount').val('')
              $('#remarks').val('')
            }, 1500);
            
          }

      })





  }


}

// var datepicker = $('input[id$=datepicker]').datepicker({
//     dateFormat: 'yyyy-mm-dd'
// });





