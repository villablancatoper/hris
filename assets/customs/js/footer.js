$(function(){

  var services_table = $('#services_table').DataTable()



  var otc_table = $('#otc_table').DataTable()

    $('#lateencode_date').datepicker({
      autoclose: true,
      changeMonth: false
    })


  $.ajax({

    url: $('#base_url').val()+'dashboard/get_productservices_by_brand_id',

    method: 'GET',

    success: function(data){

      var productservices = JSON.parse(data);



      for(let i = 0; i < productservices.length; i++){

        services_table.row.add([

          productservices[i].productservice_code,

          productservices[i].productservice_description,

          productservices[i].productservice_amount,

          productservices[i].productservice_enddate


        ]).draw(false);

      }



      services_table.draw();
    
    }
    
  })



  $.ajax({

    url: $('#base_url').val()+'dashboard/get_productretails_by_brand_id',

    method: 'GET',

    success: function(data){

      var productretails = JSON.parse(data);



      for(let i = 0; i < productretails.length; i++){

        otc_table.row.add([

          productretails[i].productretail_code,

          productretails[i].productretail_name,

          productretails[i].productretail_amount,

          productretails[i].productretail_enddate
          

        ]).draw(false);

      }



      otc_table.draw();

    }

  })



  ip_local();



  var ip = setTimeout(check_local_ip, 500);

  survey_show();


  $('.select2').select2();

  $('#transfer_alert_success').hide();

  $.ajax({
    url: $('#base_url').val()+'dashboard/get_user_notice_status',
    method: 'GET',
    success: function(data){
      if(data == 0 && ($('#user_brand_id').val() != 100002 && $('#user_brand_id').val() != 100006)){
        // show_new_ordering_modal();
        show_required_modal();
      }
    }
  })
    
  $.ajax({
      url: $('#base_url').val()+'dashboard/get_branches',
      method: 'GET',
      success: function(data){
          $('#transfer_branch').html(data);
          $('#lateencode_branch').html(data);

      }
  })     

  if ($('#maintenance_mode').val() === 1) {

    window.location.href = $('#base_url').val()+'login/logout';
 
  }   

})

$('#maintenance').on('change', function() { 
   var maintenance = $(this).prop('checked')
    // var test = $(this).attr('data-test')
    if (maintenance === true) {
      $('#maintenance_modal').modal('show')
      $('#maintenance_message').html('Are you sure you want to set the system to maintenance mode? All users will be logged out and they cannot logged in to the system while under maintenance mode.')
    }else{
      $('#maintenance_modal').modal('show')
      $('#maintenance_message').html('End maintenance mode?')
    }
});

function cancel_maintenance(){
  var maintenance = $('#maintenance').prop('checked')
  if (maintenance === true) {
      $('#maintenance').bootstrapToggle('off')
      $('#maintenance_modal').modal('hide')
  }else{
      $('#maintenance').bootstrapToggle('on')
      $('#maintenance_modal').modal('hide')
  }
  
}



function confirm_maintenance(){
  var maintenance = $('#maintenance').prop('checked')
  if (maintenance === true) {
      $('#maintenance').bootstrapToggle('on')
      $('#maintenance_modal').modal('hide')
      $.ajax({
        method: 'GET',
        url: $('#base_url').val()+'login/start_maintenance',
        success: function(data){

        }
      })
  }else{
      $('#maintenance').bootstrapToggle('off')
      $('#maintenance_modal').modal('hide')
      $.ajax({
        method: 'GET',
        url: $('#base_url').val()+'login/end_maintenance',
        success: function(data){

        }
      })

  }
  
}

function survey_show(){
  $.ajax({
    url: $('#base_url').val()+'settings/get_branch_survey_status',
    method: 'GET',
    success: function(data){
      var result = JSON.parse(data);
      var temp = result['branch_surveystatus']
      // console.log(temp)
      if (temp == 1) {
        $('#survey_modal').modal('show');
      } 
    }
  })
}

function submit_survey(){
  var survey_contact = $('#survey_contact').val()
  var internet_survey = $('#internet_survey').val()
  var telephone_survey = $('#telephone_survey').val()
  var terminal_survey = $('#terminal_survey').val()
  var computer_survey = $('#computer_survey').val()
  var other_survey = $('#other_survey').val()

  if (!survey_contact) {
    alert("Please fill required fields");
  }else{
    $.ajax({
        url: $('#base_url').val()+'settings/submit_survey',
        method: 'POST',
        data: {survey_contact: survey_contact,
          internet_survey: internet_survey,
          telephone_survey: telephone_survey,
          terminal_survey: terminal_survey,
          computer_survey: computer_survey,
          other_survey: other_survey
        },
        beforeSend: function(){
            $("#btn_encode_later").attr("disabled", true);
            $("#btn_submit_survey").attr("disabled", true);
        },
        success: function(data){
          $('#survey_success').html("<div class = 'alert alert-success' role = 'alert'>Survey submitted successfully!</h5>")
          .hide()
          .fadeIn(100);
          setTimeout(function(){
          $('#survey_modal').modal('hide')
            }, 1500);
          $("#survey_form")[0].reset();
        }
    })
  }
}

function answer_later() {
        $.ajax({
            url: $('#base_url').val()+'settings/answer_later',
            method: 'POST',
            success: function(data){
              $('#survey_modal').modal('hide')
            }
        })
}


function show_new_ordering_modal(){
  $('#new_ordering_notice_modal').modal('show');
}

function show_required_modal(){
  $('#required_modal').modal('show');
}





function update_user_notice_status(){
  $.ajax({
    url: $('#base_url').val()+'dashboard/update_user_notice_status',
    method: 'POST', 
    success: function(){
      // $('#new_ordering_notice_modal').modal('hide');
      $('#required_modal').modal('hide');

    }
  })
}

function show_transferbranch(){
    $('#transferbranch_modal').modal('show');
   
}


function transfer_branch() {

    var transfer_user_id = $('#transfer_user_id').val();

    var transfer_branch = $('#transfer_branch').val();



    if(!transfer_branch){



    alert('Please select branch!');



    } else {

      $.ajax({

        url: $('#base_url').val()+'dashboard/transfer_branch',

        method: 'POST',

        data: {transfer_user_id: transfer_user_id, transfer_branch: transfer_branch},

        success: function (data) {

            var mes = JSON.parse(data);

            // console.log(mes)

            if (mes['response'] === 'failed') {

                alert("Transfer of branch is available once per day only.")

                // $('#transfer_modal').hide()

            } else {

                $('#transfer_alert_success').show(400);

                $('#counter').text(3)

                logout_timer()

                $('#transfer_form').hide()

                $('#transfer_update_button').hide()

            }

            



        }

      })

    }

}


function logout_timer(){

  var base_url = $('#base_url').val();

  var counter = 3;

  var interval = setInterval(function() {

      counter--;

      // Display 'counter' wherever you want to display it.

      $('#counter').text(counter)

      if (counter == 0) {

         window.location.replace(base_url+"login/logout");

      }

  }, 1000);

}



function logout(){
  
  $('#logout').attr('disabled', 'disabled')

  window.location.href = $('#base_url').val()+"login/logout";

}



function startTime() {



  $.ajax({

    url: $('#base_url').val()+'dashboard/get_current_server_time',

    method: 'GET',

    success: function(data){



      document.getElementById('current_time').value = data;

      check_account_validity()

    }

  })

}



function check_account_validity(){

  var time = parseInt(document.getElementById('current_time').value);



  var current_user_brand_id = parseInt(document.getElementById('user_brand_id').value);



  if(current_user_brand_id != 100007){

      

    if((time < 80000 && time > 0) || time > 236000){

        // window.location.href = $('#base_url').val()+'login/logout';

    }



    // if(time > 233000 && time < 233100){



    //   alert('You only have 30 mins. to use the system.');



    // }



    // if(time > 230000 && time < 230100){



    //   alert('You only have 1 hour to use the system.');



    // }



    // if(time > 234000 && time < 234100){



    //   alert('You only have 20 mins. to use the system.');



    // }



    // if(time > 235000 && time < 235100){



    //   alert('You only have 10 mins. to use the system.');



    // }



    // if(time > 235500 && time < 235600){



    //   alert('You only have 5 mins. to use the system.');



    // }



    // if(time > 235959){



    //   alert('Time out!');



    // }

    

  }



  var current_time = parseInt(document.getElementById('current_time').value) + 1;



  var seconds_workaround = parseInt(document.getElementById('seconds_workaround').value) + 1;



  document.getElementById('seconds_workaround').value = seconds_workaround;



  document.getElementById('current_time').value = current_time;



  if(seconds_workaround == 59){

    document.getElementById('seconds_workaround').value = 0;

    document.getElementById('current_time').value = current_time + 40;

  }



  if(current_time > 240000){

    document.getElementById('current_time').value = 0

  }



  // console.log(document.getElementById('current_time').value);

  

  var t = setTimeout(check_account_validity, 1000);







}



function checkTime(i) {



  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10



  return i;



}



$('.no-url').click(function(){

  if($(this).attr('href') == '#'){

    alert('Not implemented yet.');

  }

})



function show_change_password_modal(){

    $('#change_password_modal').modal('show');

}



function numberWithCommas(x) {



    if(!x || x == null || parseFloat(x) == 0 || x == "0.00"){

      return 0.00;

    }



    var parts = x.toString().split(".");

    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    return parts.join(".");

}



function numberWithCommasConvertToPositive(x) {



  if(!x || x == null || x == 0){

    return 0.00;

  }



  var temp = false;



  if(eval(x) < 0){

    x = Math.abs(x);
    temp = true;
    x = x.toFixed(2);
  }



  var parts = x.toString().split(".");

  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return (temp == true ? '<span style="color: white !important; font-weight: bold;">(' : '' )+(parts.join("."))+(temp == true ? ')</span>' : '');

}



function detectNegative(number){

  

  if(eval(number) < 0){

    return ' style="background-color: red !important; text-align: right !important;"';

  }


  return '';

}



function show_services_otc_modal(){

  $('#services_otc_modal').modal('show');

}

function allow_lateencode(){

  $('#allow_lateencode_modal').modal('show');

}



$('#change_password_modal').on('show.bs.modal', function(){

  $('#input_wrapper').show()

  $('#add_turnaway_alert_success').hide()

  $('#change_password_modal').find('[class="modal-footer"]').show();



  $('#old_password').val('');

  $('#new_password').val('');

  $('#confirm_password').val('');

})





function change_password(){

    var old_password = $('#old_password').val();

    var new_password = $('#new_password').val();

    var confirm_password = $('#confirm_password').val();

    var error = '';

    

    if(!old_password || !new_password || !confirm_password || (confirm_password != new_password) ){

        if(!old_password){

            error += 'Old password is required!\n';

        }

        if(!new_password){

            error += 'New password is required!\n';

        }

        if(!confirm_password){

            error += 'Confirm password is required!\n';

        }

        if(confirm_password != new_password){

            error += 'Passwords do not match!\n';

        }

        alert(error);

    }

    else{

        $.ajax({

            url: $('#base_url').val()+'dashboard/change_password',

            method: 'POST',

            data: {'old_password': old_password, 'new_password': new_password},

            success: function(data){

              if(data == true){

                $('#input_wrapper').hide()

                $('#add_turnaway_alert_success').show(400)

                $('#change_password_modal').find('[class="modal-footer"]').hide();

              }

              if(data == false){

                alert("Old password is incorrect!");

              }

            }

        })

    }

}







function ip_local()

{

 var ip = false;

 window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection || false;



 if (window.RTCPeerConnection)

 {

  ip = [];

  var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};

  pc.createDataChannel('');

  pc.createOffer(pc.setLocalDescription.bind(pc), noop);



  pc.onicecandidate = function(event)

  {

   if (event && event.candidate && event.candidate.candidate)

   {

    var s = event.candidate.candidate.split('\n');

    ip.push(s[0].split(' ')[4]);

    $('#ip_local').val(ip);

   }

  }

 }

}



function check_local_ip(){

    if($('#ip_local').val()){

        if(($('#ip_local').val() == '') && ($('#ip_public').val() == '43.255.216.134')){

            window.location.href = 'http://192.168.2.60/crm';

        }

    }

}



function allow_passage(){

  if($('#allowed').val() == 1){ 

    window.location.href = $('#base_url').val()+'budgetfolder/store_budgetfolder'

  }

  else{

    alert('Physical Inventory is not yet activated.');

  }

}









