$(document).ready(function() {
    $('#ticket_status').DataTable( {
        "dom": '<"top"><"top"p>t<"clear">',
        "bDestroy" : true,
        "processing": true,
         'language': {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
      }
    });
});

function post_update($ticket_id){
    var ticket_status_table = $('#ticket_status').DataTable( {
        "dom": '<"top"f><"top"p>t<"clear">',
        "processing": true,
        "bDestroy" : true,
        'language': {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
      },
    });
   
    var update_details = $("#update_details").val();
    var base_url = $('#base_url').val();
    var ticket_id = $ticket_id;
    if (!update_details) {
        alert("Please input details")
         // $('.dataTables_processing', $('#ticket_status_table').closest('.dataTables_wrapper')).hide();
    }else{
        $.ajax({
            type: "POST",
            url: base_url+'ticketing/update/'+ticket_id,
            data: {
              ticket_id:ticket_id,
              update_details:update_details
            },
            success: function() {
              // $('#message').html("<div class = 'alert alert-success' role = 'alert'>Update posted!</h5>")
              $("#success").html("<div class = 'alert alert-success' role = 'alert'>Update posted!</h5>")
              .hide()
              .fadeIn(100)
              setTimeout(function(){
              $('#modal_post_comment').modal('hide')
                }, 1500);
              $("#form_post_comment")[0].reset();
              setTimeout(function(){
                 window.location.reload(1);
              }, 1000);
            

            }
    });
    }
    

}

$('.select2').select2();

function close_ticket($ticket_id){
  var base_url = $('#base_url').val();
  var ticket_id = $ticket_id;
  var date = $('#date').val();
  var remarks = $('#remarks').val();
    if(date === ''){
        alert("Please fill required fields");
    }else{
         $.ajax({
            type: "POST",
            url: base_url+'ticketing/close_ticket/'+ticket_id,
            data: {
              ticket_id:ticket_id,
              date:date,
              remarks:remarks
            },
            success: function() {
              setTimeout(function(){
              $('#modal_close_ticket').modal('hide')
                }, 500);
            setTimeout(function(){
                 window.location.reload();
              }, 1000);
            }
        });
    }
  

}
function accept_ticket($ticket_id){
  var ticket_id = $ticket_id;
  var base_url = $('#base_url').val();
  // alert(ticket_id)
  $.ajax({
            type: "POST",
            url: base_url+'ticketing/accept_ticket/'+ticket_id,
            data: {
              ticket_id:ticket_id
            },
            success: function() {
              setTimeout(function(){
              $('#modal_accept_ticket').modal('show')
                }, 300);
              setTimeout(function(){
              $('#modal_accept_ticket').modal('hide')
                }, 1500);
              setTimeout(function(){
                 window.location.reload(1);
              }, 1500); 
            }
    });

}

function new_assign($ticket_id){
  var base_url = $('#base_url').val();
  var ticket_id = $ticket_id;
  var ticket_assignee = $('#ticket_assignee').val();
  // alert(ticket_assignee)
    $.ajax({
            type: "POST",
            url: base_url+'ticket/assign_ticket',
            data: {
              ticket_id:ticket_id,
              ticket_assignee:ticket_assignee
            },
            success: function() {
              $('#message').html("<div class = 'alert alert-success' role = 'alert'>Success!</h5>")
              .hide()
              .fadeIn(100);
              setTimeout(function(){
              $('#modal_new_assignee').modal('hide')
                }, 1000);
              $("#form_assign")[0].reset();
               setTimeout(function(){
                 window.location.reload();
              }, 1000);
            }
    });




}
var text_max = 255;
$('#count_message').html(text_max + ' remaining');
$('#update_details').keyup(function() {
  var text_length = $('#update_details').val().length;
  var text_remaining = text_max - text_length;
  
  $('#count_message').html(text_remaining + ' remaining');
});