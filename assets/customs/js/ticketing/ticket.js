$(document).ready(function() {

  var ticketList_table = $('#ticketList_table').DataTable({
    "dom": '<"top">Bfrt<"bottom"lp><"clear">',
    "bDestroy" : true,
    "ordering": true,
    buttons: [{
      extend: 'excel',
    }],
    'bAutoWidth': false,
    'columnDefs': [
        { targets: [1,2,3,4,6],
          className: "align-right" }
    ]
    })
  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');


  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'ticketing/get_ticket_list',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,
        
          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button>',

  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }

});
  $('.select2').select2();

});


$('#filter_dept').change(function(){

    var dept = $('#filter_dept').val();
    // console.log(dept)
    if (dept != '') {

        var ticketList_table = $('#ticketList_table').DataTable({
          "dom": '<"top"f>rt<"bottom"lp><"clear">',
          "bDestroy" : true,
        })

        $.ajax({
        
          url: 'ticketing/fiter_ticket_by_dept',

          method: 'POST',

          data: {dept: dept},

          success: function(data){

            var ticket_list = JSON.parse(data);
            console.log(ticket_list)
            ticketList_table.clear().draw();

            for(let i = 0; i < ticket_list.length; i++){
              ticketList_table.row.add([
        
                ticket_list[i].ticket_id,

                ticket_list[i].ticket_submit_date_time,
               
                ticket_list[i].ticket_brand_branch,

                ticket_list[i].ticket_category,
                
                ticket_list[i].ticket_title,

                ticket_list[i].ticket_status,

                ticket_list[i].ticket_aging,
              
                ticket_list[i].ticket_last_update_date,
                
                ticket_list[i].ticket_last_update_details,

                '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button>',

              ])
            }

            $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

            ticketList_table.draw()
          }
        });

    }

})


function show_status($ticket_id){
// alert($ticket_id);
        window.location.href = 'ticketing/status/'+$ticket_id;
}





function sort_all(sort_index){

  // alert(sort_index)
  var ticketList_table = $('#ticketList_table').DataTable({
    "dom": '<"top">Bfrt<"bottom"lp><"clear">',
    "bDestroy" : true,
    "ordering": true,
    buttons: [{
      extend: 'excel',
    }],
    'bAutoWidth': false,
    'columnDefs': [
        { targets: [1,2,3,4,6],
          className: "align-right" }
    ]
    })
  $('.dt-buttons').addClass('pull-right');

  $('.dt-buttons').attr('style', 'margin: 0px 0px 20px 0px;');

  $('.dt-button').addClass('btn btn-default');

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'ticketing/sort_ticket',

    method: 'POST',

    data: {sort_index: sort_index},

    success: function(data){

      var ticket_list = JSON.parse(data);
      console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,
        
          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button>',

  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
  });
  
}


function ticket_submit(){
    var site_store = $("#site_store").val();
    var concern_type = $("#concern_type").val();
    var concern_details = $("#concern_details").val();
    if (site_store === "" || concern_type === "" || concern_details === "") {
      alert("Please choose required fields")
    }else{
        $.ajax({
                type: "POST",
                url: "ticketing/submit_ticket",
                data: {
                  site_store:site_store, concern_type:concern_type, concern_details:concern_details
                },
                beforeSend: function(){
                      $("#create_new_ticket").attr("disabled", true);
                },
                success: function() {

                $('#message').html("<div class = 'alert alert-success' role = 'alert'>Ticket Submitted!</h5>")
              .hide()
              .fadeIn(100);
              setTimeout(function(){
              $('#modal_new_ticket').modal('hide')
                }, 1500);
              $("#form_create_ticket")[0].reset();
             
              setTimeout(function(){
                 window.location.reload(1);
              }, 1000);
            
            }
        });
    }
}


function search_overview(){
  var overview_table = $('#overview_table').DataTable({
    "dom": '<"top"><"bottom"><"clear">',
     "bDestroy" : true,
     "ordering": false,
     'columnDefs': [
        { targets: [2,3,4,5,7,8],
          className: "align-center" },
        {targets: [1, 8], className: "no_hover"}
      ]


  });

  $('.dataTables_processing', $('#overview_table').closest('.dataTables_wrapper')).show();
  var department = $("#department").val();
  var month = $("#month").val();
  var year = $("#year").val();

  if (!department || !month || !year) {
    alert("Please select department, month and year")
  $('.dataTables_processing', $('#overview_table').closest('.dataTables_wrapper')).hide();

  }else{
    $("#dept").html(department);
      $.ajax({
  
        url: 'ticketing/search_overview',

        method: 'POST',

        data: {department: department, month: month, year: year},

        success: function(data){
          var overview_list = JSON.parse(data);
          console.log(overview_list)
          overview_table.clear().draw();
          var concern_count = 0;
          var completed_count = 0;
          var cond1 = 0;
          var total_cond1 = 0;
          var cond2 = 0;
          var total_cond2 = 0;
          var cond3 = 0;
          var total_cond3 = 0;
          var cond4 = 0;
          var total_cond4 = 0;
          var cond5 = 0;
          var total_cond5 = 0;
          var last_concern = '';
          var percentage = 0;
          var total_concern = 0;
          var total_completed = 0;
          var total_pending = 0;
          var total_percentage = 0;
          for(let i = 0; i < overview_list['category'].length; i++){
                  for (let y = 0; y < overview_list['list'].length; y++) {
                    if (overview_list['category'][i].category === overview_list['list'][y].ticket_category) {
                      concern_count = parseFloat(concern_count) + 1;
                        if (overview_list['list'][y].ticket_status === 'Completed') {
                          completed_count = parseFloat(completed_count) + 1;
                        }
                        if (overview_list['list'][y].ticket_status !== 'Completed') {
                            if (overview_list['list'][y].ticket_aging <= 15) {
                                cond1 +=1;
                            } else if (overview_list['list'][y].ticket_aging <= 30 && overview_list['list'][y].ticket_aging > 15) {
                                cond2 +=1;
                            } else if (overview_list['list'][y].ticket_aging <= 60 && overview_list['list'][y].ticket_aging > 30) {
                                cond3 +=1;
                            } else if (overview_list['list'][y].ticket_aging <= 90 && overview_list['list'][y].ticket_aging > 60) {
                                cond4 +=1;
                            } else if (overview_list['list'][y].ticket_aging > 90) {
                                cond5 +=1;
                            }

                        }


                    }
                  }
                   total_concern = eval(total_concern) + eval(concern_count);
                   total_completed = eval(total_completed) + eval(completed_count);
                   total_cond1 = eval(total_cond1) + eval(cond1);
                   total_cond2 = eval(total_cond2) + eval(cond2);
                   total_cond3 = eval(total_cond3) + eval(cond3);
                   total_cond4 = eval(total_cond4) + eval(cond4);
                   total_cond5 = eval(total_cond5) + eval(cond5);


                  // var total_pending = eval(total_pending) + eval(pending_count);

                  percentage = eval(eval(completed_count) / eval(concern_count) * 100);
                  if (isNaN(percentage)) {
                    percentage = 0;
                  }
                  if (cond1 === 0) {
                      cond1 = "";
                  }
                  if (cond2 === 0) {
                      cond2 = "";
                  }
                  if (cond3 === 0) {
                      cond3 = "";
                  }
                  if (cond4 === 0) {
                      cond4 = "";
                  }
                  if (cond5 === 0) {
                      cond5 = "";
                  }
                  if (concern_count === 0 && completed_count === 0) {
                      concern_count = "";
                      completed_count = "";
                      percentage = "";
                  }else{
                      percentage =  percentage.toFixed(2)+"%";
                  }
                  
                    overview_table.row.add([
                      overview_list['category'][i].category,
                      concern_count,
                      completed_count,
                      cond1,
                      cond2,
                      cond3,
                      cond4,
                      cond5,
                      percentage
 
                    ])
                    concern_count = 0;
                    completed_count = 0;
                    pending_count = 0;
                    cond1 = 0;
                    cond2 = 0;
                    cond3 = 0;
                    cond4 = 0;
                    cond5 = 0;
          }
          total_percentage = eval(eval(total_completed) / eval(total_concern) * 100);
           if (isNaN(total_percentage)) {
                    total_percentage = 0;
            }
          $("#total_concern").html(total_concern);
          $("#total_completed").html(total_completed);
          $("#total_cond1").html(total_cond1);
          $("#total_cond2").html(total_cond2);
          $("#total_cond3").html(total_cond3);
          $("#total_cond4").html(total_cond4);
          $("#total_cond5").html(total_cond5);


          // $("#total_pending").html(total_pending);
          $("#total_percentage").html(total_percentage.toFixed(2)+"%");

          $('.dataTables_processing', $('#overview_table').closest('.dataTables_wrapper')).hide();
          overview_table.draw()
        }
      });
  }
}






