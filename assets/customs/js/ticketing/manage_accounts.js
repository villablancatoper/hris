$(document).ready(function() {
    $('#user_list').DataTable({
        "ajax": {
            "url": "manage_accounts/get_user_list",
            "dataSrc": ""
        },
        "columns": [{
            "data": "full_name"
        }, {
            "data": "job_title"
        }, {
            "data": "department_name"
        }, {
            "data": "username"
        }, {
            "data": "password"
        }, {
            "data": "date_created"
        }, {
        	"data": "last_login"
        }]
    });
});
$('.select2').select2();
 function create_account(){
         var department = $("#department").val();
         var name = $("#name").val(); 
         var job_post = $("#job_post").val();
         var account_username = $("#account_username").val();
         var account_password = $("#account_password").val();

         if (department == "" || name == "" || job_post == "" || account_username == "" || account_password == "") {
            alert("Please fill up required fields")
         }else{
            $.ajax({
                    type: "POST",
                    url: "dashboard/create_account",
                    data: {
                      department:department,
                      name:name,
                      job_post:job_post,
                      account_username:account_username,
                      account_password:account_password
                    },
                    success: function() {

                     $('#message').html("<div class = 'alert alert-success' role = 'alert'>Account Created!</h5>")
                  .hide()
                  .fadeIn(100);
                  setTimeout(function(){
                  $('#modal_new_ticket').modal('hide')
                    }, 3000);
                  $("#form_create_ticket")[0].reset();
                 
                  setTimeout(function(){
                     window.location.reload(1);
                  }, 3000);
                

                }
            });
          }

 }
//  function changePassword(){
//     var old_password = $('#old_password').val();
//     var new_password = $('#new_password').val();
//     var confirm_password = $('#confirm_password').val();
//     var error = '';

//     if(!old_password || !new_password || !confirm_password || (confirm_password != new_password) ){
//         if(!old_password){
//             error += 'Old password is required!\n';
//         }
//         if(!new_password){
//             error += 'New password is required!\n';
//         }
//         if(!confirm_password){
//             error += 'Confirm password is required!\n';
//         }
//         if(confirm_password != new_password){
//             error += 'Passwords do not match!\n';
//         }
//         alert(error);
//     }else{
//             $.ajax({
//                     type: "POST",
//                     url: "dashboard/changePassword",
//                     data: {
//                       old_password:old_password,
//                       new_password:new_password,
//                       confirm_password:confirm_password
//                     },
//                     success: function() {
//                      $('#message').html("<div class = 'alert alert-success' role = 'alert'>Account Created!</h5>")
//                   .hide()
//                   .fadeIn(100);
//                   setTimeout(function(){
//                   $('#modal_new_ticket').modal('hide')
//                     }, 3000);
//                   $("#form_create_ticket")[0].reset();
//                   setTimeout(function(){
//                      window.location.reload(1);
//                   }, 3000);
//                 },
//                 error: function(){
//                   alert("Old password is not correct. Please try again")
//                 }
//             });
//           }

// }

