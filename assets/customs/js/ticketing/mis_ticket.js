
$(document).ready(function() {

  var ticketList_table = $('#mis_ticket_table').DataTable({
    "dom": '<"top"f>rt<"bottom"lp><"clear">',
  })

  $('.dataTables_processing', $('#mis_ticket_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_ticket_list',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      console.log(ticket_list);
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button>'
  
        ])
      }

      $('.dataTables_processing', $('#mis_ticket_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }

});
    $('.select2').select2();

});



function show_status($ticket_id){
// alert($ticket_id);
        
        window.location.href = 'ticket/status/'+$ticket_id;
}



function sort_new(){


  var ticketList_table = $('#mis_ticket_table').DataTable()

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_new_tickets',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      // console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button></a>'
  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
});

}

function sort_assigned(){


  var ticketList_table = $('#mis_ticket_table').DataTable()

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_assigned',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      // console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button></a>'
  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
});

}

function sort_all(){

      var ticketList_table = $('#mis_ticket_table').DataTable()

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_ticket_list',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      // console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

          ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button></a>'
  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
});
}

function sort_progress(){

      var ticketList_table = $('#mis_ticket_table').DataTable()

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_progress_tickets',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      // console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
         ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

           ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button></a>'
  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
});
}

function sort_completed(){

      var ticketList_table = $('#mis_ticket_table').DataTable()

  $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).show();

  $.ajax({
  
    url: 'dashboard/get_completed_tickets',

    method: 'GET',

    success: function(data){

      var ticket_list = JSON.parse(data);
      // console.log(ticket_list)
      ticketList_table.clear().draw();

      for(let i = 0; i < ticket_list.length; i++){
        ticketList_table.row.add([
  
          ticket_list[i].ticket_id,

          ticket_list[i].ticket_submit_date_time,
         
          ticket_list[i].ticket_brand_branch,

          ticket_list[i].ticket_category,
          
          ticket_list[i].ticket_title,

           ticket_list[i].ticket_assignee,

          ticket_list[i].ticket_status,

          ticket_list[i].ticket_aging,

          ticket_list[i].ticket_last_update_date,
          
          ticket_list[i].ticket_last_update_details,

          '<button class="btn btn-info btn-sm" onclick="show_status('+ticket_list[i].ticket_id+');"><i class="fa fa-search"></i></button></a>'
  
        ])
      }

      $('.dataTables_processing', $('#ticketList_table').closest('.dataTables_wrapper')).hide();

      ticketList_table.draw()
    }
});
}


function ticket_submit(){
    var site_store = $("#site_store").val();
    var concern_type = $("#concern_type").val();
    var concern_details = $("#concern_details").val();
    // alert(concern_type)
    if (site_store == "" || concern_type == "") {
      alert("Please choose required fields")
    }else{
        $.ajax({
                type: "POST",
                url: "dashboard/submit_ticket",
                data: {
                  site_store:site_store, concern_type:concern_type, concern_details:concern_details
                },
                success: function() {

                 $('#message').html("<div class = 'alert alert-success' role = 'alert'>Update posted!</h5>")
              .hide()
              .fadeIn(100);
              setTimeout(function(){
              $('#modal_new_ticket').modal('hide')
                }, 1500);
              $("#form_create_ticket")[0].reset();
             
              setTimeout(function(){
                 window.location.reload(1);
              }, 1500);
            

            }
        });
    }
}






